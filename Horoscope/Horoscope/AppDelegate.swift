//
//  AppDelegate.swift
//  Horoscope
//
//  Created by Beatman on 16/11/18.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import AppsFlyerLib
import Flurry_iOS_SDK
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate,ChooseHoroDelegate{
    var window: UIWindow?
    var rootViewController = HoroTabBarViewController()
    var firstSendNotifAnaliticsEvent = true
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool{
        self.setWindow(application)
        
        self.initFirebase()
        self.initFacebook(launchOptions, appId: "808565275950982")
        AnaliticsManager.initAnalizers()
        self.initBugly()
        
        if UserDefaults.standard.bool(forKey: "everLaunched") == false {
            UserDefaults.standard.set(true, forKey: "everLaunched")
            UserDefaults.standard.set(true, forKey: "isFirstLaunch")
        } else {
            UserDefaults.standard.set(false, forKey: "isFirstLaunch")
        }
        self.initAuth()
        //     GuideAuthManager.sharedInstance.setAuthPushGuideType()
        //     GuideAuthManager.sharedInstance.setGuideTextType()
        return true
        
    }
    
    fileprivate func setWindow(_ application: UIApplication){
        let oientation = UIInterfaceOrientation.portrait
        application.statusBarOrientation = oientation
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
    
    fileprivate func initAuth(){
        //        let status = AccountManager.sharedInstance.getLogStatus()
        //        if  status == .Null{
        //            let vc = UIViewController()
        //            AccountManager.sharedInstance.loginWithType(.Anonymous,
        //                                                        viewcontroller: vc,
        //                                                        data: nil,
        //                                                        logInSuccess: {
        //
        //                }, failed: {
        //            })
        //        }
    }
    
    fileprivate func initFirebase() {
        FIRApp.configure()
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil,"Error configuring Google services: \(String(describing: configureError))")
        GIDSignIn.sharedInstance().clientID = "706033906613-dt1cit1un0bqv0m9o3mi44oolkv7qot0.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
    }
    
    fileprivate func initFacebook(_ launchOption: [AnyHashable: Any]?,
                              appId: String){
        FBSDKApplicationDelegate.sharedInstance().application(UIApplication.shared, didFinishLaunchingWithOptions: launchOption)
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)
        FBSDKSettings.setAppID(appId)
    }
    
    fileprivate func initGA(){
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai?.trackUncaughtExceptions = true // report uncaught exceptions
        #if DEBUG
            gai?.logger.logLevel = GAILogLevel.verbose // remove before app release
        #endif
    }
    
    fileprivate func initBugly(){
        #if DEBUG
            //   AppsFlyerTracker.sharedTracker().useReceiptValidationSandbox = true;
        #endif
        Bugly.start(withAppId: "d917f4d077")
    }
    
    fileprivate func initAnaliticsManager(){
        AnaliticsManager.initAnalizers()
    }
    
    fileprivate func initLocalNotification(){
        
        //        let pushtypes : UIUserNotificationType = [UIUserNotificationType.Badge,UIUserNotificationType.Alert,UIUserNotificationType.Sound]
        //        let mySetting : UIUserNotificationSettings = UIUserNotificationSettings(forTypes: pushtypes, categories: nil)
        //        UIApplication.sharedApplication().registerUserNotificationSettings(mySetting)
        
        if UserDefaults.standard.bool(forKey: NotificationLimitManager.registedNotification) == false{
            NotificationLimitManager.sharedInstance.registerLocalNotification()
        }
        
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        if let notificationStr = notification.userInfo!["key"] as? String
        { if notificationStr == MorningNotification.morning {
            AnaliticsManager.sendEvent(AnaliticsManager.notification_click, data: ["time":"morning"])
        } else if notificationStr == EveningNotification.evening {
            AnaliticsManager.sendEvent(AnaliticsManager.notification_click, data: ["time":"evening"])
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
        NotificationManager.sharedInstance.clearAndSetAll([.morning,.evening],duration:5)
        // })
        gIsgettingNotifStatus = false
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application sdiduppgiorts background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "AppDelegate.StartApp"), object: nil)
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppsFlyerTracker.shared().trackAppLaunch()
        application.cancelAllLocalNotifications()
        application.applicationIconBadgeNumber = 0
        FBSDKAppEvents.activateApp()
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings){
        if notificationSettings.types == UIUserNotificationType(){
            if  UserDefaults.standard.bool(forKey: "isFirstLaunch") == true && firstSendNotifAnaliticsEvent == true {
                firstSendNotifAnaliticsEvent = false
                AnaliticsManager.sendEvent(AnaliticsManager.grant_notification_permission,data: ["sign":"false"])
            }
            NotificationManager.sharedInstance.allowMorningNoti(false)
            NotificationManager.sharedInstance.allowEveningNoti(false)
            //post notif authorization
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationAuthorizationStatus), object: self, userInfo: [NotifAuthorizationStatusKey: false])
            UserDefaults.standard.set(false, forKey: PushNotificationAllowed)
            UserDefaults.standard.synchronize()
        }else{
            if  UserDefaults.standard.bool(forKey: "isFirstLaunch") == true && firstSendNotifAnaliticsEvent == true{
                firstSendNotifAnaliticsEvent = false
                AnaliticsManager.sendEvent(AnaliticsManager.grant_notification_permission,data: ["sign":"true"])
            }
            NotificationManager.sharedInstance.allowMorningNoti(true)
            NotificationManager.sharedInstance.allowEveningNoti(true)
            //post notif authorization
            NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationAuthorizationStatus), object: self, userInfo: [NotifAuthorizationStatusKey: true])
            UserDefaults.standard.set(true, forKey: PushNotificationAllowed)
            UserDefaults.standard.synchronize()
        }
    }
    
    func application(_ application: UIApplication,
                     open url: URL,
                             sourceApplication: String?,
                             annotation: Any) -> Bool {
        return self.openUrlHandle(application,
                                  openURL: url,
                                  sourceApplication: sourceApplication,
                                  annotation: annotation as AnyObject?)
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return self.openUrlHandle(application,
                                  openURL: url,
                                  sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                  annotation: options[UIApplicationOpenURLOptionsKey.annotation] as AnyObject)
    }
    
    fileprivate func openUrlHandle(_ application: UIApplication,
                               openURL url: URL,
                                       sourceApplication: String?,
                                       annotation: AnyObject?) -> Bool {
        var handled = false
        handled = handled || GIDSignIn.sharedInstance().handle(url,
                                                                  sourceApplication: sourceApplication,
                                                                  annotation: annotation)
        
        handled = handled || FBSDKApplicationDelegate.sharedInstance().application(application,
                                                                                   open: url,
                                                                                   sourceApplication: sourceApplication,
                                                                                   annotation: annotation)
        
        let parsedUrl = BFURL(inboundURL: url, sourceApplication: sourceApplication)
        handled = handled || ((parsedUrl?.appLinkData) != nil)
        return handled
    }
    
    var allowRotate:NSInteger?
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if self.allowRotate == 1 {
            return .all
        }else{
            return .portrait
        }
    }
    
    func shouldAutorotate() ->Bool{
        if self.allowRotate == 1{
            return true
        }else{
            return false
        }
    }
    
    // MARK: google login
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                withError error: Error!) {
        print("googleSign\(error)")
        if let error = error {
            g_toast(error.localizedDescription)
            return
        } else {
            // TODO: pass user
            if let navController = HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController {
                if let loginViewController = navController.topViewController as? HsLoginViewController {
                    loginViewController.showProccessHUD()
                }
            }
            AccountManager.sharedInstance.loginWithType(.googleUser
                ,                             viewcontroller: nil,
                                              data: user,
                                              logInSuccess: {
                                                if let navController = HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController {
                                                    if let loginViewController = navController.topViewController as? HsLoginViewController {
                                                        // log success
                                                        loginViewController.hideHUD()
                                                        let vc = ChooseHoroViewController()
                                                        vc.delegate=self
                                                        loginViewController.present(vc, animated: true, completion: {
                                                            
                                                        })
                                                    }
                                                }
            }) {
                if let navController = HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController {
                    if let loginViewController = navController.topViewController as? HsLoginViewController {
                        //Error
                        loginViewController.showErrorHUD()
                    }
                }
            }
        }
    }
    
    func chooseCompleteToRefresh() {
        if let navController = HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController {
            if let loginViewController = navController.topViewController as? HsLoginViewController {
                loginViewController.jumpToHome()
            }}
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
                withError error: Error!) {
        if let navController = HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController {
            if let loginViewController = navController.topViewController as? HsLoginViewController {
                loginViewController.showErrorHUD()
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

