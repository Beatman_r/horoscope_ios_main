
//
//  GlobalConstant.swift
//  Horoscope
//
//  Created by xiao  on 17/1/19.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

let NotificationAuthorizationStatus = "notificationAuthorizationStatus"
let NotifAuthorizationStatusKey = "notifAuthorizationStatusKey"
let PushNotificationAllowed = "pushNotificationAllowed"
let DailyPoPTimes = "dailyPoPTimes"
let ShowGuideNotification = "showGuideNotification"
let CommentViewClickToShowReplyView = "commentViewClickToShowReplyView"

let eveningNotiContent="eveningNotiContent"
let ReplyBtnClickToShowReplyController = "replyBtnClickToShowReplyController"
let MoreCommentBtnClick = "moreCommentBtnClick"
let ReportBtnClickToShowReportController = "reportBtnClickToShowReportController"
let SeeOriginalBtnClick = "seeOriginalBtnClick"
let CommentsMoreThanSeventyFloor = "commentsMoreThanSeventyFloor"
let LikeBtnClick = "likeBtnClick"
let DisLikeBtnClick = "disLikeBtnClick"
let UserClickToUserTopic = "userClickToUserTopic"
let ShowRateUsPageNoti = "ShowRateUsPageNoti"
let HomeIntertitialAdShowNoti = "HomeIntertitialAdShowNoti"//首页插页广告展示通知
		
let HomeIntertitialAd = "HomeIntertitialAdShowNoti"//首页插页广告展示通知
