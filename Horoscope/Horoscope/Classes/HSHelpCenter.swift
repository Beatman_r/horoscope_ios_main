//
//  HSHelpCenter.swift
//  Horoscope
//
//  Created by Wang on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class HSHelpCenter: NSObject {
    static let sharedInstance = HSHelpCenter()
    static let  baseContentFontName:String = ".SFUIDisplay-Light"
    static let  baseContentFontNameRegular:String=".SFUIDisplay-Regular"
    static let  baseContentFontNameMedium:String=".SFUIDisplay-Medium"
    
    static let  charterBoldItalicFontName:String = "Charter Bd BT"
    static let  charterBoldFontName:String="charter Bold"
    static let  charterItalicFontName:String="charter Italic"
    static let  charterRegulatFontName:String = "charter"
    
    let textTool:TextPartToolProtocol = TextTransactManager()
    let dateTool:DatePartToolProtocol = DateTransactManager.init()
    let appAndDeviceTool:AppInfoToolProtocol = AppAndDeviceBaseInfoManager()
 
}
