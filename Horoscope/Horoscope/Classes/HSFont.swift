//
//  HSFont.swift
//  Horoscope
//
//  Created by Wang on 16/12/1.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class HSFont: NSObject {

    class func baseContentFont() ->UIFont{
        return UIFont(name: HSHelpCenter.baseContentFontName,size: 18) ?? UIFont.systemFont(ofSize: 18)
    }
    class func baseLightFont(_ num:CGFloat) ->UIFont{
        return UIFont(name: HSHelpCenter.baseContentFontName,size: num) ?? UIFont.systemFont(ofSize: num)
    }
    class func baseTitleFont()->UIFont {
       return UIFont(name: HSHelpCenter.baseContentFontNameRegular,size: 20) ?? UIFont.systemFont(ofSize: 20)
    }
    
    class func baseMediumFont(_ num:CGFloat) ->UIFont{
        return UIFont(name: HSHelpCenter.baseContentFontNameMedium,size: num) ?? UIFont.systemFont(ofSize: num)
    }
    
    class func baseRegularFont(_ num:CGFloat) ->UIFont{
        return UIFont(name: HSHelpCenter.baseContentFontNameRegular,size: num) ?? UIFont.systemFont(ofSize: num)
    }
    
    class func baseLightRegularFont(_ num:CGFloat) ->UIFont{
        return UIFont(name: HSHelpCenter.baseContentFontName,size: num) ?? UIFont.systemFont(ofSize: num)
    }
    
    class func baseRateFont()->UIFont {
        return UIFont(name: HSHelpCenter.baseContentFontNameRegular,size: 22) ?? UIFont.systemFont(ofSize: 22)
    }
    
    class func charterBoldItalicFont(_ fontSize:CGFloat) ->UIFont{
      return UIFont(name: HSHelpCenter.charterBoldItalicFontName,size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    class func charterBoldFont(_ fontSize:CGFloat)->UIFont {
        return UIFont(name: HSHelpCenter.charterBoldFontName,size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    class func charterItalicFont(_ fontSize:CGFloat) ->UIFont{
        return UIFont(name: HSHelpCenter.charterItalicFontName,size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    class func charterRegularFont(_ fontSize:CGFloat) ->UIFont{
        return UIFont(name: HSHelpCenter.charterRegulatFontName,size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }

}
