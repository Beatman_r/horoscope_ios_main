//
//  LoginProtocol.swift
//  bibleverse
//
//  Created by Wang on 16/8/1.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

typealias returnsuccess = (_ userInfo: UserInfo) -> Void
typealias returnfalse = (_ errorInfo: String) -> Void

protocol LoginProtocol {
    func login(_ vc: UIViewController?)
    var loginSuccess: returnsuccess? { get set }
    var loginFailed: returnfalse? { get set }
}
