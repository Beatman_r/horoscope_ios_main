//
//  BaseViewController.swift
//  Horoscope
//
//  Created by Wang on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import MJRefresh
class BaseViewController: UIViewController,
UITableViewDelegate,
UITableViewDataSource,
RetryViewDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        createTableView()
        self.initFootView()
        self.registerCell()
        self.baseTableView?.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor.clear
     // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var baseTableView:UITableView?
    func createTableView() {
        self.baseTableView = UITableView(frame:self.view.bounds, style: .plain)
        self.view.addSubview(self.baseTableView!)
        self.baseTableView?.delegate = self
        self.baseTableView?.dataSource = self
        self.baseTableView?.separatorStyle = .none
        self.baseTableView?.backgroundColor = UIColor.clear
        self.baseTableView?.showsVerticalScrollIndicator = false
        self.baseTableView?.estimatedRowHeight = 0
        self.baseTableView?.estimatedSectionHeaderHeight = 0
        self.baseTableView?.estimatedSectionFooterHeight = 0
    }
    
    func setupRefreshHeaderView() {
        if #available(iOS 11.0, *) {
            self.baseTableView?.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
            self.automaticallyAdjustsScrollViewInsets = false
        }
        let header = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(refreshData))
        header?.setTitle(NSLocalizedString("Pull up to load", comment: ""), for: .idle)
        header?.setTitle(NSLocalizedString("Release to refresh", comment: ""), for: .pulling)
        header?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        header?.lastUpdatedTimeLabel.isHidden = true
        self.baseTableView?.mj_header = header
    }
    
    func setupRefreshFooterView() {
        let footer = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreList))
        footer?.isAutomaticallyRefresh=false
        footer?.setTitle("", for: .idle)
        footer?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        self.baseTableView?.mj_footer=footer
    }
    
    @objc func loadMoreList() {
        
    }
    
    @objc func refreshData() {
        
    }
    
    var failedRetryView:RetryView?
    func addRetryView() {
        failedRetryView=RetryView.init(frame: self.view.bounds)
        failedRetryView?.delegate=self
        self.view.addSubview(failedRetryView!)
    }
    
    func retryToLoadData() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    fileprivate func initFootView(){
        let footView = UIView()
        footView.frame = CGRect(x: 0, y: 0, width: 0, height: 8)
        footView.backgroundColor=UIColor.clear
        self.baseTableView?.tableFooterView = footView
    }
    
    func loadData() {
        
    }
 
    func registerCell() {
        
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */

}
