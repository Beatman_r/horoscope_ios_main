//
//  TarotBannerView.swift
//  Horoscope
//
//  Created by Wang on 16/12/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class TarotBannerView: BaseOperationView {
    
    
    weak var rootVc:UIViewController?
    class func create(_ vc:UIViewController) ->TarotBannerView{
        let view = TarotBannerView()
        view.rootVc=vc
        view.editUI()
        view.addConstrains()
        view.tapAction()
        view.UISetting()
         return view
    }
    
    func tapAction() {
        let tap=UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(self.onSelectedCurrentView))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled=true
    }
    
    @objc func onSelectedCurrentView() {
        AnaliticsManager.sendEvent(AnaliticsManager.forecast_banner_click, data: ["sign":"tarot"])
        let tarotVC = TarotEnterController()
        self.rootVc?.navigationController?.pushViewController(tarotVC, animated: true)
    }
    
    fileprivate func UISetting()  {
        self.img.image=UIImage(named:"tarotBanner.jpg")
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */


}
