//
//  SettingTableViewCell.swift
//  Horoscope
//
//  Created by Beatman on 16/11/30.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class SettingAboutCell: BaseCardCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       
        self.cornerBackView?.addSubview(titleLab)
        self.cornerBackView?.addSubview(contentLab)
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.contentView.snp.right).offset(-BaseCardCell.cellPadding)
            make.top.equalTo(self.contentView.snp.top).offset(BaseCardCell.cellPadding)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        self.cornerBackView?.backgroundColor = UIColor.clear
        self.cornerBackView?.addSubview(spreadLine)
        self.addConstrains()
    }
    
    func addConstrains() {
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.top.equalTo(self.cornerBackView!.snp.top).offset(35)
            make.bottom.equalTo(self.cornerBackView!.snp.bottom).offset(-8)
        }
        
        spreadLine.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.cornerBackView!.snp.bottom)
            make.left.equalTo(contentLab.snp.left)
            make.right.equalTo(contentLab.snp.right)
            make.height.equalTo(1)
        }
    }
    
    let contentLab:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = UIColor.commonPinkColor()
        label.font=HSFont.baseContentFont()
        return label
    }()
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 1
        lab.textAlignment = .left
        lab.font=HSFont.baseRegularFont(21)
        lab.textColor = UIColor.commonPinkColor()
        lab.frame = CGRect(x: 12, y: 0, width: 200, height: 31)
        return lab
    }()
    
    let spreadLine:UIView = {
       let v = UIView()
       v.backgroundColor = UIColor.commentTimeColor()
       v.alpha=0.7
       return v
    }()
    
    func renderCell(_ title:String,content:String) {
         self.titleLab.text = title
         self.contentLab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.commonPinkColor(), targetStr: content, font: HSFont.baseRegularFont(18))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK:Create
    static let cellId = "SettingAboutCell"
    class func dequeueReusableCell(_ tableView: UITableView,
                                   cellForRowAtIndexPath indexPath: IndexPath) -> SettingAboutCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId,
                                                               for: indexPath) as! SettingAboutCell
        return cell
    }
    
    class func calculateHeight(_ content:String) ->CGFloat{
        return HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content, fontSize: 16, font: HSFont.baseContentFont(), width: UIScreen.main.bounds.size.width-40, multyplyLineSpace: 1.25)+16+31+8+4
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
  
}
