//
//  EveningNotification.swift
//  Horoscope
//
//  Created by Wang on 16/12/1.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class EveningNotification:BaseNotification,NotificationProtocol {
 
    var datefatter:DateFormatter?
    static let evening:String = "evening"
    func createNotification(_ duration:Int) {
        let notiDefault = UserDefaults.standard
        let ifMorningHasNoti = notiDefault.bool(forKey: "\(NotificationType.evening)")
        if  ifMorningHasNoti == true{
            
        }else if ifMorningHasNoti == false{
            return
        }
        
        for index in 0...duration {
            let notification=UILocalNotification()
            let datefatter=DateFormatter()
            let randomDura = self.random20min()
            let fireDate = calcFireTime(index, pushTime: 19*60*60+randomDura)
            datefatter.dateFormat="yyyyMMdd"
            let dateParamStr = datefatter.string(from: fireDate)
            if #available(iOS 8.2, *) {
                notification.alertTitle=NSLocalizedString("Horoscope", comment: "") + " " + HSHelpCenter.sharedInstance.dateTool.getFormatedDateString(dateParamStr)
            } else {
            }
            notification.fireDate = fireDate
            notification.timeZone = TimeZone.current
            notification.applicationIconBadgeNumber = 1
            notification.soundName = UILocalNotificationDefaultSoundName
            if index == 0{
                notification.alertBody = EveningNotification.getTomorrowNotiContent()
            }else{
                notification.alertBody = NSLocalizedString("Good Evening! What are going to do tomorrow? Check out Tomorrow's Horoscope Forecast!", comment: "")
            }
            notification.repeatInterval = NSCalendar.Unit.year
            var userInfo:[AnyHashable: Any] = [AnyHashable: Any]()
            userInfo["key"] = EveningNotification.evening
            notification.userInfo = userInfo
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    fileprivate func random20min()->Double {
        var rand_int:Int = arc4random().hashValue
        rand_int=rand_int%1200
        rand_int=rand_int-600
        return Double(rand_int)
    }
    
    fileprivate let dao = HoroscopeDAO()
    fileprivate var horoName : String?
    //= ZodiacRecordManager.getSelectedZodiac().first?.lowercaseString
    fileprivate var note:String?
   
    fileprivate func getNotificationAlertBody(_ paramDate:Date,success:(_ note:String)->())   {
//        self.datefatter?.dateFormat = "yyyyMMdd"
//        let dateStr = self.datefatter?.stringFromDate(paramDate)
//        dao.getHoroscopeOfDate(horoName ?? "", dateOffset: 1, date: dateStr ?? "", success: { (model) in
//            print("明日运势\(model.content)")
//            self.note = model.content
//            success(note:model.content!)
//            }) { (error) in
//            print("错误\(error)")
//        }
    }
    
    class func saveTomorrowNotiContent(_ content:String){
        UserDefaults.standard.setValue(content, forKey: eveningNotiContent)
        UserDefaults.standard.synchronize()
    }
    
    class func getTomorrowNotiContent() -> String{
        if let noti = UserDefaults.standard.value(forKey: eveningNotiContent) as? String{
            return noti 
        }
        else {
            return ""
        }
    }
    
    
    func addNotification(_ durationTime:Int) {
          createNotification(durationTime)
    }
    
    init(dateformatter:DateFormatter) {
        self.datefatter=dateformatter
        self.horoName = ZodiacRecordManager.getSelectedZodiac().first?.lowercased()
       
    }
    
}
