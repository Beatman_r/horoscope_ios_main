//
//  MatchDetailController.swift
//  Horoscope
//
//  Created by Beatman on 16/11/28.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class MatchDetailController: UIViewController,UITableViewDelegate,UITableViewDataSource,NotificationGuideDelegate {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    let naviHeight = UINavigationController().navigationBar.bounds.height
    let statuBarHeight = UIApplication.shared.statusBarFrame.height
    
    var leftImageView : UIImageView!
    var rightImageView : UIImageView!
   // var topBgImg : UIImageView!
    var contentString : String = ""
    
    var leftZodiacName : String = ""
    var rightZodiacName : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnaliticsManager.sendEvent(AnaliticsManager.match_show, data: ["signs":"\(leftZodiacName)-\(rightZodiacName)"])
        self.title = NSLocalizedString("Match", comment: "")
        self.view.backgroundColor = UIColor.baseDetailBackgroundColor()
        setupMatchDetail()
        setHeaderView()
        addBackBtn()
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    @objc func popView() {
        var backCount:Int = UserDefaults.standard.integer(forKey: "backCount")
        backCount += 1
        UserDefaults.standard.set(backCount, forKey: "backCount")
        UserDefaults.standard.synchronize()
        if UserDefaults.standard.bool(forKey: "hasRate") == false && UserDefaults.standard.integer(forKey: "rateUsShowCount") < RateUsFullView.getRateUsLimitShowCount() && UserDefaults.standard.integer(forKey: "backCount")%2 == 0{
            NotificationCenter.default.post(name: Notification.Name(rawValue: "matchShowRateUsView"), object: nil)
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func readyToPop() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    var imageHeaderView:partOneView?
    func setHeaderView(){
        imageHeaderView = partOneView.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: self.screenWidth * 0.53))
        imageHeaderView?.backImageView.image = UIImage(named:"topBgImg.jpg")
        self.matchTableView?.tableHeaderView=imageHeaderView
        leftImageView = MatchTopImageView.init(frame: CGRect.zero, imageName: (ZodiacModel.getHoroscopeSquareImgNameWithName(leftZodiacName)) , hasChosen: false)
        
        rightImageView = UIImageView()
        rightImageView = MatchTopImageView.init(frame: CGRect.zero, imageName: (ZodiacModel.getHoroscopeSquareImgNameWithName(rightZodiacName)), hasChosen: false)
    
        self.imageHeaderView?.addSubview(leftImageView)
        self.imageHeaderView?.addSubview(rightImageView)
        
        self.leftImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.imageHeaderView!.snp.centerY)
            make.right.equalTo(self.imageHeaderView!.snp.centerX).offset(-45)
            make.width.equalTo(self.screenWidth * 120/375)
            make.height.equalTo(self.screenHeight * 120/667)
        }
        
        self.rightImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.leftImageView.snp.centerY)
            make.left.equalTo(self.imageHeaderView!.snp.centerX).offset(45)
            make.width.equalTo(self.screenWidth * 120/375)
            make.height.equalTo(self.screenHeight * 120/667)
        }
    }
    
    var matchTableView:UITableView?
    func setupMatchDetail() {
        //refactor: too long expression
        self.matchTableView = UITableView(frame:self.view.bounds, style: .plain)
        
        matchTableView?.delegate = self
        matchTableView?.dataSource = self
        matchTableView?.backgroundColor = UIColor.clear
        matchTableView?.separatorStyle = .none
        matchTableView?.register(MatchDetailCell.self, forCellReuseIdentifier: "matchCell")
        self.view.addSubview(matchTableView!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchCell", for: indexPath) as! MatchDetailCell
        cell.renderCell(self.contentString)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        refactor: too long expression
        return MatchDetailCell.calculateHeight(self.contentString)
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
