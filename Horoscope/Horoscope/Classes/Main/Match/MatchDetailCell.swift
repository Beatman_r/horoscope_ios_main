//
//  MatchDetailCell.swift
//  Horoscope
//
//  Created by Beatman on 16/11/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

class MatchDetailCell:BaseCardCell {
    let contentLabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-16, height: UIScreen.main.bounds.height))
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func renderCell(_ text:String)  {
         self.contentLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor() , targetStr: text , font: HSFont.baseLightFont(18))
     }
    
      func createUI() {
      //refactor: move  cotentLabel code out, why? see the API document
        self.contentLabel.layer.cornerRadius = 8
        self.contentLabel.layer.masksToBounds = true
        self.contentLabel.layer.borderWidth = 0
        self.contentLabel.numberOfLines = 0
        self.contentLabel.font = HSFont.baseLightFont(18)
        self.contentLabel.textAlignment = NSTextAlignment.left
        self.contentLabel.textColor = UIColor.luckyPurpleTitleColor()
        self.cornerBackView?.addSubview(contentLabel)
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        contentLabel.snp.makeConstraints { (make) in
            make.top.equalTo(cornerBackView!.snp.top).offset(20)
            make.left.equalTo(cornerBackView!.snp.left).offset(12)
            make.bottom.equalTo(cornerBackView!.snp.bottom).offset(-12)
            make.right.equalTo(cornerBackView!.snp.right).offset(-12)
        }
    }
    
    class func calculateHeight(_ content:String)->CGFloat{
        let font = HSFont.baseLightFont(18)
        return HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font:font, width: UIScreen.main.bounds.size.width-22, multyplyLineSpace: 1.25)+16+16
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
