//
//  MatchViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/11/23.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

class MatchViewController: BaseHoroCollectionViewController {
      var leftImageView : MatchTopImageView?
      var rightImageView : MatchTopImageView?
      var leftZodiacName:String = ""
      var rightZodiacName : String = ""
    
      override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Match"
        self.view.backgroundColor = UIColor.whiteColor()
 
        setupMatchUI()
        let notiCenter = NSNotificationCenter.defaultCenter()
        notiCenter.addObserver(self, selector: #selector(showRateUsView), name: "matchShowRateUsView", object: nil)
        self.type = .MatchTab
        laodAd(AdPlacementKey.MatchList1.rawValue, placementkey2: AdPlacementKey.MatchList2.rawValue)
        self.adposition1 = 0
        self.adposition2 = 7
        horoCollectionView.frame.origin.y = screenHeight*0.22+naviHeight+statuBarHeight+17
        horoCollectionView.frame.size.height = screenHeight-(screenHeight*0.22+naviHeight+statuBarHeight+17)-49
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.zodiacDidChange(_:)), name: ZodiacDidChange, object: nil)
        }
    
    func zodiacDidChange(notification:NSNotification)  {
        let nameArray = notification.userInfo?["selectArray"] as? [String]
        LXSWLog(nameArray)
        
    }
    func setupMatchUI() {
        leftImageView = MatchTopImageView.init(frame: CGRectMake((screenWidth/2 - screenWidth*0.28)/2, screenHeight*0.22-screenWidth*0.28+naviHeight+statuBarHeight, screenWidth*0.28, screenWidth*0.28), imageName: "match_1.png", hasChosen: false)
        
        rightImageView = MatchTopImageView.init(frame: CGRectMake(screenWidth - (screenWidth/2 - screenWidth*0.28)/2 - screenWidth*0.28, screenHeight*0.22-screenWidth*0.28+naviHeight+statuBarHeight, screenWidth*0.28, screenWidth*0.28), imageName: "match_2.png", hasChosen: false)
        
        leftImageView!.image = UIImage(named: "match_1.png")
        rightImageView!.image = UIImage(named: "match_2.png")
        
        let centerLabel = UILabel()
        centerLabel.frame = CGRectMake(screenWidth/2-screenWidth*0.14, screenHeight*0.12+naviHeight+statuBarHeight, screenWidth*0.27, screenHeight*0.045)
        centerLabel.text = NSLocalizedString("And", comment: "")
        centerLabel.textAlignment = NSTextAlignment.Center
        centerLabel.font = UIFont(name: HSHelpCenter.baseContentFontNameRegular, size: 24)
        centerLabel.font = UIFont.systemFontOfSize(24)
        centerLabel.textColor = UIColor.hsTextColor(1)
        self.view.addSubview(leftImageView!)
        self.view.addSubview(rightImageView!)
        self.view.addSubview(centerLabel)
     }

    func showRateUsView() {
        let rateUsView = RateUsFullView.init(frame: UIScreen.mainScreen().bounds)
        rateUsView.rootvc = self
        self.view.addSubview(rateUsView)
        var showCount = NSUserDefaults.standardUserDefaults().integerForKey("rateUsShowCount")
        showCount += 1
        NSUserDefaults.standardUserDefaults().setInteger(showCount, forKey: "rateUsShowCount")
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
