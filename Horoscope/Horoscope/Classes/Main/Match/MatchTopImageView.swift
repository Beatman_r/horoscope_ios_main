//
//  MatchTopImageView.swift
//  Horoscope
//
//  Created by Beatman on 16/12/6.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class MatchTopImageView: UIImageView {
    
    var hasChosen : Bool = false
    
    init(frame: CGRect,imageName: String,hasChosen: Bool) {
        super.init(frame: frame)
        self.image = UIImage(named: imageName)
        self.hasChosen = hasChosen
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
