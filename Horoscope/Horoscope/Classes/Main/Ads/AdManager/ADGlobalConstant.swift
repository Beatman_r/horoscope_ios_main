//
//  ADGlobalConstant.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/22.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

// ad origin
enum AdOrigin:String {
    case Admob = "admob"
    case Fb = "fb"
}

// ad placement key
enum AdPlacementKey:String {
    // interstitial ad
    case SplashAdV2 = "splashv2"
    case PageSwitch = "pageSwitch"
    case MenuBible = "menuBibleV2"
    case HomeInterstitial_002 = "HomeInterstitial_002" //从My fortune页面返回到首页展示的自定义广告位ID
    // inner ad
    case DevotionalBread = "devotionalBread"
    case Result = "result"
    case BreadRecommend = "breadRecommend"
    case DailyTimeline1 = "dailyTimeline1"
    case DailyTimeline2 = "dailyTimeline2"
    case VodSeeAll1 = "vodSeeAll1"
    case VodSeeAll2 = "vodSeeAll2"
    case FollowMain1 = "followMain1"
    case FollowMain2 = "followMain2"
    case FollowAuthor1 = "followAuthor1"
    case FollowAuthor2 = "followAuthor2"
    case DevotionMain1 = "devotionMain1"
    case HomeTrigger = "homeTrigger"
    case BreadPreview = "breadPreview"
    case BibleTab = "bibleTab"
    case ChallengeWrong = "challengeWrong"
    case ChallengeRight = "challengeRight"
    case dailyCard1 = "dailyCard1"
    case todayRect = "todayRect"
    
    case MyFortuneNative_003 = "myFortuneNative_003" //今日运势广告（拼接的Native）
}

enum AdType:String {
    // inner ad
    case Rect = "rect"
    case VerseAdvance = "verseAdvance"
    case NativeCoverOnTop = "nativeCoverOnTop"
    case NativeResult = "nativeResult"
    case NativeHeight100 = "nativeHeight100"
    case BreadRecommendAdvance = "breadRecommendAdvance"
    case BreadRecommendNative = "breadRecommendNative"
    case AdmobAdvancedTimeline = "admobAdvancedTimeline"
    case FbNativeTimeline = "fbNativeTimeline"
    case AdmobAdvancedVodSeeAll = "admobAdvancedVodSeeAll"
    case FbNativeVodSeeAll = "fbNativeVodSeeAll"
    case AdmobAdvancedFollowMain = "admobAdvancedFollowMain"
    case FbNativeFollowMain = "fbNativeFollowMain"
    case AdmobAdvancedFollowAuthor = "admobAdvancedFollowAuthor"
    case FbNativeFollowAuthor = "fbNativeFollowAuthor"
    case AdmobAdvancedDevotionMain = "admobAdvancedDevotionMain"
    case FbNativeDevotionMain = "fbNativeDevotionMain"
    case AdmobAdvancedBreadPreview = "admobAdvancedBreadPreview"
    case FbNativeBreadPreview = "fbNativeBreadPreview"
    case AdmobAdvancedBibleTab = "admobAdvancedBibleTab"
    case FbNativeBibleTab = "fbNativeBibleTab"
    case FbNativeChallengeWrong = "fbNativeChallengeWrong"
    case AdmobAdvancedChallengeWrong = "admobAdvancedChallengeWrong"
    // interstitial ad
    case SplashAdvance = "splashAdvance"
    case SplashNative = "splashNative"
    case Interstitial = "interstitial"
    // innerAD
    case fbNativeDailyCardAd = "fbNativeDailyCardAd"
    case admobNativeDailyCardAd = "admobNativeDailyCardAd"
    case admobAdvancedVerse = "admobAdvancedVerse"
    case native = "native"
}

// ad cache key
let adStatusChange = "adStatusChange"
let adSpace:CGFloat = 13
