//
//  AdsDataDao.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/21.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class AdsDataDao: NSObject {
    
    var adsModels = [PlacementAdsModel]()
    
    func getAdsConfig(_ placementKey:String) -> [AdsDataModel] {
        var optionAdUnits:[AdsDataModel] = [AdsDataModel]()
        var detailAds:[PlacementAdsModel] = self.adsModels.filter({ (b) -> Bool in
            return (b.placementKey == placementKey) && (b.placementEnable == "true")
        })
        detailAds=detailAds.map { (b) -> PlacementAdsModel in
            return b
        }
        if detailAds.count > 0 {
            let detailAd:PlacementAdsModel? = detailAds.first
            let optionAd:[AdsDataModel] = detailAd!.optionAdUnits
            optionAdUnits = optionAd.sorted(by: { (object1, object2) -> Bool in
                return object1.adUnitPriority > object2.adUnitPriority
            })
        }
        return optionAdUnits
    }
    
    override init() {
        super.init()
        let userType = ConfigManager.shareInstance.adConfigType
        let adConfig = ConfigManager.shareInstance.getConfig(userType)
        LXSWLog("adConfig----\(adConfig)")
        self.analysisData(adConfig)
        ConfigManager.shareInstance.addObserver(userType) { (data) in
            self.analysisData(data)
            IntertitialAdManager.shareInstance.loadInterAd()
        }
    }
    
    fileprivate func analysisData(_ config: [String:AnyObject]) {
        if let placementConfig = config["placementAdsConfig"] as? [[String: AnyObject]] {
            self.adsModels = placementConfig.map({ (item:[String : AnyObject]) -> PlacementAdsModel in
                let model = PlacementAdsModel(dic: item)
                return model
            })
        }
    }
}
