//
//  LTVManager.swift
//  bibleverse
//
//  Created by 麦子 on 2017/2/16.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit

class LTVManager: NSObject {
    
    static let shareInstance = LTVManager()
    
    func adClick(_ model:SingleAdInfo?) {
        _ = getAdKey("adclick", platform: model?.adUnitPlatform ?? "", placement: model?.placementKey ?? "")
        _ = getAdParams(model)
    }
    
    func adImpression(_ model:SingleAdInfo?) {
        _ = getAdKey("adimp", platform: model?.adUnitPlatform ?? "", placement: model?.placementKey ?? "")
        _ = getAdParams(model)
    }
    
    func getAdKey(_ orgin:String, platform:String, placement:String) -> String {
        let resutlt = orgin + "_" + placement + "_" + platform
        return resutlt
    }
    
    func getAdParams(_ model:SingleAdInfo?) -> [String: AnyObject] {
        var dic = [String: AnyObject]()
        dic["adPlatform"] = model?.adUnitPlatform as AnyObject
        dic["adUnitid"] = model?.adUnitId as AnyObject
        dic["regTime"] = "" as AnyObject?
        dic["adPlacement"] = model?.placementKey as AnyObject
        dic["adType"] = model?.adUnitType as AnyObject
        return dic
    }
}
