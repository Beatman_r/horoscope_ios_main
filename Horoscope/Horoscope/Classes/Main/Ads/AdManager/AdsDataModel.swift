//
//  AdsDataModel.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import SwiftyJSON

class AdsDataModel: NSObject {
    
    var adUnitId: String?
    var adUnitPlatform: String?
    var adUnitPriority: Int?
    var adUnitType: String?
    
    init(dic: [String: AnyObject]) {
        super.init()
        adUnitId = dic["adUnitId"] as? String
        adUnitPlatform = dic["adUnitPlatform"] as? String
        adUnitPriority = dic["adUnitPriority"] as? Int
        adUnitType = dic["adUnitType"] as? String
    }
    
    init(json:JSON) {
        super.init()
        adUnitId = json["adUnitId"].string
        adUnitPlatform = json["adUnitPlatform"].string
        adUnitPriority = json["adUnitPriority"].int
        adUnitType = json["adUnitType"].string
    }
}
