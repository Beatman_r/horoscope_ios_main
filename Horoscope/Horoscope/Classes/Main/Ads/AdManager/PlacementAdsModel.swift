//
//  PlacementAdsModel.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/27.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class PlacementAdsModel: NSObject {
    
    var optionAdUnits = [AdsDataModel]()
    var placementEnable: String?
    var placementKey: String?
    
    init(dic: [String:AnyObject]) {
        if let optionAdUntis = dic["optionAdUnits"] as? [[String: AnyObject]] {
            for item in optionAdUntis {
                let model = AdsDataModel(dic: item)
                self.optionAdUnits.append(model)
            }
        }
        self.placementKey = dic["placementKey"] as? String
        self.placementEnable = dic["placementEnable"] as? String
    }
}
