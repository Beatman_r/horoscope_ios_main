//
//  AdsManager.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/21.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds
import FBAudienceNetwork

class AdsManager: NSObject {
    
    // MARK:Let Attribute
    static let shareInstance = AdsManager()
    
    // FIXME: rename kNotNewBible to kNotNewbie
    fileprivate let kNotNewbie:String = "NewUserAdStrategy.kNotNewbie"
    fileprivate let kNewbieTime:String = "NewUserAdStrategy.kNewbieIime"
    fileprivate var notNewbie:Bool = false
    
    // MARK:Config Attribute
    var countryConfig:Bool?
    var donotShowAdFirstTime:Bool?
    var interAdSequence:[String]?
    var interAdShowDuration:Int?
    var pageSwitchCount:Int?
    var splashAdWaitTime:Int?
    var webViewAdShowTime:Int?
    var newUserRemoveAdsDuration:Int?
    
    var lastAdShowTime:TimeInterval = 0
    
    // MARK:Event Attribute
    let adsDao: AdsDataDao = AdsDataDao()
    
    override init() {
        super.init()
        let userDefault = UserDefaults.standard
        let userType = ConfigManager.shareInstance.adConfigType
        let adConfig = ConfigManager.shareInstance.getConfig(userType)
        self.analysisData(adConfig)
//        PurchaseTypeManager.shareInstance.delegate = self
//        ConfigManager.shareInstance.addObserver(userType) { (data) in
//            self.analysisData(data)
//        }
        self.notNewbie = userDefault.bool(forKey: self.kNotNewbie) 
        if self.notNewbie == false {
            userDefault.set(true, forKey: self.kNotNewbie)
            let now = Date()
            userDefault.set(now.timeIntervalSince1970, forKey: self.kNewbieTime)
        }
    }
    
    func canShowIntertitialAd() -> Bool {
        if countryConfig == false {
            return false
        }
//        if PurchaseTypeManager.shareInstance.shouldShowAd() == false {
//            return false
//        }
        if self.isNewUser() == true {
            return false
        }
        let currentTime = Date().timeIntervalSince1970
        if (currentTime - lastAdShowTime)*1000 < Double(DataTypeUtil.isNull(interAdShowDuration)) {
            return false
        }
        if IntertitialAdManager.shareInstance.isAdShowing == true {
            return false
        }
        return true
    }
    
    func canShowInnerAd() -> Bool {
        if self.countryConfig == false {
            return false
        }
//        if PurchaseTypeManager.shareInstance.shouldShowAd() == false {
//            return false
//        }
        return true
    }
    
    func isNewUser() -> Bool {
        if self.notNewbie == false && self.donotShowAdFirstTime == true {
            let userDefault = UserDefaults.standard
            let newBibleTime:Double = userDefault.double(forKey: self.kNewbieTime) 
            let effectTime:Double = Double(self.newUserRemoveAdsDuration ?? 180000) / 1000
            let nowTime:Double = Date().timeIntervalSince1970
            if nowTime - newBibleTime < effectTime {
                return true
            }
        }
        return false
    }
    
    func statusChanged(_ purchased:Bool) {
        if purchased == true {
            NotificationCenter.default.post(name: Notification.Name(rawValue: adStatusChange), object: nil)
        }
    }
    
    fileprivate func analysisData(_ adConfig:[String:AnyObject]) {
        let local:String = Locale.current.identifier.components(separatedBy: "_").first ?? ""
        if let ads = adConfig["adConfigCountryBlackList"] as? [String:AnyObject] {
            if let hasAd = ads[local] {
                self.countryConfig = hasAd.boolValue
            } else {
                self.countryConfig = ads["default"]?.boolValue
            }
        }
        if let ads:[String:AnyObject] = adConfig["globalAdsConfig"] as? [String:AnyObject] {
            self.donotShowAdFirstTime = ads["donotShowAdFirstTime"] as? Bool
            self.interAdSequence = (ads["interAdSequence"] as? String)?.components(separatedBy: ",")
            self.interAdShowDuration = ads["interAdShowDuration"] as? Int
            self.pageSwitchCount = ads["pageSwitchCount"] as? Int
            self.splashAdWaitTime = ads["splashAdWaitTime"] as? Int
            self.newUserRemoveAdsDuration = ads["newUserRemoveAdsDuration"] as? Int
            self.webViewAdShowTime = ads["webViewAdShowTime"] as? Int
        }
    }
}
