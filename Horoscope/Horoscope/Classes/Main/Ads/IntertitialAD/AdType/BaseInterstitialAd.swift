//
//  BaseInterstitialAd.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

typealias InterAdLoaded = (_ adView:UIView, _ isSuccess:Bool) -> Void

class BaseInterstitialAd: NSObject {
    
    // MARK: Public Attribute
    var adInfo:SingleAdInfo?
    var hasAd:Bool?
    var adLogo:UIImage?
    var isLoading:Bool?
    
    // MARK: Public Method
    func requestAd(_ fromVc:UIViewController?) {
        
    }
    
    func showAd(_ fromVc:UIViewController) -> Bool {
        return false
    }
}
