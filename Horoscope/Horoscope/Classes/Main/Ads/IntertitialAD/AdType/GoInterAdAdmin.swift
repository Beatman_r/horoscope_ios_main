//
//  GoInterAdAdmin.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/23.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

typealias AdmobAdShowSuccess = (_ admin:GoInterAdAdmin, _ error:Bool) -> ()

class GoInterAdAdmin: BaseInterstitialAd,GADInterstitialDelegate {
    
    var admobInter:GADInterstitial?
    var admobInterBlock:AdmobAdShowSuccess?
    
    init(adInfo:SingleAdInfo, success:@escaping AdmobAdShowSuccess) {
        super.init()
        self.adInfo = adInfo
        self.admobInterBlock = success
        self.hasAd = false
        self.requestAd(nil)
    }
    
    override func requestAd(_ fromVc:UIViewController?) {
        LXSWLog("#### GAD AD Start Load")
        let adId:String = self.adInfo?.adUnitId ?? ""
        admobInter = GADInterstitial(adUnitID: adId)
        if let admobInter =  self.admobInter {
            admobInter.delegate = self
            let request = GADRequest()
            admobInter.load(request)
            self.isLoading = true
        }
    }
    
    override func showAd(_ fromVc:UIViewController) -> Bool {
        if self.admobInter != nil && self.admobInter?.isReady == true {
            self.admobInter?.present(fromRootViewController: fromVc)
            return true
        } else {
            return false
        }
    }
    
    // MARK: GADInterstitialDelegate
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        LXSWLog("#### GAD AD Success")
        self.isLoading = false
        self.hasAd = true
        self.admobInterBlock?(self, false)
    }
    
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("##### GAD Fail \(error)")
        self.admobInter = nil
        self.isLoading = false
        self.admobInterBlock?(self, true)
    }
    
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        LXSWLog("#### GAD Present: \(self.adInfo?.adUnitId ?? "")")
        IntertitialAdManager.shareInstance.isAdShowing = true
        if self.adInfo?.placementKey == AdPlacementKey.HomeInterstitial_002.rawValue{
            AnaliticsManager.sendEvent(AnaliticsManager.back_ad_splash, data: ["a1_ad_show":"admob"])
        }
        LTVManager.shareInstance.adImpression(adInfo)
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        IntertitialAdManager.shareInstance.isAdShowing = false
    }
    
    //optimize the compilation by haoxudong
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        LTVManager.shareInstance.adClick(adInfo)
        let adUnitPlatform = DataTypeUtil.isNull(self.adInfo?.adUnitPlatform)
        let placementKey = DataTypeUtil.isNull(self.adInfo?.placementKey)
        let adUnitType = DataTypeUtil.isNull(self.adInfo?.adUnitType)
        let contentId = adUnitPlatform + "_" + placementKey
        let contentType = adUnitPlatform + "_" + adUnitType
        if self.adInfo?.placementKey == AdPlacementKey.HomeInterstitial_002.rawValue{
            AnaliticsManager.sendEvent(AnaliticsManager.back_ad_splash, data: ["a2_ad_click":"admob"])
        }
        AnaliticsManager.logPurchase(1, currency: "USD", contentId: contentId, contentType: contentType)
    }
}
