//
//  NativeAdAdmin.swift
//  bibleverse
//
//  Created by 麦子 on 2016/12/9.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

typealias NativeAdShowSuccess = (_ admin:NativeAdAdmin, _ error:Bool) -> ()

class NativeAdAdmin: BaseInterstitialAd {
    
    var nativeInter: SplashNativeAd?
    var bgView:UIView?
    var admobInterBlock: NativeAdShowSuccess?
    
    init(adInfo:SingleAdInfo, success:@escaping NativeAdShowSuccess) {
        super.init()
        self.adInfo = adInfo
        self.admobInterBlock = success
        self.hasAd = false
        self.requestAd(nil)
    }
    
    override func requestAd(_ fromVc:UIViewController?) {
        LXSWLog("#### GAD AD Start Load")
        nativeInter = SplashNativeAd.create()
        self.isLoading = true
        nativeInter?.requestAd(adInfo)
        nativeInter?.addObserver({ (adView, isSuccess) in
            self.isLoading = false
            if isSuccess == true {
                self.hasAd = true
                self.adLogo = self.nativeInter?.adIcon.image
                self.admobInterBlock?(self, false)
            } else {
                self.admobInterBlock?(self, true)
            }
        })
    }
    
    override func showAd(_ fromVc:UIViewController) -> Bool {
        if self.hasAd == true && self.nativeInter != nil {
            self.bgView = UIView()
            self.bgView?.isUserInteractionEnabled = true
            self.bgView?.backgroundColor = UIColor(red: 165/255.0, green: 212/255.0, blue: 121/255.0, alpha: 1)
            fromVc.view.addSubview(self.bgView!)
            self.bgView?.snp.makeConstraints({ (make) in
                make.edges.equalTo(fromVc.view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
            })
            self.nativeInter?.showAd(bgView!, fromVC: fromVc)
            let closeBtn = UIButton()
            self.bgView?.addSubview(closeBtn)
            closeBtn.snp.makeConstraints { (make) in
                make.left.equalTo(self.bgView!)
                make.top.equalTo(self.bgView!).offset(20)
                make.size.equalTo(CGSize(width: 60, height: 60))
            }
            self.bgView?.alpha = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.bgView?.alpha = 1
            })
            closeBtn.addTarget(self, action: #selector(closeBtnClick), for: .touchUpInside)
            return true
        } else {
            return false
        }
    }
    
    @objc func closeBtnClick() {
        UIView.animate(withDuration: 0.5, animations: {
            self.bgView?.alpha = 0
        }, completion: { (bool) in
            self.bgView?.removeFromSuperview()
        }) 
    }
}
