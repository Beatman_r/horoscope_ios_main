//
//  AdvanceAdAdmin.swift
//  bibleverse
//
//  Created by 麦子 on 2016/12/9.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

typealias AdvanceAdShowSuccess = (_ admin:AdvanceAdAdmin, _ error:Bool) -> ()

class AdvanceAdAdmin: BaseInterstitialAd {
    
    var advanceInter: SplashAdvanceAd?
    var admobInterBlock: AdvanceAdShowSuccess?
    var height: CGFloat = 0
    var width: CGFloat = 0
    
    init(adInfo:SingleAdInfo, success:@escaping AdvanceAdShowSuccess) {
        super.init()
        self.adInfo = adInfo
        self.admobInterBlock = success
        self.hasAd = false
        self.requestAd(nil)
    }
    
    override func requestAd(_ fromVc:UIViewController?) {
        LXSWLog("#### GAD AD Start Load")
        advanceInter = SplashAdvanceAd()
        self.isLoading = true
        advanceInter?.show(fromVc, adInfo: self.adInfo)
        advanceInter?.addObserver({ (adView, isSuccess) in
            self.adLogo = self.advanceInter?.adLogo
            self.isLoading = false
            if isSuccess == true {
                self.hasAd = true
                self.admobInterBlock?(self, false)
            } else {
                self.admobInterBlock?(self, true)
            }
        })
    }
    
    override func showAd(_ fromVc:UIViewController) -> Bool {
        if self.hasAd == true && self.advanceInter != nil {
            self.advanceInter?.alpha = 0
            fromVc.view.addSubview(self.advanceInter!)
            UIView.animate(withDuration: 0.5, animations: {
                self.advanceInter?.alpha = 1
            })
            self.advanceInter?.adNative?.rootViewController = fromVc
            self.advanceInter!.snp.makeConstraints({ (make) in
                make.edges.equalTo(fromVc.view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
            })
            return true
        } else {
            return false
        }
    }
}
