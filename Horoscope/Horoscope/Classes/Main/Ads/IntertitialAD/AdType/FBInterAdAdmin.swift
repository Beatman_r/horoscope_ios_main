//
//  FBInterAdAdmin.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/23.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import FBAudienceNetwork

typealias FbAdShowSuccess = (_ admin:FBInterAdAdmin, _ error:Bool) -> ()

class FBInterAdAdmin: BaseInterstitialAd, FBInterstitialAdDelegate {
    
    var fbInter:FBInterstitialAd?
    var fbInterBlock:FbAdShowSuccess?
    
    init(adInfo:SingleAdInfo, success:@escaping FbAdShowSuccess) {
        super.init()
        self.adInfo = adInfo
        self.fbInterBlock = success
        self.hasAd = false
        self.requestAd(nil)
    }
    
    override func requestAd(_ fromVc:UIViewController?) {
        LXSWLog("#### FB AD Start Load")
        let adId:String = self.adInfo?.adUnitId ?? ""
        self.fbInter = FBInterstitialAd(placementID: adId)
        if let fbInter = self.fbInter {
            fbInter.delegate = self
            DispatchQueue.main.async(execute: { 
                fbInter.load()
            })
            self.isLoading = true
        }
    }
    
    override func showAd(_ fromVc: UIViewController) -> Bool {
        if self.fbInter != nil && self.fbInter?.isAdValid == true {
            self.fbInter?.show(fromRootViewController: fromVc)
            return true
        } else {
            return false
        }
    }
    
    // MARK: FBInterstitialAdDelegate
    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd) {
        LXSWLog("#### FB AD Success")
        self.isLoading = false
        self.hasAd = true
        self.fbInterBlock?(self, false)
    }
    
    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
        LXSWLog("### FB AD Failed")
        self.fbInter = nil
        self.isLoading = false
        self.fbInterBlock?(self, true)
    }
    
    func interstitialAdWillLogImpression(_ interstitialAd: FBInterstitialAd) {
        LXSWLog("#### FB Present: \(self.adInfo?.adUnitId ?? "")")
        IntertitialAdManager.shareInstance.isAdShowing = true
        LTVManager.shareInstance.adImpression(adInfo)
        //todo:广告统计，后面会统一移动到某个模块
        if self.adInfo?.placementKey == AdPlacementKey.HomeInterstitial_002.rawValue{
             AnaliticsManager.sendEvent(AnaliticsManager.back_ad_splash, data: ["a1_ad_show":"facebook"])
        }

    }
    
    func interstitialAdDidClose(_ interstitialAd: FBInterstitialAd) {
        IntertitialAdManager.shareInstance.isAdShowing = false
    }
    
    //optimize the compilation by haoxudong
    func interstitialAdDidClick(_ interstitialAd: FBInterstitialAd) {
        LTVManager.shareInstance.adClick(adInfo)
        let adUnitPlatform = DataTypeUtil.isNull(self.adInfo?.adUnitPlatform)
        let placementKey = DataTypeUtil.isNull(self.adInfo?.placementKey)
        let adUnitType = DataTypeUtil.isNull(self.adInfo?.adUnitType)
        let contentId = adUnitPlatform + "_" + placementKey
        let contentType = adUnitPlatform + "_" + adUnitType
        if self.adInfo?.placementKey == AdPlacementKey.HomeInterstitial_002.rawValue{
            AnaliticsManager.sendEvent(AnaliticsManager.back_ad_splash, data: ["a2_ad_click":"facebook"])
        }
        AnaliticsManager.logPurchase(1, currency: "USD", contentId: contentId, contentType: contentType)
    }
}
