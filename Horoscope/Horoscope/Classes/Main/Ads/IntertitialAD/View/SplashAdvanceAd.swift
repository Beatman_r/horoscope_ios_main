//
//  SplashAdvanceAd.swift
//  bibleverse
//
//  Created by 麦子 on 2017/2/27.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SplashAdvanceAd:UIView,GADNativeContentAdLoaderDelegate,GADNativeAppInstallAdLoaderDelegate,GADNativeAdDelegate {
    
    var adInfo: SingleAdInfo?
    var adRequestComplete: InterAdLoaded?
    var adLoader: GADAdLoader?
    var adLogo: UIImage?
    var adNative: GADNativeAd?
    
    func show(_ vc: UIViewController?, adInfo: SingleAdInfo?) {
        // "ca-app-pub-3940256099942544/3986624511"
        // kGADAdLoaderAdTypeNativeAppInstall, kGADAdLoaderAdTypeNativeContent
        self.adInfo = adInfo
        let option = GADNativeAdImageAdLoaderOptions()
        option.preferredImageOrientation = .portrait
        self.adLoader = GADAdLoader.init(adUnitID: adInfo?.adUnitId ?? "", rootViewController: HoroTabBarViewController.gloableInstance, adTypes: [kGADAdLoaderAdTypeNativeAppInstall, kGADAdLoaderAdTypeNativeContent], options: [option])
        adLoader?.delegate = self
        self.backgroundColor = UIColor(red: 165/255.0, green: 212/255.0, blue: 121/255.0, alpha: 1)
        self.requestAd()
    }
    
    func requestAd() {
        adLoader?.load(GADRequest())
    }
    
    func addObserver(_ requestComplete:@escaping InterAdLoaded) {
        self.adRequestComplete = requestComplete
    }
    
    // MARK: GADNativeAppInstallAdLoaderDelegate
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAppInstallAd: GADNativeAppInstallAd) {
        self.adNative = nativeAppInstallAd
        self.adNative?.delegate = self
        if let image = (nativeAppInstallAd.images?.first as? GADNativeAdImage)?.image {
            let installAdView = Bundle.main.loadNibNamed("SplashInstall", owner: nil, options: nil)?.first as! SplashInstall
            self.addSubview(installAdView)
            installAdView.snp.makeConstraints({ (make) in
                make.edges.equalTo(self).inset(UIEdgeInsetsMake(20, 0, 0, 0))
            })
            self.adLogo = nativeAppInstallAd.icon?.image
            installAdView.refreshUI(nativeAppInstallAd, image:image)
            installAdView.closeBtn.addTarget(self, action: #selector(closeBtnClick), for: .touchUpInside)
            self.adRequestComplete?(self, true)
            self.superview?.bringSubview(toFront: self)
        } else {
            self.adRequestComplete?(self,false)
            self.removeFromSuperview()
        }
    }
    
    // MARK: GADNativeContentAdLoaderDelegate
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeContentAd: GADNativeContentAd) {
        self.adNative = nativeContentAd
        self.adNative?.delegate = self
        if let image = (nativeContentAd.images?.first as? GADNativeAdImage)?.image {
            let contentAdView = Bundle.main.loadNibNamed("SplashContent", owner: nil, options: nil)?.first as! SplashContent
            self.addSubview(contentAdView)
            contentAdView.snp.makeConstraints({ (make) in
                make.edges.equalTo(self).inset(UIEdgeInsetsMake(20, 0, 0, 0))
            })
            self.adLogo = nativeContentAd.logo?.image
            contentAdView.refreshUI(nativeContentAd, image:image)
            contentAdView.closeBtn.addTarget(self, action: #selector(closeBtnClick), for: .touchUpInside)
            self.adRequestComplete?(self, true)
            self.superview?.bringSubview(toFront: self)
        } else {
            self.adRequestComplete?(self, false)
            self.removeFromSuperview()
        }
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        LXSWLog("Advance--splash:\(error.localizedDescription)")
        self.adRequestComplete?(self, false)
        self.removeFromSuperview()
    }
    
    @objc func closeBtnClick() {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0
        }, completion: { (bool) in
            self.removeFromSuperview()
        }) 
    }
    
    func nativeAdWillPresentScreen(_ nativeAd: GADNativeAd) {
        //        LTVManager.shareInstance.adImpression(adInfo)
    }
    
    func nativeAdWillLeaveApplication(_ nativeAd: GADNativeAd) {
        //todo:广告统计，后面会统一移动到某个模块
        LTVManager.shareInstance.adClick(adInfo)
    }
    
    

}
