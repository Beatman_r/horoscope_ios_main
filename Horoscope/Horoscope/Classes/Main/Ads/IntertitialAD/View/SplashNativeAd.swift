//
//  SplashNativeAd.swift
//  bibleverse
//
//  Created by 麦子 on 2017/2/27.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import FBAudienceNetwork

class SplashNativeAd: UIView,FBNativeAdDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var mediaView: FBMediaView!
    @IBOutlet weak var mediaBgV: UIView!
    @IBOutlet weak var bgCardV: UIView!
    @IBOutlet weak var adIcon: UIImageView!
    @IBOutlet weak var adTitle: UILabel!
    @IBOutlet weak var adContent: UILabel!
    @IBOutlet weak var adButton: UIButton!
    let adWhiteIma = UIImageView()
    let bbIma = UIImageView()
    var emitters = [YJFavorEmitter]()
    
    var adchoiceView: FBAdChoicesView!
    var fbAD: FBNativeAd?
    var adInfo: SingleAdInfo?
    var adRequestComplete: InterAdLoaded?
    
    class func create() -> SplashNativeAd {
        var adView = SplashNativeAd()
        if let bundle = Bundle.main.loadNibNamed("SplashNativeAd", owner:nil, options: nil) {
            adView = bundle.first as! SplashNativeAd
        }
        return adView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.createUI()
        self.initUI()
    }
    
    func createUI() {
        self.adButton!.addSubview(adWhiteIma)
        self.addSubview(bbIma)
        for index in 0..<6 {
            let xPosition:CGFloat = CGFloat(index)*55 + 20
            let yPosition:CGFloat = CGFloat(index)*25 + 667
            let heartFrame = AdaptiveUtils.BBRectMake(xPosition, y: yPosition, width: 33, height: 33)
            let emitter = YJFavorEmitter.init(frame: heartFrame, favorDisplay: self, image: UIImage(named: "heart"), highlight: UIImage(named: "heart"))
            emitter?.isHidden = true
            emitter?.extraShift = 10
            emitter?.risingY = 0
            emitter?.minRisingVelocity = 49
            emitter?.cellImages = [UIImage(named: "golden_aquarius")!,UIImage(named: "golden_aries")!,UIImage(named: "golden_gemini")!,UIImage(named: "golden_taurus")!,UIImage(named: "golden_virgo")!]
            self.addSubview(emitter!)
            emitters.append(emitter!)
        }

    }
    
    func initUI() {
        closeBtn.setImage(UIImage(named: "ad_close_white"), for: UIControlState())
        closeBtn.snp.makeConstraints { (make) in
            make.left.equalTo(AdaptiveUtils.getW(16))
            make.top.equalTo(AdaptiveUtils.getH(16))
            make.size.equalTo(AdaptiveUtils.BBSizeMake(15, height: 15))
        }
        imageView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(-20, 0, 0, 0))
        })
        bgCardV.backgroundColor = UIColor.white
        bgCardV.snp.makeConstraints { (make) in
            make.size.equalTo(AdaptiveUtils.BBSizeMake(315, height: 165))
            make.centerX.equalTo(self)
            make.top.equalTo(AdaptiveUtils.getH(70))
        }
        mediaBgV.snp.makeConstraints { (make) in
            make.edges.equalTo(bgCardV).inset(UIEdgeInsets.zero)
        }
        mediaView.snp.makeConstraints { (make) in
            make.edges.equalTo(mediaBgV).inset(UIEdgeInsets.zero)
        }
        bbIma.image = UIImage(named: "img_ad_background")
        bbIma.snp.makeConstraints { (make) in
            make.left.equalTo(mediaBgV)
            make.right.equalTo(mediaBgV)
            make.bottom.equalTo(mediaBgV.snp.top)
            make.height.equalTo(25)
        }
        adIcon?.snp.makeConstraints({ (make) in
            make.size.equalTo(CGSize(width: AdaptiveUtils.getW(70), height: AdaptiveUtils.getW(70)))
            make.centerX.equalTo(self)
            make.top.equalTo(mediaBgV.snp.bottom).offset(AdaptiveUtils.getH(80))
        })
        adButton.setTitleColor(UIColor.white, for: UIControlState())
        adButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        adButton.snp.makeConstraints({ (make) in
            make.left.equalTo(AdaptiveUtils.getW(24))
            make.right.equalTo(AdaptiveUtils.getW(-24))
            make.height.equalTo(AdaptiveUtils.getH(45))
            make.bottom.equalTo(AdaptiveUtils.getH(-24))
        })
        adWhiteIma.image = UIImage(named: "adWhite")
        adButton.addSubview(adWhiteIma)
        adWhiteIma.snp.makeConstraints { (make) in
            make.top.equalTo(adButton)
            make.bottom.equalTo(adButton)
            make.width.equalTo(AdaptiveUtils.getW(60))
            make.left.equalTo(adButton.snp.left).offset(AdaptiveUtils.getW(-50))
        }
        adContent.font = UIFont.systemFont(ofSize: AdaptiveUtils.getF(15))
        adContent.textColor = UIColor.white
        adContent.numberOfLines = 2
        adContent.textAlignment = .center
        adContent.snp.makeConstraints({ (make) in
            make.left.equalTo(AdaptiveUtils.getW(24))
            make.right.equalTo(AdaptiveUtils.getW(-24))
            make.bottom.equalTo(adButton.snp.top).offset(AdaptiveUtils.getH(-20))
        })
        adTitle.font = UIFont.systemFont(ofSize: AdaptiveUtils.getF(17))
        adTitle.textColor = UIColor.white
        adTitle.numberOfLines = 3
        adTitle.textAlignment = .center
        adTitle.snp.makeConstraints({ (make) in
            make.left.equalTo(AdaptiveUtils.getW(24))
            make.right.equalTo(AdaptiveUtils.getW(-24))
            make.bottom.equalTo(adContent.snp.top).offset(AdaptiveUtils.getH(-20))
        })
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        adButton.backgroundColor = UIColor(hexString: "ff35a5")
        adButton.layer.masksToBounds = true
        adButton.layer.cornerRadius = 4
        self.mediaView?.layer.cornerRadius = 4
        self.mediaView?.layer.masksToBounds = true
        mediaBgV.layer.masksToBounds = true
        mediaBgV.layer.cornerRadius = 4
        mediaBgV.layer.shadowRadius = 4
        mediaBgV.layer.shadowOpacity = 0.8
        mediaBgV.layer.shadowOffset = CGSize(width: 1, height: 1)
        mediaBgV.layer.shadowColor = UIColor.black.cgColor
        //        self.showIma.transform = CGAffineTransformRotate(self.showIma.transform, CGFloat(-M_PI/18.0))
        bgCardV.layer.cornerRadius = 4
        bgCardV.layer.shadowRadius = 4
        bgCardV.layer.shadowOpacity = 0.8
        bgCardV.layer.shadowOffset = CGSize(width: 1, height: 1)
        bgCardV.layer.shadowColor = UIColor.black.cgColor
        adIcon.layer.cornerRadius = AdaptiveUtils.getW(35)
        adIcon.layer.masksToBounds = true
        self.backgroundColor = UIColor.clear
    }
    
    func requestAd(_ adInfo:SingleAdInfo?) {
        self.adInfo = adInfo
        self.fbAD = FBNativeAd(placementID:self.adInfo?.adUnitId ?? "")
        fbAD?.delegate = self
        self.fbAD?.load()
    }
    
    func addObserver(_ requestComplete:@escaping InterAdLoaded) {
        self.adRequestComplete = requestComplete
    }
    
    func showAd(_ view:UIView, fromVC:UIViewController) {
        guard let nativeAd = fbAD else {
            return
        }
        self.adTitle.text = nativeAd.title
        self.adchoiceView = FBAdChoicesView(nativeAd:nativeAd)
        self.addSubview(adchoiceView)
        self.adchoiceView?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.snp.top).offset(12)
            make.right.equalTo(self.snp.right).offset(-12)
            make.size.equalTo(CGSize(width: 15, height: 15))
        })
//        nativeAd.registerView(forInteraction: self, with:fromVC)
        nativeAd.registerView(forInteraction: self.adContent, with: fromVC)
        nativeAd.registerView(forInteraction: self.adButton, with: fromVC)
        nativeAd.registerView(forInteraction: self.imageView, with: fromVC)
        self.mediaView.nativeAd = nativeAd
        self.adContent.text = nativeAd.body
        self.adButton.setTitleColor(UIColor.white, for: UIControlState())
        self.adButton.setTitle(nativeAd.callToAction, for:UIControlState())
        nativeAd.icon?.loadAsync(block: { (image) in
            self.adIcon.image = image
            self.imageView.image = image
        })
        view.addSubview(self)
        self.snp.makeConstraints({ (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(20, 0, 0, 0))
        })
        let effect = UIBlurEffect.init(style: .dark)
        let effectV = UIVisualEffectView.init(effect: effect)
        effectV.frame = CGRect(x: 0, y: 0, width: AdaptiveUtils.screenWidth, height: AdaptiveUtils.screenHeight)
        self.imageView.addSubview(effectV)
        let vibrancy = UIVibrancyEffect.init(blurEffect: effect)
        let visualEV = UIVisualEffectView.init(effect: vibrancy)
        visualEV.translatesAutoresizingMaskIntoConstraints = false
        effectV.contentView.addSubview(visualEV)
        Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(tickDown), userInfo: nil, repeats: true)
    }
    
    @objc func tickDown() {
        for emitter in emitters {
            emitter.btnClicked()
        }
        adWhiteIma.snp.updateConstraints { (make) in
            UIView.animate(withDuration: 0.7, animations: {
                self.adWhiteIma.snp.updateConstraints({ (make) in
                    make.left.equalTo(self.adButton.snp.left).offset(AdaptiveUtils.screenWidth-AdaptiveUtils.getW(48))
                })
                self.adWhiteIma.isHidden = false
                self.adWhiteIma.superview?.layoutIfNeeded()
                }, completion: { (complete) in
                    self.adWhiteIma.snp.updateConstraints({ (make) in
                        make.left.equalTo(self.adButton.snp.left).offset(AdaptiveUtils.getW(-50))
                    })
                    self.adWhiteIma.isHidden = true
                    self.adWhiteIma.superview?.layoutIfNeeded()
            })
        }
    }
    
    // MARK: FBNativeAdDelegate
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:\(error)")
        LXSWLog(error)
        self.adRequestComplete?(self, false)
        self.removeFromSuperview()
    }
    
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        self.fbAD = nativeAd
        self.adRequestComplete?(self, true)
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
    }
    
    func nativeAdWillLogImpression(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adImpression(adInfo)
        self.staticHomeSplashAdShow()
    }
    
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adClick(adInfo)
        self.staticHomeSplashAdClick()
        NotificationCenter.default.post(name: Notification.Name(rawValue: CommentsMoreThanSeventyFloor), object: nil)

    }
    //todo:后面需要统一放到广告SDK模块
    func staticHomeSplashAdShow()
    {
        if self.adInfo?.placementKey == AdPlacementKey.HomeInterstitial_002.rawValue{
                AnaliticsManager.sendEvent(AnaliticsManager.back_ad_splash, data: ["a1_ad_show":"facebook"])
        }
    }
    
    func staticHomeSplashAdClick()
    {
        if self.adInfo?.placementKey == AdPlacementKey.HomeInterstitial_002.rawValue{
                AnaliticsManager.sendEvent(AnaliticsManager.back_ad_splash, data: ["a2_ad_click":"facebook"])
        }
    }
}
