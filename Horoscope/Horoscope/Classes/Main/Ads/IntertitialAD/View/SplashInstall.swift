//
//  SplashInstall.swift
//  bibleverse
//
//  Created by 麦子 on 2017/2/27.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SplashInstall: GADNativeAppInstallAdView {
    
    let imageBg: UIView = UIView()
    let closeBtn: UIButton = UIButton()
    let showIma: UIImageView = UIImageView()
    let adWhiteIma = UIImageView()
    var emitters = [YJFavorEmitter]()
    let bbIma = UIImageView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.createUI()
        self.initUI()
    }
    
    func createUI() {
        self.addSubview(imageBg)
        self.addSubview(closeBtn)
        self.addSubview(showIma)
        self.callToActionView?.addSubview(adWhiteIma)
        for index in 0..<6 {
            let xPosition:CGFloat = CGFloat(index)*55 + 20
            let yPosition:CGFloat = CGFloat(index)*25 + 667
            let heartFrame = AdaptiveUtils.BBRectMake(xPosition, y: yPosition, width: 33, height: 33)
            let emitter = YJFavorEmitter.init(frame: heartFrame, favorDisplay: self, image: UIImage(named: "heart"), highlight: UIImage(named: "heart"))
            emitter?.isHidden = true
            emitter?.extraShift = 10
            emitter?.risingY = 0
            emitter?.minRisingVelocity = 49
            emitter?.cellImages = [UIImage(named: "golden_aquarius")!,UIImage(named: "golden_aries")!,UIImage(named: "golden_gemini")!,UIImage(named: "golden_taurus")!,UIImage(named: "golden_virgo")!]
            self.addSubview(emitter!)
            emitters.append(emitter!)
        }
        self.addSubview(bbIma)
    }
    
    func initUI() {
        starRatingView?.alpha = 0
        storeView?.alpha = 0
        priceView?.alpha = 0
        closeBtn.setImage(UIImage(named: "ad_close_white"), for: UIControlState())
        closeBtn.contentMode = UIViewContentMode.scaleAspectFit
        closeBtn.snp.makeConstraints { (make) in
            make.left.equalTo(AdaptiveUtils.getW(16))
            make.top.equalTo(AdaptiveUtils.getH(16))
            make.size.equalTo(AdaptiveUtils.BBSizeMake(20, height: 20))
        }
        imageView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(-20, 0, 0, 0))
        })
        imageBg.backgroundColor = UIColor.white
        imageBg.snp.makeConstraints { (make) in
            make.size.equalTo(AdaptiveUtils.BBSizeMake(315, height: 165))
            make.centerX.equalTo(self)
            make.top.equalTo(AdaptiveUtils.getH(70))
        }
        showIma.snp.makeConstraints { (make) in
            make.edges.equalTo(imageBg).inset(UIEdgeInsets.zero)
        }
        bbIma.image = UIImage(named: "img_ad_background")
        bbIma.snp.makeConstraints { (make) in
            make.left.equalTo(showIma)
            make.right.equalTo(showIma)
            make.bottom.equalTo(showIma.snp.top)
            make.height.equalTo(25)
        }
        iconView?.snp.makeConstraints({ (make) in
            make.size.equalTo(CGSize(width: AdaptiveUtils.getW(70), height: AdaptiveUtils.getW(70)))
            make.centerX.equalTo(self)
            make.top.equalTo(showIma.snp.bottom).offset(AdaptiveUtils.getH(80))
        })
        (self.callToActionView as? UIButton)?.setTitleColor(UIColor.white, for: UIControlState())
        (self.callToActionView as? UIButton)?.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        callToActionView?.snp.makeConstraints({ (make) in
            make.left.equalTo(AdaptiveUtils.getW(24))
            make.right.equalTo(AdaptiveUtils.getW(-24))
            make.height.equalTo(AdaptiveUtils.getH(45))
            make.bottom.equalTo(AdaptiveUtils.getH(-24))
        })
        adWhiteIma.image = UIImage(named: "adWhite")
        self.callToActionView!.addSubview(adWhiteIma)
        adWhiteIma.snp.makeConstraints { (make) in
            make.top.equalTo(self.callToActionView!)
            make.bottom.equalTo(self.callToActionView!)
            make.width.equalTo(AdaptiveUtils.getW(60))
            make.left.equalTo(self.callToActionView!.snp.left).offset(AdaptiveUtils.getW(-50))
        }
        (self.bodyView as? UILabel)?.font = UIFont.systemFont(ofSize: AdaptiveUtils.getF(15))
        (self.bodyView as? UILabel)?.textColor = UIColor.white
        (self.bodyView as? UILabel)?.numberOfLines = 2
        (self.bodyView as? UILabel)?.textAlignment = .center
        bodyView?.snp.makeConstraints({ (make) in
            make.left.equalTo(AdaptiveUtils.getW(24))
            make.right.equalTo(AdaptiveUtils.getW(-24))
            make.bottom.equalTo(callToActionView!.snp.top).offset(AdaptiveUtils.getH(-20))
        })
        (self.headlineView as? UILabel)?.font = UIFont.systemFont(ofSize: AdaptiveUtils.getF(17))
        (self.headlineView as? UILabel)?.textColor = UIColor.white
        (self.headlineView as? UILabel)?.numberOfLines = 3
        (self.headlineView as? UILabel)?.textAlignment = .center
        headlineView?.snp.makeConstraints({ (make) in
            make.left.equalTo(AdaptiveUtils.getW(24))
            make.right.equalTo(AdaptiveUtils.getW(-24))
            make.bottom.equalTo(bodyView!.snp.top).offset(AdaptiveUtils.getH(-20))
        })
        
        if let contentIma = self.imageView as? UIImageView {
            contentIma.layer.masksToBounds = true
            contentIma.contentMode = .scaleAspectFill
        }
        self.callToActionView?.backgroundColor = UIColor(hexString: "ff35a5")
        self.callToActionView?.layer.masksToBounds = true
        self.callToActionView?.layer.cornerRadius = 4
        self.showIma.contentMode = .scaleAspectFill
        self.showIma.layer.masksToBounds = true
        self.showIma.layer.cornerRadius = 4
        self.showIma.layer.shadowRadius = 4
        self.showIma.layer.shadowOpacity = 0.8
        self.showIma.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.showIma.layer.shadowColor = UIColor.black.cgColor
//        self.showIma.transform = CGAffineTransformRotate(self.showIma.transform, CGFloat(-M_PI/18.0))
        self.imageBg.layer.cornerRadius = 4
        self.imageBg.layer.shadowRadius = 4
        self.imageBg.layer.shadowOpacity = 0.8
        self.imageBg.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.imageBg.layer.shadowColor = UIColor.black.cgColor
        self.iconView?.layer.cornerRadius = AdaptiveUtils.getW(35)
        self.iconView?.layer.masksToBounds = true
        self.backgroundColor = UIColor.clear
    }
    
    func refreshUI(_ nativeAppInstallAd: GADNativeAppInstallAd, image:UIImage) {
        Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(tickDown), userInfo: nil, repeats: true)
        self.nativeAppInstallAd = nativeAppInstallAd
        (self.headlineView as? UILabel)?.text = nativeAppInstallAd.headline
        
        showIma.image = image
        (self.imageView as? UIImageView)?.image = image
        (self.iconView as? UIImageView)?.image = image
        
        (self.callToActionView as? UIButton)?.setTitle(nativeAppInstallAd.callToAction, for: UIControlState())
        (self.callToActionView as? UIButton)?.isUserInteractionEnabled = false
        (self.bodyView as? UILabel)?.text = nativeAppInstallAd.body
        
        if let logoImage = nativeAppInstallAd.icon?.image {
            (self.iconView as? UIImageView)?.image = logoImage
        }
        
        let effect = UIBlurEffect.init(style: .dark)
        let effectV = UIVisualEffectView.init(effect: effect)
        effectV.frame = CGRect(x: 0, y: 0, width: AdaptiveUtils.screenWidth, height: AdaptiveUtils.screenHeight)
        self.imageView?.addSubview(effectV)
        let vibrancy = UIVibrancyEffect.init(blurEffect: effect)
        let visualEV = UIVisualEffectView.init(effect: vibrancy)
        visualEV.translatesAutoresizingMaskIntoConstraints = false
        effectV.contentView.addSubview(visualEV)
    }
    
    @objc func tickDown() {
        for emitter in emitters {
            emitter.btnClicked()
        }
        adWhiteIma.snp.updateConstraints { (make) in
            UIView.animate(withDuration: 0.7, animations: {
                self.adWhiteIma.snp.updateConstraints({ (make) in
                    make.left.equalTo(self.callToActionView!.snp.left).offset(AdaptiveUtils.screenWidth-AdaptiveUtils.getW(48))
                })
                self.adWhiteIma.isHidden = false
                self.adWhiteIma.superview?.layoutIfNeeded()
                }, completion: { (complete) in
                    self.adWhiteIma.snp.updateConstraints({ (make) in
                        make.left.equalTo(self.callToActionView!.snp.left).offset(AdaptiveUtils.getW(-50))
                    })
                    self.adWhiteIma.isHidden = true
                    self.adWhiteIma.superview?.layoutIfNeeded()
            })
        }
    }
}
