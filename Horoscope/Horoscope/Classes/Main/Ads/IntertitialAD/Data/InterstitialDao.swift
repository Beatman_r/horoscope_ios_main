//
//  InterstitialDao.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/23.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds
import FBAudienceNetwork
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


typealias SplashLoadSuccess = (_ isSuccess:Bool) -> ()

class InterstitialDao: NSObject {
    
    var splashAd = [BaseInterstitialAd]()
    var pageSwitchAd = [BaseInterstitialAd]()
    var menuBibleAd = [BaseInterstitialAd]()
    var triggerAd = [BaseInterstitialAd]()
    var challengeAd = [BaseInterstitialAd]()
    var isSplashLoading:Bool = false
    
    // MARK: get ad data method
    func loadAllData() {    // delete all old data
        self.loadMenuBible()
        self.loadChallenge()
    }
    
    func reloadAllData() {  // load new data where ad is nil
        for admin in self.menuBibleAd {
            if admin.hasAd == false {
                admin.requestAd(nil)
            }
        }
    }
    
    
    //分析代码后，发现之前应该是因为只有一个开机的插页广告（splash）,所以再代码中很早之前把自定义广告位的placmentKey写死了，这里改为动态传入  BY dingming 2018-4-12
    func loadSplash(adPlaceMentKey:AdPlacementKey = .SplashAdV2 , splashLoadSuccess:@escaping SplashLoadSuccess) {
        self.isSplashLoading = true
        self.splashAd.removeAll()
       // let adPlaceMentKey = AdPlacementKey.SplashAdV2
        let adsDao:AdsDataDao = AdsManager.shareInstance.adsDao
        let data:[AdsDataModel] = adsDao.getAdsConfig(adPlaceMentKey.rawValue)
        for model in data {
            let adInfo = self.getAdInfo(model, placementKey: adPlaceMentKey)
            if model.adUnitType == AdType.SplashNative.rawValue {
                let nativeAdAdmin:NativeAdAdmin = NativeAdAdmin(adInfo: adInfo, success: { (admin, error) in
                    if self.isSplashLoading == false {
                        return
                    }
                    self.splashLoadSuccess(admin, splashLoadSuccess:splashLoadSuccess, error: error)
                })
                self.splashAd.append(nativeAdAdmin)
            } else if model.adUnitType == AdType.SplashAdvance.rawValue {
                let advanceAdAdmin:AdvanceAdAdmin = AdvanceAdAdmin(adInfo: adInfo, success: { (admin, error) in
                    if self.isSplashLoading == false {
                        return
                    }
                    self.splashLoadSuccess(admin, splashLoadSuccess:splashLoadSuccess, error: error)
                })
                self.splashAd.append(advanceAdAdmin)
            } else if model.adUnitPlatform == AdOrigin.Admob.rawValue {
                let goInterAdAdmin:GoInterAdAdmin = GoInterAdAdmin(adInfo: adInfo, success: { (admin, error) in
                    if self.isSplashLoading == false {
                        return
                    }
                    self.splashLoadSuccess(admin, splashLoadSuccess:splashLoadSuccess, error: error)
                })
                self.splashAd.append(goInterAdAdmin)
            } else if model.adUnitPlatform == AdOrigin.Fb.rawValue {
                let fbInterAdAdmin:FBInterAdAdmin = FBInterAdAdmin(adInfo: adInfo, success: { (admin, error) in
                    if self.isSplashLoading == false {
                        return
                    }
                    self.splashLoadSuccess(admin, splashLoadSuccess:splashLoadSuccess, error: error)
                })
                self.splashAd.append(fbInterAdAdmin)
            }
        }
        let time = Double((AdsManager.shareInstance.splashAdWaitTime ?? 0)/1000)
        LXSWLog(time)
        let delayTime = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            if self.isSplashLoading == true {
                LXSWLog("splash_timeOut")
                splashLoadSuccess(false)
                self.isSplashLoading = false
            }
        }
    }
    
    func splashLoadSuccess(_ admin:BaseInterstitialAd,splashLoadSuccess:SplashLoadSuccess,error:Bool) {
        let splashAds = self.splashAd.filter({ (splash) -> Bool in
            return splash.isLoading == false
        })
        if splashAds.count == self.splashAd.count {
            LXSWLog("splash_complete")
            splashLoadSuccess(true)
            self.isSplashLoading = false
            return
        }
        let loading = self.splashAd.filter { (splash) -> Bool in
            return splash.isLoading == true
        }
        let hasAd = self.splashAd.filter { (splash) -> Bool in
            return splash.hasAd == true
        }
        if hasAd.first?.adInfo?.adUnitPriority >= loading.first?.adInfo?.adUnitPriority {
            LXSWLog("splash_better")
            splashLoadSuccess(true)
            self.isSplashLoading = false
        }
    }
    
    func loadTrigger(_ splashLoadSuccess:@escaping SplashLoadSuccess) {
        self.triggerAd.removeAll()
        hasLoadTrigger = false
        self.triggerAd = [BaseInterstitialAd]()
        let adPlaceMentKey = AdPlacementKey.HomeTrigger
        let adsDao:AdsDataDao = AdsManager.shareInstance.adsDao
        let data:[AdsDataModel] = adsDao.getAdsConfig(adPlaceMentKey.rawValue)
        for model in data {
            let adInfo = self.getAdInfo(model, placementKey: adPlaceMentKey)
            if model.adUnitType == AdType.SplashNative.rawValue {
                let nativeAdAdmin:NativeAdAdmin = NativeAdAdmin(adInfo: adInfo, success: { (admin, error) in
                    self.triggerAdSuccess(admin, splashLoadSuccess:splashLoadSuccess, error: error)
                })
                self.triggerAd.append(nativeAdAdmin)
            } else if model.adUnitType == AdType.SplashAdvance.rawValue {
                let advanceAdAdmin:AdvanceAdAdmin = AdvanceAdAdmin(adInfo: adInfo, success: { (admin, error) in
                    self.triggerAdSuccess(admin, splashLoadSuccess:splashLoadSuccess, error: error)
                })
                self.triggerAd.append(advanceAdAdmin)
            }
        }
    }
    
    var hasLoadTrigger = false
    func triggerAdSuccess(_ admin:BaseInterstitialAd,splashLoadSuccess:SplashLoadSuccess,error:Bool) {
        if hasLoadTrigger == true {
            return
        }
        let splashAds = self.triggerAd.filter({ (trigger) -> Bool in
            return trigger.isLoading == false
        })
        if splashAds.count == self.triggerAd.count {
            splashLoadSuccess(true)
            hasLoadTrigger = true
            return
        }
        let loading = self.triggerAd.filter { (trigger) -> Bool in
            return trigger.isLoading == true
        }
        let hasAd = self.triggerAd.filter { (trigger) -> Bool in
            return trigger.hasAd == true
        }
        if hasAd.first?.adInfo?.adUnitPriority >= loading.first?.adInfo?.adUnitPriority {
            splashLoadSuccess(true)
            hasLoadTrigger = true
        }
    }
    
    func loadChallenge() {
        self.challengeAd = self.loadAd(AdPlacementKey.ChallengeRight)
    }
    
    // MARK: Private Method
    fileprivate func loadPageSwitch() {
        self.pageSwitchAd = self.loadAd(AdPlacementKey.PageSwitch)
    }
    
    fileprivate func loadMenuBible() {
        self.menuBibleAd = self.loadAd(AdPlacementKey.MenuBible)
    }
    
    fileprivate func getAdInfo(_ adModel:AdsDataModel, placementKey:AdPlacementKey) -> SingleAdInfo {
        let adInfo = SingleAdInfo()
        adInfo.adUnitId = adModel.adUnitId
        adInfo.adUnitPlatform = adModel.adUnitPlatform
        adInfo.adUnitPriority = adModel.adUnitPriority
        adInfo.adUnitType = adModel.adUnitType
        adInfo.placementKey = placementKey.rawValue
        return adInfo
    }
    
    fileprivate func loadAd(_ placementKey:AdPlacementKey) -> [BaseInterstitialAd] {
        var ads = [BaseInterstitialAd]()
        let data = AdsManager.shareInstance.adsDao.getAdsConfig(placementKey.rawValue)
        for model in data {
            let adInfo = self.getAdInfo(model, placementKey: placementKey)
            if model.adUnitType == AdType.SplashNative.rawValue {
                let nativeAdAdmin:NativeAdAdmin = NativeAdAdmin(adInfo: adInfo, success: { (admin, error) in
                })
                ads.append(nativeAdAdmin)
            } else if model.adUnitType == AdType.SplashAdvance.rawValue {
                let advanceAdAdmin:AdvanceAdAdmin = AdvanceAdAdmin(adInfo: adInfo, success: { (admin, error) in
                })
                ads.append(advanceAdAdmin)
            } else if model.adUnitPlatform == AdOrigin.Admob.rawValue {
                let goInterAdAdmin:GoInterAdAdmin = GoInterAdAdmin(adInfo: adInfo, success: { (admin, error) in
                })
                ads.append(goInterAdAdmin)
            } else if model.adUnitPlatform == AdOrigin.Fb.rawValue {
                let fbInterAdAdmin:FBInterAdAdmin = FBInterAdAdmin(adInfo: adInfo, success: { (admin, error) in
                })
                ads.append(fbInterAdAdmin)
            }
        }
        return ads
    }
    
}
