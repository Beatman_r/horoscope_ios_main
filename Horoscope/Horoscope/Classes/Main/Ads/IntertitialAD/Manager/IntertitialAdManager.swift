//
//  IntertitialAdManager.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


typealias AdShowComplete = (_ isSuccess:Bool) -> ()

let triggerAdLoaded = "triggerAdLoaded"

class IntertitialAdManager: NSObject {
    
    static let shareInstance = IntertitialAdManager()
    
    fileprivate let noAdType = "noAdType"
    fileprivate var canShowForegAd:Bool = false
    fileprivate var verseCurrentCount = 0
    fileprivate var pageCurrentCount = 0
    fileprivate var devotionCurrentCount = 0
    fileprivate var adIndex = 0
    fileprivate var adType = ""
    
    var isAdShowing:Bool = false
    
    var splashComplete: AdShowComplete?
    var pageSwitchComplete: AdShowComplete?
    var menuBibleComplete: AdShowComplete?
    
    let adsData: InterstitialDao = InterstitialDao()
    
    // MARK:Public Method
    func show(_ placementKey:AdPlacementKey, fromVC:UIViewController, complete:@escaping AdShowComplete) {
        if AdsManager.shareInstance.canShowIntertitialAd() == false {
            self.showAdFailure(complete)
            return
        }
        let adTypes = AdsManager.shareInstance.interAdSequence
        if adIndex < adTypes?.count {
            self.adType = adTypes![adIndex]
        } else {
            self.adType = self.noAdType
        }
        switch placementKey {
        case AdPlacementKey.SplashAdV2:
            self.splashComplete = complete
            self.showSplash(fromVC)
            break
        case AdPlacementKey.PageSwitch:
            self.pageSwitchComplete = complete
            self.showPageSwitch(fromVC)
            break
        case AdPlacementKey.MenuBible:
            self.menuBibleComplete = complete
            self.showMenuBible(fromVC)
            break
        default:
            break
        }
    }
    
    // MARK:Load AD Data Method
    func loadInterAd() {
        self.adsData.loadAllData()
    }
    
    func reloadInterAd() {
        self.adsData.reloadAllData()
    }
    
    func loadChallenge() {
        self.adsData.loadChallenge()
    }
    
    func showChallenge() -> Bool {
        guard let fromVC = HoroTabBarViewController.gloableInstance else {
            return false
        }
        if AdsManager.shareInstance.canShowInnerAd() == false {
            return false
        }
        for adAdmin in self.adsData.challengeAd {
            if let _ = showAd(adAdmin, fromVC: fromVC) {
                return true
            }
        }
        return false
    }
    
    func loadTrigger() {
        self.adsData.loadTrigger ({ (isSuccess) in
            NotificationCenter.default.post(name: Notification.Name(rawValue: "triggerAdLoaded"), object: nil)
        })
    }
    
    func showTriggerAd(_ complete:()->()) {
        guard let fromVC = HoroTabBarViewController.gloableInstance else {
            return
        }
        if AdsManager.shareInstance.canShowInnerAd() == false {
            return
        }
        for adAdmin in self.adsData.triggerAd {
            if let _ = showAd(adAdmin, fromVC: fromVC) {
                complete()
                self.adsData.triggerAd.removeAll()
                return
            }
        }
    }
    
    func loadSplash(_ splashLoadSuccess:@escaping SplashLoadSuccess) {
        if AdsManager.shareInstance.canShowIntertitialAd() == false {
            splashLoadSuccess(false)
            return
        }
        self.adsData.loadSplash { (isSuccess) in
             splashLoadSuccess(isSuccess)
        }
    
    }
    
    fileprivate func showSplash(_ fromVC:UIViewController) {
        for adAdmin in self.adsData.splashAd {
            if let showAd = showAd(adAdmin, fromVC: fromVC) {
                self.showSplashSuccess(showAd)
                return
            }
        }
        self.splashComplete?(false)
    }
    
    fileprivate func showPageSwitch(_ fromVC:UIViewController) {
        self.pageCurrentCount += 1
        if pageCurrentCount >= AdsManager.shareInstance.pageSwitchCount{
            self.pageCurrentCount = 0
            let ads = self.adsData.pageSwitchAd
            if let complete = self.pageSwitchComplete {
                self.showInterAd(ads, fromVC: fromVC, complete: complete)
            }
        }
    }
    
    fileprivate func showMenuBible(_ fromVC:UIViewController) {
        let ads = self.adsData.menuBibleAd
        if let complete = self.menuBibleComplete {
            if let main = HoroTabBarViewController.gloableInstance {
                self.showInterAd(ads, fromVC: main, complete: complete)
            }
        }
    }
    
    fileprivate func showInterAd(_ ads:[BaseInterstitialAd], fromVC:UIViewController, complete:@escaping AdShowComplete) {
        if self.adType != self.noAdType {
            let priorityAds = ads.filter({ (admin) -> Bool in
                return admin.adInfo?.adUnitPlatform == self.adType
            })
            for adAdmin in priorityAds {
                if let showAd = showAd(adAdmin, fromVC: fromVC) {
                    self.showAdSuccess(showAd,complete:complete)
                    return
                }
            }
        }
        for adAdmin in ads {
            if let showAd = showAd(adAdmin, fromVC: fromVC) {
                self.showAdSuccess(showAd,complete:complete)
                return
            }
        }
        self.showAdFailure(complete)
    }
    
    fileprivate func showAd(_ adAdmin:BaseInterstitialAd, fromVC:UIViewController) -> BaseInterstitialAd? {
        if let advanceAdmin = adAdmin as? AdvanceAdAdmin {
            let isSuccess = advanceAdmin.showAd(fromVC)
            if isSuccess == true {
                return advanceAdmin
            }
        } else if let nativeAdmin = adAdmin as? NativeAdAdmin {
            let isSuccess = nativeAdmin.showAd(fromVC)
            if isSuccess == true {
                return nativeAdmin
            }
        } else if let goAdmin = adAdmin as? GoInterAdAdmin {
            let isSuccess = goAdmin.showAd(fromVC)
            if isSuccess == true {
                return goAdmin
            }
        } else if let fbAdmin = adAdmin as? FBInterAdAdmin {
            let isSuccess = fbAdmin.showAd(fromVC)
            if isSuccess == true {
                return fbAdmin
            }
        }
        return nil
    }
    
    fileprivate func showAdFailure(_ complete:AdShowComplete) {
        complete(false)
        self.reloadInterAd()
    }
    
    fileprivate func showAdSuccess(_ admin:BaseInterstitialAd,complete:@escaping AdShowComplete) {
        if admin.adInfo?.adUnitPlatform == self.adType {
            self.adIndex += 1
        }
        admin.hasAd = false
        AdsManager.shareInstance.lastAdShowTime = Date().timeIntervalSince1970
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            complete(true)
        }
        self.reloadInterAd()
    }
    
    fileprivate func showSplashSuccess(_ admin:BaseInterstitialAd) {
        if admin.adInfo?.adUnitPlatform == adType {
            self.adIndex = 1
        } else {
            self.adIndex = 0
        }
        AdsManager.shareInstance.lastAdShowTime = Date().timeIntervalSince1970
        let delayTime = DispatchTime.now() + Double(Int64(0.8 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.splashComplete?(true)
        }
        self.reloadInterAd()
    }
}

// MARK: 加载插页广告
//暂时不动原来的逻辑，并且按照自己的逻辑进行修改
extension IntertitialAdManager
{
    //适用于所有的插页广告
    func loadAdConfigData(adPlaceMentKey: AdPlacementKey, splashLoadSuccess:@escaping SplashLoadSuccess) {
        if AdsManager.shareInstance.canShowIntertitialAd() == false {
            splashLoadSuccess(false)
            return
        }
        self.adsData.loadSplash(adPlaceMentKey: adPlaceMentKey) { (isSuccess) in
              splashLoadSuccess(isSuccess)
        }

    }
    
    func showIntertitialAd(fromVC:UIViewController, complete:@escaping AdShowComplete) {
        if AdsManager.shareInstance.canShowIntertitialAd() == false {
            self.showAdFailure(complete)
            return
        }
        let adTypes = AdsManager.shareInstance.interAdSequence
        if adIndex < adTypes?.count {
            self.adType = adTypes![adIndex]
        } else {
            self.adType = self.noAdType
        }
        self.splashComplete = complete
        self.showSplash(fromVC)
    }
    

}


