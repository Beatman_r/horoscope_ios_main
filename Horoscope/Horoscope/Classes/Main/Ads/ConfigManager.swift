//
//  ConfigManager.swift
//  bibleverse
//
//  Created by guest on 16/9/13.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

typealias LoadDataFinsihed = (_ data:[String: AnyObject]) -> ()

class ConfigManager: NSObject {
    
    static let shareInstance = ConfigManager()
    
    fileprivate let ConfigVersionString = "ConfigVersionString"
    fileprivate let adConfigUserType = "ConfigManager.adConfigUserType"
    fileprivate let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
    fileprivate let configPath = NSSearchPathForDirectoriesInDomains(
        FileManager.SearchPathDirectory.documentDirectory,
        FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/config.json"
    fileprivate var config: [String: AnyObject]?
    fileprivate var observes = [LoadDataFinsihed]()
    fileprivate var keys = [String]()
    fileprivate var isNewVersion = false
    
    var adConfigType:String = "adConfigA"
    
    override init() {
        super.init()
        self.isNewVersion = self.isNewUser()
//        self.removeConfigCache()
        let userDefault = UserDefaults.standard
        if (userDefault.string(forKey: self.adConfigUserType) == nil) || self.isNewVersion == true {
            if Int(Date().timeIntervalSince1970)%2 == 0 {
                userDefault.set("adConfigA", forKey: adConfigUserType)
            } else {
                userDefault.set("adConfigB", forKey: adConfigUserType)
            }
            userDefault.synchronize()
        }
        self.adConfigType = userDefault.string(forKey: self.adConfigUserType) ?? "adConfigA"
        self.getConfigOnLine { (data) in
            
        }
    }
    
    func getConfig(_ key:String) -> [String: AnyObject] {
        LXSWLog("filePath----\(self.configPath)")
        if let data = self.config {
            LXSWLog("self.config---\(data)")
            let datas = (data[key] as? [String: AnyObject]) ?? [String: AnyObject]()
            if datas.count > 0 {
                return datas
            }
        }
        if self.getConfigCache(key).count > 0 {
            LXSWLog("getConfigCache---\(self.getConfigCache(key))")
            return self.getConfigCache(key)
        }
        if self.getConfigDefault(key).count > 0 {
            LXSWLog("getConfigFile---\(self.getConfigDefault(key))")
            return self.getConfigDefault(key)
        }
        return [String: AnyObject]()
    }
    
    func getConfigOnLine(_ complete: @escaping LoadDataFinsihed) {
        DataManager.sharedInstance.adConfigDao.asyncGetConfigData({ (object) in
            if let dic = object as? [String : AnyObject] {
                _ = object.write(toFile: self.configPath, atomically: true)
                complete(dic)
                self.config = dic
                self.listenOnLineData()
            }
        }) { (error) in
            
        }
    }
    
    fileprivate func getConfigCache(_ key:String) -> [String: AnyObject]{
        if let data = NSDictionary(contentsOfFile: configPath ) {
            self.config = data as? [String : AnyObject]
            return (data[key] as? [String: AnyObject]) ?? [String: AnyObject]()
        } else {
            return [String: AnyObject]()
        }
    }
    
    fileprivate func getConfigDefault(_ key:String) -> [String: AnyObject] {
        let jsonPath = Bundle.main.path(forResource: "ConfigFile", ofType: "json")
        if let data = try? Data(contentsOf: URL(fileURLWithPath: jsonPath ?? "")) {
            let json = JSON(data: data)
            return json[key].dictionaryObject as [String : AnyObject]? ?? [String: AnyObject]()
        } else {
            return [String: AnyObject]()
        }
    }
    
    fileprivate func listenOnLineData() {
        if self.observes.count != self.keys.count {
            return
        }
        for i in 0 ..< self.observes.count {
            let block = self.observes[i]
            let key = self.keys[i]
            if let data = self.config?[key] as? [String : AnyObject] {
                block(data)
            }
        }
    }
    
    func addObserver(_ key:String, complete: @escaping LoadDataFinsihed) {
        if key.isEmpty == false {
            self.keys.append(key)
            self.observes.append(complete)
        }
    }
    
    fileprivate func isNewUser() -> Bool {
        let userDefault = UserDefaults.standard
        if let lastVersion = userDefault.string(forKey: ConfigVersionString) {
            if currentVersion == lastVersion {
                return false
            }
        }
        userDefault.set(currentVersion, forKey: ConfigVersionString)
        userDefault.synchronize()
        return true
    }
    
    fileprivate func removeConfigCache() {
        if self.isNewVersion == true {
            let fileM = FileManager.default
            if fileM.fileExists(atPath: configPath) {
                do {
                    try fileM.removeItem(atPath: configPath)
                } catch {
                    LXSWLog("delete file error")
                }
            }
        }
    }
}
