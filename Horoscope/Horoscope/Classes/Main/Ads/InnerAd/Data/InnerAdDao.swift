//
//  InnerAdDao.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

protocol  InnerAdDaoDelegate : class {
    func requestAdSuccess(_ size:CGSize,adView:UIView,adAdmin:BaseInnerAd,adinfo:SingleAdInfo)
}

class InnerAdDao: NSObject {
    
    var adInfo:SingleAdInfo!
    weak var rootVc:UIViewController?
    weak var delegate:InnerAdDaoDelegate?
    var isLoading:Bool = false
    
    func loadAdWithConfig(_ rootVc:UIViewController?, adInfo:SingleAdInfo, view:InnerAdContainer, isStepLoad:Bool) {
        if AdsManager.shareInstance.canShowInnerAd() == false {
            return
        }
        if self.isLoading == true {
            return
        }
        let ads:[AdsDataModel] = AdsManager.shareInstance.adsDao.getAdsConfig(adInfo.placementKey ?? "")
        if isStepLoad == false {
            self.adInfo = adInfo
            self.rootVc = rootVc
            self.loadAllAd(ads, view: view)
        } else if ads.count > 0 {
            self.adInfo = adInfo
            self.rootVc = rootVc
            self.loadSingleAd(ads, index: 0, view:view)
        }
    }
    
    fileprivate func setAdInfo(_ adModel:AdsDataModel) -> SingleAdInfo {
        if self.adInfo == nil {
            self.adInfo = SingleAdInfo()
        }
        self.adInfo?.adUnitId = adModel.adUnitId
        self.adInfo?.adUnitPlatform = adModel.adUnitPlatform
        self.adInfo?.adUnitPriority = adModel.adUnitPriority
        self.adInfo?.adUnitType = adModel.adUnitType
        return self.adInfo
    }
    
    fileprivate func loadAllAd(_ configArr:[AdsDataModel], view:InnerAdContainer) {
        for adsModel in configArr {
            let tempInfo = self.setAdInfo(adsModel)
            let adAdmin = self.getAdAdmin(adsModel.adUnitType)
            DispatchQueue.main.async(execute: {
                adAdmin?.requestAd(self.rootVc, view: view, adInfo: tempInfo)
            })
            (adAdmin as? UIView)?.alpha = 0
            adAdmin?.addObserver({ (size, adView, adAdmin, isSuccess) in
                if isSuccess {
                    self.delegate?.requestAdSuccess(size,adView:adView,adAdmin:adAdmin,adinfo:tempInfo)
                }
            })
        }
    }
    
    fileprivate func loadSingleAd(_ configArr:[AdsDataModel], index:Int, view:InnerAdContainer) {
        let config:AdsDataModel = configArr[index]
        self.adInfo = self.setAdInfo(config)
        self.isLoading = true
        let adAdmin = self.getAdAdmin(config.adUnitType)
        DispatchQueue.main.async(execute: {
            adAdmin?.requestAd(self.rootVc, view: view, adInfo: self.adInfo)
        })
        (adAdmin as? UIView)?.alpha = 0
        adAdmin?.addObserver({ (size, adView, adAdmin, isSuccess) in
            if isSuccess {
                self.isLoading = false
                self.delegate?.requestAdSuccess(size,adView:adView,adAdmin:adAdmin,adinfo:self.adInfo)
            } else {
                let nextIndex = index + 1
                if nextIndex < configArr.count {
                    self.loadSingleAd(configArr, index: nextIndex, view:view)
                } else {
                    self.isLoading = false
                }
            }
        })
    }
    
    fileprivate func getAdAdmin(_ adType:String?) -> BaseInnerAd? {
        var adAdmin:BaseInnerAd?
        switch adType ?? "" {
        case AdType.NativeCoverOnTop.rawValue:
            adAdmin = VerseNativeAd.create()
            break
        case AdType.Rect.rawValue:
            adAdmin = AdmobRectAd()
            break
        case AdType.VerseAdvance.rawValue:
            adAdmin = ResultAdvanceAd()
            break
        case AdType.NativeHeight100.rawValue:
            adAdmin = FBHeight100Ad()
            break
        case AdType.NativeResult.rawValue:
            adAdmin = ResultNativeAd.create()
            break
        case AdType.BreadRecommendAdvance.rawValue:
            adAdmin = WebViewAdvanceAd()
            break
        case AdType.BreadRecommendNative.rawValue:
            adAdmin = WebViewNativeAd.create()
            break
        case AdType.AdmobAdvancedTimeline.rawValue:
            adAdmin = WebViewAdvanceAd()
            break
        case AdType.FbNativeTimeline.rawValue:
            adAdmin = WebViewNativeAd.create()
            break
        case AdType.AdmobAdvancedVodSeeAll.rawValue:
            adAdmin = SeeAllAdvanceAd()
            break
        case AdType.FbNativeVodSeeAll.rawValue:
            adAdmin = SeeAllNativeAd.create()
            break
        case AdType.AdmobAdvancedFollowMain.rawValue:
            adAdmin = WebViewAdvanceAd()
            break
        case AdType.FbNativeFollowMain.rawValue:
            adAdmin = WebViewNativeAd.create()
            break
        case AdType.AdmobAdvancedFollowAuthor.rawValue:
            adAdmin = WebViewAdvanceAd()
            break
        case AdType.FbNativeFollowAuthor.rawValue:
            adAdmin = WebViewNativeAd.create()
            break
        case AdType.AdmobAdvancedDevotionMain.rawValue:
            adAdmin = SeeAllAdvanceAd()
            break
        case AdType.FbNativeDevotionMain.rawValue:
            adAdmin = SeeAllNativeAd.create()
            break
        case AdType.AdmobAdvancedBreadPreview.rawValue:
            adAdmin = VerseAdvanceAd()
            break
        case AdType.FbNativeBreadPreview.rawValue:
            adAdmin = VerseNativeAd.create()
            break
        case AdType.AdmobAdvancedBibleTab.rawValue:
            adAdmin = WebViewAdvanceAd()
            break
        case AdType.FbNativeBibleTab.rawValue:
            adAdmin = WebViewNativeAd.create()
            break
        case AdType.AdmobAdvancedChallengeWrong.rawValue:
            adAdmin = ResultAdvanceAd()
            break
        case AdType.FbNativeChallengeWrong.rawValue:
            adAdmin = ResultNativeAd.create()
            break
        //Horoscope
        case AdType.fbNativeDailyCardAd.rawValue:
            adAdmin = WebViewNativeAd.create()
        case AdType.admobNativeDailyCardAd.rawValue:
            adAdmin = WebViewAdvanceAd()
        case AdType.admobAdvancedVerse.rawValue:
            adAdmin = SeeAllAdvanceAd()
        case AdType.native.rawValue:
            adAdmin = WebViewNativeAd.create()
        default:
            break
        }
        return adAdmin
    }
}
