//
//  InnerAdData.swift
//  bibleverse
//
//  Created by 麦子 on 2016/12/8.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class InnerAdData: NSObject {
    
    static let shareInstance = InnerAdData()

    let resultContainer = InnerAdContainer()
    let challengeContainer = InnerAdContainer()
    
    func loadInnerData() {
        self.loadResultAd()
        self.loadChallengeAd()
    }
    
    func loadChallengeAd() {
        let adInfo = SingleAdInfo()
        adInfo.placementKey = AdPlacementKey.ChallengeWrong.rawValue
        challengeContainer.requestAd(nil, adInfo: adInfo, isStepLoad: true)
    }
    
    func loadResultAd() {
        let adInfo = SingleAdInfo()
        adInfo.placementKey = AdPlacementKey.Result.rawValue
        resultContainer.requestAd(nil, adInfo: adInfo, isStepLoad: true)
    }
}
