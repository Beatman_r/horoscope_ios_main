//
//  InnerAdContainer.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


protocol  InnerAdContainerdelegate : class {
    func requestAdSuccess(_ size:CGSize, adView:InnerAdContainer)
}

class InnerAdContainer: UIView,InnerAdDaoDelegate {
    
    weak var delegate:InnerAdContainerdelegate?
    var innerDao:InnerAdDao?
    weak var rootVc:UIViewController?
    var adInfo:SingleAdInfo?
    var adSize:CGSize = CGSize.zero
    var baseAd:BaseInnerAd?
    fileprivate var isStepLoad = true
    
    func requestAd(_ rootVc:UIViewController?, adInfo: SingleAdInfo, isStepLoad: Bool) {
        if self.innerDao == nil {
            self.innerDao = InnerAdDao()
            self.innerDao?.delegate = self
            NotificationCenter.default.addObserver(self, selector: #selector(appStart), name: NSNotification.Name(rawValue: "AppDelegate.StartApp"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(refreshAdStatus), name: NSNotification.Name(rawValue: adStatusChange), object: nil)
        }
        self.isStepLoad = isStepLoad
        self.rootVc = rootVc
        self.adInfo = adInfo
        self.innerDao?.loadAdWithConfig(rootVc, adInfo: adInfo, view: self, isStepLoad: self.isStepLoad)
    }
    
    func showAd(_ rootVc:UIViewController) {
        baseAd?.showAd(rootVc)
    }
    
    // MARK: notification
    @objc func appStart() {
        if self.rootVc != nil && self.adInfo != nil {
            self.innerDao?.loadAdWithConfig(self.rootVc!, adInfo: self.adInfo!, view: self, isStepLoad: self.isStepLoad)
        }
    }
    
    @objc func refreshAdStatus() {
        if let oldView = self.viewWithTag(100) {
            oldView.removeFromSuperview()
        }
        if self.adSize.height > 10 {
            self.adSize = CGSize.zero
            self.delegate?.requestAdSuccess(CGSize.zero, adView: self)
        }
    }
    
    func requestAdSuccess(_ size: CGSize, adView: UIView, adAdmin: BaseInnerAd, adinfo: SingleAdInfo) {
        if adinfo.adUnitPriority < self.baseAd?.adInfo?.adUnitPriority && self.isStepLoad == false {
            adView.removeFromSuperview()
            return
        }
        let tempH = self.adSize.height
        self.adSize = size
        self.baseAd = adAdmin
        self.bringSubview(toFront: adView)
        UIView.animate(withDuration: 0.5, animations: { 
            adView.alpha = 1
        }, completion: { (complete) in
            if let oldView = self.viewWithTag(100) {
                if adView != oldView {
                    oldView.removeFromSuperview()
                }
            }
            adView.tag = 100
        }) 
        if adinfo.adUnitPlatform == "admob" {
            LTVManager.shareInstance.adImpression(adinfo)
        }
        if (size.height - tempH > 10) || (size.height - tempH < -10) {
            self.delegate?.requestAdSuccess(size, adView: self)
        }
//        staticFortuneADShow()     
    }
    
//    //todo:广告统计，后面会统一移动到某个模块
//    func staticFortuneADShow(){
//        
//        if self.adInfo?.placementKey == AdPlacementKey.MyFortuneNative_003.rawValue{
//            if self.adInfo?.adUnitPlatform == AdOrigin.Admob.rawValue{
//                AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a1_ad_show":"admob"])
//            }
//            else if self.adInfo?.adUnitPlatform == AdOrigin.Fb.rawValue{
//                AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a1_ad_show":"facebook"])
//            }
//        }
//    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AppDelegate.StartApp"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: adStatusChange), object: nil)
    }
    
}
