//
//  VerseInstall.swift
//  bibleverse
//
//  Created by 麦子 on 2017/2/27.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class VerseInstall: GADNativeAppInstallAdView {
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        if let actionBtn = self.callToActionView as? UIButton {
            actionBtn.layer.masksToBounds = true
            actionBtn.layer.cornerRadius = 13
            actionBtn.layer.borderWidth = 1
            actionBtn.layer.borderColor = UIColor.flatGreen.cgColor
            actionBtn.backgroundColor = UIColor.white
            actionBtn.isUserInteractionEnabled = false
            actionBtn.setTitleColor(UIColor.flatGreen, for: .normal)
            actionBtn.titleLabel?.font = HSFont.baseRegularFont(16)
        }
        if let headlineL = self.headlineView as? UILabel {
            headlineL.numberOfLines = 0
            headlineL.font = HSFont.baseRegularFont(16)
            headlineL.textColor = UIColor.black
        }
        if let contentIma = self.imageView as? UIImageView {
            contentIma.layer.masksToBounds = true
            contentIma.contentMode = .scaleAspectFill
        }
        self.backgroundColor = UIColor.white
    }
    
    func refreshUI(_ nativeAppInstallAd: GADNativeAppInstallAd, image:UIImage) {
        self.nativeAppInstallAd = nativeAppInstallAd
        (self.headlineView as! UILabel).text = nativeAppInstallAd.headline
        (self.imageView as! UIImageView).image = image
        (self.callToActionView as! UIButton).setTitle(nativeAppInstallAd.callToAction, for: UIControlState())
        if image.size.height < image.size.width {
            imageHeight.constant = image.size.height/image.size.width*(AdaptiveUtils.screenWidth-adSpace*2)
        } else {
            let width = AdaptiveUtils.screenWidth-adSpace*2
            let scale:CGFloat = 359/688.41
            imageHeight.constant = width*scale
        }
    }
}
