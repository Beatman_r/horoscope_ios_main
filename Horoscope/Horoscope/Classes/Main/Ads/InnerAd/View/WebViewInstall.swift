//
//  WebViewInstall.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/17.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class WebViewInstall: GADNativeAppInstallAdView {
    
    @IBOutlet weak var titleH: NSLayoutConstraint!
    @IBOutlet weak var showIma: UIImageView!
    
    override func awakeFromNib() {
        if let actionBtn = self.callToActionView as? UIButton {
            actionBtn.layer.masksToBounds = true
            actionBtn.layer.cornerRadius = 15
            actionBtn.layer.borderWidth = 1
            actionBtn.layer.borderColor = UIColor.basePurpleBackgroundColorDark().cgColor
            actionBtn.backgroundColor = UIColor.luckyPurpleTitleColor()
            actionBtn.isUserInteractionEnabled = false
            actionBtn.setTitleColor(UIColor.basePurpleBackgroundColorDark(), for: .normal)
            actionBtn.titleLabel?.font = HSFont.baseRegularFont(14)
        }
        if let headlineL = self.headlineView as? UILabel {
            headlineL.numberOfLines = 0
            headlineL.font = HSFont.baseRegularFont(16)
            headlineL.textColor = UIColor.luckyPurpleTitleColor()
        }
        if let contentL = self.bodyView as? UILabel {
            contentL.numberOfLines = 2
            contentL.font = HSFont.baseRegularFont(16)
            contentL.textColor = UIColor.luckyPurpleContentColor()
            contentL.lineBreakMode = .byWordWrapping
        }
        showIma.layer.masksToBounds = true
        showIma.contentMode = .scaleAspectFill
        self.backgroundColor = UIColor.basePurpleBackgroundColor()
    }
    
    func refreshUI(_ nativeAppInstallAd: GADNativeAppInstallAd, image:UIImage) {
        self.nativeAppInstallAd = nativeAppInstallAd
        let headLine = self.headlineView as! UILabel
        headLine.text = nativeAppInstallAd.headline
        titleH.constant = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: nativeAppInstallAd.headline ?? "", fontSize: 16, font: HSFont.baseRegularFont(16), width: SCREEN_WIDTH-136, multyplyLineSpace: 1.25)
        showIma.image = image
        let str = (nativeAppInstallAd.body)! + "\n\n\n "
        (self.bodyView as! UILabel).text = str
        (self.callToActionView as! UIButton).setTitle(nativeAppInstallAd.callToAction, for: UIControlState())
    }
}
