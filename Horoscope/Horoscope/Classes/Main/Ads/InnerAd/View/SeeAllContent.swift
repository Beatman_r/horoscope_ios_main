//
//  SeeAllContent.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/18.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SeeAllContent: GADNativeContentAdView {
    
    @IBOutlet weak var lineL: UILabel!
    
    override func awakeFromNib() {
        self.backgroundColor = .white
        if let actionBtn = self.callToActionView as? UIButton {
            actionBtn.isUserInteractionEnabled = false
            actionBtn.setTitleColor(.white, for: UIControlState())
            actionBtn.backgroundColor = UIColor(hexString: "#BE59F7")
            actionBtn.titleLabel?.font = HSFont.baseRegularFont(16)
        }
        if let headlineL = self.headlineView as? UILabel {
            headlineL.numberOfLines = 0
            headlineL.font = UIFont(name: "PingFangHK-Medium", size: 18)
            headlineL.textColor = .white
        }
        if let contentL = self.bodyView as? UILabel {
            contentL.numberOfLines = 0
            contentL.font =  UIFont(name: "PingFangHK-Medium", size: 13)
            contentL.textColor = .white
            contentL.lineBreakMode = .byTruncatingTail
        }
        if let contentIma = self.imageView as? UIImageView {
            contentIma.layer.masksToBounds = true
            contentIma.contentMode = .scaleAspectFill
        }
        
        if let iconImg = self.logoView as? UIImageView {
            iconImg.layer.masksToBounds = true
            iconImg.layer.cornerRadius = 5
        }
        
        self.backgroundColor = UIColor(hexString: "3E1F52")
 
    }
    
    func refreshUI(_ nativeContentAd: GADNativeContentAd, image:UIImage) {
        self.nativeContentAd = nativeContentAd
        let headLine = self.headlineView as! UILabel
        headLine.text = nativeContentAd.headline

        (self.imageView as! UIImageView).image = image
        (self.logoView as! UIImageView).image = nativeContentAd.logo?.image
        let str = (nativeContentAd.body)! + "\n\n\n "
        (self.bodyView as! UILabel).text = str
        if let ctaTitle = nativeContentAd.callToAction{
            (self.callToActionView as! UIButton).setTitle(ctaTitle, for: UIControlState())
        }
        else{
            (self.callToActionView as! UIButton).setTitle("Install", for: UIControlState())
        }
        
        (self.callToActionView as! UIButton).setTitleColor(.white, for: .normal)
        (self.callToActionView as! UIButton).backgroundColor = UIColor(hexString: "#BE59F7")
        (self.callToActionView as! UIButton).layer.cornerRadius = 20
        (self.callToActionView as! UIButton).layer.masksToBounds = true

    }
}
