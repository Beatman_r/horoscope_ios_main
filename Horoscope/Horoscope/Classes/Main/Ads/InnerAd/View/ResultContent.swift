//
//  ResultContent.swift
//  bibleverse
//
//  Created by 麦子 on 2017/3/1.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ResultContent: GADNativeContentAdView {
    
    @IBOutlet weak var titleH: NSLayoutConstraint!
    @IBOutlet weak var imageH: NSLayoutConstraint!
    @IBOutlet weak var actionH: NSLayoutConstraint!
    let adWhiteIma = UIImageView()
    
    override func awakeFromNib() {
        if let actionBtn = self.callToActionView as? UIButton {
            actionBtn.layer.cornerRadius = 2
            actionBtn.backgroundColor = UIColor.markBtnGreenColor()
            actionBtn.setTitleColor(UIColor.white, for: UIControlState())
            actionBtn.titleLabel?.font = HSFont.baseRegularFont(16)
            actionBtn.isUserInteractionEnabled = false
        }
        if let headlineL = self.headlineView as? UILabel {
            headlineL.numberOfLines = 0
            headlineL.font = HSFont.baseRegularFont(16)
            headlineL.textColor = UIColor.black
            headlineL.textAlignment = .center
        }
        if let contentIma = self.imageView as? UIImageView {
            contentIma.layer.masksToBounds = true
            contentIma.contentMode = .scaleAspectFill
        }
        self.backgroundColor = UIColor.white
        
        adWhiteIma.image = UIImage(named: "adWhite")
        self.callToActionView!.addSubview(adWhiteIma)
        adWhiteIma.snp.makeConstraints { (make) in
            make.top.equalTo(self.callToActionView!)
            make.bottom.equalTo(self.callToActionView!)
            make.width.equalTo(50)
            make.left.equalTo(self.callToActionView!.snp.left).offset(-50)
        }
    }
    
    func refreshUI(_ nativeContentAd: GADNativeContentAd, image:UIImage) {
        Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(tickDown), userInfo: nil, repeats: true)
        self.nativeContentAd = nativeContentAd
        let headLine = self.headlineView as! UILabel
        headLine.text = nativeContentAd.headline
        titleH.constant = AdaptiveUtils.getH(20)
        actionH.constant = AdaptiveUtils.getH(38)
        (self.imageView as! UIImageView).image = image
        (self.callToActionView as! UIButton).setTitle(nativeContentAd.callToAction, for: UIControlState())
        imageH.constant = AdaptiveUtils.getH(190)
    }
    
    @objc func tickDown() {
        adWhiteIma.snp.updateConstraints { (make) in
            UIView.animate(withDuration: 0.7, animations: {
                self.adWhiteIma.snp.updateConstraints({ (make) in
                    make.left.equalTo(self.callToActionView!.snp.left).offset(AdaptiveUtils.screenWidth-48)
                })
                self.adWhiteIma.isHidden = false
                self.adWhiteIma.superview?.layoutIfNeeded()
                }, completion: { (complete) in
                    self.adWhiteIma.snp.updateConstraints({ (make) in
                        make.left.equalTo(self.callToActionView!.snp.left).offset(-50)
                    })
                    self.adWhiteIma.isHidden = true
                    self.adWhiteIma.superview?.layoutIfNeeded()
            })
        }
    }

}
