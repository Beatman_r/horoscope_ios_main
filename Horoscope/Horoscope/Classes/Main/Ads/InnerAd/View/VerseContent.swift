//
//  VerseContent.swift
//  bibleverse
//
//  Created by 麦子 on 2017/2/27.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class VerseContent: GADNativeContentAdView {
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        if let actionBtn = self.callToActionView as? UIButton {
            actionBtn.layer.masksToBounds = true
            actionBtn.layer.cornerRadius = 15
            actionBtn.layer.borderWidth = 1
            actionBtn.layer.borderColor = UIColor.flatGreen.cgColor
            actionBtn.backgroundColor = UIColor.white
            actionBtn.setTitleColor(UIColor.flatGreen, for: .normal)
            actionBtn.titleLabel?.font = HSFont.baseRegularFont(16)
            actionBtn.isUserInteractionEnabled = false
        }
        if let headlineL = self.headlineView as? UILabel {
            headlineL.numberOfLines = 0
            headlineL.font = HSFont.baseRegularFont(16)
            headlineL.textColor = UIColor.black
            headlineL.textAlignment = .left
        }
        if let contentIma = self.imageView as? UIImageView {
            contentIma.layer.masksToBounds = true
            contentIma.contentMode = .scaleAspectFill
        }
        self.backgroundColor = UIColor.white
    }
    
    func refreshUI(_ nativeContentAd: GADNativeContentAd, image:UIImage) {
        self.nativeContentAd = nativeContentAd
        (self.headlineView as! UILabel).text = nativeContentAd.headline
        (self.imageView as! UIImageView).image = image
        (self.callToActionView as! UIButton).setTitle(nativeContentAd.callToAction, for: UIControlState())
        if image.size.height < image.size.width {
            imageHeight.constant = image.size.height/image.size.width*(AdaptiveUtils.screenWidth-adSpace*2)
        } else {
            let width = AdaptiveUtils.screenWidth-adSpace*2
            let scale:CGFloat = 359/688.41
            imageHeight.constant = width*scale
        }
    }
}
