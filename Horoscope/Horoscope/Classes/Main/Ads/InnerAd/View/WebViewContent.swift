//
//  WebViewContent.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/17.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class WebViewContent: GADNativeContentAdView {
    
    @IBOutlet weak var titleH: NSLayoutConstraint!
    @IBOutlet weak var showIma: UIImageView!
    
    override func awakeFromNib() {
        if let headlineL = self.headlineView as? UILabel {
            headlineL.numberOfLines = 0
            headlineL.font = HSFont.baseRegularFont(16)
            headlineL.textColor = UIColor.luckyPurpleTitleColor()
            headlineL.textAlignment = .left
        }
        if let contentL = self.bodyView as? UILabel {
            contentL.numberOfLines = 3
            contentL.font = HSFont.baseRegularFont(16)
            contentL.textColor = UIColor.luckyPurpleContentColor()
            contentL.lineBreakMode = .byWordWrapping
        }
        showIma.layer.masksToBounds = true
        showIma.contentMode = .scaleAspectFill
        self.backgroundColor = UIColor.basePurpleBackgroundColor()
    }
    
    func refreshUI(_ nativeContentAd: GADNativeContentAd, image:UIImage) {
        self.nativeContentAd = nativeContentAd
        let headLine = self.headlineView as! UILabel
        headLine.text = nativeContentAd.headline
        titleH.constant = HSHelpCenter.sharedInstance.textTool.calculateLabelHeight(nativeContentAd.headline, font: headLine.font, width: AdaptiveUtils.screenWidth-136)
        showIma.image = image
        let str = (nativeContentAd.body)! + "\n\n\n "
        (self.bodyView as! UILabel).text = str
    }
}
