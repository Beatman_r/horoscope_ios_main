//
//  SeeAllInstall.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/18.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SeeAllInstall: GADNativeAppInstallAdView {
    

    @IBOutlet weak var lineL: UILabel!
    
    override func awakeFromNib() {

        if let actionBtn = self.callToActionView as? UIButton {
            actionBtn.isUserInteractionEnabled = false
            actionBtn.setTitleColor(.white, for: UIControlState())
            actionBtn.backgroundColor = UIColor(hexString: "#BE59F7")
            actionBtn.titleLabel?.font = HSFont.baseRegularFont(16)
        }
        if let headlineL = self.headlineView as? UILabel {
            headlineL.numberOfLines = 0
            headlineL.font = UIFont(name: "PingFangHK-Medium", size: 18)
            headlineL.textColor = .white  //UIColor(hexString: "#2B133A")
        }
        if let contentL = self.bodyView as? UILabel {
            contentL.numberOfLines = 0
            contentL.font =  UIFont(name: "PingFangHK-Medium", size: 13)
            contentL.textColor = .white // UIColor(hexString: "#807189")
            contentL.lineBreakMode = .byTruncatingTail
        }
        if let contentIma = self.imageView as? UIImageView {
            contentIma.layer.masksToBounds = true
            contentIma.contentMode = .scaleAspectFill
        }
        
        if let iconImg = self.iconView as? UIImageView {
            iconImg.layer.masksToBounds = true
            iconImg.layer.cornerRadius = 5
        }
        
        self.lineL.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor(hexString: "3E1F52")
    }
    
    func refreshUI(_ nativeAppInstallAd: GADNativeAppInstallAd, image:UIImage) {
        self.nativeAppInstallAd = nativeAppInstallAd
        let headLine = self.headlineView as! UILabel
        headLine.text = nativeAppInstallAd.headline
//        titleH.constant = HSHelpCenter.sharedInstance.textTool.calculateLabelHeight(nativeAppInstallAd.headline, font: HSFont.baseRegularFont(16), width: AdaptiveUtils.screenWidth-26)
//        contentH.constant = HSHelpCenter.sharedInstance.textTool.calculateLabelHeight(nativeAppInstallAd.body, font: HSFont.baseRegularFont(16), width: AdaptiveUtils.screenWidth-26)
        (self.iconView as! UIImageView).image = nativeAppInstallAd.icon?.image
        (self.imageView as! UIImageView).image = image
        (self.bodyView as! UILabel).text = nativeAppInstallAd.body
        
        if let ctaTitle = nativeAppInstallAd.callToAction{
            (self.callToActionView as! UIButton).setTitle(ctaTitle, for: UIControlState())
        }
        else{
            (self.callToActionView as! UIButton).setTitle("Install", for: UIControlState())
        }
        (self.callToActionView as! UIButton).setTitleColor(.white, for: .normal)
        (self.callToActionView as! UIButton).backgroundColor = UIColor(hexString: "#BE59F7")
        (self.callToActionView as! UIButton).layer.cornerRadius = 20
        (self.callToActionView as! UIButton).layer.masksToBounds = true
    
//        if image.size.height < image.size.width {
//            imageH.constant = image.size.height/image.size.width*(AdaptiveUtils.screenWidth-adSpace*2)
//        } else {
//            let width = AdaptiveUtils.screenWidth-adSpace*2
//            let scale:CGFloat = 359/688.41
//            imageH.constant = width*scale
//        }
    }
}
