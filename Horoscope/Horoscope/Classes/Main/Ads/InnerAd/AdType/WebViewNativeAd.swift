//
//  WebViewNativeAd.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/17.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import FBAudienceNetwork

class WebViewNativeAd: UIView,FBNativeAdDelegate,BaseInnerAd {
 
    @IBOutlet weak var mediaView: UIImageView!
    @IBOutlet weak var adTitle: UILabel!
    @IBOutlet weak var contentL: UILabel!
    @IBOutlet weak var adButton: UIButton!
    @IBOutlet weak var iconImageView: UIImageView!
    
    var adchoiceView: FBAdChoicesView!
    var fbAD: FBNativeAd?
    var adInfo: SingleAdInfo?
    var rootVc: UIViewController?
    var adRequestComplete: AdRequestComplete?
    
    class func create() -> WebViewNativeAd {
        var adView = WebViewNativeAd()
        if let bundle = Bundle.main.loadNibNamed("WebViewNativeAd", owner:nil, options: nil) {
            adView = bundle.first as! WebViewNativeAd
        }
        return adView
    }
    
    override func awakeFromNib() {
        self.adButton.layer.masksToBounds = true
        self.adButton.layer.cornerRadius = 20
        self.iconImageView.layer.masksToBounds = true
        self.iconImageView.layer.cornerRadius = 5
        self.iconImageView.layer.masksToBounds = true
//        self.adButton.layer.borderWidth = 1
//        self.adButton.layer.borderColor = UIColor.luckyPurpleTitleColor().cgColor
        self.adButton.backgroundColor = UIColor(hexString: "#BE59F7")
        self.adButton.setTitleColor(.white, for: UIControlState())
        self.adButton.titleLabel?.font = UIFont(name: "PingFangHK-Medium", size: 18)
        self.adTitle.numberOfLines = 0
        self.adTitle.font = HSFont.baseRegularFont(16)
        self.adTitle.textColor = .white
        self.adTitle.textAlignment = .left
        self.contentL.numberOfLines = 0
        self.contentL.font = UIFont(name: "PingFangHK-Medium", size: 13)
        self.contentL.textColor = .white
       // self.adLogo.image = UIImage(named: "ic_fb_ad")
        self.mediaView.layer.masksToBounds = true
        self.mediaView.contentMode = .scaleAspectFill
        self.backgroundColor = UIColor(hexString: "3E1F52")
        
        var maxAdTextWidth:CGFloat = 180
        let screenWidth =  UIScreen.main.bounds.size.width
        if screenWidth == 320{
            maxAdTextWidth = 130
        }
        else if screenWidth >= 414{
            maxAdTextWidth = 210
        }
       self.contentL.preferredMaxLayoutWidth = maxAdTextWidth
       self.adTitle.preferredMaxLayoutWidth = maxAdTextWidth
       
    }
    
    func requestAd(_ vc:UIViewController?, view:UIView, adInfo:SingleAdInfo) {
        self.rootVc = vc
        self.adInfo = adInfo
        self.tag = 110
        self.isUserInteractionEnabled = true
        view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
//        dispatch_async(dispatch_get_main_queue(), {
            self.fbAD = FBNativeAd(placementID:self.adInfo?.adUnitId ?? "")
            self.fbAD?.delegate=self
            self.fbAD?.load()
//        })
    }
    
    func showAd(_ vc:UIViewController) {
        fbAD?.registerView(forInteraction: self, with: vc)
    }
    
    func addObserver(_ requestComplete:@escaping AdRequestComplete) {
        self.adRequestComplete = requestComplete
    }
    
    // MARK: FBNativeAdDelegate
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:\(error)")
        LXSWLog(error)
        self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
        self.removeFromSuperview()
    }
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        self.adTitle.text = nativeAd.title
        let clickableViews = [self.adButton,self.iconImageView,self.mediaView,self.adTitle,self.contentL] as [UIView]
        nativeAd.unregisterView()
        nativeAd.registerView(forInteraction: self, with: self.rootVc, withClickableViews: clickableViews)
//        nativeAd.registerView(forInteraction: self, with:self.rootVc)
        let str = (nativeAd.body)! + "\n\n\n\n"
        self.contentL.text = str
        if let ctaTitle = nativeAd.callToAction{
            self.adButton.setTitle(ctaTitle, for:UIControlState())
        }
        else{
            self.adButton.setTitle("Install", for:UIControlState())
        }
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
        downLoadImageAndICon(nativeAd)
    }
    
    //FaceBook广告以前存在很多的问题，这里需要后面进行整理 1、 缓存问题  2、必须使用FB的大图还有FBChoies
    func downLoadImageAndICon(_ nativeAd: FBNativeAd)
    {
        let group = DispatchGroup()
        let queueDownloadCoverImage = DispatchQueue(label: "downloadImageQueue")
  
        queueDownloadCoverImage.async(group: group) {
            nativeAd.coverImage?.loadAsync(block: { (image) in
              let adChoiceView = FBAdChoicesView(nativeAd:nativeAd)
              adChoiceView.isBackgroundShown = false
              self.mediaView.image = image
              self.mediaView.addSubview(adChoiceView)
              adChoiceView.frame = CGRect(x: self.mediaView.frame.size.width-adChoiceView.frame.size.width,
                                         y: 0,
                                         width: adChoiceView.frame.size.width,
                                         height: adChoiceView.frame.size.height)
            
            })
        }
        
        let queueDownloadIconImage = DispatchQueue(label: "downloadIconQueue")
        queueDownloadIconImage.async(group: group) {
            nativeAd.icon?.loadAsync(block: { (image) in
                    self.iconImageView.image = image
            })
        }
        
        group.notify(queue: DispatchQueue.main) {
            if let _ = self.mediaView.image,let _ = self.iconImageView.image{
                let screenBounds = UIScreen.main.bounds
                LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
                let width = screenBounds.width
                let height:CGFloat = 120
                self.adRequestComplete?(CGSize(width: width, height: height), self, self, true)
            }
            else{
                self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
                self.removeFromSuperview()
            }
        }
        

    }
    
    func nativeAdWillLogImpression(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adImpression(adInfo)
    }
    
    //optimize the compilation by haoxudong
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adClick(adInfo)
        let adUnitPlatform = DataTypeUtil.isNull(self.adInfo?.adUnitPlatform)
        let placementKey = DataTypeUtil.isNull(self.adInfo?.placementKey)
        let adUnitType = DataTypeUtil.isNull(self.adInfo?.adUnitType)
        let contentId = adUnitPlatform + "_" + placementKey
        let contentType = adUnitPlatform + "_" + adUnitType
        
        if self.adInfo?.placementKey == AdPlacementKey.MyFortuneNative_003.rawValue{
            AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a2_ad_click":"facebook"])
        }
        AnaliticsManager.logPurchase(1, currency: "USD", contentId: contentId, contentType: contentType)
    }
}
