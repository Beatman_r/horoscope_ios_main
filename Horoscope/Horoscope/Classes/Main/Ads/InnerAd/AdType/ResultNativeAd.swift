//
//  ResultNativeAd.swift
//  bibleverse
//
//  Created by 麦子 on 2017/3/1.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import FBAudienceNetwork

class ResultNativeAd: UIView,FBNativeAdDelegate,BaseInnerAd {
    
    @IBOutlet weak var mediaView: FBMediaView!
    @IBOutlet weak var adIcon: UIImageView!
    @IBOutlet weak var adTitle: UILabel!
    @IBOutlet weak var adButton: UIButton!
    @IBOutlet weak var titleH: NSLayoutConstraint!
    @IBOutlet weak var actionH: NSLayoutConstraint!
    
    let adWhiteIma = UIImageView()
    
    var adchoiceView: FBAdChoicesView!
    var fbAD: FBNativeAd?
    var adInfo: SingleAdInfo?
    var rootVc: UIViewController?
    var adRequestComplete: AdRequestComplete?
    
    class func create() -> ResultNativeAd {
        var adView = ResultNativeAd()
        if let bundle = Bundle.main.loadNibNamed("ResultNativeAd", owner:nil, options: nil) {
            adView = bundle.first as! ResultNativeAd
        }
        return adView
    }
    
    override func awakeFromNib() {
        self.adButton.backgroundColor = UIColor.markBtnGreenColor()
        self.adButton.setTitleColor(UIColor.white, for: UIControlState())
        self.adButton.titleLabel?.font = HSFont.baseRegularFont(16)
        self.adButton.layer.cornerRadius = 2
        self.adTitle.numberOfLines = 0
        self.adTitle.font = HSFont.baseRegularFont(16)
        self.adTitle.textColor = UIColor.black
        self.adTitle.textAlignment = .center
        adWhiteIma.image = UIImage(named: "adWhite")
        self.adButton!.addSubview(adWhiteIma)
        adWhiteIma.snp.makeConstraints { (make) in
            make.top.equalTo(self.adButton!)
            make.bottom.equalTo(self.adButton!)
            make.width.equalTo(50)
            make.left.equalTo(self.adButton!.snp.left).offset(-50)
        }
    }
    
    func requestAd(_ vc:UIViewController?, view:UIView, adInfo:SingleAdInfo) {
        self.rootVc = vc
        self.adInfo = adInfo
        self.backgroundColor = UIColor.white
        self.tag = 110
        self.isUserInteractionEnabled = true
        view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        //        dispatch_async(dispatch_get_main_queue(), {
        self.fbAD=FBNativeAd(placementID:self.adInfo?.adUnitId ?? "")
        self.fbAD?.delegate=self
        self.fbAD?.load()
        //        })
    }
    
    func showAd(_ vc:UIViewController) {
        fbAD?.registerView(forInteraction: self, with: vc)
        Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(tickDown), userInfo: nil, repeats: true)
    }
    
    func addObserver(_ requestComplete:@escaping AdRequestComplete) {
        self.adRequestComplete = requestComplete
    }
    
    // MARK: FBNativeAdDelegate
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:\(error)")
        LXSWLog(error)
        self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
        self.removeFromSuperview()
    }
    
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        self.adTitle.text = nativeAd.title
        //        self.adchoiceView = FBAdChoicesView(nativeAd:nativeAd)
        //        self.addSubview(adchoiceView)
        //        self.adchoiceView?.updateFrameFromSuperview()
        nativeAd.registerView(forInteraction: self, with:self.rootVc)
        self.mediaView.nativeAd = nativeAd
        self.adIcon.image = UIImage(named: "ic_fb_ad")
        self.adButton.setTitle(nativeAd.callToAction, for:UIControlState())
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
        nativeAd.coverImage?.loadAsync(block: { (image) in
            if let _ = image {
                self.titleH.constant = AdaptiveUtils.getH(20)
                self.actionH.constant = AdaptiveUtils.getH(38)
                let width = AdaptiveUtils.screenWidth
                let adHeight:CGFloat = AdaptiveUtils.getH(248)+20
                self.adRequestComplete?(CGSize(width: width, height: adHeight), self, self, true)
            } else {
                LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:image load failure")
                self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
                self.removeFromSuperview()
            }
        })
    }
    
    @objc func tickDown() {
        adWhiteIma.snp.updateConstraints { (make) in
            UIView.animate(withDuration: 0.7, animations: {
                self.adWhiteIma.snp.updateConstraints({ (make) in
                    make.left.equalTo(self.adButton!.snp.left).offset(AdaptiveUtils.screenWidth-48)
                })
                self.adWhiteIma.isHidden = false
                self.adWhiteIma.superview?.layoutIfNeeded()
                }, completion: { (complete) in
                    self.adWhiteIma.snp.updateConstraints({ (make) in
                        make.left.equalTo(self.adButton!.snp.left).offset(-50)
                    })
                    self.adWhiteIma.isHidden = true
                    self.adWhiteIma.superview?.layoutIfNeeded()
            })
        }
    }
    
    func nativeAdWillLogImpression(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adImpression(adInfo)
    }
    
    //optimize the compilation by haoxudong
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adClick(adInfo)
        let adUnitPlatform = DataTypeUtil.isNull(self.adInfo?.adUnitPlatform)
        let placementKey = DataTypeUtil.isNull(self.adInfo?.placementKey)
        let adUnitType = DataTypeUtil.isNull(self.adInfo?.adUnitType)
        let contentId = adUnitPlatform + "_" + placementKey
        let contentType = adUnitPlatform + "_" + adUnitType
        AnaliticsManager.logPurchase(1, currency: "USD", contentId: contentId, contentType: contentType)
    }
}
