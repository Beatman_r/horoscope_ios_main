//
//  AdmobRectAd.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdmobRectAd: GADBannerView,GADBannerViewDelegate,BaseInnerAd {
    
    var adInfo: SingleAdInfo?
    var adRequestComplete: AdRequestComplete?
    
    func requestAd(_ vc:UIViewController?, view:UIView, adInfo:SingleAdInfo) {
        self.adInfo = adInfo
        self.adUnitID = adInfo.adUnitId
        self.delegate = self
        self.adSize = kGADAdSizeMediumRectangle
        self.adSize = kGADAdSizeBanner
        self.rootViewController = vc
        self.tag = 110
        view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        let request = GADRequest()
        self.load(request)
    }

    func showAd(_ vc:UIViewController) {
        self.rootViewController = vc
    }
    
    func addObserver(_ requestComplete:@escaping AdRequestComplete) {
        self.adRequestComplete = requestComplete
    }
    
    // MARK: GADBannerViewDelegate
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
        self.adRequestComplete?(CGSize(width: 300, height: 250), self, self, true)
        self.superview?.bringSubview(toFront: self)
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:\(error)")
        self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
        self.isHidden = true
        self.removeFromSuperview()
    }
    
    // optimize the compilation by haoxudong
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        let adUnitPlatform = DataTypeUtil.isNull(self.adInfo?.adUnitPlatform)
        let placementKey = DataTypeUtil.isNull(self.adInfo?.placementKey)
        let adUnitType = DataTypeUtil.isNull(self.adInfo?.adUnitType)
        let contentId = adUnitPlatform + "_" + placementKey
        let contentType = adUnitPlatform + "_" + adUnitType
        AnaliticsManager.logPurchase(1, currency: "USD", contentId: contentId, contentType: contentType)
    }
}
