//
//  FBHeight100Ad.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import FBAudienceNetwork

class FBHeight100Ad: UIView,FBNativeAdDelegate,BaseInnerAd {
    
    var fbAD: FBNativeAd?
    var nativeAdView: FBNativeAdView?
    var adInfo: SingleAdInfo?
    var rootVc: UIViewController?
    var adRequestComplete: AdRequestComplete?
    
    func requestAd(_ vc:UIViewController?, view:UIView,adInfo:SingleAdInfo) {
        self.rootVc = vc
        self.adInfo = adInfo
        self.backgroundColor = UIColor.planBackColor()
        self.isUserInteractionEnabled = true
        self.tag = 110
        view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        self.fbAD = FBNativeAd(placementID:self.adInfo?.adUnitId ?? "")
        fbAD?.delegate = self
        self.fbAD?.load()
    }

    func showAd(_ vc:UIViewController) {
        fbAD?.registerView(forInteraction: self, with: vc)
    }
    
    func addObserver(_ requestComplete:@escaping AdRequestComplete) {
        self.adRequestComplete = requestComplete
    }
    
    // MARK: FBNativeAdDelegate
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:\(error)")
        LXSWLog(error)
        self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
        self.removeFromSuperview()
    }
    
    func nativeAdDidLoad(_ nativeAd: FBNativeAd){
        self.nativeAdView = FBNativeAdView(nativeAd: nativeAd, with: FBNativeAdViewType.genericHeight100)
        LXSWLog(nativeAdView?.frame.size.width)
        nativeAd.coverImage?.loadAsync(block: { (image) in
        })
        self.addSubview(nativeAdView!)
        self.nativeAdView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        })
        nativeAd.registerView(forInteraction: self, with: self.rootVc)
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
        self.adRequestComplete?(CGSize(width: AdaptiveUtils.screenWidth, height: 100), self, self, true)
    }
    
    //optimize the compilation by haoxudong
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        let platform = DataTypeUtil.isNull(self.adInfo?.adUnitPlatform)
        let placementKey = DataTypeUtil.isNull(self.adInfo?.placementKey)
        let adUnitType = DataTypeUtil.isNull(self.adInfo?.adUnitType)
        let contentId = platform + "_" + placementKey
        let contentType = platform + "_" + adUnitType
        AnaliticsManager.logPurchase(1, currency: "USD", contentId: contentId, contentType: contentType)
    }
}
