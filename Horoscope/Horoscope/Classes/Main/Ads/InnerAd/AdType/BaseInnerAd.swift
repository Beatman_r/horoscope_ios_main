//
//  BaseInnerAd.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

typealias AdRequestComplete = (_ size:CGSize, _ adView:UIView, _ adAdmin:BaseInnerAd, _ isSuccess:Bool) -> Void

protocol BaseInnerAd {
    
    // MARK: Public Attribute
    var adInfo: SingleAdInfo?{get set}
    var adRequestComplete: AdRequestComplete?{get set}
    
    // MARK: Public Method
    func requestAd(_ vc: UIViewController?, view: UIView, adInfo: SingleAdInfo)
    func showAd(_ vc:UIViewController)
    func addObserver(_ requestComplete:@escaping AdRequestComplete)
}
