//
//  VerseNativeAd.swift
//  bibleverse
//
//  Created by 麦子 on 2017/2/27.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import FBAudienceNetwork

class VerseNativeAd: UIView,FBNativeAdDelegate,BaseInnerAd {
    
    @IBOutlet weak var mediaView: FBMediaView!
    @IBOutlet weak var adIcon: UIImageView!
    @IBOutlet weak var adTitle: UILabel!
    @IBOutlet weak var adButton: UIButton!
    
    var adchoiceView: FBAdChoicesView!
    var fbAD: FBNativeAd?
    var adInfo: SingleAdInfo?
    var rootVc: UIViewController?
    var adRequestComplete: AdRequestComplete?
    
    class func create() -> VerseNativeAd {
        var adView = VerseNativeAd()
        if let bundle = Bundle.main.loadNibNamed("VerseNativeAd", owner:nil, options: nil) {
            adView = bundle.first as! VerseNativeAd
        }
        return adView
    }
    override func awakeFromNib() {
        self.adButton.layer.masksToBounds = true
        self.adButton.layer.cornerRadius = 15
        self.adButton.layer.borderWidth = 1
        self.adButton.layer.borderColor = UIColor.flatGreen.cgColor
        self.adButton.backgroundColor = UIColor.white
        self.adButton.setTitleColor(UIColor.flatGreen, for: .normal)
        self.adButton.titleLabel?.font = HSFont.baseRegularFont(16)
        self.adTitle.numberOfLines = 0
        self.adTitle.font = HSFont.baseRegularFont(16)
        self.adTitle.textColor = UIColor.black
        self.adTitle.textAlignment = .left
    }
    
    func requestAd(_ vc:UIViewController?, view:UIView, adInfo:SingleAdInfo) {
        self.rootVc = vc
        self.adInfo = adInfo
        self.backgroundColor = UIColor.white
        self.tag = 110
        self.isUserInteractionEnabled = true
        view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
//        dispatch_async(dispatch_get_main_queue(), {
            self.fbAD = FBNativeAd(placementID:self.adInfo?.adUnitId ?? "")
            self.fbAD?.delegate = self
            self.fbAD?.load()
//        })
    }
    
    func showAd(_ vc:UIViewController) {
        fbAD?.registerView(forInteraction: self, with: vc)
    }
    
    func addObserver(_ requestComplete:@escaping AdRequestComplete) {
        self.adRequestComplete = requestComplete
    }
    
    // MARK: FBNativeAdDelegate
    func nativeAd(_ nativeAd: FBNativeAd, didFailWithError error: Error) {
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:\(error)")
        LXSWLog(error)
        self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
        self.removeFromSuperview()
    }
    func nativeAdDidLoad(_ nativeAd: FBNativeAd) {
        self.adTitle.text = nativeAd.title
        //        self.adchoiceView = FBAdChoicesView(nativeAd:nativeAd)
        //        self.addSubview(adchoiceView)
        //        self.adchoiceView?.updateFrameFromSuperview()
        nativeAd.registerView(forInteraction: self, with:self.rootVc)
        self.mediaView.nativeAd = nativeAd
        self.adIcon.image = UIImage(named: "ic_fb_ad")
        self.adButton.setTitle(nativeAd.callToAction, for:UIControlState())
        LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
        nativeAd.coverImage?.loadAsync(block: { (image) in
            if let adImage = image {
                let screenBounds = UIScreen.main.bounds
                LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- success")
                let width = screenBounds.width
                let height = adImage.size.height/adImage.size.width*(screenBounds.width-adSpace*2) + 58
                self.adRequestComplete?(CGSize(width: width, height: height), self, self, true)
            } else {
                LXSWLog("ad load id : \(self.adInfo?.adUnitId ?? "") -- failure \n failure:image load failure")
                self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
                self.removeFromSuperview()
            }
        })
    }
    
    func nativeAdWillLogImpression(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adImpression(adInfo)
    }
    
    //optimize the compilation by haoxudong
    func nativeAdDidClick(_ nativeAd: FBNativeAd) {
        LTVManager.shareInstance.adClick(adInfo)
        let adUnitPlatform = DataTypeUtil.isNull(self.adInfo?.adUnitPlatform)
        let placementKey = DataTypeUtil.isNull(self.adInfo?.placementKey)
        let adUnitType = DataTypeUtil.isNull(self.adInfo?.adUnitType)
        let contentId = adUnitPlatform + "_" + placementKey
        let contentType = adUnitPlatform + "_" + adUnitType
        AnaliticsManager.logPurchase(1, currency: "USD", contentId: contentId, contentType: contentType)
    }
}
