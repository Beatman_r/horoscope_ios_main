//
//  SeeAllAdvanceAd.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/18.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SeeAllAdvanceAd: UIView,BaseInnerAd,GADNativeContentAdLoaderDelegate,GADNativeAppInstallAdLoaderDelegate,GADNativeAdDelegate {
    
    var adInfo: SingleAdInfo?
    var adRequestComplete: AdRequestComplete?
    var adLoader: GADAdLoader?
    var adNative: GADNativeAd?
    
    func requestAd(_ vc: UIViewController?, view: UIView, adInfo: SingleAdInfo) {
        // "ca-app-pub-3940256099942544/3986624511"
        // kGADAdLoaderAdTypeNativeAppInstall, kGADAdLoaderAdTypeNativeContent
        self.adInfo = adInfo
        let option = GADNativeAdImageAdLoaderOptions()
        option.preferredImageOrientation = .portrait
        self.adLoader = GADAdLoader.init(adUnitID: adInfo.adUnitId ?? "", rootViewController: vc, adTypes: [kGADAdLoaderAdTypeNativeAppInstall, kGADAdLoaderAdTypeNativeContent], options: [option])
        self.tag = 110
        adLoader?.delegate = self
        view.addSubview(self)
        self.snp.makeConstraints { (make) in
            make.edges.equalTo(view).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        self.adLoader?.load(GADRequest())
    }
    
    func showAd(_ vc:UIViewController) {
        self.adNative?.rootViewController = vc
    }
    
    func addObserver(_ requestComplete:@escaping AdRequestComplete) {
        self.adRequestComplete = requestComplete
    }
    
    // MARK: GADNativeAppInstallAdLoaderDelegate
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAppInstallAd: GADNativeAppInstallAd) {
        self.adNative = nativeAppInstallAd
        self.adNative?.delegate = self
        if let image = (nativeAppInstallAd.images?.first as? GADNativeAdImage)?.image {
            let appInstallAdView = Bundle.main.loadNibNamed("SeeAllInstall", owner: nil, options: nil)?.first as! SeeAllInstall
            self.addSubview(appInstallAdView)
            appInstallAdView.snp.makeConstraints({ (make) in
                make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 0, 0))
            })
            appInstallAdView.refreshUI(nativeAppInstallAd, image:image)
            let width = AdaptiveUtils.screenWidth-adSpace*2
            var imaHeight:CGFloat = 0
            if image.size.height < image.size.width {
                imaHeight = (image.size.height/image.size.width*width) + 90
            } else {
                let scale:CGFloat = 359/688.41
                imaHeight = (AdaptiveUtils.screenWidth-adSpace*2)*scale + 90
            }
            let titleHeight = HSHelpCenter.sharedInstance.textTool.calculateLabelHeight(nativeAppInstallAd.headline, font: HSFont.baseRegularFont(16), width: AdaptiveUtils.screenWidth-26)
            let contentHeight = HSHelpCenter.sharedInstance.textTool.calculateLabelHeight(nativeAppInstallAd.body, font: HSFont.baseRegularFont(16), width: AdaptiveUtils.screenWidth-26)
            let adHeight = imaHeight+titleHeight+contentHeight
            self.adRequestComplete?(CGSize(width: AdaptiveUtils.screenWidth, height: adHeight), self, self, true)
            self.superview?.bringSubview(toFront: self)
        } else {
            self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
            self.removeFromSuperview()
        }
    }
    
    // MARK: GADNativeContentAdLoaderDelegate
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeContentAd: GADNativeContentAd) {
        self.adNative = nativeContentAd
        self.adNative?.delegate = self
        if let image = (nativeContentAd.images?.first as? GADNativeAdImage)?.image {
            let contentAdView = Bundle.main.loadNibNamed("SeeAllContent", owner: nil, options: nil)?.first as! SeeAllContent
            self.addSubview(contentAdView)
            contentAdView.snp.makeConstraints({ (make) in
                make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 0, 0))
            })
            contentAdView.refreshUI(nativeContentAd, image:image)
            let width = AdaptiveUtils.screenWidth-adSpace*2
            var imaHeight:CGFloat = 0
            if image.size.height < image.size.width {
                imaHeight = (image.size.height/image.size.width*width) + 90
            } else {
                let scale:CGFloat = 359/688.41
                imaHeight = (AdaptiveUtils.screenWidth-adSpace*2)*scale + 90
            }
            let titleHeight = HSHelpCenter.sharedInstance.textTool.calculateLabelHeight(nativeContentAd.headline, font: HSFont.baseRegularFont(16), width: AdaptiveUtils.screenWidth-26)
            let contentHeight = HSHelpCenter.sharedInstance.textTool.calculateLabelHeight(nativeContentAd.body, font: HSFont.baseRegularFont(16), width: AdaptiveUtils.screenWidth-26)
            let adHeight = imaHeight+titleHeight+contentHeight
            self.adRequestComplete?(CGSize(width: AdaptiveUtils.screenWidth, height: adHeight), self, self, true)
            self.superview?.bringSubview(toFront: self)
        } else {
            self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
            self.removeFromSuperview()
        }
    }
    
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        LXSWLog("Advance--seeall:\(error.localizedDescription)")
        self.adRequestComplete?(CGSize(width: 0, height: 0), self, self, false)
        self.removeFromSuperview()
    }
    
    func nativeAdWillPresentScreen(_ nativeAd: GADNativeAd) {
        
//        LTVManager.shareInstance.adImpression(adInfo)
    }
    
    func nativeAdWillLeaveApplication(_ nativeAd: GADNativeAd) {
        LTVManager.shareInstance.adClick(adInfo)
        if self.adInfo?.placementKey == AdPlacementKey.MyFortuneNative_003.rawValue{
            AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a2_ad_click":"admob"])
        }
    }
    

}
