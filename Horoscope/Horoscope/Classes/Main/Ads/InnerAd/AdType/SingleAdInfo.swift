//
//  SingleAdInfo.swift
//  bibleverse
//
//  Created by 麦子 on 16/10/18.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class SingleAdInfo: NSObject {
    
    var adUnitId: String?
    var adUnitPlatform: String?
    var adUnitPriority: Int?
    var adUnitType: String?
    var placementKey: String?
    
}
