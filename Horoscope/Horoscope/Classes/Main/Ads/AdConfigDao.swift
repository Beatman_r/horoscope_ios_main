//
//  AdConfigDao.swift
//  bibleverse
//
//  Created by 麦子 on 2016/11/23.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

protocol AdConfigProtocol {
    /**
     get ad config data
     */
    func asyncGetConfigData(_ success: @escaping (_ object:AnyObject) -> (), failure: @escaping (_ error:NSError) -> ())
}

class AdConfigDao: BaseDao,AdConfigProtocol {
    
    fileprivate let configVersionKey = "AdConfigDao.configVersion"
    
    func asyncGetConfigData(_ success: @escaping (_ object:AnyObject) -> (), failure: @escaping (_ error:NSError) -> ()) {
        var configVersion:String = "-1"
        if let version = UserDefaults.standard.object(forKey: configVersionKey) as? String {
            configVersion = version
        }
        self.GET(AWSUrlDao.config, parameters: ["configVersion":configVersion as AnyObject], success: { (jsonData) in
            LXSWLog(jsonData)
            if let version = jsonData?["configVersion"].int {
                let userDefault = UserDefaults.standard
                userDefault.set("\(version)", forKey: self.configVersionKey)
                userDefault.synchronize()
            }
            if let config = jsonData?["config"].object {
                LXSWLog(config)
                success(config as AnyObject)
            } else {
                LXSWLog("has no new message")
            }
        }) { (error) in
            LXSWLog(error)
            failure(error)
        }
    }
}
