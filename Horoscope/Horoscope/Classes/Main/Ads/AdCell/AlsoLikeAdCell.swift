//
//  AlsoLikeAdCell.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/5.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit

class AlsoLikeAdCell: UITableViewCell {
    
    var hasAd = false
    
    func refreshUI(_ adContainer:InnerAdContainer?) {
        self.selectionStyle = .none
        if hasAd == true {
            return
        }
        if let adView = adContainer {
            hasAd = true
            adView.alpha = 0
            self.contentView.backgroundColor = UIColor.white
            self.contentView.addSubview(adView)
            adView.snp.makeConstraints { (make) in
                make.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(0, 0, 1, 0))
            }
            self.layoutIfNeeded()
            UIView.animate(withDuration: 1, animations: {
                adView.alpha = 1
            })
        }
    }
    
    static let cellId = "AlsoLikeAdCell"
    class func dequeueReusableCell(_ tableView: UITableView) -> AlsoLikeAdCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? AlsoLikeAdCell
        if  cell == nil {
            cell = AlsoLikeAdCell.init(style: .default, reuseIdentifier: cellId)
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
