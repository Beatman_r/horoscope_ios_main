//
//  VerseAdCell.swift
//  bibleverse
//
//  Created by 麦子 on 2016/12/22.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class VerseAdCell: UITableViewCell {
    
    var hasAd = false
    
    func refreshUI(_ adContainer:InnerAdContainer?) {
        self.selectionStyle = .none
        if hasAd == true {
            return
        }
        if let adView = adContainer {
            hasAd = true
            adView.alpha = 0
            self.contentView.backgroundColor = UIColor.planBackColor()
            self.contentView.addSubview(adView)
            adView.snp.makeConstraints { (make) in
                make.edges.equalTo(self.contentView).inset(UIEdgeInsetsMake(10, 0, 10, 0))
            }
            self.layoutIfNeeded()
            UIView.animate(withDuration: 1, animations: {
                adView.alpha = 1
            })
        }
    }

    static let cellId = "VerseAdCell"
    class func dequeueReusableCell(_ tableView: UITableView) -> VerseAdCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId) as? VerseAdCell
        if  cell == nil {
            cell = VerseAdCell.init(style: .default, reuseIdentifier: cellId)
        }
        cell?.selectionStyle = .none
        return cell!
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
