//
//  WebViewAdView.swift
//  bibleverse
//
//  Created by 麦子 on 2017/1/18.
//  Copyright © 2017年 qi. All rights reserved.
//

import UIKit

class WebViewAdView: UIView {

    var hasAd = false
    
    func refreshUI(_ adContainer:InnerAdContainer?) {
        if hasAd == true {
            return
        }
        if let adView = adContainer {
            hasAd = true
            adView.alpha = 0
            self.backgroundColor = UIColor.white
            self.addSubview(adView)
            adView.snp.makeConstraints { (make) in
                make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 1, 0))
            }
            self.layoutIfNeeded()
            UIView.animate(withDuration: 1, animations: {
                adView.alpha = 1
            })
        }
    }
}
