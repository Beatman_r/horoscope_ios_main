//
//  HomeTimeLineViewController.swift
//  Horoscope
//
//  Created by Wang on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

protocol HomeTimeLineDelegate : class {
    func getCurrentContentOffsetY(_ offsetY : CGFloat)
}

class HomeTimeLineViewController: BaseViewController,NotificationCellDelegate,NormalPostCellDelegate,InnerAdContainerdelegate{
    
    let screenWidth = UIScreen.main.bounds.width
    weak var delegate : HomeTimeLineDelegate?
    var wheatherToShowGuideNotif:Bool?
    var needRefresh1 = true
    var needRefresh2 = true
    var needRefresh3 = true
    
    var hasLoadedData:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadAD()
        self.setupRefreshFooterView()
        self.setupRefreshHeaderView()
        self.addRetryView()
//        self.addRoundMenu()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationStatusChange(_:)), name: NSNotification.Name(rawValue: NotificationAuthorizationStatus), object: nil)
       NotificationCenter.default.addObserver(self, selector: #selector(getInsertModel(_:)), name: NSNotification.Name(rawValue: "topicDidPost"), object: nil)
        self.wheatherToShowGuideNotif = !(UserDefaults.standard.bool(forKey: PushNotificationAllowed))
    }
    
    var adView : InnerAdContainer = InnerAdContainer()
    func loadAD() {
        let view = InnerAdContainer()
        view.delegate = self
        let adInfo = SingleAdInfo()
        adInfo.placementKey = "dailyCard1"
        view.requestAd(self, adInfo: adInfo, isStepLoad: false)
        self.adView = view
    }
    
    @objc func notificationStatusChange(_ notification:Notification){
        if notification.userInfo != nil {
            self.wheatherToShowGuideNotif = !(notification.userInfo![NotifAuthorizationStatusKey] as? Bool ?? false)
        }
        self.baseTableView?.reloadData()
    }
    
  
    
    var viewmodel:HomeConfigViewModel?
    override func loadData(){
        if self.hasLoadedData == false{
            viewmodel = HomeConfigViewModel.init(key: self.horoName ?? "")
            viewmodel?.loadConfig({
                self.failedRetryView?.loadSuccess()
                self.baseTableView?.mj_header.endRefreshing()
                self.baseTableView?.reloadData()
                self.hasLoadedData = true
            }, failure: { (error) in
                self.failedRetryView?.loadFailed()
                self.baseTableView?.mj_header.endRefreshing()
                self.hasLoadedData = true
            })
        }
    }
    
    @objc func getInsertModel(_ noti:Notification) {
        let model = noti.object as! ReadPostModel
        viewmodel?.insertOneNewPost(model, success: {
            let ind = IndexPath.init(row: 0, section: 1)
            self.baseTableView?.reloadData()
            self.baseTableView?.scrollToRow(at: ind, at: .top, animated: true)
        }, failure: { (error) in
            
        })
    }
    
    var hasAd : Bool = false
    
    func requestAdSuccess(_ size: CGSize, adView: InnerAdContainer) {
        if hasAd == true {
            return
        }else {
            self.hasAd = true
            self.baseTableView?.reloadData()
        }
    }
    
    //RetryViewDelegate
    override func retryToLoadData() {
        viewmodel?.loadConfig({
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.mj_header.endRefreshing()
            self.baseTableView?.reloadData()
            self.hasLoadedData = true
        }, failure: { (error) in
            self.failedRetryView?.loadFailed()
            self.baseTableView?.mj_header.endRefreshing()
            self.hasLoadedData = true
        })
    }
    
    func getCurrentOffset() {
        self.delegate?.getCurrentContentOffsetY((self.baseTableView?.contentOffset.y)!)
    }
    
    override func registerCell() {
        self.baseTableView?.register(RateUsCell.self, forCellReuseIdentifier: "RateUsCell")
        self.baseTableView?.register(BaseHomeCell.self, forCellReuseIdentifier: "BaseHomeCell")
        self.baseTableView?.register(VideoCell.self, forCellReuseIdentifier: "videoListCell")
        self.baseTableView?.register(AllVideoCell.self, forCellReuseIdentifier: "AllVideoCell")
        self.baseTableView?.register(LuckyHeadCell.self, forCellReuseIdentifier: "LuckyHeadCell")
        self.baseTableView?.register(DailyMatchCell.self, forCellReuseIdentifier: "matchCell")
        self.baseTableView?.register(DailyFortuneCell.self, forCellReuseIdentifier: "characterCell")
        self.baseTableView?.register(SelectZodiacCell.self, forCellReuseIdentifier: "SelectZodiacCell")
        self.baseTableView?.register(NotificationCell.self, forCellReuseIdentifier: "notificationCell")
        self.baseTableView?.register(VideoPostCell.self, forCellReuseIdentifier: "VideoPostCell")
        self.baseTableView?.register(NormalPostCell.self, forCellReuseIdentifier: "NormalPostCell")
        self.baseTableView?.register(HotVideoPostCell.self, forCellReuseIdentifier: "HotVideoPostCell")
        self.baseTableView?.register(DIGTimelineCell.self, forCellReuseIdentifier: "digCell")
        self.baseTableView?.register(DailyADCardCell.self, forCellReuseIdentifier: "DailyADCardCell")
        self.baseTableView?.register(SurveyTimeLineCell.self, forCellReuseIdentifier: "surveyCell")
    }
    
    var offY : CGFloat = 0.0
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        offY = scrollView.contentOffset.y-20
        if offY < 400 {
            NotificationCenter.default.post(name: Notification.Name(rawValue: noti_didChangeColor), object: nil, userInfo: ["offsetY" : offY])
            let kColor = offY/400
            self.view.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: CGFloat(kColor))
        }
        self.changeHeightRowNum = 3000
    }
    func addRoundMenu() {
        let menuWidth = SCREEN_WIDTH * 50/375
        let pendding : CGFloat = 20.0
        let roundMenu = RoundMenuView.init(frame: CGRect(x: SCREEN_WIDTH - menuWidth - pendding, y: SCREEN_HEIGHT - menuWidth - pendding, width: menuWidth, height: menuWidth))
        roundMenu.rootVC = self
        roundMenu.horoName = self.horoName ?? ""
        self.view.addSubview(roundMenu)
        self.view.bringSubview(toFront: roundMenu)
    }
    //MARK:TableViewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.viewmodel?.getCount(section) ?? 0
    }
    
    
    //MARK:TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            let model = viewmodel?.getSection1Model(indexPath.section, row: indexPath.row)
            if model?.type == .video{
                let videomodel = model as? ReadPostModel
                let vc = VideoPlayViewController.create(videomodel?.video?.richmedia, postId: videomodel?.postId ?? "")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if model?.type == .normal{
                let postmodel = model as? ReadPostModel
                let vc = ReadPostViewController()
                vc.postModel = MyPostModel.init(model: postmodel!)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    var topHeight : CGFloat = 0
    var belowHeight : CGFloat = 0
    var topHeightOfTop : CGFloat = 0
    var belowHeightOfTop : CGFloat = 0
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let model = self.viewmodel?.getSection0Model(indexPath.row)
            if model?.category == .emptyHoroscope{
                let cell = SelectZodiacCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                cell.rootVC = self
                return cell
            }
            if model?.category == .todayHoroscope{
                let cell  = LuckyHeadCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                cell.rootVC = self
                cell.model = model as? LuckyModel
                cell.horoName = self.horoName ?? ""
                return cell
            }
            if model?.category == .campaign{
                let campaignModel = model as? CardCampaignModel
                if campaignModel?.cate == .cardCookie {
                    let cell = BaseHomeCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                    cell.rootVc = self
                    cell.model = campaignModel
                    return cell
                }
                if campaignModel?.cate == .cardNotification {
                    if self.wheatherToShowGuideNotif == true {
                        let cell = NotificationCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                        cell.delegate = self
                        cell.model = campaignModel
                        return cell
                    }else{                            return UITableViewCell()
                    }
                }
                if campaignModel?.cate == .cardTarot{
                    let cell = BaseHomeCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                    cell.rootVc = self
                    cell.model = campaignModel
                    return cell
                }
            }
            if model?.category == .dig{
                let cell = tableView.dequeueReusableCell(withIdentifier: "digCell", for: indexPath) as! DIGTimelineCell
                cell.rootvc = self
                return cell
            }
            if model?.category == .dailyCard1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DailyADCardCell", for: indexPath) as! DailyADCardCell
                cell.addSubview(self.adView)
                adView.layer.cornerRadius = 4
                adView.layer.masksToBounds = true
                adView.snp.makeConstraints({ (make) in
                    make.top.left.equalTo(10)
                    make.bottom.right.equalTo(-10)
                })
                return cell
            }
            if model?.category == .survey {
                let campaignModel = model as? SurveyCellModel
                let cell = SurveyTimeLineCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath) as! SurveyTimeLineCell
                cell.rootVc = self
                cell.surveyModel = campaignModel
                return cell
            }
            if model?.category == .match{
                let cell = tableView.dequeueReusableCell(withIdentifier: "matchCell", for: indexPath) as! DailyMatchCell
                cell.rootVC = self
                let zodiacName = self.horoName ?? ""
                if zodiacName.count == 0 {
                    
                }else {
                    cell.leftImageView.image = UIImage(named: "ic_\(zodiacName)_match")
                    cell.zodiacName = zodiacName
                    cell.leftFlag = true
                }
                return cell
            }
            if model?.category == .forecast{
                let cell = tableView.dequeueReusableCell(withIdentifier: "characterCell", for: indexPath) as! DailyFortuneCell
                let forecast = model as? ForecastPart
                cell.dataModel=forecast
                cell.rootVC = self
                let tomorrowList = cell.dataModel?.list?.first?.commentList ?? []
                if tomorrowList.count > 0 {
                    if tomorrowList.count == 1 {
                        cell.commentPreLookBar!.isHidden = false
                        let topComment = tomorrowList.first?.content ?? ""
                        let topStrHeight = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: topComment, fontSize: 15, font: HSFont.baseRegularFont(15), width: SCREEN_WIDTH - SCREEN_WIDTH*24/375, multyplyLineSpace: 1.25)
                        if tomorrowList.first?.imageList?.count > 0 {
                            self.topHeight = SCREEN_HEIGHT * 172/667
                            self.topHeight = topStrHeight + self.topHeight
                        }else {
                            self.topHeight = SCREEN_HEIGHT * 160/667
                            self.topHeight = topStrHeight + self.topHeight
                        }
                        cell.commentPreLookBar?.removeBelows()
                    }else if tomorrowList.count == 2 {
                        cell.commentPreLookBar!.isHidden = false
                        cell.commentPreLookBar!.showBelow()
                        let topComment = tomorrowList.first?.content ?? ""
                        let topStrHeight = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: topComment, fontSize: 15, font: HSFont.baseRegularFont(15), width: SCREEN_WIDTH - SCREEN_WIDTH*24/375, multyplyLineSpace: 1.25)
                        if tomorrowList.first?.imageList?.count > 0{
                            self.topHeight = SCREEN_HEIGHT * 160/667
                            self.topHeight = topStrHeight + self.topHeight
                        }else {
                            self.topHeight = SCREEN_HEIGHT * 148/667
                            self.topHeight = topStrHeight + self.topHeight
                        }
                        let belowComment = tomorrowList[1].content ?? ""
                        let belowStrHeight = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: belowComment, fontSize: 15, font: HSFont.baseRegularFont(15), width: SCREEN_WIDTH - SCREEN_WIDTH*24/375, multyplyLineSpace: 1.25)
                        self.belowHeight = tomorrowList.first?.imageList?.count > 0 ? SCREEN_HEIGHT * 160/667 : SCREEN_HEIGHT * 148/667
                        self.belowHeight = belowStrHeight + self.belowHeight
                        if tomorrowList[1].imageList?.count > 0 {
                            self.belowHeight = belowStrHeight + self.belowHeight + SCREEN_HEIGHT * 14/667
                        }
                        let count = cell.dataModel?.list?.first?.commentCount ?? 0
                        if count > 2 {
                            self.belowHeight += SCREEN_HEIGHT * 50/667
                            cell.commentPreLookBar!.viewAllCommentLabel.isHidden = false
                        }else {
                            cell.commentPreLookBar!.viewAllCommentLabel.isHidden = true
                        }
                    }
                }else {
                    cell.commentPreLookBar!.isHidden = true
                }
                return cell
            }
            if model?.category == .rateUs{
                if UserDefaults.standard.bool(forKey: RateUsCell.ratedTag) == true {
                    return UITableViewCell()
                }else{
                    let cell  = RateUsCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                    cell.rootvc = self
                    cell.model = model
                    return cell
                }
            }
        }else if indexPath.section == 1{
            let model = viewmodel?.getSection1Model(indexPath.section, row: indexPath.row)
            if model?.type == .video{
                let videomodel = model as? ReadPostModel
                if videomodel?.video?.figure?.isEmpty == false{
                    let cell = HotVideoPostCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                    cell.delegate=self
                    cell.model = videomodel
                    cell.rootVc = self
                    return cell
                }else {
                    let cell = VideoPostCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                    cell.delegate=self
                    cell.model = videomodel
                    cell.rootVc = self
                    return cell
                }
            }
            
            if model?.type == .normal{
                let cell = NormalPostCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                let videomodel = model as? ReadPostModel
                cell.readModel = videomodel
                cell.rootVc=self
                cell.delegate = self
                cell.readModel?.cellRowNumber = indexPath.row
                return cell
            }
        }
        return UITableViewCell()
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            let model = self.viewmodel?.getSection0Model(indexPath.row)
            if model?.category == .emptyHoroscope{
                return SelectZodiacCell.cellHeight()
            }
            if model?.category == .todayHoroscope{
                return LuckyHeadCell.calculateHeight("") + self.topHeightOfTop + self.belowHeightOfTop
            }
            if model?.category == .dailyCard1 {
                if self.hasAd == true {
                    return 150 * SCREEN_HEIGHT / 667
                }
                return 0
            }
            if model?.category == .forecast{
                return DailyFortuneCell.cellHeight() + topHeight + belowHeight
            }
            if model?.category == .campaign{
                let campaignModel = model as? CardCampaignModel
                if campaignModel?.cate == .cardNotification{
                    return (self.wheatherToShowGuideNotif == true ? (NotificationCell.calculateHeight(campaignModel?.title)) : 0)
                }
                if campaignModel?.cate == .cardCookie{
                    return BaseHomeCell.calculateHeight(campaignModel?.title)
                }
                if campaignModel?.cate == .cardTarot{
                    return BaseHomeCell.calculateHeight(campaignModel?.title)
                }
            }
            if model?.category == .match{
                return DailyMatchCell.cellHeight()
            }
            
            if model?.category == .rateUs{
                return RateUsCell.calculateHeight()
            }
            if model?.category == .dig {
                return DIGTimelineCell.cellHeight()
            }
            if model?.category == .survey {
                let campaignModel = model as? SurveyCellModel
                return SurveyTimeLineCell.calculateHeight(campaignModel?.surveyTitle)
            }
        }  else if indexPath .section == 1 {
            let model = viewmodel?.getSection1Model(0, row: indexPath.row)
            let postModel = model as? ReadPostModel
            if model?.type == .video{
                let videomodel = model as? ReadPostModel
                if videomodel?.video?.figure?.isEmpty == false{
                    return HotVideoPostCell.calculateHeight(postModel)
                }else {
                    return VideoPostCell.calculateHeight(postModel)
                }
            }
            if model?.type == .normal{
                if indexPath.row == self.changeHeightRowNum {
                    return NormalPostCell.calculateheight(postModel) + NormalPostCell.calculateTagListHeight(postModel?.tagList ?? [])
                }else {
                    return NormalPostCell.calculateheight(postModel)
                }
            }
        }
        return 0
    }
    var postShouldShowAllTag : Bool = false
    var changeHeightRowNum : Int?
    func shouldChangeCellHeight(_ rowNum: Int) {
        let cellIndexPath = IndexPath(row: rowNum, section: 1)
        let cell = self.baseTableView?.cellForRow(at: cellIndexPath) as! NormalPostCell
        cell.readModel?.shouldShowAllTag = true
        self.postShouldShowAllTag = true
        self.changeHeightRowNum = rowNum
        self.baseTableView?.reloadData()
    }
    
    fileprivate let dao = HoroscopeDAO.sharedInstance
    
    //MARK:praiseOrtreadePost
    func praiseOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOneNormalTopic(name, action: action, value: value, id: id)
    }
    
    func tradeOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOrPraiseAction(name, action: action, value: value, id: id)
    }
    
    func tradeOrPraiseAction(_ name: String, action: String, value: Bool, id: String) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            dao.likeOrDislikeAction(name, action: action, value: value, id: id)
        }
    }
    
    override func loadMoreList(){
        self.viewmodel?.loadMoreData({ (more) in
            if more == false{
                self.baseTableView?.mj_footer.endRefreshingWithNoMoreData()
                self.perform(#selector(self.delayReload),
                             with: nil,
                             afterDelay: 0.5)
                self.baseTableView?.reloadData()
            }else{
                self.baseTableView?.mj_footer.endRefreshing()
                self.baseTableView?.reloadData()
            }
            self.hasLoadedData = true
        }, failure: { (error) in
            self.baseTableView?.mj_footer.endRefreshing()
            self.baseTableView?.reloadData()
            self.hasLoadedData = true
        })
    }
    
    override func refreshData(){
        self.viewmodel?.offSet = 0
        viewmodel?.loadConfig({
            self.baseTableView?.mj_header.endRefreshing()
            self.baseTableView?.reloadData()
            self.hasLoadedData = true
        }, failure: { (error) in
            self.baseTableView?.mj_header.endRefreshing()
            self.hasLoadedData = true
        })
    }
    
    @objc func delayReload()  {
        self.baseTableView?.mj_footer.endRefreshing()
    }
    
    //MARK:-NotificationCellDelegate
    func notificationCellDidClick() {
        AnaliticsManager.sendEvent(AnaliticsManager.main_card_click, data: ["category":"notification"])
        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
    }
    
    //MARK:-InnerAdContainerdelegate
    func showAdSuccess(_ width: CGFloat, height: CGFloat) {
        self.baseTableView?.reloadData()
    }
    
    //MARK:Create
    var horoName:String?
    class func create(_ name:String)->HomeTimeLineViewController{
        let vc = HomeTimeLineViewController()
        vc.horoName = name
        return vc
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
