//
//  MeRowCell.swift
//  Horoscope
//
//  Created by Wang on 16/12/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class MeRowCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
        addConstrains()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createUI(){
        self.selectionStyle = .none
        self.addSubview(titleLab)
     //   self.addSubview(arrowImg)
        self.addSubview(line)
        self.backgroundColor = UIColor.clear
    }
    
    func addConstrains(){
        titleLab.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.bottom.equalTo(self.snp.bottom).offset(-1)
            make.top.equalTo(self.snp.top).offset(1)
        }
//        arrowImg.snp.makeConstraints { (make) in
//            make.right.equalTo(self.snp.right).offset(-26)
//            make.centerY.equalTo(self.titleLab.snp.centerY)
//            make.height.width.equalTo(15)
//        }
        line.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.contentView.snp.bottom)
            make.width.equalTo(130)
            make.centerX.equalTo(self.snp.centerX)
            make.height.equalTo(1)
        }
    }
    
    func renderCell(_ title:String) {
        self.titleLab.text=title
    }
    
    let line:UIView={
        let lineView = UIView()
        lineView.backgroundColor = UIColor.meSpreadLineColor()
        return lineView
    }()
    
    let arrowImg:UIImageView={
        let img = UIImageView()
        img.image = UIImage(named:"ic_aeeow_")
        return img
    }()
    
    let titleLab:UILabel={
        let lab = UILabel()
        lab.text = "sadasdas"
        lab.textColor=UIColor.purpleContentColor()
        lab.font=HSFont.baseLightRegularFont(18)
        lab.textAlignment = .center
        return lab
    }()
    
    // MARK:Create
    static let cellId = "MeRowCell"
    class func dequeueReusableCell(_ tableView: UITableView,
                                   cellForRowAtIndexPath indexPath: IndexPath) -> MeRowCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId,
                                                               for: indexPath) as! MeRowCell
        
        return cell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
