//
//  AuthHoroManager.swift
//  Horoscope
//
//  Created by Wang on 16/12/23.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class AuthHoroManager: NSObject {
    
    static let sharedInstance = AuthHoroManager()
    static let HoroName = "HoroName"
    static let birthDay = ""

    func saveHoroScope(_ name:String){
        let userDefault = UserDefaults.standard
        userDefault.setValue(name, forKey: AuthHoroManager.HoroName)
        userDefault.synchronize()
    }
    
    func getAuthHoroName() ->String{
        let userDefault = UserDefaults.standard
        if let horo=userDefault.value(forKey: AuthHoroManager.HoroName) as? String{
            return horo
        }else{
            return ""
        }
    }
    
    func saveAuthBirthday(_ dateString:String)  {
        let userDefault = UserDefaults.standard
        userDefault.setValue(dateString, forKey: AuthHoroManager.birthDay)
        userDefault.synchronize()
    }
    
    func getAuthBirthday() ->String {
        let userDefault = UserDefaults.standard
        if let horo=userDefault.value(forKey: AuthHoroManager.birthDay) as? String{
            return horo
        }else{
            return ""
        }
    }
    

}
