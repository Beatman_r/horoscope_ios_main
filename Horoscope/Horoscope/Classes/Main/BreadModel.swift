//
//  VideoModel.swift
//  Horoscope
//
//  Created by Beatman on 16/12/10.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class BreadModel: BaseCardListModel {
    
    var author: String?
    var authorAvatar: String?
    var authorized: Bool?
    var authorUid: String?
    var authorWebsite: String?
    var businessAd: Bool?
    var commentCount: Int?
    var content: String?
    var contentType: String?
    var createTime: String?
    var id: String?
    var likeCount: Int?
    var locale: String?
    var originalUrl: String?
    var richMedia: String?
    var richMediaDuration: String?
    var richMediaWebsite: String?
    var shareCount: Int?
    var thumbnail: String?
    var title: String?
    var type: String?
    var viewCount: Int?
    var relatedDate: Int?
    var verseAri: String?
    var verseContent: String?
    var sectionId:String?
    var isTop:Bool?
    var postId:String?
    
    init(json:JSON) {
        super.init()
        self.author             = json["author"].string
        self.authorAvatar       = json["authorAvatar"].string
        self.authorized         = json["authorized"].bool
        self.authorUid          = json["authorUid"].string
        self.authorWebsite      = json["authorWebsite"].string
        self.businessAd         = json["businessAd"].bool
        self.commentCount       = json["commentCount"].int
        self.content            = json["content"].string
        self.contentType        = json["contentType"].string
        self.createTime         = json["createTime"].string
        self.id                 = json["id"].string
        self.likeCount          = json["likeCount"].int
        self.locale             = json["locale"].string
        self.originalUrl        = json["originalUrl"].string
        self.richMedia          = json["richMedia"].string
        self.richMediaDuration  = json["richMediaDuration"].string
        self.richMediaWebsite   = json["richMediaWebsite"].string
        self.shareCount         = json["shareCount"].int
        self.thumbnail          = json["thumbnail"].string
        self.title              = json["title"].string
        self.type               = json["type"].string
        self.viewCount          = json["viewCount"].int
        self.relatedDate        = json["relatedDate"].int
        self.verseAri           = json["verseAri"].string
        self.verseContent       = json["verseContent"].string
        self.isTop               = json["isTop"].bool
    }
    
    init(dic:[String:AnyObject]) {
        super.init()
        self.author            = dic["author"] as? String
        self.authorAvatar      = dic["authorAvatar"] as? String
        self.authorized        = dic["authorized"] as? Bool
        self.authorUid         = dic["authorUid"] as? String
        self.authorWebsite     = dic["authorWebsite"] as? String
        self.businessAd        = dic["businessAd"] as? Bool
        self.commentCount      = dic["commentCount"] as? Int
        self.content           = dic["content"] as? String
        self.contentType       = dic["contentType"] as? String
        self.createTime        = dic["createTime"] as? String
        self.id                = dic["id"] as? String
        self.likeCount         = dic["likeCount"] as? Int
        self.locale            = dic["locale"] as? String
        self.originalUrl       = dic["originalUrl"] as? String
        self.richMedia         = dic["richMedia"] as? String
        self.richMediaDuration = dic["richMediaDuration"] as? String
        self.richMediaWebsite  = dic["richMediaWebsite"] as? String
        self.shareCount        = dic["shareCount"] as? Int
        self.thumbnail         = dic["thumbnail"] as? String
        self.title             = dic["title"] as? String
        self.type              = dic["type"] as? String
        self.viewCount         = dic["viewCount"] as? Int
        self.relatedDate       = dic["relatedDate"] as? Int
        self.verseAri          = dic["verseAri"] as? String
        self.verseContent      = dic["verseContent"] as? String
        self.isTop      = dic["isTop"] as? Bool
    }
    
    
}
