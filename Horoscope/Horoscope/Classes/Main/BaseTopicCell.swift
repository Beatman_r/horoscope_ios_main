//
//  BaseTopicCell.swift
//  Horoscope
//
//  Created by Wang on 17/2/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit
import IDMPhotoBrowser

class BaseTopicCell: BaseCardCell {
     
     weak var rootVc:UIViewController?
     weak var delegate:NormalPostCellDelegate?
     override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
     }
     
     override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
          super.init(style: style, reuseIdentifier: reuseIdentifier)
          self.createUI()
          self.addConstrains()
     }
     
     required init?(coder aDecoder: NSCoder) {
          fatalError("init(coder:) has not been implemented")
     }
     
     func createUI() {
          self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
          self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
          self.cornerBackView?.addSubview(userNameLabe)
          self.cornerBackView?.addSubview(userAvaIma)
          self.cornerBackView?.addSubview(horoLab)
          self.cornerBackView?.addSubview(timeLab)
          self.cornerBackView?.addSubview(contentLab)
          self.cornerBackView?.addSubview(figureIma)
          self.cornerBackView?.addSubview(praiseBtn)
          self.cornerBackView?.addSubview(praiseLab)
          self.cornerBackView?.addSubview(treadBtn)
          self.cornerBackView?.addSubview(treadNumLab)
          self.cornerBackView?.addSubview(commentBtn)
          self.cornerBackView?.addSubview(commentNumLab)
          self.cornerBackView?.addSubview(shareBtn)
          self.cornerBackView?.addSubview(tagListView)
          self.cornerBackView?.addSubview(contentLine)
          self.cornerBackView?.addSubview(divideLine)
          
          shareBtn.addTarget(self, action: #selector(shareBtnClick(_:)), for: .touchUpInside)
          commentBtn.addTarget(self, action: #selector(commentBtnClick), for: .touchUpInside)
          praiseBtn.addTarget(self, action: #selector(praiseBtnClick), for: .touchUpInside)
          treadBtn.addTarget(self, action: #selector(treadBtnClick), for: .touchUpInside)
          
          let tap = UITapGestureRecognizer()
          tap.addTarget(self, action: #selector(viewImage))
          self.figureIma.addGestureRecognizer(tap)
          self.figureIma.isUserInteractionEnabled = true
     }
     
     //MARK;fullScreenIma
     @objc func viewImage() {
          let imaName = self.model?.imageList?.first as? String
          let imgUrl = URL(string: imaName ?? "")
          let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
          let brower = IDMPhotoBrowser.init(photoURLs: urlArr, animatedFrom: self.figureIma)
          brower?.forceHideStatusBar = true
          brower?.usePopAnimation = true
          self.rootVc?.present(brower!, animated: true, completion: nil)
     }
     
     //MARK:BtnClick
     @objc func shareBtnClick(_ sender:UIButton) {
          let content = FBSDKShareLinkContent.init()
          content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_POST + (self.model?.postId ?? ""))
          FBSDKShareDialog.show(from: self.rootVc, with: content, delegate: nil)
     }
     
     @objc func praiseBtnClick() {
          let userStatus = AccountManager.sharedInstance.getLogStatus()
          if userStatus == .null || userStatus == .anonymous{
               let vc = HsLoginViewController()
               self.rootVc?.navigationController?.pushViewController(vc, animated: true)
          }else{
               if self.model?.iliked == false{
                    let newCount = (model?.likeCount ?? 0) + 1
                    self.model?.likeCount = newCount
                    self.model?.iliked = true
                    self.rencderCell()
                    self.delegate?.praiseOneNormalTopic("post", action: "like", value: true, id: model?.postId ?? "" )
               }
               else  if self.model?.iliked == true {
                    var newCount = (model?.likeCount ?? 0) - 1
                    if newCount <= 0{
                         newCount = 0
                    }
                    self.model?.likeCount = newCount
                    self.model?.iliked = false
                    self.rencderCell()
                    self.delegate?.praiseOneNormalTopic("post", action: "like", value: false, id: model?.postId ?? "" )
               }
          }
     }
     
     @objc func treadBtnClick() {
          let userStatus = AccountManager.sharedInstance.getLogStatus()
          if userStatus == .null || userStatus == .anonymous{
               let vc = HsLoginViewController()
               self.rootVc?.navigationController?.pushViewController(vc, animated: true)
          }else{
               if  self.model?.iDisliked == false{
                    self.model?.iDisliked = true
                    let newCount = (model?.dislikeCount ?? 0) + 1
                    self.model?.dislikeCount = newCount
                    self.rencderCell()
                    self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: true, id: model?.postId ?? "" )
               }
               else if  self.model?.iDisliked == true{
                    self.model?.iDisliked = false
                    var newCount = (model?.dislikeCount ?? 0) - 1
                    if newCount <= 0{
                         newCount = 0
                    }
                    self.model?.dislikeCount = newCount
                    self.rencderCell()
                    self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: false, id: model?.postId ?? "" )
               }
          }
     }
     
     @objc func commentBtnClick()  {
          let userStatus = AccountManager.sharedInstance.getLogStatus()
          if userStatus == .null || userStatus == .anonymous{
               let vc = HsLoginViewController()
               self.rootVc?.navigationController?.pushViewController(vc, animated: true)
          }else{
               let vc = PostViewController()
               vc.isFirstClassReply = true
               vc.replyModel = self.model
               self.rootVc?.navigationController?.pushViewController(vc, animated: true)
          }
     }
     
     func addConstrains() {
          self.cornerBackView?.snp.makeConstraints { (make) in
               make.left.equalTo(self.contentView.snp.left)
               make.right.equalTo(self.contentView.snp.right)
               make.top.equalTo(self.contentView.snp.top)
               make.bottom.equalTo(self.contentView.snp.bottom)
          }
          
          userAvaIma.snp.makeConstraints { (make) in
               make.left.equalTo(cornerBackView!.snp.left).offset(12)
               make.top.equalTo(cornerBackView!.snp.top).offset(20)
               make.width.height.equalTo(44)
          }
          
          userNameLabe.snp.makeConstraints { (make) in
               make.left.equalTo(userAvaIma.snp.right).offset(15)
               make.top.equalTo(userAvaIma.snp.top)
          }
          
          horoLab.snp.makeConstraints { (make) in
               make.left.equalTo(userNameLabe.snp.left)
               make.bottom.equalTo(userAvaIma.snp.bottom)
          }
          
          timeLab.snp.makeConstraints { (make) in
               make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
               make.bottom.equalTo(self.userNameLabe.snp.bottom)
          }
          
          contentLine.snp.makeConstraints { (make) in
               make.left.equalTo(cornerBackView!.snp.left)
               make.top.equalTo(userAvaIma.snp.bottom).offset(20)
               make.height.equalTo(1)
               make.right.equalTo(cornerBackView!.snp.right)
          }
          
          contentLab.snp.makeConstraints { (make) in
               make.left.equalTo(userAvaIma.snp.left)
               make.right.equalTo(self.contentLine.snp.right).offset(-12)
               make.top.equalTo(contentLine.snp.bottom).offset(10)
          }
          
          figureIma.snp.makeConstraints { (make) in
               make.centerX.equalTo(self.cornerBackView!.snp.centerX)
               make.width.height.equalTo(SCREEN_WIDTH-24)
               make.top.equalTo(contentLab.snp.bottom).offset(20)
          }
          
          praiseBtn.snp.makeConstraints { (make) in
               make.left.equalTo(userAvaIma.snp.left).offset(1)
               make.bottom.equalTo(cornerBackView!.snp.bottom).offset(-20)
               make.height.equalTo(18)
               make.width.equalTo(18)
          }
          
          praiseLab.snp.makeConstraints { (make) in
               make.left.equalTo(praiseBtn.snp.right).offset(10)
               make.bottom.equalTo(praiseBtn.snp.bottom)
          }
          
          treadBtn.snp.makeConstraints { (make) in
               make.left.equalTo(praiseLab.snp.right).offset(30)
               make.bottom.equalTo(praiseLab.snp.bottom).offset(3)
               make.height.equalTo(18)
               make.width.equalTo(18)
          }
          
          treadNumLab.snp.makeConstraints { (make) in
               make.left.equalTo(treadBtn.snp.right).offset(10)
               make.bottom.equalTo(praiseLab.snp.bottom)
          }
          
          commentNumLab.snp.makeConstraints { (make) in
               make.left.equalTo(commentBtn.snp.right).offset(10)
               make.bottom.equalTo(commentBtn.snp.bottom)
          }
          
          shareBtn.snp.makeConstraints { (make) in
               make.right.equalTo(contentLine.snp.right).offset(-12)
               make.bottom.equalTo(praiseBtn.snp.bottom)
               make.width.height.equalTo(18)
          }
          
          commentBtn.snp.makeConstraints { (make) in
               make.left.equalTo(self.cornerBackView!.snp.centerX).offset(50)
               make.bottom.equalTo(shareBtn.snp.bottom)
               make.width.height.equalTo(18)
          }
          
          tagListView.snp.makeConstraints { (make) in
               make.left.equalTo(self.userAvaIma.snp.left).offset(-2)
               make.right.equalTo(self.contentLine.snp.right).offset(-12)
               make.bottom.equalTo(self.praiseBtn.snp.top).offset(-30)
          }
          divideLine.snp.makeConstraints { (make) in
               make.bottom.equalTo(self.cornerBackView!.snp.bottom)
               make.centerX.equalTo(self.snp.centerX)
               make.width.equalTo(SCREEN_WIDTH)
               make.height.equalTo(1)
          }
     }
     
     
     var model:MyPostModel? {
          didSet{
               rencderCell()
               createTaglistView()
          }
     }
     func rencderCell() {
          if model?.iliked == true{
               praiseBtn.setImage(UIImage(named:"praise2_"), for: UIControlState())
          }else{
               praiseBtn.setImage(UIImage(named:"praise_"), for: UIControlState())
          }
          
          if model?.iDisliked == true{
               treadBtn.setImage(UIImage(named:"dislike_pink_"), for: UIControlState())
          }else{
               treadBtn.setImage(UIImage(named:"Step-on_"), for: UIControlState())
          }
          
          if model?.likeCount != 0 {
               self.praiseLab.isHidden = false
               self.praiseLab.text = "\(self.model?.likeCount ?? 0)"
          }else {
               self.praiseLab.isHidden = true
          }
          
          if model?.dislikeCount != 0{
               self.treadNumLab.isHidden = false
               self.treadNumLab.text="\(model?.dislikeCount ?? 0)"
          }else {
               self.treadNumLab.isHidden = true
          }
          
          if model?.commentCount != 0{
               self.commentNumLab.isHidden = false
               self.commentNumLab.text="\(model?.commentCount ?? 0)"
          }else {
               self.commentNumLab.isHidden = true
               self.commentNumLab.text="\(model?.commentCount ?? 0)"
          }
          
          self.userAvaIma.sd_setImage(with: URL(string: "\(model?.user?.avatar ?? "")"), placeholderImage: UIImage(named:"icon_author"))
          self.userNameLabe.text = model?.user?.name
          self.contentLab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.5, textColor: UIColor.purpleContentColor(), targetStr: model?.content ?? "", font: HSFont.baseRegularFont(18))
          self.horoLab.text = "\(model?.user?.horoscopeName?.capitalized ?? "" )"
          self.timeLab.text =  HSHelpCenter.sharedInstance.dateTool.calculateTimeInterval(Double(model?.createTime ?? 0))
          if self.model?.imageList?.count != 0{
               let imaName = self.model?.imageList?.first as? String
               self.figureIma.sd_setImage(with: URL(string:"\(imaName ?? "")"), placeholderImage: UIImage(named:"defaultImg1.jpg"))
               figureIma.snp.remakeConstraints{ (make) in
                    make.centerX.equalTo(self.cornerBackView!.snp.centerX)
                    make.width.height.equalTo(SCREEN_WIDTH-24)
                    make.top.equalTo(contentLab.snp.bottom).offset(20)
               }
               figureIma.isHidden = false
          }else{
               figureIma.isHidden = true
               figureIma.snp.makeConstraints { (make) in
                    make.left.equalTo(userAvaIma.snp.left)
                    make.right.equalTo(contentLab.snp.right)
                    make.top.equalTo(contentLab.snp.bottom)
                    make.height.equalTo(0)
               }
          }
          
     }
     
     func createTaglistView() {
          for arr  in tagListView.subviews {
               arr .removeFromSuperview()
          }
          var x:CGFloat = 0
          var y:CGFloat = 0
          let labHeight:CGFloat = 18
          let arr = model?.tagList
          
          for index in 0..<(arr ?? []).count {
               let btn = UIButton()
               let eachTag = arr![index]
               let tagWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth("#\(eachTag.capitalized ?? "")", font: 15, height: labHeight)
               if (x+tagWidth+10)>(UIScreen.main.bounds.size.width-24){
                    x = 0
                    y += 30
               }else{
               }
               btn.setTitle("#\(eachTag.capitalized ?? "")", for: UIControlState())
               btn.titleLabel?.font = HSFont.baseRegularFont(15)
               btn.setTitleColor(UIColor.rssSourceBlueColor(), for: UIControlState())
               btn.titleLabel?.textAlignment = .left
               btn.frame = CGRect(x: x, y: y, width: tagWidth, height: 20)
               btn.tag = index
               btn.addTarget(self, action: #selector(goSeeTagList(_:)), for: .touchUpInside)
               x = x+tagWidth+10
               tagListView.addSubview(btn)
               tagListView.snp.remakeConstraints { (make) in
                    make.height.equalTo(y+20)
                    make.top.equalTo(figureIma.snp.bottom).offset(20)
                    make.left.equalTo(self.userAvaIma.snp.left).offset(-2)
                    make.right.equalTo(self.contentLine.snp.right).offset(-12)
                    make.bottom.equalTo(self.praiseBtn.snp.top).offset(-30)
               }
          }
          self.layoutIfNeeded()
     }
     
     @objc func goSeeTagList(_ sender:UILabel){
          let tagName = self.model?.tagList![sender.tag] as! String
          let vc = ReadTagListViewController()
          vc.tagName = tagName
          self.rootVc?.navigationController?.pushViewController(vc, animated: true)
     }
     
     let tagListView:UIView = {
          let v = UIView()
          return v
     }()
     
     let userAvaIma:UIImageView = {
          let ima = UIImageView()
          ima.layer.masksToBounds=true
          ima.layer.cornerRadius=22
          return ima
     }()
     
     let contentLine:UIView = {
          let line = UIView()
          line.backgroundColor=UIColor.divideColor()
          return line
     }()
     
     let userNameLabe:UILabel = {
          let lab = UILabel()
          lab.font=HSFont.baseRegularFont(18)
          lab.textColor = UIColor.purpleContentColor()
          lab.textAlignment = .left
          return lab
     }()
     
     let horoLab:UILabel = {
          let lab = UILabel()
          lab.textColor = UIColor.pubTimeColor()
          lab.font=HSFont.baseRegularFont(15)
          return lab
     }()
     
     let timeLab:UILabel = {
          let lab = UILabel()
          lab.textColor = UIColor.pubTimeColor()
          lab.font=HSFont.baseRegularFont(15)
          return lab
     }()
     
     let contentLab:UILabel = {
          let lab = UILabel()
          lab.numberOfLines = 0
          lab.font=HSFont.baseRegularFont(18)
          lab.textColor = UIColor.purpleContentColor()
          return lab
     }()
     
     let figureIma:UIImageView = {
          let ima = UIImageView()
          ima.contentMode = UIViewContentMode.scaleAspectFit
          return ima
     }()
     
     let treadBtn:UIButton = {
          let button = UIButton(type:.custom)
          button.setImage(UIImage(named:"Step-on_"), for: UIControlState())
          return button
     }()
     
     let praiseBtn:UIButton = {
          let button = UIButton(type:.custom)
          button.setImage(UIImage(named:"praise_"), for: UIControlState())
          return button
     }()
     
     let treadNumLab:UILabel = {
          let lab = UILabel()
          lab.textColor=UIColor.purpleContentColor()
          lab.font=HSFont.baseRegularFont(15)
          return lab
     }()
     
     let commentNumLab:UILabel = {
          let lab = UILabel()
          lab.textColor=UIColor.purpleContentColor()
          lab.font=HSFont.baseRegularFont(15)
          return lab
     }()
     
     
     let praiseLab:UILabel = {
          let lab = UILabel()
          lab.textColor=UIColor.purpleContentColor()
          lab.font=HSFont.baseRegularFont(15)
          return lab
     }()
     
     let commentBtn:UIButton = {
          let btn = UIButton()
          btn.setImage(UIImage(named:"comments_"), for: UIControlState())
          return btn
     }()
     
     let shareBtn : UIButton = {
          let btn = UIButton()
          btn.setImage(UIImage(named:"CB_share_"), for: UIControlState())
          return btn
     }()
     
     let divideLine : UIView = {
          let divideLine = UIView()
          divideLine.backgroundColor = UIColor.divideColor()
          return divideLine
     }()
     
     //MARK:Dequeuse
     static let cellId="BaseTopicCell"
     class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> BaseTopicCell{
          
          let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! BaseTopicCell
          return cell
     }
     
     class func calculateheight(_ model:ReadPostModel?) ->CGFloat{
          let wenben = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: model?.content ?? "", fontSize: 18, font: HSFont.baseRegularFont(18), width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.5)
          
          var h = 20+44+20+10+wenben
          
          h += 20
          let figureHeight = (UIScreen.main.bounds.size.width - 24)
          if (model?.imageList?.count) == 0{
               h -= 20
          }else {
               h += figureHeight
          }
          h += 20
          h += calculateTagListHeight(model?.tagList ?? [])
          h += 30
          h += 18
          h += 20
          h += 10
          return  h
     }
     
     class func calculateTagListHeight(_ tagList:[AnyObject]) ->CGFloat {
          var x:CGFloat = 0
          var y:CGFloat = 0
          let labHeight:CGFloat = 18
          for index in 0..<tagList.count {
               let eachTag = tagList[index]
               let tagWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth("#\(eachTag.capitalized)", font: 15, height: labHeight)
               //if longer than screenWidth
               if (x+tagWidth+10)>(UIScreen.main.bounds.size.width-24){
                    x = 0
                    y += 30
               }else{
               }
               x = x+tagWidth+10
          }
          return y+20
          
     }
     
     class func calculateHeight(_ model:MyPostModel?) ->CGFloat{
          let wenben = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: model?.content ?? "", fontSize: 18, font: HSFont.baseRegularFont(18), width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.5)
          
          var h = 20+44+20+10+wenben
          
          h += 20
          let figureHeight = (UIScreen.main.bounds.size.width - 24)
          if (model?.imageList?.count) == 0{
               h -= 20
          }else {
               h += figureHeight
          }
          h += 20
          h += calculateTagListHeight(model?.tagList ?? [])
          h += 30
          h += 18
          h += 20
          h += 10
          return  h
     }
     
     override func setSelected(_ selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
          // Configure the view for the selected state
     }
     
}
