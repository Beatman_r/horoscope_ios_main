//
//  CommentFootBtn.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

protocol CommentFootBtnDelegate:class {
    func commentFootBtnClick()
}

class CommentFootBtn: UIButton {
    weak var delegate: CommentFootBtnDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.commentFootBtnBGColor()
        self.setTitle("Add comments...", for:UIControlState())
        self.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.titleLabel?.textAlignment = NSTextAlignment.left
        self.setTitleColor(UIColor.purpleContentColorWithAlpha(), for: UIControlState())
        self.setImage(UIImage(named: "reply_"), for: UIControlState())
        self.isUserInteractionEnabled = true
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.purpleContentColor().cgColor
        self.layer.cornerRadius = 22
        self.layer.masksToBounds = true
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0,(AdaptiveUtils.screenWidth - 100)/2)
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0,(AdaptiveUtils.screenWidth - 110)/2)
        self.addTarget(self, action: #selector(self.tapClick), for: .touchUpInside)
    }
    
    @objc func tapClick() {
        self.delegate?.commentFootBtnClick()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
