//
//  GridCommentCell.swift
//  Horoscope
//
//  Created by xiao  on 17/2/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class GridCommentCell: UIView {

    var model:ParentComment?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.basePurpleBackgroundColor()
        let tap  = UITapGestureRecognizer(target: self, action: #selector(self.cellDidTap))
        self.addGestureRecognizer(tap)
    }

    @objc func cellDidTap() {
        guard let model = model else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: CommentViewClickToShowReplyView), object: nil, userInfo: ["model":model])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
