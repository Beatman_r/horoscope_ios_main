//
//  ReplyViewDisLikeBtn.swift
//  Horoscope
//
//  Created by xiao  on 17/2/24.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ReplyViewDisLikeBtn: UIButton {
    
    var model : BaseComment?
    
    init(frame: CGRect,model:BaseComment) {
        super.init(frame: frame)
        self.model = model
        
        self.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        if model.isDisliked == true {
            self.setImage(UIImage(named:"Step-on2_" ), for: UIControlState())
        }else{
            self.setImage(UIImage(named:"Step-on_3" ), for: UIControlState())
        }
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 40)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.setTitle(String(model.likeCount ?? 0), for: UIControlState())
        self.titleLabel?.textAlignment = NSTextAlignment.right
        self.setTitleColor(UIColor.windowColor(), for: UIControlState())
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
