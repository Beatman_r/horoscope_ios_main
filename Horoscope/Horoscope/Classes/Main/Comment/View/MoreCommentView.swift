//
//  MoreCommentView.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class MoreCommentView: UIView {

    var model:ParentComment?
    var moreCommentBtn:UIButton?
    var parent:UIView?
    
    init(frame:CGRect,model:ParentComment,parentView:UIView,isLast:Bool) {
        super.init(frame: frame)
          self.backgroundColor = UIColor.basePurpleBackgroundColor()
        self.parent = parentView
        self.model = model
        if(!isLast){
            self.layer.borderWidth = 0.6
            self.layer.borderColor = UIColor(red: 72/255, green:56/255, blue: 107/255, alpha: 1).cgColor
            self.backgroundColor = UIColor.basePurpleBackgroundColor()
        }
        
        self.moreCommentBtn = UIButton(type: .custom)
        self.moreCommentBtn?.frame = CGRect(x: 5, y: parentView.frame.size.height + 12, width: self.frame.size.width - 10, height: 30)
        self.moreCommentBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.moreCommentBtn?.setTitleColor(UIColor.commentViewImage(), for: UIControlState())
        self.moreCommentBtn?.setImage(UIImage(named: "unfold_"), for: UIControlState())
        self.moreCommentBtn?.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.moreCommentBtn?.imageEdgeInsets = UIEdgeInsetsMake(0,-10, 0, 0)
        self.moreCommentBtn?.isUserInteractionEnabled = true
        self.addSubview(self.moreCommentBtn!)
        
    }
         required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
