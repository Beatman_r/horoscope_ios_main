//
//  ViewImageButton.swift
//  Horoscope
//
//  Created by xiao  on 17/2/15.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ViewImageButton: UIButton {

    var model : ParentComment?
    
     init(frame: CGRect,model:ParentComment) {
        super.init(frame: frame)
        self.model = model
        
        self.setTitle("View Image", for:UIControlState())
        self.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.setTitleColor(UIColor.commentViewImage(), for: UIControlState())
        self.setImage(UIImage(named: "picture_"), for: UIControlState())
        self.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        self.isUserInteractionEnabled = true
        }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
