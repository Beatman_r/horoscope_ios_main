//
//  LikeBtn.swift
//  Horoscope
//
//  Created by xiao  on 17/2/24.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class LikeBtn: UIButton {
    
    var model : ParentComment?
    
    init(frame: CGRect,model:ParentComment) {
        super.init(frame: frame)
        self.model = model
        
        self.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        if model.isLiked == true {
        self.setImage(UIImage(named:"praise2_" ), for: UIControlState())
        }else{
        self.setImage(UIImage(named:"praise_" ), for: UIControlState())
        }
        if model.likeCount == 0 {
            self.setTitle("", for: UIControlState())
        }else {
            self.setTitle("\(model.likeCount ?? 0)", for: UIControlState())
        }
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 40)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        self.setTitle(String(model.likeCount ?? 0), for: UIControlState())
        self.titleLabel?.textAlignment = NSTextAlignment.left
        self.setTitleColor(UIColor.purpleContentColor(), for: UIControlState())
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
