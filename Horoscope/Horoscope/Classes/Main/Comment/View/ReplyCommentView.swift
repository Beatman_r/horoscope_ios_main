
//
//  ReplyCommentView.swift
//  Horoscope
//
//  Created by xiao  on 17/2/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit



class ReplyCommentView: UIView {
    
    let backView = UIView()
    let replyBtn = UIButton(type:.custom)
    let likeCountLabel = UILabel()
    let dislikeCountLabel = UILabel()
    let reportBtn = UIButton(type:.custom)
//    var likeButton :ReplyViewLikeBtn?
//    var dislikeButton :ReplyViewDisLikeBtn?
    let cancelBtn = UIButton(type:.custom)
    let reportLabelBtn = UIButton(type:.custom)
    let replyLabelBtn = UIButton(type:.custom)
  
    var model:BaseComment?
    
     init(frame: CGRect,model:BaseComment) {
        super.init(frame: frame)
        self.model = model
        
        self.backgroundColor = UIColor(hexString: "000000", withAlpha: 0.6)
        let tap  = UITapGestureRecognizer(target: self, action: #selector(self.dismissReplyView))
        self.addGestureRecognizer(tap)
        self.setUpUI()
    }
    func setUpUI()  {
        self.addSubview(backView)
        backView.backgroundColor = UIColor.white
        
//        self.likeButton = ReplyViewLikeBtn(frame: CGRect.zero, model: model ?? BaseComment())
//        self.likeButton?.addTarget(self, action: #selector(self.likeBtnClick), forControlEvents: .TouchUpInside)
//        
//        self.dislikeButton = ReplyViewDisLikeBtn(frame: CGRect.zero, model: model ?? BaseComment())
//        self.dislikeButton?.addTarget(self, action: #selector(self.dislikeBtnClick), forControlEvents: .TouchUpInside)
        
        replyBtn.setTitleColor(UIColor.red, for: UIControlState())
        replyBtn.setImage(UIImage(named:"reply_4" ), for: UIControlState())
        
        reportBtn.setImage(UIImage(named:"report_4" ), for: UIControlState())
        
        cancelBtn.setImage(UIImage(named:"Shut down_" ), for: UIControlState())
        cancelBtn.addTarget(self, action:#selector(self.dismissReplyView), for: .touchUpInside)
        
        self.replyLabelBtn.setTitle("Reply", for: UIControlState())
        self.replyLabelBtn.setTitleColor(UIColor.replyViewLabelColor(), for: UIControlState())
        self.replyLabelBtn.addTarget(self, action: #selector(self.replyBtnClick), for: .touchUpInside)
        self.replyLabelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        
        self.reportLabelBtn.setTitle("Report", for: UIControlState())
        self.reportLabelBtn.setTitleColor(UIColor.replyViewLabelColor(), for: UIControlState())
        self.reportLabelBtn.addTarget(self, action: #selector(self.reportBtnClick), for: .touchUpInside)
        self.reportLabelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    
        backView.layer.cornerRadius = 5
        backView.layer.masksToBounds = true
        
        backView.addSubview(replyBtn)
        backView.addSubview(replyLabelBtn)
        backView.addSubview(reportBtn)
        backView.addSubview(reportLabelBtn)
//        backView.addSubview(likeButton!)
//        backView.addSubview(dislikeButton!)
        backView.addSubview(cancelBtn)
        
        backView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(281/375*AdaptiveUtils.screenWidth)
            make.height.equalTo(120/667*AdaptiveUtils.screenHeight)
        }
        
//        likeButton!.snp.makeConstraints { (make) in
//            make.left.equalTo(backView.snp.left).offset(40/375*AdaptiveUtils.screenWidth)
//            make.top.equalTo(backView.snp.top).offset(30/667*AdaptiveUtils.screenHeight)
//            make.width.equalTo(80)
//            make.height.equalTo(27)
//        }
//        
//        dislikeButton!.snp.makeConstraints { (make) in
//            make.left.equalTo(backView.snp.right).offset(-120/375*AdaptiveUtils.screenWidth)
//            make.top.equalTo(backView.snp.top).offset(30/667*AdaptiveUtils.screenHeight)
//            make.width.equalTo(80)
//            make.height.equalTo(27)
//        }
//        
        reportBtn.snp.makeConstraints { (make) in
            make.left.equalTo(backView.snp.left).offset(40/375*AdaptiveUtils.screenWidth)
            make.top.equalTo(backView.snp.top).offset(30/667*AdaptiveUtils.screenHeight)
            make.width.equalTo(27)
            make.height.equalTo(27)
        }
        
        reportLabelBtn.snp.makeConstraints { (make) in
            make.left.equalTo(reportBtn.snp.right).offset(18/375*AdaptiveUtils.screenWidth)
            make.height.equalTo(30)
            make.centerY.equalTo(reportBtn.snp.centerY)
        }
        
        replyBtn.snp.makeConstraints { (make) in
            make.left.equalTo(backView.snp.right).offset(-120/375*AdaptiveUtils.screenWidth)
            make.top.equalTo(backView.snp.top).offset(30/667*AdaptiveUtils.screenHeight)
            make.width.equalTo(27)
            make.height.equalTo(27)
        }
        
        replyLabelBtn.snp.makeConstraints { (make) in
            make.left.equalTo(replyBtn.snp.right).offset(18/375*AdaptiveUtils.screenWidth)
            make.height.equalTo(30)
            make.centerY.equalTo(replyBtn.snp.centerY)
        }
        
        cancelBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView.snp.centerX)
            make.bottom.equalTo(backView.snp.bottom).offset(-18/665*AdaptiveUtils.screenHeight)
        }
        
    }
    

//    func likeBtnClick() {
//        guard let model = self.model else {
//            return
//        }
//        
//        if model.isLiked == false{
//            model.isLiked = true
//            let newCount = (model.likeCount ?? 0) + 1
//            model.likeCount = newCount
//            self.renderLikeBtn(model)
//            HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: model.commentId ?? "")
//        }
//        else  if model.isLiked == true {
//            let newCount = (model.likeCount ?? 0) - 1
//            if newCount <= 0{
//                newCount == 0
//            }
//            model.likeCount = newCount
//            model.isLiked = false
//            self.renderLikeBtn(model)
//            HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: model.commentId ?? "")
//        }
//    }
    
//    func dislikeBtnClick() {
//        guard let model = self.model else {
//            return
//        }
//        
//        if  model.isDisliked == false{
//            model.isDisliked = true
//            let newCount = (model.dislikeCount ?? 0) + 1
//            model.dislikeCount = newCount
//            self.rendrenderDislikeBtn(model)
//            HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: model.commentId ?? "")
//        }
//        else if  model.isDisliked == true{
//            model.isDisliked = false
//            let newCount = (model.dislikeCount ?? 0) - 1
//            if newCount <= 0{
//                newCount == 0
//            }
//            model.dislikeCount = newCount
//            self.rendrenderDislikeBtn(model)
//            HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: model.commentId ?? "")
//        }
//    }
//    
//    func renderLikeBtn(model:BaseComment) {
//        if model.isLiked == true{
//            self.likeButton!.setImage(UIImage(named:"praise2_"), forState: .Normal)
//            self.likeButton!.setTitle("\(model.likeCount ?? 0)", forState: .Normal)
//        }else{
//            self.likeButton!.setImage(UIImage(named:"praise_3"), forState: .Normal)
//            self.likeButton!.setTitle("\(model.likeCount ?? 0)", forState: .Normal)
//            
//        }
//    }
//    
//    func rendrenderDislikeBtn(model:BaseComment) {
//        if model.isDisliked == true{
//            self.dislikeButton!.setImage(UIImage(named:"Step-on2_"), forState: .Normal)
//            self.dislikeButton!.setTitle("\(model.dislikeCount ?? 0)", forState: .Normal)
//            
//        }else{
//            self.dislikeButton!.setImage(UIImage(named:"Step-on_3"), forState: .Normal)
//            self.dislikeButton!.setTitle("\(model.dislikeCount ?? 0)", forState: .Normal)
//        }
//    }
    
    
    @objc func dismissReplyView() {
        self.removeFromSuperview()
    }
    
    @objc func replyBtnClick() {
        guard let model = self.model  else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: ReplyBtnClickToShowReplyController), object: self, userInfo: ["model":model])
        self.removeFromSuperview()
    }
    
    @objc func reportBtnClick() {
        guard let model = self.model  else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: ReportBtnClickToShowReportController), object: self, userInfo: ["model":model])
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
