//
//  CommentFootView.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class CommentFootView: UIView {

    var blackBgView = UIView()
    var bgView = UIView()
    var commentTextfield: UITextView = UITextView()
    var addCommentBtn: UIButton = UIButton()
    var bottomLine: UIView = UIView()
    var duration:Double = 0.5
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: AdaptiveUtils.screenWidth, height: AdaptiveUtils.screenHeight))
        createUI()
        initUI()
    }
    
    func createUI() {
        self.addSubview(blackBgView)
        blackBgView.snp.makeConstraints { (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        self.addSubview(bgView)
        bgView.addSubview(commentTextfield)
        bgView.addSubview(addCommentBtn)
        bgView.addSubview(bottomLine)
        bgView.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.bottom)
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(150)
        }
        bottomLine.snp.makeConstraints { (make) in
            make.left.equalTo(bgView)
            make.right.equalTo(bgView)
            make.height.equalTo(1)
            make.top.equalTo(bgView)
        }
        addCommentBtn.snp.makeConstraints { (make) in
            make.right.equalTo(bgView).offset(-10)
            make.bottom.equalTo(bgView).offset(-15)
            make.width.equalTo(60)
            make.height.equalTo(25)
        }
        commentTextfield.snp.makeConstraints { (make) in
            make.left.equalTo(bgView).offset(10)
            make.right.equalTo(bgView).offset(-10)
            make.top.equalTo(bottomLine.snp.bottom).offset(15)
            make.bottom.equalTo(addCommentBtn.snp.top).offset(-15)
        }
    }
    
    func initUI() {
        self.backgroundColor = UIColor.clear
        blackBgView.backgroundColor = UIColor.black
        blackBgView.alpha = 0.4
        bgView.backgroundColor = UIColor.planBackColor()
        bottomLine.backgroundColor = UIColor.gray
        commentTextfield.font = UIFont.systemFont(ofSize: 16)
        commentTextfield.backgroundColor = UIColor.white
        commentTextfield.returnKeyType = .done
        addCommentBtn.setTitle("Post", for: UIControlState())
        addCommentBtn.backgroundColor = UIColor.gray
        addCommentBtn.layer.cornerRadius = 5
        addCommentBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        commentTextfield.layer.cornerRadius = 5
        addCommentBtn.isUserInteractionEnabled = false
        blackBgView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(tapClick))
        blackBgView.addGestureRecognizer(tap)
        self.isHidden = true
    }
    
    @objc func tapClick() {
        self.hidden(self.duration)
    }
    
    func show(_ duration:Double, height:CGFloat) {
        self.duration = duration
        self.isUserInteractionEnabled = true
        self.isHidden = false
        UIView.animate(withDuration: duration, animations: {
            self.bgView.snp.remakeConstraints({ (make) in
                make.left.equalTo(self)
                make.right.equalTo(self)
                make.height.equalTo(150)
                make.top.equalTo(self.snp.bottom).offset(-height)
            })
            self.bgView.superview?.layoutIfNeeded()
        })
    }
    
    func hidden(_ duration:Double) {
        self.commentTextfield.resignFirstResponder()
        self.isUserInteractionEnabled = false
        UIView.animate(withDuration: duration, animations: {
            self.bgView.snp.remakeConstraints({ (make) in
                make.left.equalTo(self)
                make.right.equalTo(self)
                make.height.equalTo(150)
                make.top.equalTo(self.snp.bottom)
            })
            self.bgView.superview?.layoutIfNeeded()
        }, completion: { (bool) in
            self.isHidden = true
        }) 
    }
    
    func remove() {
        self.hidden(duration)
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }}
