//
//  NameBtn.swift
//  Horoscope
//
//  Created by xiao  on 17/2/27.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class NameBtn: UIButton {
    
    var model : ParentComment?
    
    init(frame: CGRect,model:ParentComment) {
        super.init(frame: frame)
        self.model = model
        
        self.titleLabel?.textAlignment = NSTextAlignment.left
        self.setTitle(model.userInfo?.name, for:UIControlState())
        self.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        self.setTitleColor(UIColor.purpleContentColor(), for: UIControlState())
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
