//
//  BaseComment.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class BaseComment: NSObject {
    var commentId   :String?
    var likeCount   :Int?
    var dislikeCount:Int?
    var createTime  :Int64?
    var content     :String?
    var imageList   :[String]?
    var userInfo    :User?
    var pos   :Int?
    var createTimeHuman:String?
    var postId :String?
    var anonymous:Bool?
    var isLiked:Bool?
    var isDisliked:Bool?
    var commentCount:Int?
    var horoscopeName:String?
    var forecastName:String?
    
    
    func parse(_ jsonData:JSON)  {
        if jsonData == JSON.null{
            return
        }
        commentId  = jsonData["commentId"].string
        likeCount = jsonData["likeCount"].intValue
        dislikeCount = jsonData["dislikeCount"].intValue
        createTime = jsonData["createTime"].int64
        content = jsonData["content"].string
        imageList = jsonData["imageList"].arrayObject as? [String]
        pos = jsonData["pos"].intValue
        postId = jsonData["postId"].string
        anonymous = jsonData["anonymous"].boolValue
        isLiked = jsonData["isLiked"].boolValue
        isDisliked = jsonData["isDisliked"].boolValue
        commentCount = jsonData["commentCount"].intValue
        createTimeHuman = jsonData["createTimeHuman"].string
        horoscopeName = jsonData["horoscopeName"].string
        forecastName = jsonData["forecastName"].string
        if let  userJson = jsonData["userInfo"].dictionaryObject{
          let user = User()
         user.initWitchDic(userJson as [String : AnyObject])
        self.userInfo = user
        }
    }
}
