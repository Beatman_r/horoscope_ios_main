//
//  ParentComment.swift
//  Horoscope
//
//  Created by xiao  on 17/2/12.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class ParentComment: BaseComment {
    override func parse(_ jsonData: JSON) {
        if jsonData == JSON.null{
            LXSWLog("jsonData is nil")
            return
        }
        super.parse(jsonData)
    }
    
    func sizeWithConstrainedToSize(_ size:CGSize) -> CGSize {
        let font = UIFont.systemFont(ofSize: 18)
        let textSize = (self.content ?? (""  as NSString) as String).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes:[NSAttributedStringKey.font:font], context: nil).size
        return textSize
    }
    func nameSize(_ size:CGSize) -> CGSize {
        let font = UIFont.systemFont(ofSize: 18)
        let textSize = (self.userInfo?.name ?? (""  as NSString) as String).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes:[NSAttributedStringKey.font:font], context: nil).size
        return textSize
    }
}
