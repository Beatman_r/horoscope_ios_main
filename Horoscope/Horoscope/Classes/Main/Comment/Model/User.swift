//
//  User.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class User: NSObject {
    var name : String?
    var avatar : String?
    var horoscopeName : String?
    var userId : String?
    func initWitchDic(_ dict:[String:AnyObject])  {
        name = dict["name"] as? String
        avatar = dict["avatar"] as? String
        userId = dict["userId"] as? String
        horoscopeName = dict["horoscopeName"] as? String
    }
    
    override init() {
        super.init()
    }
}
