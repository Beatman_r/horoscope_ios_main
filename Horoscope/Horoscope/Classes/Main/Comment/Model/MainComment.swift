//
//  MainComment.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.


import UIKit
import SwiftyJSON

class MainComment: BaseComment {
    
    var parentCommentWarp  :[String:AnyObject]?
    var parentCommentIdList:[String]?
    var parentCommentList :[BaseComment] = [BaseComment]()
    override func parse(_ jsonData: JSON) {
        if jsonData == JSON.null{
            LXSWLog("jsonData is nil")
            return
        }
        super.parse(jsonData)
        parentCommentWarp = jsonData["parentCommentWarp"].dictionaryObject as [String : AnyObject]?
        parentCommentIdList = jsonData["parentCommentIdList"].arrayObject as?[String]
    }
    
    func sizeWithConstrainedToSize(_ size:CGSize) -> CGSize {
        let font = UIFont.systemFont(ofSize: 18)
        let textSize = (self.content ?? (""  as NSString) as String).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes:[NSAttributedStringKey.font:font], context: nil).size
        return textSize
    }
    func nameSize(_ size:CGSize) -> CGSize {
        let font = UIFont.systemFont(ofSize: 18)
        let textSize = (self.userInfo?.name ?? (""  as NSString) as String).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes:[NSAttributedStringKey.font:font], context: nil).size
        return textSize
    }
}
