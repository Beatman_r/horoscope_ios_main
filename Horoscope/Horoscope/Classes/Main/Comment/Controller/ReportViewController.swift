//
//  ReportViewController.swift
//  Horoscope
//
//  Created by xiao  on 17/2/23.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD
class ReportViewController: NSObject {

    class func addacrion(_ model:BaseComment,postId:String,isPostReport:Bool,vc:UIViewController) {
        
        let alertVC =  UIAlertController(title: "Report Submitted", message: nil, preferredStyle: .actionSheet)
        
        if isPostReport  == true{
            let PornographicContentAction = UIAlertAction(title: "Pornographic Content", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportPost(postId, reason: "Pornographic Content")

                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(PornographicContentAction)
            
            let GraphicViolenceAction = UIAlertAction(title: "Graphic Violence", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportPost(postId, reason: "Graphic Violence")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(GraphicViolenceAction)
            
            let Harassment = UIAlertAction(title: "Harassment", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportPost(postId,reason: "Harassment")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Harassment)
            
            let HatefulorAbusiveContentAction = UIAlertAction(title: "Hateful or Abusive Content", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportPost(postId, reason: "Hateful or Abusive Content")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(HatefulorAbusiveContentAction)
            
            let InfringementAction = UIAlertAction(title: "Infringement", style: .default) { (action) in
                HoroscopeDAO.sharedInstance.reportPost(postId, reason: "Infringement")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(InfringementAction)
            
            let Fraud = UIAlertAction(title: "Fraud", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportPost(postId, reason: "Fraud")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Fraud)
            
            let Spam = UIAlertAction(title: "Spam", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportPost(postId, reason: "Spam")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Spam)
            
            let Others = UIAlertAction(title: "Others", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportPost(postId, reason: "Others")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Others)
            
            let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertVC.addAction(Cancel)
            vc.present(alertVC, animated: true, completion: nil)
        }else{
 
            let PornographicContentAction = UIAlertAction(title: "Pornographic Content", style: .default) { (action) in
                HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Pornographic Content")
               
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(PornographicContentAction)
            
            let GraphicViolenceAction = UIAlertAction(title: "Graphic Violence", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Graphic Violence")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(GraphicViolenceAction)
            
            let Harassment = UIAlertAction(title: "Harassment", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Harassment")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Harassment)
            
            let HatefulorAbusiveContentAction = UIAlertAction(title: "Hateful or Abusive Content", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Hateful or Abusive Content")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(HatefulorAbusiveContentAction)
            
            let InfringementAction = UIAlertAction(title: "Infringement", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Infringement")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(InfringementAction)
            
            let Fraud = UIAlertAction(title: "Fraud", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Fraud")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Fraud)
            
            let Spam = UIAlertAction(title: "Spam", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Spam")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Spam)
            
            let Others = UIAlertAction(title: "Others", style: .default) { (action) in
                 HoroscopeDAO.sharedInstance.reportComment(model.commentId ?? "", postId: model.postId ?? "", reason: "Others")
                HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
            }
            alertVC.addAction(Others)
            
            let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertVC.addAction(Cancel)
            vc.present(alertVC, animated: true, completion: nil)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

 

        // Dispose of any resources that can be recreated.
    }
    

   


