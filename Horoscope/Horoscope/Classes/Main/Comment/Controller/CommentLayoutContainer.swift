
//
//  CommentLayoutContainer.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let MaxOverlapNumber = 5
let OverlapSpace = 3
let ScreenW = UIScreen.main.bounds.size.width
class CommentLayoutContainer: UIView{
    
    //    var nameButton:UIButton?
    var nameLabel : UILabel?
    var timeLabel:UILabel?
    var headImageView:UIImageView?
    var mainCommentModel:MainComment?
    var commentLabel:UILabel?
    var parentCommentWarp :[String:AnyObject]?
    var isMyNotification:Bool?
    var parentCommentArray:[ParentComment] = [ParentComment]()
    var signLabel : UILabel?
    var divideView : UIView?
    
    //最外层带头像 但不包括评论的布局
    init(model:MainComment,isMyNotification:Bool) {
        super.init(frame: CGRect.zero)
        self.isMyNotification = isMyNotification
        self.backgroundColor = UIColor.basePurpleBackgroundColor()
        self.mainCommentModel = model
        guard let model = self.mainCommentModel else {
            return
        }
        self.parentCommentWarp = model.parentCommentWarp
        self.headImageView = UIImageView()
        self.headImageView?.sd_setImage(with: URL(string: model.userInfo?.avatar ?? ""), placeholderImage:  UIImage(named: "head_default"))
        self.headImageView?.layer.cornerRadius = 17.5
        self.headImageView?.layer.masksToBounds = true
        self.headImageView?.contentMode = UIViewContentMode.scaleAspectFill
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.nameBtnClick))
        self.headImageView?.addGestureRecognizer(tap)
        self.headImageView?.isUserInteractionEnabled = true
        
        //        self.nameButton = UIButton(type:.Custom)
        //        self.nameButton?.titleLabel?.textAlignment = NSTextAlignment.Left
        //        self.nameButton?.setTitle(model.userInfo?.name, forState:.Normal)
        //        self.nameButton?.titleLabel?.font = UIFont.systemFontOfSize(18)
        //        self.nameButton?.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        //        self.nameButton?.titleLabel?.adjustsFontSizeToFitWidth = true
        //        self.nameButton?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Fill
        //        self.nameButton?.setTitleColor(UIColor.purpleContentColor(), forState: .Normal)
        //        self.nameButton?.addTarget(self, action: #selector(self.nameBtnClick), forControlEvents: .TouchUpInside)
        //        self.nameButton?.userInteractionEnabled = true
        
        self.nameLabel = UILabel()
        self.nameLabel?.text = model.userInfo?.name ?? ""
        self.nameLabel?.font = HSFont.baseRegularFont(15)
        self.nameLabel?.textColor = UIColor.luckyPurpleTitleColor()
        
        self.signLabel = UILabel()
        self.signLabel?.textAlignment = .left
        self.signLabel?.text = model.userInfo?.horoscopeName ?? ""
        self.signLabel?.textColor = UIColor.commentTimeColor()
        self.signLabel?.font = HSFont.baseRegularFont(13)
        
        self.timeLabel = UILabel()
        self.timeLabel?.textColor = UIColor.gray
        self.timeLabel?.font = UIFont.systemFont(ofSize: 13)
        self.timeLabel?.text = model.createTimeHuman
        self.timeLabel?.textColor = UIColor.commentTimeColor()
        
        self.divideView = UIView()
        self.divideView?.backgroundColor = UIColor.divideColor()
        
        self.addSubview(self.headImageView!)
        //        self.addSubview(self.nameButton!)
        self.addSubview(self.nameLabel!)
        self.addSubview(self.signLabel!)
        self.addSubview(self.timeLabel!)
        self.addSubview(self.divideView!)
        
        self.headImageView?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.snp.top).offset(SCREEN_HEIGHT * 20 / 667)
            make.left.equalTo(self.snp.left).offset(12)
            make.height.equalTo(35)
            make.width.equalTo(35)
        })
        self.nameLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.headImageView?.snp.top)!)
            make.left.equalTo((self.headImageView?.snp.right)!).offset(SCREEN_HEIGHT * 20 / 667)
        })
        //        self.nameButton?.snp.makeConstraints({ (make) in
        //            make.top.equalTo((self.headImageView?.snp.top)!)
        //            make.left.equalTo((self.headImageView?.snp.right)!).offset(SCREEN_HEIGHT * 20 / 667)
        //        })
        self.signLabel?.snp.makeConstraints({ (make) in
            make.bottom.equalTo((self.headImageView?.snp.bottom)!)
            make.left.equalTo((self.headImageView?.snp.right)!).offset(SCREEN_HEIGHT * 20 / 667)
        })
        self.timeLabel?.snp.makeConstraints({ (make) in
            make.right.equalTo(self.snp.right).offset(-SCREEN_HEIGHT * 12 / 667)
            make.top.equalTo((self.headImageView?.snp.top)!)
        })
        self.divideView?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.snp.bottom).offset(-1)
            make.left.equalTo(self.snp.left).offset(0)
            make.right.equalTo(self.snp.right)
            make.height.equalTo(1)
        })
        
        if model.parentCommentList.count > 0  {
            var parentCommentArray:[ParentComment] = model.parentCommentList as! [ParentComment]
            let mainCmt  = ParentComment()
            change(mainCmt, mainComment: model)
            parentCommentArray.append(mainCmt)
            self.updateWithModelArray(parentCommentArray,model: model)
        }else{
            let mainCmt  = ParentComment()
            change(mainCmt, mainComment:model)
            parentCommentArray.append(mainCmt)
            self.updateWithModelArray(parentCommentArray,model: model)
        }
    }
    //把主评论转成二级评论，所有评论都按二级评论来处理
    func change(_ parentComment:ParentComment,mainComment:MainComment)  {
        parentComment.commentId = mainComment.commentId
        parentComment.likeCount = mainComment.likeCount
        parentComment.dislikeCount = mainComment.dislikeCount
        parentComment.createTime = mainComment.createTime
        parentComment.content = mainComment.content
        parentComment.imageList = mainComment.imageList
        parentComment.userInfo = mainComment.userInfo
        parentComment.pos = mainComment.pos
        parentComment.createTimeHuman = mainComment.createTimeHuman
        parentComment.postId = mainComment.postId
        parentComment.anonymous = mainComment.anonymous
        parentComment.isLiked  = mainComment.isLiked
        parentComment.isDisliked = mainComment.isDisliked
    }
    
    func updateWithModelArray(_ secondCommentArray:[ParentComment],model:MainComment) {
        var lastView:UIView? = nil
        var lastH = 0
        var i = 0
        let maxOverlapNumber = MaxOverlapNumber
        let overlapSpace = OverlapSpace
        let screenW = Int(ScreenW)
        
        //如果有需要折叠的楼层
        if self.parentCommentWarp != nil {
            let index = self.parentCommentWarp?["index"] as? Int ?? 0
            let total  = self.parentCommentWarp?["total"] as? Int ?? 0
            var arr = [ParentComment]()
            for item in 0..<secondCommentArray.count  {
                if item < 2   {
                    arr.append(secondCommentArray[item])
                }
            }
            let f = (maxOverlapNumber-2) * overlapSpace
            let x = 6 + f
            let y = 64 + f
            let w = (screenW - 10 - 2 * f)
            let h = 0
            let frame = CGRect(x: x, y: y , width: w, height: h)
            let gridLayoutView = CommentView(frame: frame, modelArray: arr)
            lastView = gridLayoutView
            lastH = Int(gridLayoutView.frame.size.height)
            self.addSubview(gridLayoutView)
            
            var offsetArr = [ParentComment]()
            let count = secondCommentArray.count
            for item in 0..<count{
                if item > (count - 3) {
                    offsetArr.append(secondCommentArray[item])
                }
            }
            var reference = 0
            for item in 0..<(offsetArr.count + 1) {
                i =  offsetArr.count - item
                let f = i * overlapSpace
                var offsetFrame = CGRect(x:CGFloat(3 + f), y: CGFloat(64 + f), width: CGFloat(screenW - 10 - 2 * f), height: 0)
                
                if item == index - 2 {
                    offsetFrame.size.height =  CGFloat(lastH) + 55
                    lastH = Int(offsetFrame.size.height)
                    let viewMoreBtn  = MoreCommentView(frame: offsetFrame, model: offsetArr[reference], parentView: lastView!, isLast: false)
                    viewMoreBtn.moreCommentBtn?.setTitle("View all \(total-3) comments", for: UIControlState())
                    viewMoreBtn.moreCommentBtn?.addTarget(self, action: #selector(self.moreCommentBtnClick), for: .touchUpInside)
                    self.insertSubview(viewMoreBtn, belowSubview: lastView!)
                    lastView = viewMoreBtn
                    continue
                }else{
                    let sz = offsetArr[reference].sizeWithConstrainedToSize(CGSize(width: offsetFrame.size.width - 50 , height: CGFloat(MAXFLOAT)))
                    
                    //last cell image shold unfold
                    if offsetArr[reference] == secondCommentArray.last && secondCommentArray.last?.imageList?.count > 0{
                        let imageViewH = CGFloat(160/667*AdaptiveUtils.screenHeight)
                        offsetFrame.size.height = sz.height + CGFloat(lastH) + 55 + 30 + imageViewH
                    }else{
                        let iamgeH = CGFloat((offsetArr[reference].imageList?.count) > 0 ? 25 : 0)
                        offsetFrame.size.height = sz.height + CGFloat(lastH) + 55 + 36 + iamgeH
                    }
                    lastH = Int(offsetFrame.size.height)
                    var layoutView:DeviationCommentView?
                    
                    if (lastView != nil) {
                        layoutView = DeviationCommentView(frame: offsetFrame, model: offsetArr[reference], parentView: lastView!, isLast: ((self.mainCommentModel?.commentId ?? "").isEqual(offsetArr[reference].commentId ?? "")),isMyNotification:self.isMyNotification ?? false)
                        self.insertSubview(layoutView!, belowSubview: lastView!)
                    }else{
                        layoutView = DeviationCommentView(frame: offsetFrame, model: offsetArr[reference], parentView: UIView(frame:CGRect.zero), isLast: (self.mainCommentModel?.commentId!.isEqual(offsetArr[reference].commentId))!,isMyNotification:self.isMyNotification ?? false)
                    }
                    lastView = layoutView
                    reference = reference + 1
                }
            }
            //if is MyNotification controller
            if self.isMyNotification == true {
                if model.imageList?.count > 0 {
                    
                    let seeOriginalBtnH = 37
                    self.frame = CGRect(x: 0, y: 0, width: screenW, height: lastH  + seeOriginalBtnH + 64)
                }else{
                    let seeOriginalBtnH = 37
                    self.frame = CGRect(x: 0, y: 0, width: screenW, height: lastH + 54 + seeOriginalBtnH)
                }
            }else{
                if model.imageList?.count > 0 {
                    let imageViewH = Int(160/667*AdaptiveUtils.screenHeight)
                    self.frame = CGRect(x: 0, y: 0, width: screenW, height: lastH + imageViewH + 54)
                }else{
                    self.frame = CGRect(x: 0, y: 0, width: screenW, height: lastH + 54)
                }
                
            }
        }
            //如果没有需要折叠的楼层
        else{
            if secondCommentArray.count > maxOverlapNumber {
                
                var arr = [ParentComment]()
                for item in 0..<secondCommentArray.count {
                    if item < (secondCommentArray.count - maxOverlapNumber  ) {
                        arr.append(secondCommentArray[item])
                    }
                }
                
                let f = maxOverlapNumber * overlapSpace
                let x = 6 + f
                let y = 64 + f
                let w = (screenW - 10 - 2 * f)
                let h = 0
                let frame = CGRect(x: x, y: y , width: w, height: h)
                let gridLayoutView = CommentView(frame: frame, modelArray: arr)
                lastView = gridLayoutView
                lastH = Int(gridLayoutView.frame.size.height)
                self.addSubview(gridLayoutView)
            }
            
            var offsetArr = [ParentComment]()
            for item in 0..<secondCommentArray.count {
                if item > (secondCommentArray.count - maxOverlapNumber - 1  ) {
                    offsetArr.append(secondCommentArray[item])
                }
            }
            
            for item in 0..<offsetArr.count {
                i = offsetArr.count - item
                let f = i * overlapSpace
                var offsetFrame = CGRect(x:CGFloat(6 + f), y: CGFloat(64 + f), width: CGFloat(screenW - 10 - 2 * f), height: 0)
                
                let sz = offsetArr[item].sizeWithConstrainedToSize(CGSize(width: offsetFrame.size.width - 50 , height: CGFloat(MAXFLOAT)))
                var layoutView:DeviationCommentView?
                
                if self.isMyNotification == true {
                    if offsetArr[item] == secondCommentArray.last && secondCommentArray.last?.imageList?.count > 0{
                        let imageViewH = CGFloat(160/667*AdaptiveUtils.screenHeight)
                        offsetFrame.size.height = sz.height + CGFloat(lastH) + 55 + 30 + imageViewH + 50
                    }else if  offsetArr[item] == secondCommentArray.last{
                        offsetFrame.size.height = sz.height + CGFloat(lastH) + 55 + 36  + 20
                    }else{
                        let iamgeH = CGFloat((offsetArr[item].imageList?.count) > 0 ? 25 : 0)
                        offsetFrame.size.height = sz.height + CGFloat(lastH) + 55 + 36 + iamgeH
                    }
                }else if self.isMyNotification == false{
                    if offsetArr[item] == secondCommentArray.last && secondCommentArray.last?.imageList?.count > 0{
                        let imageViewH = CGFloat(160/667*AdaptiveUtils.screenHeight)
                        offsetFrame.size.height = sz.height + CGFloat(lastH) + 55 + 30 + imageViewH
                    }else{
                        let iamgeH = CGFloat((offsetArr[item].imageList?.count) > 0 ? 25 : 0)
                        offsetFrame.size.height = sz.height + CGFloat(lastH) + 55 + 36 + iamgeH
                    }
                }
                lastH = Int(offsetFrame.size.height)
                if (lastView != nil) {
                    layoutView = DeviationCommentView(frame: offsetFrame, model: offsetArr[item], parentView: lastView!, isLast: (self.mainCommentModel?.commentId!.isEqual(offsetArr[item].commentId))!,isMyNotification:self.isMyNotification ?? false)
                    self.insertSubview(layoutView!, belowSubview: lastView!)
                }else{
                    layoutView = DeviationCommentView(frame: offsetFrame, model: offsetArr[item], parentView: UIView(frame:CGRect.zero), isLast: (self.mainCommentModel?.commentId!.isEqual(offsetArr[item].commentId))!,isMyNotification:self.isMyNotification ?? false)
                    self.addSubview(layoutView!)
                }
                lastView = layoutView
            }
            if self.isMyNotification == true {
                if model.imageList?.count > 0 {
                    let seeOriginalBtnH = 44
                    self.frame = CGRect(x: 0, y: 0, width: screenW, height: lastH  + 14 + seeOriginalBtnH )
                }else{
                    let seeOriginalBtnH = 44
                    self.frame = CGRect(x: 0, y: 0, width: screenW, height: lastH + 14 + seeOriginalBtnH)
                }
            }else{
                self.frame = CGRect(x: 0, y: 0, width: screenW, height: lastH + 44)
            }
        }
    }
    
    @objc func nameBtnClick() {
        guard let model = self.mainCommentModel else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: UserClickToUserTopic), object: self, userInfo: ["model":model])
    }
    
    @objc func moreCommentBtnClick() {
        
        if self.mainCommentModel != nil && self.mainCommentModel?.parentCommentWarp != nil  {
            NotificationCenter.default.post(name: Notification.Name(rawValue: MoreCommentBtnClick), object: nil, userInfo: ["mainCommet":self.mainCommentModel!])
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
