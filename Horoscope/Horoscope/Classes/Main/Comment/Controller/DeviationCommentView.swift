//
//  DeviationCommentView.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class DeviationCommentView: UIView {
    var model:ParentComment?
    var nameButton:UIButton?
    lazy var floorLabel:UILabel? = UILabel(frame:CGRect.zero)
    lazy var commentLabel:UILabel? = UILabel(frame:CGRect.zero)
    lazy var timeLabel:UILabel? = UILabel(frame:CGRect.zero)
    var parent:UIView?
    var isLastFloor:Bool?
    var likeButton:UIButton?
    var dislikeButton:UIButton?
    lazy var viewImageBtn:UIButton? = UIButton(type:.custom)
    lazy var imageView:UIImageView? = UIImageView()
    lazy var viewBigImageView = UIImageView()
    var isMyNotification:Bool?
    var seeOriginalLabel:UILabel?
    var viewLastBigImage = UIImageView()
    var signNameLabel : UILabel?
    
    init(frame:CGRect,model:ParentComment,parentView:UIView,isLast:Bool,isMyNotification:Bool) {
        super.init(frame: frame)
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        let tap  = UITapGestureRecognizer(target: self, action: #selector(self.cellDidTap))
        self.addGestureRecognizer(tap)
        self.isMyNotification = isMyNotification
        self.isLastFloor = isLast
        self.parent = parentView
        self.model = model
        if(!isLastFloor!){ 
            self.layer.borderWidth = 0.6
            self.layer.borderColor = UIColor(red: 72/255, green:56/255, blue: 107/255, alpha: 1).cgColor
            self.backgroundColor = UIColor.basePurpleBackgroundColor()
        }
        
        self.nameButton = NameBtn(frame: CGRect.zero, model: model)
        self.nameButton?.addTarget(self, action: #selector(self.nameBtnClick(_:)), for: .touchUpInside)
        
        self.timeLabel?.text = model.createTimeHuman
        self.timeLabel?.textColor = UIColor.commentTimeColor()
        self.timeLabel?.font = UIFont.systemFont(ofSize: 15)
        
        self.commentLabel?.text = model.content
        self.commentLabel?.numberOfLines = 0
        self.commentLabel?.textColor = UIColor.purpleContentColor()
        self.commentLabel?.font = UIFont.systemFont(ofSize: 15)
        
        self.signNameLabel = UILabel()
        self.signNameLabel?.text = model.userInfo?.horoscopeName ?? ""
        self.signNameLabel?.textColor = UIColor.commentTimeColor()
        self.signNameLabel?.font = HSFont.baseRegularFont(13)
        self.signNameLabel?.textAlignment = .left
        
        self.floorLabel?.font = UIFont.systemFont(ofSize: 11)
        self.floorLabel?.text = String(model.pos ?? 0)
        self.floorLabel?.textColor = UIColor.commentFloorColor()
        self.floorLabel?.backgroundColor = UIColor.commentFloorBackgroundColor()
        self.floorLabel?.textAlignment = .center
        
        self.likeButton = LikeBtn(frame: CGRect.zero, model: self.model ?? model)
        self.likeButton?.addTarget(self, action: #selector(self.likeBtnClick(_:)), for: .touchUpInside)
        
        self.dislikeButton = DisLikeBtn(frame: CGRect.zero, model: self.model ?? model)
        self.dislikeButton?.addTarget(self, action: #selector(self.dislikeBtnClick(_:)), for: .touchUpInside)
        
        self.viewImageBtn = ViewImageButton(frame: CGRect.zero, model: self.model ?? model)
        self.viewImageBtn?.addTarget(self, action:  #selector(self.viewImageBtnClick(_:)), for: .touchUpInside)
        self.viewImageBtn?.isUserInteractionEnabled = true
        
        self.floorLabel?.frame = CGRect(x: 0, y: parentView.frame.size.height + 15, width: 15, height: 12)
        self.nameButton?.frame = CGRect(x: 18, y: parentView.frame.size.height + 15, width: (120/375*AdaptiveUtils.screenWidth), height: 20)
        self.signNameLabel?.frame = CGRect(x: 18, y: Int((self.nameButton?.frame ?? CGRect.zero).maxY), width: Int(120/375*AdaptiveUtils.screenWidth), height: 15)
        let nameBtnX = nameButton!.frame.origin.x
        let timeLabelX = Int(SCREEN_WIDTH - nameButton!.frame.size.width - nameBtnX)
        self.timeLabel?.frame = CGRect(x: timeLabelX, y: Int(parentView.frame.size.height) + 5,  width: Int(150/375*AdaptiveUtils.screenWidth), height: 34)
        let sz = model.sizeWithConstrainedToSize(CGSize(width: self.frame.size.width - 24 , height: CGFloat(MAXFLOAT)))
        let commentLabelY = (signNameLabel?.frame.origin.y)! + (signNameLabel?.frame.height)!
        self.commentLabel?.frame = CGRect(x: nameBtnX, y: commentLabelY, width: self.frame.size.width - 24, height: model.content == "" ? 0 : sz.height)
        
        self.seeOriginalLabel = UILabel()
        self.seeOriginalLabel?.font = UIFont.systemFont(ofSize: 15)
        self.seeOriginalLabel?.text = "See Original"
        self.seeOriginalLabel?.textAlignment = NSTextAlignment.left
        self.seeOriginalLabel?.textColor = UIColor.commonPinkColor()
        let seeOriginalTap = UITapGestureRecognizer()
        seeOriginalTap.addTarget(self, action: #selector(seeOriginalBtnClick))
        self.seeOriginalLabel?.addGestureRecognizer(seeOriginalTap)
        self.seeOriginalLabel?.isUserInteractionEnabled = true
        
        if model.imageList?.count > 0 {
            self.viewImageBtn?.frame = CGRect(x:nameButton!.frame.origin.x, y: commentLabel!.frame.size.height + (commentLabel?.frame.origin.y)! + 8, width: 120, height: 18)
            let btnY = viewImageBtn!.frame.size.height + 5 + (viewImageBtn?.frame.origin.y)!
            self.dislikeButton?.frame = CGRect(x:  self.frame.size.width - 68, y: btnY, width: 80, height: 27)
            self.likeButton?.frame = CGRect(x:  self.frame.size.width - 143, y: btnY, width: 80, height: 27)
        }else{
            let btnY = commentLabel!.frame.size.height + 5 + (commentLabel?.frame.origin.y)!
            self.dislikeButton?.frame = CGRect(x:  self.frame.size.width - 68, y: btnY, width: 80, height: 27)
            self.likeButton?.frame = CGRect(x:  self.frame.size.width - 143, y: btnY, width: 80, height: 27)
        }
        
        self.addSubview(self.timeLabel!)
        self.addSubview(self.nameButton!)
        self.addSubview(self.commentLabel!)
        self.addSubview(self.floorLabel!)
        self.addSubview(self.likeButton!)
        self.addSubview(self.dislikeButton!)
        self.addSubview(self.viewImageBtn!)
        self.addSubview(self.signNameLabel!)
        
        self.timeLabel?.snp.makeConstraints({ (make) in
            make.right.equalTo(self.snp.right).offset(-12)
            make.top.equalTo(self.nameButton!.snp.top)
        })
        
        if(self.isLastFloor!){
            self.nameButton?.isHidden = true
            self.timeLabel?.isHidden = true
            self.floorLabel?.isHidden = true
            self.viewImageBtn?.isHidden = true
            self.signNameLabel?.isHidden = true
            self.commentLabel?.frame = CGRect(x: 5, y: parentView.frame.size.height + 8, width: self.frame.size.width - SCREEN_WIDTH * 12/375 , height:model.content == "" ? 0 : sz.height)
            let commentY = commentLabel!.frame.origin.y + commentLabel!.frame.height
            if self.isMyNotification == true {
                if model.imageList?.count > 0 {
                    let url = URL(string: model.imageList!.first!)
                    self.imageView?.sd_setImage(with: url, placeholderImage: UIImage(named: "defaultImgIcon"))
                    let imageViewW = 290/375*AdaptiveUtils.screenWidth
                    let imageViewH = 160/667*AdaptiveUtils.screenHeight
                    self.imageView?.frame = CGRect(x: 0, y:commentY+12, width: imageViewW/2, height: imageViewH)
                    let tap  = UITapGestureRecognizer(target: self, action: #selector(self.imageDidTap))
                    self.imageView?.addGestureRecognizer(tap)
                    self.imageView?.isUserInteractionEnabled = true
                    self.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                    let btnY = imageView!.frame.origin.y + imageView!.frame.height
                    self.dislikeButton?.frame = CGRect(x:  self.frame.size.width - 68, y:btnY  + 12, width: 80, height: 27)
                    self.likeButton?.frame = CGRect(x:  self.frame.size.width - 143, y: btnY  + 12, width: 80, height: 27)
                    //                    let seeOriginalBtnY = likeButton!.frame.origin.y + likeButton!.frame.height
                    //                    self.seeOriginalBtn?.frame = CGRect(x:-55, y:  seeOriginalBtnY + 10 , width: 120, height: 27)
                    self.addSubview(self.imageView!)
                    self.addSubview(self.seeOriginalLabel!)
                    self.seeOriginalLabel?.snp.makeConstraints({ (make) in
                        make.top.equalTo(self.likeButton!.snp.bottom).offset(10)
                        make.left.equalTo(self.snp.left)
                    })
                }else{
                    self.dislikeButton?.frame = CGRect(x:  self.frame.size.width - 68, y:commentY  + 12, width: 80, height: 27)
                    self.likeButton?.frame = CGRect(x:  self.frame.size.width - 143, y: commentY  + 12, width: 80, height: 27)
                    //                    let seeOriginalBtnY = likeButton!.frame.origin.y + likeButton!.frame.height
                    //                    self.seeOriginalBtn?.frame = CGRect(x:-55, y:  seeOriginalBtnY + 10, width: 120, height: 27)
                    self.addSubview(self.seeOriginalLabel!)
                    self.seeOriginalLabel?.snp.makeConstraints({ (make) in
                        make.top.equalTo(self.likeButton!.snp.bottom).offset(10)
                        make.left.equalTo(self.snp.left)
                    })
                }
                
            }else{
                if model.imageList?.count > 0 {
                    let url = URL(string: model.imageList!.first!)
                    self.imageView?.sd_setImage(with: url, placeholderImage: UIImage(named: "defaultImgIcon"))
                    let imageViewW = 290/375*AdaptiveUtils.screenWidth
                    let imageViewH = 160/667*AdaptiveUtils.screenHeight
                    self.imageView?.frame = CGRect(x: 0, y:commentY+12, width: imageViewW/2, height: imageViewH)
                    let tap  = UITapGestureRecognizer(target: self, action: #selector(self.imageDidTap))
                    self.imageView?.addGestureRecognizer(tap)
                    self.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                    self.imageView?.isUserInteractionEnabled = true
                    let btnY = imageView!.frame.origin.y + imageView!.frame.height
                    self.dislikeButton?.frame = CGRect(x:  self.frame.size.width - 68, y:btnY  + 12, width: 80, height: 27)
                    self.likeButton?.frame = CGRect(x:  self.frame.size.width - 143, y: btnY  + 12, width: 80, height: 27)
                    self.addSubview(self.imageView!)
                }else{
                    self.dislikeButton?.frame = CGRect(x:  self.frame.size.width - 68, y:commentY  + 12, width: 80, height: 27)
                    self.likeButton?.frame = CGRect(x:  self.frame.size.width - 143, y: commentY  + 12, width: 80, height: 27)
                }
            }
            self.dislikeButton?.isUserInteractionEnabled = true
            self.likeButton?.isUserInteractionEnabled = true
        }
    }
    
    func nameBtnClick() {
        print("----------nameBtnClick")
    }
    
    @objc func viewImageBtnClick(_ sender:UIButton) {
        let viewImageBtn = sender as! ViewImageButton
        
        guard let model = viewImageBtn.model else {
            return
        }
        guard let imageStr = model.imageList?.first else {
            return
        }
        let noti = NotificationCenter.default
        noti.post(name: Notification.Name(rawValue: "deviationClickBigImage"), object: nil, userInfo: ["url":imageStr])
    }
    
    func imageViewDidClickToDismiss() {
        self.viewBigImageView.removeFromSuperview()
        
    }
    
    func lastBigImageClickToDismiss() {
        self.viewLastBigImage.removeFromSuperview()
        
    }
    @objc func imageDidTap() {
        guard let model = self.model else {
            return
        }
        let imageStr = model.imageList!.first! 
        let noti = NotificationCenter.default
        noti.post(name: Notification.Name(rawValue: "deviationClickBigImage"), object: nil, userInfo: ["url":imageStr])
    }
    
    @objc func nameBtnClick(_ sender:UIButton) {
        let btn  = sender as! NameBtn
        guard let model = btn.model else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: UserClickToUserTopic), object: self, userInfo: ["model":model])
    }
    
    @objc func cellDidTap() {
        guard let model = model else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: CommentViewClickToShowReplyView), object: nil, userInfo: ["model":model])
    }
    
    @objc func seeOriginalBtnClick() {
        guard let model = model else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: SeeOriginalBtnClick), object: nil, userInfo: ["model":model])
    }
    
    @objc func likeBtnClick(_ sender:UIButton) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let btn  = sender as! LikeBtn
            guard let model = btn.model else {
                return
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: LikeBtnClick), object: self, userInfo: ["model":model])
        }else{
            let btn  = sender as! LikeBtn
            guard let model = btn.model else {
                return
            }
            
            if model.isLiked == false{
                model.isLiked = true
                let newCount = (model.likeCount ?? 0) + 1
                model.likeCount = newCount
                btn.model?.isLiked = true
                btn.model?.likeCount = newCount
                self.renderLikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: model.commentId ?? "")
                //最后一个DeviationView的状态得通过通知改最上层的model，如果有其他合理的方法可以更改此处
                NotificationCenter.default.post(name: Notification.Name(rawValue: LikeBtnClick), object: self, userInfo: ["model":model])
            }
            else  if model.isLiked == true {
                var newCount = (model.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                model.likeCount = newCount
                model.isLiked = false
                btn.model?.isLiked = false
                btn.model?.likeCount = newCount
                self.renderLikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: model.commentId ?? "")
                NotificationCenter.default.post(name: Notification.Name(rawValue: LikeBtnClick), object: self, userInfo: ["model":model])
            }
        }
    }
    
    @objc func dislikeBtnClick(_ sender:UIButton) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let btn  = sender as! DisLikeBtn
            guard let model = btn.model else {
                return
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: DisLikeBtnClick), object: self, userInfo: ["model":model])
        }else{
            let btn  = sender as! DisLikeBtn
            guard let model = btn.model else {
                return
            }
            
            if  model.isDisliked == false{
                model.isDisliked = true
                let newCount = (model.dislikeCount ?? 0) + 1
                model.dislikeCount = newCount
                btn.model?.isDisliked = true
                btn.model?.dislikeCount = newCount
                self.rendrenderDislikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: model.commentId ?? "")
                NotificationCenter.default.post(name: Notification.Name(rawValue: DisLikeBtnClick), object: self, userInfo: ["model":model])
            }
            else if  model.isDisliked == true{
                model.isDisliked = false
                var newCount = (model.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                btn.model?.isDisliked = false
                btn.model?.dislikeCount = newCount
                model.dislikeCount = newCount
                self.rendrenderDislikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: model.commentId ?? "")
                NotificationCenter.default.post(name: Notification.Name(rawValue: DisLikeBtnClick), object: self, userInfo: ["model":model])
            }
        }
    }
    
    func renderLikeBtn(_ model:ParentComment) {
        if model.isLiked == true{
            self.likeButton!.setImage(UIImage(named:"praise2_"), for: UIControlState())
            self.likeButton!.setTitle("\(model.likeCount ?? 0)", for: UIControlState())
        }else{
            self.likeButton!.setImage(UIImage(named:"praise_"), for: UIControlState())
            self.likeButton!.setTitle("\(model.likeCount ?? 0)", for: UIControlState())
            
        }
    }
    
    func rendrenderDislikeBtn(_ model:ParentComment) {
        if model.isDisliked == true{
            self.dislikeButton!.setImage(UIImage(named:"Step-on2_"), for: UIControlState())
            self.dislikeButton!.setTitle("\(model.dislikeCount ?? 0)", for: UIControlState())
            
        }else{
            self.dislikeButton!.setImage(UIImage(named:"Step-on_"), for: UIControlState())
            self.dislikeButton!.setTitle("\(model.dislikeCount ?? 0)", for: UIControlState())
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
