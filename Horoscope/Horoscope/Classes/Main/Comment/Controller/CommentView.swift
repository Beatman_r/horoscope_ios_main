//
//  CommentView.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CommentView: UIView {
    var nameButton:UIButton?
    var floorLabel:UILabel?
    var commentLabel:UILabel?
    var timeLabel:UILabel?
    var parent:UIView?
    var isLastFloor:Bool?
    var likeButton:LikeBtn?
    var dislikeButton:DisLikeBtn?
    var commentId:String = ""
    var viewImageBtn:ViewImageButton?
    var  modelArray = [ParentComment]()
    lazy var viewBigImageView = UIImageView()
    var likeCountBtn:UIButton?
    var dislikeCountBtn:UIButton?
    var likeBtnArray = [LikeBtn]()
    var disLikeBtnArray = [DisLikeBtn]()
    var signNameLabel : UILabel?
    
    
    init(frame:CGRect,modelArray:[ParentComment]){
        super.init(frame: frame)
        self.layer.borderWidth = 0.6
        self.layer.borderColor = UIColor(red: 72/255, green:56/255, blue: 107/255, alpha: 1).cgColor
        self.backgroundColor = UIColor.basePurpleBackgroundColor()
        var lastH = 0
        var cellH = 0
        self.modelArray = modelArray
        for model in modelArray {
            let comentSize = model.sizeWithConstrainedToSize(CGSize(width: self.frame.size.width - 10, height: CGFloat(MAXFLOAT)))
            self.floorLabel = UILabel(frame:CGRect(x: 0, y: lastH + 15, width: 15, height: 12))
            self.floorLabel?.font = UIFont.systemFont(ofSize: 11)
            self.floorLabel?.text = String(model.pos ?? 0)
            self.floorLabel?.textColor = UIColor.commentFloorColor()
            self.floorLabel?.textAlignment = .center
            self.floorLabel?.backgroundColor = UIColor.commentFloorBackgroundColor()
            self.floorLabel?.numberOfLines = 1
            
            self.nameButton = NameBtn(frame: CGRect(x: 18, y:  15 + lastH, width: Int(120/375*AdaptiveUtils.screenWidth), height: 20), model: model)
            self.nameButton?.addTarget(self, action: #selector(self.nameBtnClick(_:)), for: .touchUpInside)
            
            let nameBtnX = nameButton!.frame.origin.x
            let timeLabelX = Int(nameBtnX + SCREEN_WIDTH * 125/375 + nameButton!.frame.size.width)
            self.timeLabel = UILabel(frame:CGRect(x: timeLabelX, y:  5 + lastH , width: Int(150/375*AdaptiveUtils.screenWidth), height: 34))
            self.timeLabel?.text = model.createTimeHuman
            self.timeLabel?.textColor = UIColor.commentTimeColor()
            self.timeLabel?.font = UIFont.systemFont(ofSize: 13)
            
            self.commentLabel = UILabel(frame:CGRect(x: nameBtnX , y: nameButton!.frame.origin.y + nameButton!.frame.size.height + 5, width: frame.size.width - 10, height: comentSize.height + 20))
            self.commentLabel?.text = model.content
            self.commentLabel?.numberOfLines = 0
            self.commentLabel?.textColor = UIColor.purpleContentColor()
            self.commentLabel?.font = UIFont.systemFont(ofSize: 18)
            
            self.signNameLabel = UILabel()
            self.signNameLabel?.text = model.userInfo?.horoscopeName ?? ""
            self.signNameLabel?.textColor = UIColor.commentTimeColor()
            self.signNameLabel?.font = HSFont.baseRegularFont(13)
            self.signNameLabel?.textAlignment = .left
            self.signNameLabel?.frame = CGRect(x: 18, y: Int((self.nameButton?.frame ?? CGRect.zero).maxY), width: Int(120/375*AdaptiveUtils.screenWidth), height: 15)
            
            self.viewImageBtn = ViewImageButton(frame: CGRect.zero, model: model)
            self.viewImageBtn?.addTarget(self, action:  #selector(self.viewImageBtnClick(_:)), for: .touchUpInside)
            
            self.likeButton = LikeBtn(frame: CGRect.zero, model: model)
            self.likeButton?.addTarget(self, action: #selector(self.likeBtnClick(_:)), for: .touchUpInside)
            self.likeBtnArray.append(likeButton!)
            
            self.dislikeButton = DisLikeBtn(frame: CGRect.zero, model: model)
            self.dislikeButton?.addTarget(self, action: #selector(self.dislikeBtnClick(_:)), for: .touchUpInside)
            self.disLikeBtnArray.append(dislikeButton!)
            
            if  model.imageList?.count > 0 {
                self.viewImageBtn?.frame = CGRect(x:nameBtnX, y: commentLabel!.frame.size.height + 5 + (commentLabel?.frame.origin.y)! , width: 120, height: 27)
                let btnY = viewImageBtn!.frame.size.height + 5 + (viewImageBtn?.frame.origin.y)! + 16
                self.dislikeButton?.frame = CGRect(x:  self.frame.size.width - 68, y:btnY, width: 80, height: 27)
                self.likeButton?.frame = CGRect(x:  self.frame.size.width - 143, y:  btnY, width: 80, height: 27)
            }else{
                let btnY = commentLabel!.frame.size.height + 5 + (commentLabel?.frame.origin.y)! + 10
                self.dislikeButton?.frame = CGRect(x:self.frame.size.width - 68, y: btnY, width: 80, height: 27)
                self.likeButton?.frame = CGRect(x:self.frame.size.width - 143, y: btnY, width: 80, height: 27)
            }
            
            let viewImageH = Int(self.viewImageBtn?.frame.height ?? 0)
            cellH = Int(self.commentLabel!.frame.height) + viewImageH + 75
            let backView  = GridCommentCell()
            backView.frame = CGRect(x: 5, y:  5 + lastH, width: Int(self.frame.size.width - 10), height: cellH)
            backView.model = model
            self.addSubview(backView)
            self.addSubview(self.timeLabel!)
            self.addSubview(self.nameButton!)
            self.addSubview(self.commentLabel!)
            self.addSubview(self.signNameLabel!)
            self.addSubview(self.floorLabel!)
            self.addSubview(self.likeButton!)
            self.addSubview(self.dislikeButton!)
            self.addSubview(self.viewImageBtn!)

            let r = CGRect(x: 0, y: likeButton!.frame.origin.y + likeButton!.frame.size.height + 10, width: self.bounds.size.width, height: 0.6)
            let view = UIView(frame: r) 
            view.backgroundColor = UIColor(red: 72/255, green:56/255, blue: 107/255, alpha: 1)
            if(modelArray.last != modelArray.first){
                self.addSubview(view)
            }
            lastH = Int(r.origin.y)
            
            var fr = frame
            fr.size.height = CGFloat(lastH)
            self.frame = fr
        }
    }

    @objc func likeBtnClick(_ sender:UIButton) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let btn  = sender as! LikeBtn
            guard let model = btn.model else {
                return
            }
         NotificationCenter.default.post(name: Notification.Name(rawValue: LikeBtnClick), object: self, userInfo: ["model":model])
        }else{
            let btn  = sender as! LikeBtn
            guard let model = btn.model else {
                return
            }
            
            if model.isLiked == false{
                model.isLiked = true
                let newCount = (model.likeCount ?? 0) + 1
                model.likeCount = newCount
                btn.model?.isLiked = true
                btn.model?.likeCount = newCount
                self.renderLikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: model.commentId ?? "")
                NotificationCenter.default.post(name: Notification.Name(rawValue: LikeBtnClick), object: self, userInfo: ["model":model])
            }
            else  if model.isLiked == true {
                var newCount = (model.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                model.likeCount = newCount
                model.isLiked = false
                btn.model?.isLiked = false
                btn.model?.likeCount = newCount
                self.renderLikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: model.commentId ?? "")
           }
        }
    }
    
    @objc func dislikeBtnClick(_ sender:UIButton) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let btn  = sender as! DisLikeBtn
            guard let model = btn.model else {
                return
            }
             NotificationCenter.default.post(name: Notification.Name(rawValue: DisLikeBtnClick), object: self, userInfo: ["model":model])
        }else{
            let btn  = sender as! DisLikeBtn
            guard let model = btn.model else {
                return
            }
            
            if  model.isDisliked == false{
                model.isDisliked = true
                let newCount = (model.dislikeCount ?? 0) + 1
                model.dislikeCount = newCount
                btn.model?.isDisliked = true
                btn.model?.dislikeCount = newCount
                self.rendrenderDislikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: model.commentId ?? "")
                NotificationCenter.default.post(name: Notification.Name(rawValue: DisLikeBtnClick), object: self, userInfo: ["model":model])
            }
            else if  model.isDisliked == true{
                model.isDisliked = false
                var newCount = (model.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                btn.model?.isDisliked = false
                btn.model?.dislikeCount = newCount
                model.dislikeCount = newCount
                self.rendrenderDislikeBtn(model)
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: model.commentId ?? "")
            }
        }
    }

    @objc func nameBtnClick(_ sender:UIButton) {
        let btn  = sender as! NameBtn
        guard let model = btn.model else {
            return
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: UserClickToUserTopic), object: self, userInfo: ["model":model])
    }
    
    @objc func viewImageBtnClick(_ sender:UIButton) {
        let viewImageBtn = sender as! ViewImageButton
        
        guard let model = viewImageBtn.model else {
            return
        }
        guard let imageStr = model.imageList?.first else {
            return
        }
        
        let noti = NotificationCenter.default
        noti.post(name: Notification.Name(rawValue: "deviationClickBigImage"), object: nil, userInfo: ["url":imageStr])
    }
    
    func imageViewDidClickToDismiss() {
        self.viewBigImageView.removeFromSuperview()
    }
    
    func renderLikeBtn(_ model:ParentComment) {
        if model.isLiked == true{
            for btn  in self.likeBtnArray {
                if btn.model?.commentId == model.commentId {
                   btn.setImage(UIImage(named:"praise2_"), for: UIControlState())
                    btn.setTitle("\(model.likeCount ?? 0)", for: UIControlState())
                }
            }
        }else{
            for btn  in self.likeBtnArray {
                if btn.model?.commentId == model.commentId {
                   btn.setImage(UIImage(named:"praise_"), for: UIControlState())
                   btn.setTitle("\(model.likeCount ?? 0)", for: UIControlState())
                }
            }
        }
    }
    
    func rendrenderDislikeBtn(_ model:ParentComment) {
        if model.isDisliked == true{
            for btn  in self.disLikeBtnArray {
                if btn.model?.commentId == model.commentId {
                    btn.setImage(UIImage(named:"Step-on2_"), for: UIControlState())
                    btn.setTitle("\(model.dislikeCount ?? 0)", for: UIControlState())
                }
            }
        }else{
            for btn  in self.disLikeBtnArray {
                if btn.model?.commentId == model.commentId {
                    btn.setImage(UIImage(named:"Step-on_"), for: UIControlState())
                    btn.setTitle("\(model.dislikeCount ?? 0)", for: UIControlState())
                }
            }
        }

    }
        
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
