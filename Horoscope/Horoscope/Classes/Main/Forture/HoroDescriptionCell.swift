//
//  HoroDescriptionCell.swift
//  Horoscope
//
//  Created by Wang on 16/11/26.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit

class HoroDescriptionCell: BaseCardCell {
    let backView = UIView()
    let viewCountLabel = UILabel()
    let likeCountLabel = UILabel()
    let dislikeCountLabel = UILabel()
    let commentCountLabel = UILabel()
    let screenW = AdaptiveUtils.screenWidth
    let screenH = AdaptiveUtils.screenHeight
    
    weak var delegete:NormalPostCellDelegate?
    weak var rootVC : UIViewController?

    
    var postModel:ReadPostModel?{
        didSet{
            renderCell()
            //         likeCountLabel.text = String(postModel?.likeCount ?? 0)
            //         dislikeCountLabel.text = String(postModel?.dislikeCount ?? 0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
        let topTap = UITapGestureRecognizer()
        topTap.addTarget(self, action: #selector(topOpenRssURL))
        self.rssTopLabel.addGestureRecognizer(topTap)
        self.rssTopLabel.isUserInteractionEnabled = true
        
        let bottomTap = UITapGestureRecognizer()
        bottomTap.addTarget(self, action: #selector(bottomOpenRssURL))
        self.rssBottomLabel.addGestureRecognizer(bottomTap)
        self.rssBottomLabel.isUserInteractionEnabled = true
        
        rateStartButton.addTarget(self, action: #selector(gotoAppStoreRate), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer()
        adviceLabel.isUserInteractionEnabled = true
        tapGesture.addTarget(self, action: #selector(clickRateUSDetail))
        adviceLabel.addGestureRecognizer(tapGesture)
    }
    
    @objc func topOpenRssURL() {
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/bios/rick-levine/?code=dailyinnovations&utm_medium=Referral&utm_source=DailyInnovations&utm_campaign=daily_app&utm_content=rick-levine-bio")!)
    }
    
    @objc func bottomOpenRssURL() {
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/readings-reports/tarot-readings/daily-reflection-tarot-reading?code=dailyinnovations&utm_medium=Referral&utm_source=DailyInnovations&utm_campaign=daily_app&utm_content=tarot-reading")!)
    }
    
    let dislikeButton:UIButton = {
        let likeButton = UIButton(type: .custom)
        return likeButton
    }()
    
    let likeButton:UIButton = {
        let likeButton = UIButton(type: .custom)
        return likeButton
    }()
    
    func createUI() {
        self.cornerBackView?.addSubview(self.topImage)
        self.cornerBackView?.addSubview(nameAndAvaBackview)
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.nameAndAvaBackview.addSubview(nameLab)
        self.nameAndAvaBackview.addSubview(dateLab)
        self.cornerBackView?.addSubview(contentLab)
        self.cornerBackView?.addSubview(backView)
        self.cornerBackView?.addSubview(rssTopLabel)
        self.cornerBackView?.addSubview(rssBottomLabel)
//        self.cornerBackView?.addSubview(rateStartButton)
//        self.cornerBackView?.addSubview(adviceLabel)
        
        likeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        likeButton.setImage(UIImage(named:"praise_" ), for: UIControlState())
        likeButton.setImage(UIImage(named:"praise2_" ), for: .selected)
        likeButton.addTarget(self, action: #selector(praiseBtnClick), for: .touchUpInside)
        
        backView.addSubview(likeButton)
        
        likeCountLabel.font = UIFont.systemFont(ofSize: 15)
        likeCountLabel.text = String(self.postModel?.likeCount ?? 0)
        likeCountLabel.textColor = UIColor.purpleContentColor()
        
        backView.addSubview(likeCountLabel)
        
        dislikeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        dislikeButton.addTarget(self, action: #selector(treadBtnClick), for: .touchUpInside)
        backView.addSubview(dislikeButton)
        
        dislikeCountLabel.font = UIFont.systemFont(ofSize: 15)
        dislikeCountLabel.text = String(self.postModel?.dislikeCount ?? 0)
        dislikeCountLabel.textColor = UIColor.purpleContentColor()
        backView.addSubview(dislikeCountLabel)
        
        let commentButton = UIButton(type: .custom)
        commentButton.addTarget(self, action: #selector(commentBtnClick), for: .touchUpInside)
        commentButton.setImage(UIImage(named:"comments_" ), for: UIControlState())
        backView.addSubview(commentButton)
        
        commentCountLabel.textColor = UIColor.luckyPurpleTitleColor()
        commentCountLabel.textAlignment = .left
        commentCountLabel.font = HSFont.baseRegularFont(13)
        backView.addSubview(commentCountLabel)
        
        let shareButton = UIButton(type: .custom)
        shareButton.setImage(UIImage(named:"CB_share_" ), for: UIControlState())
        backView.addSubview(shareButton)
        shareButton.addTarget(self, action: #selector(goFBShare), for: .touchUpInside)
        
        self.topImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.cornerBackView!.snp.top)
            make.width.equalTo(SCREEN_WIDTH)
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.height.equalTo(SCREEN_WIDTH / 1.91+54)
        }
        
        self.nameAndAvaBackview.snp.makeConstraints { (make) in
            make.top.equalTo(self.topImage.snp.bottom)
            make.width.equalTo(SCREEN_WIDTH-24)
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.height.equalTo(64)
        }
        
        rssTopLabel.snp.makeConstraints { (make) in
            make.left.equalTo(dateLab.snp.left)
            make.right.equalTo(contentLab.snp.right)
            make.top.equalTo(dateLab.snp.bottom).offset(8)
        }
        
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.top.equalTo(self.rssTopLabel.snp.bottom).offset(4)
        }
        
        rssBottomLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentLab.snp.bottom).offset(4)
            make.left.equalTo(self.contentLab.snp.left)
            make.right.equalTo(self.contentLab.snp.right)
        }
        
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        addRateStartToolView()
//        rateStartButton.snp.makeConstraints { (make) in
//            make.height.equalTo(44)
//            make.left.equalTo(self.cornerBackView!.snp.left).offset(20)
//            make.right.equalTo(self.cornerBackView!.snp.right).offset(-20)
//            make.top.equalTo(self.rssBottomLabel.snp.bottom).offset(35)
//        }
//        adviceLabel.snp.makeConstraints { (make) in
//            make.height.equalTo(20)
//            make.left.equalTo(self.cornerBackView!.snp.left).offset(86)
//            make.right.equalTo(self.cornerBackView!.snp.right).offset(-86)
//            make.top.equalTo(self.rateStartButton.snp.bottom).offset(6)
//        }
//        backView.snp.makeConstraints { (make) in
//            make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
//            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
//            make.top.equalTo(self.adviceLabel.snp.bottom).offset(10)
//            make.height.equalTo(35/667*screenH)
//        }
        
        likeButton.snp.makeConstraints { (make) in
            make.top.equalTo(backView)
            make.left.equalTo(backView)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }
        
        likeCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(likeButton.snp.centerY)
            make.left.equalTo(likeButton.snp.right).offset(10/375*screenW)
            make.width.equalTo(50/375*screenW)
            make.height.equalTo(24)
        }
        
        dislikeButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(likeButton.snp.centerY)
            make.left.equalTo(likeCountLabel.snp.right).offset(10/375*screenW)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }
        
        dislikeCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(likeButton.snp.centerY)
            make.left.equalTo(dislikeButton.snp.right).offset(10/375*screenW)
            make.width.equalTo(50/375*screenW)
            make.height.equalTo(24)
        }
        
        shareButton.snp.makeConstraints { (make) in
            make.right.equalTo(backView.snp.right)
            make.centerY.equalTo(likeButton.snp.centerY)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }
        
        commentButton.snp.makeConstraints { (make) in
            make.right.equalTo(backView.snp.right).offset(-100/375*screenW)
            make.centerY.equalTo(likeButton.snp.centerY)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }
        commentCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(commentButton.snp.centerY)
            make.left.equalTo(commentButton.snp.right).offset(10)
        }
    }
    
    let topImage : UIImageView = {
        let topImage = UIImageView()
        topImage.image = UIImage(named: "\(CURRENT_MAIN_SIGN?.lowercased() ?? "taurus")_charaTop")
        topImage.contentMode = UIViewContentMode.scaleAspectFill
        topImage.isUserInteractionEnabled = true
        return topImage
    }()
    
    let rssTopLabel:UILabel = {
        let lab  = UILabel()
        lab.textColor = UIColor(hexString: "#70609C")
        let soureTextAttr = NSMutableAttributedString(string: NSLocalizedString("Written by Tarot's Rick Levine", comment: ""))
        let range = NSRange(location: 0, length: soureTextAttr.length)
        let style = NSNumber(value:Int8(NSUnderlineStyle.styleSingle.rawValue))
        soureTextAttr.addAttribute(NSAttributedStringKey.underlineStyle, value: style, range: range)
        soureTextAttr.addAttribute(NSAttributedStringKey.foregroundColor, value:lab.textColor, range: range)
        lab.attributedText = soureTextAttr;
        lab.font = HSFont.baseRegularFont(13)
        lab.numberOfLines = 0
        return lab
    }()
    
    let rssBottomLabel:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.textColor = UIColor(hexString: "#70609C")
        lab.font = HSFont.baseRegularFont(13)
        let soureTextAttr = NSMutableAttributedString(string: NSLocalizedString("Need more insight? See what the cards have to say...", comment: ""))
        let range = NSRange(location: 0, length: soureTextAttr.length)
        let style = NSNumber(value:Int8(NSUnderlineStyle.styleSingle.rawValue))
        soureTextAttr.addAttribute(NSAttributedStringKey.underlineStyle, value: style, range: range)
        soureTextAttr.addAttribute(NSAttributedStringKey.foregroundColor, value: lab.textColor, range: range)
        lab.attributedText = soureTextAttr;
        return lab
    }()
    
    let adviceLabel:UILabel = {
        let adviceLabel = UILabel()
        adviceLabel.font = UIFont(name: "PingFangSC-Regular", size: 13)
        adviceLabel.textColor = UIColor.rssSourceBlueColor()
        let soureTextAttr = NSMutableAttributedString(string: "Dissatisfied? Please give us advices")
        let range1 = NSRange(location: 0, length: soureTextAttr.length)
        let number = NSNumber(value:Int8(NSUnderlineStyle.styleSingle.rawValue))
        soureTextAttr.addAttribute(NSAttributedStringKey.underlineStyle, value: number, range: range1)
        soureTextAttr.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.rssSourceBlueColor(), range: range1)
        adviceLabel.attributedText = soureTextAttr;
        return adviceLabel
    }()
    
    let rateStartButton:UIButton = {
        let rateButton = UIButton(type:.custom)
        rateButton.setTitle("Very accurate, rate the app now!", for: .normal)
        rateButton.titleLabel?.font = UIFont(name: "PingFangSC-Medium", size: 16)
        rateButton.titleLabel?.textAlignment = NSTextAlignment.center
        rateButton.setTitleColor(UIColor(hexString: "#2B133A"), for: .normal)
        rateButton.layer.cornerRadius = 22
        rateButton.layer.masksToBounds = true
        rateButton.isUserInteractionEnabled = true
        rateButton.setBackgroundImage(UIImage(named: "fortune_appstore_rateUs"), for: .normal)
        rateButton.addTarget(self, action: #selector(gotoAppStoreRate), for: .touchUpInside)
        return rateButton
    }()
    
    let nameAndAvaBackview:UIView = {
        let view = UIView()
        view.backgroundColor=UIColor.clear
//        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width-2*BaseCardCell.cellPadding, height: 64)
        return view
    }()
    
    let avaIma:UIImageView = {
        let ima = UIImageView()
        ima.frame = CGRect(x: 12, y: 12, width: 40, height: 40)
        ima.layer.masksToBounds = true
        ima.layer.cornerRadius = 20
        return ima
    }()
    
    let nameLab:UILabel = {
        let lab = UILabel()
        lab.frame = CGRect(x: 12, y: 13, width: 180, height: 22)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        let font = HSFont.baseRegularFont(20)
        lab.font = font
        lab.textAlignment = .left
        return lab
    }()
    
    let dateLab:UILabel = {
        let lab = UILabel()
        lab.frame = CGRect(x: 12,y: 40, width: 150, height: 18)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        // let font = HSFont.baseLightFont(15)
        
        lab.font = HSFont.baseRegularFont(15)
        lab.textAlignment = .left
        return lab
    }()
    
    let contentLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.luckyPurpleTitleColor()
        lab.backgroundColor = UIColor.clear
        let font = HSFont.baseLightFont(18)
        lab.font = font
        lab.numberOfLines=0
        lab.textAlignment = .left
        return lab
    }()
    
    //MARK:BtnClik
    //MARK:BtnClick
    @objc func commentBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let postVC = PostViewController()
            postVC.isFirstClassReply = true
            postVC.isForcastReply = true
            postVC.horoscopeName = CURRENT_MAIN_SIGN
            postVC.forecastName = "today"
            let postId = self.postModel?.postId ?? ""
            let model = MyPostModel(postId: postId)
            let readModel = ReadPostModel.init(model: model)
            postVC.postModel = readModel
            self.rootVC?.navigationController?.pushViewController(postVC, animated: true)
        }
    }
    
    func addRateStartToolView()
    {
        let isAlreadyClick =  UserDefaults.standard.bool(forKey: "isAlreadyClick")
        let isTodayFortuneType =  UserDefaults.standard.bool(forKey: "isTodayFortuneType")
        if isAlreadyClick == false && isTodayFortuneType == true
        {
            
            self.cornerBackView?.addSubview(rateStartButton)
            self.cornerBackView?.addSubview(adviceLabel)
  
            rateStartButton.snp.makeConstraints { (make) in
                make.height.equalTo(44)
                make.left.equalTo(self.cornerBackView!.snp.left).offset(20)
                make.right.equalTo(self.cornerBackView!.snp.right).offset(-20)
                make.top.equalTo(self.rssBottomLabel.snp.bottom).offset(35)
            }
            adviceLabel.snp.makeConstraints { (make) in
                make.height.equalTo(20)
                make.left.equalTo(self.cornerBackView!.snp.left).offset(70)
                make.right.equalTo(self.cornerBackView!.snp.right).offset(-70)
                make.top.equalTo(self.rateStartButton.snp.bottom).offset(12)
            }
            
            backView.snp.makeConstraints { (make) in
                make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
                make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
                make.top.equalTo(self.adviceLabel.snp.bottom).offset(35)
                make.height.equalTo(35/667*screenH)
            }
        }
        else{
            
            backView.snp.makeConstraints { (make) in
                make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
                make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
                make.top.equalTo(self.rssBottomLabel.snp.bottom).offset(20)
                make.height.equalTo(35/667*screenH)
            }
        }
    }
    
    
    @objc func praiseBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.postModel?.iliked == false{
                let newCount = (postModel?.likeCount ?? 0) + 1
                self.postModel?.likeCount = newCount
                self.postModel?.iliked = true
                self.renderCell()
                //  self.delegete?.praiseOneNormalTopic("forecast", action: "like", value: true, id: postModel?.postId ?? "" )
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: true, id: postModel?.postId ?? "")
            }
            else  if self.postModel?.iliked == true {
                var newCount = (postModel?.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.postModel?.likeCount = newCount
                self.postModel?.iliked = false
                self.renderCell()
                //                self.delegete?.praiseOneNormalTopic("forecast", action: "like", value: false, id: postModel?.postId ?? "" )
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: false, id: postModel?.postId ?? "")
            }
        }
        
    }
    
    @objc func gotoAppStoreRate() {
        let urlString = "itms-apps://itunes.apple.com/app/id1184938845"
        let url = URL(string: urlString)
        UIApplication.shared.openURL(url!)
        AnaliticsManager.sendEvent(AnaliticsManager.rate_appstore_click, data: ["action":"click_rateAppstore_button"])
        setShowRateStarUserDefaults(showRateStar: true)
    }
    
    @objc func clickRateUSDetail() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: ShowRateUsPageNoti), object: self, userInfo: nil)
        setShowRateStarUserDefaults(showRateStar:true)
    }
    
    func setShowRateStarUserDefaults(showRateStar:Bool){
        UserDefaults.standard.setValue(showRateStar, forKey: "isAlreadyClick")
        UserDefaults.standard.synchronize()
    }
    
    @objc func treadBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if  self.postModel?.iDisliked == false{
                self.postModel?.iDisliked = true
                let newCount = (postModel?.dislikeCount ?? 0) + 1
                self.postModel?.dislikeCount = newCount
                self.renderCell()
                // self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: true, id: postModel?.postId ?? "" )
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: true, id: postModel?.postId ?? "")
            }
            else if  self.postModel?.iDisliked == true{
                self.postModel?.iDisliked = false
                var newCount = (postModel?.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.postModel?.dislikeCount = newCount
                self.renderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: false, id: postModel?.postId ?? "")
                //   self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: false, id: postModel?.postId ?? "" )
            }
        }
    }
    
    var zodiacImgAvaDic = ["aries":"ava_aries.png","taurus":"ava_taurus.png","gemini":"ava_gemini.png","cancer":"ava_cancer.png","leo":"ava_leo.png","virgo":"ava_virgo.png","libra":"ava_libra.png","scorpio":"ava_scorpio.png","sagittarius":"ava_sagittarius.png","capricorn":"ava_capricorn.png","aquarius":"ava_aquarius.png","pisces":"ava_pisces.png"]
    
    
    func renderCell() {
        if postModel?.iliked == true{
            likeButton.setImage(UIImage(named:"praise2_"), for: UIControlState())
        }else{
            likeButton.setImage(UIImage(named:"praise_"), for: UIControlState())
        }
        
        if postModel?.iDisliked == true{
            dislikeButton.setImage(UIImage(named:"Step-on2_"), for: UIControlState())
        }else{
            dislikeButton.setImage(UIImage(named:"Step-on_"), for: UIControlState())
        }
        
        if postModel?.likeCount == 0 {
            likeCountLabel.isHidden = true
        }else{
            likeCountLabel.isHidden = false
            likeCountLabel.text = String(postModel?.likeCount ?? 0)
        }
        
        if postModel?.dislikeCount == 0 {
            dislikeCountLabel.isHidden = true
        }else{
            dislikeCountLabel.isHidden = false
            dislikeCountLabel.text = String(postModel?.dislikeCount ?? 0)
        }
        
        if postModel?.commentCount == 0 {
            commentCountLabel.isHidden = true
        }else {
            commentCountLabel.isHidden = false
            commentCountLabel.text = String(postModel?.commentCount ?? 0)
        }
        
        let font = HSFont.baseLightFont(18)
        contentLab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: postModel?.content ?? "", font: font)
    }
    
    func renderCell(_ horo:String,needTssAlert:Bool) {
        nameLab.text = horo.capitalized
        dateLab.text = ZodiacModel.getZodicPeriodWithName(horo)
        if needTssAlert == true{
            rssBottomLabel.isHidden=false
            rssTopLabel.isHidden=false
        }else{
            rssTopLabel.isHidden=true
            rssBottomLabel.text = ""
            rssTopLabel.text=""
            rssBottomLabel.isHidden=true
        }
    }
    
    @objc func goFBShare() {
        let content = FBSDKShareLinkContent.init()
        content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_FORECAST + (self.postModel?.postId ?? ""))
        FBSDKShareDialog.show(from: self.rootVC, with: content, delegate: nil)
    }
    
    class func calculateHeight(_ content:String,needTssAlert:Bool) ->CGFloat{
        let font = HSFont.baseLightFont(18)
        if needTssAlert == true{
            return  HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + 80 + 40 + 67 + SCREEN_WIDTH / 16 * 9
        }else {return  HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + 80 + 40 + SCREEN_WIDTH / 16 * 9
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
