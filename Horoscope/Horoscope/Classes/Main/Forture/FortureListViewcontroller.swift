//
//  SecondViewController.swift
//  Horoscope
//
//  Created by Wang on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import IDMPhotoBrowser

class FortureListViewcontroller: UIViewController,YSLContainerViewControllerDelegate,eachCardDelegate,GuideNotificationViewDelegate,FortureDetailViewcontrollerDelegate{

    var imageHeaderView:partOneView?
    var horoScopeName:String?
    let cellLabelHeight = UIScreen.main.bounds.height-(HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()/15*8)-64+49
    var pageType : Int = 0
    var dateArr : [String] =
        [
            NSLocalizedString("Yesterday", comment: ""),
            NSLocalizedString("Today", comment:""),
            NSLocalizedString("Tomorrow", comment:""),
            NSLocalizedString("Weekly", comment:""),
            NSLocalizedString("Monthly",comment:""),
            NSLocalizedString("Yearly", comment:"")
        ]
    var canShowComment = false
    var postId  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor=UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha:1)
        addSegmentSectionHeader()
//        createTableHeaderAndSectionHeader()
        NotificationCenter.default.addObserver(self, selector: #selector(imageClick(_:)), name: NSNotification.Name(rawValue: "deviationClickBigImage"), object: nil)
        addBackBtn()
        self.navigationItem.title = NSLocalizedString("My Fortune", comment: "")
        self.addRightNavReportBtn()
  
    }
    
    @objc func imageClick(_ noti:Notification){
        let imaName = noti.userInfo?["url"] ?? ""
        let imgUrl = URL(string: (imaName) as! String)
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.present(brower!, animated: true, completion: nil)
    }


    func addRightNavReportBtn() {
        let reportBtn = UIButton(type:.custom)
        reportBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        reportBtn.setImage(UIImage(named: "report_"), for: UIControlState())
        reportBtn.isUserInteractionEnabled = true
        reportBtn.addTarget(self, action: #selector(self.rightNavReportBtnClick), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: reportBtn)
    }
    
    @objc func rightNavReportBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        ReportViewController.addacrion(BaseComment(),postId: self.postId, isPostReport: true,vc:self)
        }
    }
    
    func loadDataSuccess(_ postid: String) {
        self.postId = postid
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    @objc func popView() {
        var backCount:Int = UserDefaults.standard.integer(forKey: "backCount")
        backCount += 1
        UserDefaults.standard.set(backCount, forKey: "backCount")
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: Notification.Name(rawValue: HomeIntertitialAdShowNoti), object: self, userInfo: nil)
//需求要求去掉
//        let lastPopTimes = (UserDefaults.standard.integer(forKey: DailyPoPTimes))
//        let newPopTimes = lastPopTimes+1
//        UserDefaults.standard.set(newPopTimes, forKey: DailyPoPTimes)
//        UserDefaults.standard.synchronize()
//
//        if newPopTimes == 2 {
//            if wheatherShowNotif() == true {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: ShowGuideNotification), object: self)
//            }
//            else if wheatherShowRateUs() == true {
//                NotificationCenter.default.post(name: Notification.Name(rawValue: "forcastShowRateUsView"), object: nil)
//            }
//        }
//
//        if newPopTimes == 4 {
//            if wheatherShowNotif() == true {
//                if wheatherShowRateUs() == true {
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "forcastShowRateUsView"), object: nil)
//                }else{
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: ShowGuideNotification), object: self)
//                }
//            }else if wheatherShowNotif() == false {
//                if wheatherShowRateUs() == true {
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "forcastShowRateUsView"), object: nil)
//                }
//            }
//        }
//
//        if newPopTimes == 6 {
//            if wheatherShowNotif() == true  {
//                 NotificationCenter.default.post(name: Notification.Name(rawValue: ShowGuideNotification), object: self)
//            }else if wheatherShowRateUs() == true {
//                     NotificationCenter.default.post(name: Notification.Name(rawValue: "forcastShowRateUsView"), object: nil)
//                }
//        }
         _ = self.navigationController?.popViewController(animated: true)
     }
  

    func wheatherShowNotif() -> Bool {
        return NotificationLimitManager.sharedInstance.wheatherToShowGuideView()
    }
    func wheatherShowRateUs() -> Bool {
        return (UserDefaults.standard.bool(forKey: "hasRate") == false && UserDefaults.standard.integer(forKey: "rateUsShowCount") < RateUsFullView.getRateUsLimitShowCount())
    }
    
    //MARK:-GuideNotificationViewDelegate
    func guideNotificationViewTurnOnBtnClick(_ guideView: GuideNotificationView) {
         guideView.removeFromSuperview()
    }
    
    var segments : YSLContainerViewController?
    var heightArr:[CGFloat]?
    var header:UIView?
    func addSegmentSectionHeader(){
        if pageType == 0 {
        }else if pageType == 1{
            let vc1 = FortureDetailViewcontroller.create(self.horoScopeName, style: .off1)
            let vc2 = FortureDetailViewcontroller.create(self.horoScopeName, style: .today)
            let vc3 = FortureDetailViewcontroller.create(self.horoScopeName, style: .tomorrow)
            let vc4 = FortureDetailViewcontroller.create(self.horoScopeName, style: .weekly)
            let vc5 = FortureDetailViewcontroller.create(self.horoScopeName, style: .monthly)
            let vc6 = FortureDetailViewcontroller.create(self.horoScopeName, style: .yearly)
            
            vc1.fortune = "yesterday"
            vc2.fortune = "today"
            vc3.fortune = "tomorrow"
            vc4.fortune = "weekly"
            vc5.fortune = "monthly"
            vc6.fortune = "yearly"
            
            vc1.delegate=self
            vc2.delegate=self
            vc3.delegate=self
            vc4.delegate=self
            vc5.delegate=self
            vc6.delegate=self
            
            vc1.navBtndelegate=self
            vc2.navBtndelegate=self
            vc3.navBtndelegate=self
            vc4.navBtndelegate=self
            vc5.navBtndelegate=self
            vc6.navBtndelegate=self
            
            vc1.title = dateArr[0]
            vc2.title = dateArr[1]
            vc3.title = dateArr[2]
            vc4.title = dateArr[3]
            vc5.title = dateArr[4]
            vc6.title = dateArr[5]
            
            segments = YSLContainerViewController.init(controllers: [vc1,vc2,vc3,vc4,vc5,vc6], topBarHeight: 64,startheight: 0, parentViewController: self)
            segments?.delegate = self
            segments?.menuItemFont = HSFont.baseMediumFont(20)
            segments?.menuIndicatorColor=UIColor.luckyPurpleTitleColor()
            segments?.menuItemTitleColor=UIColor.luckyPurpleContentColor()
            segments?.menuBackGroudColor=UIColor.baseCardBackgroundColor()
            segments?.menuItemSelectedTitleColor=UIColor.luckyPurpleTitleColor()
            segments?.startIndex=currentIndex
            self.view.addSubview(segments!.view)
            
        }else if pageType == 2{
            
        }
    }
    
    var currentIndex:Int32 = 0
    func returnRowHeight(_ rowHeight: CGFloat, tag: Int) {
    }
    
    func createTableHeaderAndSectionHeader() {
        imageHeaderView = partOneView.init(frame: CGRect(x: 0, y: 0, width: HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth(), height: HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()/15*8))
        
        imageHeaderView?.backImageView.image = UIImage(named:"topBgImg.jpg")
      //  self.baseTableView?.tableHeaderView=imageHeaderView
        //daily_aries
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of an y resources that can be recreated.
    }
    
    
    let analDic:[Int:String] = [0:"yesterday",1:"today",2:"tomorrow",3:"weekly",4:"monthly",5:"yearly"]
    
    func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {
        //switch ad
        if let anastring = analDic[index]{
            AnaliticsManager.sendEvent(AnaliticsManager.forecast_show, data:["date":anastring])
        }
        
        guard let count = segments?.childControllers.count else {
            return
        }
        let currentVC = controller as! FortureDetailViewcontroller
        
        for index  in 0..<count {
            let controller  = segments?.childControllers[index] as! FortureDetailViewcontroller
            if controller.isEqual(currentVC) {
                controller.isCurrentController = true
                controller.getData()
            }else{
                controller.isCurrentController = false
                controller.removeNoti()
            }
        }
        
        self.currentIndex=Int32(index)
        controller.viewWillAppear(true)
    }
    
    class func create(_ name:String) ->FortureListViewcontroller{
        let vc = FortureListViewcontroller()
        vc.horoScopeName=name
        return vc
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("销毁")
    }

    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
}
