//
//  CommentCellTableViewCell.swift
//  CommentDemo
//
//  Created by xiao  on 17/2/7.
//  Copyright © 2017年 xiao . All rights reserved.
//

import UIKit


class CommentCellTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.snp.edges)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
