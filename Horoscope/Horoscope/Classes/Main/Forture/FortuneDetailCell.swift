//
//  FortuneBannerCell.swift
//  Horoscope
//
//  Created by dingming on 2018/4/12.
//  Copyright © 2018年 meevii. All rights reserved.
//

import UIKit

class FortuneDetailCell: HoroDescriptionCell, InnerAdContainerdelegate {
    let view = InnerAdContainer()
    var adloadSuccessed = false
    var fortune:String = ""
    
    override func createUI() {
        super.createUI()
        self.createAdView()
    }
    
    func createAdView() {
        view.delegate = self
        let adInfo = SingleAdInfo()
        adInfo.placementKey = AdPlacementKey.MyFortuneNative_003.rawValue
        view.requestAd(self.rootVC, adInfo: adInfo, isStepLoad: false)
        let height = UIScreen.main.bounds.size.width/1.91 + 54
        view.frame = CGRect(x:0, y: 0, width: UIScreen.main.bounds.width, height:height)
        self.addSubview(view)

    }
    
    //MARK: InnerAdContainerdelegate
    func requestAdSuccess(_ size: CGSize, adView: InnerAdContainer) {
        self.adloadSuccessed = true
        
        if ((self.rootVC?.parent) != nil) {
            if self.view.adInfo?.placementKey == AdPlacementKey.MyFortuneNative_003.rawValue{
                if self.view.adInfo?.adUnitPlatform == AdOrigin.Admob.rawValue {
                    AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a1_ad_show":"admob"])
                }
                else if self.view.adInfo?.adUnitPlatform == AdOrigin.Fb.rawValue{
                    AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a1_ad_show":"facebook"])
                }
            }
        }
    }
}
