//
//  HoroscopeView.swift
//  Horoscope
//
//  Created by Wang on 16/12/5.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
protocol HoroscopeViewDelegate:class {
    func clickHoroscope(_ name:String)
}

class HoroscopeView: UIView {
    
    //push Viewcontroller type
    static let Daily:Int = 1
    static let Forture:Int = 0
    static let Character:Int = 2
    
    var imgView:UIImageView?
    var titleLabel:UILabel?
    var periodLabel:UILabel?
    var colorView:UIView = UIView()
    weak var rootVc:UIViewController?
    weak var delegate:HoroscopeViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.white
        imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.width))
        self.addSubview(imgView!)
        self.colorView = UIView(frame: CGRect(x: 0, y: imgView!.bounds.maxY, width: self.bounds.width, height: self.bounds.height))
        self.colorView.backgroundColor = UIColor.cardBackColor()
        
        titleLabel = UILabel(frame: CGRect(x: 8, y: 4, width: self.bounds.width-16, height: self.bounds.height/8))
        titleLabel?.font = UIFont(name: HSHelpCenter.baseContentFontNameRegular, size: 20)
        titleLabel?.textColor = UIColor.hsTextColor(1)
        titleLabel?.textAlignment = NSTextAlignment.left
        self.colorView.addSubview(titleLabel!)
        
        periodLabel = UILabel(frame: CGRect(x: 8, y: titleLabel!.bounds.maxY+1, width: self.bounds.width-16, height: 20))
        periodLabel?.textColor = UIColor.hsTextColor(1)
        periodLabel?.font = UIFont(name: HSHelpCenter.baseContentFontName, size: 15)
        periodLabel?.textAlignment = NSTextAlignment.left
        self.colorView.addSubview(periodLabel!)
        self.addSubview(self.colorView)
        self.addPushAction()
     }
    
     func addPushAction(){
        let tap=UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(self.onSelectedHoroscope))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled=true
     }
    
     @objc func onSelectedHoroscope() {
        if let model = self.model{
          if model.zodiacName.isEmpty == false{
          self.delegate?.clickHoroscope(model.zodiacName.lowercased())
        }
      }
     }
    
     var model:ZodiacCellModel?
     func renderHoroView(_ model:ZodiacCellModel) {
        self.model=model
        self.titleLabel!.text = model.zodiacName
        self.periodLabel!.text = model.periodString
        self.bringSubview(toFront: self.periodLabel!)
        self.imgView?.image = UIImage(named: model.imageName)
     }
    
     class var height:CGFloat{
         return (UIScreen.main.bounds.width-30)/2+(UIScreen.main.bounds.width-30)/6
     }
    
     required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
     }

}
