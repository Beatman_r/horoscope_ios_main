//
//  CommentViewController.swift
//  Horoscope
//
//  Created by xiao  on 17/2/8.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD
import MJRefresh
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

class CommentViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,CommentFootBtnDelegate, PostViewControllerDelegate{
    
    var tableView:UITableView?
    var commentArray = [MainComment]()
    let commentFootBtn = CommentFootBtn.init(frame: CGRect.zero)
    var post:ReadPostModel?
    var postId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFoldComents(_:)), name: NSNotification.Name(rawValue: MoreCommentBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.CommentViewClickToShowCommentReplyView(_:)), name: NSNotification.Name(rawValue: CommentViewClickToShowReplyView), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.replyBtnClick(_:)), name: NSNotification.Name(rawValue: ReplyBtnClickToShowReplyController), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reportBtnClickToShowReportController), name: NSNotification.Name(rawValue: ReportBtnClickToShowReportController), object: nil)
        createCommentFootBtn()
        self.getJson()
        self.setupRefreshFooterView()
    }
    @objc func replyBtnClick(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let replyVC = PostViewController()
        replyVC.isReply = true
        replyVC.model = model as? BaseComment
        replyVC.postModel = self.post
        replyVC.delegate = self
        self.navigationController?.pushViewController(replyVC, animated: true)
    }
    
    func getJson() {
            HoroscopeDAO.sharedInstance.getTopic(self.postId ?? "58a40762abfcef000119ce68", success: { (post) in
                self.post = post
                guard let commentList = post.commentList else{
                    return
                }
                self.commentArray = commentList
                self.tableView?.reloadData()
            }) { (failure) in
        }
    }

    func setupRefreshFooterView() {
        let footer = MJRefreshBackNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreList))
        footer?.setTitle(NSLocalizedString("Pull up to load", comment: ""), for: .idle)
        footer?.setTitle(NSLocalizedString("Release to refresh", comment: ""), for: .pulling)
        footer?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        self.tableView?.mj_footer = footer
    }
    
    @objc func loadMoreList() {
        let offset  = String(self.commentArray.count)
        HoroscopeDAO.sharedInstance.getMoreComment(self.post?.postId ?? "", size: "20", offset: offset, sort: "new", success: { (commentList) in
            if commentList.count == 0 {
                HUD.flash(.label("No More Comments"),delay: 1)
                self.tableView?.mj_footer.endRefreshing()
            }else{
            self.commentArray  = self.commentArray + commentList
                self.tableView?.reloadData()
            self.tableView?.mj_footer.endRefreshing()
            }
        }) { (failure) in
                _ = self.tableView?.mj_footer
        }
    }
    
    func createCommentFootBtn() {
        self.view.addSubview(commentFootBtn)
        commentFootBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.height.equalTo(49)
        }
        commentFootBtn.delegate = self
    }
   
    // MARK: CommentFootBtn delegate
    func commentFootBtnClick() {
        let replyVC = PostViewController()
        replyVC.isReply = true
        self.navigationController?.pushViewController(replyVC, animated: true)
    }
    
    func getData()  {
        guard let filePath = Bundle.main.path(forResource: "commentTestJson", ofType: "json") else {
            return
        }
        guard  let jsonData = try? Data(contentsOf: URL(fileURLWithPath: filePath )) else{
            return
        }
        let json = JSON(data: jsonData)
        
        guard let commentListjsonArray = json["commentList"].array else {
            return
        }
        
        var commentList :[MainComment] = [MainComment]()
        for json  in commentListjsonArray {
            guard  let commentListDic = json["comment_id_map"].dictionary else{
                return
            }
            guard let commentStructure = json["commentStructure"].array?.first else {
                return
            }
            
            let mainComment = MainComment()
            mainComment.parse(commentStructure)
            
            if mainComment.parentCommentIdList != nil {
                for item  in commentListDic {
                    if mainComment.parentCommentIdList!.contains(item.0) {
                        let parentComment = ParentComment()
                        parentComment.parse(item.1)
                        mainComment.parentCommentList.append(parentComment)
                    }else if mainComment.commentId == item.0{
                        let comment = BaseComment()
                        comment.parse(item.1)
         mainComment.likeCount = comment.likeCount
         mainComment.dislikeCount = comment.dislikeCount
         mainComment.createTime = comment.createTime
         mainComment.content = comment.content
         mainComment.imageList = comment.imageList
         mainComment.userInfo = comment.userInfo
         mainComment.pos = comment.pos
         mainComment.createTimeHuman = comment.createTimeHuman
                    }
                }
                mainComment.parentCommentList.sort(by: { (item1, item2) -> Bool in
                 return item1.pos < item2.pos
                })
                commentList.append(mainComment)
            }
        }
        
      self.commentArray = commentList
    }
    
    @objc func getFoldComents(_ noti:Notification) {
        guard let mainComment = noti.userInfo?["mainCommet"] else {
            return
        }
        guard let foldIndex = (mainComment as! MainComment ).parentCommentWarp?["index"] else {
            return
        }
        
        guard let negative_index = (mainComment as! MainComment ).parentCommentWarp?["negative_index"] else {
            return
        }

        guard let commentId = (mainComment as! MainComment ).commentId else {
            return
        }

        HoroscopeDAO.sharedInstance.getFoldCommentList(commentId, index: (foldIndex as! Int), negative_index: (negative_index as! Int), success: { (foldCommentList) in
            
            var needInsertCmt : MainComment?
            var indexpath : IndexPath = IndexPath(row: 0, section: 0)
            for  index  in 0..<self.commentArray.count {
                let mainCmt = self.commentArray[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
                    indexpath = IndexPath(row: index, section: 0)
                }
            }
            needInsertCmt?.parentCommentWarp = nil
            
            guard let  needInsertCmtSubCmtCount = needInsertCmt?.parentCommentList.count else {
                return
            }
            for index in 0..<needInsertCmtSubCmtCount {
                if index == (foldIndex as! Int ) {
                    for index in 0..<foldCommentList.count {
                        needInsertCmt?.parentCommentList.insert(foldCommentList[index], at: index+2)
                    }
                }
            }
            self.tableView?.reloadRows(at: [indexpath], with: .automatic)
            }) { (failure) in
                LXSWLog(failure)
        }
    }
    
    func setUpUI() {
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-64-49-44), style: .plain)
        tableView?.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView?.backgroundView = nil
        tableView?.tableFooterView = UIView(frame: CGRect.zero)
        tableView?.delegate = self
        tableView?.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0)
        tableView?.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        tableView?.dataSource = self
        tableView?.register(CommentCellTableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView!)
    }
    
    //MARK:- UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let container = CommentLayoutContainer(model: self.commentArray[indexPath.row],isMyNotification:false)
        cell.contentView.addSubview(container)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let container = CommentLayoutContainer.init(model: self.commentArray[indexPath.row],isMyNotification:false)
//        return  container.frame.size.height + (container.parentCommentWarp != nil ? 40 : 0)
        return  container.frame.size.height
    }
    
    //MARK:- CommentViewCellDelegate
    @objc func CommentViewClickToShowCommentReplyView(_ noti: Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let replyView = ReplyCommentView(frame: self.view.bounds, model: model as! BaseComment)
        self.view.addSubview(replyView)
    }
    
    @objc func reportBtnClickToShowReportController() {
        let actionVC = UIAlertController(title: "Report Submitted", message: "", preferredStyle: .actionSheet)
        let PornographicContentAction = UIAlertAction(title: "Pornographic Content", style: .default) { (action) in
            HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(PornographicContentAction)
        let GraphicViolenceAction = UIAlertAction(title: "Graphic Violence", style: .default) { (action) in
             HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(GraphicViolenceAction)
        let Harassment = UIAlertAction(title: "Harassment", style: .default) { (action) in
            HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(Harassment)
        let HatefulorAbusiveContentAction = UIAlertAction(title: "Hateful or Abusive Content", style: .default) { (action) in
            HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(HatefulorAbusiveContentAction)
        let InfringementAction = UIAlertAction(title: "Infringement", style: .default) { (action) in
            HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(InfringementAction)
        let Fraud = UIAlertAction(title: "Fraud", style: .default) { (action) in
            HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(Fraud)
        
        let Spam = UIAlertAction(title: "Spam", style: .default) { (action) in
            HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(Spam)
        let Others = UIAlertAction(title: "Others", style: .default) { (action) in
            HUD.flash(.label("We'll handle your report as quickly as possible. Thanks!"),delay: 1)
        }
        actionVC.addAction(Others)
        let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            actionVC.addAction(Cancel)
        self.present(actionVC, animated: true, completion: nil)
    }
    
    //MARK:- PostViewControllerDelegate
    func postCommentFailure(_ error: NSError) {
        HUD.flash(.label("Public failure"),delay: 1)
    }
    
    func postCommentSuccess(_ comment: MainComment) {
        self.commentArray.insert(comment, at: 0)
        self.tableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
