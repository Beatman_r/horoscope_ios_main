//
//  DatePickSectionHeaderView.swift
//  Horoscope
//
//  Created by Wang on 16/11/25.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

protocol DatePickSectionHeaderViewDelegate:class {
    func changeSelectedDate(_ tag:Int)
}

class DatePickSectionHeaderView: UIView,UIScrollViewDelegate {
    weak var delegate:DatePickSectionHeaderViewDelegate?
    
    init(frame: CGRect,arr:[String]) {
        super.init(frame: frame)
        self.titleNameArr=arr
        self.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.6)
        self.calculateScrollViewWidth()
        self.createScrollView()
        self.createThreeButtons()
        self.createMovingLine()
    }
    
    fileprivate func createThreeButtons(){
        let buttonYPosition:CGFloat = 0
        let buttonHeight:CGFloat = 44
        for index in 0..<self.titleNameArr!.count{
            let buttonWidth:CGFloat = eachBtnWidthArr[index]
            let buttonXposition = self.sumWidthBeforeIndex(index)
            let button=UIButton(type:.custom)
            button.frame=CGRect(x: buttonXposition,y: buttonYPosition, width: buttonWidth, height: buttonHeight)
            button.backgroundColor = UIColor.white
            button.setTitle("\(self.titleNameArr![index])", for: UIControlState())
            button.titleLabel?.font=UIFont(name: HSHelpCenter.charterRegulatFontName,size: 18)
            button.backgroundColor = UIColor.clear
            if index == 0{
                button.setTitleColor(UIColor.hsTextColor(1), for: UIControlState())
            }else{
                button.setTitleColor(UIColor.hsTextColor(2), for: UIControlState())
            }
            button.tag = 200 + index
            button.addTarget(self, action: #selector(self.buttonClick(_:)), for: .touchUpInside)
            self.scrollView!.addSubview(button)
        }
    }
    
    @objc func buttonClick(_ sender:UIButton) {
        let paramNum:CGFloat = CGFloat(sender.tag - 200)
        self.delegate?.changeSelectedDate(sender.tag - 200)
        lineMove(paramNum)
        changeTitleColor(sender.tag)
    }
    
    func lineMove(_ tag:CGFloat) {
        let btnWidth = self.eachBtnWidthArr[Int(tag)]
        let xPosition = self.sumWidthBeforeIndex(Int(tag))
        UIView.animate(withDuration: 0.2, animations: {
            self.movingLine!.frame=CGRect(x: xPosition, y: 42, width: btnWidth, height: 2)
        })
        
    }
    
    func changeTitleColor(_ tag:Int)  {
        for index in 0..<self.titleNameArr!.count {
            let eachBtn = scrollView?.viewWithTag(index+200) as!UIButton
            if index == tag-200{
                eachBtn.setTitleColor(UIColor.hsTextColor(1), for: UIControlState())
            }else{
                eachBtn.setTitleColor(UIColor.hsTextColor(2), for: UIControlState())
            }
        }
    }
    
    func sumWidthBeforeIndex(_ index:Int) ->CGFloat {
        if index == 0{
            return 0
        }else{
            var width:CGFloat = 0
            for index in 0...index-1{
                width += self.eachBtnWidthArr[index]
            }
            return width
        }
    }
    
    var movingLine:UIView?
    func createMovingLine(){
        movingLine=UIView()
        movingLine?.backgroundColor = UIColor.hsTextColor(1)
        movingLine?.frame = CGRect(x: 0, y: 42,width: self.eachBtnWidthArr[0], height: 2)
        self.scrollView?.addSubview(movingLine!)
        self.scrollView?.bringSubview(toFront: movingLine!)
    }
    
    var titleNameArr:[String]?
    fileprivate var totalScrollWidth:CGFloat=0
    fileprivate var eachBtnWidthArr:[CGFloat]=[]
    
    func calculateScrollViewWidth(){
        var totalWidth:CGFloat = 0
        for eachString in self.titleNameArr ?? [""] {
            let eachWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth(eachString, font: 18, height: 44)+5
            self.eachBtnWidthArr.append(eachWidth)
            totalWidth += eachWidth
        }
        totalScrollWidth=totalWidth
    }
    
    let screenWidth  = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()
    let screenHeight = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenHeight()
    
    var scrollView:UIScrollView?
    func createScrollView() {
        scrollView=UIScrollView()
        scrollView?.delegate=self
        scrollView?.frame=CGRect(x: 0,y: 0, width: screenWidth, height: 44)
        scrollView?.backgroundColor =  UIColor.clear
        scrollView?.contentSize=CGSize(width: totalScrollWidth, height: 44) // 内容大小
        scrollView?.showsVerticalScrollIndicator=false
        scrollView?.showsHorizontalScrollIndicator=false
        scrollView?.bounces=false
        scrollView?.bouncesZoom=false
        self.addSubview(scrollView!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func calculateHeight() ->CGFloat{
        return 44
    }
    
}
