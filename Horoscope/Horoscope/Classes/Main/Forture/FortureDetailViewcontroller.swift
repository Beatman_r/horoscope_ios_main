//
//  thirdViewController.swift
//  Horoscope
//
//  Created by Wang on 16/11/25.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import PKHUD
import MJRefresh
import SwiftyJSON

enum FortuneType:String {
    case Today = "today"
    case Yesterday = "yesterday"
    case Tomorrow = "tomorrow"
    case Weekly = "weekly"
    case Monthly = "monthly"
    case Yearly = "yearly"
}

class eachCardViewmodel:NSObject{
    init(name:String,dataStyle:HoroDetailType) {
        super.init()
        self.horoName=name
        self.style=dataStyle
    }
    var style:HoroDetailType?
    var horoName:String?
    var model:ZodiacModel?
    fileprivate let dao = HoroscopeDAO()

    func loadDetail(_ type:HoroDetailType,success:@escaping () ->(),failure:@escaping (_ error:NSError) -> ()) {
        switch  type {
        case .weekly:
            dao.getWeeklyHoroscope(self.horoName ?? "", dateOffset: 0, success: { (model) in
                self.model=model
                success()
                }, failure: { (error) in
                    failure(error)
            })
        case .monthly:
            dao.getMonthlyHoroscopeUrl(self.horoName ?? "", success: { (model) in
                self.model=model
                success()
                }, failure: { (error) in
                    failure(error)
            })
        case .yearly:
            dao.getYearlyHoroscopeUrl(self.horoName ?? "", success: { (model) in
                self.model=model
                success()
                }, failure: { (error) in
                   failure(error)
            })
        case .tomorrow:
            dao.getDailyHoroscope(self.horoName ?? "", dateOffset: 1, success: { (model) in
                self.model = model
                 success()
                }, failure: { (error) in
                    failure(error)
            })
        case.today:
            dao.getDailyHoroscope(self.horoName ?? "", dateOffset: 0, success: { (model) in
                self.model = model
                
                success()
                }, failure: { (error) in
                    failure(error)
            })
        case .off1:
            dao.getDailyHoroscope(self.horoName ?? "", dateOffset: -1, success: { (model) in
                self.model = model
                success()
                }, failure: { (error) in
                    failure(error)
            })
        }
    }
}

protocol eachCardDelegate :class{
    func returnRowHeight(_ rowHeight:CGFloat,tag:Int)
}

protocol FortureDetailViewcontrollerDelegate:class{
    func loadDataSuccess(_ postid:String)
}


class FortureDetailViewcontroller: UIViewController,UITableViewDelegate,UITableViewDataSource, PostViewControllerDelegate,CommentFootBtnDelegate,NormalPostCellDelegate{
    weak var delegate:eachCardDelegate?
    var viewModel:eachCardViewmodel?
    var indiacator = UIActivityIndicatorView()
    var retryImg = UIImageView()
    var oopsLabel = UILabel()
    var retryLabel = UILabel()
    var refreshButton = UIButton()
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    var rowNum = 0
    var hasAd:Bool? = false
    var post:ReadPostModel?
    
    @objc var postId:String?
    var fortune:String = ""
    var commentList = [MainComment]()
    var hotComentList = [MainComment]()
    let commentFootBtn = CommentFootBtn.init(frame: CGRect.zero)
    var baseTableView:UITableView?
    var fortuneDetailCell: FortuneDetailCell?
    var isCurrentController = false
    weak var navBtndelegate:FortureDetailViewcontrollerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         createTableView()
         refreshNeedRssStatus()
         initViewmodel()
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        self.setupRefreshFooterView()
        self.createCommentView()
        self.registerCell()
    }
    
    func createTableView() {
        let frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height-64-44)
        self.baseTableView = UITableView(frame:frame, style: .grouped)
        self.view.addSubview(self.baseTableView!)
        self.baseTableView?.delegate = self
        self.baseTableView?.dataSource = self
        self.baseTableView?.separatorStyle = .none
        self.baseTableView?.backgroundColor = UIColor.clear
        self.baseTableView?.showsVerticalScrollIndicator = false
    }
    
   
    func createCommentView() {
          let backview = UIView()
        self.view.addSubview(backview)
        backview.backgroundColor = UIColor.commentFootBtnBGColor()
        backview.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-44)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.height.equalTo(44)
        }
        backview.addSubview(commentFootBtn)
        commentFootBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(backview)
            make.left.equalTo(backview).offset(20)
            make.right.equalTo(backview).offset(-20)
            make.top.equalTo(backview)
        }
        commentFootBtn.delegate = self
        commentFootBtn.isHidden = true
    }

    func setupRefreshFooterView() {
        let footer = MJRefreshBackNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreList))
        footer?.setTitle(NSLocalizedString("Pull up to load", comment: ""), for: .idle)
        footer?.setTitle(NSLocalizedString("Release to refresh", comment: ""), for: .pulling)
        footer?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        self.baseTableView?.mj_footer = footer
    }
    
    @objc func loadMoreList() {
        let offset  = String(self.commentList.count)
        HoroscopeDAO.sharedInstance.getMoreComment(self.post?.postId ?? "", size: "20", offset: offset, sort: "new", success: { (commentList) in
            if commentList.count == 0 {
                HUD.flash(.label("No More Comments"),delay: 1)
                self.baseTableView?.mj_footer.endRefreshing()
            }else{
                self.commentList  = self.commentList + commentList
                self.baseTableView?.reloadData()
                self.baseTableView?.mj_footer.endRefreshing()
            }
        }) { (failure) in
            self.baseTableView?.mj_footer.endRefreshing()
        }
    }
    
    @objc func commentsMoreThanSeventyFloor() {
        HUD.flash(.label("Too more comments"),delay: 1)
    }
    
    
    func addNoti() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFoldComents(_:)), name: NSNotification.Name(rawValue: MoreCommentBtnClick), object: nil)
        NotificationCenter.default.addObserver(self
            , selector: #selector(self.CommentViewClickToShowCommentReplyView(_:)), name: NSNotification.Name(rawValue: CommentViewClickToShowReplyView), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.replyBtnClick(_:)), name: NSNotification.Name(rawValue: ReplyBtnClickToShowReplyController), object: nil)
        NotificationCenter.default.addObserver(self
            , selector: #selector(self.reportBtnClickToShowReportController(_:)), name: NSNotification.Name(rawValue: ReportBtnClickToShowReportController), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.commentsMoreThanSeventyFloor), name: NSNotification.Name(rawValue: CommentsMoreThanSeventyFloor), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.likeBtnClick(_:)), name: NSNotification.Name(rawValue: LikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.disLikeBtnClick(_:)), name: NSNotification.Name(rawValue: DisLikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userClickToUserTopic(_:)), name: NSNotification.Name(rawValue: UserClickToUserTopic), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userClickRateUS(_:)), name: NSNotification.Name(rawValue: ShowRateUsPageNoti), object: nil)
    }
    
    @objc func userClickToUserTopic(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! BaseComment)
        let userVC  = ReadOtherUserPostViewController()
        userVC.userId = mod.userInfo?.userId ?? ""
        userVC.userName = mod.userInfo?.name ?? ""
        self.navigationController?.pushViewController(userVC, animated: true)
    }
    
    @objc func userClickRateUS(_ noti:Notification) {
        let rateDetailVC  = RateDetailViewController()
        rateDetailVC.starRank = 203
        self.navigationController?.pushViewController(rateDetailVC, animated: true)
        AnaliticsManager.sendEvent(AnaliticsManager.advice_rateUs_click, data: ["action":"click_rateUs_button"])
    }
    
    @objc func likeBtnClick(_ noti:Notification) {
        
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! ParentComment)
        for main in self.commentList {
            if main.commentId == mod.commentId {
              changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
        
        for main in self.hotComentList {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
    }
 }
    
    @objc func disLikeBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! ParentComment)
        for main in self.commentList {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
        
        for main in self.hotComentList {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
      }
    }
    func changeModelLikeOrDisLikeState(_ model:BaseComment,mod:ParentComment){
        model.likeCount = mod.likeCount
        model.dislikeCount = mod.dislikeCount
        model.isDisliked = mod.isDisliked
        model.isLiked = mod.isLiked

    }
    
    func removeNoti()  {
        NotificationCenter.default.removeObserver(self)
    }
  
    func getData() {
        if self.isCurrentController == true {
        self.addNoti()
        }else{
        self.removeNoti()
        }
        let date  = Date()
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let str = formatter.string(from: date)
        if self.fortune == FortuneType.Today.rawValue {
            HoroscopeDAO.sharedInstance.getMyTodayFortune(str, horoscopeName: self.horoname ?? "", success: { (post) in
                
                self.post = post
                self.post?.horo = self.horoname
                self.post?.dateDura = ZodiacModel.getZodicPeriodWithName(self.horoname ?? "")
                self.commentList = post.commentList ?? [MainComment]()
                self.hotComentList = post.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(post.postId ?? "")
                }, failure: { (error) in
                    
            })
        }else if self.fortune == FortuneType.Yesterday.rawValue{
            HoroscopeDAO.sharedInstance.getMyYestodayFortune(str, horoscopeName: self.horoname ?? "", success: { (post) in
                 self.post = post
                self.commentList = post.commentList ?? [MainComment]()
                self.hotComentList = post.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(post.postId ?? "")
                }, failure: { (error) in
                    
            })
        }else if self.fortune == FortuneType.Tomorrow.rawValue{
            HoroscopeDAO.sharedInstance.getMyTomorrowFortune(str, horoscopeName: self.horoname ?? "", success: { (post) in
                 self.post = post
                self.commentList = post.commentList ?? [MainComment]()
                self.hotComentList = post.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(post.postId ?? "")
                }, failure: { (error) in
                    
            })
        }else if self.fortune == FortuneType.Weekly.rawValue{
            HoroscopeDAO.sharedInstance.getMyWeeklyFortune(str, horoscopeName: self.horoname ?? "", success: { (post) in
                 self.post = post
                self.commentList = post.commentList ?? [MainComment]()
                self.hotComentList = post.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(post.postId ?? "")
                }, failure: { (error) in
                    
            })
        }else if self.fortune == FortuneType.Monthly.rawValue{
            HoroscopeDAO.sharedInstance.getMyMonthlyFortune(str, horoscopeName: self.horoname ?? "", success: { (post) in
                 self.post = post
                self.commentList = post.commentList ?? [MainComment]()
                self.hotComentList = post.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(post.postId ?? "")
                }, failure: { (error) in
                    
            })
        }else if self.fortune == FortuneType.Yearly.rawValue{
            HoroscopeDAO.sharedInstance.getMyYearlyFortune(str, horoscopeName: self.horoname ?? "",success: { (post) in
                 self.post = post
                self.commentList = post.commentList ?? [MainComment]()
                self.hotComentList = post.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(post.postId ?? "")
                }, failure: { (error) in
                    
            })
        }
    }
    
    func setupRetryUI() {
        self.view.addSubview(self.retryImg)
        self.view.addSubview(self.oopsLabel)
        self.view.addSubview(self.retryLabel)
        self.view.addSubview(self.refreshButton)
        self.retryImg.image = UIImage(named: "ic_retry_")
        self.retryImg.isHidden = true
        
        self.oopsLabel.text = "Oops!"
        self.oopsLabel.font = HSFont.baseLightFont(18)
        self.oopsLabel.textColor = UIColor.luckyPurpleContentColor()
        self.oopsLabel.textAlignment = NSTextAlignment.center
        self.oopsLabel.isHidden = true
       
        self.retryLabel.text = "The data was lost. Click to retry."
        self.retryLabel.font = HSFont.baseLightFont(18)
        self.retryLabel.textColor = UIColor.luckyPurpleContentColor()
        self.retryLabel.textAlignment = NSTextAlignment.center
        self.retryLabel.numberOfLines = 1
        self.retryLabel.isHidden = true
        
        self.refreshButton.setTitle("Retry", for: UIControlState())
        self.refreshButton.layer.cornerRadius = 18
        self.refreshButton.layer.masksToBounds = true
        self.refreshButton.layer.borderWidth = 1
        self.refreshButton.layer.borderColor = UIColor.commonPinkColor().cgColor
        self.refreshButton.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        self.refreshButton.addTarget(self, action: #selector(reloadSegmentContent), for: .touchUpInside)
        self.refreshButton.isHidden = true
        
        //make constraint
        self.retryImg.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX).offset(-5)
            make.centerY.equalTo(self.view.snp.centerY).offset(-self.screenHeight * 100/667)
            make.height.equalTo(self.screenHeight * 107/667)
            make.width.equalTo(self.screenWidth * 140/375)
        }
        self.oopsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.retryImg.snp.bottom).offset(24)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.view.snp.width).offset(-10)
            make.bottom.equalTo(self.oopsLabel.snp.top).offset(22)
        }
        self.retryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.oopsLabel.snp.bottom).offset(5)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.view.snp.width).offset(-10)
            make.bottom.equalTo(self.retryLabel.snp.top).offset(22)
        }
        self.refreshButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.retryLabel.snp.bottom).offset(24)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.equalTo(self.screenHeight * 36/667)
            make.width.equalTo(self.screenWidth * 120/375)
        }
    }
    
    @objc func getFoldComents(_ noti:Notification) {

        guard let mainComment = noti.userInfo?["mainCommet"] else {
            return
        }
        guard let foldIndex = (mainComment as! MainComment ).parentCommentWarp?["index"] else {
            return
        }
        
        guard let negative_index = (mainComment as! MainComment ).parentCommentWarp?["negative_index"] else {
            return
        }
        
        guard let commentId = (mainComment as! MainComment ).commentId else {
            return
        }
        
        HoroscopeDAO.sharedInstance.getFoldCommentList(commentId, index: (foldIndex as! Int), negative_index: (negative_index as! Int), success: { (foldCommentList) in
            
            var needInsertCmt : MainComment?
               for  index  in 0..<self.hotComentList.count {
                let mainCmt = self.hotComentList[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
               }
            }
            
            for  index  in 0..<self.commentList.count {
                let mainCmt = self.commentList[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
              }
            }
            
            needInsertCmt?.parentCommentWarp = nil
            guard let  needInsertCmtSubCmtCount = needInsertCmt?.parentCommentList.count else {
                return
            }
            for index in 0..<needInsertCmtSubCmtCount {
                if index == (foldIndex as! Int ) {
                    for index in 0..<foldCommentList.count {
                        needInsertCmt?.parentCommentList.insert(foldCommentList[index], at: index+2)
                    }
                }
            }
            self.baseTableView?.reloadData()
        }) { (failure) in
            LXSWLog(failure)
        }
    }
    
    //MARK:- CommentViewCellDelegate
    @objc func CommentViewClickToShowCommentReplyView(_ noti: Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
       let replyView = ReplyCommentView(frame: self.view.bounds, model: model as! BaseComment)
        self.view.addSubview(replyView)
    }
    
    @objc func replyBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            guard let model = noti.userInfo?["model"] else {
                return
            }
            let replyVC = PostViewController()
            replyVC.isReply = true
            replyVC.isForcastReply = true
            replyVC.horoscopeName = self.horoname
            replyVC.forecastName = self.fortune
            replyVC.model = model as? BaseComment
            let replyModel = MyPostModel.init(jsonData: JSON.null, num: 0)
            replyVC.replyModel = replyModel
            replyModel.postId = (model as AnyObject).postId
            replyVC.delegate = self
            self.navigationController?.pushViewController(replyVC, animated: true)
        }
    }
    
    @objc func reloadSegmentContent() {
        self.indiacator.startAnimating()
        self.retryImg.isHidden = true
        self.oopsLabel.isHidden = true
        self.retryLabel.isHidden = true
        self.refreshButton.isHidden = true
        viewModel?.loadDetail(self.style!, success: {
            self.rowNum = 1
            self.indiacator.stopAnimating()
            
            self.baseTableView?.reloadData()
            }, failure: { (error) in
                self.rowNum = 0
                self.indiacator.stopAnimating()
                self.setupRetryUI()
                self.retryImg.isHidden = false
                self.oopsLabel.isHidden = false
                self.retryLabel.isHidden = false
                self.refreshButton.isHidden = false
        })
    }
    
    var adHeight:CGFloat = 0
    var horoname:String?
    var style:HoroDetailType?
    
    func initViewmodel()  {
        self.indiacator.frame = CGRect(x: screenWidth/2-50, y: screenHeight/5, width: 100, height: 100)
        self.indiacator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        self.indiacator.color = UIColor.black
        self.indiacator.hidesWhenStopped = true
        self.view.addSubview(self.indiacator)
        self.indiacator.startAnimating()
       
        viewModel = eachCardViewmodel.init(name: self.horoname ?? "",
                                           dataStyle: self.style ?? .weekly)
        viewModel?.loadDetail(self.style ?? .weekly, success: {
            self.retryImg.isHidden = true
            self.oopsLabel.isHidden = true
            self.retryLabel.isHidden = true
            self.refreshButton.isHidden = true
            self.indiacator.stopAnimating()
            self.rowNum = 1
            self.baseTableView?.reloadData()
            }, failure: { (error) in
                self.indiacator.stopAnimating()
                self.rowNum = 0
                self.setupRetryUI()
                self.retryImg.isHidden = false
                self.oopsLabel.isHidden = false
                self.retryLabel.isHidden = false
                self.refreshButton.isHidden = false
        })
    }
    
    var needRss:Bool=false
 
    func refreshNeedRssStatus() {
        if let s = self.style{
            switch s {
            case .off1:
                self.needRss=true
            case .today:
                self.needRss=true
            case .tomorrow:
                self.needRss=true
            case .weekly:
                self.needRss=false
            case .monthly:
                self.needRss=false
            case .yearly:
                self.needRss=false
            }
        }
    }
    let typeArr = [HoroDetailType.weekly,
                   HoroDetailType.monthly,
                   HoroDetailType.off1,
                   HoroDetailType.yearly,
                   HoroDetailType.today,
                   HoroDetailType.tomorrow]
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard self.fortuneDetailCell != nil else {
            return
        }
        
        if (self.fortuneDetailCell?.adloadSuccessed)! {
            if self.fortuneDetailCell?.view.adInfo?.placementKey == AdPlacementKey.MyFortuneNative_003.rawValue{
                if self.fortuneDetailCell?.view.adInfo?.adUnitPlatform == AdOrigin.Admob.rawValue {
                    AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a1_ad_show":"admob"])
                }
                else if self.fortuneDetailCell?.view.adInfo?.adUnitPlatform == AdOrigin.Fb.rawValue{
                    AnaliticsManager.sendEvent(AnaliticsManager.today_ad_native, data: ["a1_ad_show":"facebook"])
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func registerCell() {
        self.baseTableView?.register(FortuneDetailCell.self, forCellReuseIdentifier: NSStringFromClass(FortuneDetailCell.self))
        self.baseTableView?.register(CommentCellTableViewCell.self, forCellReuseIdentifier: "cell")
        self.baseTableView?.register(CommentCellTableViewCell.self, forCellReuseIdentifier: "hotCell")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            return 3
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if section == 0 {
                return self.rowNum
            }else if section == 1{
                return self.hotComentList.count + (self.hasAd == true ? 1 : 0)
            }else if section == 2{
                return self.commentList.count
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 0 {
                return self.rowNum
            }else if section == 1{
                if self.commentList.count > 0 {
                    return self.commentList.count + (self.hasAd == true ? 1 : 0)
                }else if self.hotComentList.count > 0 {
                    return self.hotComentList.count + (self.hasAd == true ? 1 : 0)
                }
            }
        }else{
            return self.rowNum + (self.hasAd == true ? 1 : 0)
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let isTodayFortuneType:Bool = (self.fortune == FortuneType.Today.rawValue)
      
        UserDefaults.standard.setValue(isTodayFortuneType, forKey: "isTodayFortuneType")
        UserDefaults.standard.synchronize()
        
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                if let cell:FortuneDetailCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(FortuneDetailCell.self), for: indexPath) as? FortuneDetailCell{
                    cell.renderCell(self.horoname ?? "",needTssAlert:needRss)
                    cell.postModel = self.post
                    cell.rootVC = self

                    self.fortuneDetailCell = cell

                    return cell
                }
            }else if indexPath.section == 1{
                if self.hasAd == true {
                    if indexPath.row == 0 {
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                        let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }else {
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier:"hotCell")
                        let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }
                }else if self.hasAd == false{
                    let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                    let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                    cell.contentView.addSubview(container)
                    return cell
                }
            }else if indexPath.section == 2{
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                        let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                
                if let cell:FortuneDetailCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(FortuneDetailCell.self), for: indexPath) as? FortuneDetailCell {
                     cell.renderCell(self.horoname ?? "",needTssAlert:needRss)
                     cell.postModel = self.post
                     cell.rootVC = self
                    
                    self.fortuneDetailCell = cell

                    return cell
                }
            }else if indexPath.section == 1{
                if self.commentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                            let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }else{
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                            let container = CommentLayoutContainer(model: self.commentList[indexPath.row - 1],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }
                    }else if  self.hasAd == false{
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                        let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }
                    
                }else if self.hotComentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                            let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }else{
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                            let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }
                    }else if self.hasAd == false{
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                        let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }
                }
            }
        }else{
            if indexPath.row == 0 {
              
                if let cell:FortuneDetailCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(FortuneDetailCell.self), for: indexPath) as? FortuneDetailCell{
                    cell.renderCell(self.horoname ?? "",needTssAlert:needRss)
                    cell.postModel = self.post
                    cell.rootVC = self
                    cell.fortune = self.fortune
                    
                    self.fortuneDetailCell = cell
                    return cell
                }
            }
        }
        return UITableViewCell(frame: CGRect.zero)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }else if section == 1{
                return 30
            }
            else if section == 2{
                return 30
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }else if section == 1{
                return 30
            }
        }
        return CGFloat.leastNormalMagnitude
    }
    
//    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if self.commentList.count > 0 && self.hotComentList.count > 0 {
//            if section == 1 {
//                return "Top Comments"
//            }else if section == 2{
//                return "New Comments"
//            }
//        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
//            if section == 1 && self.commentList.count > 0{
//                return "New Comments"
//            }else if section == 1 && self.hotComentList.count > 0{
//                return "Top Comments"
//            }
//        }
//        return ""
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            if section == 1 {
              return createHeaderView("   Top Comments")
            }else if section == 2{
                return createHeaderView("   New Comments")
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 1 && self.commentList.count > 0{
               return createHeaderView("   New Comments")
            }else if section == 1 && self.hotComentList.count > 0{
                return createHeaderView("   Top Comments")
            }
        }
        return nil
    }
    
    func createHeaderView(_ str:String)-> UIView {
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 32)
        label.textColor = UIColor.purpleContentColor()
        label.font = UIFont.systemFont(ofSize: 18)
        label.text = str
        label.backgroundColor = UIColor.basePurpleBackgroundColor()
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        headerView.addSubview(label)
        headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 32)
        
        let divideLine = UIView()
        divideLine.backgroundColor = UIColor.divideColor()
        divideLine.frame = CGRect(x: 0, y: headerView.frame.maxY, width: SCREEN_WIDTH, height: 1)
        headerView.addSubview(divideLine)
        return headerView
    }
    
    func createHeaderLable(_ str:String) -> UILabel {
        let label = UILabel()
        label.frame = CGRect(x: 20, y: 0, width: AdaptiveUtils.screenWidth - 20, height: 30)
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.purpleContentColor()
        label.font = UIFont.systemFont(ofSize: 21)
        label.text = str
        return label
    }
    
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let showRateStarTool:Bool = (self.fortune == FortuneType.Today.rawValue)
        let isAlreadyClick =  UserDefaults.standard.bool(forKey: "isAlreadyClick")
        var offsetHeight:CGFloat = 0
        if showRateStarTool == true && isAlreadyClick == false{
            offsetHeight = 110
        }
        
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                return FortuneDetailCell.calculateHeight(self.post?.content ?? "", needTssAlert: needRss)+offsetHeight
            }else if indexPath.section == 1{
                if self.hasAd == true {
                    if indexPath.row == 0 {
                        let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                        return  container.frame.size.height
                    }else if indexPath.row == 1{
                        return self.adHeight
                    }else{
                        let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                        return  container.frame.size.height
                    }

                }else if self.hasAd == false{
                    let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                    return  container.frame.size.height
                }
            }else if indexPath.section == 2{
                let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                return  container.frame.size.height
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                return FortuneDetailCell.calculateHeight(self.post?.content ?? "", needTssAlert: needRss)+offsetHeight
            }else if indexPath.section == 1{
                if self.hotComentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                            return  container.frame.size.height
                        }else if indexPath.row == 1{
                            return self.adHeight
                        }else{
                            let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                            return  container.frame.size.height
                        }
                    }else if self.hasAd == false{
                            let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                            return  container.frame.size.height
                    }
                }else if self.commentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                            return  container.frame.size.height
                        }else if indexPath.row == 1{
                            return self.adHeight
                        }else{
                            let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row - 1],isMyNotification:false)
                            return  container.frame.size.height
                        }
                    }else if self.hasAd == false{
                        let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                        return  container.frame.size.height
                    }
                }
            }
        }else{
            if indexPath.row == 0 {
               return FortuneDetailCell.calculateHeight(self.post?.content ?? "", needTssAlert: needRss)+offsetHeight
            }else  if self.hasAd == true {
                return self.adHeight
            }
        }
        return 0
    }
 
    var totalHeight:CGFloat = 0
    //MARK:AD Delegate
    
    func showAdSuccess(_ width: CGFloat, height: CGFloat) {
        self.adHeight = (screenWidth*257/375)
        self.hasAd = true
        self.baseTableView?.reloadData()
    }
    
    
    class func create(_ name:String?,style:HoroDetailType) -> FortureDetailViewcontroller{
        let vc = FortureDetailViewcontroller()
        vc.style = style
        vc.horoname = name
        return vc
    }
    
    // MARK: CommentFootBtn delegate
    func commentFootBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let replyVC = PostViewController()
            replyVC.isFirstClassReply = true
            replyVC.isForcastReply = true
            let replyModel = MyPostModel.init(jsonData: JSON.null, num: 0)
            replyVC.replyModel = replyModel
            replyVC.delegate = self
            replyModel.postId = self.post?.postId  ?? ""
            replyVC.horoscopeName = CURRENT_MAIN_SIGN
            replyVC.forecastName = self.fortune
            self.navigationController?.pushViewController(replyVC, animated: true)
        }
    }
//    init(name:String?,style:HoroDetailType) {
//        super.init(coder: .Grouped)
//        self.style=style
//        self.horoname=name
//    }
   
    //MARK:- FortureDetailViewcontrollerDelegate
    func dataLoadSuccess(_ post: ReadPostModel) {
        self.postId = post.postId ?? ""
        //        commentFootBtn.hidden = false
    }
    //MARK:- PostViewControllerDelegate
    func postCommentFailure(_ error: NSError) {
        HUD.flash(.label("Public failure"),delay: 1)
        print(error)
    }
    
    
    func postCommentSuccess(_ comment: MainComment) {
        self.commentList.insert(comment, at: 0)
        self.baseTableView?.reloadData()
    }
    
   
    @objc func reportBtnClickToShowReportController(_ noti:Notification) {
        
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod = model as! BaseComment
        ReportViewController.addacrion(mod, postId: mod.postId ?? "", isPostReport: false,vc:self)
        }
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        // Custom initialization
    }
    
    //MARK:praiseOrtreadePost
    func praiseOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOneNormalTopic(name, action: action, value: value, id: id)
    }
    
    func tradeOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOrPraiseAction(name, action: action, value: value, id: id)
    }
    
    func tradeOrPraiseAction(_ name: String, action: String, value: Bool, id: String) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            HoroscopeDAO.sharedInstance.likeOrDislikeAction(name, action: action, value: value, id: id)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
   
}

