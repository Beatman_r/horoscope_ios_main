//
//  HoroDetailBaseViewModel.swift
//  Horoscope
//
//  Created by Wang on 16/11/25.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

public enum HoroDetailType : Int {
 
     case weekly
     case monthly
     case yearly
     case tomorrow
     case today
     case off1
}

class HoroDetailBaseViewModel: NSObject {

    init(name:String) {
        super.init()
        self.horoName = name
    }
    
    var horoName:String?
    var weekModel:ZodiacModel?
    var monthlyModel:ZodiacModel?
    var yearlyModel:ZodiacModel?
    var dailyModel:ZodiacModel?
    
    fileprivate let dao = HoroscopeDAO()
    
    func loadDetail(_ success:@escaping () ->(),failure:(_ error:NSError) -> ()) {
         dao.getWeeklyHoroscope(horoName ?? "", dateOffset: 0, success: { (model) in
            self.weekModel = model
            self.dao.getMonthlyHoroscopeUrl(self.horoName ?? "", success: { (model) in
                self.monthlyModel = model
                  self.dao.getYearlyHoroscopeUrl(self.horoName ?? "", success: { (model) in
                    self.yearlyModel = model
                    success()
                  }) { (error) in
                  }
                }) { (error) in
              }
            }) { (error) in
         }
    }
    
 
 }
