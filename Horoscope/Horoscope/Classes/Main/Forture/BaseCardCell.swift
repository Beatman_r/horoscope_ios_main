//
//  BaseCardCell.swift
//  Horoscope
//
//  Created by Wang on 16/11/25.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

class BaseCardCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static let cellPadding:CGFloat=12
    func editBasicUI() {
        
    }
    
    var cornerBackView:UIView?
    func createVornerBackView() {
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        cornerBackView = UIView()
        self.contentView.addSubview(cornerBackView!)
        let backLayer = cornerBackView!.layer
        backLayer.shouldRasterize=true
        backLayer.rasterizationScale=UIScreen.main.scale
        cornerBackView?.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createVornerBackView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
