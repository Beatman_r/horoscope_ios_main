//
//  partOneView.swift
//  Horoscope
//
//  Created by Wang on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class partOneView: UIView,UIScrollViewDelegate {
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(backImageView)
     }
    
    var zodiacImgNameArr : [String] = ["daily_aries.jpg","daily_taurus.jpg","daily_gemini.jpg","daily_cancer.jpg","daily_leo.jpg","daily_virgo.jpg","daily_libra.jpg","daily_scorpio.jpg","daily_sagittarius.jpg","daily_capricorn.jpg","daily_aquarius.jpg","daily_pisces.jpg"]
    
    let backImageView:UIImageView = {
        let screenWidth=HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()
        let screenHeight=HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenHeight()
        let image:UIImageView = UIImageView()
        image.frame = CGRect(x: 0, y: 0,width: screenWidth, height: screenWidth/15*8)
       return image
    }()
    
    let bgImageViewCale:CGFloat = 0.6
    let screenWidth  = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()
    let screenHeight = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenHeight()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
