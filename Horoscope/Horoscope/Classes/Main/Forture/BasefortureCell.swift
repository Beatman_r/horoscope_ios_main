//
//  BasefortureCell.swift
//  Horoscope
//
//  Created by Wang on 16/11/25.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class BasefortureCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func editUI() {
        
    }
    
    func renderCell() {
        
    }
    
    //Mark:CalculateHeght
    class func calculateCellHeight() -> CGFloat {
         return 100.0
    }
    
    
    // MARK:Create
    static let cellId = "BasefortureCell"
    class func dequeueReusableCell(_ tableView: UITableView,
                                   cellForRowAtIndexPath indexPath: IndexPath) -> BasefortureCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId,
                                                               for: indexPath) as! BasefortureCell
        return cell
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
