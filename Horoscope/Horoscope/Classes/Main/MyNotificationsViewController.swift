//
//  MyNotificationsViewController.swift
//  Horoscope
//
//  Created by xiao  on 17/2/17.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD
import MJRefresh
import IDMPhotoBrowser
import SwiftyJSON
class MyNotificationsViewController: BaseViewController,PostViewControllerDelegate{
    
    var commentArray = [MainComment]()
    var post:ReadPostModel?
    var tableView:UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        self.navigationItem.title = NSLocalizedString("Notifications", comment: "")
        self.getJson()
        self.setupRefreshFooterView()
        self.setUpUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFoldComents(_:)), name: NSNotification.Name(rawValue: MoreCommentBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.CommentViewClickToShowCommentReplyView(_:)), name: NSNotification.Name(rawValue: CommentViewClickToShowReplyView), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.replyBtnClick(_:)), name: NSNotification.Name(rawValue: ReplyBtnClickToShowReplyController), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reportBtnClickToShowReportController), name: NSNotification.Name(rawValue: ReportBtnClickToShowReportController), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.seeOriginalBtnClick), name: NSNotification.Name(rawValue: SeeOriginalBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.commentsMoreThanSeventyFloor), name: NSNotification.Name(rawValue: CommentsMoreThanSeventyFloor), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.likeBtnClick(_:)), name: NSNotification.Name(rawValue: LikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.disLikeBtnClick(_:)), name: NSNotification.Name(rawValue: DisLikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userClickToUserTopic(_:)), name: NSNotification.Name(rawValue: UserClickToUserTopic), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(imageClick(_:)), name: NSNotification.Name(rawValue: "deviationClickBigImage"), object: nil)
    }
    
    @objc func imageClick(_ noti:Notification){
        let imaName = noti.userInfo?["url"] ?? ""
        let imgUrl = URL(string: (imaName ) as! String)
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.present(brower!, animated: true, completion: nil)
    }
    
    @objc func userClickToUserTopic(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! BaseComment)
        let userVC  = ReadOtherUserPostViewController()
        userVC.userId = mod.userInfo?.userId ?? ""
        userVC.userName = mod.userInfo?.name ?? ""
        self.navigationController?.pushViewController(userVC, animated: true)
    }


    
    @objc func likeBtnClick(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! ParentComment)
        for main in self.commentArray {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
    }
    
    @objc func disLikeBtnClick(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! ParentComment)
        for main in self.commentArray {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
    }
    
    func changeModelLikeOrDisLikeState(_ model:BaseComment,mod:ParentComment){
        model.likeCount = mod.likeCount
        model.dislikeCount = mod.dislikeCount
        model.isDisliked = mod.isDisliked
        model.isLiked = mod.isLiked
        
    }
    

    @objc func commentsMoreThanSeventyFloor() {
        HUD.flash(.label("Too much comments"),delay: 1)
    }
    
    func setUpUI() {
        baseTableView?.separatorStyle = UITableViewCellSeparatorStyle.none
        baseTableView?.register(CommentCellTableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func getJson() {
        let offset = "0"
        HoroscopeDAO.sharedInstance.getMyNotificationList(offset, success: { (commentList) in
            self.commentArray = commentList
            if self.commentArray.count == 0 {
                let blankLabel = UILabel()
                blankLabel.text = "No notification at this moment."
                blankLabel.textColor = UIColor.luckyPurpleTitleColor()
                blankLabel.font = HSFont.baseRegularFont(20)
                blankLabel.textAlignment = .center
                blankLabel.numberOfLines = 0
                self.view.addSubview(blankLabel)
                self.view.bringSubview(toFront: blankLabel)
                blankLabel.snp.makeConstraints({ (make) in
                    make.center.equalTo(self.view)
                    make.width.equalTo(self.view).offset(-24)
                })
            }
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            }) { (failure) in
                self.failedRetryView?.loadFailed()
        }
    }
    
    override func retryToLoadData() {
        self.getJson()
    }
    
    @objc func replyBtnClick(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let replyVC = PostViewController()
        replyVC.isReply = true
        replyVC.model = model as? BaseComment
//        let postModel  = ReadPostModel(jsonData: nil)
//        postModel.postId = (model as? BaseComment)?.postId
      
        let replyModel = MyPostModel(jsonData: JSON.null, num: 0)
        replyModel.postId = (model as? BaseComment)?.postId
        replyVC.replyModel = replyModel
        replyVC.delegate = self
        self.navigationController?.pushViewController(replyVC, animated: true)
    }
    
    @objc func getFoldComents(_ noti:Notification) {
        guard let mainComment = noti.userInfo?["mainCommet"] else {
            return
        }
        guard let foldIndex = (mainComment as! MainComment ).parentCommentWarp?["index"] else {
            return
        }
        
        guard let negative_index = (mainComment as! MainComment ).parentCommentWarp?["negative_index"] else {
            return
        }
        
        guard let commentId = (mainComment as! MainComment ).commentId else {
            return
        }
        
        HoroscopeDAO.sharedInstance.getFoldCommentList(commentId, index: (foldIndex as! Int), negative_index: (negative_index as! Int), success: { (foldCommentList) in
            
            var needInsertCmt : MainComment?
            var indexpath : IndexPath = IndexPath(row: 0, section: 0)
            for  index  in 0..<self.commentArray.count {
                let mainCmt = self.commentArray[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
                    indexpath = IndexPath(row: index, section: 0)
                }
            }
            needInsertCmt?.parentCommentWarp = nil
            
            guard let  needInsertCmtSubCmtCount = needInsertCmt?.parentCommentList.count else {
                return
            }
            for index in 0..<needInsertCmtSubCmtCount {
                if index == (foldIndex as! Int ) {
                    for index in 0..<foldCommentList.count {
                        needInsertCmt?.parentCommentList.insert(foldCommentList[index], at: index+2)
                    }
                }
            }
            self.baseTableView?.reloadRows(at: [indexpath], with: .automatic)
        }) { (failure) in
            LXSWLog(failure)
        }
    }
    
     override func setupRefreshFooterView() {
        let footer = MJRefreshBackNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreList))
        footer?.setTitle(NSLocalizedString("Pull up to load", comment: ""), for: .idle)
        footer?.setTitle(NSLocalizedString("Release to refresh", comment: ""), for: .pulling)
        footer?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        self.tableView?.mj_footer = footer
    }
    
     override func loadMoreList() {
        let offset  = String(self.commentArray.count)
        
        HoroscopeDAO.sharedInstance.getMyNotificationList(offset, success: { (commentList) in
            if commentList.count == 0 {
                HUD.flash(.label("No More Comments"),delay: 1)
                self.tableView?.mj_footer.endRefreshing()
            }else{
                self.commentArray  = self.commentArray + commentList
                self.tableView?.reloadData()
                self.tableView?.mj_footer.endRefreshing()
            }
        }) { (failure) in
            _ = self.tableView?.mj_footer
        }
    }
    
    //MARK:- CommentViewCellDelegate
    @objc func CommentViewClickToShowCommentReplyView(_ noti: Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let replyView = ReplyCommentView(frame: self.view.bounds, model: model as! BaseComment)
        self.view.addSubview(replyView)
    }
    
    //MARK:- UITableViewDelegate
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentArray.count
    }
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let container = CommentLayoutContainer(model: self.commentArray[indexPath.row],isMyNotification:true)
        cell.contentView.addSubview(container)
        return cell
    }
    
     override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let container = CommentLayoutContainer.init(model: self.commentArray[indexPath.row],isMyNotification:true)
        //        return  container.frame.size.height + (container.parentCommentWarp != nil ? 40 : 0)
        return  container.frame.size.height
    }
    
    @objc func seeOriginalBtnClick(_ note:Notification) {
        guard let model = note.userInfo?["model"] else {
            return
        }
        let mod = model as! BaseComment
        if mod.forecastName != nil {
            let forcastVC = FortureListViewcontroller.create(mod.forecastName ?? "")
            forcastVC.pageType = 1
            if mod.forecastName == "yesterday"  {
                forcastVC.currentIndex = 0
            }else if mod.forecastName == "today" {
                forcastVC.currentIndex = 1
            }else if mod.forecastName  == "tomorrow"{
                forcastVC.currentIndex = 2
            }else if mod.forecastName == "weekly"{
                forcastVC.currentIndex = 3
            }else if mod.forecastName == "monthly"{
                forcastVC.currentIndex = 4
            }else if mod.forecastName == "yearly"{
                forcastVC.currentIndex = 5
            }
            self.navigationController?.pushViewController(forcastVC, animated: true)
        }else{
            let postVC = ReadPostViewController()
            let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            activityView.color = UIColor.white
            activityView.frame = CGRect(x: SCREEN_WIDTH/2 - 100, y: SCREEN_HEIGHT/2 - 100, width: 200, height: 200)
            self.view.addSubview(activityView)
            activityView.startAnimating()
            HoroscopeDAO.sharedInstance.getTopic(mod.postId ?? "", success: { (post) in
                let model = MyPostModel(model: post)
                postVC.postModel = model
                self.navigationController?.pushViewController(postVC, animated: true)
                activityView.stopAnimating()
                }, failure: { (failure) in
                    print("SeeOriginalFailure")
            })
        }
    }
    
    @objc func reportBtnClickToShowReportController(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod = model as! BaseComment
        ReportViewController.addacrion(mod, postId: mod.postId ?? "", isPostReport: false,vc:self)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- PostViewControllerDelegate
    func postCommentFailure(_ error: NSError) {
        HUD.flash(.label("Public failure"),delay: 1)
    }
    
    func postCommentSuccess(_ comment: MainComment) {
//        self.commentArray.insert(comment, atIndex: 0)
//        self.tableView?.reloadData()
    }

}
