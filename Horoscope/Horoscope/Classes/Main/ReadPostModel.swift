//
//  ReadPostModel.swift
//  Horoscope
//
//  Created by Beatman on 17/2/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

enum PostType: Int {
    case video
    case normal
    case ad
}
class ReadPostModel: basePostModel {
    
    var horo:String?
    var dateDura:String?
    var dislikeCount : Int?
    var likeCount : Int?
    var commentCount : Int?
    var anonymous : Bool?
    var postId : String?
    var createTime : Int64?
    var tagList : [AnyObject]?
    var viewCount : Int?
    var content : String?
    var createTimeHuman : String?
    var shareCount : Int?
    var imageList : [AnyObject]?
    var user:User?
    var commentList:[MainComment]?
    var hotCommentList:[MainComment]?    
    var video : VideoPostModel?
    var iliked:Bool? = false
    var iDisliked:Bool? = false
    var title:String?
    var dateString:String?
    var luckColorValue:String?
    var isDisliked :Bool?
    var isLiked:Bool?
    var cellRowNumber : Int?
    var shouldShowAllTag : Bool?
    
   
    init(jsonData : JSON){
        super.init()
        
        self.dislikeCount = jsonData["dislikeCount"].intValue 
        self.likeCount = jsonData["likeCount"].intValue 
        self.commentCount = jsonData["commentCount"].intValue 
        self.anonymous = jsonData["anonymous"].boolValue 
        self.postId = jsonData["postId"].stringValue 
        self.createTime = jsonData["createTime"].int64Value
        self.tagList = jsonData["tagList"].arrayObject as [AnyObject]?
        self.viewCount = jsonData["viewCount"].intValue
        self.content = jsonData["content"].stringValue
        self.shareCount = jsonData["shareCount"].intValue 
        self.createTimeHuman = jsonData["createTimeHuman"].stringValue 
        self.imageList = jsonData["imageList"].arrayObject as [AnyObject]?
        self.dateString = jsonData["dateString"].stringValue
        self.luckColorValue = jsonData["luckColorValue"].stringValue
        self.isDisliked = jsonData["isDisliked"].boolValue
        self.isLiked = jsonData["isLiked"].boolValue
        
        
        if let  userJson = jsonData["userInfo"].dictionaryObject{
            let user = User()
            user.initWitchDic(userJson as [String : AnyObject])
            self.user = user
        }
        if let videoData = jsonData["video"].dictionaryObject{
            let video = VideoPostModel(dic: videoData as [String : AnyObject])
            self.video = video
        }
        self.title = jsonData["title"].stringValue 
        self.iDisliked = jsonData["isDisliked"].boolValue
        self.iliked = jsonData["isLiked"].boolValue
    }
    
    init(model : MyPostModel) {
        self.dislikeCount = model.dislikeCount ?? 0
        self.likeCount = model.likeCount ?? 0
        self.commentCount = model.commentCount ?? 0
        self.anonymous = model.anonymous ?? true
        self.postId = model.postId ?? ""
        self.createTime = model.createTime ?? 0
        self.tagList = model.tagList ?? []
        self.viewCount = model.viewCount ?? 0
        self.content = model.content ?? ""
        self.createTimeHuman = model.createTimeHuman ?? ""
        self.shareCount = model.shareCount ?? 0
        self.imageList = model.imageList ?? []
        self.user = model.user
        self.iDisliked = model.iDisliked
        self.iliked = model.iliked
    }
}
