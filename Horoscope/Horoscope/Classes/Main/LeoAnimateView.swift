//
//  LeoAnimateView.swift
//  Horoscope
//
//  Created by Beatman on 17/1/20.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class LeoAnimateView: BaseZodiacAnimateView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createBasicUI()
        self.addToView()
        self.addViewConstraint()
    }
    
    func createBasicUI() {
        self.bgColorView.image = UIImage(named: "img_leo");//UIImage(contentsOfFile: bgColorPath ?? "")
        
        self.shootingTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(subFireShootingStar), userInfo: nil, repeats: true)
    }
    
    override func startAnimation() {
        self.shootingTimer?.fireDate = Date.init(timeIntervalSinceNow: 0)
    }

    override func stopAnimate() {
        self.shootingTimer?.invalidate()
    }

    @objc func subFireShootingStar() {
        self.fireShootingStar(self.shootingStarView)
    }
    
    func addToView() {
        self.addSubview(bgColorView)
        self.addSubview(shootingStarView)
    }
    
    func addViewConstraint() {
        self.bgColorView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.snp.edges)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
