//
//  ZodiacAnimateView.swift
//  Horoscope
//
//  Created by Beatman on 17/1/20.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SnapKit

class BaseZodiacAnimateView: UIView {
    
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    
    var starTimer : Timer?
    var shootingTimer : Timer?
    
    let bgColorView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    let cloudView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    let shootingStarView : UIImageView = {
        let imgView = UIImageView()
        let shootingStarPath = Bundle.main.path(forResource: "scorpio_meteor", ofType: "png")
        imgView.image = UIImage(contentsOfFile: shootingStarPath ?? "")
        return imgView
    }()
    
    let backStarView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    let frontStarView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    let moonView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    let thingView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    let mountainView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    let characterView : UIImageView = {
        let imgView = UIImageView()
        return imgView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    func fireShootingStar(_ shootingView : UIImageView) {
        let randomNum = CGFloat(arc4random()).truncatingRemainder(dividingBy: (self.screenHeight/2-self.screenHeight * 100/667)) - 200.0
        shootingView.frame = CGRect(x: -self.screenWidth*159/375, y: randomNum, width: self.screenWidth*159/375, height: self.screenHeight*98/667)
        
        let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            UIView.animate(withDuration: 2, animations: {
                shootingView.frame = CGRect(x: self.screenWidth+self.screenWidth * 50/375, y: randomNum+self.screenHeight * 330/667, width: self.screenWidth*159/375, height: self.screenHeight*98/667)
                shootingView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                }, completion: { (true) in
                    shootingView.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        }
    }
    
    func lightUpStar(_ frontView:UIImageView,backView:UIImageView) {
        UIView.animate(withDuration: 2, animations: {
            frontView.alpha = 1
            backView.alpha = 0
        }, completion: { (true) in
            UIView.animate(withDuration: 2, animations: {
                frontView.alpha = 0
                backView.alpha = 1
            })
        }) 
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startAnimation() {
        
    }
    
    func stopAnimate() {
        
    }
}
