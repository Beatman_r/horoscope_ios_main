//
//  MeHeaderView.swift
//  Horoscope
//
//  Created by Wang on 16/12/20.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class MeHeaderView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createUI()
        self.nameLab.isHidden=false
        self.signBtn.isHidden=false
        addCondtrains()
    }
    
    fileprivate func createUI(){
        self.backgroundColor=UIColor.clear
        self.addSubview(backImg)
        self.addSubview(avatarCover)
        self.addSubview(avatar)
        self.addSubview(signBtn)
        self.addSubview(nameLab)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onClickSignBtn))
        avatar.isUserInteractionEnabled = true
        avatar.addGestureRecognizer(gesture)
        signBtn.addTarget(self, action: #selector(onClickSignBtn), for: .touchUpInside)
    
        avatar.layer.masksToBounds=true
        avatar.layer.cornerRadius=44
        avatarCover.tintColor=UIColor.commonPinkColor()
    }
    
    @objc func onClickSignBtn()  {
        let vc = HsLoginViewController()
        vc.mevc = self.rootVc
        self.rootVc?.navigationController?.pushViewController(vc, animated: true)
    }
    
    weak var rootVc:UIViewController?
    
    fileprivate func addCondtrains(){
        backImg.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom).offset(-50)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
        }
        avatarCover.snp.makeConstraints { (make) in
            make.top.equalTo(self.backImg.snp.top).offset(46)
            make.centerX.equalTo(self.backImg.snp.centerX)
            make.height.width.equalTo(88)
        }
        avatar.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatarCover.snp.top)
            make.centerX.equalTo(self.backImg.snp.centerX)
            make.height.width.equalTo(88)
        }
        signBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatar.snp.bottom).offset(28)
            make.centerX.equalTo(self.avatar.snp.centerX)
            make.height.equalTo(40)
            make.width.equalTo(137)
        }
        nameLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatar.snp.bottom).offset(24)
            make.centerX.equalTo(self.avatar.snp.centerX)
            make.width.equalTo(300)
        }
    }
    
    let backImg:UIImageView={
       let img = UIImageView()
       img.image=UIImage(named:"MeBlackground.jpg")
       return img
    }()
    
    let avatarCover:UIImageView={
        let img = UIImageView()
        img.image=UIImage(named: "ic_avatar_")
        return img
    }()
    
    let signBtn:UIButton={
        let btn = UIButton(type:.custom)
        btn.layer.borderWidth = 1
        btn.setTitle(NSLocalizedString("Sign up", comment: ""), for: UIControlState())
        btn.titleLabel?.font = HSFont.baseRegularFont(20)
        btn.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        btn.layer.borderColor = UIColor.commonPinkColor().cgColor
        btn.layer.masksToBounds=true
        btn.layer.cornerRadius=20
        return btn
    }()
    
    let nameLab:UILabel={
        let lab=UILabel()
        lab.font=HSFont.baseRegularFont(20)
        lab.textColor=UIColor.commonPinkColor()
        lab.textAlignment = .center
        lab.numberOfLines = 0
        lab.text = ""
        return lab
    }()
    
    func userSetting(){
        let user = AccountManager.sharedInstance.getUserInfo()
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus != .anonymous && userStatus != .null{
            if let imgUrl = user?.avatar{
            self.avatar.sd_setImage(with: URL(string:imgUrl), placeholderImage: UIImage(named:""))
            }
            self.nameLab.text="\(user?.userName ?? "")\n\((user?.horoname ?? "").capitalized)"
            self.nameLab.isHidden=false
            self.signBtn.isHidden=true
        }else{
            self.avatar.sd_setImage(with: URL(string:""), placeholderImage: UIImage(named:""))
            self.nameLab.isHidden=true
            self.signBtn.isHidden=false
        }
    }
    
    let avatar:UIImageView={
        let img = UIImageView()
        return img
    }()
    
     required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
