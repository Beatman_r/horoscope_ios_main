//
//  BaseCommentCell.swift
//  Horoscope
//
//  Created by Wang on 17/2/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import IDMPhotoBrowser
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class BaseCommentCell: BaseCardCell {
    weak var rootVc:UIViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
        self.addConstrains()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:BtnClick
    
    @objc func praiseBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.iliked == false{
                let newCount = (model?.likeCount ?? 0) + 1
                self.model?.likeCount = newCount
                self.model?.iliked = true
                self.rencderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: model?.commentId ?? "")
            }
            else  if self.model?.iliked == true {
                var newCount = (model?.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.model?.likeCount = newCount
                self.model?.iliked = false
                self.rencderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: model?.commentId ?? "")
            }
        }
    }
    
    @objc func treadBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if  self.model?.iDisliked == false{
                self.model?.iDisliked = true
                let newCount = (model?.dislikeCount ?? 0) + 1
                self.model?.dislikeCount = newCount
                self.rencderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: model?.commentId ?? "")
               
            }
            else if  self.model?.iDisliked == true{
                self.model?.iDisliked = false
                var newCount = (model?.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.model?.dislikeCount = newCount
                self.rencderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: model?.commentId ?? "")
            }
        }
    }
    
   

    func createUI() {
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        
        self.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.cornerBackView?.addSubview(userNameLabe)
        self.cornerBackView?.addSubview(userAvaCoverIma)
        self.cornerBackView?.addSubview(userAvaIma)
        self.cornerBackView?.addSubview(timeLab)
        self.cornerBackView?.addSubview(horoLab)
        self.cornerBackView?.addSubview(contentLab)
        self.cornerBackView?.addSubview(figureIma)
        self.cornerBackView?.addSubview(praiseBtn)
        self.cornerBackView?.addSubview(praiseLab)
        self.cornerBackView?.addSubview(treadBtn)
        self.cornerBackView?.addSubview(treadNumLab)
        self.cornerBackView?.addSubview(contentLine)
        self.cornerBackView?.addSubview(bottomDivideLine)
        self.cornerBackView?.addSubview(signLabel)
        
        praiseBtn.addTarget(self, action: #selector(praiseBtnClick), for: .touchUpInside)
        treadBtn.addTarget(self, action: #selector(treadBtnClick), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(viewImage))
        self.figureIma.addGestureRecognizer(tap)
        self.figureIma.isUserInteractionEnabled = true
   }
    
    //MARK: fullScreenIma
    @objc func viewImage() {
        
        let imaName = self.model?.imageList?.first as? String
        let imgUrl = URL(string: imaName ?? "")
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr, animatedFrom: self.figureIma)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.rootVc?.present(brower!, animated: true, completion: nil)
    }
    
    func addConstrains() {
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        userAvaIma.snp.makeConstraints { (make) in
            make.left.equalTo(cornerBackView!.snp.left).offset(12)
            make.top.equalTo(cornerBackView!.snp.top).offset(20)
            make.width.height.equalTo(44)
        }
        
        userNameLabe.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.right).offset(15)
            make.top.equalTo(userAvaIma.snp.top)
        }
        
        horoLab.snp.makeConstraints { (make) in
            make.left.equalTo(userNameLabe.snp.left)
            make.bottom.equalTo(userAvaIma.snp.bottom)
        }
        
        timeLab.snp.makeConstraints { (make) in
            make.right.equalTo(contentLine.snp.right).offset(-12)
            make.bottom.equalTo(self.userNameLabe.snp.bottom)
        }
        
        contentLine.snp.makeConstraints { (make) in
            make.left.equalTo(cornerBackView!.snp.left)
            make.top.equalTo(userAvaIma.snp.bottom).offset(0)
            make.height.equalTo(0)
            make.right.equalTo(cornerBackView!.snp.right)
        }
        
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.left)
            make.right.equalTo(self.contentLine.snp.right).offset(-12)
            make.top.equalTo(contentLine.snp.bottom).offset(10)
        }
        
        figureIma.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.left)
            make.right.equalTo(self.contentLine.snp.right).offset(-12)
            make.top.equalTo(contentLab.snp.bottom).offset(20)
            let figureHeight = (UIScreen.main.bounds.size.width - 24)
            make.height.equalTo(figureHeight)
        }
        
        treadNumLab.snp.makeConstraints { (make) in
            make.right.equalTo(self.contentLine.snp.right).offset(-12)
            make.bottom.equalTo(self.cornerBackView!.snp.bottom).offset(-20)
        }
        
        treadBtn.snp.makeConstraints { (make) in
            make.right.equalTo(self.treadNumLab.snp.left).offset(-10)
            make.bottom.equalTo(treadNumLab.snp.bottom).offset(3)
            make.width.height.equalTo(18)
        }
        
        praiseLab.snp.makeConstraints { (make) in
            make.right.equalTo(treadBtn.snp.left).offset(-30)
            make.bottom.equalTo(treadNumLab.snp.bottom)
            make.top.equalTo(treadNumLab.snp.top)
        }
        
        praiseBtn.snp.makeConstraints { (make) in
            make.right.equalTo(praiseLab.snp.left).offset(-10)
            make.bottom.equalTo(self.praiseLab.snp.bottom).offset(-3)
            make.width.height.equalTo(18)
        }
        
        bottomDivideLine.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.bottom)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(1)
        }
    }
    
    var model:MyCommentModel? {
        didSet{
           rencderCell()
        }
    }
    
    func rencderCell() {
        if model?.iliked == true{
            praiseBtn.setImage(UIImage(named:"praise2_"), for: UIControlState())
        }else{
            praiseBtn.setImage(UIImage(named:"praise_"), for: UIControlState())
        }
        
        if model?.iDisliked == true{
            treadBtn.setImage(UIImage(named:"dislike_pink_"), for: UIControlState())
        }else{
            treadBtn.setImage(UIImage(named:"Step-on_"), for: UIControlState())
        }
        
        self.praiseLab.text = "\(HSHelpCenter.sharedInstance.textTool.getFormattedNumber(self.model?.likeCount ?? 0))"
        self.treadNumLab.text="\(HSHelpCenter.sharedInstance.textTool.getFormattedNumber(model?.dislikeCount ?? 0))"
      
        
        self.userAvaIma.sd_setImage(with: URL(string: model?.user?.avatar ?? ""), placeholderImage: UIImage(named:"icon_author"))
        self.userNameLabe.text = model?.user?.name ?? ""
        self.timeLab.text = model?.createTimeHuman ?? ""
        self.horoLab.text = model?.user?.horoscopeName ?? ""
        self.contentLab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.purpleContentColor(), targetStr: model?.content ?? "", font: HSFont.baseRegularFont(15))
        self.praiseLab.text = "\(model?.likeCount ?? 0)" 
        self.treadNumLab.text="\(model?.dislikeCount ?? 0)" 
        self.signLabel.text = model?.user?.horoscopeName ?? ""
        if model?.imageList?.count > 0{
            let imgName = (model?.imageList?.first)! as! String 
            figureIma.sd_setImage(with: URL(string: imgName), placeholderImage: UIImage(named: "icon_author"))
            figureIma.snp.makeConstraints { (make) in
                make.left.equalTo(userAvaIma.snp.left)
                make.right.equalTo(self.contentLine.snp.right).offset(-12)
                make.top.equalTo(contentLab.snp.bottom).offset(20)
                let figureHeight = (UIScreen.main.bounds.size.width - 24)
                make.height.equalTo(figureHeight)
            }
                figureIma.isHidden = false
        }else{
            figureIma.snp.remakeConstraints { (make) in
                make.left.equalTo(userAvaIma.snp.left)
                make.right.equalTo(contentLab.snp.right)
                make.top.equalTo(contentLab.snp.bottom)
                make.height.equalTo(0)
            }
            figureIma.isHidden = true
        }
    }
    
    let userAvaIma:UIImageView = {
        let ima = UIImageView()
        ima.layer.masksToBounds=true
        ima.layer.cornerRadius=22
        ima.contentMode = .scaleAspectFill
        return ima
    }()
    
    let userAvaCoverIma:UIImageView = {
        let ima = UIImageView()
        ima.image=UIImage(named:"authorCover")
        ima.backgroundColor=UIColor.white
        ima.layer.cornerRadius = 24
        ima.layer.masksToBounds = true
        return ima
    }()
    
    let userNameLabe:UILabel = {
        let lab = UILabel()
        lab.textAlignment = .left
        lab.textColor = UIColor.luckyPurpleTitleColor()
        lab.font = HSFont.baseRegularFont(18)
        return lab
    }()
    
    let horoLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.pubTimeColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let timeLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.pubTimeColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let contentLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.font = HSFont.baseRegularFont(18)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        return lab
    }()
    
   
    let figureIma:UIImageView = {
        let ima = UIImageView()
        ima.contentMode = UIViewContentMode.scaleAspectFit
        return ima
    }()
    
    let treadBtn:UIButton = {
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "Step-on_"), for: UIControlState())
        return button
    }()
    
    let praiseBtn:UIButton = {
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named: "praise_"), for: UIControlState())
        return button
    }()
    
    let treadNumLab:UILabel = {
        let lab = UILabel()
        lab.font = HSFont.baseRegularFont(18)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        return lab
    }()
    
    let praiseLab:UILabel = {
        let lab = UILabel()
        lab.font = HSFont.baseRegularFont(18)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        return lab
    }()
    
    let contentLine : UIView = {
        let divideView = UIView()
        divideView.backgroundColor = UIColor.divideColor()
        return divideView
    }()
    
    let bottomDivideLine : UIView = {
        let bottomDivideLine = UIView()
        bottomDivideLine.backgroundColor = UIColor.divideColor()
        return bottomDivideLine
    }()
    
    let signLabel : UILabel = {
        let signLabel = UILabel()
        signLabel.textColor = UIColor.luckyPurpleTitleColor()
        signLabel.font = HSFont.baseRegularFont(18)
        signLabel.textAlignment = NSTextAlignment.right
        return signLabel
    }()
    
    class func calculateHeight(_ model:MyCommentModel?) ->CGFloat{
        let wenben = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: model?.content ?? "", fontSize: 15, font: HSFont.baseRegularFont(15), width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25)
       var h = 20+44+20+10+wenben
        h += 20
        let figureHeight = (UIScreen.main.bounds.size.width - 24)
        if (model?.imageList?.count) == 0{
            h -= 20
        }else {
            h += figureHeight
        }
        h += 18
        h += 20
        return  h
    }
    
    //MARK:Dequeuse
    static let cellId="BaseCommentCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> BaseCommentCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! BaseCommentCell
        return cell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
