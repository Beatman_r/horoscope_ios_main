//
//  RateUsCell.swift
//  Horoscope
//
//  Created by Beatman on 16/12/6.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class RateUsCell: BaseCardCell {
    weak var rootvc:UIViewController?
    var index = 0
    var flag = false
    var buttons = [UIButton]()
    var bgButtons = [UIButton]()
    static let ratedTag:String = "hasRate"
    fileprivate var shouldShowRateUsKey:String="HS_hasRate"
    
    func getRateCellHeight() -> CGFloat {
        return 0
    }
    
    class func shouldShowRateUs()->Bool{
        return !(UserDefaults.standard.bool(forKey: "hasRate"))
    }
    
    func ratedOrCanceled(){
        UserDefaults.standard.set(true, forKey: "hasRate")
        UserDefaults.standard.synchronize()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createRateUs()
    }
    
    static let height:CGFloat = 200
    func createRateUs() {
        self.cornerBackView?.layer.masksToBounds = false
        self.cornerBackView?.layer.cornerRadius = 0
        var starYposition:CGFloat = 90
        if SCREEN_WIDTH == 375{
             starYposition = 100
        }else if SCREEN_WIDTH == 414{
             starYposition = 115
        }
        
        let starWidth:CGFloat=35
        let starHeight:CGFloat=35
        let jiange:CGFloat=15
        let startX=(SCREEN_WIDTH-starWidth*5-jiange*4)/2
        self.backgroundColor = UIColor.basePurpleBackgroundColor()
        self.UISetting()
        for index in 0...4 {
            let bgButton:UIButton = {
                let button = UIButton(type: .custom)
                button.tag = index + 10
                button.frame = CGRect(x: startX+CGFloat(index)*(starWidth+jiange), y: starYposition, width: starWidth, height: starHeight)
                button.addTarget(self, action: #selector(rateButtonOnClick(_:)), for: .touchUpInside)
                button.setImage(UIImage(named:"ic_rate_star_empty_small")?.withRenderingMode(.alwaysOriginal), for: UIControlState())
                return button
            } ()
            self.addSubview(bgButton)
            let fButton:UIButton = {
                let button = UIButton(type: .custom)
                button.alpha = 0
                button.tag = index + 10
                button.addTarget(self, action: #selector(rateButtonOnClick(_:)), for: .touchUpInside)
                button.frame = CGRect(x: startX+CGFloat(index)*(starWidth+jiange), y: starYposition, width: starWidth, height: starHeight)
                button.setImage(UIImage(named:"card_star")?.withRenderingMode(.alwaysOriginal), for: UIControlState())
                return button
            } ()
            self.buttons.append(fButton)
            self.bgButtons.append(bgButton)
            self.addSubview(fButton)
        }
        self.createTimer()
    }
    
    func dismissCloseButton() {
        
        UIView.animate(withDuration: 0.4, animations: {
            self.frame = CGRect(x: 10, y: 64, width: UIScreen.main.bounds.width-20, height: 0.01)
        }) 
        self.ratedOrCanceled()
    }
    
    let img:UIImageView = {
        let view = UIImageView()
        view.backgroundColor=UIColor.white
        return view
    }()
    
    var titleLabel : UILabel?
//    var divideView : UIView?
    var bottomDivideView : UIView?
    func UISetting() {
        
//        self.divideView = UIView()
//        self.divideView?.alpha = 0.4
//        self.divideView?.backgroundColor = UIColor.init(red: 88/255, green: 88/255, blue: 88/255, alpha: 1)
        
        self.addSubview(img)
        self.titleLabel = UILabel()
        self.titleLabel?.textColor = UIColor.init(red: 181/255, green: 176/255, blue: 253/255, alpha: 1)
        self.titleLabel?.text = NSLocalizedString("Rate us", comment: "")
        self.titleLabel?.font = HSFont.baseRegularFont(20)
        self.cornerBackView?.addSubview(titleLabel!)
//        self.cornerBackView?.addSubview(divideView!)
        
        self.bottomDivideView = UIView()
        self.bottomDivideView?.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.cornerBackView?.addSubview(self.bottomDivideView!)
        
//        self.divideView?.snp.makeConstraints(closure: { (make) in
//            make.top.equalTo(self.titleLabel!.snp.bottom).offset(5)
//            make.left.equalTo(self.cornerBackView!.snp.left).offset(BaseCardCell.cellPadding)
//            make.right.equalTo(self.cornerBackView!.snp.right).offset(-BaseCardCell.cellPadding)
//            make.height.equalTo(0.3)
//        })
        
        self.bottomDivideView?.snp.makeConstraints({ (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(self.snp.bottom)
            make.height.equalTo(10)
        })
        
        self.titleLabel?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(self.bottomDivideView!.snp.top).offset(-12)
            make.left.equalTo(self.cornerBackView!.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-BaseCardCell.cellPadding)
         })
        
        self.cornerBackView?.backgroundColor = UIColor.basePurpleBackgroundColor()
        
        img.snp.makeConstraints { (make) in
            make.top.equalTo(self.cornerBackView!.snp.top).offset(12)
            make.left.equalTo(self.cornerBackView!.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-BaseCardCell.cellPadding)
            make.bottom.equalTo(self.titleLabel!.snp.top).offset(-12)
        }
    }
    
    @objc func rateButtonOnClick(_ sender:UIButton) {
         if sender.tag-10 == 4 {
            //go AppStore
            AnaliticsManager.sendEvent(AnaliticsManager.rate_us_click, data: ["sign":"5"])
            let urlString = "itms-apps://itunes.apple.com/app/id1184938845"
            let url = URL(string: urlString)
            UIApplication.shared.openURL(url!)
            let notiCenter = NotificationCenter.default
            notiCenter.post(name: Notification.Name(rawValue: "rateButtonOnClick"), object: nil)
            return
           }else{
            AnaliticsManager.sendEvent(AnaliticsManager.rate_us_click, data: ["sign":"\(sender.tag-9)"])
            let rateDetailVC = RateDetailViewController()
            rateDetailVC.starRank = sender.tag+200-10
            self.rootvc?.navigationController?.pushViewController(rateDetailVC, animated: true)
        }
        UserDefaults.standard.set(true, forKey: "hasRate")
        UserDefaults.standard.synchronize()
        for view in (self.subviews){
            view.removeFromSuperview()
        }
        if let vc = self.rootvc as? HomeTimeLineViewController{
            vc.baseTableView?.reloadData()
        }
    }
    
    var model:BaseCardListModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        if self.model != nil {
            if let authorImaStr = model?.figureImg {
               img.sd_setImage(with: URL(string:authorImaStr), placeholderImage: UIImage(named:"defaultImg1.jpg"))
             }
        }
    }
    
    func createTimer() {
        if UserDefaults.standard.bool(forKey: "hasRate") == false {
        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(fullTheStar), userInfo: nil, repeats: true)
        }
    }
    
    @objc func fullTheStar() {
        if self.flag == false {
            if self.buttons.count > 0 {
                if self.index == 0{
                    for i in 1 ..< self.buttons.count {
                        self.buttons[i].alpha = 0
                        self.bgButtons[i].alpha = 1
                    }
                }
                if self.buttons.count > self.index {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.buttons[self.index].alpha = 1
                        self.bgButtons[self.index].alpha = 0
                    })
                    self.index += 1
                    if self.index >= 5 {
                        self.index = 0
                        self.flag = true
                        UIView.animate(withDuration: 0.4, animations: {
                            }, completion: { (finished:Bool) in
                                UIView.animate(withDuration: 0.4, animations: {
                                    }, completion: { (finished:Bool) in
                                        self.flag = false
                                })
                        })
                    }
                } else {
                    index = 0
                }
            }
        }
    }
    
    class func calculateHeight() ->CGFloat {
        if UserDefaults.standard.bool(forKey: "hasRate") == true {
        return 0
        }else{
        return (UIScreen.main.bounds.size.width-2*BaseCardCell.cellPadding)/16 * 9 + 45 + 20
        }
     }
    
    
    static let cellId="RateUsCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> RateUsCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! RateUsCell
        return cell
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
