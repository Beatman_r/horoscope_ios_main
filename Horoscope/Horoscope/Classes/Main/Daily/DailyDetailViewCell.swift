//
//  DailyDetailViewCell.swift
//  Horoscope
//
//  Created by Beatman on 16/11/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class DailyDetailViewCell: BaseCardCell {
    
    var dailyContentLabel = UILabel()
    var zodiacCardView = UIView()
    
     override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupCellUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCellUI() {
        self.dailyContentLabel = UILabel(frame: CGRect(x: 15, y: 44, width: UIScreen.main.bounds.width-30, height: 100))
        self.dailyContentLabel.numberOfLines = 0
        self.dailyContentLabel.font = HSFont.charterRegularFont(18)
        self.contentView.addSubview(self.cornerBackView!)
        self.cornerBackView?.addSubview(dailyContentLabel)
        self.cornerBackView?.addSubview(nameAndAvaBackview)
    }
    
    let nameAndAvaBackview:UIView = {
        let view = UIView()
        view.backgroundColor=UIColor.cardBackColor()
        view.frame = CGRect(x: 0, y: 44, width: UIScreen.main.bounds.size.width-2*BaseCardCell.cellPadding, height: 64)
        return view
    }()
    
    class func getHeightForCell(_ descripeString:String,width:CGFloat,font:UIFont) -> CGFloat{
        
        let normalText : NSString = descripeString as NSString
        let size = CGSize(width: width, height: 1000)
        let dic = NSDictionary(object: font, forKey: NSAttributedStringKey.font as NSCopying)
        let stringSize = normalText.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: dic as? [NSAttributedStringKey:AnyObject], context: nil).size
        return stringSize.height + 25
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
