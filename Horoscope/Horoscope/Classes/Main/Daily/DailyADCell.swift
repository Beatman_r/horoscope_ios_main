//
//  DailyADCell.swift
//  Horoscope
//
//  Created by Beatman on 16/12/19.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class DailyADCell: UICollectionViewCell {
    
    var ADView : UIView?
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    override init(frame: CGRect) {
        super.init(frame: frame)
        let width = (screenWidth-30)/2
        let height = (screenWidth-30)/2+(screenWidth-30)/6
        ADView?.frame = CGRect(x: 0, y: 0, width: width, height: height)
        ADView?.backgroundColor = UIColor.clear
        ADView?.layer.cornerRadius = 12
        ADView?.layer.masksToBounds = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func dequeueReusableCellWithReuseIdentifier(_ collection:UICollectionView, forIndexPath indexPath: IndexPath, vc:UIViewController) -> DailyADCell{
        let cellID = "DailyADCell"
        let cell:DailyADCell? = collection.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as? DailyADCell
        return cell!
    }
}
