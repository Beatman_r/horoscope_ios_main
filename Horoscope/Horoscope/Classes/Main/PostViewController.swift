//
//  PostViewController.swift
//  Horoscope
//
//  Created by Beatman on 17/2/6.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import YYText
import PKHUD

@objc protocol PostViewControllerDelegate {
    @objc optional func postCommentSuccess(_ comment:MainComment)
    @objc optional func postCommentFailure(_ error:NSError)
}

class PostViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UIScrollViewDelegate {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var isReply = false
    var isFirstClassReply = false
    var model:BaseComment?
    var postModel : ReadPostModel?
    var replyModel : MyPostModel?
    var userInfo : UserInfo?
    var isForcastReply = false
    var horoscopeName : String? = ""
    var forecastName : String? = ""
    weak var delegate:PostViewControllerDelegate?
    
    var tagList : [String] = []
    var emojiList : [String] = []
    var loadSuccess : Bool = false
    var zodiacList : [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        AnaliticsManager.sendEvent(AnaliticsManager.post_show)
        let userDefalt = UserDefaults.standard
        userDefalt.setValue(["#Zodiac","#Aries","#Taurus","#Gemini","#Cancer","#Leo","#Virgo","#Lbra","#Scorpio","#Sagittarius","#Capricorn","#Aquarius","#Pisces"], forKey: LOCAL_TAG_LIST)
        userDefalt.synchronize()
        userDefalt.setValue(["http://img.idailybread.com/horoscope/publishExtra/staticImage/1.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/10.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/11.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/12.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/13.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/14.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/2.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/3.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/4.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/5.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/6.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/7.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/8.jpg", "http://img.idailybread.com/horoscope/publishExtra/staticImage/9.jpg"], forKey: LOCAL_EMOJI_LIST)
        userDefalt.synchronize()
        
        self.zodiacList = ZodiacRecordManager.getSelectedZodiac()
        
        if self.isReply == true || self.isFirstClassReply == true{
            if self.replyModel != nil {
                self.postModel = ReadPostModel(model: self.replyModel!)
            }
        }
        self.userInfo = AccountManager.sharedInstance.getUserInfo()
        self.avatarIV.layer.cornerRadius = self.screenWidth * 16.5/375
        self.avatarIV.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.white
        self.view.backgroundColor = UIColor.basePurpleBackgroundColor()
        self.createMainView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name:.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name:.UIKeyboardWillHide, object: nil)
        HoroscopeDAO().getEmojiAndTagList({ (tagList, emojiList) in
            self.tagList = tagList
            self.emojiList = emojiList
            self.loadSuccess = true
            UserDefaults.standard.setValue(self.tagList, forKey: LOCAL_TAG_LIST)
            UserDefaults.standard.setValue(self.emojiList, forKey: LOCAL_EMOJI_LIST)
            UserDefaults.standard.synchronize()
        }) { (error) in
            print(error)
        }
        let delayTime = DispatchTime.now() + Double(Int64(0.6 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.mainTF.becomeFirstResponder()
        }
    }
    
    let avatarIV : UIImageView = {
        let avatarIV = UIImageView()
        avatarIV.backgroundColor = UIColor.clear
        return avatarIV
    }()
    
    let zodiacLabel : UILabel = {
        let zodiacLabel = UILabel()
        zodiacLabel.text = "MySign"
        zodiacLabel.textAlignment = NSTextAlignment.left
        zodiacLabel.font = HSFont.baseRegularFont(baseBigFontScale)
        zodiacLabel.textColor = UIColor.pubTimeColor()
        return zodiacLabel
    }()
    
    let nameLabel : UILabel = {
        let nameLabel = UILabel()
        nameLabel.text = "MyName"
        nameLabel.textAlignment = NSTextAlignment.left
        nameLabel.font = HSFont.baseRegularFont(baseBigFontScale)
        nameLabel.textColor = UIColor.luckyPurpleTitleColor()
        return nameLabel
    }()
    
    let mainTF : UITextView = {
        let mainTF = UITextView()
//        mainTF.placeholderFont = HSFont.baseRegularFont(baseBigFontScale)
//        mainTF.placeholderTextColor = UIColor.luckyPlaceHolderColor()
        mainTF.font = HSFont.baseRegularFont(baseBigFontScale)
        mainTF.textColor = UIColor.luckyPurpleTitleColor()
        mainTF.backgroundColor = UIColor.clear
        return mainTF
    }()
    
    let tagLabel : YYLabel = {
        let tagLabel = YYLabel()
        tagLabel.text = ""
        tagLabel.textColor = UIColor.pubTimeColor()
        tagLabel.font = HSFont.baseRegularFont(baseBigFontScale)
        tagLabel.textAlignment = NSTextAlignment.left
        return tagLabel
    }()
    
    let tipLabel : UILabel = {
        let tipLabel = UILabel()
        tipLabel.text = "Select Tag (must)"
        tipLabel.textAlignment = NSTextAlignment.right
        tipLabel.font = HSFont.baseRegularFont(baseBigFontScale)
        tipLabel.textColor = UIColor.luckyPurpleTitleColor()
        return tipLabel
    }()
    
    let selectTagButton : UIButton = {
        let selectTagButton = UIButton()
        selectTagButton.setImage(UIImage(named: "right_"), for: UIControlState())
        return selectTagButton
    }()
    
    let tagView : UIView = {
        let tagView = UIView()
        tagView.backgroundColor = UIColor.basePurpleBackgroundColor()
        return tagView
    }()
    let tagViewDivide : UIView = {
        let tagViewDivide = UIView()
        tagViewDivide.backgroundColor = UIColor.divideColor()
        return tagViewDivide
    }()
    
    let anonymousLabel : UILabel = {
        let anonymousLabel = UILabel()
        anonymousLabel.text = "Anonymous"
        anonymousLabel.textColor = UIColor.luckyPurpleTitleColor()
        anonymousLabel.textAlignment = NSTextAlignment.left
        anonymousLabel.font = HSFont.baseRegularFont(baseBigFontScale)
        return anonymousLabel
    }()
    
    let anonymousButton : UIButton = {
        let anonymousButton = UIButton()
        anonymousButton.setImage(UIImage(named: "anonymous_choose_"), for: UIControlState())
        return anonymousButton
    }()
    
    var anonymousPost : Bool = false
    
    let anonymousView : UIView = {
        let anonymousView = UIView()
        anonymousView.backgroundColor = UIColor.basePurpleBackgroundColor()
        return anonymousView
    }()
    
    let closeKBButton : UIButton = {
        let closeKBButton = UIButton()
        closeKBButton.setImage(UIImage(named: "tag_under_"), for: UIControlState())
        return closeKBButton
    }()
    
    let photoSelector : UIButton = {
        let photoSelector = UIButton()
        photoSelector.setImage(UIImage(named: "camera_"), for: UIControlState())
        return photoSelector
    }()
    
    let emojiPicker : UIButton = {
        let emojiPicker = UIButton()
        emojiPicker.setImage(UIImage(named: "expression_"), for: UIControlState())
        return emojiPicker
    }()
    
    let inputToolBar : UIView = {
        let inputToolBar = UIView()
        inputToolBar.backgroundColor = UIColor(hexString: "#251132")
        return inputToolBar
    }()
    
    let imageArea : UIImageView = {
        let imageArea = UIImageView()
        imageArea.layer.masksToBounds = true
        imageArea.contentMode = UIViewContentMode.scaleAspectFill
        imageArea.isUserInteractionEnabled = true
        return imageArea
    }()
    
    let imgDeleteBtn : UIButton = {
        let imgDeleteBtn = UIButton()
        imgDeleteBtn.setImage(UIImage(named: "post_delete_"), for: UIControlState())
        imgDeleteBtn.isHidden = true
        return imgDeleteBtn
    }()
    
    let replyContent : UILabel = {
        let replyContent = UILabel()
        replyContent.textColor = UIColor.luckyPurpleReplyColor()
        replyContent.font = HSFont.baseRegularFont(baseBigFontScale)
        replyContent.textAlignment = NSTextAlignment.left
        replyContent.numberOfLines = 0
        return replyContent
    }()
    
    let replyDivideView : UIView = {
        let replyDivideView = UIView()
        replyDivideView.alpha = 0.4
        replyDivideView.backgroundColor = UIColor.init(red: 88/255, green: 88/255, blue: 88/255, alpha: 1)
        return replyDivideView
    }()
    
    var postButton = UIButton()
    
    //MARK: function area
    func createMainView() {
        self.tagList = UserDefaults.standard.value(forKey: LOCAL_TAG_LIST) as! [String]
        self.emojiList = UserDefaults.standard.value(forKey: LOCAL_EMOJI_LIST) as! [String]
        if self.isReply == true {
            self.addReplySubViews()
            self.addReplyConstraints()
            self.replyContent.text = self.model?.content
        }else if self.isFirstClassReply == true {
            self.addFirstReplySubviews()
            self.addFirstClassReplyConstraints()
        }else {
            self.addSubViews()
            self.addConstraints()
            self.createTagView()
        }
        self.mainTF.delegate = self
        self.createEmojiView()
        photoSelector.addTarget(self, action: #selector(goSelectPhoto), for: .touchUpInside)
        anonymousButton.addTarget(self, action: #selector(anonymousButtonAction), for: .touchUpInside)
        closeKBButton.addTarget(self, action: #selector(closeKeyBoard), for: .touchUpInside)
        emojiPicker.addTarget(self, action: #selector(pullUpEmojiView), for: .touchUpInside)
        imgDeleteBtn.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
        selectTagButton.addTarget(self, action: #selector(pullUpTagView), for: .touchUpInside)
        
        self.tagLabel.text = "#\(CURRENT_MAIN_SIGN?.capitalized ?? "")"
        if self.zodiacList.count > 0 {
            self.tagNameList.append(CURRENT_MAIN_SIGN?.lowercased() ?? "")
            self.tagDisplayList.append("#\(CURRENT_MAIN_SIGN?.capitalized ?? "")")
        }
        self.nameLabel.text = userInfo?.userName ?? "UserName"
        self.zodiacLabel.text = userInfo?.horoname?.capitalized ?? "ZodiacName"
        let imageUrl = URL(string: userInfo?.avatar ?? "")
        self.avatarIV.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "icon_author"))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(pullUpTagView))
        self.tagView.addGestureRecognizer(tap)
        postButton = UIButton(type: .custom)
        postButton.frame = CGRect(x: 0, y: 0, width: 50, height: 20)
        postButton.setTitle("POST", for: UIControlState())
        postButton.setTitleColor(UIColor.gray, for: UIControlState())
        postButton.addTarget(self, action: #selector(postTopic), for: .touchUpInside)
        let rightBarItem : UIBarButtonItem = UIBarButtonItem(customView: postButton)
        self.navigationItem.rightBarButtonItem = rightBarItem
    }
    
    let devideView = UIView()
    let selectTagView = UIView()
    var tagButton : UIButton?
    func createTagView() {
        selectTagView.backgroundColor = UIColor.basePurpleBackgroundColor()
        self.view.addSubview(selectTagView)
        let tipLabel = UILabel()
        tipLabel.text = "Please select Tag"
        tipLabel.textColor = UIColor.luckyPurpleTitleColor()
        let saveButton = UIButton()
        saveButton.setTitle("SAVE", for: UIControlState())
        saveButton.setTitleColor(UIColor.luckyPurpleTitleColor(), for: UIControlState())
        saveButton.addTarget(self, action: #selector(removeTagView), for: .touchUpInside)
        devideView.backgroundColor = UIColor.purple
        devideView.alpha = 0.4
        devideView.backgroundColor = UIColor.init(red: 88/255, green: 88/255, blue: 88/255, alpha: 1)
        devideView.isHidden = true
        selectTagView.addSubview(tipLabel)
        selectTagView.addSubview(saveButton)
        selectTagView.addSubview(devideView)
        
        let buttonsView = UIView()
        buttonsView.backgroundColor = UIColor.basePurpleBackgroundColor()
        selectTagView.addSubview(buttonsView)
        
        selectTagView.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left)
            make.top.equalTo(self.view.snp.bottom)
            make.height.equalTo(self.screenHeight * 267/667)
            make.width.equalTo(self.view.snp.width)
        }
        tipLabel.snp.makeConstraints { (make) in
            make.top.equalTo(selectTagView.snp.top).offset(self.screenHeight * 10/667)
            make.height.equalTo(self.screenHeight * 30/667)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 25/375)
            make.right.equalTo(self.view.snp.centerX)
        }
        saveButton.snp.makeConstraints { (make) in
            make.top.equalTo(selectTagView.snp.top).offset(self.screenHeight * 10/667)
            make.bottom.equalTo(tipLabel.snp.top).offset(self.screenHeight * 30/667)
            make.right.equalTo(self.selectTagView.snp.right).offset(-self.screenWidth * 25/375)
            make.left.equalTo(self.selectTagView.snp.right).offset(-self.screenWidth * 100/375)
        }
        devideView.snp.makeConstraints { (make) in
            make.top.equalTo(saveButton.snp.bottom).offset(10)
            make.width.equalTo(self.view.snp.width).offset(-20)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.equalTo(1)
        }
        buttonsView.snp.makeConstraints { (make) in
            make.top.equalTo(devideView.snp.bottom).offset(5)
            make.bottom.equalTo(self.view.snp.bottom)
            make.width.equalTo(self.view.snp.width)
        }
        let count = self.loadSuccess == true ? self.tagList.count : 12
        self.tagList = UserDefaults.standard.value(forKey: LOCAL_TAG_LIST) as! [String]
        for sub in 0...count {
            tagButton = UIButton(type: .custom)
            if sub >= 1 {
                let isDefaultSign = ZodiacModel.getZodiacName(sub-1).lowercased() == CURRENT_MAIN_SIGN?.lowercased() ? true : false
                if isDefaultSign == true {
                    tagButton?.backgroundColor = UIColor(hexString: "#f95cd1")
                    tagButton?.setTitleColor(UIColor.white, for: UIControlState())
                    tagButton?.layer.borderColor = UIColor(hexString: "#f95cd1")?.cgColor
                    tagButton?.tag = 10
                }else {
                    tagButton?.backgroundColor = UIColor.clear
                    tagButton?.layer.borderColor = UIColor(hexString: "#f95cd1")?.cgColor
                    tagButton?.setTitleColor(UIColor(hexString: "#f95cd1"), for: .normal)
                }
            }else {
                tagButton?.backgroundColor = UIColor.clear
                tagButton?.layer.borderColor = UIColor(hexString: "#f95cd1")?.cgColor
                tagButton?.setTitleColor(UIColor(hexString: "#f95cd1"), for: .normal)
            }
            tagButton?.layer.borderWidth = 1
            tagButton?.layer.cornerRadius = 4
            tagButton?.layer.masksToBounds = true
            tagButton?.titleLabel?.font = HSFont.baseLightRegularFont(15)
            tagButton?.addTarget(self, action: #selector(tagButtonAction(_:)), for: .touchUpInside)
            tagButton?.setTitle(self.tagList[sub], for: UIControlState())
            tagButton?.frame = CGRect(x: CGFloat(sub)*(self.screenWidth-40)/3 + 20, y: 30, width: (self.screenWidth-40)/3-5, height: self.screenWidth == 320 ? 30 : 34)
            if sub >= 0 && sub <= 2 {
                tagButton?.frame.origin.x = CGFloat(sub)*(self.screenWidth-40)/3 + 25
                tagButton?.frame.origin.y = self.screenWidth == 320 ? 0 : 14
            }else if sub >= 3 && sub <= 5{
                tagButton?.frame.origin.x = CGFloat(sub-3)*(self.screenWidth-40)/3 + 25
                tagButton?.frame.origin.y = self.screenWidth == 320 ? 35 : 54
            }else if sub >= 6 && sub <= 8{
                tagButton?.frame.origin.x = CGFloat(sub-6)*(self.screenWidth-40)/3 + 25
                tagButton?.frame.origin.y = self.screenWidth == 320 ? 70 : 94
            }else if sub >= 9 && sub <= 11{
                tagButton?.frame.origin.x = CGFloat(sub-9)*(self.screenWidth-40)/3 + 25
                tagButton?.frame.origin.y = self.screenWidth == 320 ? 105 : 134
            }else {
                tagButton?.frame.origin.x = 25
                tagButton?.frame.origin.y = self.screenWidth == 320 ? 145 : 174
            }
            tagButton?.tag = sub
            buttonsView.addSubview(tagButton!)
        }
    }
    
    var tagNameList : [String] = []
    var tagDisplayList : [String] = []
    @objc func tagButtonAction(_ sender:UIButton) {
        if sender.backgroundColor == UIColor.clear{
            sender.backgroundColor = UIColor(hexString: "#f95cd1")
            sender.setTitleColor(UIColor.white, for: UIControlState())
            sender.layer.borderColor = UIColor(hexString: "#f95cd1")?.cgColor
            if sender.tag == 0 {
                let tagName = "zodiac"
                self.tagNameList.append(tagName)
                let tagDisplayName = "#Zodiac"
                self.tagDisplayList.append(tagDisplayName)
            }else {
                let tagName = ZodiacModel.getZodiacName(sender.tag-1)
                self.tagNameList.append(tagName)
                let tagDisplayName = "#" + ZodiacModel.getZodiacName(sender.tag-1).capitalized
                self.tagDisplayList.append(tagDisplayName)
            }
        }else if sender.backgroundColor == UIColor(hexString: "#f95cd1") {
            sender.backgroundColor = UIColor.clear
            sender.setTitleColor(UIColor(hexString: "#f95cd1"), for: .normal)
            sender.layer.borderColor = UIColor(hexString: "#f95cd1")?.cgColor
            var removeSub = 0
            if sender.tag == 0 {
                for sub in 0..<self.tagNameList.count {
                    if self.tagNameList[sub] == "#Zodiac" {
                        removeSub = sub
                    }
                }
                if self.tagNameList.count == 1 {
                    self.tagNameList.removeLast()
                    self.tagDisplayList.removeLast()
                }else {
                    self.tagNameList.remove(at: removeSub+1)
                    self.tagDisplayList.remove(at: removeSub+1)
                }
            }else {
                for sub in 0..<self.tagNameList.count {
                    if self.tagNameList[sub] == ZodiacModel.getZodiacName(sender.tag-1) {
                        removeSub = sub
                    }
                }
                if self.tagNameList.count == 1 {
                    self.tagNameList.removeLast()
                    self.tagDisplayList.removeLast()
                }else {
                    self.tagNameList.remove(at: removeSub)
                    self.tagDisplayList.remove(at: removeSub)
                }
            }
        }
        print(self.tagNameList)
    }
    
    @objc func anonymousButtonAction() {
        if self.anonymousPost == false {
            self.anonymousPost = true
            self.anonymousButton.setImage(UIImage(named: "anonymous_choosen_"), for: UIControlState())
        }else if self.anonymousPost == true {
            self.anonymousPost = false
            self.anonymousButton.setImage(UIImage(named: "anonymous_choose_"), for: UIControlState())
        }
    }
    
    @objc func pullUpTagView() {
        self.mainTF.resignFirstResponder()
//        self.mainTF.placeholderText = "What's on your mind?"
        self.devideView.isHidden = false
        UIView.animate(withDuration: 0.4, animations: {
            self.selectTagView.snp.updateConstraints({ (make) in
                make.top.equalTo(self.view.snp.bottom).offset(-self.screenHeight * 270/667)
            })
            self.view.layoutIfNeeded()
        }, completion: { (true) in
            
        }) 
    }
    @objc func removeTagView() {
        self.devideView.isHidden = true
        if self.mainTF.text.count < 1000 && self.tagNameList.count > 0 && self.mainTF.text.count > 1{
            self.postButton.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        }
        UIView.animate(withDuration: 0.4, animations: {
            self.selectTagView.snp.updateConstraints({ (make) in
                make.top.equalTo(self.view.snp.bottom)
            })
            self.view.layoutIfNeeded()
        }, completion: { (true) in
            self.tagLabel.textColor = UIColor.commonPinkColor()
            if self.zodiacList.count > 0 {
                self.tagLabel.text = self.tagNameList.count>1 ? "#\(self.tagNameList.first?.capitalized ?? "")..." : "#\(self.tagNameList.first?.capitalized ?? "")"
            }else {
                self.tagLabel.text = ""
            }
            
        }) 
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        self.emojiView.isHidden = true
        if self.isReply == false && self.isFirstClassReply == false{
            self.removeTagView()
        }
        let keyboardheight:CGFloat = ((notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height)!
        self.inputToolBar.snp.remakeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom).offset(-keyboardheight-(self.screenHeight*50/667))
            make.height.equalTo(self.screenHeight * 50/667)
            make.width.equalTo(self.view.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        self.view.layoutIfNeeded()
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        self.inputToolBar.snp.remakeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom)
            make.height.equalTo(self.screenHeight * 50/667)
            make.width.equalTo(self.view.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        self.view.layoutIfNeeded()
    }
    
    //MARK: EmojiView
    let emojiView = UIView()
    let emojiDivideView = UIView()
    let emojiPageControl = UIPageControl()
    let emojiNameList : [String] = ["horo_emoji_1_","horo_emoji_2_","horo_emoji_3_","horo_emoji_4_","horo_emoji_5_","horo_emoji_6_","horo_emoji_7_","horo_emoji_8_","horo_emoji_9_","horo_emoji_10_","horo_emoji_11_","horo_emoji_12_","horo_emoji_12_","horo_emoji_12_"]
    func createEmojiView() {
        let scrollView = UIScrollView()
        let count = self.emojiList.count == 3 ? Int(self.emojiList.count/3) : Int(self.emojiList.count/3 + 1)
        scrollView.frame = CGRect(x: 0, y: 0, width: self.screenWidth, height: 0)
        scrollView.contentSize = CGSize(width: Int(screenWidth)*count, height: 0)
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        scrollView.backgroundColor = UIColor.basePurpleBackgroundColorDark()
        self.emojiView.addSubview(scrollView)
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.emojiView.snp.edges)
        }
        emojiPageControl.numberOfPages = count
        emojiPageControl.currentPage = 0
        emojiPageControl.backgroundColor = UIColor.clear
        self.emojiView.addSubview(emojiPageControl)
        self.emojiPageControl.snp.makeConstraints { (make) in
            make.width.equalTo(self.emojiView.snp.width)
            make.bottom.equalTo(self.emojiView.snp.bottom).offset(-self.screenHeight * 30/667)
            make.height.equalTo(self.screenHeight * 10/667)
            make.centerX.equalTo(self.emojiView.snp.centerX)
        }
        emojiDivideView.alpha = 0.4
        emojiDivideView.backgroundColor = UIColor.init(red: 88/255, green: 88/255, blue: 88/255, alpha: 1)
        emojiDivideView.isHidden = false
        self.emojiView.addSubview(emojiDivideView)
        self.emojiDivideView.snp.makeConstraints { (make) in
            make.top.equalTo(self.emojiView.snp.top).offset(self.screenHeight * 5/667)
            make.left.equalTo(self.emojiView.snp.left).offset(self.screenWidth * 25/375)
            make.right.equalTo(self.emojiView.snp.right).offset(-self.screenWidth * 25/375)
            make.bottom.equalTo(self.emojiDivideView.snp.top).offset(1)
        }
        for count in 0...self.emojiList.count-1 {
            let emojiButton = UIButton()
            emojiButton.tag = count
            emojiButton.sd_setImage(with: URL(string: self.emojiList[count]), for: UIControlState(), placeholderImage: UIImage(named: self.emojiNameList[count]))
            //            emojiButton.setImage(UIImage(named: self.emojiNameList[count]), forState: .Normal)
            emojiButton.frame = CGRect(x: CGFloat(count)*(self.screenWidth-24)/3, y: 30, width: (self.screenWidth-40)/3-5, height: (self.screenWidth-40)/3-5)
            emojiButton.addTarget(self, action: #selector(addEmoji(_:)), for: .touchUpInside)
            scrollView.addSubview(emojiButton)
        }
    }
    
    @objc func addEmoji(_ sender:UIButton) {
        self.imageArea.sd_setImage(with: URL(string: self.emojiList[sender.tag]), placeholderImage: UIImage(named: self.emojiNameList[sender.tag]))
        //        self.imageArea.image = UIImage(named: self.emojiNameList[sender.tag])
        self.scaledImg = self.imageArea.image
        self.imgDeleteBtn.isHidden = false
    }
    
    @objc func pullUpEmojiView() {
        self.mainTF.resignFirstResponder()
        self.emojiView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.emojiView.snp.remakeConstraints { (make) in
                make.top.equalTo(self.view.snp.bottom).offset(-self.screenHeight * 200/667)
                make.width.equalTo(self.view.snp.width)
                make.height.equalTo(self.screenHeight * 200/667)
                make.centerX.equalTo(self.view.snp.centerX)
            }
            self.inputToolBar.snp.remakeConstraints({ (make) in
                make.top.equalTo(self.emojiView.snp.top).offset(-self.screenHeight * 50/667)
                make.height.equalTo(self.screenHeight * 50/667)
                make.width.equalTo(self.view.snp.width)
                make.centerX.equalTo(self.view.snp.centerX)
            })
            self.view.layoutIfNeeded()
        }) 
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let viewPage = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        self.emojiPageControl.currentPage = viewPage
    }
    
    func removeEmojiView() {
        self.emojiView.isHidden = true
        self.mainTF.becomeFirstResponder()
        UIView.animate(withDuration: 0.3, animations: {
            self.emojiView.snp.updateConstraints({ (make) in
                make.top.equalTo(self.view.snp.bottom).offset(self.screenHeight * 50/667)
            })
            self.view.layoutIfNeeded()
        }) 
    }
    
    //MARK: PhotoSelect
    @objc func goSelectPhoto() {
        self.mainTF.resignFirstResponder()
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (UIAlertAction) in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        }
        let photoAction = UIAlertAction(title: "PhotoLibrary", style: .default) { (UIAlertAction) in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        }
        alertController.addAction(cameraAction)
        alertController.addAction(photoAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    var scaledImg : UIImage?
    var hasImg : Bool = false
    //MARK: imagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image : UIImage!
        image = info[UIImagePickerControllerOriginalImage]as!UIImage
        scaledImg = self.scaleImage(image, size: CGSize(width: image.size.width/2, height: image.size.height/2))
        //        self.insertImage(scaledImg)
        self.view.layoutIfNeeded()
        self.imageArea.image = image
        self.imgDeleteBtn.isHidden = false
        self.hasImg = true
        self.postButton.titleLabel?.textColor = UIColor.commonPinkColor()
        //        uploadImage(scaledImg!)
        picker.dismiss(animated: true, completion: {() in
            self.mainTF.becomeFirstResponder()
        })
    }
    
    func scaleImage(_ image:UIImage,size:CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let scaledImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImg!
    }
    
    @objc func deleteImage() {
        self.imageArea.image = UIImage(named: "")
        self.imageList.removeAll()
        self.scaledImg = nil
        self.imgDeleteBtn.isHidden = true
    }
    
    func insertImage(_ image:UIImage) {
        let text = NSMutableAttributedString()
        let font = HSFont.baseRegularFont(18)
        let content = self.mainTF.text
        text.append(NSAttributedString(string: content!, attributes: nil))
        let insertImage = UIImage(cgImage: image.cgImage!)
        let attachText = NSMutableAttributedString.yy_attachmentString(withContent: insertImage, contentMode: UIViewContentMode.center, attachmentSize: insertImage.size, alignTo: font, alignment: YYTextVerticalAlignment.center)
        text.append(attachText)
        self.mainTF.attributedText = text
    }
    
    var postableStatus : Bool = false
    var imageList : [String] = []
    
    func uploadImage(_ uploadImage : UIImage){
        self.imageList.removeAll()
        HoroscopeDAO.uploadImageDAO(uploadImage, success: { (url) in
            let recieveUrl = url
            self.postableStatus = true
            self.imageList.append(recieveUrl)
        }) { (error) in
            print(error)
        }
    }
    
    //MARK: POST
    
    @objc func postTopic() {
        self.mainTF.isEditable = false
        self.postButton.isEnabled = false
        if self.isReply == false && self.isFirstClassReply == false {
            if self.mainTF.text.count < 1 {
                HUD.flash(.label("One word at least Plz..."), delay: 1)
                self.mainTF.isEditable = true
                self.postButton.isEnabled = true
            }
            if (self.tagNameList.count > 0 && self.mainTF.text.count < 1000 && self.mainTF.text.count > 0) || (self.tagNameList.count > 0 && self.mainTF.text.count < 1000 && self.hasImg == true){
                self.imageList.removeAll()
                if scaledImg != nil {
                    HUD.flash(.label("Publishing your post..."),delay: 10)
                    HoroscopeDAO.uploadImageDAO(scaledImg!, success: { (url) in
                        let recieveUrl = url
                        self.postableStatus = true
                        self.imageList.append(recieveUrl)
                        HoroscopeDAO().postTopic(self.mainTF.text, imageList: self.imageList, tagList: self.tagNameList, anonymous: self.anonymousPost, success: { (model) in
                            HUD.flash(.success,delay: 1)
                            model.type = .normal
                            let insertModel : ReadPostModel = model
                            let notiCenter = NotificationCenter.default
                            notiCenter.post(name: Notification.Name(rawValue: "topicDidPost"), object: insertModel)
                            let isAnonymous = self.anonymousPost == true ? "anonymous" : "users"
                            AnaliticsManager.sendEvent(AnaliticsManager.post_succeed, data: ["type":isAnonymous])
                            _ = self.navigationController?.popViewController(animated: true)
                            }, failure: { (error) in
                                print(error)
                                HUD.flash(.label("Public failure"),delay: 1)
                                self.mainTF.isEditable = true
                                self.postButton.isEnabled = true
                        })
                    }) { (error) in
                        print(error)
                        HUD.flash(.label("Public failure"),delay: 1)
                        self.mainTF.isEditable = true
                        self.postButton.isEnabled = true
                    }
                }else {
                    HUD.flash(.label("Publishing your post..."),delay: 10)
                    HoroscopeDAO().postTopic(self.mainTF.text, imageList: self.imageList, tagList: self.tagNameList, anonymous: self.anonymousPost, success: { (model) in
                        HUD.flash(.success,delay: 1)
                        model.type = .normal
                        let insertModel : ReadPostModel = model
                        let notiCenter = NotificationCenter.default
                        notiCenter.post(name: Notification.Name(rawValue: "topicDidPost"), object: insertModel)
                        let isAnonymous = self.anonymousPost == true ? "anonymous" : "users"
                        AnaliticsManager.sendEvent(AnaliticsManager.post_succeed, data: ["type":isAnonymous])
                        _ = self.navigationController?.popViewController(animated: true)
                        }, failure: { (error) in
                            print(error)
                            HUD.flash(.label("Public failure"),delay: 1)
                            self.mainTF.isEditable = true
                            self.postButton.isEnabled = true
                    })
                }
            }else if self.tagNameList.count == 0 {
                HUD.flash(.label("Must select a Tag"), delay: 1)
                self.mainTF.isEditable = true
                self.postButton.isEnabled = true
            }else if self.mainTF.text.count > 1000 {
                HUD.flash(.label("limit: 1000 characters"),delay: 1)
                self.mainTF.isEditable = true
                self.postButton.isEnabled = true
            }
        }else if self.isReply == true || self.isFirstClassReply == true {
            var spaceFlag : Bool = false
            if self.mainTF.text.first == " " && self.mainTF.text.count == 1 {
                spaceFlag = true
                HUD.flash(.label("One word at least Plz..."),delay: 1)
                self.mainTF.isEditable = true
                self.postButton.isEnabled = true
            }else if self.mainTF.text.count == 0 {
                HUD.flash(.label("One word at least Plz..."),delay: 1)
                self.mainTF.isEditable = true
                self.postButton.isEnabled = true
            }else if self.mainTF.text.first != " " && self.mainTF.text.count > 0 {
                spaceFlag = false
                self.mainTF.isEditable = true
                self.postButton.isEnabled = true
            }
            if self.mainTF.text.count < 1000 && self.mainTF.text.count > 0 && spaceFlag == false{
                self.imageList.removeAll()
                if scaledImg != nil {
                    HUD.flash(.label("Publishing your comment..."),delay: 10)
                    HoroscopeDAO.uploadImageDAO(scaledImg!, success: { (url) in
                        let recieveUrl = url
                        self.postableStatus = true
                        self.imageList.append(recieveUrl)
                        if self.isForcastReply == true{
                            HoroscopeDAO.sharedInstance.postForcastContent(self.mainTF.text, postId: self.postModel?.postId ?? "", imageList: self.imageList, parent: self.model?.commentId ?? "", anonymous: self.anonymousPost, horoscopeName: self.horoscopeName ?? "", forecastName: self.forecastName ?? "", success: { (comment) in
                                self.delegate?.postCommentSuccess?(comment)
                                HUD.flash(.success,delay: 1)
                                _ = self.navigationController?.popViewController(animated: true)
                                }, failure: { (error) in
                                    self.delegate?.postCommentFailure?(error)
                                    _ = self.navigationController?.popViewController(animated: true)
                            })
                        }else{
                            HUD.flash(.label("Publishing your comment..."),delay: 10)
                            HoroscopeDAO().postContent(self.mainTF.text, postId: self.postModel?.postId ?? "", imageList: self.imageList, parent: self.model?.commentId ?? "", anonymous: self.anonymousPost, success: { (comment) in
                                
                                self.delegate?.postCommentSuccess?(comment)
                                HUD.flash(.success,delay: 1)
                                _ = self.navigationController?.popViewController(animated: true)
                                
                                }, failure: { (error) in
                                    self.delegate?.postCommentFailure?(error)
                                    _ = self.navigationController?.popViewController(animated: true)
                            })
                        }
                        
                    }) { (error) in
                        print(error)
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }else {
                    var spaceFlag : Bool = false
                    if self.mainTF.text.first == " " && self.mainTF.text.count == 1 {
                        spaceFlag = true
                        HUD.flash(.label("One word at least Plz..."),delay: 1)
                        self.mainTF.isEditable = true
                        self.postButton.isEnabled = true
                    }else if self.mainTF.text.count == 0 {
                        HUD.flash(.label("One word at least Plz..."),delay: 1)
                        self.mainTF.isEditable = true
                        self.postButton.isEnabled = true
                    }
                    if isForcastReply == true && spaceFlag == false{
                        HUD.flash(.label("Publishing your comment..."),delay: 10)
                        HoroscopeDAO.sharedInstance.postForcastContent(self.mainTF.text, postId: self.postModel?.postId ?? "", imageList: self.imageList, parent: self.model?.commentId ?? "", anonymous: self.anonymousPost, horoscopeName: self.horoscopeName ?? "", forecastName: self.forecastName ?? "", success: { (comment) in
                            self.delegate?.postCommentSuccess?(comment)
                            HUD.flash(.success,delay: 1)
                            _ = self.navigationController?.popViewController(animated: true)
                            }, failure: { (error) in
                                print(error)
                                self.delegate?.postCommentFailure?(error)
                                _ = self.navigationController?.popViewController(animated: true)
                        })
                    }else{
                        HUD.flash(.label("Publishing your comment..."),delay: 10)
                        HoroscopeDAO().postContent(self.mainTF.text, postId: self.postModel?.postId ?? "", imageList: self.imageList, parent: self.model?.commentId ?? "", anonymous: self.anonymousPost, success: { (comment) in
                            self.delegate?.postCommentSuccess?(comment)
                            HUD.flash(.success,delay: 1)
                            _ = self.navigationController?.popViewController(animated: true)
                            
                            }, failure: { (error) in
                                print(error)
                                self.delegate?.postCommentFailure?(error)
                                _ = self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }else if self.mainTF.text.count > 1000 {
                HUD.flash(.label("limit: 1000 characters"),delay: 1)
                self.mainTF.isEditable = true
                self.postButton.isEnabled = true
            }
        }
    }
    
    @objc func closeKeyBoard() {
        self.removeEmojiView()
        self.mainTF.resignFirstResponder()
    }
    
    //MARK: TextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        if self.mainTF.text.count < 1 {
            self.postButton.setTitleColor(UIColor.gray, for: UIControlState())
        }
        if self.mainTF.text.count > 1000 {
            HUD.flash(.label("limit: 1000 characters"), delay: 1)
        }
        if self.mainTF.text.count <= 1000 && self.tagNameList.count > 0 && self.mainTF.text.count > 0{
            self.postButton.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        }
        if self.mainTF.text.count == 1 && self.mainTF.text.first == " " {
            self.postButton.setTitleColor(UIColor.gray, for: UIControlState())
        }
        if self.isReply == true {
            if self.mainTF.text.count > 0 {
                self.postButton.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
            }else if self.mainTF.text.count == 0 {
                self.postButton.setTitleColor(UIColor.gray, for: UIControlState())
            }
        }
        let height = HSHelpCenter().textTool.calculateStringHeight(paramString: self.mainTF.text, fontSize: 18, font: HSFont.baseRegularFont(18), width: self.screenWidth-50, multyplyLineSpace: 1.25)
        if height <= 130 {
            self.mainTF.snp.updateConstraints { (make) in
                make.height.equalTo(height)
            }
        }
    }
    
    func addSubViews() {
        self.view.addSubview(avatarIV)
        self.view.addSubview(zodiacLabel)
        self.view.addSubview(nameLabel)
        self.view.addSubview(mainTF)
        self.view.addSubview(imageArea)
        self.view.addSubview(imgDeleteBtn)
        self.view.addSubview(tagView)
        tagView.addSubview(tagLabel)
        tagView.addSubview(tipLabel)
        tagView.addSubview(selectTagButton)
        tagView.addSubview(tagViewDivide)
        self.view.addSubview(anonymousView)
        anonymousView.addSubview(anonymousLabel)
        anonymousView.addSubview(anonymousButton)
        self.view.addSubview(inputToolBar)
        inputToolBar.addSubview(closeKBButton)
        inputToolBar.addSubview(photoSelector)
        inputToolBar.addSubview(emojiPicker)
        self.view.addSubview(emojiView)
    }
    
    func addReplySubViews() {
        self.view.addSubview(replyContent)
        self.view.addSubview(replyDivideView)
        self.view.addSubview(mainTF)
        self.view.addSubview(imageArea)
        self.view.addSubview(imgDeleteBtn)
        self.view.addSubview(anonymousView)
        anonymousView.addSubview(anonymousLabel)
        anonymousView.addSubview(anonymousButton)
        self.view.addSubview(inputToolBar)
        inputToolBar.addSubview(closeKBButton)
        inputToolBar.addSubview(photoSelector)
        inputToolBar.addSubview(emojiPicker)
        self.view.addSubview(emojiView)
    }
    
    func addFirstReplySubviews() {
        self.view.addSubview(avatarIV)
        self.view.addSubview(zodiacLabel)
        self.view.addSubview(nameLabel)
        self.view.addSubview(mainTF)
        self.view.addSubview(imageArea)
        self.view.addSubview(imgDeleteBtn)
        self.view.addSubview(anonymousView)
        anonymousView.addSubview(anonymousLabel)
        anonymousView.addSubview(anonymousButton)
        self.view.addSubview(inputToolBar)
        inputToolBar.addSubview(closeKBButton)
        inputToolBar.addSubview(photoSelector)
        inputToolBar.addSubview(emojiPicker)
        self.view.addSubview(emojiView)
    }
    
    func addConstraints() {
        self.avatarIV.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(self.screenHeight * 88/667)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 12/375)
            make.width.equalTo(self.screenWidth * 33/375)
            make.height.equalTo(self.screenWidth * 33/375)
        }
        self.nameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.avatarIV.snp.centerY)
            make.left.equalTo(self.avatarIV.snp.right).offset(self.screenWidth * 10/375)
        }
        self.zodiacLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.nameLabel.snp.centerY)
            make.right.equalTo(self.view.snp.right).offset(-self.screenHeight * 12/667)
        }
        self.mainTF.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatarIV.snp.bottom).offset(self.screenHeight * 12/667)
            make.left.equalTo(self.avatarIV.snp.left)
            make.right.equalTo(self.zodiacLabel.snp.right)
            make.height.equalTo(self.screenHeight * 50/667)
        }
        self.imageArea.snp.makeConstraints { (make) in
            make.top.equalTo(self.mainTF.snp.bottom).offset(self.screenHeight * 10/667)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 12 / 375)
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 12 / 375 - self.screenWidth/2)
            make.height.equalTo(self.imageArea.snp.width)
        }
        self.imgDeleteBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self.imageArea.snp.top).offset(-self.screenHeight * 15 / 667)
            make.right.equalTo(self.imageArea.snp.right).offset(self.screenWidth * 15 / 375)
            make.width.equalTo(self.screenHeight * 30 / 667)
            make.height.equalTo(self.screenHeight * 30 / 667)
        }
        self.inputToolBar.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom)
            make.height.equalTo(self.screenHeight * 50/667)
            make.width.equalTo(self.view.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        self.closeKBButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.inputToolBar.snp.right).offset(-self.screenWidth * 12/375)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
        }
        self.emojiPicker.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputToolBar.snp.left).offset(self.screenWidth * 12/375)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
        }
        self.photoSelector.snp.makeConstraints { (make) in
            make.left.equalTo(self.emojiPicker.snp.right).offset(self.screenWidth * 30/375)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
        }
        self.anonymousView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.inputToolBar.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.screenHeight * 50/667)
        }
        self.anonymousLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.anonymousView.snp.left).offset(self.screenWidth * 12/375)
            make.centerY.equalTo(self.anonymousView.snp.centerY)
            make.height.equalTo(self.screenHeight * 20/667)
        }
        self.anonymousButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.tagView.snp.right).offset(-self.screenWidth * 12/375)
            make.centerY.equalTo(self.anonymousView.snp.centerY)
            make.height.equalTo(self.screenHeight * 24/667)
            make.width.equalTo(self.screenHeight * 24/667)
        }
        self.tagView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.anonymousView.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.screenHeight * 55/667)
        }
        self.tagViewDivide.snp.makeConstraints { (make) in
            make.top.equalTo(self.tagView.snp.bottom).offset(-1)
            make.centerX.equalTo(self.tagView)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(1)
        }
        self.selectTagButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.tagView.snp.right).offset(-self.screenWidth * 12/375)
            make.height.equalTo(self.screenHeight * 24/667)
            make.width.equalTo(self.screenHeight * 24/667)
            make.centerY.equalTo(self.tagView.snp.centerY)
        }
        self.tagLabel.snp.makeConstraints { (make) in
            make.right.equalTo(self.selectTagButton.snp.left).offset(-self.screenWidth * 12/375)
            make.centerY.equalTo(self.tagView.snp.centerY)
        }
        self.tipLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.tagView.snp.left).offset(self.screenWidth * 12/375)
            make.centerY.equalTo(self.tagView.snp.centerY)
        }
        self.emojiView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.screenHeight * 200/667)
            make.centerX.equalTo(self.view.snp.centerX)
        }
    }
    
    func addReplyConstraints() {
        self.replyContent.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(self.screenWidth * 84/375)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 12/375)
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 12/375)
            make.height.equalTo(80)
        }
        self.replyDivideView.snp.makeConstraints { (make) in
            make.top.equalTo(self.replyContent.snp.bottom).offset(self.screenHeight * 5/667)
            make.left.equalTo(self.replyContent.snp.left).offset(self.screenWidth * 12/375)
            make.right.equalTo(self.replyContent.snp.right).offset(-self.screenWidth * 12/375)
            make.height.equalTo(1)
        }
        self.mainTF.snp.makeConstraints { (make) in
            make.top.equalTo(self.replyContent.snp.bottom).offset(self.screenHeight * 15/667)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 12/375)
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 12/375)
            make.height.equalTo(self.screenHeight * 50/667)
        }
        self.imageArea.snp.makeConstraints { (make) in
            make.top.equalTo(self.mainTF.snp.bottom).offset(self.screenHeight * 10/667)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 12 / 375)
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 12 / 375 - self.screenWidth/2)
            make.height.equalTo(self.imageArea.snp.width)
        }
        self.imgDeleteBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self.imageArea.snp.top).offset(-self.screenHeight * 15 / 667)
            make.right.equalTo(self.imageArea.snp.right).offset(self.screenWidth * 15 / 375)
            make.width.equalTo(self.screenHeight * 30 / 667)
            make.height.equalTo(self.screenHeight * 30 / 667)
        }
        self.inputToolBar.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom)
            make.height.equalTo(self.screenHeight * 50/667)
            make.width.equalTo(self.view.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        self.closeKBButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.inputToolBar.snp.right).offset(-self.screenWidth * 12/375)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
        }
        self.emojiPicker.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputToolBar.snp.left).offset(self.screenWidth * 12/375)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
        }
        self.photoSelector.snp.makeConstraints { (make) in
            make.left.equalTo(self.emojiPicker.snp.right).offset(self.screenWidth * 27/375)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
        }
        self.anonymousView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.inputToolBar.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.screenHeight * 50/667)
        }
        self.anonymousLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.anonymousView.snp.left).offset(self.screenWidth * 12/375)
            make.centerY.equalTo(self.anonymousView.snp.centerY)
            make.height.equalTo(self.screenHeight * 20/667)
        }
        self.anonymousButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 12/375)
            make.centerY.equalTo(self.anonymousView.snp.centerY)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
        }
        self.emojiView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.screenHeight * 200/667)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        
    }
    
    func addFirstClassReplyConstraints() {
        self.avatarIV.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(self.screenHeight * 88/667)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 12/375)
            make.width.equalTo(self.screenWidth * 33/375)
            make.height.equalTo(self.screenWidth * 33/375)
        }
        self.nameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.avatarIV.snp.centerY)
            make.left.equalTo(self.avatarIV.snp.right).offset(self.screenWidth * 10/375)
        }
        self.zodiacLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.nameLabel.snp.centerY)
            make.right.equalTo(self.view.snp.right).offset(-self.screenHeight * 12/667)
        }
        self.mainTF.snp.makeConstraints { (make) in
            make.top.equalTo(self.zodiacLabel.snp.bottom).offset(self.screenHeight * 12/667)
            make.left.equalTo(self.avatarIV)
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 25/667)
            make.height.equalTo(self.screenHeight * 50/667)
        }
        self.imageArea.snp.makeConstraints { (make) in
            make.top.equalTo(self.mainTF.snp.bottom).offset(self.screenHeight * 10/667)
            make.left.equalTo(self.view.snp.left).offset(self.screenWidth * 12 / 375)
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 12 / 375 - self.screenWidth/2)
            make.height.equalTo(self.imageArea.snp.width)
        }
        self.imgDeleteBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self.imageArea.snp.top).offset(-self.screenHeight * 15 / 667)
            make.right.equalTo(self.imageArea.snp.right).offset(self.screenWidth * 15 / 375)
            make.width.equalTo(self.screenHeight * 30 / 667)
            make.height.equalTo(self.screenHeight * 30 / 667)
        }
        self.inputToolBar.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom)
            make.height.equalTo(self.screenHeight * 50/667)
            make.width.equalTo(self.view.snp.width)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        self.closeKBButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.inputToolBar.snp.right).offset(-self.screenWidth * 12/375)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
        }
        self.emojiPicker.snp.makeConstraints { (make) in
            make.left.equalTo(self.inputToolBar.snp.left).offset(self.screenWidth * 12/375)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
        }
        self.photoSelector.snp.makeConstraints { (make) in
            make.left.equalTo(self.emojiPicker.snp.right).offset(self.screenWidth * 30/375)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
            make.centerY.equalTo(self.inputToolBar.snp.centerY)
        }
        self.anonymousView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.inputToolBar.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.screenHeight * 50/667)
        }
        self.anonymousLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.anonymousView.snp.left).offset(self.screenWidth * 12/375)
            make.centerY.equalTo(self.anonymousView.snp.centerY)
            make.height.equalTo(self.screenHeight * 20/667)
        }
        self.anonymousButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.view.snp.right).offset(-self.screenWidth * 12/375)
            make.centerY.equalTo(self.anonymousView.snp.centerY)
            make.height.equalTo(self.screenHeight * 30/667)
            make.width.equalTo(self.screenHeight * 30/667)
        }
        self.emojiView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.screenHeight * 200/667)
            make.centerX.equalTo(self.view.snp.centerX)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
