//
//  BaseSegmentController.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/17.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

enum DIGDataType {
    case love
    case tarot
    case number
    case forecast
}

class BaseSegmentController: FortureListViewcontroller {
    
    var pageTitle : String = ""
    var dataType : DIGDataType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if dataType == DIGDataType.forecast {
            self.dateArr = [NSLocalizedString("Weekly", comment: ""),NSLocalizedString("Monthly", comment:""),NSLocalizedString("Yearly", comment:"")]
        }else {
            self.dateArr = [NSLocalizedString("Yesterday", comment: ""),NSLocalizedString("Today", comment:""),NSLocalizedString("Tomorrow", comment:"")]
        }
        self.title = NSLocalizedString(self.pageTitle, comment: "")
    }
    override func addSegmentSectionHeader() {
        if self.dataType == DIGDataType.love {
            self.createLovescope()
        }else if self.dataType == DIGDataType.tarot {
            self.createTarotScope()
        }else if self.dataType == DIGDataType.forecast{
            self.createForecast()
        }
        segments?.delegate = self
        segments?.menuItemFont = HSFont.baseMediumFont(20)
        segments?.menuIndicatorColor=UIColor.luckyPurpleTitleColor()
        segments?.menuItemTitleColor=UIColor.luckyPurpleContentColor()
        segments?.menuBackGroudColor=UIColor.baseCardBackgroundColor()
        segments?.menuItemSelectedTitleColor=UIColor.luckyPurpleTitleColor()
        segments?.startIndex=currentIndex
        self.view.addSubview(segments!.view)
    }
    
    override func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {
        //switch ad
        if let anastring = analDic[index]{
            AnaliticsManager.sendEvent(AnaliticsManager.forecast_show, data:["date":anastring])
        }
        
        guard let count = segments?.childControllers.count else {
            return
        }
        if self.dataType == DIGDataType.love {
            let currentVC = controller as! LovescopeViewController
            for index  in 0..<count {
                let controller  = segments?.childControllers[index] as! LovescopeViewController
                if controller.isEqual(currentVC) {
                    controller.isCurrentController = true
                    controller.getData()
                }else{
                    controller.isCurrentController = false
                    controller.removeNoti()
                }
            }
        }else if self.dataType == DIGDataType.tarot{
            let currentVC = controller as! TarotscopeViewController
            for index  in 0..<count {
                let controller  = segments?.childControllers[index] as! TarotscopeViewController
                if controller.isEqual(currentVC) {
                    controller.isCurrentController = true
                    controller.getData()
                }else{
                    controller.isCurrentController = false
                    controller.removeNoti()
                }
            }
        }else if self.dataType == DIGDataType.number{
            
            
        }else if self.dataType == DIGDataType.forecast {
            let currentVC = controller as! FortureDetailViewcontroller
            for index  in 0..<count {
                let controller  = segments?.childControllers[index] as! FortureDetailViewcontroller
                if controller.isEqual(currentVC) {
                    controller.isCurrentController = true
                    controller.getData()
                }else{
                    controller.isCurrentController = false
                    controller.removeNoti()
                }
            }
        }
        self.currentIndex=Int32(index)
        controller.viewWillAppear(true)
    }
    
    func createTarotScope() {
        let vc1 = TarotscopeViewController.create(self.horoScopeName, style: .off1)
        let vc2 = TarotscopeViewController.create(self.horoScopeName, style: .today)
        let vc3 = TarotscopeViewController.create(self.horoScopeName, style: .tomorrow)
        
        vc1.fortune = "yesterday"
        vc2.fortune = "today"
        vc3.fortune = "tomorrow"
        
        vc1.delegate=self
        vc2.delegate=self
        vc3.delegate=self
        
        vc1.navBtndelegate=self
        vc2.navBtndelegate=self
        vc3.navBtndelegate=self
        
        vc1.title = dateArr[0]
        vc2.title = dateArr[1]
        vc3.title = dateArr[2]
        
        segments = YSLContainerViewController.init(controllers: [vc1,vc2,vc3], topBarHeight: 64,startheight: 0, parentViewController: self)
    }
    
    func createLovescope() {
        let vc1 = LovescopeViewController.create(self.horoScopeName, style: .off1)
        let vc2 = LovescopeViewController.create(self.horoScopeName, style: .today)
        let vc3 = LovescopeViewController.create(self.horoScopeName, style: .tomorrow)
        
        vc1.fortune = "yesterday"
        vc2.fortune = "today"
        vc3.fortune = "tomorrow"
        
        vc1.delegate=self
        vc2.delegate=self
        vc3.delegate=self
        
        vc1.navBtndelegate=self
        vc2.navBtndelegate=self
        vc3.navBtndelegate=self
        
        vc1.title = dateArr[0]
        vc2.title = dateArr[1]
        vc3.title = dateArr[2]
        
        segments = YSLContainerViewController.init(controllers: [vc1,vc2,vc3], topBarHeight: 64,startheight: 0, parentViewController: self)
    }
    
    func createForecast() {
        let vc1 = FortureDetailViewcontroller.create(self.horoScopeName, style: .off1)
        let vc2 = FortureDetailViewcontroller.create(self.horoScopeName, style: .today)
        let vc3 = FortureDetailViewcontroller.create(self.horoScopeName, style: .tomorrow)
        let vc4 = FortureDetailViewcontroller.create(self.horoScopeName, style: .weekly)
        let vc5 = FortureDetailViewcontroller.create(self.horoScopeName, style: .monthly)
        let vc6 = FortureDetailViewcontroller.create(self.horoScopeName, style: .yearly)
        
        vc1.fortune = "yesterday"
        vc2.fortune = "today"
        vc3.fortune = "tomorrow"
        vc4.fortune = "weekly"
        vc5.fortune = "monthly"
        vc6.fortune = "yearly"
        
        vc1.delegate=self
        vc2.delegate=self
        vc3.delegate=self
        vc4.delegate=self
        vc5.delegate=self
        vc6.delegate=self
        
        vc1.navBtndelegate=self
        vc2.navBtndelegate=self
        vc3.navBtndelegate=self
        vc4.navBtndelegate=self
        vc5.navBtndelegate=self
        vc6.navBtndelegate=self
        
        vc1.title = dateArr[0]
        vc2.title = dateArr[1]
        vc3.title = dateArr[2]
        vc4.title = dateArr[3]
        vc5.title = dateArr[4]
        vc6.title = dateArr[5]
        
        segments = YSLContainerViewController.init(controllers: [vc1,vc2,vc3,vc4,vc5,vc6], topBarHeight: 64,startheight: 0, parentViewController: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
