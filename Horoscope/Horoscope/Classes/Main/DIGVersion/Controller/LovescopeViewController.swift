//
//  LovescopeViewController.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/17.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD
import SwiftyJSON

class lovescopeViewmodel:NSObject{
    init(name:String) {
        super.init()
        self.horoName=name
    }
    var horoName:String?
    var loveModel : LovescopeModel?
    fileprivate let dao = HoroscopeDAO()
    
    func loadDetail(success:@escaping () ->(),failure:@escaping (_ error:NSError) -> ()) {
        dao.getLovescope(horoscopeName: self.horoName ?? "", success: { (model) in
            self.loveModel = model
            success()
        }, failure: { (error) in
            failure(error)
        })
    }
}

class LovescopeViewController: FortureDetailViewcontroller {
    
    var loveViewModel : lovescopeViewmodel?
    var loveModel : BaseLovescopeModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViewmodel()
        self.baseTableView?.register(LovescopeCell.self, forCellReuseIdentifier: "LovescopeCell")
    }
    
    override func initViewmodel() {
        self.indiacator.frame = CGRect(x: screenWidth/2-50, y: screenHeight/5, width: 100, height: 100)
        self.indiacator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        self.indiacator.color = UIColor.black
        self.indiacator.hidesWhenStopped = true
        self.view.addSubview(self.indiacator)
        self.indiacator.startAnimating()
        loveViewModel = lovescopeViewmodel.init(name: self.horoname ?? "")
        
        loveViewModel?.loadDetail(success: { 
            self.retryImg.isHidden = true
            self.oopsLabel.isHidden = true
            self.retryLabel.isHidden = true
            self.refreshButton.isHidden = true
            self.indiacator.stopAnimating()
            self.rowNum = 1
            self.baseTableView?.reloadData()
        }, failure: { (error) in
            self.indiacator.stopAnimating()
            self.rowNum = 0
            self.setupRetryUI()
            self.retryImg.isHidden = false
            self.oopsLabel.isHidden = false
            self.retryLabel.isHidden = false
            self.refreshButton.isHidden = false
        })
    }
    
    override func loadMoreList() {
        let offset  = String(self.commentList.count)
        HoroscopeDAO.sharedInstance.getMoreComment(self.loveModel?.postId ?? "", size: "20", offset: offset, sort: "new", success: { (commentList) in
            if commentList.count == 0 {
                HUD.flash(.label("No More Comments"),delay: 1)
                self.baseTableView?.mj_footer.endRefreshing()
            }else{
                self.commentList  = self.commentList + commentList
                self.baseTableView?.reloadData()
                self.baseTableView?.mj_footer.endRefreshing()
            }
        }) { (failure) in
            self.baseTableView?.mj_footer.endRefreshing()
        }
    }
    
    override func getData() {
        if self.isCurrentController == true {
            self.addNoti()
        }else{
            self.removeNoti()
        }
        if self.fortune == FortuneType.Today.rawValue {
            HoroscopeDAO.sharedInstance.getLovescope(horoscopeName: self.horoname ?? "", success: { (model) in
                self.loveModel = model.todayLoveModel
                self.commentList = self.loveModel?.commentList ?? [MainComment]()
                self.hotComentList = self.loveModel?.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(self.loveModel?.postId ?? "")
            }, failure: { (error) in
                print(error)
            })
        }else if self.fortune == FortuneType.Yesterday.rawValue{
            HoroscopeDAO.sharedInstance.getLovescope(horoscopeName: self.horoname ?? "", success: { (model) in
                self.loveModel = model.yesterdayLoveModel
                self.commentList = self.loveModel?.commentList ?? [MainComment]()
                self.hotComentList = self.loveModel?.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(self.loveModel?.postId ?? "")
            }, failure: { (error) in
                print(error)
            })
        }else if self.fortune == FortuneType.Tomorrow.rawValue{
            HoroscopeDAO.sharedInstance.getLovescope(horoscopeName: self.horoname ?? "", success: { (model) in
                self.loveModel = model.tomorrowLoveModel
                self.commentList = self.loveModel?.commentList ?? [MainComment]()
                self.hotComentList = self.loveModel?.hotCommentList ?? [MainComment]()
                self.baseTableView?.reloadData()
                self.commentFootBtn.isHidden = false
                self.navBtndelegate?.loadDataSuccess(self.loveModel?.postId ?? "")
            }, failure: { (error) in
                print(error)
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                if let cell:LovescopeCell = tableView.dequeueReusableCell(withIdentifier: "LovescopeCell", for: indexPath) as? LovescopeCell{
                    cell.renderCell(self.horoname ?? "",needTssAlert:needRss)
                    cell.lovescopeModel = self.loveModel
                    cell.rootVC = self
                    return cell
                }
            }else if indexPath.section == 1{
                if self.hasAd == true {
                    if indexPath.row == 0 {
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                        let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }else {
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier:"hotCell")
                        let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }
                }else if self.hasAd == false{
                    let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                    let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                    cell.contentView.addSubview(container)
                    return cell
                }
            }else if indexPath.section == 2{
                let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                cell.contentView.addSubview(container)
                return cell
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                
                if let cell:LovescopeCell = tableView.dequeueReusableCell(withIdentifier: "LovescopeCell", for: indexPath) as? LovescopeCell {
                    cell.renderCell(self.horoname ?? "",needTssAlert:needRss)
                    cell.lovescopeModel = self.loveModel
                    cell.rootVC = self
                    return cell
                }
            }else if indexPath.section == 1{
                if self.commentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                            let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }else{
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                            let container = CommentLayoutContainer(model: self.commentList[indexPath.row - 1],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }
                    }else if  self.hasAd == false{
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                        let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }
                    
                }else if self.hotComentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                            let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }else{
                            let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                            let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                            cell.contentView.addSubview(container)
                            return cell
                        }
                    }else if self.hasAd == false{
                        let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "hotCell")
                        let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                        cell.contentView.addSubview(container)
                        return cell
                    }
                }
            }
        }else{
            if indexPath.row == 0 {
                
                if let cell:LovescopeCell = tableView.dequeueReusableCell(withIdentifier: "LovescopeCell", for: indexPath) as? LovescopeCell{
                    cell.renderCell(self.horoname ?? "",needTssAlert:needRss)
                    cell.lovescopeModel = self.loveModel
                    cell.rootVC = self
                    return cell
                }
            }
        }
        return UITableViewCell(frame: CGRect.zero)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                return LovescopeCell.calculateHeight(self.loveModel?.content ?? "", needTssAlert: needRss)
            }else if indexPath.section == 1{
                if self.hasAd == true {
                    if indexPath.row == 0 {
                        let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                        return  container.frame.size.height
                    }else if indexPath.row == 1{
                        return self.adHeight
                    }else{
                        let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                        return  container.frame.size.height
                    }
                    
                }else if self.hasAd == false{
                    let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                    return  container.frame.size.height
                }
            }else if indexPath.section == 2{
                let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                return  container.frame.size.height
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                return LovescopeCell.calculateHeight(self.loveModel?.content ?? "", needTssAlert: needRss)
            }else if indexPath.section == 1{
                if self.hotComentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                            return  container.frame.size.height
                        }else if indexPath.row == 1{
                            return self.adHeight
                        }else{
                            let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row - 1],isMyNotification:false)
                            return  container.frame.size.height
                        }
                    }else if self.hasAd == false{
                        let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                        return  container.frame.size.height
                    }
                }else if self.commentList.count > 0 {
                    if self.hasAd == true {
                        if indexPath.row == 0 {
                            let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                            return  container.frame.size.height
                        }else if indexPath.row == 1{
                            return self.adHeight
                        }else{
                            let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row - 1],isMyNotification:false)
                            return  container.frame.size.height
                        }
                    }else if self.hasAd == false{
                        let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                        return  container.frame.size.height
                    }
                }
            }
        }else{
            if indexPath.row == 0 {
                return LovescopeCell.calculateHeight(self.loveModel?.content ?? "", needTssAlert: needRss)
            }else  if self.hasAd == true {
                return self.adHeight
            }
        }
        return 0
    }
    
    override func commentFootBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let replyVC = PostViewController()
            replyVC.isFirstClassReply = true
            replyVC.isForcastReply = true
            let replyModel = MyPostModel.init(jsonData: JSON.null, num: 0)
            replyModel.postId = self.loveModel?.postId  ?? ""
            replyVC.replyModel = replyModel
            replyVC.delegate = self
            replyVC.horoscopeName = CURRENT_MAIN_SIGN
            replyVC.forecastName = self.fortune
            self.navigationController?.pushViewController(replyVC, animated: true)
        }
    }
    
    override class func create(_ name:String?,style:HoroDetailType) -> LovescopeViewController{
        let vc = LovescopeViewController()
        vc.style = style
        vc.horoname = name
        return vc
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
