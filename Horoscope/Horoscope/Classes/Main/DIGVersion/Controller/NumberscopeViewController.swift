//
//  NumberscopeViewController.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/22.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import PKHUD
import MJRefresh
import SwiftyJSON

class NumViewModel : NSObject {
    var birthday : String?
    var numberModel : NumberScopeModel?
    init(birthday : String) {
        super.init()
        self.birthday = birthday
    }
    private let dao = HoroscopeDAO.sharedInstance
    
    func loadData(success:@escaping () -> (),failure:@escaping (_ error:NSError) -> ()) {
        dao.getNumberScope(birthday: self.birthday ?? "", success: { (model) in
            self.numberModel = model
            success()
        }) { (error) in
            print(error)
        }
    }
}

class NumberscopeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var birthday : String = ""
    var commentList = [MainComment]()
    var numViewModel : NumViewModel?
    var numModel : NumberScopeModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Numberscope"
        self.createTableView()
        self.initViewmodel()
        self.view.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: createLeftItemButton())
    }
    
    func createLeftItemButton() ->UIButton{
        let button = UIButton()
        button.setImage(UIImage(named: "ic_1"), for: UIControlState())
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(goBackToFeature), for: .touchUpInside)
        return button
    }
    
    @objc func goBackToFeature() {
        if self.isKind(of: TarotEnterController.self){
            if (HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController) != nil {
                self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = nil
            }
            _ = self.navigationController?.popViewController(animated: true)
        }else if self.isKind(of: TarotDefineController.self){
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            if let vc = self.navigationController?.viewControllers[0]{
                _ = self.navigationController?.popToViewController(vc, animated: true)
            }
        }
        
    }
    
    var indiacator = UIActivityIndicatorView()
    var retryImg = UIImageView()
    var oopsLabel = UILabel()
    var retryLabel = UILabel()
    var refreshButton = UIButton()
    var rowNum = 0
    
    func initViewmodel() {
        self.indiacator.frame = CGRect(x: SCREEN_WIDTH/2-50, y: SCREEN_HEIGHT/5, width: 100, height: 100)
        self.indiacator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        self.indiacator.color = UIColor.black
        self.indiacator.hidesWhenStopped = true
        self.view.addSubview(self.indiacator)
        self.indiacator.startAnimating()
        numViewModel = NumViewModel.init(birthday: self.birthday)
        numViewModel?.loadData(success: {
            self.numModel = self.numViewModel?.numberModel
            self.retryImg.isHidden = true
            self.oopsLabel.isHidden = true
            self.retryLabel.isHidden = true
            self.refreshButton.isHidden = true
            self.indiacator.stopAnimating()
            self.rowNum = 1
            self.numTableView?.reloadData()
        }, failure: { (error) in
            self.indiacator.stopAnimating()
            self.rowNum = 0
            self.setupRetryUI()
            self.retryImg.isHidden = false
            self.oopsLabel.isHidden = false
            self.retryLabel.isHidden = false
            self.refreshButton.isHidden = false
        })
    }
    
    func setupRetryUI() {
        self.view.addSubview(self.retryImg)
        self.view.addSubview(self.oopsLabel)
        self.view.addSubview(self.retryLabel)
        self.view.addSubview(self.refreshButton)
        self.retryImg.image = UIImage(named: "ic_retry_")
        self.retryImg.isHidden = true
        
        self.oopsLabel.text = "Oops!"
        self.oopsLabel.font = HSFont.baseLightFont(18)
        self.oopsLabel.textColor = UIColor.luckyPurpleContentColor()
        self.oopsLabel.textAlignment = NSTextAlignment.center
        self.oopsLabel.isHidden = true
        
        self.retryLabel.text = "The data was lost. Click to retry."
        self.retryLabel.font = HSFont.baseLightFont(18)
        self.retryLabel.textColor = UIColor.luckyPurpleContentColor()
        self.retryLabel.textAlignment = NSTextAlignment.center
        self.retryLabel.numberOfLines = 1
        self.retryLabel.isHidden = true
        
        self.refreshButton.setTitle("Retry", for: UIControlState())
        self.refreshButton.layer.cornerRadius = 18
        self.refreshButton.layer.masksToBounds = true
        self.refreshButton.layer.borderWidth = 1
        self.refreshButton.layer.borderColor = UIColor.commonPinkColor().cgColor
        self.refreshButton.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        self.refreshButton.addTarget(self, action: #selector(reloadSegmentContent), for: .touchUpInside)
        self.refreshButton.isHidden = true
        
        //make constraint
        self.retryImg.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX).offset(-5)
            make.centerY.equalTo(self.view.snp.centerY).offset(-SCREEN_HEIGHT * 100/667)
            make.height.equalTo(SCREEN_HEIGHT * 107/667)
            make.width.equalTo(SCREEN_WIDTH * 140/375)
        }
        self.oopsLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.retryImg.snp.bottom).offset(24)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.view.snp.width).offset(-10)
            make.bottom.equalTo(self.oopsLabel.snp.top).offset(22)
        }
        self.retryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.oopsLabel.snp.bottom).offset(5)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.view.snp.width).offset(-10)
            make.bottom.equalTo(self.retryLabel.snp.top).offset(22)
        }
        self.refreshButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.retryLabel.snp.bottom).offset(24)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.equalTo(SCREEN_HEIGHT * 36/667)
            make.width.equalTo(SCREEN_WIDTH * 120/375)
        }
    }
    
    @objc func reloadSegmentContent() {
        self.indiacator.startAnimating()
        self.retryImg.isHidden = true
        self.oopsLabel.isHidden = true
        self.retryLabel.isHidden = true
        self.refreshButton.isHidden = true
        numViewModel?.loadData(success: {
            self.numModel = self.numViewModel?.numberModel
            self.rowNum = 1
            self.indiacator.stopAnimating()
            self.numTableView?.reloadData()
        }, failure: { (error) in
            self.rowNum = 0
            self.indiacator.stopAnimating()
            self.setupRetryUI()
            self.retryImg.isHidden = false
            self.oopsLabel.isHidden = false
            self.retryLabel.isHidden = false
            self.refreshButton.isHidden = false
        })
    }
    
    var numTableView : UITableView?
    
    func createTableView() {
        let rect = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        self.numTableView = UITableView(frame: rect, style: .grouped)
        self.numTableView?.register(NumscopeCell.self, forCellReuseIdentifier: "NumscopeCell")
        self.numTableView?.register(DIGTimelineCell.self, forCellReuseIdentifier: "DIGTimelineCell")
        self.numTableView?.delegate = self
        self.numTableView?.dataSource = self
        self.numTableView?.separatorStyle = .none
        self.numTableView?.backgroundColor = UIColor.clear
        self.numTableView?.showsVerticalScrollIndicator = false
        self.view.addSubview(self.numTableView!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NumscopeCell.init(style: .default, reuseIdentifier: "NumscopeCell")
        cell.numberscopeModel = self.numModel
        
//        let cell = DIGTimelineCell.init(style: .default, reuseIdentifier: "DIGTimelineCell")
//        cell.rootvc = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 12
    }
    
    func commentsMoreThanSeventyFloor() {
        HUD.flash(.label("Too more comments"),delay: 1)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
