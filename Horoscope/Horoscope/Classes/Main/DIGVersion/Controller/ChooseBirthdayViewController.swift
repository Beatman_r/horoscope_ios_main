//
//  ChooseBirthdayViewController.swift
//  Horoscope
//
//  Created by Beatman on 2017/5/4.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ChooseBirthdayViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.contents = UIImage(named: "splash_animate_background.jpg")?.cgImage
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: createLeftItemButton())
        self.createUI()
    }
    
    @objc func doneBtnOnClick() {
        let date = self.datePicker.date
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyyMMdd"
        let dateString = formatter.string(from: date)
        UserDefaults.standard.setValue(dateString, forKey: "userBirthday")
        UserDefaults.standard.synchronize()
        let vc = NumberscopeViewController()
        vc.birthday = dateString
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func createLeftItemButton() ->UIButton{
        let button = UIButton()
        button.setImage(UIImage(named: "backicon"), for: UIControlState())
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(goBackToFeature), for: .touchUpInside)
        return button
    }
    
    @objc func goBackToFeature() {
        if self.isKind(of: TarotEnterController.self){
            if (HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController) != nil {
                self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = nil
            }
            _ = self.navigationController?.popViewController(animated: true)
        }else if self.isKind(of: TarotDefineController.self){
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            if let vc = self.navigationController?.viewControllers[0]{
                _ = self.navigationController?.popToViewController(vc, animated: true)
            }
        }
        
    }
    
    @objc func cancelBtnOnClick() {
        self.navigationController?.popViewController(animated: true)
    }
    
    let titleLabel : UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.luckyPurpleContentColor()
        titleLabel.text = "Please enter your DOB to get"
        titleLabel.font = HSFont.baseRegularFont(20)
        return titleLabel
    }()
    let underLabel : UILabel = {
        let underLabel = UILabel()
        underLabel.textAlignment = .center
        underLabel.textColor = UIColor.luckyPurpleTitleColor()
        underLabel.text = "your lucky number"
        underLabel.font = HSFont.baseRegularFont(24)
        return underLabel
    }()
    let doneBtn : UIButton = {
        let doneBtn = UIButton()
        doneBtn.setTitle("Done", for: .normal)
        doneBtn.backgroundColor = UIColor.commonPinkColor()
        doneBtn.setTitleColor(UIColor.flatWhite, for: .normal)
        doneBtn.titleLabel?.font = HSFont.baseRegularFont(20)
        doneBtn.layer.masksToBounds = true
        doneBtn.layer.cornerRadius = 8
        return doneBtn
    }()
    let cancelBtn : UIButton = {
        let cancel = UIButton()
        cancel.setTitle("Cancel", for: .normal)
        cancel.backgroundColor = UIColor.commonPinkColor()
        cancel.setTitleColor(UIColor.flatWhite, for: .normal)
        cancel.titleLabel?.font = HSFont.baseRegularFont(20)
        cancel.layer.masksToBounds = true
        cancel.layer.cornerRadius = 8			
        return cancel
    }()
    
    let datePicker : UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.setValue(UIColor.white, forKey: "textColor")
        datePicker.maximumDate = NSDate.init(timeIntervalSinceNow: 0) as Date
        return datePicker
    }()
    
    func createUI() {
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.underLabel)
        self.view.addSubview(self.datePicker)
        self.view.addSubview(self.doneBtn)
        self.view.addSubview(self.cancelBtn)
        self.doneBtn.addTarget(self, action: #selector(doneBtnOnClick), for: .touchUpInside)
        self.cancelBtn.addTarget(self, action: #selector(cancelBtnOnClick), for: .touchUpInside)
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(50)
            make.centerX.equalTo(self.view)
            make.width.equalTo(self.view)
        }
        self.underLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(12)
            make.centerX.equalTo(self.view)
            make.width.equalTo(self.view)
        }
        self.datePicker.snp.makeConstraints { (make) in
            make.top.equalTo(self.underLabel.snp.bottom).offset(30)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.height.equalTo(self.view.snp.width).offset(-40)
        }
        self.doneBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self.datePicker.snp.bottom).offset(30)
            make.width.equalTo(SCREEN_WIDTH-40)
            make.height.equalTo(50)
            make.centerX.equalTo(self.view)
        }
        self.cancelBtn.snp.makeConstraints { (make) in
            make.top.equalTo(self.doneBtn.snp.bottom).offset(20)
            make.width.equalTo(SCREEN_WIDTH-40)
            make.height.equalTo(50)
            make.centerX.equalTo(self.view)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
