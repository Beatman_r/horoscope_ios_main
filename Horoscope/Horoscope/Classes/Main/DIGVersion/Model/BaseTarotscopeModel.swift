//
//  BaseTarotscopeModel.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/19.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class BaseTarotscopeModel: NSObject {
    var commentCount : Int?
    var dislikeCount : Int?
    let viewCount : Int?
    var isLiked : Bool?
    let dateString : String?
    var likeCount : Int?
    let content : String?
    var isDisLiked : Bool?
    var shareCount : Int?
    let horoscopeName : String?
    let postId : String?
    var commentList:[MainComment]?
    var hotCommentList : [MainComment]?
    let img : String?
    let deck : String?
    let tarotName : String?
    let motto : String?
    
    init(jsonData:JSON) {
        self.commentCount = jsonData["commentCount"].intValue
        self.dislikeCount = jsonData["dislikeCount"].intValue
        self.viewCount = jsonData["viewCount"].intValue
        self.isLiked = jsonData["isLiked"].boolValue
        self.dateString = jsonData["dateString"].stringValue
        self.likeCount = jsonData["likeCount"].intValue
        self.content = jsonData["content"].stringValue
        self.isDisLiked = jsonData["isDisliked"].boolValue
        self.shareCount = jsonData["shareCount"].intValue
        self.horoscopeName = jsonData["horoscopeName"].stringValue
        self.postId = jsonData["postId"].stringValue
        self.img = jsonData["img"].stringValue
        self.deck = jsonData["deck"].stringValue
        self.tarotName = jsonData["tarotName"].stringValue
        self.motto = jsonData["motto"].stringValue
    }
}
