//
//  NumberScopeModel.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/21.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class NumberScopeModel: NSObject {
    let content : String?
    let number : String?
    let img : String?
    
    init(jsonData:JSON) {
        self.content = jsonData["content"].stringValue
        self.number = jsonData["number"].stringValue
        self.img = jsonData["img"].stringValue
    }
}
