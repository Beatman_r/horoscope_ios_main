//
//  LovescopeModel.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/18.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class LovescopeModel: NSObject {
    
    var yesterdayLoveModel : BaseLovescopeModel?
    var todayLoveModel : BaseLovescopeModel?
    var tomorrowLoveModel : BaseLovescopeModel?
    
    init(jsonData:JSON) {
        self.yesterdayLoveModel = BaseLovescopeModel.init(jsonData: jsonData["yesterday"])
        self.todayLoveModel = BaseLovescopeModel.init(jsonData: jsonData["today"])
        self.tomorrowLoveModel = BaseLovescopeModel.init(jsonData: jsonData["tomorrow"])
    }
}
