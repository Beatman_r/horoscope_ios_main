//
//  DIGCellModel.swift
//  Horoscope
//
//  Created by Beatman on 2017/5/5.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class DIGCellModel: NSObject {
    
    var tarotImgName : String?
    var number : String?
    
    init(tarotImgName:String,number:String) {
        self.tarotImgName = tarotImgName
        self.number = number
    }
}
