//
//  TarotscopeModel.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/19.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class TarotscopeModel: NSObject {
    
    var yesterdayTarotModel : BaseTarotscopeModel?
    var todayTarotModel : BaseTarotscopeModel?
    var tomorrowTarotModel : BaseTarotscopeModel?
    
    init(jsonData:JSON) {
        self.yesterdayTarotModel = BaseTarotscopeModel.init(jsonData: jsonData["yesterday"])
        self.todayTarotModel = BaseTarotscopeModel.init(jsonData: jsonData["today"])
        self.tomorrowTarotModel = BaseTarotscopeModel.init(jsonData: jsonData["tomorrow"])
    }
}
