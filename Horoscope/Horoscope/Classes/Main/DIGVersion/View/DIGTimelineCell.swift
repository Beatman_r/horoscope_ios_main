//
//  DIGTarotTimelineCell.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/18.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class DIGTimelineCell: BaseCardCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
    }
    
    weak var rootvc:UIViewController?
    var tarotImgName:String?
    var literalNum:String?
    var viewList : [UIView] = []
    var imgList : [UIImage] = []
    let titleList : [String] = ["love","tarot","number"]
    var collectionView : UICollectionView!
    
    func createUI() {
        self.cornerBackView?.layer.contents = UIImage(named: "transparent_half_")?.cgImage
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.contentView.layer.contents = UIImage(named: "transparent_half_")?.cgImage
        self.cornerBackView?.backgroundColor = UIColor.clear
        
        let flowLayout = UICollectionViewFlowLayout()
        let itemMargin : CGFloat = 10
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        let sizeWidth = (SCREEN_WIDTH-24 - itemMargin * 4)/3
        let sizeHeight = (SCREEN_WIDTH-24 - itemMargin * 4)/3*1.2
        flowLayout.itemSize = CGSize(width: sizeWidth, height: sizeHeight)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(DIGCollectionCell.self, forCellWithReuseIdentifier: "DIGCollectionCell")
        self.collectionView.backgroundColor = UIColor.clear
        self.cornerBackView?.addSubview(self.collectionView)
        self.collectionView.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left).offset(12)
            make.right.equalTo(self.snp.right).offset(-12)
            make.height.equalTo(self)
            make.top.equalTo(self.snp.top).offset(12)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DIGCollectionCell", for: indexPath) as! DIGCollectionCell
        
        cell.itemName.text = self.titleList[indexPath.item].capitalized
        switch indexPath.item {
        case 0:
            cell.numberLabel.text = ""
            cell.itemImg.image = UIImage(named: "ic_daily_love")
            cell.filterView.isHidden = true
            cell.contentView.backgroundColor = UIColor.init(hexString: "#d341d8")
        case 1:
            cell.numberLabel.text = ""
            HoroscopeDAO.sharedInstance.getTarotscope(success: { (model) in
                let tarotModel : TarotscopeModel = model
                let imgName = tarotModel.todayTarotModel?.img ?? ""
                let tarotImgName = URL(string: imgName)
                cell.itemImg.contentMode = UIViewContentMode.scaleToFill
                cell.itemImg.sd_setImage(with: tarotImgName, placeholderImage: UIImage(named: "tarot_card_main"), options: .continueInBackground)
            }, failure: { (error) in
                print(error)
            })
            cell.contentView.backgroundColor = UIColor.init(hexString: "#4fa8e3")
        case 2:
            cell.filterView.isHidden = true
            if UserDefaults.standard.value(forKey: "userBirthday") != nil {
                HoroscopeDAO.sharedInstance.getNumberScope(birthday: UserDefaults.standard.value(forKey: "userBirthday") as! String, success: { (model) in
                    cell.numberLabel.text = model.number ?? ""
                }, failure: { (error) in
                    print(error)
                })
            }else {
                cell.numberLabel.text = "?"
            }
            cell.contentView.backgroundColor = UIColor.init(hexString: "#50b29a")
        default:
            cell.contentView.backgroundColor = UIColor.clear
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            let vc = BaseSegmentController()
            vc.dataType = DIGDataType.love
            vc.currentIndex = 1
            vc.horoScopeName = CURRENT_MAIN_SIGN?.lowercased()
            vc.pageTitle = "Lovescope"
            self.rootvc?.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = BaseSegmentController()
            vc.dataType = DIGDataType.tarot
            vc.currentIndex = 1
            vc.horoScopeName = CURRENT_MAIN_SIGN?.lowercased()
            vc.pageTitle = "Tarotscope"
            self.rootvc?.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let userBirthday = UserDefaults.standard.value(forKey: "userBirthday") as! String
            if userBirthday == "" {
                let vc = ChooseBirthdayViewController()
                self.rootvc?.navigationController?.pushViewController(vc, animated: true)
            }else {
                let vc = NumberscopeViewController()
                vc.birthday = UserDefaults.standard.value(forKey: "userBirthday") as! String
                self.rootvc?.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            return
        }
    }
    
    class func cellHeight() -> CGFloat {
        let itemMargin : CGFloat = 10
        return (SCREEN_WIDTH-24 - itemMargin * 4)/3*1.2 + 24
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
