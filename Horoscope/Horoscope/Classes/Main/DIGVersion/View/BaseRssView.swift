//
//  BaseRssView.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/18.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class BaseRssView: UIView {

    let urlLabel : UILabel = {
        let urlLabel = UILabel()
        urlLabel.text = "www.tarot.com"
        urlLabel.textAlignment = .left
        urlLabel.font = HSFont.baseRegularFont(16)
        urlLabel.textColor = UIColor.commonPinkColor()
        urlLabel.isUserInteractionEnabled = true
        return urlLabel
    }()
    
    let writerLabel : UILabel = {
        let writerLabel = UILabel()
        writerLabel.text = "who wrote this horoscope"
        writerLabel.textAlignment = .left
        writerLabel.font = HSFont.baseRegularFont(16)
        writerLabel.textColor = UIColor.commonPinkColor()
        writerLabel.isUserInteractionEnabled = true 
        return writerLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(urlLabel)
        self.addSubview(writerLabel)
        self.urlLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left)
            make.width.equalTo(self.snp.width)
            make.height.equalTo(18)
        }
        self.writerLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.urlLabel.snp.bottom).offset(12)
            make.left.equalTo(self.snp.left)
            make.width.equalTo(self.snp.width)
            make.height.equalTo(18)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
