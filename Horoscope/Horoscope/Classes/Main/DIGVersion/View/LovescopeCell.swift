//
//  LovescopeCell.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/18.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit

class LovescopeCell: HoroDescriptionCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.rssTopLabel.text = "Get to the Bottom of Your Relationship Issues"
        self.rssBottomLabel.text = "Know Where Your Relationship is Headed this Weekend"
    }
    
    var lovescopeModel:BaseLovescopeModel?{
        didSet{
            renderCell()
        }
    }
    
    override func createUI() {
        super.createUI()
        self.rssTopLabel.text = "Get to the Bottom of Your Relationship Issues"
        self.rssBottomLabel.text = "Know Where Your Relationship is Headed this Weekend"
        self.createAdView()
    }
    
    func createAdView() {
        let view = InnerAdContainer()
        let adInfo = SingleAdInfo()
        adInfo.placementKey = "todayRect"
        view.requestAd(self.rootVC, adInfo: adInfo, isStepLoad: false)
        self.topImage.addSubview(view)
        view.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.topImage)
        })
    }
    
    override func renderCell() {
        if self.lovescopeModel?.isLiked == true{
            likeButton.setImage(UIImage(named:"praise2_"), for: UIControlState.normal)
        }else{
            likeButton.setImage(UIImage(named:"praise_"), for: UIControlState.normal)
        }
        
        if self.lovescopeModel?.isDisLiked == true{
            dislikeButton.setImage(UIImage(named:"Step-on2_"), for: UIControlState.normal)
        }else{
            dislikeButton.setImage(UIImage(named:"Step-on_"), for: UIControlState.normal)
        }
        
        if self.lovescopeModel?.likeCount == 0 {
            likeCountLabel.isHidden = true
        }else{
            likeCountLabel.isHidden = false
            likeCountLabel.text = String(self.lovescopeModel?.likeCount ?? 0)
        }
        
        if self.lovescopeModel?.dislikeCount == 0 {
            dislikeCountLabel.isHidden = true
        }else{
            dislikeCountLabel.isHidden = false
            dislikeCountLabel.text = String(self.lovescopeModel?.dislikeCount ?? 0)
        }
        
        if self.lovescopeModel?.commentCount == 0 {
            commentCountLabel.isHidden = true
        }else {
            commentCountLabel.isHidden = false
            commentCountLabel.text = String(self.lovescopeModel?.commentCount ?? 0)
        }
        
        let font = HSFont.baseLightFont(18)
        contentLab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: lovescopeModel?.content ?? "", font: font)
    }
    
    override func topOpenRssURL() {
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/readings-reports/tarot-readings/bottom-line/love/?code=dailyinnovations&utm_medium=partner&utm_source=dailyinnovations&utm_campaign=daily_app&utm_content=love-bottom")!)
    }
    
    override func bottomOpenRssURL() {
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/horoscopes/weekend-love?code=dailyinnovations&utm_medium=partner&utm_source=dailyinnovations&utm_campaign=daily_app&utm_content=weekend")!)
    }
    
    override func commentBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let postVC = PostViewController()
            postVC.isFirstClassReply = true
            postVC.isForcastReply = true
            postVC.horoscopeName = CURRENT_MAIN_SIGN
            postVC.forecastName = "today"
            let postId = self.lovescopeModel?.postId ?? ""
            let model = MyPostModel(postId: postId)
            let readModel = ReadPostModel.init(model: model)
            postVC.postModel = readModel
            self.rootVC?.navigationController?.pushViewController(postVC, animated: true)
        }
    }
    
    override func praiseBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.lovescopeModel?.isLiked == false{
                let newCount = (self.lovescopeModel?.likeCount ?? 0) + 1
                self.lovescopeModel?.likeCount = newCount
                self.lovescopeModel?.isLiked = true
                self.renderCell()
                //  self.delegete?.praiseOneNormalTopic("forecast", action: "like", value: true, id: postModel?.postId ?? "" )
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: true, id: self.lovescopeModel?.postId ?? "")
            }
            else  if self.lovescopeModel?.isLiked == true {
                var newCount = (self.lovescopeModel?.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.lovescopeModel?.likeCount = newCount
                self.lovescopeModel?.isLiked = false
                self.renderCell()
                //                self.delegete?.praiseOneNormalTopic("forecast", action: "like", value: false, id: postModel?.postId ?? "" )
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: false, id: self.lovescopeModel?.postId ?? "")
            }
        }
        
    }
    
    override func treadBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if  self.lovescopeModel?.isDisLiked == false{
                self.lovescopeModel?.isDisLiked = true
                let newCount = (self.lovescopeModel?.dislikeCount ?? 0) + 1
                self.lovescopeModel?.dislikeCount = newCount
                self.renderCell()
                // self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: true, id: postModel?.postId ?? "" )
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: true, id: self.lovescopeModel?.postId ?? "")
            }
            else if  self.lovescopeModel?.isDisLiked == true{
                self.lovescopeModel?.isDisLiked = false
                var newCount = (self.lovescopeModel?.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.lovescopeModel?.dislikeCount = newCount
                self.renderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: false, id: self.lovescopeModel?.postId ?? "")
                //   self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: false, id: postModel?.postId ?? "" )
            }
        }
    }
    
    override func goFBShare() {
        let content = FBSDKShareLinkContent.init()
        content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_LOVE + (self.lovescopeModel?.postId ?? ""))
        FBSDKShareDialog.show(from: self.rootVC, with: content, delegate: nil)
    }
    
    override class func calculateHeight(_ content:String,needTssAlert:Bool) ->CGFloat{
        let font = HSFont.baseLightFont(18)
        let kAdpt = SCREEN_HEIGHT / 667
        if needTssAlert == true{
            if SCREEN_WIDTH == 320 {
                return HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + SCREEN_WIDTH / 16 * 9 + ((SCREEN_WIDTH == 375 ? 202 : 177) * kAdpt) + 70
            }else {
                return  HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + SCREEN_WIDTH / 16 * 9 + ((SCREEN_WIDTH == 375 ? 202 : 177) * kAdpt)
            }
        }else {
            return  HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + (80 * kAdpt) + (40 * kAdpt) + SCREEN_WIDTH / 16 * 9
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
