//
//  NumscopeCell.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/18.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class NumscopeCell: BaseCardCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var numberscopeModel:NumberScopeModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        self.descriptionLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: self.numberscopeModel?.content ?? "", font: HSFont.baseLightFont(18))
//        self.descriptionLabel.text = self.numberscopeModel?.content ?? ""
        let url = URL(string: self.numberscopeModel?.img ?? "")
        let path = Bundle.main.path(forResource: "defaultImg1", ofType: "jpg")
        let placeHolderImg = UIImage(contentsOfFile: path ?? "")
        self.numberImage.sd_setImage(with: url, placeholderImage: placeHolderImg, options: .continueInBackground)
    }
    
    let numberImage : UIImageView = {
        let numberImage = UIImageView()
        numberImage.backgroundColor = UIColor.flatGray
        numberImage.contentMode = UIViewContentMode.scaleAspectFill
        return numberImage
    }()
    
    let descriptionLabel : UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.text = ""
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .left
        descriptionLabel.textColor = UIColor.luckyPurpleTitleColor()
        return descriptionLabel
    }()
    
    let rssView : BaseRssView = {
        let rssView = BaseRssView.init(frame: CGRect.zero)
        rssView.urlLabel.text = "Written by Tarot.com’s Hans Decoz"
        rssView.writerLabel.text = "Learn How Numbers Impact Your Life"
        return rssView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
        self.createConstraints()
    }
    
    func createUI() {
        self.backgroundColor = UIColor.clear
        self.cornerBackView?.addSubview(numberImage)
        self.cornerBackView?.addSubview(descriptionLabel)
        self.cornerBackView?.addSubview(rssView)
        
        let tap1 = UITapGestureRecognizer()
        tap1.addTarget(self, action: #selector(openTopRss))
        let tap2 = UITapGestureRecognizer()
        tap2.addTarget(self, action: #selector(openBottomRss))
        self.rssView.urlLabel.addGestureRecognizer(tap1)
        self.rssView.writerLabel.addGestureRecognizer(tap2)
        self.rssView.urlLabel.isUserInteractionEnabled = true
        self.rssView.writerLabel.isUserInteractionEnabled = true
    }
    
    @objc func openTopRss(){
        UIApplication.shared.openURL(URL(string: "http://www.tarot.com/tarot/forecast-monthly/?code=dailyinnovations&utm_medium=partner&utm_source=dailyinnovations&utm_campaign=daily_app&utm_content=monthly")!)
    }
    @objc func openBottomRss(){
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/readings-reports/tarot-readings/celtic-cross/?code=dailyinnovations&utm_medium=partner&utm_source=dailyinnovations&utm_campaign=daily_app&utm_content=celtic")!)
    }

    func createConstraints() {
        self.numberImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.cornerBackView!.snp.top)
            make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
            make.width.equalTo(SCREEN_WIDTH-24)
            make.height.equalTo((SCREEN_WIDTH-24) / 16 * 9)
        }
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.numberImage.snp.bottom).offset(20)
            make.left.equalTo(self.numberImage.snp.left)
            make.right.equalTo(self.numberImage.snp.right)
        }
        self.rssView.snp.makeConstraints { (make) in
            make.top.equalTo(self.descriptionLabel.snp.bottom).offset(20)
            make.left.equalTo(self.numberImage.snp.left)
            make.right.equalTo(self.numberImage.snp.right)
            make.height.equalTo(50)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
