//
//  TarotscopeCell.swift
//  Horoscope
//
//  Created by Beatman on 2017/4/18.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit

class TarotscopeCell: HoroDescriptionCell,CommentStatusBarDelegate,FBSDKSharingDelegate {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createUI()
        createConstraint()
    }
    
    var tarotcopeModel:BaseTarotscopeModel?{
        didSet{
            renderCell()
        }
    }
    let horoName = ""
    override func renderCell() {
        
        if self.tarotcopeModel?.isLiked == true {
            self.commentStatusBar.likeButton.setImage(UIImage(named:"praise2_"), for: UIControlState.normal)
        }else {
            self.commentStatusBar.likeButton.setImage(UIImage(named:"praise_"), for: UIControlState.normal)
        }
        let disLikeImgSelected = UIImage(named: "Step-on2_")
        let disLikeImg = UIImage(named: "Step-on_")
        self.commentStatusBar.unLikeButton.setImage(self.tarotcopeModel?.isDisLiked == true ? disLikeImgSelected : disLikeImg, for: UIControlState.normal)
        
        if self.tarotcopeModel?.likeCount == 0 {
            self.commentStatusBar.likeCountLabel.isHidden = true
        }else{
            self.commentStatusBar.likeCountLabel.isHidden = false
            self.commentStatusBar.likeCountLabel.text = String(self.tarotcopeModel?.likeCount ?? 0)
        }
        
        if self.tarotcopeModel?.dislikeCount == 0 {
            self.commentStatusBar.unLikeCountLabel.isHidden = true
        }else{
            self.commentStatusBar.unLikeCountLabel.isHidden = false
            self.commentStatusBar.unLikeCountLabel.text = String(self.tarotcopeModel?.dislikeCount ?? 0)
        }
        
        if self.tarotcopeModel?.commentCount == 0 {
            self.commentStatusBar.commentCountLabel.isHidden = true
        }else {
            self.commentStatusBar.commentCountLabel.isHidden = false
            self.commentStatusBar.commentCountLabel.text = String(self.tarotcopeModel?.commentCount ?? 0)
        }
        
        let font = HSFont.baseLightFont(18)
        descriptionLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: self.tarotcopeModel?.content ?? "", font: font)
        let smallFont = HSFont.baseLightFont(16)
        cardNameLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: self.tarotcopeModel?.tarotName ?? "", font: smallFont)
        cardDefineLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: self.tarotcopeModel?.motto ?? "", font: smallFont)
        deckLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: "Deck: \(self.tarotcopeModel?.deck ?? "")", font: smallFont)
        let imgURL = URL(string: self.tarotcopeModel?.img ?? "")
        self.tarotCardView.sd_setImage(with: imgURL, placeholderImage: UIImage(named: "tarot_card_main"), options: .continueInBackground)
    }
    
    func didClickComment() {
        
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let postVC = PostViewController()
            postVC.isFirstClassReply = true
            postVC.isForcastReply = true
            postVC.horoscopeName = self.horoName
            postVC.forecastName = "today"
            let postId = self.tarotcopeModel?.postId ?? ""
            let model = MyPostModel(postId: postId)
            let readModel = ReadPostModel.init(model: model)
            postVC.postModel = readModel
            self.rootVC?.navigationController?.pushViewController(postVC, animated: true)
        }
    }
    
    func didClickDislike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.tarotcopeModel?.isDisLiked == false {
                let newCount = (self.tarotcopeModel?.dislikeCount ?? 0) + 1
                self.tarotcopeModel?.dislikeCount = newCount
                self.tarotcopeModel?.isDisLiked = true
                self.commentStatusBar.unLikeCountLabel.isHidden = false
                self.renderCell()
                self.commentStatusBar.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                self.commentStatusBar.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: true, id: self.tarotcopeModel?.postId ?? "")
            }else if self.tarotcopeModel?.isDisLiked == true{
                var newCount = (self.tarotcopeModel?.dislikeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
                    self.commentStatusBar.unLikeCountLabel.isHidden = true
                }
                self.tarotcopeModel?.dislikeCount = newCount
                self.tarotcopeModel?.isDisLiked = false
                self.renderCell()
                self.commentStatusBar.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                self.commentStatusBar.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: false, id: self.tarotcopeModel?.postId ?? "")
            }
        }
    }
    
    func didClickLike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.tarotcopeModel?.isLiked == false {
                let newCount = (self.tarotcopeModel?.likeCount ?? 0) + 1
                self.tarotcopeModel?.likeCount = newCount
                self.tarotcopeModel?.isLiked = true
                self.commentStatusBar.likeCountLabel.isHidden = false
                self.renderCell()
                self.commentStatusBar.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                self.commentStatusBar.likeCountLabel.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: true, id: self.tarotcopeModel?.postId ?? "")
            }else if self.tarotcopeModel?.isLiked == true{
                var newCount = (self.tarotcopeModel?.likeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
                    self.commentStatusBar.likeCountLabel.isHidden = true
                }
                self.tarotcopeModel?.likeCount = newCount
                self.tarotcopeModel?.isLiked = false
                self.renderCell()
                self.commentStatusBar.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                self.commentStatusBar.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: false, id: self.tarotcopeModel?.postId ?? "")
            }
        }
    }
    
    func didClickShare() {
        let content = FBSDKShareLinkContent.init()
        content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_TAROT + (self.tarotcopeModel?.postId ?? ""))
        FBSDKShareDialog.show(from: self.rootVC, with: content, delegate: self)
        HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "share", value: true, id: self.tarotcopeModel?.postId ?? "")
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        let newCount = (self.tarotcopeModel?.shareCount ?? 0) + 1
        self.tarotcopeModel?.shareCount = newCount
        self.renderCell()
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        return
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!){
        print(error)
    }
    
    let tarotCardView : UIImageView = {
        let tarotCardView = UIImageView()
        tarotCardView.contentMode = .scaleToFill
        return tarotCardView
    }()
    
    let cardNameLabel : UILabel = {
        let cardNameLabel = UILabel()
        cardNameLabel.text = "Six of wands"
        cardNameLabel.textAlignment = .left
        return cardNameLabel
    }()
    
    let cardDefineLabel : UILabel = {
        let cardDefineLabel = UILabel()
        cardDefineLabel.text = "Your enthusiastic stewardship is producing dramatic results"
        cardDefineLabel.textAlignment = .center
        cardDefineLabel.numberOfLines = 0
        
        return cardDefineLabel
    }()
    
    let deckLabel : UILabel = {
        let deckLabel = UILabel()
        deckLabel.textAlignment = .left
        return deckLabel
    }()
    
    let descriptionLabel : UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.text = "tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot tarot"
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .left
        return descriptionLabel
    }()
    
    let urlLabel : UILabel = {
        let urlLabel = UILabel()
        urlLabel.text = "Gain the Empowerment You Need to Move Forward"
        urlLabel.textColor = UIColor.commonPinkColor()
        urlLabel.textAlignment = .left
        urlLabel.numberOfLines = 1
        return urlLabel
    }()
    
    let writerLabel : UILabel = {
        let writerLabel = UILabel()
        writerLabel.textAlignment = .left
        writerLabel.textColor = UIColor.commonPinkColor()
        writerLabel.text = "Reveal Deeper Insight with a Free Tarot Reading"
        writerLabel.numberOfLines = 1
        return writerLabel
    }()
    
    let commentStatusBar : CommentStatusBar = {
        let commentStatusBar = CommentStatusBar(frame: CGRect.zero)
        return commentStatusBar
    }()
    
    override func createUI() {
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.cornerBackView?.addSubview(tarotCardView)
        self.cornerBackView?.addSubview(cardNameLabel)
        self.cornerBackView?.addSubview(cardDefineLabel)
        self.cornerBackView?.addSubview(deckLabel)
        self.cornerBackView?.addSubview(descriptionLabel)
        self.cornerBackView?.addSubview(urlLabel)
        self.cornerBackView?.addSubview(writerLabel)
        self.cornerBackView?.addSubview(commentStatusBar)
        commentStatusBar.delegate = self
        let tap1 = UITapGestureRecognizer()
        tap1.addTarget(self, action: #selector(self.openTopRss))
        let tap2 = UITapGestureRecognizer()
        tap2.addTarget(self, action: #selector(self.openBottomRss))
        self.urlLabel.addGestureRecognizer(tap1)
        self.writerLabel.addGestureRecognizer(tap2)
        urlLabel.isUserInteractionEnabled = true
        writerLabel.isUserInteractionEnabled = true
    }
    
    @objc func openTopRss(){
        UIApplication.shared.openURL(URL(string: "http://www.tarot.com/tarot/forecast-monthly/?code=dailyinnovations&utm_medium=partner&utm_source=dailyinnovations&utm_campaign=daily_app&utm_content=monthly")!)
    }
    @objc func openBottomRss(){
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/readings-reports/tarot-readings/celtic-cross/?code=dailyinnovations&utm_medium=partner&utm_source=dailyinnovations&utm_campaign=daily_app&utm_content=celtic")!)
    }
    
    func createConstraint() {
        self.tarotCardView.snp.makeConstraints { (make) in
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(12)
            make.top.equalTo((self.cornerBackView?.snp.top)!).offset(12)
            make.width.equalTo(120)
            make.height.equalTo(120/3*4)
        }
        self.cardNameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.tarotCardView.snp.right).offset(20)
            make.top.equalTo(self.tarotCardView.snp.top)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
        }
        self.cardDefineLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.cardNameLabel.snp.left)
            make.top.equalTo(self.cardNameLabel.snp.bottom).offset(12)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.bottom.equalTo(self.deckLabel.snp.top).offset(-12)
        }
        self.deckLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.cardNameLabel.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.bottom.equalTo(self.tarotCardView.snp.bottom)
        }
        self.descriptionLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.tarotCardView.snp.left)
            make.top.equalTo(self.deckLabel.snp.bottom).offset(20)
            make.right.equalTo(self.cardNameLabel.snp.right)
        }
        self.urlLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.tarotCardView.snp.left)
            make.top.equalTo(self.descriptionLabel.snp.bottom).offset(20)
            make.width.equalTo(SCREEN_WIDTH - 24)
        }
        self.writerLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.tarotCardView.snp.left)
            make.top.equalTo(self.urlLabel.snp.bottom).offset(20)
            make.width.equalTo(SCREEN_WIDTH - 24)
        }
        self.commentStatusBar.snp.makeConstraints { (make) in
            make.top.equalTo(self.writerLabel.snp.bottom).offset(12)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(50)
            make.left.equalTo(self.cornerBackView!.snp.left)
        }
    }
    
    override class func calculateHeight(_ content:String,needTssAlert:Bool) ->CGFloat{
        let font = HSFont.baseLightFont(18)
        let kAdpt = SCREEN_HEIGHT / 736
        if needTssAlert == true{
            if SCREEN_WIDTH == 320 {
                return  (HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + SCREEN_WIDTH / 16 * 9 + 200*kAdpt)
            }
            return  (HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + SCREEN_WIDTH / 16 * 9 + ((SCREEN_WIDTH == 375 ? 146 : 123) * kAdpt))
        }else {return  (HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font: font, width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.25) + SCREEN_WIDTH / 16 * 9)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
