//
//  DIGCollectionCell.swift
//  Horoscope
//
//  Created by Beatman on 2017/5/3.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class DIGCellView : UIView {
    var isTarot : Bool = false
    var isLove : Bool = false
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DIGCollectionCell: UICollectionViewCell {
    var itemImg : UIImageView!
    var itemName : UILabel!
    let numberLabel = UILabel()
    var filterView : UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.backgroundColor = UIColor.clear
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 4
        
        let backView = DIGCellView()
        backView.backgroundColor = UIColor.clear
        self.contentView.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.center.equalTo(self.contentView)
            make.width.height.equalTo(self.contentView).offset(-20)
        }
        
        self.filterView = UIView()
        filterView.backgroundColor = UIColor.white
        backView.addSubview(filterView)
        self.filterView.layer.masksToBounds = true
        self.filterView.layer.cornerRadius = 6
        self.filterView.snp.makeConstraints { (make) in
            make.top.equalTo(backView)
            make.centerX.equalTo(backView)
            make.width.equalTo(backView).offset(-30)
            make.height.equalTo(backView).offset(-30)
        }
        
        self.itemImg = UIImageView()
        itemImg.layer.masksToBounds = true
        itemImg.layer.cornerRadius = 6
        itemImg.backgroundColor = UIColor.clear
        itemImg.contentMode = UIViewContentMode.scaleAspectFit
        backView.addSubview(itemImg)
        itemImg.snp.makeConstraints { (make) in
            make.top.equalTo(backView).offset(2)
            make.centerX.equalTo(backView)
            make.width.equalTo(filterView).offset(-4)
            make.height.equalTo(filterView).offset(-4)
        }
        numberLabel.text = "?"
        numberLabel.textAlignment = .center
        numberLabel.textColor = UIColor.white
        numberLabel.font = SCREEN_WIDTH == 320 ? HSFont.baseRegularFont(70) : HSFont.baseRegularFont(100)
        backView.addSubview(numberLabel)
        numberLabel.snp.makeConstraints({ (make) in
            make.top.equalTo(backView)
            make.centerX.equalTo(backView)
//            make.width.equalTo(backView).offset(-33)
            make.height.equalTo(backView).offset(-30)
        })
        self.itemName = UILabel()
        itemName.textColor = UIColor.flatWhite
        itemName.textAlignment = .center
        backView.addSubview(itemName)
        itemName.snp.makeConstraints { (make) in
            make.centerX.equalTo(backView)
            make.bottom.equalTo(backView).offset(-3)
            make.width.equalTo(backView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
