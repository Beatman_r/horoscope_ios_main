//
//  DIGCellMainView.swift
//  Horoscope
//
//  Created by Beatman on 2017/5/3.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class DIGCellMainView: UIView {
    
    let titleLabel : UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = HSFont.baseRegularFont(15)
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.white
        return titleLabel
    }()
    
    let mainImg : UIImageView = {
        let mainImg = UIImageView()
        mainImg.contentMode = UIViewContentMode.scaleAspectFill
        mainImg.backgroundColor = UIColor.windowColor()
        return mainImg
    }()
    
    init(title:String,image:UIImage,frame:CGRect) {
        super.init(frame:frame)
        self.addSubview(mainImg)
        self.addSubview(titleLabel)
        self.titleLabel.text = title
        self.mainImg.image = image
        self.titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
            make.centerY.equalTo(self.snp.centerY).offset(20)
            make.width.equalTo(self)
        }
        self.mainImg.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
