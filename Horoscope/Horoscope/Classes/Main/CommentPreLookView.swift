//
//  CommentPreLookView.swift
//  Horoscope
//
//  Created by Beatman on 17/2/15.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

protocol CommentPreLookViewDelegate : class {
    func preLookDidClickLike()
    func preLookDidClickDislike()
    func prelookDidClickViewImg()
    func seeOriginalTapAction()
}

protocol CommentPreLookViewBelowDelegate : class {
    func preLookBelowDidClickLike()
    func preLookBelowDidClickDislike()
    func prelookBelowDidClickViewImg()
}

class CommentPreLookView: UIView {
    weak var delegate : CommentPreLookViewDelegate?
    weak var belowDelegate : CommentPreLookViewBelowDelegate?
    var imgUrl : String?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.backgroundColor = UIColor.clear
        self.addCPSubviews()
        self.addCPConstraint()
        self.addTargets()
    }
    
    let userAva : UIImageView = {
        let userAva = UIImageView()
        userAva.layer.masksToBounds = true
        userAva.layer.cornerRadius = 15
        return userAva
    }()
    
    let nameLabel : UILabel = {
        let nameLabel = UILabel()
        nameLabel.text = ""
        nameLabel.textColor = UIColor.luckyPurpleTitleColor()
        nameLabel.textAlignment = NSTextAlignment.left
        nameLabel.font = HSFont.baseRegularFont(15)
        return nameLabel
    }()
    
    let signLabel : UILabel = {
        let signLabel = UILabel()
        signLabel.text = ""
        signLabel.textAlignment = NSTextAlignment.left
        signLabel.textColor = UIColor.createTimeColor()
        signLabel.font = HSFont.baseRegularFont(13)
        return signLabel
    }()
    
    let createTime : UILabel = {
        let createTime = UILabel()
        createTime.textColor = UIColor.createTimeColor()
        createTime.textAlignment = NSTextAlignment.left
        createTime.text = ""
        createTime.font = HSFont.baseRegularFont(13)
        return createTime
    }()
    
    let contentLabel : UILabel = {
        let contentLabel = UILabel()
        contentLabel.text = ""
        contentLabel.textColor = UIColor.luckyPurpleTitleColor()
        contentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        contentLabel.numberOfLines = 0
        contentLabel.textAlignment = NSTextAlignment.left
        contentLabel.font = HSFont.baseRegularFont(15)
        return contentLabel
    }()
    
    let viewImageButton : UIButton = {
        let viewImageButton = UIButton()
        viewImageButton.setImage(UIImage(named: "picture_"), for: UIControlState())
        viewImageButton.isHidden = true
        return viewImageButton
    }()
    
    let viewImgLabel : UILabel = {
        let viewImgLabel = UILabel()
        viewImgLabel.textColor = UIColor.luckyHeaderButtonColor()
        viewImgLabel.font = HSFont.baseRegularFont(SCREEN_WIDTH == 320 ? 13 : 15)
        viewImgLabel.textAlignment = .left
        viewImgLabel.text = "View Image"
        viewImgLabel.isUserInteractionEnabled = true
        return viewImgLabel
    }()
    
    let likeButton : UIButton = {
        let likeButton = UIButton()
        likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
        return likeButton
    }()
    
    let likeCountLabel : UILabel = {
        let likeCountLabel = UILabel()
        likeCountLabel.text = "\(0)"
        likeCountLabel.textAlignment = NSTextAlignment.left
        likeCountLabel.font = HSFont.baseRegularFont(15)
        likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
        return likeCountLabel
    }()
    
    let unLikeButton : UIButton = {
        let unLikeButton = UIButton()
        unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
        return unLikeButton
    }()
    
    let unLikeCountLabel : UILabel = {
        let unLikeCountLabel = UILabel()
        unLikeCountLabel.text = "\(0)"
        unLikeCountLabel.textAlignment = NSTextAlignment.left
        unLikeCountLabel.font = HSFont.baseRegularFont(15)
        unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
        return unLikeCountLabel
    }()
    
    let divideLine : UIView = {
        let divideLine = UIView()
        divideLine.backgroundColor = UIColor.divideColor()
        return divideLine
    }()
    
    //MARK: BelowPart
    
    let userAvaBelow : UIImageView = {
        let userAvaBelow = UIImageView()
        userAvaBelow.layer.masksToBounds = true
        userAvaBelow.layer.cornerRadius = 15
        return userAvaBelow
    }()
    
    let nameLabelBelow : UILabel = {
        let nameLabelBelow = UILabel()
        nameLabelBelow.text = ""
        nameLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
        nameLabelBelow.textAlignment = NSTextAlignment.left
        nameLabelBelow.font = HSFont.baseRegularFont(15)
        return nameLabelBelow
    }()
    
    let signLabelBelow : UILabel = {
        let signLabelBelow = UILabel()
        signLabelBelow.text = ""
        signLabelBelow.textAlignment = NSTextAlignment.left
        signLabelBelow.textColor = UIColor.createTimeColor()
        signLabelBelow.font = HSFont.baseRegularFont(13)
        return signLabelBelow
    }()
    
    let createTimeBelow : UILabel = {
        let createTimeBelow = UILabel()
        createTimeBelow.textColor = UIColor.createTimeColor()
        createTimeBelow.textAlignment = NSTextAlignment.left
        createTimeBelow.text = ""
        createTimeBelow.font = HSFont.baseRegularFont(13)
        return createTimeBelow
    }()
    
    let contentLabelBelow : UILabel = {
        let contentLabelBelow = UILabel()
        contentLabelBelow.text = ""
        contentLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
        contentLabelBelow.lineBreakMode = NSLineBreakMode.byWordWrapping
        contentLabelBelow.numberOfLines = 0
        contentLabelBelow.textAlignment = NSTextAlignment.left
        contentLabelBelow.font = HSFont.baseRegularFont(15)
        return contentLabelBelow
    }()
    
    let viewImageButtonBelow : UIButton = {
        let viewImageButtonBelow = UIButton()
        viewImageButtonBelow.setImage(UIImage(named: "picture_"), for: UIControlState())
        viewImageButtonBelow.isHidden = true
        return viewImageButtonBelow
    }()
    
    let viewImgLabelBelow : UILabel = {
        let viewImgLabelBelow = UILabel()
        viewImgLabelBelow.textColor = UIColor.luckyHeaderButtonColor()
        viewImgLabelBelow.font = HSFont.baseRegularFont(SCREEN_WIDTH == 320 ? 13 : 15)
        viewImgLabelBelow.textAlignment = .left
        viewImgLabelBelow.text = "View Image"
        viewImgLabelBelow.isUserInteractionEnabled = true
        return viewImgLabelBelow
    }()
    
    let likeButtonBelow : UIButton = {
        let likeButtonBelow = UIButton()
        likeButtonBelow.setImage(UIImage(named: "praise_"), for: UIControlState())
        return likeButtonBelow
    }()
    
    let likeCountLabelBelow : UILabel = {
        let likeCountLabelBelow = UILabel()
        likeCountLabelBelow.text = "\(0)"
        likeCountLabelBelow.textAlignment = NSTextAlignment.left
        likeCountLabelBelow.font = HSFont.baseRegularFont(15)
        likeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
        return likeCountLabelBelow
    }()
    
    let unLikeButtonBelow : UIButton = {
        let unLikeButtonBelow = UIButton()
        unLikeButtonBelow.setImage(UIImage(named: "Step-on_"), for: UIControlState())
        return unLikeButtonBelow
    }()
    
    let unLikeCountLabelBelow : UILabel = {
        let unLikeCountLabelBelow = UILabel()
        unLikeCountLabelBelow.text = "\(0)"
        unLikeCountLabelBelow.textAlignment = NSTextAlignment.left
        unLikeCountLabelBelow.font = HSFont.baseRegularFont(15)
        unLikeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
        return unLikeCountLabelBelow
    }()
    let divideLineBelow : UIView = {
        let divideLineBelow = UIView()
        divideLineBelow.backgroundColor = UIColor.divideColor()
        return divideLineBelow
    }()
    
    let viewAllCommentLabel : UILabel = {
        let viewAllCommentLabel = UILabel()
        viewAllCommentLabel.text = NSLocalizedString("View all comment", comment: "")
        viewAllCommentLabel.font = UIFont.systemFont(ofSize: SCREEN_WIDTH == 320 ? 13 : 15)
        viewAllCommentLabel.textAlignment = .left
        viewAllCommentLabel.textColor = UIColor.luckyHeaderButtonColor()
        return viewAllCommentLabel
    }()
    
//    let bottomDivideLine : UIView = {
//        let bottomDivideLine = UIView()
//        bottomDivideLine.backgroundColor = UIColor.divideColor()
//        return bottomDivideLine
//    }()
    
    func addCPSubviews() {
        self.addSubview(userAva)
        self.addSubview(nameLabel)
        self.addSubview(signLabel)
        self.addSubview(createTime)
        self.addSubview(contentLabel)
        self.addSubview(likeButton)
        self.addSubview(likeCountLabel)
        self.addSubview(unLikeButton)
        self.addSubview(unLikeCountLabel)
        self.addSubview(divideLine)
        
        self.addSubview(userAvaBelow)
        self.addSubview(nameLabelBelow)
        self.addSubview(signLabelBelow)
        self.addSubview(createTimeBelow)
        self.addSubview(contentLabelBelow)
        self.addSubview(likeButtonBelow)
        self.addSubview(likeCountLabelBelow)
        self.addSubview(unLikeButtonBelow)
        self.addSubview(unLikeCountLabelBelow)
        self.addSubview(viewImageButton)
        self.addSubview(viewImgLabel)
        self.addSubview(viewImageButtonBelow)
        self.addSubview(viewImgLabelBelow)
        self.addSubview(divideLineBelow)
        self.addSubview(viewAllCommentLabel)
//        self.addSubview(bottomDivideLine)
    }
    
    func showBelow() {
        self.nameLabelBelow.isHidden = false
        self.signLabelBelow.isHidden = false
        self.createTimeBelow.isHidden = false
        self.contentLabelBelow.isHidden = false
        self.likeButtonBelow.isHidden = false
        self.likeCountLabelBelow.isHidden = false
        self.unLikeButtonBelow.isHidden = false
        self.unLikeCountLabelBelow.isHidden = false
        self.divideLineBelow.isHidden = false
    }
    
    func removeBelows() {
        self.nameLabelBelow.isHidden = true
        self.signLabelBelow.isHidden = true
        self.createTimeBelow.isHidden = true
        self.contentLabelBelow.isHidden = true
        self.likeButtonBelow.isHidden = true
        self.likeCountLabelBelow.isHidden = true
        self.unLikeButtonBelow.isHidden = true
        self.unLikeCountLabelBelow.isHidden = true
        self.viewImageButtonBelow.isHidden = true
        self.viewAllCommentLabel.isHidden = true
        self.viewImgLabelBelow.isHidden = true
        self.divideLineBelow.isHidden = true
    }
    let iconHeight = 18
    func addCPConstraint() {
        
        self.userAva.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left).offset(SCREEN_WIDTH * 12 / 375)
            make.width.height.equalTo(SCREEN_HEIGHT * 30 / 667)
        }
        
        self.nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.userAva.snp.right).offset(SCREEN_WIDTH * 12 / 375)
        }
        self.createTime.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.nameLabel.snp.bottom)
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
        }
        self.signLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.userAva.snp.bottom)
            make.left.equalTo(self.userAva.snp.right).offset(SCREEN_WIDTH * 12 / 375)
        }
        self.contentLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.userAva.snp.bottom).offset(SCREEN_HEIGHT * 30 / 667)
            make.left.equalTo(self.userAva.snp.left)
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
        }
        self.viewImageButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentLabel.snp.bottom).offset(SCREEN_HEIGHT * 10 / 667)
            make.left.equalTo(self.userAva.snp.left)
            make.width.height.equalTo(SCREEN_WIDTH * 15 / 375)
        }
        self.viewImgLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.viewImageButton.snp.centerY)
            make.left.equalTo(self.viewImageButton.snp.right).offset(SCREEN_WIDTH * 10 / 375)
        }
        self.unLikeCountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.viewImageButton.snp.bottom).offset(SCREEN_WIDTH * 18 / 375)
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
        }
        self.unLikeButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.viewImageButton.snp.bottom).offset(SCREEN_WIDTH * 22 / 375)
            make.right.equalTo(self.unLikeCountLabel.snp.left).offset(-SCREEN_WIDTH * 12 / 375)
            make.width.equalTo(SCREEN_HEIGHT * 18 / 667)
            make.height.equalTo(SCREEN_HEIGHT * 18 / 667)
        }
        self.likeCountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.viewImageButton.snp.bottom).offset(SCREEN_WIDTH * 18 / 375)
            make.right.equalTo(self.unLikeButton.snp.left).offset(-SCREEN_WIDTH * 12 / 375)
        }
        self.likeButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.likeCountLabel.snp.bottom)
            make.right.equalTo(self.likeCountLabel.snp.left).offset(-SCREEN_WIDTH * 12 / 375)
            make.width.equalTo(SCREEN_HEIGHT * 18 / 667)
            make.height.equalTo(SCREEN_HEIGHT * 18 / 667)
        }
        self.divideLine.snp.makeConstraints { (make) in
            make.top.equalTo(self.unLikeButton.snp.bottom).offset(SCREEN_HEIGHT * 20 / 667)
            make.height.equalTo(1)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
        }
        
        //MARK: Below
        
        self.userAvaBelow.snp.makeConstraints { (make) in
            make.top.equalTo(self.divideLine.snp.bottom).offset(SCREEN_WIDTH * 20 / 375)
            make.left.equalTo(self.snp.left).offset(SCREEN_WIDTH * 12 / 375)
            make.height.width.equalTo(SCREEN_HEIGHT * 30/667)
        }
        self.nameLabelBelow.snp.makeConstraints { (make) in
            make.left.equalTo(self.userAvaBelow.snp.right).offset(SCREEN_WIDTH * 12 / 375)
            make.top.equalTo(self.divideLine.snp.bottom).offset(SCREEN_WIDTH * 20 / 375)
        }
        self.createTimeBelow.snp.makeConstraints { (make) in
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
            make.centerY.equalTo(self.nameLabelBelow.snp.centerY)
        }
        self.signLabelBelow.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.userAvaBelow.snp.bottom)
            make.left.equalTo(self.userAvaBelow.snp.right).offset(SCREEN_WIDTH * 12 / 375)
        }
        self.contentLabelBelow.snp.makeConstraints { (make) in
            make.top.equalTo(self.userAvaBelow.snp.bottom).offset(SCREEN_WIDTH * 30 / 375)
            make.left.equalTo(self.snp.left).offset(SCREEN_WIDTH * 12 / 375)
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
        }
        self.viewImageButtonBelow.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentLabelBelow.snp.bottom).offset(SCREEN_HEIGHT * 5 / 667)
            make.left.equalTo(self.snp.left).offset(SCREEN_WIDTH * 12 / 375)
            make.width.height.equalTo(SCREEN_WIDTH * 15 / 375)
        }
        self.viewImgLabelBelow.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.viewImageButtonBelow.snp.centerY)
            make.left.equalTo(self.viewImageButtonBelow.snp.right).offset(SCREEN_WIDTH * 10 / 375)
        }
        self.unLikeCountLabelBelow.snp.makeConstraints { (make) in
            make.top.equalTo(self.viewImageButtonBelow.snp.bottom).offset(SCREEN_WIDTH * 18 / 375)
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
        }
        self.unLikeButtonBelow.snp.makeConstraints { (make) in
            make.top.equalTo(self.viewImageButtonBelow.snp.bottom).offset(SCREEN_WIDTH * 22 / 375)
            make.right.equalTo(self.unLikeCountLabelBelow.snp.left).offset(-SCREEN_WIDTH * 12 / 375)
            make.width.equalTo(SCREEN_HEIGHT * 18 / 667)
            make.height.equalTo(SCREEN_HEIGHT * 18 / 667)
        }
        self.likeCountLabelBelow.snp.makeConstraints { (make) in
            make.top.equalTo(self.viewImageButtonBelow.snp.bottom).offset(SCREEN_WIDTH * 18 / 375)
            make.right.equalTo(self.unLikeButtonBelow.snp.left).offset(-SCREEN_WIDTH * 12 / 375)
        }
        self.likeButtonBelow.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.likeCountLabelBelow.snp.bottom)
            make.right.equalTo(self.likeCountLabelBelow.snp.left).offset(-SCREEN_WIDTH * 12 / 375)
            make.width.equalTo(SCREEN_HEIGHT * 18 / 667)
            make.height.equalTo(SCREEN_HEIGHT * 18 / 667)
        }
        self.divideLineBelow.snp.makeConstraints { (make) in
            make.top.equalTo(self.unLikeButtonBelow.snp.bottom).offset(SCREEN_HEIGHT * 20 / 667)
            make.height.equalTo(1)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
        }
        
        self.viewAllCommentLabel.snp.makeConstraints { (make) in
            make.top.equalTo(divideLineBelow.snp.bottom).offset(SCREEN_HEIGHT * 16 / 667)
            make.left.equalTo(self.snp.left).offset(SCREEN_WIDTH * 12 / 375)
        }
//        self.bottomDivideLine.snp.makeConstraints { (make) in
//            make.top.equalTo(self.viewAllCommentLabel.snp.bottom).offset(16)
//            make.height.equalTo(1)
//            make.width.equalTo(SCREEN_WIDTH)
//        }
    }
    
    func addTargets() {
        self.likeButton.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
        self.unLikeButton.addTarget(self, action: #selector(unlikeAction), for: .touchUpInside)
        self.likeButtonBelow.addTarget(self, action: #selector(likeActionBelow), for: .touchUpInside)
        self.unLikeButtonBelow.addTarget(self, action: #selector(unlikeActionBelow), for: .touchUpInside)
        self.viewImageButton.addTarget(self, action: #selector(viewBigImage), for: .touchUpInside)
        self.viewImageButtonBelow.addTarget(self, action: #selector(viewBelowBigImage), for: .touchUpInside)
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(viewBigImage))
        self.viewImgLabel.addGestureRecognizer(tap)
        let belowTap = UITapGestureRecognizer()
        belowTap.addTarget(self, action: #selector(viewBelowBigImage))
        self.viewImgLabelBelow.addGestureRecognizer(belowTap)
        let seeOriginal = UITapGestureRecognizer()
        seeOriginal.addTarget(self, action: #selector(seeOriginalTap))
        self.viewAllCommentLabel.addGestureRecognizer(seeOriginal)
    }
    
    @objc func seeOriginalTap() {
        self.delegate?.seeOriginalTapAction()
    }
    
    @objc func viewBigImage() {
        self.delegate?.prelookDidClickViewImg()
    }
    
    @objc func viewBelowBigImage() {
        self.belowDelegate?.prelookBelowDidClickViewImg()
    }
    
    @objc func likeAction() {
        self.delegate?.preLookDidClickLike()
    }
    @objc func unlikeAction() {
        self.delegate?.preLookDidClickDislike()
    }
    @objc func likeActionBelow(){
        self.belowDelegate?.preLookBelowDidClickLike()
    }
    @objc func unlikeActionBelow(){
        self.belowDelegate?.preLookBelowDidClickDislike()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
