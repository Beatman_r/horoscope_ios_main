//
//  SignoutCell.swift
//  Horoscope
//
//  Created by Wang on 17/2/15.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class SignoutCell: BaseCardCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.cornerBackView?.addSubview(contentLab)
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.contentView.snp.right).offset(-BaseCardCell.cellPadding)
            make.top.equalTo(self.contentView.snp.top).offset(BaseCardCell.cellPadding)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        self.cornerBackView?.backgroundColor = UIColor.clear
        self.addConstrains()
    }
    
    func addConstrains() {
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.centerY.equalTo(self.cornerBackView!.snp.centerY)
        }
    }
    
    let contentLab:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = UIColor.commonPinkColor()
        label.font=HSFont.baseRegularFont(18)
        return label
    }()
    
    
    func renderCell() {
        self.contentLab.text = NSLocalizedString("Sign out", comment: "")
     }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK:Create
    static let cellId = "SignoutCell"
    class func dequeueReusableCell(_ tableView: UITableView,
                                   cellForRowAtIndexPath indexPath: IndexPath) -> SignoutCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId,
                                                               for: indexPath) as! SignoutCell
        return cell
    }
    
    class func calculateHeight(_ content:String) ->CGFloat{
        return 63
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
