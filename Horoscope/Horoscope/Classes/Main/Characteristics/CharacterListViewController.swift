//
//  CharacterListViewController.swift
//  Horoscope
//
//  Created by Wang on 17/1/13.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class CharacterListViewController: UIViewController,YSLContainerViewControllerDelegate,eachCardDelegate {
    
    var segments : YSLContainerViewController?
    var heightArr:[CGFloat]?
    var header:UIView?
    var imageHeaderView:partOneView?
    let cellLabelHeight = UIScreen.main.bounds.height-(HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()/15*8)-64+49
    var viewList : [UIViewController] = []
    
    let zodiacArr :[String] = ["aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn","aquarius","pisces"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Characteristics", comment: "")
        self.view.backgroundColor=UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha:1)
        self.addBackBtn()
        addSegmentSectionHeader()
        createTableHeaderAndSectionHeader()
        
        // Do any additional setup after loading the view.
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    @objc func popView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    var charaModel : CharacteristicsModel?
    
    func addSegmentSectionHeader() {
        heightArr=[cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight,
                   cellLabelHeight]
        var controllerArr:[UIViewController]? = [UIViewController]()
        for index in 0..<self.zodiacArr.count{
            let name = self.zodiacArr[index]
            if self.horoScopeName == name{
                self.currentIndex=Int32(index)
            }
            let vc = EachCharacterDetailViewController.create(name,index:index)
            vc.title = NSLocalizedString("\(name.capitalized)", comment: "")
            vc.delegate=self
            controllerArr?.append(vc)
        }
        
        self.segments = YSLContainerViewController.init(controllers: controllerArr, topBarHeight: 64,startheight: 0, parentViewController: self)
        self.segments?.delegate = self
        self.segments?.menuItemFont = HSFont.baseMediumFont(20)
          segments?.menuIndicatorColor=UIColor.luckyPurpleTitleColor()
        self.segments?.menuItemTitleColor=UIColor.luckyPurpleContentColor()
        self.segments?.menuBackGroudColor=UIColor.baseCardBackgroundColor()
        self.segments?.menuItemSelectedTitleColor=UIColor.luckyPurpleTitleColor()
        self.segments?.startIndex = currentIndex
        self.view.addSubview(self.segments!.view)
     
    }
    func createTableHeaderAndSectionHeader() {
//        imageHeaderView = partOneView.init(frame: CGRectMake(0, 0, HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth(), HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()/15*8))
//        if self.horoScopeName?.count > 0 {
//            let name = self.horoScopeName ?? ""
//            imageHeaderView?.backImageView.image = UIImage(named: ZodiacModel.getCharacterTopImage(name))
//        }else {
//            imageHeaderView?.backImageView.image = UIImage(named:ZodiacModel.getCharacterTopImage("aries"))
//        }
       }
    
    //MARK:HeightDelegate
    func returnRowHeight(_ rowHeight: CGFloat, tag: Int) {
//        if rowHeight+45 >= heightArr![tag] {
//            heightArr![tag]=rowHeight+45
//        }
//        if tag == Int(currentIndex){
//            self.scheight=heightArr![tag]
//            segments?.setHeight(self.scheight ?? 0, startpositon: 44)
//        }
    }
    
    var scheight:CGFloat?
    var currentIndex:Int32 = 0
    func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {
        let currentName = self.zodiacArr[index]
        AnaliticsManager.sendEvent(AnaliticsManager.characteristics_show, data: ["signs":currentName])
        self.scheight=heightArr![index]
        self.currentIndex=Int32(index)
        let name = ZodiacModel.getZodiacName(index)
        imageHeaderView?.backImageView.image = UIImage(named: ZodiacModel.getCharacterTopImage(name))
       // segments?.setHeight(self.scheight ?? 0, startpositon: 44)
        controller.viewWillAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   var horoScopeName:String?
    class func create(_ name:String) ->CharacterListViewController{
        let vc = CharacterListViewController()
        vc.horoScopeName=name
        return vc
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    
}
