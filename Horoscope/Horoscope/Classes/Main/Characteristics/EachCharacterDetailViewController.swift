//
//  EachCharacterDetailViewController.swift
//  Horoscope
//
//  Created by Wang on 17/1/13.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class eachCharacterCardViewmodel:NSObject{
    init(name:String) {
        super.init()
        self.horoName=name
    }
    var charaModel : CharacteristicsModel?
    var horoName:String?
    
    func loadDetail() {
        self.charaModel = Characteristics.getCharacteristicsWithName(self.horoName ?? "")
    }
}

class EachCharacterDetailViewController: BaseViewController {
    weak var delegate:eachCardDelegate?
    var viewModel:eachCharacterCardViewmodel?
    override func viewDidLoad() {
        super.viewDidLoad()
    //  self.view.backgroundColor = UIColor.baseCardBackgroundColor()
        self.baseTableView?.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height-64-49)
        initViewmodel()
        self.getTotalHeight()
   }
    
    func initViewmodel(){
         viewModel=eachCharacterCardViewmodel.init(name: self.horoName ?? "")
         viewModel?.loadDetail()
         self.baseTableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate var totalHeight:CGFloat=0
    fileprivate var adHeght:CGFloat=0
    func getTotalHeight(){
    }
    
    var horoIndex:Int?
    var horoName:String?
    class func create(_ name:String,index:Int) -> EachCharacterDetailViewController{
        let vc = EachCharacterDetailViewController()
        vc.horoName = name
        vc.horoIndex = index
        return vc
    }
    
    override func registerCell() {
        self.baseTableView?.register(CharaTableViewCell.self, forCellReuseIdentifier: "CharaTableViewCell")
        self.baseTableView?.register(CharaHeaderCell.self, forCellReuseIdentifier: "CharaHeaderCell")
    }
    
    //MARK:TableViewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let titleCell = tableView.dequeueReusableCell(withIdentifier: "CharaHeaderCell", for: indexPath) as! CharaHeaderCell
            titleCell.model = self.viewModel?.charaModel
            return titleCell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CharaTableViewCell", for: indexPath) as! CharaTableViewCell
            cell.renderCell(NSLocalizedString("Positive", comment: ""), model: self.viewModel?.charaModel)
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CharaTableViewCell", for: indexPath) as! CharaTableViewCell
            cell.renderCell(NSLocalizedString("Negative", comment: ""), model: self.viewModel?.charaModel)
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CharaTableViewCell", for: indexPath) as! CharaTableViewCell
            cell.renderCell(NSLocalizedString("Description", comment: ""), model: self.viewModel?.charaModel)
            return cell
        }
       
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return CharaHeaderCell.calculateHeight()
        case 1:
            if self.viewModel?.charaModel?.positive == "" {
                return 0
            }else{
                return  CharaTableViewCell.calculateHeight(self.viewModel?.charaModel?.positive ?? "")+16
            }
        case 2:
            if self.viewModel?.charaModel?.negative == "" {
                return 0
            }else{
                return CharaTableViewCell.calculateHeight(self.viewModel?.charaModel?.negative ?? "")+16
            }
        case 3:
            if self.viewModel?.charaModel?.descriptions == "" {
                return 0
            }else{
                return CharaTableViewCell.calculateHeight(self.viewModel?.charaModel?.descriptions ?? "")+16
            }
        default :
            return 0
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
