//
//  CharaTableViewCell.swift
//  Horoscope
//
//  Created by Beatman on 16/11/25.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit


class CharaTableViewCell: BaseCardCell {
   
   var describeLabel : UILabel!
   var contentLabel : UILabel!
   override func awakeFromNib() {
      super.awakeFromNib()
   }
   
   override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      self.backgroundColor = UIColor.clear
      self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
      let colorView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-10, height: 25/2*3))
      colorView.backgroundColor = UIColor.clear
      
      describeLabel = UILabel()
      describeLabel.frame = CGRect(x: 12, y: 0, width: UIScreen.main.bounds.width-10,height: 25/2*3)
      describeLabel.backgroundColor = UIColor.clear
      describeLabel.font = HSFont.baseRegularFont(20)
      describeLabel.textColor = UIColor.luckyPurpleTitleColor()
      describeLabel.textAlignment = NSTextAlignment.left
      colorView.addSubview(describeLabel)
      
      contentLabel = UILabel()
      contentLabel.numberOfLines = 0
      contentLabel.font = HSFont.baseLightFont(18)
      contentLabel.textAlignment = NSTextAlignment.left
      contentLabel.textColor = UIColor.luckyPurpleTitleColor()
      contentLabel.sizeToFit()
      
      self.cornerBackView?.addSubview(colorView)
      self.cornerBackView?.addSubview(contentLabel)
      
      contentLabel.snp.makeConstraints { (make) in
         make.top.equalTo(self.describeLabel.snp.bottom)
         make.bottom.equalTo(self.cornerBackView!.snp.bottom).offset(-8)
         make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
         make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
      }
      self.cornerBackView?.snp.remakeConstraints { (make) in
         make.left.equalTo(self.contentView.snp.left).offset(BaseCardCell.cellPadding)
         make.right.equalTo(self.contentView.snp.right).offset(-BaseCardCell.cellPadding)
         make.top.equalTo(self.contentView.snp.top).offset(BaseCardCell.cellPadding)
         make.bottom.equalTo(self.contentView.snp.bottom)
      }
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   func renderCell(_ title:String,model:CharacteristicsModel?) {
      self.describeLabel.text = title
      let font = HSFont.baseLightFont(18)
      switch title {
      case "Positive":
         contentLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: model?.positive ?? "", font: font)
      case "Negative":
         contentLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: model?.negative ?? "", font: font)
      case "Description":
         contentLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: model?.descriptions ?? "", font: font)
      default:
         break
      }
   }
   
   class func calculateHeight(_ content:String)->CGFloat{
      let font = HSFont.baseLightFont(18)
      return HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content , fontSize: 18, font:font, width: UIScreen.main.bounds.size.width-44, multyplyLineSpace: 1.25)+37.5+16
   }
   
   override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
      
      // Configure the view for the selected state
   }
   
}
