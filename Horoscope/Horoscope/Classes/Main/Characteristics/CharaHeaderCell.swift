//
//  CharaHeaderCell.swift
//  Horoscope
//
//  Created by Beatman on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class CharaHeaderCell: BaseCardCell {
    
    var titleLabel : UILabel!
    var elementTitle : UILabel!
    var elementDetail : UILabel!
    var qualityTitle : UILabel!
    var qualityDetail : UILabel!
    var colorTitle : UILabel!
    var colorDetail : UILabel!
    var rulingPlanetTitle : UILabel!
    var rulingPlanetDetail : UILabel!
    var rulingHouseTitle : UILabel!
    var rulingHouseDetail : UILabel!
    var tarotCardTitle : UILabel!
    var tarotCardDetail : UILabel!
    var colorView : UIView!
    var avaImg : UIImageView!
    
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.createHeaderUI()
        self.addLabelsToView()
        self.addLabelConstraints()
    }
    
    func createHeaderUI() {
        colorView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-10, height: 25/2*3))
        colorView.backgroundColor = UIColor.clear
        
        self.avaImg = UIImageView()
        self.titleLabel = self.createHeaderViewLabels("Taurus at a glance")
        self.titleLabel.font = HSFont.baseRegularFont(20)
        self.elementTitle = self.createHeaderViewLabels("Element:")
        self.elementDetail = self.createHeaderViewLabels("Earth")
        self.qualityTitle = self.createHeaderViewLabels("Quality:")
        self.qualityDetail = self.createHeaderViewLabels("Fixed")
        self.colorTitle = self.createHeaderViewLabels("Color:")
        self.colorDetail = self.createHeaderViewLabels("Earthy greens and browns")
        self.rulingPlanetTitle = self.createHeaderViewLabels("Ruling Planet:")
        self.rulingPlanetDetail = self.createHeaderViewLabels("Venus")
        self.rulingHouseTitle = self.createHeaderViewLabels("Ruling House:")
        self.rulingHouseDetail = self.createHeaderViewLabels("2nd House of Material Possessions")
        self.rulingHouseDetail.numberOfLines = 0
        self.tarotCardTitle = self.createHeaderViewLabels("Tarot Card:")
        self.tarotCardDetail = self.createHeaderViewLabels("The Hierophant")
    }
    
    func addLabelsToView() {
        self.cornerBackView?.addSubview(colorView)
        self.colorView!.addSubview(titleLabel)
        self.cornerBackView!.addSubview(elementTitle)
        self.cornerBackView!.addSubview(elementDetail)
        self.cornerBackView!.addSubview(qualityTitle)
        self.cornerBackView!.addSubview(qualityDetail)
        self.cornerBackView!.addSubview(colorTitle)
        self.cornerBackView!.addSubview(colorDetail)
        self.cornerBackView!.addSubview(rulingPlanetTitle)
        self.cornerBackView!.addSubview(rulingPlanetDetail)
        self.cornerBackView!.addSubview(rulingHouseTitle)
        self.cornerBackView!.addSubview(rulingHouseDetail)
        self.cornerBackView!.addSubview(tarotCardTitle)
        self.cornerBackView!.addSubview(tarotCardDetail)
        self.cornerBackView?.addSubview(avaImg)
    }
    
    func addLabelConstraints() {
        let labWidth = self.screenWidth == 320 ? self.screenWidth * 220/375 : self.screenWidth * 250/375
        
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.contentView.snp.right).offset(-BaseCardCell.cellPadding)
            make.top.equalTo(self.contentView.snp.top).offset(BaseCardCell.cellPadding)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo((self.colorView?.snp.top)!).offset(12)
            make.left.equalTo((self.colorView?.snp.left)!).offset(10)
            make.width.equalTo(labWidth)
        }
        self.elementTitle.snp.makeConstraints { (make) in
            make.top.equalTo(colorView.snp.bottom).offset(18)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(10)
        }
        self.elementDetail.snp.makeConstraints { (make) in
            make.top.equalTo(elementTitle.snp.top)
            make.left.equalTo(self.elementTitle.snp.right).offset(10)
            make.width.equalTo(labWidth)
        }
        self.qualityTitle.snp.makeConstraints { (make) in
            make.top.equalTo(elementDetail.snp.bottom).offset(7)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(10)
        }
        self.qualityDetail.snp.makeConstraints { (make) in
            make.top.equalTo(qualityTitle.snp.top)
            make.left.equalTo(self.qualityTitle.snp.right).offset(10)
            make.width.equalTo(labWidth)
        }
        self.colorTitle.snp.makeConstraints { (make) in
            make.top.equalTo(qualityDetail.snp.bottom).offset(7)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(10)
        }
        self.colorDetail.snp.makeConstraints { (make) in
            make.top.equalTo(colorTitle.snp.top)
            make.left.equalTo(self.colorTitle.snp.right).offset(10)
           make.width.equalTo(labWidth)
        }
        self.rulingPlanetTitle.snp.makeConstraints { (make) in
            make.top.equalTo(colorDetail.snp.bottom).offset(7)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(10)
        }
        self.rulingPlanetDetail.snp.makeConstraints { (make) in
            make.top.equalTo(rulingPlanetTitle.snp.top)
            make.left.equalTo(self.rulingPlanetTitle.snp.right).offset(10)
            make.width.equalTo(labWidth)
        }
        self.rulingHouseTitle.snp.makeConstraints { (make) in
            make.top.equalTo(rulingPlanetDetail.snp.bottom).offset(7)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(10)
        }
        self.rulingHouseDetail.snp.makeConstraints { (make) in
            make.top.equalTo(rulingHouseTitle.snp.top)
            make.left.equalTo(rulingHouseTitle.snp.right).offset(10)
            make.width.equalTo(labWidth)
        }
        self.tarotCardTitle.snp.makeConstraints { (make) in
            make.top.equalTo(rulingHouseDetail.snp.bottom).offset(7)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(10)
        }
        self.tarotCardDetail.snp.makeConstraints { (make) in
            make.top.equalTo(tarotCardTitle.snp.top)
            make.left.equalTo(tarotCardTitle.snp.right).offset(10)
            make.width.equalTo(labWidth)
        }
        self.avaImg.snp.makeConstraints { (make) in
            make.top.equalTo((self.cornerBackView?.snp.top)!).offset(10)
            make.right.equalTo((self.cornerBackView?.snp.right)!).offset(-10)
            make.width.equalTo(self.screenWidth * 100/375)
            make.height.equalTo(self.screenWidth * 100/375)
        }
    }
    
    var model:CharacteristicsModel?{
        didSet{
           renderCell()
        }
     }
    func renderCell() {
        avaImg.image = UIImage(named: (model?.avaImg) ?? "")
        titleLabel.text = model?.charaTitle
        elementDetail.text = model?.element
        qualityDetail.text = model?.quality
        colorDetail.text = model?.color
        rulingPlanetDetail.text = model?.rulingPlanet
        rulingHouseDetail.text = model?.rulingHouse
        tarotCardDetail.text = model?.tarotCard
    }
    
    func createHeaderViewLabels(_ text:String) -> UILabel {
        let lab = UILabel()
        lab.text = text
        lab.textAlignment = NSTextAlignment.left
        lab.textColor = UIColor.luckyPurpleTitleColor()
        lab.font = HSFont.baseLightFont(self.screenWidth == 320 ? 15 : 18)
        lab.numberOfLines = 0
        lab.sizeToFit()
        return lab
    }
    
    class func calculateHeight() ->CGFloat{
        if UIScreen.main.bounds.width == 320 {
            return UIScreen.main.bounds.height * 260/667
        }else if UIScreen.main.bounds.width == 375 {
            return UIScreen.main.bounds.height * 255/667
        }else if UIScreen.main.bounds.width == 414{
            return UIScreen.main.bounds.height * 245/667
        }
        return 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
