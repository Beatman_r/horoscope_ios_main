//
//  TarotPrepareController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import pop

class TarotPrepareController: TarotBaseViewController,cardPaningDelegate {
    
    var topic : Int!
    
    let shuffleButton = UIButton()
    let describeLabel = UILabel()
    var landscapeCardBack = TarotMainCard.init(image: UIImage(named: "tarot_card_land_"))
    var cardSetList : [UIImageView] = []
    var originCenterList : [CGPoint] = []
    var originRotateList : [CGFloat] = []
    var cardForSelectList : [UIImageView] = []
    var resultModelList : [TarotCardModel] = []
    let adContainerView  = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = self.createResultModel()
        createPrepareSubviews()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshCardSet), name: NSNotification.Name(rawValue: "shuffleFinished"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(shuffleTheSet), name: NSNotification.Name(rawValue: "setCreated"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.leftCardPool.frame != CGRect.zero && self.leftHasDefine == false {
            self.leftCardAnimate()
            self.centerCardPool.isUserInteractionEnabled = true
        }else if self.rightCardPool.frame != CGRect.zero && self.rightHasDefine == false {
            self.rightCardAnimate()
            self.centerCardPool.isUserInteractionEnabled = true
            self.leftCardPool.isUserInteractionEnabled = true
            self.rightCardPool.isUserInteractionEnabled = true
        }else if self.rightCardPool.frame != CGRect.zero && self.rightHasDefine == true {
            self.centerCardPool.isUserInteractionEnabled = true
            self.leftCardPool.isUserInteractionEnabled = true
            self.rightCardPool.isUserInteractionEnabled = true
        }
        
        if self.rightHasDefine == true && self.tarotAgainButton.frame == CGRect.zero{
            self.tarotShareButton.setImage(UIImage(named: "tarot_share_"), for: UIControlState())
            self.tarotShareButton.addTarget(self, action: #selector(tarotShareAction), for: .touchUpInside)
            self.tarotAgainButton.setImage(UIImage(named: "tarot_again_"), for: UIControlState())
            self.tarotAgainButton.addTarget(self, action: #selector(selectAgain), for: .touchUpInside)
            
            self.view.addSubview(self.tarotShareButton)
            self.view.addSubview(self.tarotAgainButton)
            self.tarotShareButton.snp.makeConstraints { (make) in
                make.top.equalTo(self.centerCardPool.snp.bottom).offset(70)
                make.right.equalTo(self.view.snp.centerX).offset(self.screenWidth * 22 / 375)
                make.width.equalTo(self.screenWidth * 60*3.4/375)
                make.height.equalTo(self.screenHeight * 60/667)
            }
            
            self.tarotAgainButton.snp.makeConstraints { (make) in
                make.top.equalTo(self.tarotShareButton.snp.top)
                make.left.equalTo(self.view.snp.centerX).offset(-(self.screenWidth * 22 / 375))
                make.width.equalTo(self.screenWidth * 60*3.4/375)
                make.height.equalTo(self.screenHeight * 60/667)
            }
        }
    }
    
    func createResultModel() -> [TarotCardModel] {
        var num : UInt32 = 77
        for _ in 0..<3 {
            let count = arc4random()%num
            let cardName = self.cardList[Int(count)]
            let resultModel = TarotDataSource.getResult(cardName, topic: self.topic)
            self.resultModelList.append(resultModel)
            self.cardList.remove(at: Int(count))
            num -= 1
        }
        return self.resultModelList
    }
    
    //MARK: Animation Function
    @objc func refreshCardSet() {
        let delayTime = DispatchTime.now() + Double(Int64(1.01 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            for count in 0...46 {
                self.cardSetList[count].removeFromSuperview()
            }
            self.cardSetList.removeSubrange(0...46)
        }
        for count in 0...49 {
            UIView.animate(withDuration: 0.5, animations: {
                self.cardSetList[count].center = self.landscapeCardBack.center
                }, completion: { (true) in
                    UIView.animate(withDuration: 0.5, animations: {
                        self.cardSetList[count].transform = CGAffineTransform(rotationAngle: 0)
                        }, completion: { (true) in
                            UIView.animate(withDuration: 0.6, animations: {
                                self.describeLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(NSLocalizedString("The shuffle is done, keep concentrating on and repeating your question in mind. Remember that sincerity can excite Tarot's mysterious force.", comment: ""), space: 1.2)
                                self.describeLabel.alpha = 1
                                self.shuffleButton.alpha = 1
                                self.describeLabel.textAlignment = .center
                                self.buttonTitleLabel.text = NSLocalizedString("Cut Cards", comment: "")
                            }, completion: { (true) in
                                self.view.layoutIfNeeded()
                                let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: { 
                                    self.shuffleButton.isUserInteractionEnabled = true
                                })
                            }) 
                    })
            })
        }
    }
    
    @objc func shuffleTheSet() {
        self.shuffleButton.isUserInteractionEnabled = false
        let delayTime = DispatchTime.now() + Double(Int64(0.9 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "shuffleFinished"), object: nil)
        }
        for count in 0...49 {
            UIView.animate(withDuration: 0.8, animations: {
                let cardRotateAnimation = POPBasicAnimation(propertyNamed: kPOPLayerRotation)
                cardRotateAnimation?.fromValue = 0
                cardRotateAnimation?.toValue = (CGFloat(arc4random()).truncatingRemainder(dividingBy: CGFloat(Double.pi * 2.0))) + CGFloat(Double.pi*1.0)
                cardRotateAnimation?.duration = 0.8
                cardRotateAnimation?.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                self.cardSetList[count].layer.pop_add(cardRotateAnimation, forKey: "cardRotation")
                self.cardSetList[count].frame.origin = CGPoint(x: CGFloat(arc4random()).truncatingRemainder(dividingBy: ((self.screenWidth * 250/414) - 0 + 1)) + 0, y: CGFloat(arc4random()).truncatingRemainder(dividingBy: ((self.screenHeight * 300/736) - 74 + 1)) + 74)
                self.view.layoutIfNeeded()
                self.landscapeCardBack.removeFromSuperview()
            })
        }
    }
    
    func beginDragging(_ img: CardForSelect) {
        UIView.animate(withDuration: 0.5, animations: {
            img.transform = CGAffineTransform(rotationAngle: 0)
        }) 
    }
    
    func cardPoolAnimate(_ cardPool : UIImageView) {
        UIView.animate(withDuration: 2, delay: 0, options: [.repeat], animations: {
            cardPool.alpha = 0.6
        }) { (true) in
        }
    }
    
    //add ad
    func testContain(_ img:CardForSelect,originCenter:CGPoint){
        self.selectedCardPool.layoutIfNeeded()
        switch self.leftCardPool.tag {
        case 0:
            if self.leftCardPool.alpha < 0.2 {
                self.cardPoolAnimate(self.leftCardPool)
            }
        case 1:
            if self.centerCardPool.alpha < 0.2 {
                self.cardPoolAnimate(self.centerCardPool)
            }
        case 2:
            if self.rightCardPool.alpha < 0.2 {
                self.cardPoolAnimate(self.rightCardPool)
            }
        default:
            return
        }
        if img.panGesture.state == UIGestureRecognizerState.ended {
            if self.leftCardPool.frame.contains(img.center) && self.leftCardPool.tag == 0{
                self.putInCard(img, cardPool: self.leftCardPool, title: "Select 2nd Card", tag: 1)
                self.leftCardPool.alpha = 1
            }else if self.centerCardPool.frame.contains(img.center) && self.leftCardPool.tag == 1{
                self.putInCard(img, cardPool: self.centerCardPool, title: "Select 3rd Card", tag: 2)
                self.centerCardPool.alpha = 1
            }else if self.rightCardPool.frame.contains(img.center) && self.leftCardPool.tag == 2{
                self.putInCard(img, cardPool: self.rightCardPool, title: "Open Cards", tag: 3)
                self.rightCardPool.alpha = 1
                //go Detail
                let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                    UIView.animate(withDuration: 0.8, animations: {
                        for count in 0...20{
                            if self.view.subviews.contains(self.cardForSelectList[count]){
                                self.cardForSelectList[count].alpha = 0
                                self.cardForSelectList[count].center.y = 0
                            }
                            self.selectedCardPool.snp.updateConstraints({ (make) in
                                make.top.equalTo(self.view.snp.top).offset(self.screenWidth == 320 ? 90 :104)
                            })
                            self.shuffleButton.snp.remakeConstraints({ (make) in
                                make.bottom.equalTo(self.view.snp.bottom).offset(-36)
                                make.centerX.equalTo(self.view.snp.centerX)
                                make.width.equalTo(self.describeLabel).offset(-20)
                                make.height.equalTo(self.screenHeight * 65/736)
                            })
                            self.selectedCardPool.alpha = 0
                            self.describeLabel.alpha = 0
                            self.shuffleButton.alpha = 0
                            self.view.layoutIfNeeded()
                        }
                        }, completion: { (true) in
                            for count in 0...20{
                                if self.view.subviews.contains(self.cardForSelectList[count]){
                                    self.cardForSelectList[count].removeFromSuperview()
                                }
                            }
                            UIView.animate(withDuration: 1.5, animations: {
                                self.describeLabel.alpha = 1
                                self.shuffleButton.alpha = 1
                                self.describeLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(NSLocalizedString("Cards have already been selected.\nOpen each card following the instruction and see what Tarot interprets.", comment: ""),space: 1.2)
                                self.describeLabel.textAlignment = .center
                            })
                            self.cardForSelectList.removeAll()
                            self.view.layoutIfNeeded()
                            //show ad
                            self.adContainerView.isHidden = false
                            self.buttonTitleLabel.text = NSLocalizedString("Open Cards", comment: "")
                            self.shuffleButton.isUserInteractionEnabled = true
                    })
                })
            }else {
                UIView.animate(withDuration: 0.2, animations: {
                    img.center = self.originCenterList[img.tag]
                    img.transform = CGAffineTransform(rotationAngle: self.originRotateList[img.tag])
                })
            }
        }
    }
    
    func putInCard(_ img:UIImageView,cardPool:UIImageView,title:String,tag:Int) {
        img.removeFromSuperview()
        cardPool.layer.removeAllAnimations()
        self.leftCardPool.tag = tag
        buttonTitleLabel.text = title
    }
    
    func spreadCard() {
        for count in 0...1 {
            self.cardSetList[count].removeFromSuperview()
            self.cardSetList.remove(at: count)
        }
        self.shuffleButton.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, animations: {
            let cardRotateAnimation = POPBasicAnimation(propertyNamed: kPOPLayerRotation)
            cardRotateAnimation?.toValue = CGFloat(Double.pi / 2)
            cardRotateAnimation?.duration = 0.5
            cardRotateAnimation?.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            self.cardSetList.first?.layer.pop_add(cardRotateAnimation, forKey: nil)
            self.shuffleButton.snp.remakeConstraints({ (make) in
                make.top.equalTo(self.view.snp.bottom)
                make.centerX.equalTo(self.view.snp.centerX)
                make.width.equalTo(self.describeLabel).offset(-20)
                make.height.equalTo(self.screenHeight * 65/736)
            })
            self.describeLabel.alpha = 0
            self.cardSetList.first?.frame.origin = CGPoint(x: -5, y: self.screenHeight * 150/667)
            self.view.layoutIfNeeded()
        }, completion: { (true) in
            self.cardSetList.first?.removeFromSuperview()
            _ = self.createCardPool()
            self.view.layoutIfNeeded()
            for count in 0...20{
                
                let cardForSelect = CardForSelect.init(frame: CGRect(x: 0, y: self.screenHeight * 120/667, width: self.screenWidth * 92 / 375, height: self.screenHeight * 161/736))
                cardForSelect.cardBackImg.frame = CGRect(x: 0, y: 0, width: self.screenWidth * 92 / 375, height: self.screenHeight * 161/736)
                cardForSelect.tag = count
                cardForSelect.delegate = self
                self.view.addSubview(cardForSelect)
                self.cardForSelectList.append(cardForSelect)
                UIView.animate(withDuration: 1, animations: {
                    self.describeLabel.alpha = 0
                })
                UIView.animate(withDuration: 1.5, animations: {
                    let r = self.screenWidth * 1.0
                    let mX = self.screenWidth * 0.4
                    let a : CGFloat = asin(mX/r)
                    var offRotate = a/(CGFloat(21)/2.0)
                    if count > 10 {
                        offRotate = -offRotate
                    }else if count == 10{
                        offRotate = 0
                    }
                    let rota = CGFloat(abs(count - 10)) * offRotate
                    self.originRotateList.append(rota)
                    let offX = mX - r * sin(rota)
                    var offY = r * cos(rota) - r * cos(a)
                    if count > 10 {
                        offY = r * cos(rota) - r*cos(a)
                    }
                    
                    self.cardForSelectList[count].center = CGPoint(x: offX + self.screenWidth * 40/375, y: self.screenHeight * 170/667+offY)
                    self.cardForSelectList[count].transform = CGAffineTransform(rotationAngle: rota)
                    let originCenter = self.cardForSelectList[count].center
                    self.originCenterList.append(originCenter)
                    }, completion: { (true) in
                        UIView.animate(withDuration: 1, animations: {
                            self.selectedCardPool.alpha = 1
                            self.leftCardPool.alpha = 0.1
                            self.centerCardPool.alpha = 0.1
                            self.rightCardPool.alpha = 0.1
                            self.describeLabel.alpha = 1
                            self.describeLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(NSLocalizedString("Follow your heart, draw 3 cards from the deck and drag them into the box below in turn.", comment: ""),space: 1.2)
                            self.describeLabel.textAlignment = .center
                        })
                })
            }
        }) 
    }
    
    func orderSelectedCard() {
        self.selectedCardPool.removeFromSuperview()
        UIView.animate(withDuration: 0.8, animations: {
            self.leftCardPool.snp.remakeConstraints({ (make) in
                make.centerX.equalTo(self.view.snp.centerX).offset(-(self.screenWidth * 110/375))
                make.centerY.equalTo(self.view.snp.top).offset(self.screenHeight * 167/667)
                make.width.equalTo(self.screenWidth * 100/375)
                make.height.equalTo(self.screenHeight * 167/667)
            })
            self.rightCardPool.snp.remakeConstraints({ (make) in
                make.centerX.equalTo(self.view.snp.centerX).offset(self.screenWidth * 110/375)
                make.centerY.equalTo(self.view.snp.top).offset(self.screenHeight * 167/667)
                make.width.equalTo(self.screenWidth * 100/375)
                make.height.equalTo(self.screenHeight * 167/667)
            })
            self.centerCardPool.snp.remakeConstraints({ (make) in
                make.centerY.equalTo(self.view.snp.centerY).offset(self.screenHeight * 84/667)
                make.centerX.equalTo(self.view.snp.centerX)
                make.width.equalTo(self.screenWidth * 100/375)
                make.height.equalTo(self.screenHeight * 167/667)
            })
            self.describeLabel.alpha = 0
            self.shuffleButton.alpha = 0
            self.view.layoutIfNeeded()
        }, completion: { (true) in
            self.describeLabel.removeFromSuperview()
            self.shuffleButton.removeFromSuperview()
            let pastLabel = self.createTimeLabel(NSLocalizedString("Past", comment: ""))
            let presentLabel = self.createTimeLabel(NSLocalizedString("Present", comment: ""))
            let futureLabel = self.createTimeLabel(NSLocalizedString("Future", comment: ""))
            self.view.addSubview(pastLabel)
            self.view.addSubview(presentLabel)
            self.view.addSubview(futureLabel)
            
            pastLabel.snp.makeConstraints({ (make) in
                make.top.equalTo(self.centerCardPool.snp.centerY).offset(self.screenHeight * 100/667)
                make.centerX.equalTo(self.centerCardPool.snp.centerX)
                make.width.equalTo(self.screenWidth * 120/375)
                make.height.equalTo(24)
            })
            presentLabel.snp.makeConstraints({ (make) in
                make.top.equalTo(self.leftCardPool.snp.centerY).offset(self.screenHeight * 100/667)
                make.centerX.equalTo(self.leftCardPool.snp.centerX)
                make.width.equalTo(self.screenWidth * 120/375)
                make.height.equalTo(24)
            })
            futureLabel.snp.makeConstraints({ (make) in
                make.top.equalTo(self.rightCardPool.snp.centerY).offset(self.screenHeight * 100/667)
                make.centerX.equalTo(self.rightCardPool.snp.centerX)
                make.width.equalTo(self.screenWidth * 120/375)
                make.height.equalTo(24)
            })
            UIView.animate(withDuration: 0.5, animations: {
                pastLabel.alpha = 1
                presentLabel.alpha = 1
                futureLabel.alpha = 1
                }, completion: { (true) in
                    self.centerCardAnimate()
                    let centerTap = UITapGestureRecognizer()
                    centerTap.addTarget(self, action: #selector(self.centerCardOnClick))
                    self.centerCardPool.isUserInteractionEnabled = true
                    self.centerCardPool.addGestureRecognizer(centerTap)
            })
        }) 
    }
    
    func centerCardAnimate() {
        UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction], animations: {
            self.centerCardPool.snp.updateConstraints { (make) in
                make.width.equalTo(self.screenWidth * 100/375*1.1)
                make.height.equalTo(self.screenHeight * 167/667*1.1)
            }
            self.view.layoutIfNeeded()
        }) { (true) in
            UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction], animations: { 
                self.centerCardPool.snp.updateConstraints { (make) in
                    make.width.equalTo(self.screenWidth * 100/375)
                    make.height.equalTo(self.screenHeight * 167/667)
                }
                self.view.layoutIfNeeded()
                }, completion: { (true) in
            })
        }
    }
    
    func leftCardAnimate() {
        UIView.animate(withDuration: 1, delay: 0, options: .allowUserInteraction, animations: {
            self.leftCardPool.snp.updateConstraints({ (make) in
                make.width.equalTo(self.screenWidth * 100/375*1.1)
                make.height.equalTo(self.screenHeight * 167/667*1.1)
            })
            self.view.layoutIfNeeded()
        }) { (true) in
            UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction], animations: {
                self.leftCardPool.snp.updateConstraints { (make) in
                    make.width.equalTo(self.screenWidth * 100/375)
                    make.height.equalTo(self.screenHeight * 167/667)
                }
                self.view.layoutIfNeeded()
            }, completion: { (true) in
            })
        }
    }
    
    func rightCardAnimate() {
        UIView.animate(withDuration: 1, delay: 0, options: .allowUserInteraction, animations: {
            self.rightCardPool.snp.updateConstraints({ (make) in
                make.width.equalTo(self.screenWidth * 100/375*1.1)
                make.height.equalTo(self.screenHeight * 167/667*1.1)
            })
            self.view.layoutIfNeeded()
        }) { (true) in
            UIView.animate(withDuration: 1, delay: 0, options: [.allowUserInteraction], animations: {
                self.rightCardPool.snp.updateConstraints { (make) in
                    make.width.equalTo(self.screenWidth * 100/375)
                    make.height.equalTo(self.screenHeight * 167/667)
                }
                self.view.layoutIfNeeded()
            }, completion: { (true) in
            })
        }
    }
    
    func createTimeLabel(_ title:String) -> UILabel {
        let lab = UILabel()
        lab.text = title
        lab.textColor = UIColor.init(red: 255/255, green: 169/255, blue: 225/255, alpha: 1)
        lab.backgroundColor = UIColor.clear
        lab.textAlignment = NSTextAlignment.center
        lab.font = HSFont.baseRegularFont(24)
        lab.alpha = 0
        return lab
    }
    
    //MARK: detailCardOnClick
    var leftHasDefine : Bool = false
    var rightHasDefine : Bool = false
    @objc func leftCardOnClick() {
        let rightTap = UITapGestureRecognizer()
        rightTap.addTarget(self, action: #selector(self.rightCardOnClick))
        self.rightCardPool.addGestureRecognizer(rightTap)
        self.rightCardPool.isUserInteractionEnabled = true
        self.leftCardPool.layer.removeAllAnimations()
        self.leftHasDefine = true
        self.leftCardPool.subviews.first?.removeFromSuperview()
        self.leftCardPool.image = UIImage(named: self.resultModelList[1].imageName)
        self.leftCardPool.pop_removeAllAnimations()
        self.leftCardPool.isUserInteractionEnabled = false
        let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            AnaliticsManager.sendEvent(AnaliticsManager.tarot_result_show, data: [self.resultModelList[1].name:"present"])
            let vc = TarotDefineController()
            vc.model = self.resultModelList[1]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    let tarotShareButton = UIButton()
    let tarotAgainButton = UIButton()
    @objc func selectAgain() {
        AnaliticsManager.sendEvent(AnaliticsManager.tarot_click, data: [self.resultModelList[0].name:"again"])
        self.goBackToFeature()
    }
    @objc func tarotShareAction() {
        AnaliticsManager.sendEvent(AnaliticsManager.tarot_click, data: [self.resultModelList[0].name:"share"])
        let tarotShareUrl = URL.init(string: "http://idailybread.com/horoshare/horoscopeshare.html")
        let activityViewController = UIActivityViewController(activityItems: [tarotShareUrl!],applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func rightCardOnClick() {
        self.rightCardPool.layer.removeAllAnimations()
        self.rightHasDefine = true
        self.rightCardPool.subviews.first?.removeFromSuperview()
        self.rightCardPool.image = UIImage(named: self.resultModelList[2].imageName)
        self.rightCardPool.pop_removeAllAnimations()
        self.rightCardPool.isUserInteractionEnabled = false
        let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            AnaliticsManager.sendEvent(AnaliticsManager.tarot_result_show, data: [self.resultModelList[2].name:"future"])
            let vc = TarotDefineController()
            vc.model = self.resultModelList[2]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func centerCardOnClick() {
        let leftTap = UITapGestureRecognizer()
        leftTap.addTarget(self, action: #selector(self.leftCardOnClick))
        self.leftCardPool.addGestureRecognizer(leftTap)
        self.leftCardPool.isUserInteractionEnabled = true
        self.centerCardPool.layer.removeAllAnimations()
        self.centerCardPool.subviews.first?.removeFromSuperview()
        self.centerCardPool.image = UIImage(named: self.resultModelList[0].imageName)
        self.centerCardPool.pop_removeAllAnimations()
        self.centerCardPool.isUserInteractionEnabled = false
        let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            AnaliticsManager.sendEvent(AnaliticsManager.tarot_result_show, data: [self.resultModelList[0].name:"past"])
            let vc = TarotDefineController()
            vc.model = self.resultModelList[0]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: Create UserInterface
    let selectedCardPool = UIImageView()
    let leftCardPool = UIImageView()
    let centerCardPool = UIImageView()
    let rightCardPool = UIImageView()
    
    func createCardPool() ->UIView{
        
        selectedCardPool.image = UIImage(named: "tarot_backLight_land_")
        selectedCardPool.backgroundColor = UIColor.clear
        selectedCardPool.alpha = 0
        
        self.view.addSubview(selectedCardPool)
        selectedCardPool.snp.makeConstraints({ (make) in
            make.top.equalTo(self.view.snp.top).offset(self.screenWidth == 320 ? (self.screenHeight * 0.5 - 15) : (self.screenHeight * 0.5))
            make.bottom.equalTo(selectedCardPool.snp.top).offset(self.screenHeight * 177/667)
            make.left.equalTo(self.view.snp.left).offset(5)
            make.right.equalTo(self.view.snp.right).offset(-5)
        })
        
        leftCardPool.tag = 0
        leftCardPool.image = UIImage(named: "tarot_main_backlight_")
        let leftCardBackImg = UIImageView(image: UIImage(named: "tarot_card_main"))
        leftCardPool.addSubview(leftCardBackImg)
        leftCardPool.alpha = 0
        self.view.addSubview(leftCardPool)
        
        leftCardPool.snp.makeConstraints { (make) in
            make.top.equalTo(selectedCardPool.snp.top).offset(15)
            make.bottom.equalTo(selectedCardPool.snp.bottom).offset(-15)
            make.left.equalTo(selectedCardPool.snp.left).offset(20)
            make.width.equalTo(self.screenWidth * 92 / 375)
        }
        leftCardBackImg.snp.makeConstraints { (make) in
            make.edges.equalTo(leftCardPool)
        }
        
        centerCardPool.tag = 0
        centerCardPool.image = UIImage(named: "tarot_main_backlight_")
        let centerCardBackImg = UIImageView(image: UIImage(named: "tarot_card_main"))
        centerCardPool.addSubview(centerCardBackImg)
        centerCardPool.alpha = 0
        self.view.addSubview(centerCardPool)
        centerCardPool.snp.makeConstraints { (make) in
            make.top.equalTo(leftCardPool.snp.top)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.equalTo(leftCardPool.snp.height)
            make.width.equalTo(leftCardPool.snp.width)
        }
        centerCardBackImg.snp.makeConstraints { (make) in
            make.edges.equalTo(centerCardPool)
        }
        
        rightCardPool.tag = 0
        rightCardPool.image = UIImage(named: "tarot_main_backlight_")
        let rightCardBackImg = UIImageView(image: UIImage(named: "tarot_card_main"))
        rightCardPool.addSubview(rightCardBackImg)
        rightCardPool.alpha = 0
        self.view.addSubview(rightCardPool)
        rightCardPool.snp.makeConstraints { (make) in
            make.top.equalTo(leftCardPool.snp.top)
            make.height.equalTo(leftCardPool.snp.height)
            make.right.equalTo(selectedCardPool.snp.right).offset(-20)
            make.width.equalTo(leftCardPool.snp.width)
        }
        rightCardBackImg.snp.makeConstraints { (make) in
            make.edges.equalTo(rightCardPool)
        }
        return selectedCardPool
    }
    let buttonTitleLabel = UILabel()
    func createPrepareSubviews() {
        describeLabel.numberOfLines = 0
        describeLabel.textColor = UIColor.hsTarotContentColor()
        describeLabel.textAlignment = .center
        if self.screenWidth == 320 {
            describeLabel.font = HSFont.baseRegularFont(16)
        }else{
            describeLabel.font = HSFont.baseRegularFont(20)
        }
        
        describeLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(NSLocalizedString("Now, you are going to shuffle the cards. In this process, please focus on your question and keep repeating it in your mind.", comment: ""), space:1.2)
        self.describeLabel.textAlignment = .center
        
        let buttonImg = UIImageView(image: UIImage(named: "tarot_button_"))
        buttonTitleLabel.text = NSLocalizedString("Shuffle Cards", comment: "")
        buttonTitleLabel.textColor = UIColor.white
        buttonTitleLabel.textAlignment = NSTextAlignment.center
        buttonTitleLabel.font = HSFont.charterBoldItalicFont(24)
        shuffleButton.addTarget(self, action: #selector(shuffleCard), for: .touchUpInside)
        
        shuffleButton.addSubview(buttonImg)
        shuffleButton.addSubview(buttonTitleLabel)
        self.view.addSubview(landscapeCardBack)
        self.view.addSubview(describeLabel)
        self.view.addSubview(shuffleButton)
        
        landscapeCardBack.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(self.screenHeight * 200/736)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.screenWidth * 130/414)
            make.height.equalTo(self.screenHeight * 80/736)
        }
        
        landscapeCardBack.cardBackImg.snp.makeConstraints { (make) in
            make.top.equalTo(landscapeCardBack.snp.top).offset(-5)
            make.left.equalTo(landscapeCardBack.snp.left).offset(-5)
            make.right.equalTo(landscapeCardBack.snp.right).offset(5)
            make.bottom.equalTo(landscapeCardBack.snp.bottom).offset(5)
        }
        
        describeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left).offset(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
            make.bottom.equalTo(shuffleButton.snp.top).offset(-(self.screenWidth * 40/414))
        }
        //add addContainer
        self.view.addSubview(self.adContainerView)
        self.adContainerView.layer.cornerRadius = 10
        self.adContainerView.layer.masksToBounds = true
        self.adContainerView.backgroundColor = UIColor.clear
        self.adContainerView.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left).offset(30)
            make.right.equalTo(self.view.snp.right).offset(-30)
            make.height.equalTo(self.screenWidth * 271/750)
            make.bottom.equalTo(describeLabel.snp.top).offset(-(self.screenWidth * 13/414))
        }
        self.adContainerView.isHidden = true
        shuffleButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom).offset(-36)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(describeLabel).offset(-20)
            make.height.equalTo(self.screenHeight * 65/736)
        }
        
        buttonImg.snp.makeConstraints { (make) in
            make.edges.equalTo(shuffleButton)
        }
        buttonTitleLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(shuffleButton)
        }
    }
    
    //MARK: Button Action
    @objc func shuffleCard() {
        if buttonTitleLabel.text == NSLocalizedString("Cut Cards", comment: "") {
            UIView.animate(withDuration: 0.6, animations: {
                self.describeLabel.alpha = 0
                self.shuffleButton.alpha = 0
            })
            self.shuffleButton.isUserInteractionEnabled = false
            UIView.animate(withDuration: 0.3, animations: {
                self.cardSetList.first?.center.y -= self.screenWidth * 100/414
                }, completion: { (true) in
                    UIView.animate(withDuration: 0.3, animations: {
                        self.cardSetList.last?.center.y += self.screenWidth * 100/414
                        }, completion: { (true) in
                            UIView.animate(withDuration: 0.3, animations: {
                                self.cardSetList.first?.center.y += self.screenWidth * 100/414
                                }, completion: { (true) in
                                    UIView.animate(withDuration: 1, animations: {
                                        self.buttonTitleLabel.text = NSLocalizedString("Select Cards", comment: "")
                                        self.describeLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(NSLocalizedString("Cards have been cut. Now it's time to select cards. Remember to put away all your extraneous thoughts, and communicate with Tarot's soul directly with your innermost thoughts.", comment: ""), space: 1.2)
                                        self.describeLabel.textAlignment = .center
                                        
                                    })
                                    UIView.animate(withDuration: 0.3, animations: {
                                        self.cardSetList.last?.center.y -= self.screenWidth * 100/414
                                        self.describeLabel.alpha = 1
                                        self.shuffleButton.alpha = 1
                                        }, completion: { (true) in
                                            let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                                            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                                                self.shuffleButton.isUserInteractionEnabled = true
                                            })
                                    })
                            })
                    })
            })
        }else if buttonTitleLabel.text == NSLocalizedString("Shuffle Cards", comment: ""){
            for _ in 0...49 {
                let cardSet = TarotMainCard.init(image: UIImage(named: "tarot_card_land_"))
                self.view.addSubview(cardSet)
                cardSet.frame = self.landscapeCardBack.frame
                cardSet.cardBackImg.frame = self.landscapeCardBack.cardBackImg.frame
                self.cardSetList.append(cardSet)
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.describeLabel.alpha = 0
                self.shuffleButton.alpha = 0
            })
            NotificationCenter.default.post(name: Notification.Name(rawValue: "setCreated"), object: nil)
        }else if buttonTitleLabel.text == NSLocalizedString("Select Cards", comment: ""){
            self.spreadCard()
        }else if buttonTitleLabel.text == NSLocalizedString("Open Cards", comment: ""){
            self.orderSelectedCard()
            self.adContainerView.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    lazy var cardList : [String] = {
        return ["queenofwands","kingofwands","death","knightofwands","pageofwands","tenofwands","nineofwands","eightofwands","sevenofwands","sixofwands","fiveofwands","fourofwands","threeofwands","twoofwands","aceofwands","kingofswords","queenofswords","knightofswords","pageofswords","tenofswords","nineofswords","eightofswords","sevenofswords","sixofswords","fiveofswords","fourofswords","threeofswords","twoofswords","aceofswords","kingofpentacles","queenofpentacles","knightofpentacles","pageofpentacles","tenofpentacles","nineofpentacles","eightofpentacles","sevenofpentacles","sixofpentacles","fiveofpentacles","fourofpentacles","threeofpentacles","twoofpentacles","aceofpentacles","kingofcups","queenofcups","knightofcups","pageofcups","thefool","judgment","themagician","thehighpriestess","theempress","theemperor","thehierophant","thelovers","thechariot","strength","thehermit","thewheeloffortune","justice","thehangedman","temperance","thedevil","thetower","thestar","themoon","thesun","theworld","aceofcups","threeofcups","fourofcups","fiveofcups","sixofcups","sevenofcups","eightofcups","nineofcups","tenofcups","twoofcups"]
    }()
    
    
    deinit {
        print("deinit")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
