//
//  ReadPostCommentCell.swift
//  Horoscope
//
//  Created by xiao  on 17/2/28.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ReadPostCommentCell:  UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor=UIColor.baseDetailBackgroundColor()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
