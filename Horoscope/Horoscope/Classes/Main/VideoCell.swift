//
//  VideoCell.swift
//  Horoscope
//
//  Created by Wang on 16/12/8.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class VideoCell: BaseCardCell {
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    static let rowHeight:CGFloat=126
    
    weak var rootVc:UIViewController?
    var videoCellModel : BreadModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        if videoCellModel != nil{
            titleLabel.text=videoCellModel?.title
            durationLabel.text = videoCellModel?.richMediaDuration
            let host = "YouTube"
            let viewsCountWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth(String(describing: videoCellModel?.viewCount) + " " + NSLocalizedString("views", comment: ""), font: 15, height: 17)
            self.viewsCountLab.snp.updateConstraints({ (make) in
                make.width.equalTo(viewsCountWidth)
            })
            fromOrg.text = host
            if videoCellModel?.viewCount == 1 || videoCellModel?.viewCount == 0{
                viewsCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((videoCellModel?.viewCount) ?? 0) + " " + NSLocalizedString("view", comment: "")
            }else{
                viewsCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((videoCellModel?.viewCount) ?? 0) + " " + NSLocalizedString("views", comment: "")
            }
            if let imgUrl = videoCellModel?.thumbnail{
                thumbNailView.sd_setImage(with: URL(string:imgUrl), placeholderImage: UIImage(named:"defaultImgIcon.jpg"), options: .continueInBackground) {
                    (image,error,type,url) in
                    if image != nil {
                        self.thumbNailView.image=image
                    }
                }
            }
            let width : CGFloat = HSHelpCenter.sharedInstance.textTool.calculateStrWidth(videoCellModel?.richMediaDuration, font: 12, height: 14)
            durationLabel.snp.updateConstraints({ (make) in
                make.width.equalTo(width+8)
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCellView()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellView()
//     let gesture = UITapGestureRecognizer(target: self, action: #selector(cellDidSelect))
//     self.addGestureRecognizer(gesture)
    }
    
    func cellDidSelect(){
     }
    
    let titleLabel = UILabel()
    let fromOrg = UILabel()
    let viewsCountLab = UILabel()
    let thumbNailView = UIImageView()
    let durationLabel = UILabel()
    let videobutton = UIImageView()
    let divideview = UIView()
    
    func setupCellView() {
        self.cornerBackView?.layer.cornerRadius = 0
        self.cornerBackView?.layer.masksToBounds = false
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.backgroundColor = UIColor.baseDetailBackgroundColor()
        
        durationLabel.textColor = UIColor.durationTimeColor()
        durationLabel.textAlignment = NSTextAlignment.center
        durationLabel.layer.cornerRadius = 6
        durationLabel.layer.masksToBounds = true
        durationLabel.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        thumbNailView.addSubview(durationLabel)
       
        self.cornerBackView?.addSubview(thumbNailView)
        titleLabel.textColor = UIColor.luckyPurpleTitleColor()
        titleLabel.font = HSFont.baseRegularFont(screenWidth == 320 ? 18 : 18)
        titleLabel.text = " "
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        titleLabel.numberOfLines = 0
        fromOrg.textColor = UIColor.luckyPurpleTitleColor()
        fromOrg.font = HSFont.baseLightFont(15)
        if UIScreen.main.bounds.size.width >= 375 {
            fromOrg.font = HSFont.baseLightFont(15)
            viewsCountLab.font =  HSFont.baseLightFont(15)
        } else{
            viewsCountLab.font =  HSFont.baseLightFont(13)
            fromOrg.font = HSFont.baseLightFont(13)
        }
        videobutton.image = UIImage(named:"ic_youtube_" )
        divideview.backgroundColor = UIColor.divideLineColor()
        
        viewsCountLab.textColor = UIColor.luckyPurpleTitleColor()
        viewsCountLab.font = HSFont.baseLightFont(15)
        viewsCountLab.textAlignment = NSTextAlignment.right
       
        self.cornerBackView!.addSubview(titleLabel)
        self.cornerBackView!.addSubview(fromOrg)
        self.cornerBackView!.addSubview(viewsCountLab)
        self.cornerBackView?.addSubview(videobutton)
        self.cornerBackView?.addSubview(divideview)
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo((self.cornerBackView?.snp.top)!).offset(screenWidth*0.048)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(12)
            make.right.equalTo(self.thumbNailView.snp.left).offset(-12)
        }
        
        videobutton.snp.makeConstraints { (make) in
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(screenWidth*0.04)
            make.bottom.equalTo(self.thumbNailView.snp.bottom).offset(2)
            make.width.equalTo(15)
            make.height.equalTo(12)
        }
        fromOrg.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.thumbNailView.snp.bottom).offset(4)
            make.left.equalTo(self.videobutton.snp.right).offset(screenWidth*0.01)
          //  make.right.equalTo(self.viewsCountLab.snp.left).offset(-8)
        }
        viewsCountLab.snp.makeConstraints { (make) in
            make.right.equalTo(self.thumbNailView.snp.left).offset(-12)
            make.centerY.equalTo(self.videobutton.snp.centerY)
        }
        
        let imgWidth = (VideoCell.rowHeight-screenWidth*0.096)*344/246
        thumbNailView.snp.makeConstraints { (make) in
            make.top.equalTo((self.cornerBackView?.snp.top)!).offset(screenWidth*0.048)
            make.bottom.equalTo((self.cornerBackView?.snp.bottom)!).offset(-screenWidth*0.048)
            make.right.equalTo((self.cornerBackView?.snp.right)!).offset(-12)
            make.width.equalTo(imgWidth)
        }
        
        durationLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.thumbNailView.snp.bottom).offset(-5)
            make.right.equalTo(self.thumbNailView.snp.right).offset(-6)
        }
        cornerBackView?.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        divideview.snp.makeConstraints { (make) in
            make.bottom.equalTo((self.cornerBackView?.snp.bottom)!)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(screenWidth*0.04)
            make.right.equalTo((self.cornerBackView?.snp.right)!).offset(-screenWidth*0.04)
            make.height.equalTo(1)
        }
    }
    
    class func dequeueReusableCell(_ tableView:UITableView,cellForRowAtIndexPath:IndexPath,model:BreadModel?)->VideoCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "videoListCell") as! VideoCell
        cell.videoCellModel = model
        return cell
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
