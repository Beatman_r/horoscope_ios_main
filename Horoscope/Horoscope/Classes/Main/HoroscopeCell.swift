//
//  HoroscopeCell.swift
//  Horoscope
//
//  Created by Beatman on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class HoroscopeCell: UICollectionViewCell {
    
    var imgView : UIImageView?
    var titleLabel:UILabel?
    var periodLabel : UILabel?
    var cellModel = ZodiacCellModel()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.width))
        imgView!.layer.borderWidth = 0.3;
        imgView!.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.addSubview(imgView!)
        titleLabel = UILabel(frame: CGRect(x: 0, y: CGRectGetMaxY(imgView!.bounds), width: self.bounds.width, height: self.bounds.height/8))
        titleLabel?.numberOfLines = 0
        titleLabel?.font = UIFont.boldSystemFontOfSize(18.0)
        titleLabel?.backgroundColor = UIColor.clearColor()
        self.addSubview(titleLabel!)
        periodLabel = UILabel(frame: CGRect(x: 0, y: CGRectGetMaxY(titleLabel!.bounds)+imgView!.bounds.height, width: self.bounds.width, height: self.bounds.height/8))
        periodLabel?.backgroundColor = UIColor.clearColor()
        self.addSubview(periodLabel!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func horoscopeCellModel()->[ZodiacCellModel]? {
        var modelArr : [ZodiacCellModel]?=[ZodiacCellModel]()
        for sub in 0...11 {
            let cellModel = ZodiacCellModel()
            cellModel.zodiacName = ZodiacModel.getZodiacName(sub)
            cellModel.imageName = ZodiacModel.getZodiaImage(sub)
            cellModel.periodString = ZodiacModel.getZodiacPeriod(sub)
            modelArr?.append(cellModel)
            
        }
        return modelArr
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
