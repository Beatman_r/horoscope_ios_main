//
//  AccountManager.swift
//  bibleverse
//
//  Created by Wang on 16/8/2.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FirebaseAuth

enum AuthType: Int {
    case null = 0
    case anonymous = 1
    case faceBookUser = 2
    case googleUser = 3
}

class AccountManager: NSObject, AWSBackend {
    
    static let sharedInstance = AccountManager()
    override init() {
        super.init()
        FIRAuth.auth()?.addStateDidChangeListener({ (auth:FIRAuth, user:FIRUser?) in
             
            if user==nil && self.getLogStatus() != .null {
                self.setLogStatus(.null)
                self.setUserInfo(nil)
            }
        })
    }
    
    fileprivate let statusFromInt: [Int: AuthType] = [0: .null,
                                                  1: .anonymous,
                                                  2: .faceBookUser,
                                                  3: .googleUser]
    let kLoginStatus = "LoginStatus"
    func getLogStatus() -> AuthType {
        let userdefault = UserDefaults.standard
        let loginStatus = userdefault.object(forKey: kLoginStatus)
        if  loginStatus == nil {
            userdefault.set(0, forKey: kLoginStatus)
            userdefault.synchronize()
        }
        let newStatus = userdefault.object(forKey: kLoginStatus) as! Int
        return self.statusFromInt[newStatus]!
    }
    
    func setLogStatus(_ status: AuthType) {
        let userdefault = UserDefaults.standard
        userdefault.set(status.rawValue, forKey: kLoginStatus)
        userdefault.synchronize()
    }
    
    func loginConfirm(_ token:String,birthday:String,horo:String,success:@escaping ()->(),failed:@escaping () -> ())  {
        let user = self.getUserInfo()
        self.POST(BaseUrl.userLogin, parameters: ["idToken":"\(user?.token ?? "")" as AnyObject,"birthdayMonth":"\(birthday)" as AnyObject,"horoscopeName":"\(horo)" as AnyObject], success: { (jsonData) in
            print("验证成功\(jsonData ?? "") 参数\(["idToken":"\(user?.token ?? "")","birthdayMonth":"\(birthday)","horoscopeName":"\(horo)"])")
            success()
            }) { (error) in
            print("验证失败\(error)  参数\(["idToken":"\(user?.token ?? "")","birthdayMonth":"\(birthday)","horoscopeName":"\(horo)"])")
            failed()
        }
    }
    
    func loadUserInfo(_ success:@escaping ()->(),failed:@escaping () -> ()) {
         self.GET(BaseUrl.userInfo, parameters: ["":"" as AnyObject], success: { (jsonData) in
            print("获取用户信息成功\(jsonData ?? "") 参数")
            success()
         }) { (error) in
            print("获取用户信息成功失败\(error)")
            failed()
        }
    }
    
    func loginWithType(_ type: AuthType, viewcontroller: UIViewController?,
                       data: AnyObject?,
                       logInSuccess: @escaping () -> Void,
                       failed: @escaping () -> Void) {
        print("#### login type: \(type)")
        let clearAndSetFromFunc = {
            if [AuthType.anonymous].contains(self.getLogStatus()) {
                self.setLogStatus(AuthType.null)
                self.setUserInfo(nil)
            }
        }
        var act: LoginProtocol!
        switch type {
        case .anonymous:
            clearAndSetFromFunc()
            act = AnonymousAuth()
            act.loginSuccess = { (userInfo: UserInfo) in
             self.setUserInfo(userInfo)
             self.setLogStatus(.anonymous)
             logInSuccess()
            }
            act.loginFailed = { (UserInfo: String) in
                failed()
            }
            
        case .faceBookUser:
            act = FIRFacebookAuth()
            act.loginSuccess = {(userInfo: UserInfo) in
               self.setUserInfo(userInfo)
               self.setLogStatus(.faceBookUser)
               logInSuccess()
            }
            act.loginFailed = { (UserInfo: String) in
                self.setLogStatus(.null)
                self.setUserInfo(nil)
                failed()
            }
            
        case .googleUser:
            clearAndSetFromFunc()
            let googleAuth = FIRGoogleAuth()
            if let googleUser = data as? GIDGoogleUser {
                googleAuth.user = googleUser
            }
            act = googleAuth
            act.loginSuccess = { (userInfo: UserInfo) in
                    self.setUserInfo(userInfo)
                    self.setLogStatus(.googleUser)
                    logInSuccess()
             }
            act.loginFailed = { (UserInfo: String) in
                self.setLogStatus(.null)
                self.setUserInfo(nil)
                failed()
            }
        default:
            break
        }
        act.login(viewcontroller)
    }
    
    func logout(_ success: () -> Void, failure: () -> Void) {
         if let auth = FIRAuth.auth() {
                do {
                    if self.getLogStatus() == AuthType.faceBookUser  {
                        let login = FBSDKLoginManager()
                        login.logOut()
                    }
                    else if self.getLogStatus() == AuthType.googleUser  {
                        GIDSignIn.sharedInstance().signOut()
                    }
    
                    try auth.signOut()
                    self.setLogStatus(.null)
                    self.setUserInfo(nil)
                    success()
//                    self.loginWithType(.Anonymous , viewcontroller: UIViewController(), data: nil, logInSuccess: {
//                        success()
//                        }, failed: {
//                            success()
//                    })
                }
                catch {
                    failure()
                }
            }
        }
    
        func LogOutWithType(_ type: AuthType, view: UIView, success: () -> Void) {
    
        }
    
        fileprivate func deleteUserInfoWhileLogout() {
            var userArr = self.getUserInfoArr()
            if userArr.count == 2 {
                userArr.remove(at: 1)
                let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/UserInfo"
                let success: Bool = NSKeyedArchiver.archiveRootObject(userArr, toFile: path)
                if success {
                    print("归档成功 ")
                }
            }
     }
   
    func setUserInfo(_ info: UserInfo?) {
        if info == nil {
            let userInfoArr: [UserInfo] = []
            let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/UserInfo"
            let success: Bool = NSKeyedArchiver.archiveRootObject(userInfoArr, toFile: path)
            if success {
                
            }
        }  else {
            var userInfoArr: [UserInfo] = []
            userInfoArr.append(info!)
            let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/UserInfo"
            let success: Bool = NSKeyedArchiver.archiveRootObject(userInfoArr, toFile: path)
            if success {
                print("归档成功")
            }
        }
    }
  
    fileprivate func getUserInfoArr() -> [UserInfo] {
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] + "/UserInfo"
        let archArr = (NSKeyedUnarchiver.unarchiveObject(withFile: path) as? [UserInfo]) ?? [UserInfo]()
        let info = archArr
        return info
    }
    
    func getUserInfo() -> UserInfo? {
        let archArr = self.getUserInfoArr()
        let info = archArr.last
        return info
    }
    
}
