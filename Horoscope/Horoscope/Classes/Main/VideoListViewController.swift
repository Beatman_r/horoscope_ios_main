//
//  VideoListViewController.swift
//  Horoscope
//
//  Created by Wang on 16/12/9.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import MJRefresh
import PKHUD

class videoListViewmodel:NSObject{
    
    init(type:BreadRequestWay) {
        self.requestWay=type
    }
    
    fileprivate var requestWay:BreadRequestWay?
    
    var list:[BreadModel]?=[]
    
    var count:Int{
        return self.list?.count ?? 0
    }
    
    func get(_ index:Int) -> BreadModel? {
        return self.list?[index]
    }
    
    fileprivate var sizeCount:Int = 10
    func loadData(_ success:@escaping ()->(),failure:@escaping (_ errors:NSError)->()) {
        DataManager.sharedInstance.breadInfoDao.getBreadDataWithType(1, size: sizeCount, requestType: self.requestWay ?? .top, success: { (dataList) in
            self.list = dataList
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func loadMoreData(_ success:@escaping (_ list:[BreadModel])->(),failure:@escaping (_ errors:NSError)->()) {
        self.sizeCount += 10
        DataManager.sharedInstance.breadInfoDao.getBreadDataWithType(1, size: sizeCount, requestType: self.requestWay ?? .top, success: { (dataList) in
            success(dataList)
        }) { (error) in
            failure(error)
        }
    }
}

class VideoListViewController: BaseTableViewController{
    var sourceArr:[String]?
    var titleArr:[String]?
    var viewModel:videoListViewmodel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        if self.type?.rawValue == "top" {
           self.navigationItem.title = "Top Video"
        }else{
           self.navigationItem.title = "New Video"
        }
        setupRefreshFooterView()
        viewModel=videoListViewmodel(type:self.type ?? .new)
        viewModel?.loadData({
            self.failedRetryView?.loadSuccess()
            self.tableView.mj_header?.endRefreshing()
            self.tableView.reloadData()
            }, failure: { (error) in
            self.failedRetryView?.loadFailed()
            self.tableView.mj_header?.endRefreshing()
        })
    }
    
    func setupRefreshFooterView() {
        let footer = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreList))
        footer?.isAutomaticallyRefresh=false
        footer?.setTitle("", for: .idle)
        footer?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        self.tableView.mj_footer=footer
    }
    
    @objc func loadMoreList(){
        viewModel?.loadMoreData({ (list) in
            if list.count == self.viewModel?.count{
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                self.perform(#selector(self.delayReload),
                    with: nil,
                    afterDelay: 0.5)
                self.viewModel?.list=list
                self.tableView.reloadData()
            }else{
                self.tableView.mj_footer.endRefreshing()
                self.viewModel?.list=list
                self.tableView.reloadData()
            }
            }, failure: { (errors) in
                
        })
    }
    
    @objc func delayReload()  {
         self.tableView.mj_footer.endRefreshing()
    }
    
    override func registerCell() {
        self.tableView.register(AllVideoCell.self, forCellReuseIdentifier: "AllVideoCell")
    }
    
    //RetryViewDelegate
    override func retryToLoadData() {
        viewModel?.loadData({
            self.failedRetryView?.loadSuccess()
            self.tableView.mj_header?.endRefreshing()
            self.tableView.reloadData()
            }, failure: { (error) in
                self.failedRetryView?.loadFailed()
                self.tableView.mj_header?.endRefreshing()
        })
    }
    
    //MARK:tableViewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel?.get(indexPath.row)
        let cell = AllVideoCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath,model:model!)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model=viewModel?.get(indexPath.row)
        return AllVideoCell.calculateHeight(model?.title ?? "")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK:Create
    var type:BreadRequestWay?
    class func create(_ type:BreadRequestWay) ->VideoListViewController {
        let vc = VideoListViewController()
        vc.type = type
        return vc
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
