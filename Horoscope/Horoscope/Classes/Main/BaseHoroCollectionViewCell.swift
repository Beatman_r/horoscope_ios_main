//
//  BaseHoroCollectionViewCell.swift
//  Horoscope
//
//  Created by Wang on 16/12/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class BaseHoroCollectionViewCell: UICollectionViewCell {
        var imgView : UIImageView?
        var titleLabel:UILabel?
        var periodLabel : UILabel?
        var colorView : UIView = UIView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.layer.cornerRadius = 5
            self.layer.masksToBounds = true
            self.backgroundColor = UIColor.white
            imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.width))
            self .addSubview(imgView!)
            
            self.colorView = UIView(frame: CGRect(x: 0, y: imgView!.bounds.maxY, width: self.bounds.width, height: self.bounds.height))
            self.colorView.backgroundColor = UIColor.cardBackColor()
            
            titleLabel = UILabel(frame: CGRect(x: 8, y: 4, width: self.bounds.width-16, height: self.bounds.height/8))
            titleLabel?.font = UIFont(name: HSHelpCenter.baseContentFontNameRegular, size: 18)
            titleLabel?.textColor = UIColor.hsTextColor(1)
            titleLabel?.textAlignment = NSTextAlignment.left
            self.colorView.addSubview(titleLabel!)
            
            periodLabel = UILabel(frame: CGRect(x: 8, y: titleLabel!.bounds.maxY-4, width: self.bounds.width-16, height: self.bounds.height/8))
            periodLabel?.textColor = UIColor.hsTextColor(1)
            periodLabel?.font = UIFont(name: HSHelpCenter.baseContentFontName, size: 16)
            periodLabel?.textAlignment = NSTextAlignment.left
            self.colorView.addSubview(periodLabel!)
            
            titleLabel?.text = ZodiacCellModel().zodiacName
            periodLabel?.text = ZodiacCellModel().periodString
            imgView?.image = UIImage(named: ZodiacCellModel().imageName)
            self.addSubview(self.colorView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    
        var model:ZodiacCellModel?{
            didSet{
                renderCell()
            }
        }
        
        func renderCell() {
            self.titleLabel?.text = model?.zodiacName
            self.periodLabel?.text = model?.periodString
            self.imgView?.image = UIImage(named: model?.imageName ?? "")
        }
        
        class func horoscopeCellModel() -> [ZodiacCellModel] {
            var cellModelArr = [ZodiacCellModel]()
            for sub in 0...11 {
                let cellModel = ZodiacCellModel()
                cellModel.zodiacName = ZodiacModel.getZodiacNameForDisplay(sub)
                cellModel.periodString = ZodiacModel.getZodiacPeriod(sub)
                cellModel.imageName = ZodiacModel.getZodiaImage(sub)
                cellModelArr.append(cellModel)
            }
            
            return cellModelArr
        }
}
