//
//  CardForSelect.swift
//  Horoscope
//
//  Created by Beatman on 16/12/23.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
protocol cardPaningDelegate : class{
    func testContain(_ img:CardForSelect,originCenter:CGPoint)
    func beginDragging(_ img:CardForSelect)
}
class CardForSelect: UIImageView,UIGestureRecognizerDelegate {
    
    let panGesture = UIPanGestureRecognizer()
    weak var delegate : cardPaningDelegate?
    var originCenter : CGPoint?
    let cardBackImg = UIImageView(image: UIImage(named: "tarot_card_main"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        cardBackImg.isUserInteractionEnabled = true
        panGesture.addTarget(self, action: #selector(handlePanGesture(_:)))
        self.image = UIImage(named: "tarot_main_backlight_")
        cardBackImg.addGestureRecognizer(panGesture)
        self.isUserInteractionEnabled = true
        panGesture.delegate = self
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true
        self.originCenter = self.center
        self.addSubview(cardBackImg)
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        self.delegate?.beginDragging(self)
        return true
    }
    
    @objc func handlePanGesture(_ sender: UIPanGestureRecognizer){
        let translation : CGPoint = sender.location(in: self.superview)
        self.center = translation
        self.delegate?.testContain(self,originCenter: self.originCenter!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
