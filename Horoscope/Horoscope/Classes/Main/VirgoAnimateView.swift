//
//  VirgoAnimateView.swift
//  Horoscope
//
//  Created by Beatman on 17/1/20.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class VirgoAnimateView: BaseZodiacAnimateView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createBasicUI()
        self.addToView()
        self.addViewConstraint()
    }
    
    override func startAnimation() {
        self.starTimer?.fireDate = Date.init(timeIntervalSinceNow: 0)
        self.shootingTimer?.fireDate = Date.init(timeIntervalSinceNow: 0)
    }

    override func stopAnimate() {
        self.starTimer?.invalidate()
        self.shootingTimer?.invalidate()
    }

    @objc func subFireShootingStar() {
        self.fireShootingStar(self.shootingStarView)
    }
    
    @objc func subLightUpStar() {
        self.lightUpStar(self.frontStarView, backView: self.backStarView)
    }
    
    func createBasicUI() {
        self.bgColorView.image = UIImage(named: "img_virgo")
        
        self.starTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(subLightUpStar), userInfo: nil, repeats: true)
        self.shootingTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(subFireShootingStar), userInfo: nil, repeats: true)
    }
    
    func addToView() {
        self.addSubview(self.bgColorView)
        self.addSubview(self.shootingStarView)
    }
    func addViewConstraint() {
        self.bgColorView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.snp.edges)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
