//
//  RoundMenuView.swift
//  Horoscope
//
//  Created by Beatman on 17/2/14.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class RoundMenuView: UIView {
    
    weak var rootVC : UIViewController?
    var horoName:String?
    
    let tipImg : UIImageView = {
        let tipImg = UIImageView()
        tipImg.contentMode = UIViewContentMode.scaleAspectFit
        tipImg.image = UIImage(named: "round_menu_bubbles_")
        return tipImg
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let userDefault = UserDefaults.standard
        let shouldShow = userDefault.bool(forKey: "shouldShowTipImg")
        if shouldShow == false {
            self.tipImg.isHidden = true
        }
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = false
        let addButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        addButton.setImage(UIImage(named: "button_add_"), for: UIControlState())
        addButton.addTarget(self, action: #selector(goPost), for: .touchUpInside)
        self.addSubview(addButton)
        self.addSubview(tipImg)
        self.tipImg.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.snp.top)
            make.right.equalTo(self.snp.right)
            make.width.equalTo(SCREEN_WIDTH * 150/375)
            make.height.equalTo(SCREEN_HEIGHT * 60/667)
        }
    }
    
    @objc func goPost() {
        AnaliticsManager.sendEvent(AnaliticsManager.main_post_click, data: ["signs":"\(self.horoName ?? "")"])
        let notiName = "didClickPost"
        let notiCenter = NotificationCenter.default
        notiCenter.post(name: Notification.Name(rawValue: notiName), object: nil)
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        let userDefault = UserDefaults.standard
        userDefault.set(false, forKey: "shouldShowTipImg")
        userDefault.synchronize()
        if userStatus == .null || userStatus == .anonymous{
            UIView.animate(withDuration: 0.2, animations: {
                self.tipImg.snp.remakeConstraints({ (make) in
                    make.top.equalTo(self.snp.top)
                    make.left.equalTo(self.snp.right).offset(-20)
                    make.height.equalTo(0)
                    make.width.equalTo(0)
                })
                self.layoutIfNeeded()
            }, completion: { (true) in
                let vc = HsLoginViewController()
                self.rootVC?.navigationController?.pushViewController(vc, animated: true)
             }) 
          }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.tipImg.snp.remakeConstraints({ (make) in
                    make.top.equalTo(self.snp.top)
                    make.left.equalTo(self.snp.right).offset(-20)
                    make.height.equalTo(0)
                    make.width.equalTo(0)
                })
                self.layoutIfNeeded()
            }, completion: { (true) in
                let vc = PostViewController()
                vc.isReply = false
                self.rootVC?.navigationController?.pushViewController(vc, animated: true)
            }) 
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
