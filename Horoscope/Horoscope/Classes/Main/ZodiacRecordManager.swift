//
//  ZodiacRecordManager.swift
//  Horoscope
//
//  Created by Beatman on 17/1/6.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ZodiacRecordManager: NSObject {
    static let zodiacViewCount = "zodiacViewCount"
    class func recordFirstSelectedZodiac(_ zodiacNameList:[String]) {
        var sub = 0
        for zodiacName in zodiacNameList {
            UserDefaults.standard.setValue(zodiacName, forKey: "firstSelectedZodiacName\(sub)")
            UserDefaults.standard.synchronize()
            sub += 1
        }
        UserDefaults.standard.set(zodiacNameList.count, forKey: zodiacViewCount)
        UserDefaults.standard.synchronize()
    }
    
    class func getSelectedZodiacNameList() -> [String] {
        var zodiacNameList : [String] = []
        let count = UserDefaults.standard.integer(forKey: zodiacViewCount)
        for sub in 0...(count - 1) {
            let zodiacName = UserDefaults.standard.value(forKey: "firstSelectedZodiacName\(sub)")
            zodiacNameList.append((zodiacName ?? "") as! String)
        }
        return zodiacNameList
    }
    
    class  func getSelectedZodiac() -> [String]{
        var selectArray:[IndexPath] = [IndexPath]()
        let chooseArrayData = UserDefaults.standard.value(forKey: ChooseZodiacArray) as? Data
        if chooseArrayData != nil {
            let chooseArray = NSKeyedUnarchiver.unarchiveObject(with: chooseArrayData!) as! [IndexPath]
            selectArray = chooseArray
        }else{
            return []
        }
        let zodiacArray = BaseHoroCollectionViewCell.horoscopeCellModel()
        var selectedZodiacArray = [String]()
        for indexPath in selectArray {
            let selectedZodiacName = zodiacArray[indexPath.row].zodiacName
            selectedZodiacArray.append(selectedZodiacName)
        }
        return selectedZodiacArray
    }
    
    class  func getSelectedZodiacFromselectArray(_ selectArray:[IndexPath]) -> [String]{
        let zodiacArray = BaseHoroCollectionViewCell.horoscopeCellModel()
        var selectedZodiacArray = [String]()
        for indexPath in selectArray {
            let selectedZodiacName = zodiacArray[indexPath.row].zodiacName
            selectedZodiacArray.append(selectedZodiacName)
        }
        return selectedZodiacArray
    }
    
    class  func getSelectedZodiacItems() -> [ZodiacCellModel]{
        var selectArray:[IndexPath] = [IndexPath]()
        let chooseArrayData = UserDefaults.standard.value(forKey: ChooseZodiacArray) as? Data
        if chooseArrayData != nil {
            let chooseArray = NSKeyedUnarchiver.unarchiveObject(with: chooseArrayData!) as! [IndexPath]
            selectArray = chooseArray
        }else{
            return []
        }
        let zodiacArray = BaseHoroCollectionViewCell.horoscopeCellModel()
        var selectedZodiacArray = [ZodiacCellModel]()
        for indexPath in selectArray {
            selectedZodiacArray.append(zodiacArray[indexPath.row])
        }
        return selectedZodiacArray
    }
}
