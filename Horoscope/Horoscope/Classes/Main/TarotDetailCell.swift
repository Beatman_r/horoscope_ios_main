//
//  TarotDetailCell.swift
//  Horoscope
//
//  Created by Beatman on 16/12/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class TarotDetailCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createDetailUI()
    }
    
    let titleLabel = UILabel()
    let detailLabel = UILabel()
    let bgImg = UIImageView()
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    func createDetailUI() {
        self.backgroundColor = UIColor.clear
        
        self.bgImg.image = UIImage(named: "explanationCard")
        
        self.titleLabel.textColor = UIColor.init(red: 255/255, green: 104/255, blue: 202/255, alpha: 1)
        self.titleLabel.font = HSFont.baseMediumFont(self.screenHeight == 480 ? 16 : 18)
        
        self.detailLabel.textColor = UIColor.white
        self.detailLabel.font = HSFont.baseRegularFont(self.screenHeight == 480 ? 13 : 15)
        self.detailLabel.numberOfLines = 0
        
        self.contentView.addSubview(bgImg)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(detailLabel)
        
        self.bgImg.snp.makeConstraints { (make) in
            make.edges.equalTo(self.contentView.snp.edges)
        }
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView.snp.top).offset(15)
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
        }
        
        self.detailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            make.left.equalTo(self.contentView.snp.left).offset(15)
            make.right.equalTo(self.contentView.snp.right).offset(-15)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
