//
//  MorningNotification.swift
//  Horoscope
//
//  Created by Wang on 16/12/1.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class MorningNotification: BaseNotification,NotificationProtocol {

    var datefatter:DateFormatter?
    static let morning = "morning"
    func createNotification(_ duration:Int) {
        let notiDefault = UserDefaults.standard
        let ifMorningHasNoti = notiDefault.bool(forKey: "\(NotificationType.morning)")
        if  ifMorningHasNoti == true{
            
        }else if ifMorningHasNoti == false{
                return
        }
     
        for index in 0...duration {
                let notification=UILocalNotification()
                let datefatter=DateFormatter()
                let randomDura = self.random20min()
                let fireDate = calcFireTime(index, pushTime: 8*60*60+randomDura)
            print("早上通知\(fireDate)")
                datefatter.dateFormat="yyyyMMdd"
            let dateParamStr = datefatter.string(from: fireDate)
            if #available(iOS 8.2, *) {
                notification.alertTitle=NSLocalizedString("Horoscope", comment: "") + " " + HSHelpCenter.sharedInstance.dateTool.getFormatedDateString(dateParamStr)
            } else {
            }
            notification.fireDate = fireDate
            notification.timeZone = TimeZone.current
            notification.applicationIconBadgeNumber = 1
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.alertBody = NSLocalizedString("Good Morning! What should you focus on today? Check out Today's Horoscope Forecast!", comment: "")
            notification.repeatInterval = NSCalendar.Unit.year
            var userInfo:[AnyHashable: Any] = [AnyHashable: Any]()
            userInfo["key"] = MorningNotification.morning
            notification.userInfo = userInfo
            UIApplication.shared.scheduleLocalNotification(notification)
         }
    }
    
    fileprivate func random20min()->Double {
        var rand_int:Int = arc4random().hashValue
        rand_int=rand_int%120
        rand_int=rand_int-60
        return Double(rand_int)
    }
    
    func  addNotification(_ durationTime:Int) {
        createNotification(durationTime)
    }
    
    init(dateformatter:DateFormatter) {
        self.datefatter=dateformatter
    }
   
}
