//
//  TarotMainCard.swift
//  Horoscope
//
//  Created by Beatman on 16/12/28.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class TarotMainCard: UIImageView {

    let cardBackImg = UIImageView(image: UIImage(named: "tarot_backLight_land_"))
    override init(image: UIImage?) {
        super.init(image: image)
        self.addSubview(cardBackImg)
        self.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
