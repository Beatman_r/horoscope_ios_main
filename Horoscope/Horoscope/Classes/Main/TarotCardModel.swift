//
//  TarotCardModel.swift
//  Horoscope
//
//  Created by Beatman on 16/12/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class TarotCardModel: NSObject {
    
    var cardName: String!
    var keyWord : String!
    var astrological : String!
    var imageName : String!
    var detail : String!
    var title : String!
    var name : String!
    var content : String!
    
    
    init(jsonData : JSON,topic : Int) {
        super.init()
        self.cardName = jsonData["cardName"].stringValue
        self.keyWord = jsonData["keyWord"].stringValue
        self.astrological = jsonData["astrological"].stringValue
        self.imageName = jsonData["imageName"].stringValue 
        self.detail = jsonData["detail"].stringValue
        self.title = jsonData["topic"][topic]["title"].stringValue
        self.name = jsonData["topic"][topic]["name"].stringValue
        self.content = jsonData["topic"][topic]["content"].stringValue 
    }
    
    
}
