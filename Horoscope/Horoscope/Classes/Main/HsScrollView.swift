//
//  HsScrollView.swift
//  Horoscope
//
//  Created by Wang on 16/12/7.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class HsScrollView: UICollectionReusableView, UIScrollViewDelegate {
    
    static let height = (UIScreen.main.bounds.size.width)/16 * 9
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createVornerBackView()
        self.createScrollView()
    }
    
    class func create() ->HsScrollView{
        return HsScrollView()
    }
    
    func refreshView(_ arr:[UIView]) {
        self.scrollView!.contentSize = CGSize(width: 0, height: CGFloat(self.bounds.height-10))
        self.viewArr?.removeAll()
        self.viewArr?=arr
        self.scrollView!.contentSize = CGSize(width: CGFloat(screenWidth)*CGFloat(arr.count), height: CGFloat(self.bounds.height-36))
        for item in self.scrollView!.subviews {
            item.removeFromSuperview()
        }
        self.scrollAddViews()
        self.createPagecontrol()
        self.removeTimer()
        if arr.count>0{
            if arr.count == 1{
                pageCtrl.isHidden=true
            }else{
                pageCtrl.isHidden=false
            }
            self.addTimer()
        }
    }
    
    fileprivate var timer:Timer?
    func addTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(self.changeView), userInfo: nil, repeats: true)
        let mainLoop = RunLoop.main
        mainLoop.add(self.timer!, forMode: RunLoopMode.commonModes)
    }
    
    func removeTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @objc func changeView() {
        var currentPage=self.pageCtrl.currentPage
        var offset=(self.scrollView?.contentOffset) ?? CGPoint(x: 0, y: 0)
        if (currentPage >= (viewArr?.count)! - 1) {
            currentPage=0;
            offset.x = 0;
        }else{
            currentPage += 1
            offset.x += screenWidth
        }
        self.pageCtrl.currentPage=currentPage
        self.scrollView?.setContentOffset(offset, animated: true)
    }
    
    var viewArr:[UIView]?=[]
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var backView:UIView?
    func createVornerBackView() {
        self.backgroundColor = UIColor.clear
        backView = UIView()
        self.addSubview(backView!)
        backView?.backgroundColor=UIColor.clear
        backView?.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom)
        }
    }
    
    var titleNameArr:[String]? = ["",""]
    fileprivate var totalScrollWidth:CGFloat=0
    fileprivate var eachBtnWidthArr:[CGFloat]=[]
    
    func calculateScrollViewWidth(){
        var totalWidth:CGFloat = 0
        for eachString in self.titleNameArr ?? [""] {
            let eachWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth(eachString, font: 18, height: 44)+5
            self.eachBtnWidthArr.append(eachWidth)
            totalWidth += eachWidth
        }
        totalScrollWidth=totalWidth
    }
    
    func scrollAddViews() {
        for index in 0..<(self.viewArr)!.count {
            let eachView = self.viewArr![index]
            eachView.frame=CGRect(x: CGFloat(index)*screenWidth, y: 0, width: screenWidth, height: self.bounds.height)
            scrollView?.addSubview(eachView)
        }
    }
    
    let screenWidth  = UIScreen.main.bounds.size.width
    var scrollView:UIScrollView?
    var pageCtrl:SMPageControl!
    func createScrollView() {
        scrollView=UIScrollView()
        scrollView?.delegate=self
        scrollView?.showsVerticalScrollIndicator=false
        scrollView?.showsHorizontalScrollIndicator=false
        scrollView?.isPagingEnabled=true
        scrollView?.bounces=false
        scrollView?.bouncesZoom=false
        self.backView!.addSubview(scrollView!)
        scrollView?.snp.makeConstraints { (make) in
            make.left.equalTo(self.backView!.snp.left)
            make.right.equalTo(self.backView!.snp.right)
            make.top.equalTo(self.backView!.snp.top)
            make.bottom.equalTo(self.backView!.snp.bottom)
        }
    }
    
    func createPagecontrol(){
        pageCtrl?.removeFromSuperview()
        pageCtrl = SMPageControl()
        pageCtrl.numberOfPages = (self.viewArr!.count)
        pageCtrl.currentPage = 0
        pageCtrl.isUserInteractionEnabled=false
        pageCtrl.pageIndicatorImage=UIImage(named: "hs_uncurrent")
        pageCtrl.currentPageIndicatorImage=UIImage(named: "hs_current")
        pageCtrl.indicatorMargin = 20
        self.backView?.addSubview(pageCtrl)
        self.pageCtrl?.snp.makeConstraints { (make) in
            make.left.equalTo(self.backView!.snp.left)
            make.right.equalTo(self.backView!.snp.right)
            make.height.equalTo(20)
            make.bottom.equalTo(self.backView!.snp.bottom).offset(-2)
        }
    }
    
    //MARK:ScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.addTimer()
        let offSet = scrollView.contentOffset
        let bounds = scrollView.frame
        pageCtrl.currentPage = Int(offSet.x / bounds.size.width)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.removeTimer()
    }
    
    
}
