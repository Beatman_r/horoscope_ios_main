//
//  TarotBaseViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class TarotBaseViewController: UIViewController {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createUI()
       // self.navigationController?.navigationBar.hidden=true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.barTintColor=UIColor.clearColor()
//        self.navigationController?.navigationBar.tintColor=UIColor.clearColor()
//        self.navigationController?.navigationBar.backgroundColor=UIColor.clearColor()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: createLeftItemButton())
        self.view.layer.masksToBounds = true
    }
    override func viewDidAppear(_ animated: Bool) {
        if self.isKind(of: TarotPrepareController.self) || self.isKind(of: TarotIntroController.self) || self.isKind(of: TarotDefineController.self){
            
            self.navigationController!.interactivePopGestureRecognizer!.isEnabled = false
        }else{
            self.navigationController!.interactivePopGestureRecognizer!.isEnabled = true
        }
    }
    
    func createLeftItemButton() ->UIButton{
        let button = UIButton()
        button.setImage(UIImage(named: "backicon"), for: UIControlState())
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(goBackToFeature), for: .touchUpInside)
        return button
    }
    
    @objc func goBackToFeature() {
        if self.isKind(of: TarotEnterController.self){
            if (HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController) != nil {
                self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = nil
            }
            _ = self.navigationController?.popViewController(animated: true)
        }else if self.isKind(of: TarotDefineController.self){
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            if let vc = self.navigationController?.viewControllers[1]{
                _ = self.navigationController?.popToViewController(vc, animated: true)
            }
        }
     
    }
    
    func createUI() {
        let bgImageView = UIImageView(frame: UIScreen.main.bounds)
        bgImageView.image = UIImage(named: "tarotBackgroundImage.jpg")
        self.view.addSubview(bgImageView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
