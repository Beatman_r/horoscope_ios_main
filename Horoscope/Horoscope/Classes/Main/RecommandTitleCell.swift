//
//  RecommandTitleCell.swift
//  Horoscope
//
//  Created by Wang on 16/12/27.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class RecommandTitleCell: UITableViewCell {
    static let height:CGFloat = 44 + 1 + 10
    let screenWidth = UIScreen.main.bounds.width
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addUI()
        self.addConstrains()
    }
    
    func addUI() {
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.addSubview(img)
        self.addSubview(blankview)
        self.addSubview(titleLab)
        self.addSubview(divideview)
        
    }
    
    let blankview: UIView = {
        let blank = UIView()
        blank.backgroundColor = UIColor.clear
        return blank
    }()
    
    let divideview: UIView = {
        let divide = UIView()
        divide.backgroundColor = UIColor.divideLineColor()
        return divide
    }()
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.textAlignment = .left
        lab.text = NSLocalizedString("You May Also Like", comment: "")
        lab.font=HSFont.baseRegularFont(20)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    let img:UIImageView = {
        let view = UIImageView()
        view.backgroundColor=UIColor.baseCardBackgroundColor()
        return view
    }()
    
    func addConstrains(){
        blankview.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.height.equalTo(10)
            make.top.equalTo(self.snp.top)
        }
        
        titleLab.snp.makeConstraints { (make) in
 
            make.left.equalTo(self.snp.left).offset(12)
            make.right.equalTo(self.snp.right).offset(-12)
            make.centerY.equalTo(self.snp.centerY).offset(7)
        }
        divideview.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.contentView.snp.bottom)
            make.left.equalTo(self.contentView.snp.left).offset(screenWidth*0.04)
            make.right.equalTo(self.contentView.snp.right).offset(-screenWidth*0.04)
            make.height.equalTo(1)
        }
 
        img.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.blankview.snp.bottom)
            make.bottom.equalTo(self.snp.bottom)
        }
 
    }
    
    func renderCell() {
    
    }
    
  
    class func calculateHeight(_ title:String)->CGFloat{
      return 45
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let cellId="RecommandTitleCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> RecommandTitleCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! RecommandTitleCell
        return cell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
