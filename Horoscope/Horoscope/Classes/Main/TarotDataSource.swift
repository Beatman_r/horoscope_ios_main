//
//  TarotDataSource.swift
//  Horoscope
//
//  Created by Beatman on 16/12/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class TarotDataSource: NSObject {
    
    class func getResult(_ cardName : String,topic : Int) -> TarotCardModel{
        let path = Bundle.main.path(forResource: cardName, ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""))
        let json = JSON(data: jsonData!)
        let resultModel = TarotCardModel(jsonData: json, topic: topic)
        return resultModel
    }

   
}
