//
//  VideoToolCell.swift
//  Horoscope
//
//  Created by xiao  on 17/2/22.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

class VideoToolCell: BaseCardCell {

    let screenWidth = UIScreen.main.bounds.width
    let viewCountLabel = UILabel()
    let likeCountLabel = UILabel()
    let dislikeCountLabel = UILabel()
    let titleLab:UILabel = UILabel()
    let authorNameLab:UILabel = UILabel()
    let timeLab:UILabel = UILabel()
    let viewCountLab:UILabel = UILabel()
    let screenW = AdaptiveUtils.screenWidth
    let screenH = AdaptiveUtils.screenHeight
    let authorImg:UIImageView = UIImageView()

    weak var rootVc:UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addUI()
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK:Btn click
    
    //MARK:BtnClick
    @objc func goFBShare() {
        let content = FBSDKShareLinkContent.init()
        content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_FORECAST + (self.detailModel?.postId ?? ""))
        FBSDKShareDialog.show(from: self.rootVc, with: content, delegate: nil)
    }
    
    @objc func commentBtnClick()  {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
        let vc = PostViewController()
        vc.isFirstClassReply = true
        // vc.postModel = MyPostModel.ini
        vc.replyModel = MyPostModel.init(model: detailModel)
        self.rootVc?.navigationController?.pushViewController(vc, animated: true)
    }
    }
    
    @objc func praiseBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.detailModel?.iliked == false{
                let newCount = (detailModel?.likeCount ?? 0) + 1
                self.detailModel?.likeCount = newCount
                self.detailModel?.iliked = true
                self.renderCell()
               // HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: true, id: detailModel?.postId ?? "")
            }
            else  if self.detailModel?.iliked == true {
                var newCount = (detailModel?.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.detailModel?.likeCount = newCount
                self.detailModel?.iliked = false
                self.renderCell()
             //   HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: false, id: detailModel?.postId ?? "")
            }
        }
    }
    
    @objc func treadBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if  self.detailModel?.iDisliked == false{
                self.detailModel?.iDisliked = true
                let newCount = (detailModel?.dislikeCount ?? 0) + 1
                self.detailModel?.dislikeCount = newCount
                self.renderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: true, id: detailModel?.postId ?? "")
            }
            else if  self.detailModel?.iDisliked == true{
                self.detailModel?.iDisliked = false
                var newCount = (detailModel?.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.detailModel?.dislikeCount = newCount
                self.renderCell()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: false, id: detailModel?.postId ?? "")
            }
        }
    }

    
    var detailModel:ReadPostModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        if detailModel != nil{
           if detailModel?.iliked == true{
                likeButton.setImage(UIImage(named:"praise2_"), for: UIControlState())
            }else{
                likeButton.setImage(UIImage(named:"praise_"), for: UIControlState())
            }
            
            if detailModel?.iDisliked == true{
                dislikeButton.setImage(UIImage(named:"Step-on2_"), for: UIControlState())
            }else{
                dislikeButton.setImage(UIImage(named:"Step-on_"), for: UIControlState())
            }
            
            if detailModel?.likeCount == 0 {
                likeCountLabel.isHidden = true
            }else{
                likeCountLabel.isHidden = false
                likeCountLabel.text = String(detailModel?.likeCount ?? 0)
            }
            
            if detailModel?.dislikeCount == 0 {
                dislikeCountLabel.isHidden = true
            }else{
                dislikeCountLabel.isHidden = false
                dislikeCountLabel.text = String(detailModel?.dislikeCount ?? 0)
            }

            if let authorImaStr = detailModel?.user?.avatar {
            self.authorImg.sd_setImage(with: URL(string: authorImaStr), placeholderImage: UIImage(named:"icon_author"))
            }
            titleLab.text=detailModel?.title
            timeLab.text=detailModel?.createTimeHuman
            authorNameLab.text=detailModel?.user?.name
            if detailModel?.viewCount <= 1{
            viewCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((detailModel?.viewCount) ?? 0) + " " + NSLocalizedString("view", comment: "")
            }else{
            viewCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((detailModel?.viewCount) ?? 0)+" "+NSLocalizedString("views", comment: "")
            }
            likeCountLabel.text = String(self.detailModel?.likeCount ?? 0)

            dislikeCountLabel.text = String(self.detailModel?.dislikeCount ?? 0)

        }
    }
    
    let dislikeButton:UIButton = {
        let likeButton = UIButton(type: .custom)
        return likeButton
    }()
    
    let likeButton:UIButton = {
        let likeButton = UIButton(type: .custom)
        return likeButton
    }()
    
    func addUI() {
        
        titleLab.numberOfLines = 0
        titleLab.textAlignment = .left
        titleLab.font=HSFont.baseRegularFont(18)
        titleLab.textColor = UIColor.luckyPurpleTitleColor()
        titleLab.backgroundColor=UIColor.clear
        self.cornerBackView!.addSubview(titleLab)
        
       
        likeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        likeButton.addTarget(self, action: #selector(praiseBtnClick), for: .touchUpInside)
        self.cornerBackView!.addSubview(likeButton)

        
        likeCountLabel.font = UIFont.systemFont(ofSize: 15)
        likeCountLabel.textColor = UIColor.purpleContentColor()
        self.cornerBackView!.addSubview(likeCountLabel)

    
        dislikeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        dislikeButton.addTarget(self, action: #selector(treadBtnClick), for: .touchUpInside)
        self.cornerBackView!.addSubview(dislikeButton)

        dislikeCountLabel.font = UIFont.systemFont(ofSize: 15)
        dislikeCountLabel.textColor = UIColor.purpleContentColor()
        self.cornerBackView!.addSubview(dislikeCountLabel)

        let commentButton = UIButton(type: .custom)
        commentButton.setImage(UIImage(named:"comments_" ), for: UIControlState())
        
         commentButton.addTarget(self, action: #selector(commentBtnClick), for: .touchUpInside)
        self.cornerBackView!.addSubview(commentButton)

        let shareButton = UIButton(type: .custom)
        shareButton.setImage(UIImage(named:"CB_share_" ), for: UIControlState())
        shareButton.addTarget(self, action: #selector(goFBShare), for: .touchUpInside)
        self.cornerBackView!.addSubview(shareButton)
        
        authorImg.layer.masksToBounds=true
        authorImg.layer.cornerRadius=25
        self.cornerBackView!.addSubview(authorImg)
        
        authorNameLab.numberOfLines = 1
        authorNameLab.textAlignment = .left
        authorNameLab.font=HSFont.baseMediumFont(18)
        authorNameLab.textColor = UIColor.purpleContentColor()
        authorNameLab.backgroundColor=UIColor.clear
        self.cornerBackView!.addSubview(authorNameLab)
        
        timeLab.numberOfLines = 1
        timeLab.textAlignment = .left
        timeLab.font=HSFont.baseMediumFont(15)
        timeLab.textColor = UIColor.purpleContentColor()
        timeLab.backgroundColor=UIColor.clear
        self.cornerBackView!.addSubview(timeLab)
        
        viewCountLab.numberOfLines = 1
        viewCountLab.textAlignment = .right
        viewCountLab.font=HSFont.baseMediumFont(15)
        viewCountLab.textColor = UIColor.purpleContentColor()
        viewCountLab.backgroundColor=UIColor.clear
        self.cornerBackView!.addSubview(viewCountLab)
        
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
         }
        
        titleLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!).offset(20)
            make.top.equalTo(self.cornerBackView!.snp.top).offset(15)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-20)
        }
        
        likeButton.snp.makeConstraints { (make) in
            make.top.equalTo(titleLab.snp.bottom).offset(10/667*screenH)
            make.left.equalTo(titleLab.snp.left)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }

        likeCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(likeButton.snp.centerY)
            make.left.equalTo(likeButton.snp.right).offset(10/375*screenW)
            make.width.equalTo(50/375*screenW)
            make.height.equalTo(24)
        }

        dislikeButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(likeButton.snp.centerY)
            make.left.equalTo(likeCountLabel.snp.right).offset(10/375*screenW)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }

        dislikeCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(likeButton.snp.centerY)
            make.left.equalTo(dislikeButton.snp.right).offset(10/375*screenW)
            make.width.equalTo(50/375*screenW)
            make.height.equalTo(24)
        }

        shareButton.snp.makeConstraints { (make) in
            make.right.equalTo(titleLab.snp.right)
            make.centerY.equalTo(likeButton.snp.centerY)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }

        commentButton.snp.makeConstraints { (make) in
            make.right.equalTo(shareButton.snp.right).offset(-80/375*screenW)
            make.centerY.equalTo(likeButton.snp.centerY)
            make.width.equalTo(24)
            make.height.equalTo(24)
        }
        
        self.authorImg.snp.makeConstraints({ (make) in
            make.left.equalTo(titleLab)
            make.top.equalTo(likeButton.snp.bottom).offset(10/667*screenH)
            make.width.equalTo(50)
            make.height.equalTo(50)
        })

        self.authorNameLab.snp.makeConstraints({ (make) in
            make.left.equalTo(self.authorImg.snp.right).offset(8/375*screenW)
            make.top.equalTo(authorImg.snp.top).offset(4/667*screenH)
            make.width.equalTo(150/375*screenW)
            make.height.equalTo(18)
        })
        self.timeLab.snp.makeConstraints({ (make) in
            make.left.equalTo(authorNameLab)
            make.top.equalTo(authorNameLab.snp.bottom).offset(4/667*screenH)
            make.width.equalTo(150/375*screenW)
            make.height.equalTo(18)
        })
        
        viewCountLab.snp.makeConstraints { (make) in
            make.centerY.equalTo(authorImg.snp.centerY)
            make.right.equalTo(shareButton.snp.right)
            make.width.equalTo(120/375*screenW)
            make.height.equalTo(24)
        }
    }
    
    class func calculateHeight(_ title:String)->CGFloat{
        var titleHeight=HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: title, fontSize: 18, font: HSFont.baseMediumFont(18), width: UIScreen.main.bounds.size.width-40, multyplyLineSpace: 1)
        titleHeight += 125/667*(AdaptiveUtils.screenHeight)
        return titleHeight
    }
    
    static let cellId="VideoToolCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath,model:ReadPostModel?) -> VideoToolCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! VideoToolCell
        cell.detailModel=model
        return cell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
