//
//  ReadOtherUserPostViewController.swift
//  Horoscope
//
//  Created by Beatman on 17/2/23.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD

class ReadOtherUserPostViewController: BaseViewController,OtherUserPostListViewModelDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        self.title=NSLocalizedString("\(self.userName)", comment: "")
        
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        initViewModel()
        self.setupRefreshHeaderView()
        self.setupRefreshFooterView()
        // Do any additional setup after loading the view.
    }
    
    var userId : String = ""
    var userName:String = ""
    
    var viewModel:OtherUserPostListViewModel?
    func initViewModel() {
        viewModel = OtherUserPostListViewModel()
        viewModel?.delegate = self
        viewModel?.loadOtherUserPostList(userId, offset: 0, success: { 
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            }, failed: { 
                self.failedRetryView?.loadFailed()
        })
    }
    
    override func refreshData() {
        let offset = 0
        viewModel?.loadOtherUserPostList(userId, offset: offset, success: { 
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_header.endRefreshing()
            }, failed: { 
                self.failedRetryView?.loadFailed()
                self.baseTableView?.mj_header.endRefreshing()
        })
    }
    
    override func loadMoreList() {
        let offset = self.viewModel?.count ?? 0
        viewModel?.loadOtherUserPostList(userId, offset: offset, success: { 
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_footer.endRefreshing()
            }, failed: { 
                self.failedRetryView?.loadFailed()
                self.baseTableView?.mj_footer.endRefreshing()
        })
    }
    //RetryViewDelegate
    override func retryToLoadData() {
        viewModel?.loadOtherUserPostList(userId, offset: 0, success: {
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            }, failed: {
                self.failedRetryView?.loadFailed()
        })
    }
    
    override func registerCell() {
        self.baseTableView?.register(BaseTopicCell.self, forCellReuseIdentifier: "BaseTopicCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:tableviewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return viewModel?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BaseTopicCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
        let model = viewModel?.getModel(indexPath.row)
        cell.model = model
        cell.rootVc=self
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = viewModel?.getModel(indexPath.row)
        let vc = ReadPostViewController()
        vc.postModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func ShowFlash() {
        HUD.flash(.label("No More Topics"),delay: 1)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = viewModel?.getModel(indexPath.row)
        return BaseTopicCell.calculateHeight(model)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
