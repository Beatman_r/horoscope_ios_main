//
//  NotificationLimitManager.swift
//  Horoscope
//
//  Created by Wang on 16/12/14.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class NotificationLimitManager: NSObject {
    
    static let sharedInstance = NotificationLimitManager()
    static let firstStartupKey:String = "FirstStartup"
    static let refuseTimes:String = "refuseTimes"
    static let registedNotification:String = "registedNotification"
    
    func firstStartUpApp () ->Bool{
        let first:Bool = !(UserDefaults.standard.bool(forKey: NotificationLimitManager.firstStartupKey))
        return first
    }
    //old update app version
    func isOldUpdateVersion() -> Bool {
        if UserDefaults.standard.bool(forKey: NotificationLimitManager.registedNotification) == true{
            return true
        }else{
            return false
        }
    }
    
    //wheather to show GuideHoroscopeViewController
    func showGuideOptionZodiac() -> Bool {
        if isOldUpdateVersion() == false {
            return true
        }
        if isOldUpdateVersion() == true && hasChooseZoidiac() == false {
            return true
        }else{
            return false
        }
    }

    func hasChooseZoidiac() -> Bool {
        let chooseArrayData = UserDefaults.standard.value(forKey: ChooseZodiacArray) as? Data
        if chooseArrayData == nil {
            return false
        }
        return true
}
    
    func refreshFirstStartApp(){
         UserDefaults.standard.set(false, forKey: NotificationLimitManager.firstStartupKey)
    }
    
    func registerLocalNotification(){
        let uns = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(uns)
        UserDefaults.standard.set(true, forKey: NotificationLimitManager.registedNotification)
        UserDefaults.standard.synchronize()
    }
    
    func refuseNotification(){
        let currentRefuseValue=UserDefaults.standard.integer(forKey: NotificationLimitManager.refuseTimes)
        let newValue = currentRefuseValue+1
        UserDefaults.standard.set(newValue, forKey: NotificationLimitManager.refuseTimes)
        UserDefaults.standard.synchronize()
    }
    
    func refreshGuideViewShowTime() {
        UserDefaults.standard.set(0, forKey: NotificationLimitManager.refuseTimes)
        UserDefaults.standard.synchronize()
    }
    
    func wheatherToShowGuideView() -> Bool{
        let userDefault = UserDefaults.standard
        if userDefault.bool(forKey: PushNotificationAllowed) == false && userDefault.integer(forKey: NotificationLimitManager.refuseTimes) <= 2{
            return true
        }else{
        return false
      }
    }
    
    
    func showSystemNotificationGuide(_ viewController:UIViewController) {
        let value=UserDefaults.standard.bool(forKey: NotificationLimitManager.registedNotification)
        if value == false{
            let notiVC = NotificationViewController()
            viewController.present(notiVC, animated: true, completion: nil)
        }
    }
    
    func postNotificationToDissmissGuide() {
        let notiCenter = NotificationCenter.default
        notiCenter.post(name: Notification.Name(rawValue: "RegisteredNotification"), object: nil)
    }
  
    
}
