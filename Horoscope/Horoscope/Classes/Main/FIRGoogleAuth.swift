//
//  FIRGoogleAuth.swift
//  bibleverse
//
//  Created by qi on 16/8/17.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FirebaseAuth

class FIRGoogleAuth: NSObject, LoginProtocol {
    override init() {
        super.init()
    }
    
    var user: GIDGoogleUser!
    
    func login(_ vc: UIViewController?) {
        guard let authentication = user.authentication else {
            self.loginFailed!("google login failed")
            return
        }
        let credential = FIRGoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                                     accessToken: authentication.accessToken)
        if let auth = FIRAuth.auth() {
           auth.signIn(with: credential, completion:
                { (user, error) in
                    if user != nil {
                        let userInfo = UserInfo()
                        userInfo.uid = user?.uid
                        userInfo.gid = UserInfo.getGid(userInfo.uid)
                        userInfo.userName = user?.displayName
                        userInfo.avatar = user?.photoURL?.absoluteString
                        userInfo.email = user?.email
                        userInfo.source = "google.com"
                        userInfo.sourceId = self.user.userID
                        let currentAuth = auth.currentUser
                        currentAuth?.getTokenForcingRefresh(true
                            , completion: { (idToken, error) in
                                if (error != nil) {
                                    self.loginFailed?("firebase anomymous auth failed")
                                }else{
                                    userInfo.token = idToken
                                    self.loginSuccess?(userInfo)
                                }
                        })
                    } else {
                        GIDSignIn.sharedInstance().signOut()
                        self.loginFailed?("firebase facebook auth failed")
                    }
            })
        } else {
            self.loginFailed?("firebase facebook login failed")
        }
    }
    var loginSuccess: returnsuccess?
    var loginFailed: returnfalse?
}
