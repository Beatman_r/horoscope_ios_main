//
//  MasterViewmodel.swift
//  Horoscope
//
//  Created by Wang on 17/2/28.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class MasterViewmodel: NSObject {

    var id:String?
    init(userId:String) {
        self.id = userId
    }
    
    var count:Int{
         return self.masterPostList.count
    }
    
    func getModel(_ row:Int) -> ReadPostModel {
        return self.masterPostList[row]
    }
    
    var masterPostList:[ReadPostModel]=[]
    
    var offSet = 0
    fileprivate let dao = HoroscopeDAO()
    func loadAndRefreshPostList(_ success:@escaping ()->(),failed:@escaping ()->()) {
          dao.getMasterPostList(id ?? "", offset: offSet, success: { (postList) in
            self.masterPostList.removeAll()
            if postList.count == 0 {
            }else{
               self.masterPostList += postList
            }
            self.offSet += postList.count
            success()
        }) { (failure) in
            failed()
        }
    }
    
    func loadMorePostList(_ success:@escaping (_ more:Bool) ->(),failed:@escaping () -> ()) {
        dao.getMasterPostList(id ?? "", offset: offSet, success: { (postList) in
            var ifmore = false
            if postList.count == 0 {
                ifmore = false
            }else{
                 ifmore = true
                self.masterPostList += postList
            }
            
            self.offSet += postList.count
            success(ifmore)
         }) { (failure) in
            failed()
        }
    }
    
    
    
    
}
