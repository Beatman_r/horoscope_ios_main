//
//  BaseDetailViewModel.swift
//  Horoscope
//
//  Created by Wang on 16/12/12.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class BaseDetailViewModel: NSObject {
    
    init(id:String) {
        super.init()
        self.breadId=id
        self.viewAction()
    }
    
    fileprivate var breadId:String?
    var detail:BreadModel?
    func loadDetail(_ success:@escaping ()->(),failure:@escaping (_ error:NSError)->()){
        DataManager.sharedInstance.breadInfoDao.getBread(self.breadId, success: { (model) in
            self.detail=model
            success()
        }) { (error) in
            failure(error)
        }
     }
    
    func viewAction(){
        DataManager.sharedInstance.breadInfoDao.viewBread(self.breadId ?? "", success: {
            
        }) { (error) in
            print("\(error)")
        }
        //DataManager.sharedInstance.breadInfoDao.viewBread(self.breadId ?? "")
    }
    
}

class videoPlayViewModel: BaseDetailViewModel{
    
    
}
