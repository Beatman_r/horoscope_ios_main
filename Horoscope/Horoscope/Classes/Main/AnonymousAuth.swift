//
//  FIRAnonymousAuth.swift
//  bibleverse
//
//  Created by qi on 16/8/17.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class AnonymousAuth: NSObject, LoginProtocol {
    func login(_ vc: UIViewController?) {
        if let auth = FIRAuth.auth() {
          
           auth.signInAnonymously(completion: { (user, error) in
                if user != nil {
                    let userInfo = UserInfo()
                    userInfo.uid = user!.uid
                    userInfo.gid = UserInfo.getGid(userInfo.uid)
                    self.loginSuccess?(userInfo)
                } else {
                    self.loginFailed?("firebase anomymous auth failed")
                }
            })
        } else {
            self.loginFailed?("firebase anomymous auth failed")
        }
    }

    var loginSuccess: returnsuccess?
    var loginFailed: returnfalse?
}
