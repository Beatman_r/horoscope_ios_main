//
//  VideoPostModel.swift
//  Horoscope
//
//  Created by Wang on 17/2/20.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class VideoPostModel: NSObject {

    var originalUrl:String?
    var figure:String?
    var richmedia:String?
    var richMediaDuration:String?
    var richMediaWebsite:String?
    var thumbnail:String?
 
    init(json:JSON) {
        super.init()
        self.originalUrl          = json["originalUrl"].string
        self.figure               = json["figure"].string
        self.richmedia            = json["richMedia"].string
        self.richMediaDuration    = json["richMediaDuration"].string
        self.richMediaWebsite      = json["richMediaWebsite"].string
        self.thumbnail           = json["thumbnail"].string
    }
    init(dic:[String:AnyObject]) {
        super.init()
        self.originalUrl       = dic["originalUrl"] as? String
        self.figure       = dic["figure"] as? String
        self.richmedia       = dic["richMedia"] as? String
        self.richMediaDuration = dic["richMediaDuration"] as? String
        self.richMediaWebsite  = dic["richMediaWebsite"] as? String
        self.thumbnail         = dic["thumbnail"] as? String
    }
 
    
}
