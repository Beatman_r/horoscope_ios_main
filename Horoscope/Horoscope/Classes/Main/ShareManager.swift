//
//  ShareOperationConfig.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/12.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class ShareManager: NSObject {
    
    #if DEBUG
    let baseUrl = "http://idailybread.com/test"
    #else
    let baseUrl = "http://idailybread.com/"
    #endif
    
    var shareUrl: String?
    var shareIma: UIImage?
    
    func shareVod(_ dateString:String, date:Date, fromeVC:UIViewController) {
       
        var shareUrl:String = baseUrl
        #if DEBUG
            shareUrl += "ShareVodtest.html?date="
            shareUrl += dateString
            shareUrl += "&translation="
           
        #else
            shareUrl += "shareVodtest.html?date="
            shareUrl += dateString
            shareUrl += "&translation="
           
        #endif
        self.shareUrl = shareUrl
        let dateInteval = date.timeIntervalSince1970
        if dateInteval < 1472918400 {
            let shareDao = ShareDao.shareInstance
        //  AnaliticsManager.sendEvent(AnaliticsManager.verse_image_share_to, data: ["to":"other"])
            shareDao.localLink(fromeVC, url: shareUrl)
        } else {
            createSelectView(fromeVC)
        }
    }
    
    func shareBread(_ breadId:String, fromeVC:UIViewController) {
        var shareUrl:String = baseUrl
        #if DEBUG
            shareUrl += "ShareDevotionaltest.html?breadId="
            shareUrl += breadId
        #else
            shareUrl += "shareDevotionaltest.html?breadId="
            shareUrl += breadId
        #endif
        self.shareUrl = shareUrl
        createSelectView(fromeVC)
    }
    
    func shareImage(_ image:UIImage, fromeVC:UIViewController) {
        self.shareIma = image
        createSelectView(fromeVC)
    }

    fileprivate func createSelectView(_ fromVC:UIViewController) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel) {
            (action: UIAlertAction!) -> Void in
        }
        let deleteAction = UIAlertAction(title: NSLocalizedString("Share to Facebook", comment: ""), style: UIAlertActionStyle.default) {
            (action: UIAlertAction!) -> Void in
           //  AnaliticsManager.sendEvent(AnaliticsManager.verse_image_share_to, data: ["to":"facebook"])
            let shareDao = ShareDao.shareInstance
            if let url = self.shareUrl {
                shareDao.faceBookLink(fromVC, url: url)
            }
            if let image = self.shareIma {
                shareDao.faceBookIma(fromVC, image: image)
            }
        }
        let archiveAction = UIAlertAction(title: NSLocalizedString("Share to Other", comment: ""), style: UIAlertActionStyle.default) {
            (action: UIAlertAction!) -> Void in
         //   AnaliticsManager.sendEvent(AnaliticsManager.verse_image_share_to, data: ["to":"other"])
            let localDao = ShareDao.shareInstance
            if let url = self.shareUrl {
                localDao.localLink(fromVC, url: url)
            }
            if let image = self.shareIma {
                localDao.localIma(fromVC, image: image)
            }
        }
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        alertController.addAction(archiveAction)
        fromVC.present(alertController, animated: true, completion: nil)
    }
}
