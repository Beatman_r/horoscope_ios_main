//
//  VideoViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/9.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class SectionVideoViewModel:NSObject{
    
    fileprivate var topList:[BreadModel]?=[]
    fileprivate var newList:[BreadModel]?=[]
    
    func count(_ section:Int) -> Int {
        if section == 0 {
            return self.topList?.count ?? 0
        }
        return self.newList?.count ?? 0
    }
    
    func get(_ type:String,index:Int) -> BreadModel? {
        if type == BreadRequestWay.top.rawValue {
            return self.topList![index]
        }else{
            return self.newList?[index]
        }
    }
    
    func loadData(_ success:@escaping ()->(),failure:@escaping (_ errors:NSError)->()) {
        DataManager.sharedInstance.breadInfoDao.getAllTypeBreadList({ (topList, newList) in
            self.topList = topList
            self.newList = newList
            success()
        }) { (error) in
            failure(error)
            
        }
    }
}

class VideoViewController: BaseTableViewController{
    
    let screenHeight:CGFloat = UIScreen.main.bounds.height
    let screenWidth:CGFloat = UIScreen.main.bounds.width
    var sectionVideoViewModel:SectionVideoViewModel?
    var dataLoaded = false
    var adHeight:CGFloat = 0
    var adWidth:CGFloat = 0
    
    var arr :[UIView]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        DataManager.sharedInstance.campaingInfo.getCampainList(CampaignManager.videoPosition, success: { (list) in
            self.initOperationScroll(list)
        }) { (error) in
        }
        
        sectionVideoViewModel=SectionVideoViewModel()
        self.viewModelRequestData()
    }
    
    func viewModelRequestData()  {
        sectionVideoViewModel?.loadData({
            self.failedRetryView?.loadSuccess()
            self.dataLoaded = true
            self.tableView.reloadData()
            }, failure: { (errors) in
                self.failedRetryView?.loadFailed()
        })
    }
    
    //MARK:retryDelegate
    override func retryToLoadData() {
        self.viewModelRequestData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    var scrollV:HsScrollView?
    func initOperationScroll(_ list:[CampaignModel]) {
        scrollV = HsScrollView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: RecommandVideoView.height))
        arr?.removeAll()
        for index in 0..<list.count {
            let model = list[index]
            let view = RecommandVideoView.create(model,viewController:self,index:index+1)
            arr?.append(view)
        }
        scrollV?.refreshView(arr!)
        self.tableView.tableHeaderView=scrollV
    }
    
    override func registerCell() {
        self.tableView.register(VideoCell.self, forCellReuseIdentifier: "videoListCell")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return sectionVideoViewModel?.count(section) ?? 0
        }else if section == 1{
            return sectionVideoViewModel?.count(section) ?? 0
        }else {
            return 0
        }
    }
    
    let position1 = 1//FIXME:need reconsitution
    let position2 = 1
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
                let model:BreadModel? = sectionVideoViewModel?.get(BreadRequestWay.top.rawValue, index: indexPath.row)
                let cell:VideoCell = VideoCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath, model: model!)
                let lastindex:Int  = tableView.numberOfRows(inSection: indexPath.section)
                if indexPath.row == lastindex {
                    cell.divideview.isHidden = true
                }
                return cell
        }else if indexPath.section == 1{
                let model:BreadModel? = sectionVideoViewModel?.get(BreadRequestWay.new.rawValue, index: indexPath.row)
                let cell:VideoCell = VideoCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath, model: model!)
                let lastindex:Int  = tableView.numberOfRows(inSection: indexPath.section)
                if indexPath.row == lastindex {
                    cell.divideview.isHidden = true
                }
                return cell
        }
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = VideoHeaderView()
            let type:String? = self.sectionVideoViewModel?.topList?.first?.sectionId
            headerView.createSectionHeaderView((type ?? "").capitalized)
            headerView.rootVC = self
            return headerView
        }else{
            let headerView:VideoHeaderView = VideoHeaderView()
            
            let type = self.sectionVideoViewModel?.newList?.first?.sectionId
            headerView.createSectionHeaderView((type ?? "").capitalized)
            
            headerView.rootVC = self
            return headerView
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.cellDidClicked(indexPath.section, row: indexPath.row)
    }
    func cellDidClicked(_ section:Int, row:Int) {
        if section == 0 {
            let type = self.sectionVideoViewModel?.topList?.first?.sectionId
            let name = self.sectionVideoViewModel?.topList?[row].title
            AnaliticsManager.sendEvent(AnaliticsManager.video_section_click, data: ["\(type ?? "")":"\(name ?? "")"])
        }
        if section == 1 {
            let type = self.sectionVideoViewModel?.newList?.first?.sectionId
            let name = self.sectionVideoViewModel?.newList?[row].title
            AnaliticsManager.sendEvent(AnaliticsManager.video_section_click, data: ["\(type ?? "")":"\(name ?? "")"])
           
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tableView {
            let sectionHeaderHeight: CGFloat = 35
            if scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0 {
                scrollView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
            }else if scrollView.contentOffset.y >= sectionHeaderHeight {
                scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 49, 0)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return VideoCell.rowHeight
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.dataLoaded == false {
            return 0
        }
        return 45
    }
    
    func showSettingButton() {
        let button = SettingButton(type:.custom)
        button.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        button.setImage(UIImage(named: "ic_more_"), for: UIControlState())
        button.addTarget(self, action: #selector(self.onGotoSetting), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: button)
    }
    
    @objc func onGotoSetting() {
        let vc = MeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
