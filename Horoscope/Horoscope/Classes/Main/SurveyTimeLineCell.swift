//
//  SurveyTimeLineCell.swift
//  Horoscope
//
//  Created by Beatman on 2017/5/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class SurveyTimeLineCell: NotificationCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let gradientLayer: CAGradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [UIColor(hexString: "2B1439", withAlpha: 0.0)!.cgColor, UIColor(hexString: "2B1439", withAlpha: 1.0)!.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var surveyModel : SurveyCellModel?{
        didSet{
            self.renderCell()
        }
    }
    
    override func renderCell() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        if self.surveyModel != nil {
            if let authorImaStr = surveyModel?.surveyImage {
                bgImg.sd_setImage(with: URL(string:authorImaStr), placeholderImage: UIImage(named:"defaultImg1.jpg"))
            }
            self.titleLab.text = self.surveyModel?.surveyTitle ?? ""
        }
    }
    override func cellDidSelect(){
        if let url = surveyModel?.surveyUrl {
            UIApplication.shared.openURL(URL(string:url)!)
        }
    }
    
    static let surveyCellId="surveyCell"
    override class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> NotificationCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: surveyCellId) as! SurveyTimeLineCell
        return cell
    }
    override class func calculateHeight(_ title:String?) ->CGFloat{
        let titleHeight = title == nil ? 0 : HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: title ?? "", fontSize: 20, font: HSFont.baseRegularFont(20), width: UIScreen.main.bounds.size.width-20, multyplyLineSpace: 1)
        print(titleHeight)
        return (titleHeight + imgHeight + BaseCardCell.cellPadding + 22)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
