//
//  NotificationManager.swift
//  Horoscope
//
//  Created by Wang on 16/12/1.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

enum NotificationType {
    case morning
    case evening
 }

class NotificationManager: NSObject {
      static let sharedInstance = NotificationManager()
      var dateFormatter = DateFormatter()
    
      func allowMorningNoti(_ status:Bool) {
        UserDefaults.standard.set(status, forKey: "\(NotificationType.morning)")
      }
    
      func allowEveningNoti(_ status:Bool) {
        UserDefaults.standard.set(status, forKey: "\(NotificationType.evening)")
      }
    
      func morningNotiAllowStatus() -> Bool {
        return  UserDefaults.standard.bool(forKey: "\(NotificationType.morning)")
      }
    
      func eveningNotiAllowStatus() -> Bool {
          return UserDefaults.standard.bool(forKey: "\(NotificationType.evening)")
      }
    
      func clearAndSetAll(_ notificationTypes:[NotificationType],
                                duration:Int)  {
        UIApplication.shared.cancelAllLocalNotifications()
        for notificationType in notificationTypes{
            var act1:NotificationProtocol?
             switch notificationType {
            case .morning:
            act1=MorningNotification(dateformatter: self.dateFormatter)
            case .evening:
            act1=EveningNotification(dateformatter: self.dateFormatter );
            }
            act1?.addNotification(duration)
        }
    }
 }
