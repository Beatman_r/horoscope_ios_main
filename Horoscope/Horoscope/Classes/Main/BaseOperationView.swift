//
//  BaseOperationView.swift
//  Horoscope
//
//  Created by Wang on 16/12/8.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class BaseOperationView: UIView {

    func editUI() {
        self.backgroundColor = UIColor.white
//        self.addSubview(titleBackview)
//        self.titleBackview.addSubview(titleLab)
        self.addSubview(img)
    }
    
//    let titleBackview:UIView = {
//        let view = UIView()
//        view.backgroundColor=UIColor.cardBackColor()
//        return view
//    }()
    
//    let titleLab:UILabel = {
//        let lab = UILabel()
//        lab.numberOfLines = 1
//        lab.textAlignment = .Left
//        lab.font=HSFont.baseMediumFont(18)
//        lab.textColor = UIColor.hsTextColor(1)
//        lab.backgroundColor=UIColor.clearColor()
//        return lab
//    }()
    
    let img:UIImageView = {
        let view = UIImageView()
        view.backgroundColor=UIColor.white
        return view
    }()
    
    func addConstrains(){
//         titleBackview.snp.makeConstraints { (make) in
//            make.bottom.equalTo(self.snp.bottom)
//            make.left.equalTo(self.snp.left)
//            make.right.equalTo(self.snp.right)
//            make.height.equalTo(36)
//         }
    
//         titleLab.snp.makeConstraints { (make) in
//            make.top.equalTo(self.titleBackview.snp.top)
//            make.left.equalTo(self.snp.left).offset(12)
//            make.right.equalTo(self.snp.right).offset(-8)
//            make.bottom.equalTo(self.titleBackview.snp.bottom)
//         }
    
         img.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(self.snp.bottom)
         }
    }
 }
