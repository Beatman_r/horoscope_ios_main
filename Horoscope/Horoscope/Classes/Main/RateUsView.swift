//
//  RateUsView.swift
//  Horoscope
//
//  Created by Beatman on 16/12/3.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

//protocol RateUsViewDelegate:class{
//    func starBtnOnClick()
//}

class RateUsView: UIView {
    weak var rootvc:UIViewController?
    var index = 0
    var flag = false
    var buttons = [UIButton]()
    var bgButtons = [UIButton]()
    fileprivate var shouldShowRateUsKey:String="HS_hasRate"
    class func shouldShowRateUs()->Bool{
        return !(UserDefaults.standard.bool(forKey: "hasRate"))
    }
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    func ratedOrCanceled(){
        UserDefaults.standard.set(true, forKey: "hasRate")
        UserDefaults.standard.synchronize()
    }
    
    class func create(_ viewcontroller:UIViewController) ->RateUsView{
        let view = RateUsView()
        view.createRateUs()
        view.rootvc=viewcontroller
        view.tapAction()
        return view
    }
    
    func tapAction() {
        let tap=UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(self.onSelectedCurrentView))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled=true
    }
    
    @objc func onSelectedCurrentView() {
        
    }
    
    func createRateUs() {
        var starYposition:CGFloat = 115
        if UIScreen.main.bounds.size.width == 375{
            starYposition = 130
        }else if UIScreen.main.bounds.size.width == 414{
            starYposition = 145
        }
        
        let starWidth:CGFloat=35
        let starHeight:CGFloat=35
        let jiange:CGFloat=15
        let startX=(screenWidth-starWidth*5-jiange*4)/2
        self.backgroundColor = UIColor.white
        self.createLabels()
        for index in 0...4 {
            let bgButton:UIButton = {
                let button = UIButton(type: .custom)
                button.tag = index + 10
                button.frame = CGRect(x: startX+CGFloat(index)*(starWidth+jiange), y: starYposition, width: starWidth, height: starHeight)
                button.addTarget(self, action: #selector(rateButtonOnClick(_:)), for: .touchUpInside)
                button.setImage(UIImage(named:"ic_rate_star_empty_small")?.withRenderingMode(.alwaysOriginal), for: UIControlState())
                return button
            } ()
            self.addSubview(bgButton)
            let fButton:UIButton = {
                let button = UIButton(type: .custom)
                button.alpha = 0
                button.tag = index + 10
                button.addTarget(self, action: #selector(rateButtonOnClick(_:)), for: .touchUpInside)
                button.frame = CGRect(x: startX+CGFloat(index)*(starWidth+jiange), y: starYposition, width: starWidth, height: starHeight)
                button.setImage(UIImage(named:"card_star")?.withRenderingMode(.alwaysOriginal), for: UIControlState())
                return button
            } ()
            self.buttons.append(fButton)
            self.bgButtons.append(bgButton)
            self.addSubview(fButton)
        }
        self.createTimer()
    }
    
    func dismissCloseButton() {
        UIView.animate(withDuration: 0.4, animations: {
            self.frame = CGRect(x: 10, y: 64, width: UIScreen.main.bounds.width-20, height: 0.01)
        }) 
        self.ratedOrCanceled()
    }
    
    let img:UIImageView = {
        let view = UIImageView()
        view.backgroundColor=UIColor.white
        return view
    }()
    
    func createLabels() {
        let rateTopView = UIView()
        rateTopView.backgroundColor = UIColor.cardBackColor()
       self.addSubview(rateTopView)
        rateTopView.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.snp.bottom).offset(-36)
            make.bottom.equalTo(self.snp.bottom)
        }
        self.addSubview(img)
        img.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(self.snp.bottom)
        }
    }
    
    func createTimer() {
        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(fullTheStar), userInfo: nil, repeats: true)
    }
    
    @objc func fullTheStar() {
        if self.flag == false {
            if self.buttons.count > 0 {
                if self.index == 0{
                    for i in 1 ..< self.buttons.count {
                        self.buttons[i].alpha = 0
                        self.bgButtons[i].alpha = 1
                    }
                }
                if self.buttons.count > self.index {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.buttons[self.index].alpha = 1
                        self.bgButtons[self.index].alpha = 0
                    })
                    self.index += 1
                    if self.index >= 5 {
                        self.index = 0
                        self.flag = true
                        UIView.animate(withDuration: 0.4, animations: {
                            }, completion: { (finished:Bool) in
                                UIView.animate(withDuration: 0.4, animations: {
                                    }, completion: { (finished:Bool) in
                                        self.flag = false
                                })
                        })
                    }
                } else {
                    index = 0
                }
            }
        }
    }
    
    @objc func rateButtonOnClick(_ sender:UIButton) {
        if sender.tag-10 == 4 {
            let urlString = "itms-apps://itunes.apple.com/app/id1184938845"
            let url = URL(string: urlString)
            UIApplication.shared.openURL(url!)
            let notiCenter = NotificationCenter.default
            notiCenter.post(name: Notification.Name(rawValue: "rateButtonOnClick"), object: nil)
        }else{
            AnaliticsManager.sendEvent(AnaliticsManager.forecast_banner_click_rate_us, data: ["sign":"\(sender.tag-9)"])
             let notiCenter = NotificationCenter.default
            notiCenter.post(name: Notification.Name(rawValue: "rateButtonOnClick"), object: nil)
            let rateDetailVC = RateDetailViewController()
            rateDetailVC.starRank = sender.tag+200-10
            self.rootvc?.navigationController?.pushViewController(rateDetailVC, animated: true)
         }
        self.ratedOrCanceled()
    }
    
}
