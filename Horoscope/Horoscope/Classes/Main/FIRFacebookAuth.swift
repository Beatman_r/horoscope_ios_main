//
//  FIRFacebookAuth.swift
//  bibleverse
//
//  Created by qi on 16/8/17.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import FirebaseAuth
class FIRFacebookAuth: NSObject, LoginProtocol {
    
    override init() {
        super.init()
    }
    
    func login(_ vc: UIViewController?) {
        let login = FBSDKLoginManager()
        login.logOut()
        login.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: vc) { (result, error) in
            if (error != nil) {
                AnaliticsManager.sendEvent(AnaliticsManager.facebook_login, data: ["result": "fail"])
                self.loginFailed?("Failed")
            }
            else if (result?.isCancelled)! {
                self.loginFailed?("Failed")
                AnaliticsManager.sendEvent(AnaliticsManager.facebook_login, data: ["result": "fail"])
            } else {
                if let facebookLoginViewController = vc as? HsLoginViewController {
                    facebookLoginViewController.showProccessHUD()
                }
                AnaliticsManager.sendEvent(AnaliticsManager.facebook_login, data: ["result": "success"])
                let fbToken = result?.token
                AnaliticsManager.sendEvent(AnaliticsManager.facebook_token)
                FBSDKAccessToken.setCurrent(fbToken)
                FBSDKProfile.loadCurrentProfile(completion: { (profile, error) in
                    if (error != nil) {
                        print("ProfileError\(error ?? "" as! Error)")
                        self.loginFailed?("Failed")
                     } else {
                        let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                        if let auth = FIRAuth.auth() {
                            auth.signIn(with: credential, completion:
                                { (user, error) in
                                    if user != nil {
                                        let userInfo = UserInfo()
                                        userInfo.uid = user?.uid
                                        userInfo.gid = UserInfo.getGid(userInfo.uid)
                                        userInfo.userName = user?.displayName
                                        userInfo.avatar = profile?.imageURL(for: .normal, size: CGSize(width: 100,height: 100)).absoluteString
                                        userInfo.email = user?.email
                                        userInfo.source = "facebook.com"
                                        userInfo.sourceId = profile?.userID
                                        
                                        let currentAuth = auth.currentUser
                                        currentAuth?.getTokenForcingRefresh(true
                                            , completion: { (idToken, error) in
                                                if (error != nil) {
                                                    self.loginFailed?("firebase anomymous auth failed")
                                                }else{
                                                    userInfo.token = idToken
                                                    self.loginSuccess?(userInfo)
                                                }
                                        })
                                    } else {
                                        self.loginFailed?("firebase facebook auth failed")
                                    }
                            })
                            } else {
                            self.loginFailed?("firebase facebook login failed")
                        }
                    }
                })
            }
        }
    }
    
    var loginSuccess: returnsuccess?
    var loginFailed: returnfalse?
}
