//
//  HomeConfigViewModel.swift
//  Horoscope
//
//  Created by Wang on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


enum HomeCellType:Int {
    case rateUS
    case tarot
    case cookie
    
    case breadFigure
    case breadThumbnail
    case horoscopeToday
   
}

class HomeConfigViewModel: NSObject {
    
    var horoKey:String?
    init(key:String) {
        self.horoKey = key
    }
    
    func getCount(_ sec:Int) ->Int {
        if sec == 0{
            return self.campaignList.count
        }else if sec == 1{
            return self.finalDataList.count
        }
        return 0
    }
    
    func getSection0Model(_ row:Int) -> BaseCardListModel? {
         return self.campaignList[row]
    }
    
    func getSection1Model(_ sec:Int,row:Int) -> basePostModel? {
         return self.finalDataList[row]
    }
    
    var campaignList:[BaseCardListModel]=[]
    fileprivate var finalDataList:[basePostModel] = []
    var postList:[ReadPostModel] = []
    
    
    fileprivate let dao = TimelineDao()
    fileprivate var dataNumber:Int = 0
    var offSet = 0
    
    func insertOneNewPost(_ model:ReadPostModel,success:() ->(),failure:(_ error:NSError) -> ()) {
        self.finalDataList.insert(model, at: 0)
        success()
    }
    
    func loadMoreData(_ success:@escaping (_ more:Bool) ->(),failure:@escaping (_ error:NSError) -> ()) {
        dao.loadTimelineDataV2(offSet, date: Date(), horoscopeKey:self.horoKey ?? "" , success: { (postList,campainList,adlist,count) in
            print("更多偏移\(self.offSet)")
            var ifMore:Bool = false
            if  postList.count == 0{
                ifMore = false
            }else{
                ifMore = true
                self.postList += postList
            }
            self.offSet += count
            self.finalDataList.removeAll()
            self.finalDataList=self.getFinalDataList(adlist)
            success(ifMore)
        }) { (error) in
            failure(error)
        }
    }
    
    func loadConfig(_ success: @escaping () ->(),failure: @escaping (_ error:NSError) -> ()) {
         dao.loadTimelineDataV2(offSet, date: Date(), horoscopeKey:self.horoKey ?? "" , success: { (postList,campainList,adlist,count) in
            print("偏移\(self.offSet)")
            self.postList.removeAll()
            self.postList=postList
            self.campaignList.removeAll()
            self.campaignList=campainList
            self.finalDataList.removeAll()
            self.finalDataList=self.getFinalDataList(adlist)
            self.offSet = count
            success()
            }) { (error) in
            failure(error)
         }
    }
    
    func getFinalDataList(_ adArr:[basePostModel])->[basePostModel] {
          for eachPost in postList{
            finalDataList.append(eachPost)
          }
          for eachModel in adArr{
            let pos = eachModel.position
            if pos >= self.finalDataList.count{
              self.finalDataList.append(eachModel)
            }else{
              self.finalDataList.insert(eachModel, at: pos!)
            }
         }
//         print("下拉刷新\(finalDataList)")
         return finalDataList
    }
 
}
