//
//  MyCommentModel.swift
//  Horoscope
//
//  Created by Beatman on 17/2/16.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyCommentModel: NSObject {
    
    var content : String?
    var createTimeHuman : String?
    var user:User?
    var anonymous : Bool?
    var dislikeCount : Int?
    var commentId : String?
    var imageList : [AnyObject]?
    var pos : Int?
    var createTime : Int64?
    var likeCount : Int?
    var total : Int?
    var size : Int?
    var hasImage : Bool?
    var iliked:Bool? = false
    var iDisliked:Bool? = false
    var postId:String?
    
    init(jsonData : JSON, num : Int) {
        super.init()
        self.dislikeCount = jsonData["commentList"][num]["dislikeCount"].intValue 
        self.likeCount = jsonData["commentList"][num]["likeCount"].intValue 
        self.iliked = jsonData["commentList"][num]["isLiked"].boolValue
        self.postId = jsonData["commentList"][num]["postId"].stringValue 
        self.iDisliked = jsonData["commentList"][num]["isDisliked"].boolValue
        self.anonymous = jsonData["commentList"][num]["anonymous"].boolValue 
        self.createTime = jsonData["commentList"][num]["createTime"].int64Value 
        self.content = jsonData["commentList"][num]["content"].stringValue 
        self.createTimeHuman = jsonData["commentList"][num]["createTimeHuman"].stringValue 
        self.imageList = jsonData["commentList"][num]["imageList"].arrayObject! as [AnyObject]
        self.total = jsonData["commentList"][num]["total"].intValue 
        self.size = jsonData["commentList"][num]["size"].intValue 
        self.commentId = jsonData["commentList"][num]["commentId"].stringValue 
        self.pos = jsonData["commentList"][num]["pos"].intValue 
        if let  userJson = jsonData["commentList"][num]["userInfo"].dictionaryObject{
            let user = User()
            user.initWitchDic(userJson as [String : AnyObject])
            self.user = user
        }
        self.hasImage = self.imageList!.count > 0 ? true : false
    }
}
