//
//  NotificationProtocol.swift
//  Horoscope
//
//  Created by Wang on 16/12/1.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

protocol NotificationProtocol {
    func addNotification(_ durationTime:Int)
}

class BaseNotification:NSObject{
    func calcFireTime(_ index:Int, pushTime:TimeInterval)->Date {
        let detalTime:TimeInterval = pushTime + (TimeInterval)(86400*index)
        let startOfToday = Calendar.current.startOfDay(for: Date())
        let fireDate = startOfToday.addingTimeInterval(detalTime)
        return fireDate
    }
}
