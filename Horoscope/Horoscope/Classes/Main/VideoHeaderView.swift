//
//  VideoHeaderView.swift
//  Horoscope
//
//  Created by Beatman on 16/12/12.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import PKHUD

class VideoHeaderView: UIView {
    let typeLabel = UILabel()
    let seeAllButton = UIButton()
    let screenWidth = UIScreen.main.bounds.width
    weak var rootVC : UIViewController?
    class func create(_ viewController:UIViewController,type:String){
        let view = VideoHeaderView()
        view.createSectionHeaderView(type)
        view.rootVC = viewController
    }
    
    func createSectionHeaderView(_ type:String) {
        typeLabel.text = type
        typeLabel.textColor = UIColor.hsTextColor(1)
        typeLabel.font = HSFont.baseMediumFont(20)
        typeLabel.textAlignment = NSTextAlignment.left
        
        seeAllButton.setTitle("See All", for: UIControlState())
        seeAllButton.setTitleColor(UIColor.hsViewcountsColor(), for: UIControlState())
        seeAllButton.addTarget(self, action: #selector(goDetailListView), for: .touchUpInside)
        seeAllButton.titleLabel?.font = HSFont.baseMediumFont(20)
        seeAllButton.titleLabel?.textAlignment = NSTextAlignment.right
        self.addSubview(typeLabel)
        self.addSubview(seeAllButton)
        typeLabel.snp.makeConstraints { (make) in
//            make.top.equalTo(self.snp.top).offset(10)
//            make.bottom.equalTo(self.snp.bottom).offset(0)
//            make.left.equalTo(self.snp.left).offset(20)
//            make.right.equalTo(self.snp.left).offset(100)
            make.left.equalTo(self.snp.left).offset(screenWidth*0.04)
            make.centerY.equalTo(self.snp.centerY)
        }
        seeAllButton.snp.makeConstraints { (make) in
//            make.top.equalTo(self.snp.top).offset(10)
//            make.bottom.equalTo(self.snp.bottom).offset(0)
//            make.left.equalTo(self.snp.right).offset(-100)
//            make.right.equalTo(self.snp.right).offset(-8)
            make.right.equalTo(self.snp.right).offset(-screenWidth*0.04)
            make.centerY.equalTo(self.snp.centerY)
        }
    }
    
    @objc func goDetailListView() {
        if self.typeLabel.text == BreadRequestWay.top.rawValue.capitalized {
            AnaliticsManager.sendEvent(AnaliticsManager.video_section_see_all_click, data: ["section_name":"top"])
            let listVC = VideoListViewController.create(.top)
            self.rootVC?.navigationController?.pushViewController(listVC, animated: true)
        }else if self.typeLabel.text == BreadRequestWay.new.rawValue.capitalized {
            AnaliticsManager.sendEvent(AnaliticsManager.video_section_see_all_click, data: ["section_name":"new"])
            let listVC = VideoListViewController.create(.new)
            self.rootVC?.navigationController?.pushViewController(listVC, animated: true)
        }
    }
}
