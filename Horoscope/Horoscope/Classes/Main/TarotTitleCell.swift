//
//  TarotTitleCell.swift
//  Horoscope
//
//  Created by Beatman on 16/12/29.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class TarotTitleCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
    }
    
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    
    let bgImg = UIImageView()
    let topLeftPlaceholder = UIImageView()
    let cardImg = UIImageView()
    var cardNameLabel = UILabel()
    var keyWordLabel = UILabel()
    var astrologyLabel = UILabel()
    var cardNameDetailLabel = UILabel()
    var keyWordDetailLabel = UILabel()
    var astrologyDetailLabel = UILabel()
    
    func createTitleLabel(_ title:String) -> UILabel{
        let lab = UILabel()
        lab.text = title
        lab.textColor = UIColor.init(red: 223/255, green: 68/255, blue: 181/255, alpha: 1)
        lab.font = HSFont.baseMediumFont(self.screenHeight == 480 ? 13 : 15)
        lab.numberOfLines = 0
        return lab
    }
    
    func createDetailLabel() -> UILabel{
        let lab = UILabel()
        lab.textColor = UIColor.white
        lab.font = HSFont.baseRegularFont(self.screenHeight == 480 ? 10 : 12)
        lab.numberOfLines = 0
        return lab
    }
    
    func createUI() {
        self.backgroundColor = UIColor.clear
        self.bgImg.image = UIImage(named: "titleCard")
        self.topLeftPlaceholder.image = UIImage(named: "taort_bg")
        
        self.cardNameLabel = self.createTitleLabel(NSLocalizedString("Name of The Card", comment: ""))
        self.keyWordLabel = self.createTitleLabel(NSLocalizedString("Key Interpretations", comment: ""))
        self.astrologyLabel = self.createTitleLabel(NSLocalizedString("Astrology", comment: ""))
        self.cardNameDetailLabel = self.createDetailLabel()
        self.keyWordDetailLabel = self.createDetailLabel()
        self.astrologyDetailLabel = self.createDetailLabel()
        self.cardImg.image = UIImage(named: "kingofpentacles")
        self.addSubviews()
        self.bgImg.snp.makeConstraints { (make) in
            make.edges.equalTo(self.contentView.snp.edges)
        }
        self.topLeftPlaceholder.snp.makeConstraints { (make) in
            make.top.equalTo(self.bgImg.snp.top).offset(7)
            make.left.equalTo(self.bgImg.snp.left).offset(7)
            make.right.equalTo(self.bgImg.snp.left).offset(self.screenWidth * 100/375)
            make.bottom.equalTo(self.bgImg.snp.bottom).offset(-7)
        }
        
        self.cardImg.snp.makeConstraints { (make) in
            make.edges.equalTo(self.topLeftPlaceholder.snp.edges)
        }
        
        self.cardNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.bgImg.snp.top).offset(self.screenHeight * 11/667)
            make.left.equalTo(self.cardImg.snp.right).offset(5)
            make.right.equalTo(self.bgImg.snp.right)
            make.bottom.equalTo(self.cardNameLabel.snp.top).offset(17)
        }
        self.cardNameDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.cardNameLabel.snp.bottom).offset(5)
            make.left.equalTo(self.cardImg.snp.right).offset(5)
            make.right.equalTo(self.snp.right).offset(-5)
        }
        self.keyWordLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.cardNameDetailLabel.snp.bottom).offset(self.screenWidth == 320 ? 5 : 20)
            make.left.equalTo(self.cardImg.snp.right).offset(5)
            make.right.equalTo(self.snp.right).offset(-5)
        }
        self.keyWordDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.keyWordLabel.snp.bottom).offset(5)
            make.left.equalTo(self.cardImg.snp.right).offset(5)
            make.right.equalTo(self.snp.right).offset(-5)
        }
        self.astrologyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.keyWordDetailLabel.snp.bottom).offset(self.screenWidth == 320 ? 5 : 20)
            make.left.equalTo(self.cardImg.snp.right).offset(5)
            make.right.equalTo(self.snp.right).offset(-5)
        }
        self.astrologyDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.astrologyLabel.snp.bottom).offset(5)
            make.left.equalTo(self.cardImg.snp.right).offset(5)
            make.right.equalTo(self.snp.right).offset(-5)
        }
    }
    
    func addSubviews() {
        self.contentView.addSubview(bgImg)
        self.bgImg.addSubview(topLeftPlaceholder)
        self.topLeftPlaceholder.addSubview(cardImg)
        self.bgImg.addSubview(cardNameLabel)
        self.bgImg.addSubview(keyWordLabel)
        self.bgImg.addSubview(astrologyLabel)
        self.bgImg.addSubview(cardNameDetailLabel)
        self.bgImg.addSubview(keyWordDetailLabel)
        self.bgImg.addSubview(astrologyDetailLabel)
    }
    
   required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
