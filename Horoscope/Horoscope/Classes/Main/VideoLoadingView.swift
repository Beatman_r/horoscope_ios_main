//
//  VideoLoadingView.swift
//  Horoscope
//
//  Created by Wang on 16/12/13.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

protocol VideoLoadingViewDelegate:class {
    
    func retry()
}

class VideoLoadingView: UIView {
    weak var delegate:VideoLoadingViewDelegate?
    class func create()->VideoLoadingView{
        let view = VideoLoadingView()
        view.editUI()
        view.addLoading()
        view.addRetryBtn()
        return view
    }
    
    fileprivate func editUI(){
        self.backgroundColor = UIColor.black
    }
    
    var indiacator:UIActivityIndicatorView?
    fileprivate func addLoading(){
        indiacator = UIActivityIndicatorView()
        self.addSubview(self.indiacator!)
        indiacator!.snp.makeConstraints { (make) in
           make.centerX.equalTo(self.snp.centerX)
           make.centerY.equalTo(self.snp.centerY).offset(30)
           make.height.equalTo(100)
           make.width.equalTo(100)
        }
        self.indiacator?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        self.indiacator?.color = UIColor.white
        self.indiacator?.hidesWhenStopped = true
    }
    
    var retryBtn:UIButton?
    func addRetryBtn() {
        retryBtn=UIButton(type:.custom)
        retryBtn?.setTitle(NSLocalizedString("RETRY", comment: ""), for: UIControlState())
        retryBtn?.addTarget(self, action: #selector(self.retryBtnClicked), for: .touchUpInside)
        retryBtn?.isHidden=true
        retryBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        retryBtn?.setTitleColor(UIColor.white, for: UIControlState())
        retryBtn?.backgroundColor = UIColor.hsYouTubuttonColor()
        retryBtn?.layer.cornerRadius = 5
        retryBtn?.layer.masksToBounds = true
        self.addSubview(retryBtn!)
        self.bringSubview(toFront: retryBtn!)
        retryBtn!.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY).offset(30)
            make.height.equalTo(34)
            make.width.equalTo(80)
        }
    }
    
    @objc func retryBtnClicked() {
        self.retryBtn?.isHidden=true
        self.indiacator?.startAnimating()
        self.delegate?.retry()
    }
    
    func loadError() {
        self.indiacator?.stopAnimating()
        self.retryBtn?.isHidden=false
    }
    
    func loading() {
        self.indiacator?.startAnimating()
    }
    
    func endLoading()  {
        self.indiacator?.stopAnimating()
        self.removeFromSuperview()
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
