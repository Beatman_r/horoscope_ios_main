//
//  MyFortuneViewController.swift
//  Horoscope
//
//  Created by Wang on 16/12/23.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
class todayFortuneViewmodel:eachCardViewmodel{
    
}
class MyFortuneViewController: BaseTableViewController {
    
    var viewModel:todayFortuneViewmodel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("My Fortune Today", comment: "")
        AnaliticsManager.sendEvent(AnaliticsManager.my_fortune_today_click)
        createTableHeaderAndSectionHeader()
        self.addRetryView()
        initViewmodel()
    // Do any additional setup after loading the view.
    }
    var imageHeaderView:partOneView?
    func createTableHeaderAndSectionHeader() {
        imageHeaderView = partOneView.init(frame: CGRect(x: 0, y: 0, width: HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth(), height: HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()/15*8))
        imageHeaderView?.backImageView.image = UIImage(named:ZodiacModel.getHoroscopeBackImgNameWithName(self.horoname ?? ""))
        self.tableView.tableHeaderView=imageHeaderView
    }

    func initViewmodel()  {
        viewModel = todayFortuneViewmodel.init(name: self.horoname ?? "",
                                           dataStyle: self.style ?? .today)
        viewModel?.loadDetail(self.style ?? .today, success: {
            self.rowCount = 1
            self.failedRetryView?.loadSuccess()
            self.tableView.reloadData()
            }, failure: { (error) in
            self.rowCount = 0
            self.failedRetryView?.loadFailed()
        })
    }
    
    //RetryViewDelegate
    override func retryToLoadData() {
        viewModel?.loadDetail(self.style ?? .today, success: {
            
            self.failedRetryView?.loadSuccess()
            self.rowCount = 1
            self.tableView.reloadData()
            }, failure: { (error) in
            self.rowCount = 0
            self.failedRetryView?.loadFailed()
        })
    }
    
    override func registerCell() {
        self.tableView.register(HoroDescriptionCell.self, forCellReuseIdentifier: "HoroDescriptionCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var rowCount:Int = 0
    override func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell:HoroDescriptionCell = tableView.dequeueReusableCell(withIdentifier: "HoroDescriptionCell", for: indexPath) as? HoroDescriptionCell {
            
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return  0
        //HoroDescriptionCell.calculateHeight(self.viewModel?.model?.content ?? "")
    }
    
    //MARK:Init
    var horoname:String?
    var style:HoroDetailType?
    init(name:String?,style:HoroDetailType) {
        super.init(style: .grouped)
        self.style=style
        self.horoname=name
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        // Custom initialization
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
