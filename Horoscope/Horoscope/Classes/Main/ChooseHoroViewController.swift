//
//  ChooseHoroViewController.swift
//  Horoscope
//
//  Created by Wang on 17/2/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

protocol ChooseHoroDelegate:class {
    func chooseCompleteToRefresh()
}

class ChooseHoroViewController: UIViewController,HsHUD,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var delegate:ChooseHoroDelegate?
    var verScan:CGFloat? = 1
    var horScan:CGFloat? = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        AnaliticsManager.sendEvent(AnaliticsManager.select_sign_show)
        self.view.layer.contents = UIImage(named: "meBgImg.jpg")?.cgImage
        verScan = AdaptiveUtils.screenHeight/667
        horScan = AdaptiveUtils.screenWidth/375
        createTitleLab()
        createDatePicker()
        createHoroIma()
        createOKBtnAndCancelBtn()
        createBottomLab()
        createAlertControl()
       // getHoroImaAndTextWithDateString(self.monthSelectedRow, row: self.daySelectedRow)
    }
    
    var alertController:UIAlertController?
    func createAlertControl() {
        alertController = UIAlertController.init(title: NSLocalizedString("Sure you want to cancel? Thus you won't be able to post, comment, or share in the forum, you will also lost the opportunity to meet with people that meant to be your friends.", comment: ""), message: NSLocalizedString("", comment: ""), preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("NO", comment: ""), style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: NSLocalizedString("YES", comment: ""), style: .default, handler: {
            action in
            self.chooseCancled()
        })
        alertController!.addAction(okAction)
        alertController!.addAction(cancelAction)
    }
    
    func createBottomLab()  {
        self.view.addSubview(bottomLab)
        self.bottomLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left).offset(24*horScan!)
            make.right.equalTo(self.view.snp.right).offset(-24*horScan!)
            make.top.equalTo(self.cancelBtn.snp.bottom).offset(8*verScan!)
           // make.bottom.equalTo(self.view.snp.bottom).offset(-8)
        }
    }
    
    let bottomLab:UILabel = {
       let lab = UILabel()
        lab.numberOfLines = 0
        lab.textColor = UIColor.disableCancelGrey()
        lab.font = HSFont.baseLightFont(12)
        lab.text = NSLocalizedString("please note:by so doing,your sign will be highlighted when involve in the foru,you can involved in a specific  horoscope friend circle to interact with people that fit for you! We won't divulge your iformation to any third parties.", comment: "")
       return lab
    }()
    
    func createTitleLab() {
        self.view.addSubview(introLab)
        if UIScreen.main.bounds.size.width == 320 {
           introLab.font = HSFont.baseRegularFont(20)
        }
        self.introLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left).offset(25*horScan!)
            make.right.equalTo(self.view.snp.right).offset(-25*horScan!)
            make.top.equalTo(self.view.snp.top).offset(74*verScan!)
        }
    }
    
    let horoIma:UIImageView = {
        let ima = UIImageView()
        return ima
    }()
    
    let horoNameLab:UILabel = {
        let lab = UILabel()
        lab.textAlignment = .center
        lab.textColor = UIColor.commonPinkColor()
        lab.font = HSFont.baseRegularFont(20)
        return lab
    }()
    
    func createHoroIma()  {
        self.view.addSubview(horoIma)
        horoIma.snp.makeConstraints { (make) in
            make.top.equalTo(self.datePickerview!.snp.bottom).offset(10*verScan!)
            make.width.height.equalTo(45*verScan!)
            make.centerX.equalTo(self.view.snp.centerX).offset(-45)
        }
        
        self.view.addSubview(horoNameLab)
        horoNameLab.snp.makeConstraints { (make) in
            make.centerY.equalTo(horoIma.snp.centerY)
            make.left.equalTo(horoIma.snp.right).offset(8)
        }
    }
    
    func createOKBtnAndCancelBtn() {
         okButton.addTarget(self, action: #selector(chooseHoroComplete), for: .touchUpInside)
         self.view.addSubview(okButton)
         okButton.layer.masksToBounds=true
         okButton.layer.cornerRadius=(46*verScan!)/2
         okButton.snp.makeConstraints { (make) in
            make.width.equalTo(277)
            make.height.equalTo(46*verScan!)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(horoIma.snp.bottom).offset(19*verScan!)
        }
        
        cancelBtn.addTarget(self, action: #selector(presentAlerControl), for: .touchUpInside)
        self.view.addSubview(cancelBtn)
        cancelBtn.snp.makeConstraints { (make) in
            make.width.equalTo(277)
            make.height.equalTo(46)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(okButton.snp.bottom).offset(10*verScan!)
        }
    }
    
    @objc func presentAlerControl() {
        self.present(alertController!, animated: true, completion: nil)
    }
    
    
    func getHoroImaAndTextWithDateString(_ section:Int,row:Int) {
        let paramNum = String((section+1)*100+(row+1))
        let horoNameList=DataManager.sharedInstance.horoInfo.getZodiacnameByDate(paramNum)
        
        horoIma.image=UIImage(named: "\(horoNameList.last ?? "")")
        horoNameLab.text = "\(NSLocalizedString(horoNameList.first?.capitalized ?? "", comment: ""))"
   }
    
    var datePickerview:UIPickerView?
    let screenWidth=UIScreen.main.bounds.size.width
    var datePickWidth:CGFloat?
    func createDatePicker() {
        datePickerview=UIPickerView()
        datePickWidth=screenWidth
        self.view.addSubview(datePickerview!)
        datePickerview?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.introLab.snp.bottom).offset(12*verScan!)
            make.height.equalTo(230*verScan!)
            make.width.equalTo(169)
            make.centerX.equalTo(self.view.snp.centerX)
        })
        datePickerview?.delegate=self
        datePickerview?.dataSource = self
        for index in 0..<31 {
            dayArr.append(index+1)
        }
      datePickerview?.selectCurrentZodiacMonth()
      refreshHoroImgAndText()
     
    }
    
    func refreshHoroImgAndText()
    {
        if let selectRow = datePickerview?.selectedRow(inComponent: 0) {
            getHoroImaAndTextWithDateString(selectRow+1, row: 0)
        }
    }
    
    func getDayArrLines(_ section:Int) -> Int {
        let sec = section + 1
        if sec == 2{
            return 29
        }else if sec == 4 || sec == 6 || sec == 9 || sec == 11 {
            return 30
        }
        return 31
    }
    
    func freshDayArr(_ section:Int)  {
        dayArr.removeAll()
        var defaultDay = 29
        let sec = section + 1
        if sec == 2{
            defaultDay = 29
        }else if sec == 4 || sec == 6 || sec == 9 || sec == 11 {
            defaultDay = 30
        }else{
            defaultDay=31
        }
        for index in 0..<defaultDay {
            dayArr.append(index+1)
        }
        if daySelectedRow > (dayArr.count-1){
            daySelectedRow = dayArr.count-1
        }
    }
    
    var monthSelectedRow:Int = 0
    var daySelectedRow:Int = 0
    
    //MARK:UIPickerViewDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
   
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            return self.monthArr.count
        }
        if component == 1{
            return self.dayArr.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat
    {
        if component == 0{
            return 115.0
        }else {
            return 44.0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 33.0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if 0 == component{
            let month = self.monthArr[row]
            return month
        }else if 1 == component{
            let dict = self.dayArr[row]
            return String(dict)
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int,
                    forComponent component: Int, reusing view: UIView?) -> UIView{
        let topLine = pickerView.subviews[1]
        let bottomLine = pickerView.subviews[2]
        topLine.backgroundColor = UIColor.purpleContentColor()
        bottomLine.backgroundColor = UIColor.purpleContentColor()
        if component == 0{
            let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 115, height: 33.0))
            label.textAlignment = .left
            if row == monthSelectedRow{
                label.font=HSFont.baseRegularFont(22)
                label.textColor = UIColor.commonPinkColor()
            }else{
                label.font = HSFont.baseRegularFont(21)
                label.textColor = UIColor.purpleContentColor()
            }
            let dict = self.monthArr[row]
            label.text = dict
            return label
        }else if component == 1{
            let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 44, height: 33.0))
            label.textAlignment = .right
            if row == daySelectedRow{
                label.font=HSFont.baseRegularFont(22)
                label.textColor = UIColor.commonPinkColor()
            }else{
                label.font = HSFont.baseRegularFont(21)
                label.textColor = UIColor.purpleContentColor()
            }
            let dict = self.dayArr[row]
            label.text = String(dict)
            return label
        }
        return UIView()
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString?
    {
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0{
            monthSelectedRow = row
            self.freshDayArr(row)
        }
        if component == 1{
            daySelectedRow = row
        }
        self.getHoroImaAndTextWithDateString(monthSelectedRow, row: daySelectedRow)
        self.datePickerview?.reloadAllComponents()
    }
    
    func dateChange() {
        
    }
    
    var monthArr:[String] = [NSLocalizedString("January", comment: ""),
                             NSLocalizedString("February", comment: ""),
                             NSLocalizedString("March", comment: ""),
                             NSLocalizedString("April", comment: ""),
                             NSLocalizedString("May", comment: ""),
                             NSLocalizedString("June", comment: ""),
                             NSLocalizedString("July", comment: ""),
                             NSLocalizedString("August", comment: ""),
                             NSLocalizedString("September", comment: ""),
                             NSLocalizedString("October", comment: ""),
                             NSLocalizedString("November", comment: ""),
                             NSLocalizedString("December", comment: "")]
    var dayArr:[Int] = []
    let introLab:UILabel = {
        let lab = UILabel()
        lab.textColor=UIColor.purpleContentColor()
        lab.textAlignment = .center
        lab.font = HSFont.baseRegularFont(24)
        lab.numberOfLines = 0
        lab.text = NSLocalizedString("Enhance accuracy of your sign by entering your D.O.B.", comment: "")
        return lab
    }()
    
    let okButton:UIButton = {
        let btn = UIButton(type:.custom)
        
        btn.setTitle(NSLocalizedString("OK", comment: ""), for: UIControlState())
        btn.titleLabel?.font = HSFont.baseRegularFont(24)
        btn.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        btn.layer.borderColor = UIColor.commonPinkColor().cgColor
         btn.layer.borderWidth = 1
       
        return btn
    }()
    
    let cancelBtn:UIButton = {
        let btn = UIButton(type:.custom)
        btn.setTitle(NSLocalizedString("Cancel", comment: ""), for: UIControlState())
        btn.titleLabel?.font = HSFont.baseRegularFont(18)
        btn.setTitleColor(UIColor.disableCancelGrey(), for: UIControlState())
        return btn
    }()
    
    fileprivate var formatter:DateFormatter? = DateFormatter()
    @objc func chooseHoroComplete() {
        self.showProccessHUD()
        let birthdayString = String((monthSelectedRow+1)*100+(daySelectedRow+1))
        let horoNameList=DataManager.sharedInstance.horoInfo.getZodiacnameByDate(birthdayString)
        let horoName=horoNameList.first
        AnaliticsManager.sendEvent(AnaliticsManager.select_sign_click, data: ["name":"\(horoName ?? "")"])
        let user = AccountManager.sharedInstance.getUserInfo()
        user?.horoname = horoName
        AccountManager.sharedInstance.setUserInfo(user)
        AccountManager.sharedInstance.loginConfirm(user!.token ?? "", birthday: birthdayString , horo: horoName!, success: {
            self.hideHUD()
            if let navController = HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController {
                if let topVC = navController.topViewController {
                    topVC.navigationController?.popToRootViewController(animated: false)
                }}
            self.dismiss(animated: true, completion: { 
            })
        }) {
           AccountManager.sharedInstance.logout({
                self.hideHUD()
                self.delegate?.chooseCompleteToRefresh()
                self.dismiss(animated: true, completion: {
                })
                }, failure: {
                self.hideHUD()
                self.delegate?.chooseCompleteToRefresh()
                self.dismiss(animated: true, completion: {
                 })
            })
        }
    }
    
    func chooseCancled() {
        self.toast(NSLocalizedString("Log in failed! You haven't entered your date of birth!", comment: ""))
            AccountManager.sharedInstance.logout({
            self.delegate?.chooseCompleteToRefresh()
            self.dismiss(animated: true) {
            }
            }, failure: {
                
        })
        self.delegate?.chooseCompleteToRefresh()
        self.dismiss(animated: true) {
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


 extension UIPickerView {
    fileprivate func  selectCurrentZodiacMonth(){
       let alreadyChooseZodiacData = UserDefaults.standard.value(forKey: ChooseZodiacArray) as? Data
      if let zodiacData =  alreadyChooseZodiacData ,
         let dataRowIndexPath = NSKeyedUnarchiver.unarchiveObject(with: zodiacData) as? [IndexPath],
         let indexPath = dataRowIndexPath.first {
        let zodiacName = ZodiacModel.getZodiacName(indexPath.row)
        let currentMonth = ZodiacModel.getZodicMonthWithName(zodiacName)
        let startDay = Int((ZodiacModel.getZodiacPeriod(indexPath.row).components(separatedBy: "-").first?.components(separatedBy: "/").last)!)
        self.selectRow(currentMonth, inComponent: 0, animated: false)
        self.selectRow(startDay! - 1, inComponent: 1, animated: false)
        }
    }
}
