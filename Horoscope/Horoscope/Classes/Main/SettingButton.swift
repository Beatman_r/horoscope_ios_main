//
//  SettingButton.swift
//  Horoscope
//
//  Created by Wang on 16/12/5.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class SettingButton: UIButton {
    class func create(_ viewController:UIViewController?) ->SettingButton{
        let button = SettingButton(type:.custom)
        button.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        button.rootVc=viewController
        button.setImage(UIImage(named: "hs_setting_"), for: UIControlState())
        button.addTarget(viewController, action: #selector(self.click), for: .touchUpInside)
        return button
    }
    var rootVc:UIViewController?
    @objc func click() {
        let vc = SettingViewController()
        self.rootVc?.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
