//
//  OtherUserPostListViewModel.swift
//  Horoscope
//
//  Created by Beatman on 17/2/23.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
protocol OtherUserPostListViewModelDelegate : class {
    func ShowFlash()
}
class OtherUserPostListViewModel: NSObject {
    fileprivate var otherUserPostList : [MyPostModel] = []
    
    
    weak var delegate:OtherUserPostListViewModelDelegate?
    var count : Int {
        return self.otherUserPostList.count
    }
    
    func getModel(_ row:Int) -> MyPostModel {
        return self.otherUserPostList[row]
    }
    
    fileprivate let dao = HoroscopeDAO()
    func loadOtherUserPostList(_ userId:String,offset:Int,success:@escaping ()->(),failed:@escaping ()->()) {
        dao.getOtherUserPostList(userId, offset: offset, success: { (postList) in
            if postList.count == 0 {
                self.delegate?.ShowFlash()
            }else {
                self.otherUserPostList = offset == 0 ? postList : self.otherUserPostList + postList
            }
            success()
            }) { (failure) in
                failed()
        }
    }
}
