//
//  ZodiacCellModel.swift
//  Horoscope
//
//  Created by Beatman on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class ZodiacCellModel: NSObject {
    var imageName : String?
    var zodiacName : String?
    var periodString : String?
}
