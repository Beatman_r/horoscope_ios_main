//
//  ShareOperationDao.swift
//  bibleverse
//
//  Created by 麦子 on 16/9/12.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import FBSDKShareKit

class ShareDao: NSObject,FBSDKSharingDelegate {
    static let shareInstance = ShareDao()
    weak var fromeVC:UIViewController?
    let shareLink:FBSDKShareDialog = FBSDKShareDialog()
    
    func faceBookLink(_ fromVC:UIViewController, url:String) {
        self.fromeVC = fromVC
        let myContent:FBSDKShareLinkContent = FBSDKShareLinkContent()
        myContent.contentURL = URL(string: url)
        shareLink.delegate = self
        shareLink.shareContent = myContent
        shareLink.fromViewController = fromVC
        shareLink.show()
    }
    
    func faceBookIma(_ fromVC:UIViewController, image:UIImage) {
        self.fromeVC = fromVC
        let photo = FBSDKSharePhoto()
        photo.image = image
        photo.isUserGenerated = true
        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        let shareDialog:FBSDKShareDialog = FBSDKShareDialog()
        shareDialog.shareContent = content
        shareDialog.delegate = self
        shareDialog.fromViewController = fromVC
        shareDialog.show()
    }
    
    func localLink(_ fromVC:UIViewController, url:String) {
        self.fromeVC = fromVC
        DispatchQueue.main.async(execute: {
            let activityVc = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            activityVc.completionWithItemsHandler = { (str, bool, arr, error) in
                //let vc = MarkResultController()
                //vc.pageType = .Share
                //self.fromeVC?.navigationController?.pushViewController(vc, animated: true)
            }
            fromVC.present(activityVc, animated: true, completion: nil)
        })
    }
    
    func localIma(_ fromVC:UIViewController, image:UIImage) {
        self.fromeVC = fromVC
        DispatchQueue.main.async(execute: {
            let activityVc = UIActivityViewController(activityItems: [image], applicationActivities: nil)
            fromVC.present(activityVc, animated: true, completion: nil)
        })
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        //let vc = MarkResultController()
        //vc.pageType = .Share
        //self.fromeVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
}
