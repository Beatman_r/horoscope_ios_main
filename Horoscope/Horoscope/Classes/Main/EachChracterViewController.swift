//
//  EachChracterViewController.swift
//  Horoscope
//
//  Created by Wang on 16/12/23.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class EachChracterViewController: BaseTableViewController {
    var charaModel : CharacteristicsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("My Zodiac Characteristics", comment: "")
        self.charaModel = Characteristics.getCharacteristicsWithName("\(self.name ?? "")")
        AnaliticsManager.sendEvent(AnaliticsManager.my_zodiac_today_click)
    }
    
    override func registerCell() {
        self.tableView?.register(CharaTableViewCell.self, forCellReuseIdentifier: "tableViewCell")
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as! CharaTableViewCell
        if indexPath.row == 0 {
            cell.renderCell("Positive", model: self.charaModel)
        }else if indexPath.row == 1{
            cell.renderCell("Negative", model: self.charaModel)
        }else if indexPath.row == 2{
            cell.renderCell("Description", model: self.charaModel)
        }
        if self.charaModel?.positive == "" && self.charaModel?.negative == "" {
            cell.describeLabel.text = "Description"
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
 
        switch indexPath.row {
        case 0:
            if (self.charaModel?.positive ?? "") == "" {
                return 0
            }else{
                return  CharaTableViewCell.calculateHeight(self.charaModel?.positive ?? "")+16
            }
            
        case 1:
            if (self.charaModel?.negative ?? "") == "" {
                return 0
            }else{
                return CharaTableViewCell.calculateHeight(self.charaModel?.negative ?? "")+16
            }
            
        case 2:
            if (self.charaModel?.descriptions ?? "") == "" {
                return 0
            }else{
                return CharaTableViewCell.calculateHeight(self.charaModel?.descriptions ?? "")+16
            }
        default :
            return 0
        }
    }
    
    // MARK: tableView
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    var name:String?
    class func create(_ horoName:String) ->EachChracterViewController{
        let vc = EachChracterViewController()
        vc.name = horoName
        return vc
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
