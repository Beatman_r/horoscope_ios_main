//
//  BaseTableViewController.swift
//  Horoscope
//
//  Created by Wang on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import MJRefresh
 


class BaseTableViewController: UITableViewController,RetryViewDelegate {
    
    var dataArr:[AnyObject]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.registerCell()
        self.initFootView()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.loadData()
    }
    
    var failedRetryView:RetryView?
    func addRetryView() {
        failedRetryView=RetryView.init(frame: self.view.bounds)
        failedRetryView?.delegate=self
        self.view.addSubview(failedRetryView!)
    }
    
    func retryToLoadData() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func registerCell() {
        
    }
    
    func loadData() {
        
    }
 
    fileprivate func initFootView(){
        let footView = UIView()
        footView.frame = CGRect(x: 0, y: 0, width: 0, height: 8)
        footView.backgroundColor=UIColor.clear
        self.tableView.tableFooterView = footView
    }
    
    func setupRefreshHeaderView() {
        let header = MJRefreshNormalHeader()
        header.stateLabel.textColor = UIColor.white
        header.setTitle(NSLocalizedString("Pull up to load", comment: ""), for: .idle)
        header.setTitle(NSLocalizedString("Release to refresh", comment: ""), for: .pulling)
        header.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        header.lastUpdatedTimeLabel.isHidden = true
        self.tableView.mj_header = header
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArr?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
