//
//  MainDailyScrollController.swift
//  Horoscope
//
//  Created by Beatman on 17/1/6.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import MJRefresh
import pop

protocol mainScrollerDelegate : class {
    func didScrollToItem(_ index:Int)
}

var gIsgettingNotifStatus = false

class MainDailyScrollController: UIViewController,UIScrollViewDelegate,GuideHoroscopeViewControllerDelegate,HomeTimeLineDelegate,GuideNotificationViewDelegate, AKPickerViewDataSource, AKPickerViewDelegate {
    
    weak var delegate : mainScrollerDelegate?
    var zodiacNameList : [ZodiacCellModel] = []
    var contentViews : [UITableView] = []
    
    let toolBarBackView = UIView()
    var pageControl : UIPageControl!
    var scrollView : UIScrollView!
    var zodiacNameLabel : UILabel!
    var pickerView: AKPickerView!
    
    var backAnimateView : BaseZodiacAnimateView!
    var backViewList : [BaseZodiacAnimateView] = []
    
    var mainContentView = UIView()
    var dataVC : HomeTimeLineViewController!
    var roundMenu : RoundMenuView?
    fileprivate var timer:Timer?
    var triggerButtonn:UIButton?
    var isTrigerShowing = false
    var wheatherToShowTrigger = true
    let triggerView = UIView()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let noti = NotificationCenter.default
        noti.addObserver(self, selector: #selector(changeColor(_:)), name: NSNotification.Name(rawValue: noti_didChangeColor), object: nil)
        noti.addObserver(self, selector: #selector(refreshViewCount(_:)), name: NSNotification.Name(rawValue: ZodiacDidChange), object: nil)
        
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.scrollView = UIScrollView()
        self.mainContentView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        let firstStart = NotificationLimitManager.sharedInstance.firstStartUpApp()
        if firstStart == true {
            let guideVC = GuideHoroscopeViewController()
            guideVC.delegate = self
            self.present(guideVC, animated: true, completion: {
            })
        }else {
            self.createMainScrollView()
            self.createToolBar()
        }
        
        noti.addObserver(self, selector: #selector(shutDownRateUs), name: NSNotification.Name(rawValue: "rateButtonOnClick"), object: nil)
        noti.addObserver(self, selector: #selector(showRateUsView), name: NSNotification.Name(rawValue: "forcastShowRateUsView"), object: nil)
        noti.addObserver(self, selector: #selector(showRateUsView), name: NSNotification.Name(rawValue: "matchShowRateUsView"), object: nil)
        noti.addObserver(self, selector: #selector(showGuideNotificationView), name: NSNotification.Name(rawValue: ShowGuideNotification), object: nil)
        noti.addObserver(self, selector: #selector(appStart), name: NSNotification.Name(rawValue: "AppDelegate.StartApp"), object: nil)
        self.requestNotificationNum()
    }
    
    let numLab:UILabel = {
        let lab = UILabel()
        lab.backgroundColor=UIColor.notiRed()
        lab.textColor = UIColor.white
        lab.layer.masksToBounds = true
        lab.layer.cornerRadius = 4
        lab.text = " 11 "
        lab.isHidden = true
        lab.font=HSFont.baseRegularFont(11)
        return lab
    }()
    
    func requestNotificationNum() {
        HoroscopeDAO.sharedInstance.getUnreadNumber {[weak self](num) in
            if num == 0{
                self?.numLab.isHidden=true
            }else{
                self?.numLab.isHidden=false
                self?.numLab.text = " \(num) "
            }
        }
    }
    
    func buttonAnimation() {
        let buttonSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerPositionY)
        let buttonCenterY : CGFloat = self.triggerButtonn!.center.y
        buttonSpringAnimation?.fromValue = buttonCenterY - 5
        buttonSpringAnimation?.toValue = buttonCenterY
        buttonSpringAnimation?.springSpeed = 20
        buttonSpringAnimation?.springBounciness = 30
        if self.triggerButtonn != nil {
            self.triggerButtonn!.pop_add(buttonSpringAnimation, forKey: "buttonSpringAnimation")
        }
    }
    
    func showGuideHoroscopeView() {
        if NotificationLimitManager.sharedInstance.showGuideOptionZodiac() == true{
            let guideVC = GuideHoroscopeViewController()
            guideVC.delegate = self
            self.present(guideVC, animated: true, completion: {
            })
        }else{
            return
        }
    }
    @objc func appStart() {
        if gIsgettingNotifStatus == false {
            NotificationLimitManager.sharedInstance.registerLocalNotification()
            gIsgettingNotifStatus = true
        }else{
            return
        }
    }
    
    @objc func changeColor(_ noti:Notification) {
        let offY = (noti.userInfo?["offsetY"] as AnyObject).floatValue
        let kColor = offY!/90.0
        self.toolBarBackView.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: CGFloat(kColor))
    }
    
    @objc func showGuideNotificationView() {
        let guideView = GuideNotificationView.init(frame: self.view.bounds)
        self.view.addSubview(guideView)
        guideView.delegate = self
    }
    @objc func showRateUsView() {
        let rateUsView:RateUsFullView = RateUsFullView.init(frame: UIScreen.main.bounds)
        rateUsView.rootvc = self
        self.view.addSubview(rateUsView)
        var showCount:Int = UserDefaults.standard.integer(forKey: "rateUsShowCount")
        showCount += 1
        UserDefaults.standard.set(showCount, forKey: "rateUsShowCount")
        UserDefaults.standard.synchronize()
    }
    
    @objc func shutDownRateUs(){
        UserDefaults.standard.set(true, forKey: "hasRate")
        UserDefaults.standard.synchronize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    //MARK: refresh
    @objc func refreshViewCount(_ noti:Notification) {
        
        if self.dataVC != nil {
            self.dataVC.removeFromParentViewController()
        }
        self.toolBarBackView.removeFromSuperview()
        self.tempBgImage.removeFromSuperview()
        if (self.backAnimateView != nil) {
            self.backAnimateView.stopAnimate()
        }
        for views in self.mainContentView.subviews {
            views.removeFromSuperview()
        }
        for views in self.view.subviews {
            views.removeFromSuperview()
        }
        for view in self.toolBarBackView.subviews {
            view.removeFromSuperview()
        }
        self.zodiacNameList = ZodiacRecordManager.getSelectedZodiacItems()
        self.createMainScrollView()
        self.createToolBar()
        if self.currentPage < self.zodiacNameList.count {
           self.scroll(self.currentPage, animated: false)
        }
    }
    
    @objc func pageChanged(_ sender:UIPageControl) {
        var frame = self.scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(sender.currentPage)
        frame.origin.y = 64
        scrollView.scrollRectToVisible(frame, animated:true)
    }
    
    func scroll(_ pageIndx: Int, animated: Bool) -> Void {
        guard pageIndx <  self.zodiacNameList.count else {
            return
        }
                
        self.toolBarBackView.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: 0)
        backAnimateView.stopAnimate()
        dataVC.view.removeFromSuperview()
        for subViews in self.mainContentView.subviews {
            subViews.removeFromSuperview()
        }
        dataVC.view.removeFromSuperview()
        let path = Bundle.main.path(forResource: "\(self.zodiacNameList[pageIndx].zodiacName.lowercased())_bg", ofType: "jpg")
        tempBgImage.image = UIImage(contentsOfFile: path ?? "")
        self.pickerView.selectItem(UInt(pageIndx), animated: false, notifySelection: false)
        self.mainContentView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        
        dataVC = HomeTimeLineViewController.create(self.zodiacNameList[pageIndx].zodiacName.lowercased())
        dataVC.delegate = self
        self.addChildViewController(dataVC)
        dataVC.view.frame = CGRect(x: CGFloat(pageIndx) * SCREEN_WIDTH, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        dataVC.view.layer.contents = UIColor.clear.cgColor
        dataVC.loadData()
        
        let viewName = self.zodiacNameList[pageIndx].zodiacName.capitalized
        self.backAnimateView = self.addAnimateView(viewName + "AnimateView")
        self.backAnimateView.frame = CGRect(x: CGFloat(pageIndx) * SCREEN_WIDTH, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT+20)
        self.mainContentView.addSubview(self.backAnimateView)
        self.scrollView.addSubview(dataVC.view)
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            self.backAnimateView.startAnimation()
        })
        self.delegate?.didScrollToItem(pageIndx)
        AnaliticsManager.sendEvent(AnaliticsManager.main_show, data: ["signs":"\(viewName)"])
        dataVC.needRefresh1 = true
        dataVC.needRefresh2 = true
        dataVC.needRefresh3 = true
        CURRENT_MAIN_SIGN = viewName.lowercased()
        self.zodiacNameLabel.text = NSLocalizedString("\(self.zodiacNameList[pageIndx].periodString.replacingOccurrences(of: "-", with: " - "))", comment: "")
        self.currentPage = pageIndx
        var offset = self.scrollView.contentOffset
        offset.x = self.scrollView.frame.size.width * CGFloat(pageIndx)
        self.scrollView.setContentOffset(offset, animated: animated)
    }
    
    @objc func addZodiacButtonOnClick() {
        if self.zodiacNameList.count == 0 {
            let vc = AddHoroscopeViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            AnaliticsManager.sendEvent(AnaliticsManager.main_add_click)
            let vc = HorocopeListViewController()
            self.backAnimateView.stopAnimate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        for vc in self.vcList {
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
        }
        for animateView in self.backViewList {
            animateView.removeFromSuperview()
        }
        self.vcList.removeAll()
        self.backViewList.removeAll()
    }
    
    @objc func settingButtonOnClick() {
        AnaliticsManager.sendEvent(AnaliticsManager.main_notification_click)
        let vc = MeViewController()
        self.numLab.isHidden = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func commentButtonOnClick() {
        let vc = CommentViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var userIma:UIImageView = UIImageView()
    func createToolBar() {
        toolBarBackView.backgroundColor = UIColor.clear
        toolBarBackView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 115)
        self.view.addSubview(toolBarBackView)
        
        let addZodiacButton = UIButton()
        addZodiacButton.setImage(UIImage(named: "add_"), for: UIControlState())
        addZodiacButton.addTarget(self, action: #selector(addZodiacButtonOnClick), for: .touchUpInside)
        toolBarBackView.addSubview(addZodiacButton)
        
        zodiacNameLabel = UILabel()
        zodiacNameLabel.text = NSLocalizedString("\(self.zodiacNameList.first?.periodString.replacingOccurrences(of: "-", with: " - ") ?? "")", comment: "")
        zodiacNameLabel.textColor = UIColor.white
        zodiacNameLabel.font = HSFont.baseMediumFont(14)
        zodiacNameLabel.textAlignment = NSTextAlignment.center
        toolBarBackView.addSubview(zodiacNameLabel)
        
        pickerView = AKPickerView(frame: CGRect(x: 0.0, y: 55.0, width: SCREEN_WIDTH, height: 37.0))
        pickerView.dataSource = self
        pickerView.font = HSFont.baseMediumFont(18.0)
        pickerView.highlightedFont = HSFont.baseMediumFont(26.0)
        pickerView.textColor = UIColor.white.withAlphaComponent(0.2)
        pickerView.highlightedTextColor = UIColor.white
        pickerView.isMaskDisabled = true
        pickerView.pickerViewStyle = .styleFlat
        pickerView.fisheyeFactor = 0.001
        pickerView.interitemSpacing = 12.5 * (SCREEN_WIDTH / 375.0)
        toolBarBackView.addSubview(pickerView)
        
        let postButton = UIButton()
        postButton.isHidden = false
        postButton.setImage(UIImage(named: "toolBar_post"), for: .normal)
        postButton.addTarget(self, action: #selector(postButtonOnClick), for: .touchUpInside)
        toolBarBackView.addSubview(postButton)
        
        let settingButton = UIButton()
        settingButton.setImage(UIImage(named: "mainUserCover"), for: UIControlState())
        settingButton.addTarget(self, action: #selector(settingButtonOnClick), for: .touchUpInside)
        toolBarBackView.addSubview(settingButton)
        
        let user = AccountManager.sharedInstance.getUserInfo()?.avatar
        //"http://img.idailybread.com/horoscope/bread/author_avatar/astral_masters.jpg"
        userIma.sd_setImage(with: URL(string:user ?? ""), placeholderImage: UIImage(named:""))
        userIma.layer.cornerRadius = 14
        userIma.layer.masksToBounds = true
        
        toolBarBackView.addSubview(userIma)
        toolBarBackView.addSubview(numLab)
        toolBarBackView.addSubview(triggerView)
        self.triggerView.backgroundColor = UIColor.clear
        self.triggerView.isHidden = true
        
        numLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.userIma.snp.centerX).offset(4)
            make.top.equalTo(self.userIma.snp.top).offset(-4)
        }
        addZodiacButton.snp.makeConstraints { (make) in
            make.left.equalTo(toolBarBackView.snp.left).offset(15)
            make.top.equalTo(toolBarBackView).offset(27.0)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        
        pickerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(55.0)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(37.0)
        }
        
        zodiacNameLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(toolBarBackView.snp.centerX)
            make.top.equalTo(pickerView.snp.bottom).offset(-4.0)
            make.bottom.equalToSuperview().offset(-8.0)
        }

        settingButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.view.snp.right).offset(-15)
            make.top.equalTo(addZodiacButton)
            make.width.equalTo(31)
            make.height.equalTo(31)
        }
        
        userIma.snp.makeConstraints { (make) in
            make.right.equalTo(settingButton.snp.right).offset(-1)
            make.centerY.equalTo(settingButton)
            make.left.equalTo(settingButton.snp.left).offset(1)
            make.height.equalTo(29)
        }
        
        postButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(settingButton)
            make.right.equalTo(settingButton.snp.left).offset(-15)
            make.width.equalTo(28)
            make.height.equalTo(28)
        }
        
        self.triggerView.snp.makeConstraints { (make) in
            make.centerY.equalTo(userIma.snp.centerY)
            make.right.equalTo(userIma.snp.left).offset(10)
            make.height.equalTo(60)
            make.width.equalTo(60)
        }
        
        if self.zodiacNameList.count > 0 {
            pickerView.reloadData()
            pickerView.selectItem(0, animated: false)
        }
        pickerView.delegate = self
    }
    
    @objc func postButtonOnClick() {
        AnaliticsManager.sendEvent(AnaliticsManager.main_post_click, data: ["signs":"\(CURRENT_MAIN_SIGN?.lowercased() ?? "")"])
        let notiName = "didClickPost"
        let notiCenter = NotificationCenter.default
        notiCenter.post(name: Notification.Name(rawValue: notiName), object: nil)
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        let userDefault = UserDefaults.standard
        userDefault.set(false, forKey: "shouldShowTipImg")
        userDefault.synchronize()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = PostViewController()
            vc.isReply = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    var vcList : [HomeTimeLineViewController] = []
    
    let tempBgImage = UIImageView()
    //MARK: Create
    func createMainScrollView() {
        self.zodiacNameList = ZodiacRecordManager.getSelectedZodiacItems()
        self.automaticallyAdjustsScrollViewInsets = true
        scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        scrollView.contentSize = CGSize(width: (CGFloat(self.zodiacNameList.count) * SCREEN_WIDTH), height: 0)
        scrollView.bounces = false
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        
        if self.zodiacNameList.count>0{
            let path = Bundle.main.path(forResource: "\(self.zodiacNameList[0].zodiacName.lowercased())_bg", ofType: "jpg")
            tempBgImage.image = UIImage(contentsOfFile: path ?? "")
            tempBgImage.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            self.view.addSubview(tempBgImage)
            
            dataVC = HomeTimeLineViewController.create(self.zodiacNameList[0].zodiacName.lowercased())
            dataVC.delegate = self
            self.addChildViewController(dataVC)
            dataVC.view.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
            dataVC.view.layer.contents = UIColor.clear.cgColor
            
            dataVC.loadData()
            
            let viewName = self.zodiacNameList[0].zodiacName.capitalized
            self.backAnimateView = self.addAnimateView(viewName + "AnimateView")
            self.backAnimateView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT+20)
            self.mainContentView.addSubview(self.backAnimateView)
            self.mainContentView.addSubview(dataVC.view)
            let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                self.backAnimateView.startAnimation()
            })
            scrollView.addSubview(self.mainContentView)
        }else {
            AnaliticsManager.sendEvent(AnaliticsManager.main_show, data: ["signs":"empty"])
            let vc = EmptyHoroTimelineViewController.create("")
            vc.loadData()
            if vc.isKind(of: BaseViewController.self) {
                self.addChildViewController(vc)
                vc.view.frame = CGRect(x: CGFloat(0) * SCREEN_WIDTH, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                vc.view.layer.contents = UIColor.clear.cgColor
                self.scrollView.layer.contents = UIImage(named: "balckground.jpg")?.cgImage
                scrollView.addSubview(vc.view)
            }
        }
        self.view.addSubview(scrollView)
        CURRENT_MAIN_SIGN = self.zodiacNameList.first?.zodiacName.lowercased()
    }
    
    
    func addAnimateView(_ viewName: String) ->BaseZodiacAnimateView{
        let nameSpace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String
        let className : AnyClass = NSClassFromString(nameSpace + "." + viewName)!
        let viewClass = className as! BaseZodiacAnimateView.Type
        let view = viewClass.init()
        return view
    }
    
    var currentPage : Int = 0
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.currentPage = Int(self.pickerView.selectedItem)
        if scrollView != self.scrollView {
            UIView.animate(withDuration: 0.25) {
                self.zodiacNameLabel.alpha = 0.0
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView != self.scrollView && decelerate == false {
            UIView.animate(withDuration: 0.4) {
                self.zodiacNameLabel.alpha = 1.0
            }
        }
    }
    
    func getCurrentContentOffsetY(_ offsetY: CGFloat) {
        let kColor = offsetY/90.0
        self.toolBarBackView.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: CGFloat(kColor))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView != self.scrollView {
            UIView.animate(withDuration: 0.4) {
                self.zodiacNameLabel.alpha = 1.0
            }
        }

        let viewPage = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        //start from 0
        if self.zodiacNameList.count > 1 {
            if viewPage != self.currentPage {
                self.toolBarBackView.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: 0)
                backAnimateView.stopAnimate()
                dataVC.view.removeFromSuperview()
                for subViews in self.mainContentView.subviews {
                    subViews.removeFromSuperview()
                }
                dataVC.view.removeFromSuperview()
                let path = Bundle.main.path(forResource: "\(self.zodiacNameList[viewPage].zodiacName.lowercased())_bg", ofType: "jpg")
                tempBgImage.image = UIImage(contentsOfFile: path ?? "")
                self.pickerView.selectItem(UInt(viewPage), animated: true, notifySelection: false)
                self.mainContentView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                
                dataVC = HomeTimeLineViewController.create(self.zodiacNameList[viewPage].zodiacName.lowercased())
                dataVC.delegate = self
                self.addChildViewController(dataVC)
                dataVC.view.frame = CGRect(x: CGFloat(viewPage) * SCREEN_WIDTH, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                dataVC.view.layer.contents = UIColor.clear.cgColor
                dataVC.loadData()
                
                let viewName = self.zodiacNameList[viewPage].zodiacName.capitalized
                self.backAnimateView = self.addAnimateView(viewName + "AnimateView")
                self.backAnimateView.frame = CGRect(x: CGFloat(viewPage) * SCREEN_WIDTH, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT+20)
                self.mainContentView.addSubview(self.backAnimateView)
                self.scrollView.addSubview(dataVC.view)
                let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                    self.backAnimateView.startAnimation()
                })
                self.delegate?.didScrollToItem(viewPage)
                AnaliticsManager.sendEvent(AnaliticsManager.main_show, data: ["signs":"\(viewName)"])
                dataVC.needRefresh1 = true
                dataVC.needRefresh2 = true
                dataVC.needRefresh3 = true
                CURRENT_MAIN_SIGN = viewName.lowercased()
            }
            self.zodiacNameLabel.text = NSLocalizedString("\(self.zodiacNameList[viewPage].periodString.replacingOccurrences(of: "-", with: " - "))", comment: "")
        }
    }
    
    //MARK:- GuideHoroscopeViewControllerDelegate
    func goOnBtnClick() {
        if NotificationLimitManager.sharedInstance.firstStartUpApp() == false{
            return
        }
        NotificationLimitManager.sharedInstance.registerLocalNotification()
        UserDefaults.standard.set(true, forKey: NotificationLimitManager.firstStartupKey)
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- GuideNotificationViewDelegate
    func guideNotificationViewTurnOnBtnClick(_ guideView: GuideNotificationView) {
        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
        guideView.removeFromSuperview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func  viewDidAppear(_ animated: Bool) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            userIma.sd_setImage(with: URL(string:""), placeholderImage: UIImage(named:""))
        }else{
            let user = AccountManager.sharedInstance.getUserInfo()?.avatar
            userIma.sd_setImage(with: URL(string:user ?? ""), placeholderImage: UIImage(named:""))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: AKPickerViewDataSource
    func numberOfItems(in pickerView: AKPickerView!) -> UInt {
        return UInt(self.zodiacNameList.count)
    }
    
    func pickerView(_ pickerView: AKPickerView!, titleForItem item: Int) -> String! {
        return self.zodiacNameList[item].zodiacName
    }
    
    //MARK: AKPickerViewDelegate
    
    func pickerView(_ pickerView: AKPickerView!, didSelectItem item: Int) {
        AnaliticsManager.sendEvent(AnaliticsManager.index_changehoroscope, data: ["a1_button_changehoroscope_click" : "\(self.zodiacNameList[item].zodiacName)"])
        self.scroll(item, animated: true)
    }
}



