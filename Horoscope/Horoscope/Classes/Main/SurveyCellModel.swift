//
//  SurveyCellModel.swift
//  Horoscope
//
//  Created by Beatman on 2017/5/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class SurveyCellModel: BaseCardListModel {
    
    init(dic:[String:AnyObject]) {
        super.init()
        self.surveyId            = dic["id"] as? String
        self.surveyImage          = dic["image"] as? String
        self.surveyTitle = dic["dic"] as? String
        self.surveyUrl = dic["url"] as? String
    }
    
}
