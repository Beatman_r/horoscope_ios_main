//
//  ForecastModel.swift
//  Horoscope
//
//  Created by Wang on 17/1/11.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ForecastPart: BaseCardListModel {
    var  list:[ForecastModel]? = []
}

class ForecastModel: NSObject {
    var  period:String?
    var  content:String?
    var  commentList:[MainComment]? = []
    
    var postId:String?
    var shareCount:Int = 0
    var viewCount:Int = 0
    var dislikeCount:Int = 0
    var likeCount:Int = 0
    var commentCount:Int = 0
    var isDisliked:Bool = false
    var isLiked:Bool = false
 
  
    func parse(_ dic:[String:AnyObject])  {
        self.content = dic["content"] as? String
        self.likeCount = dic["likeCount"] as? Int ?? 0
        self.dislikeCount = dic["dislikeCount"] as? Int ?? 0
        self.shareCount = dic["shareCount"] as? Int ?? 0
        self.commentCount = dic["commentCount"] as? Int ?? 0
        self.viewCount = dic["viewCount"] as? Int ?? 0
        self.isDisliked = dic["isDisliked"] as? Bool ?? false
        self.isLiked = dic["isLiked"] as? Bool ?? false
        self.postId = dic["postId"] as? String
    }
    
   
   class func create(_ dic:[String:AnyObject]) ->ForecastModel{
         let model = ForecastModel()
         model.period            = dic["period"] as? String
         model.content           = dic["content"] as? String
         return model
    }
}
