//
//  TagListViewModel.swift
//  Horoscope
//
//  Created by Beatman on 17/2/21.
//  Copyright © 2017年 meevii. All rights reserved.
//

protocol TagListViewModelDelegate : class{
    func shitHappends()
}

import UIKit

class TagListViewModel: NSObject {
    fileprivate var tagList : [MyPostModel] = []
    weak var delegate : TagListViewModelDelegate?
    var count : Int {
        return self.tagList.count
    }
    
    func getModel(_ row:Int) -> MyPostModel {
        return self.tagList[row]
    }
    
    let dao = HoroscopeDAO()
    func loadTagList(_ tagName:String,offset:Int,success:@escaping ()->(),failed:@escaping ()->()) {
        dao.getPostListWithTag(tagName, offset: offset, success: { (postList) in
            if postList.count == 0 {
                self.delegate?.shitHappends()
            }else {
                self.tagList = offset == 0 ? postList : self.tagList + postList
            }
            success()
            }) { (failure) in
                failed()
        }
    }
}
