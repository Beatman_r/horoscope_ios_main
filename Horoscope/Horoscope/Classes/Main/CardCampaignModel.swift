//
//  CardCampaignModel.swift
//  Horoscope
//
//  Created by Wang on 17/1/19.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
enum CampaignCategory:Int {
    case cardCookie
    case cardTarot
    case cardNotification
}

class CardCampaignModel: BaseCardListModel {
    
    var title:String?
    var cate:CampaignCategory?
    var figure:String?

    init(dic:[String:AnyObject]) {
        super.init()
        self.title            = dic["title"] as? String
        self.figure          = dic["figure"] as? String
    }
    
    
}
