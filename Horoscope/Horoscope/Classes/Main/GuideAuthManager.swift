//
//  GuideAuthManager.swift
//  Horoscope
//
//  Created by Wang on 16/12/28.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
enum TestAuthPushType:Int {
    case allowType
    case allow_laterType
    case noneType
}

enum TestGuideTextType:Int {
    case text1
    case text2
}

class GuideAuthManager: NSObject{
    
    func sendNewUserAnaliticEvent() {
    }
    
    func sendGuideViewShowAnaliticEvent() {
    }
    
    func sendConfirmEvent() {
    }
    
    func sendGuideGrantEvent(_ ifAllowed:Bool) {
         AnaliticsManager.sendEvent(AnaliticsManager.grant_notification_permission,data: ["sign":"\(ifAllowed)"])
    }
    
    static let guideTextType="pushTextType"
    func setGuideTextType() {
        let userdefault = UserDefaults.standard
        if let oldValue = userdefault.object(forKey: GuideAuthManager.guideTextType){
            print(oldValue)
        }else{
            let randomIndex = arc4random_uniform(100).hashValue
            if  randomIndex < 50{
                userdefault.set(TestGuideTextType.text1.rawValue, forKey: GuideAuthManager.guideTextType)
            }
            if randomIndex  >= 50{
                userdefault.set(TestGuideTextType.text2.rawValue, forKey: GuideAuthManager.guideTextType)
            }
        }
    }
    
    fileprivate let textStatusFromInt: [Int: TestGuideTextType] = [0: .text1,
                                                               1: .text2]
    func getGuideTextType() ->TestGuideTextType{
        let userdefault = UserDefaults.standard
        let loginStatus = userdefault.object(forKey: GuideAuthManager.guideTextType)
        if  loginStatus == nil {
            setGuideTextType()
        }
        let newStatus = userdefault.object(forKey: GuideAuthManager.guideTextType) as! Int
        return self.textStatusFromInt[newStatus]!
    }
    
    func getGuideTextArr() -> [String]{
        let type = GuideAuthManager.sharedInstance.getGuideTextType()
        switch type {
        case .text1:
            return ["We Do And Don't","We will only send you something cool when it happened. We promise!"]
        case .text2:
            return ["Notifications That Know You Well","We only send notifications when you need them most. Will you allow us?"]
        }
    }
    
    static let authGuideType = "testAuthPushType"
    static let sharedInstance = GuideAuthManager()
    func setAuthPushGuideType(){
        let userdefault = UserDefaults.standard
        if let oldValue = userdefault.object(forKey: GuideAuthManager.authGuideType){
            let s = userdefault.bool(forKey: NotificationLimitManager.registedNotification)
            print("第er次\(oldValue)   sss s\(s)")
            userdefault.synchronize()
        }
        else{
            //老用户升级上来
            if userdefault.bool(forKey: NotificationLimitManager.registedNotification) == true {
            userdefault.set(TestAuthPushType.noneType.rawValue, forKey: GuideAuthManager.authGuideType)
                      }//新用户第一次启动
            else{
            NotificationLimitManager.sharedInstance.refreshFirstStartApp()
            NotificationLimitManager.sharedInstance.refreshGuideViewShowTime()
            let randomIndex = arc4random_uniform(100).hashValue
            if  randomIndex < 50{
                userdefault.set(TestAuthPushType.allowType.rawValue, forKey: GuideAuthManager.authGuideType)
            }
            if randomIndex >= 50{
                userdefault.set(TestAuthPushType.allow_laterType.rawValue, forKey: GuideAuthManager.authGuideType)
            }
            userdefault.synchronize()
            GuideAuthManager.sharedInstance.sendNewUserAnaliticEvent()
        }
        }
    }
    
    fileprivate let statusFromInt: [Int: TestAuthPushType] = [0: .allowType,
                                                            1: .allow_laterType,
                                                            2: .noneType]
    func getGuideAuthType() ->TestAuthPushType{
        let userdefault = UserDefaults.standard
        let loginStatus = userdefault.object(forKey: GuideAuthManager.authGuideType)
        if  loginStatus == nil {
            setAuthPushGuideType()
        }
        let newStatus = userdefault.object(forKey: GuideAuthManager.authGuideType) as! Int
        return self.statusFromInt[newStatus]!
    }
}
