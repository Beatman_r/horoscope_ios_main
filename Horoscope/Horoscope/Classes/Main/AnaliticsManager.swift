//
//  AnaliticsManager.swift
//  Horoscope
//
//  Created by Wang on 16/12/3.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import FBSDKCoreKit.FBSDKAppEvents
import AppsFlyerLib
import Flurry_iOS_SDK
import Firebase

class AnaliticsManager: NSObject {
    
    //MARK:ME
    static let my_fortune_today_click = "my_fortune_today_click"
    static let my_zodiac_today_click = "my_zodiac_today_click"
    static let select_horoscope_done_click = "select_horoscope_done_click"
   
    //MARK:Banner
    static let forecast_banner_click="forecast_banner_click"
    static let forecast_banner_click_rate_us="forecast_banner_click_rate_us"

    //MARK:Video
    static let video_section_see_all_click="video_section_see_all_click"
    static let video_section_click="video_section_click"
    static let video_banner_click="video_banner_click"
    //MARK:notification
    static let grant_notification_permission="grant_notification_permission"
    
    //MARK:Rateus
    static let daily_top_ads_rate_click="daily_top_ads_rate_click"
    static let daily_top_ads_rate_close="daily_top_ads_rate_close"
    
    //MARK:Detail
    static let forecast_detail_show="forecast_detail_show"
    //MARK:  my fortune
    static let rate_appstore_click="rate_appstore_click"
    static let advice_rateUs_click="advice_rateUs_click"
     //MARK:login 非正式版本
    static let facebook_login="facebook_login"
    static let facebook_token="facebook_token"
    
    
    // MARK: Ad Analitics
    static let interAd = "interAd"
    static let innerAd = "innerAd"
    static let adUserType = "adUserType"
    static let nativeAds = "nativeAds"
    static let newInstall = "newInstall"
    
    
    //MARK:V1.1.5
    static let guide_show="guide_show"
    static let guide_continue_click="guide_continue_click"
    static let main_add_click="main_add_click"
    static let rate_us_click="rate_us_click"
    static let main_card_forcast_click="main_card_forcast_click"
    static let main_select_now_click="main_select_now_click"
    static let main_card_today_click="main_card_today_click"
    static let characteristics_show="characteristics_show"
    static let forecast_show="forecast_show"
    static let match_show="match_show"
    static let video_show="video_show"
    static let video_screen_stay_time="video_screen_stay_time"
    static let cookie_show="cookie_show"
    static let cookie_open="cookie_open"
    static let cookie_another_open="cookie_another_open"
    static let cookie_share_click="cookie_share_click"
    static let tarot_show="tarot_show"
    static let rate_us_1="rate_us_1"
    //MARK:Tarot
    static let tarot_select_category = "tarot_select_category"
    static let tarot_result_show="tarot_result_show"
    static let tarot_click="tarot_click"
    static let notification_click="notification_click" 
    static let daily_tarot="daily_tarot"
    static let forturn_cookie="forturn_cookie"
    static let today_horoscope="today_horoscope"
    static let lucky_number="lucky_number"
    static let lucky_color = "lucky_color"
    static let character_dailog="character_dailog"
    static let main_show="main_show"
    static let main_timeline_top_click="main_timeline_top_click"
    static let main_timeline_click="main_timeline_click"
    
    //MARK:Topic Version
    static let main_post_click = "main_post_click"
    static let main_notification_click = "main_notification_click"
    static let log_in_show = "log_in_show"
    static let select_sign_show = "select_sign_show"
    static let select_sign_click = "select_sign_click"
    static let me_click="me_click"
    static let main_card_click="main_card_click"
    static let splash_show = "splash_show"
    static let thread_view = "thread_view"
    
    //MARK: Post Version
    static let log_in_succeed = "log_in_succeed"
    static let post_show = "post_show"
    static let post_succeed = "post_succeed"
    static let drawer_item_click = "drawer_item_click"
    
    //MARK: 2.1.4
    static let index_changehoroscope = "index_changehoroscope"
    static let horoscopelist_detail = "horoscopelist_detail"
    static let back_ad_splash = "back_ad_splash"
    static let today_ad_native = "today_ad_native"

    fileprivate static var analizers = [Analizer]()
    static func initAnalizers() {
        analizers.removeAll()
        #if DEBUG
            analizers.append(FBAnalizer())
            analizers.append(GAAnalizer())
            analizers.append(FirebaseAnalizer())
            analizers.append(FlurryAnalizer())
            analizers.append(AppsFlyerAnalizer())
        #else
            analizers.append(FBAnalizer())
            analizers.append(GAAnalizer())
            analizers.append(FirebaseAnalizer())
            analizers.append(FlurryAnalizer())
            analizers.append(AppsFlyerAnalizer())
        #endif
        for a in analizers {
            a.startSession()
        }
    }
    
    static func sendEvent(_ event: String, data: [String: String]) {
        for a in analizers {
            a.sendEvent(event, data: data)
        }
    }
    static func sendEvent(_ event: String) {
        for a in analizers {
            a.sendEvent(event, data: [String: String]())
        }
    }
    static func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String) {
        for a in analizers {
            a.logPurchase(price, currency: currency, contentId: contentId, contentType: contentType)
        }
    }
}



protocol Analizer {
    func startSession()
    func sendEvent(_ event: String, data: [String: String])
    func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String)
}

class FBAnalizer: Analizer {
    func startSession() {
        FBSDKAppEvents.activateApp()
    }
    
    func sendEvent(_ event: String, data: [String: String]) {
        FBSDKAppEvents.logEvent(event, parameters: data)
        LXSWLog("\(NSStringFromClass(FBAnalizer.self)): \(event) \(data.description)")
    }
    
    func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String) {
        _ = [FBSDKAppEvents .logPurchase(price, currency: currency, parameters: [
            FBSDKAppEventParameterNameContentID: contentId,
            FBSDKAppEventParameterNameContentType : contentType,
            FBSDKAppEventParameterNameNumItems: NSNumber.init(value: price as Double),
            FBSDKAppEventParameterNameCurrency: currency])]
    }
}

class FlurryAnalizer: Analizer {
    func startSession() {
        Flurry.startSession("8Z9B2ZRQ4ZPG47FGTVNB")
    }
    
    func sendEvent(_ event: String, data: [String: String]) {
        Flurry.logEvent(event, withParameters: data)
        LXSWLog("\(NSStringFromClass(FlurryAnalizer.self)): \(event) \(data.description)")
    }
    
    func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String) {
        
    }
}

class GAAnalizer: Analizer {
    func startSession() {
        GAI.sharedInstance().tracker(withTrackingId: "UA-85978074-2")
    }
    
    func sendEvent(_ event: String, data: [String: String]) {
        let gai = GAI.sharedInstance()
        let traker = gai?.defaultTracker
        if let d = GAIDictionaryBuilder.createEvent(withCategory: event,action: data.keys.first,label: data.values.first,value: nil).build() {
            traker?.send(d as NSDictionary? as! [AnyHashable : Any])
        }
        LXSWLog("\(NSStringFromClass(GAAnalizer.self)): \(event) \(data.description)")
    }
    
    func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String) { }
}

class FirebaseAnalizer: Analizer {
    func startSession() {
        // start in Firebase init
    }
    
    func sendEvent(_ event: String, data: [String: String]) {
        FIRAnalytics.logEvent(withName: event, parameters: data as [String : NSObject]?)
        LXSWLog("\(NSStringFromClass(FirebaseAnalizer.self)): \(event) \(data.description)")
    }
    
    func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String) { }
}

class AppsFlyerAnalizer: Analizer {
    func startSession() {
    AppsFlyerTracker.shared().appsFlyerDevKey="qNsTSFixbaPufCv5sQ6yJV"
        AppsFlyerTracker.shared().appleAppID="1184938845"
    }
    
    func sendEvent(_ event: String, data: [String: String]) {
        AppsFlyerTracker.shared().trackEvent(event, withValues:data)
        LXSWLog("\(NSStringFromClass(AppsFlyerAnalizer.self)): \(event) \(data.description)")
    }
    
    func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String) {
        #if DEBUG
            AppsFlyerTracker.shared().useReceiptValidationSandbox = true;
        #endif
        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [
            AFEventParamContentId: contentId,
            AFEventParamContentType : contentType,
            AFEventParamRevenue: NSNumber.init(value: price as Double),
            AFEventParamCurrency:currency])
    }
}

class BuglyAnalizer:Analizer{
    func startSession() {
    }
    
    func sendEvent(_ event: String, data: [String: String]) {
        if data["exception"] != nil {
            let errorMessage = data["exception"]
            Bugly.reportError(NSError(domain: errorMessage ?? "", code: 1, userInfo: nil))
        }
    }
    
    func logPurchase(_ price: Double, currency: String, contentId: String, contentType: String) { }
}


