//
//  HoroPickerView.swift
//  Horoscope
//
//  Created by Wang on 16/12/21.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

protocol HoroPickerViewDelegate:class {
    func chooseComplete(_ date:Date)
    func chooseCancled()
}

class HoroPickerView: UIView {
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
       
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    weak var delegate:HoroPickerViewDelegate?
    
    func setupView() {
        self.dialogView = createContainerView()
        self.dialogView!.layer.shouldRasterize = true
        self.dialogView!.layer.rasterizationScale = UIScreen.main.scale
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.dialogView!.layer.opacity = 0.5
        self.dialogView!.layer.transform = CATransform3DMakeScale(1, 1, 1)
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        self.addSubview(self.dialogView!)
    }
    
    // MARK: - Constants
    fileprivate let kDatePickerDialogDefaultButtonHeight:       CGFloat = 150
    fileprivate let kDatePickerDialogDefaultButtonSpacerHeight: CGFloat = 1
    fileprivate let kDatePickerDialogCornerRadius:              CGFloat = 7
    fileprivate let kDatePickerDialogDoneButtonTag:             Int     = 1
    /// Creates the container view here: create the dialog, then add the custom content and buttons
    fileprivate func createContainerView() -> UIView {
        var diaLogHeight:CGFloat = 340
        if UIScreen.main.bounds.size.height == 480{
      
        }else{
            diaLogHeight=375
        }
        let screenSize = countScreenSize()
        let dialogSize = CGSize(
            width: 300,
            height: diaLogHeight)
            //diaLogHeight
             //   + kDatePickerDialogDefaultButtonHeight
              //  + kDatePickerDialogDefaultButtonSpacerHeight)
        // For the black background
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        // This is the dialog's container; we attach the custom content and the buttons to this one
        let dialogContainer = UIView(frame: CGRect(x: (screenSize.width - dialogSize.width) / 2, y: (screenSize.height - dialogSize.height) / 2, width: dialogSize.width, height: dialogSize.height))
        // First, we style the dialog to match the iOS8 UIAlertView >>>
        let gradient: CAGradientLayer = CAGradientLayer(layer: self.layer)
        gradient.frame = dialogContainer.bounds
        gradient.colors = [UIColor.white.cgColor,
                           UIColor.white.cgColor,
                           UIColor.white.cgColor]
        
        let cornerRadius = kDatePickerDialogCornerRadius
        gradient.cornerRadius = cornerRadius
        dialogContainer.layer.insertSublayer(gradient, at: 0)
        
        dialogContainer.layer.cornerRadius = cornerRadius
        dialogContainer.layer.borderColor = UIColor.white.cgColor
        dialogContainer.layer.borderWidth = 1
        dialogContainer.layer.shadowRadius = cornerRadius + 5
        dialogContainer.layer.shadowOpacity = 0.1
        dialogContainer.layer.shadowOffset = CGSize(width: 0 - (cornerRadius+5)/2, height: 0 - (cornerRadius + 5) / 2)
        dialogContainer.layer.shadowColor =  UIColor.white.cgColor
        dialogContainer.layer.shadowPath = UIBezierPath(roundedRect: dialogContainer.bounds, cornerRadius: dialogContainer.layer.cornerRadius).cgPath
        
        self.cancelButton = UIButton(type:.custom)
        self.cancelButton.setImage(UIImage(named: "ic_close_"), for: UIControlState())
        self.cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        dialogContainer.addSubview(self.cancelButton)
        self.cancelButton.snp.makeConstraints { (make) in
            make.top.equalTo(dialogContainer.snp.top).offset(4)
            make.right.equalTo(dialogContainer.snp.right).offset(-8)
            make.width.height.equalTo(40)
        }
        
       //Title
        self.titleLabel = UILabel(frame: CGRect(x: 10, y: 24, width: 280, height: 60))
        self.titleLabel.textAlignment = .center
        self.titleLabel.numberOfLines=0
        self.titleLabel.text = "Enter your date of birth to find your sign"
        self.titleLabel.font = HSFont.baseRegularFont(22)
        self.titleLabel.textColor=UIColor.hsTextColor(1)
        dialogContainer.addSubview(self.titleLabel)
        
        self.datePickers = UIDatePicker(frame: CGRect(x: 10, y: 60+14, width: 0, height: 0))
        self.datePickers.datePickerMode = .date
        self.datePickers.autoresizingMask = .flexibleRightMargin
        self.datePickers.frame.size.width = 280
        self.datePickers.frame.size.height = 190
        self.datePickers.date=Date()
        self.datePickers.addTarget(self, action: #selector(dateChange), for: .valueChanged)
        self.horoImg=UIImageView()
        dialogContainer.addSubview(self.datePickers)
        dialogContainer.addSubview(self.horoImg!)
        horoImg?.layer.masksToBounds=true
        self.dateChange()
        self.doneButton=UIButton(type:.custom)
        dialogContainer.addSubview(self.doneButton)
        self.doneButton.setTitle(NSLocalizedString("Done", comment: ""), for: UIControlState())
       
        if UIScreen.main.bounds.size.height == 480{
            horoImg?.snp.makeConstraints({ (make) in
                make.top.equalTo(self.datePickers.snp.bottom).offset(2)
                make.centerX.equalTo(self.datePickers.snp.centerX)
                make.width.equalTo(130)
                make.height.equalTo(24)
            })
            self.doneButton.snp.makeConstraints { (make) in
                make.centerX.equalTo(self.datePickers.snp.centerX)
                make.height.equalTo(28)
                make.width.equalTo(220)
                make.top.equalTo(horoImg!.snp.bottom).offset(12)
            }
            doneButton.layer.cornerRadius=14
            doneButton.layer.masksToBounds = true
            doneButton.titleLabel?.font = HSFont.baseRegularFont(20)
         }else{
            horoImg?.snp.makeConstraints({ (make) in
                make.top.equalTo(self.datePickers.snp.bottom).offset(12)
                make.centerX.equalTo(self.datePickers.snp.centerX)
                make.width.equalTo(130)
                make.height.equalTo(24)
            })
            self.doneButton.snp.makeConstraints { (make) in
                make.width.equalTo(220)
                make.centerX.equalTo(self.datePickers.snp.centerX)
                make.height.equalTo(38)
                make.top.equalTo(horoImg!.snp.bottom).offset(24)
            }
            doneButton.layer.cornerRadius=19
            doneButton.layer.masksToBounds = true
              doneButton.titleLabel?.font = HSFont.baseRegularFont(24)
        }
         doneButton.backgroundColor=UIColor.hsViewcountsColor()
    
        self.doneButton.addTarget(self, action: #selector(done), for: .touchUpInside)
        return dialogContainer
    }
    
    @objc func cancel() {
        let currentTransform = self.dialogView.layer.transform
        let startRotation = (self.value( forKeyPath: "layer.transform.rotation.z") as? NSNumber) as? Double ?? 0.0
        let rotation = CATransform3DMakeRotation((CGFloat)(-startRotation + Double.pi * 270 / 180), 0, 0, 0)
        
        self.dialogView.layer.transform = CATransform3DConcat(rotation, CATransform3DMakeScale(1, 1, 1))
        self.dialogView.layer.opacity = 1
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [],
            animations: {
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.dialogView.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6, 0.6, 1))
                self.dialogView.layer.opacity = 0
        }) { (finished) in
            for v in self.subviews {
                v.removeFromSuperview()
            }
            self.removeFromSuperview()
            self.setupView()
        }
        self.delegate?.chooseCancled()
    }
    
   
    @objc func done() {
        
        let currentTransform = self.dialogView.layer.transform
        let startRotation = (self.value( forKeyPath: "layer.transform.rotation.z") as? NSNumber) as? Double ?? 0.0
        let rotation = CATransform3DMakeRotation((CGFloat)(-startRotation + Double.pi * 270 / 180), 0, 0, 0)
        
        self.dialogView.layer.transform = CATransform3DConcat(rotation, CATransform3DMakeScale(1, 1, 1))
        self.dialogView.layer.opacity = 1
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [],
            animations: {
                self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                self.dialogView.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6, 0.6, 1))
                self.dialogView.layer.opacity = 0
        }) { (finished) in
            for v in self.subviews {
                v.removeFromSuperview()
            }
            self.removeFromSuperview()
            self.setupView()
        }
        self.delegate?.chooseComplete(self.datePickers.date)
    }
    
    @objc func dateChange() {
//    let imgStr = DataManager.sharedInstance.//horoInfo.getZodiacPicNameByDate(self.datePickers.date)
//    self.horoImg?.image=UIImage(named: imgStr)
    }
    
    func show(_ parentView:UIView){
        /* Anim */
        parentView.addSubview(self)
        parentView.bringSubview(toFront: self)
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
            self.dialogView!.layer.opacity = 2
            self.dialogView!.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }) { (ff) in
            
        }
    }
    
    /// Count and return the screen's size
    func countScreenSize() -> CGSize {
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        return CGSize(width: screenWidth, height: screenHeight)
    }
    
    // MARK: - Views
    fileprivate var dialogView:   UIView!
    fileprivate var titleLabel:   UILabel!
    var datePickers: UIDatePicker!
    fileprivate var cancelButton: UIButton!
    fileprivate var doneButton:   UIButton!
 
    var horoImg:UIImageView?
    
    
}
