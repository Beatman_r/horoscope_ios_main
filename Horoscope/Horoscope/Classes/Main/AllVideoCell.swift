//
//  AllVideoCell.swift
//  Horoscope
//
//  Created by Wang on 16/12/9.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SDWebImage

class AllVideoCell:BaseCardCell {
    weak var rootvc:UIViewController?
    var viewCountWidth:CGFloat = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addUI()
        self.addConstrains()
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(cellDidSelect))
//        self.addGestureRecognizer(gesture)
    }
    
    func cellDidSelect(){
     }
    
    func addUI() {
        self.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.cornerBackView?.layer.cornerRadius = 0
        self.cornerBackView?.layer.masksToBounds = true
        self.cornerBackView?.addSubview(bgImg)
        self.cornerBackView?.addSubview(titleLab)
        self.cornerBackView?.addSubview(fromOrgLab)
        self.cornerBackView?.addSubview(videoImg)
        self.cornerBackView?.addSubview(viewsLabel)
        self.cornerBackView?.addSubview(divideLine)
        self.cornerBackView?.addSubview(durationLab)
    }
    
    let fromOrgLab:UILabel = {
        let view = UILabel()
        view.font = HSFont.baseLightFont(15)
        view.text = NSLocalizedString("YouTube", comment: "")
        view.textColor = UIColor.luckyPurpleTitleColor()
        return view
    }()
    
    let videoImg:UIImageView={
        let img = UIImageView()
        img.image = UIImage(named:"ic_youtube_" )
        return img
    }()
    
    let viewCountBackImgView:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "video_blackground_")
        return view
    }()
    
    let viewsLabel:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 1
        lab.textAlignment = .left
        lab.font=HSFont.baseLightFont(15)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    let divideLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.divideLineColor()
        return view
    }()
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.textAlignment = .left
        lab.font=HSFont.baseRegularFont(20)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    let bgImg:UIImageView = {
        let view = UIImageView()
        view.backgroundColor=UIColor.white
        return view
    }()
    
    let durationLab:UILabel = {
        let lab = UILabel()
        lab.font = UIFont.systemFont(ofSize: 11)
        lab.textColor = UIColor.durationTimeColor()
        lab.textAlignment = NSTextAlignment.center
        lab.layer.cornerRadius = 6
        lab.layer.masksToBounds = true
        lab.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        lab.textColor = UIColor.white
        lab.font = HSFont.baseRegularFont(13)
        return lab
    }()
    
    func addConstrains(){
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)//.offset(BaseCardCell.cellPadding+2)
            make.right.equalTo(self.contentView.snp.right)//.offset(-BaseCardCell.cellPadding-2)
            make.top.equalTo(self.contentView.snp.top)//.offset(BaseCardCell.cellPadding+2)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        bgImg.snp.makeConstraints { (make) in
            make.top.equalTo(self.cornerBackView!.snp.top).offset(10)
            make.left.equalTo(self.cornerBackView!.snp.left).offset(10)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-10)
            make.height.equalTo(AllVideoCell.height2)
        }
       
        titleLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.bgImg.snp.bottom).offset(12)
            make.left.equalTo(bgImg.snp.left)
            make.right.equalTo(bgImg.snp.right)
        }
        
        viewsLabel.snp.makeConstraints { (make) in
            make.right.equalTo(bgImg.snp.right).offset(-10)
            make.centerY.equalTo(self.fromOrgLab.snp.centerY)
        }
        
        videoImg.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left).offset(15)
            make.top.equalTo(self.titleLab.snp.bottom).offset(12)
            make.width.equalTo(15)
            make.height.equalTo(12)
        }
        
        fromOrgLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.videoImg.snp.right).offset(screenWidth*0.01)
            make.centerY.equalTo(videoImg.snp.centerY)
            make.height.equalTo(16)
        }
        
        divideLine.snp.makeConstraints { (make) in
            make.left.equalTo(self.bgImg.snp.left)
            make.right.equalTo(self.bgImg.snp.right)
            make.height.equalTo(1)
            make.top.equalTo(self.cornerBackView!.snp.bottom).offset(-1)
        }
        
        durationLab.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.bgImg.snp.bottom).offset(-5)
            make.right.equalTo(self.bgImg.snp.right).offset(-6)
        }
    }
    
    var model:BreadModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        if self.model != nil {
            if let authorImaStr = model?.thumbnail {
                if model?.viewCount == 1 || model?.viewCount == 0{
                    viewsLabel.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((model?.viewCount) ?? 0) + " " + NSLocalizedString("view", comment: "")
                }else{
                viewsLabel.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((model?.viewCount) ?? 0) + " " + NSLocalizedString("views", comment: "")
                }
                titleLab.text = model?.title
                bgImg.sd_setImage(with: URL(string:authorImaStr), placeholderImage: UIImage(named:"defaultImg1.jpg"))
                
                let width : CGFloat = HSHelpCenter.sharedInstance.textTool.calculateStrWidth(model?.richMediaDuration, font: 12, height: 14)
                durationLab.text = model?.richMediaDuration
                durationLab.snp.updateConstraints({ (make) in
                    make.width.equalTo(width+8)
                })
                
            }
        }
    }
   
    static let height2 = (UIScreen.main.bounds.size.width-BaseCardCell.cellPadding*2)/344 * 246
    
    class func calculateHeight(_ title:String)->CGFloat{
    let titleHeight=HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: title, fontSize: 20, font: HSFont.baseRegularFont(20), width: UIScreen.main.bounds.size.width-18, multyplyLineSpace: 1)
        return (titleHeight + height2 + 22 + 28 + 13)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let cellId="AllVideoCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath,model:BreadModel) -> AllVideoCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! AllVideoCell
        cell.model=model
        return cell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
