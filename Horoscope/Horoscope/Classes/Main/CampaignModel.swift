//
//  CampaignModel.swift
//  Horoscope
//
//  Created by Wang on 16/12/14.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class CampaignModel: BaseCardListModel {
    
    var actionParam:String?
    var weight:Int?
    var figure :String?
    var title : String = ""
    var content:String?
    var action:String?
    var id:String?
    var gameCate:Int=0
    
   
    init(json:JSON) {
        super.init()
        self.actionParam    = json["actionParam"].string
        self.weight         = json["weight"].intValue
        self.figure         = json["figure"].string
        self.title          = json["title"].string ?? ""
        self.content        = json["content"].string
        self.action         = json["action"].string
        self.id             = json["id"].string
    }
    
    init(dic:[String:AnyObject]) {
        super.init()
        self.content         = dic["content"] as? String
        self.figure          = dic["figure"] as? String
        self.title           = dic["title"] as? String ?? ""
        self.actionParam     = dic["actionParam"] as? String
        self.id              = dic["id"] as? String
        self.weight          = dic["weight"] as?Int
        self.action          = dic["action"] as? String
    }
 }
