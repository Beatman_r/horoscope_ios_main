//
//  LuckyModel.swift
//  Horoscope
//
//  Created by Wang on 17/1/11.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class LuckyModel: BaseCardListModel {
    var luckNumber : String?
    var luckColor:String?
    var luckColorValue:String?
    var content:String?
    var zodiacName:String?
    var commentList:[MainComment]?
    var postId:String?
    var shareCount:Int = 0
    var viewCount:Int = 0
    var dislikeCount:Int = 0
    var likeCount:Int = 0
    var commentCount : Int = 0
    var isLiked : Bool? = false
    var isDisliked : Bool? = false
    
    //You may be lost in lovely ..."
    
    func create(_ dic:[String:AnyObject]) {
        self.luckNumber            =  dic["luckNumber"] as? String
        self.luckColor             = dic["luckColor"] as? String
        self.luckColorValue        = dic["luckColorValue"] as? String
        self.content        = dic["content"] as? String
        self.postId = dic["postId"] as? String
        self.shareCount = dic["shareCount"] as? Int ?? 0
        self.viewCount = dic["viewCount"] as? Int ?? 0
        self.dislikeCount = dic["dislikeCount"] as? Int ?? 0
        self.likeCount = dic["likeCount"] as? Int  ?? 0
        self.commentList = dic["commentList"] as? [MainComment] ?? []
        self.commentCount = dic["commentCount"] as? Int ?? 0
        self.isLiked = dic["isLiked"] as? Bool
        self.isDisliked = dic["isDisliked"] as? Bool
    }
}
