//
//  SettingViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/11/30.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class SettingViewController: BaseTableViewController,HsHUD {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let helper = HSHelpCenter.sharedInstance
        content = helper.appAndDeviceTool.appVersionCode()+"  "+NSLocalizedString("Build", comment: "")+" "+helper.appAndDeviceTool.appBuildNum()
        setupSettingTableView()
        self.addBackBtn()
        self.refreshTableview()
        self.view.layer.contents = UIImage(named: "Setting_bg.jpg")?.cgImage
        // Do any additional setup after loading the view.
        self.title=NSLocalizedString(NSLocalizedString("Setting", comment: ""), comment: "")
    }
    var content:String?
    func setupSettingTableView() {
        self.tableView.isScrollEnabled = false
    }
    
    var rowNum:Int = 3
    func refreshTableview() {
        let status = AccountManager.sharedInstance.getLogStatus()
        if status == .faceBookUser || status == .googleUser {
            self.rowNum = 4
            self.tableView.reloadData()
        }else{
            self.rowNum = 3
            self.tableView.reloadData()
        }
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    @objc func popView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func registerCell() {
        self.tableView.register(SettingAboutCell.self, forCellReuseIdentifier: "SettingAboutCell")
        self.tableView.register(MorningNotiCell.self, forCellReuseIdentifier: "MorningNotiCell")
        self.tableView.register(EveningNotiCell.self, forCellReuseIdentifier: "EveningNotiCell")
         self.tableView.register(SignoutCell.self, forCellReuseIdentifier: "SignoutCell")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowNum
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var rowNum = 0
        if indexPath.row == rowNum {
            let cell = SettingAboutCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell(NSLocalizedString("About", comment: ""), content: NSLocalizedString("Version", comment: "")+" " + (content ?? ""))
            return cell
        }
        rowNum += 1
        if indexPath.row == rowNum{
            let cell = MorningNotiCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell(NSLocalizedString("Moring Notification", comment: ""), content: "")
            return cell
        }
        rowNum += 1
        if indexPath.row == rowNum {
            let cell = EveningNotiCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell("Moring Notification", content: "ssssss")
            return cell
        }
        rowNum += 1
        if indexPath.row == rowNum {
            let cell = SignoutCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell()
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 3{
        let status = AccountManager.sharedInstance.getLogStatus()
        if status == .faceBookUser || status == .googleUser {
            self.showProccessHUD()
            AccountManager.sharedInstance.logout({
                self.hideHUD()
                self.refreshTableview()
                }, failure: {
                self.showErrorHUD()
            })
        }
       }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowNum = 0
        if  indexPath.row == rowNum{
            return SettingAboutCell.calculateHeight(self.content ?? "")
        }
        rowNum += 1
        if indexPath.row == rowNum{
            return MorningNotiCell.calculateHeight("Morning Horoscope NotificationMorning")
        }
        rowNum += 1
        if indexPath.row == rowNum{
            return EveningNotiCell.calculateHeight("Morning Horoscope NotificationMorning")
        }
        rowNum += 1
        if indexPath.row == rowNum {
           return SignoutCell.calculateHeight("")
        }
        return 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
