//
//  EveningNotiCell.swift
//  Horoscope
//
//  Created by Wang on 16/12/1.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class EveningNotiCell: BaseCardCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.cornerBackView?.addSubview(contentLab)
        self.cornerBackView?.addSubview(spreadLine)
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.contentView.snp.right).offset(-BaseCardCell.cellPadding)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        self.cornerBackView?.addSubview(spreadLine)
        self.addConstrains()
        self.switchSetting()
        self.cornerBackView?.backgroundColor = UIColor.clear
    }
    
    func addConstrains() {
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-65)
            make.top.equalTo(self.cornerBackView!.snp.top).offset(8)
            make.bottom.equalTo(self.cornerBackView!.snp.bottom).offset(-8)
        }
        spreadLine.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.cornerBackView!.snp.bottom)
            make.left.equalTo(contentLab.snp.left)
            make.right.equalTo(cornerBackView!.snp.right).offset(-12)
            make.height.equalTo(1)
        }
    }
    
    let contentLab:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.text = NSLocalizedString("Evening Notification", comment: "")
        label.font=HSFont.baseRegularFont(18)
        label.textColor = UIColor.commonPinkColor()
        return label
    }()
    
    let enableBtn:UIImageView = {
        let button = UIImageView()
        return button
    }()
    
    let spreadLine:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.commentTimeColor()
        v.alpha=0.7
        return v
    }()
    
    
    var switchView:UISwitch?
    func switchSetting() {
        switchView = UISwitch()
        self.cornerBackView?.addSubview(switchView!)
        switchView?.onTintColor = UIColor.commonPinkColor()
        switchView?.tintColor = UIColor.commonPinkColor()
        switchView?.addTarget(self, action: #selector(switchActin), for: .valueChanged)
        switchView!.snp.makeConstraints { (make) in
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.height.equalTo(28)
            make.centerY.equalTo(self.contentLab.snp.centerY).offset(-1)
        }
    }
    
    @objc func switchActin(){
        if NotificationManager.sharedInstance.eveningNotiAllowStatus() == false{
            NotificationManager.sharedInstance.allowEveningNoti(true)
        }else{
            NotificationManager.sharedInstance.allowEveningNoti(false)
        }
    }
    
    func renderCell(_ title:String,content:String) {
        if NotificationManager.sharedInstance.eveningNotiAllowStatus() == false{
            switchView?.isOn=false
        }else{
            switchView?.isOn=true
        }
    //    self.contentLab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.hsTextColor(1)!, targetStr: content ?? "", font: HSFont.baseContentFont())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK:Create
    static let cellId = "EveningNotiCell"
    class func dequeueReusableCell(_ tableView: UITableView,
                                   cellForRowAtIndexPath indexPath: IndexPath) -> EveningNotiCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId,
                                                               for: indexPath) as! EveningNotiCell
        return cell
    }
    
    class func calculateHeight(_ content:String) ->CGFloat{
        return HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: content, fontSize: 16, font: HSFont.baseTitleFont(), width: UIScreen.main.bounds.size.width-100, multyplyLineSpace: 1.25)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.layoutSubviews()
        // Configure the view for the selected state
    }
}
