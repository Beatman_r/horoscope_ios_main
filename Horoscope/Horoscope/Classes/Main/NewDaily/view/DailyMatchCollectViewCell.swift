//
//  DailyMatchCollectViewCell.swift
//  Horoscope
//
//  Created by Beatman on 17/1/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class DailyMatchCollectViewCell: UICollectionViewCell {
    
    var avaImg : UIImageView!
    var itemName : String!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.avaImg = UIImageView()
        self.contentView.addSubview(avaImg)
        self.contentView.backgroundColor = UIColor.clear
        avaImg.frame = self.contentView.frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
