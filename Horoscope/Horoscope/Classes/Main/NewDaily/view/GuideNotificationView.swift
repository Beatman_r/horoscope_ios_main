//
//  GuideNotificationView.swift
//  Horoscope
//
//  Created by xiao  on 17/1/19.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
protocol GuideNotificationViewDelegate:class{
//    func guideNotificationViewLaterBtnClick(guideView:GuideNotificationView)
    func guideNotificationViewTurnOnBtnClick(_ guideView:GuideNotificationView)
}
class GuideNotificationView: UIView {
    
    let backView = UIImageView()
    weak var delegate:GuideNotificationViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(hexString: "000000",withAlpha: 0.6)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapToDismissView))
        self.addGestureRecognizer(tapGesture)
        self.createNotiSubviews()
    }
    
    func createNotiSubviews() {
        self.backView.backgroundColor = UIColor.clear
        self.backView.image = UIImage(named: "inform_")
        self.backView.isUserInteractionEnabled = true
        let closeBtn = UIButton(type: .custom)
        closeBtn.setImage(UIImage(named: "ic_Popup-window"), for: UIControlState())
        closeBtn.addTarget(self, action: #selector(closeGuideView), for: .touchUpInside)
        self.addSubview(closeBtn)
        
        let titleLabel = UILabel()
        titleLabel.font = HSFont.baseLightFont(22)
        titleLabel.textColor = UIColor.guideNotifColor()
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.text = "Please allow us to send you notifications,or you might miss"

        let contentLabel1 = createContentLabel("your daily predictions")
        let imageView1 = createImageView()
        let contentLabel2 = createContentLabel("today's lucky number")
        let imageView2 = createImageView()
        let contentLabel3 = createContentLabel("the lucky color for the day")
        let imageView3 = createImageView()
        let contentLabel4 = createContentLabel("and more")
        let imageView4 = createImageView()
        let allowBtn = UIButton(type: .custom)
        allowBtn.setTitle("ALLOW", for: UIControlState())
        allowBtn.titleLabel?.font = HSFont.baseLightFont(22)
        allowBtn.layer.borderWidth = 1
        allowBtn.backgroundColor = UIColor.white
        allowBtn.layer.cornerRadius = 23/667*AdaptiveUtils.screenHeight
        allowBtn.layer.masksToBounds = true
        allowBtn.layer.borderColor = UIColor.commonPinkColor().cgColor
        allowBtn.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        allowBtn.addTarget(self, action: #selector(self.allowBtnCLick), for: .touchUpInside)
        
        self.addSubview(backView)
        self.backView.addSubview(titleLabel)
        self.backView.addSubview(imageView1)
        self.backView.addSubview(contentLabel1)
        self.backView.addSubview(imageView2)
        self.backView.addSubview(contentLabel2)
        self.backView.addSubview(imageView3)
        self.backView.addSubview(contentLabel3)
        self.backView.addSubview(imageView4)
        self.backView.addSubview(contentLabel4)
        self.backView.addSubview(allowBtn)
        
        //create constrains
        closeBtn.snp.makeConstraints { (make) in
            make.right.equalTo(self.snp.right).offset(-23)
            make.top.equalTo(self.snp.top).offset(32)
            make.height.equalTo(45)
            make.width.equalTo(45)
        }
        self.backView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.height.equalTo(442/667*AdaptiveUtils.screenHeight)
            make.width.equalTo(312/375*AdaptiveUtils.screenWidth)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.backView.snp.top).offset(AdaptiveUtils.screenWidth < 350 ? (90/667*AdaptiveUtils.screenHeight):(100/667*AdaptiveUtils.screenHeight))
            make.left.equalTo(self.backView.snp.left).offset(33/375*AdaptiveUtils.screenWidth)
            make.right.equalTo(self.backView.snp.right).offset(-33/375*AdaptiveUtils.screenWidth)
        }
        
        imageView1.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(22/667*AdaptiveUtils.screenHeight)
            make.left.equalTo(self.backView.snp.left).offset(45/375*AdaptiveUtils.screenWidth)
            make.height.equalTo(10)
            make.width.equalTo(10)
        }
        
        contentLabel1.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageView1)
            make.left.equalTo(imageView1).offset(15/375*AdaptiveUtils.screenWidth)
            make.right.equalTo(self.backView.snp.right).offset(-32/375*AdaptiveUtils.screenWidth)
        }
        
        imageView2.snp.makeConstraints { (make) in
            make.centerX.equalTo(imageView1.snp.centerX)
            make.top.equalTo(imageView1.snp.bottom).offset(22/667*AdaptiveUtils.screenHeight)
            make.height.equalTo(10)
            make.width.equalTo(10)
        }
        
        contentLabel2.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageView2)
            make.left.equalTo(contentLabel1.snp.left)
        }
        
        imageView3.snp.makeConstraints { (make) in
            make.centerX.equalTo(imageView1.snp.centerX)
            make.top.equalTo(imageView2.snp.bottom).offset(22/667*AdaptiveUtils.screenHeight)
            make.height.equalTo(10)
            make.width.equalTo(10)
        }
        
        contentLabel3.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageView3)
            make.left.equalTo(contentLabel1.snp.left)
        }
        
        imageView4.snp.makeConstraints { (make) in
            make.centerX.equalTo(imageView1.snp.centerX)
            make.top.equalTo(imageView3.snp.bottom).offset(22/667*AdaptiveUtils.screenHeight)
            make.height.equalTo(10)
            make.width.equalTo(10)
        }
        
        contentLabel4.snp.makeConstraints { (make) in
            make.centerY.equalTo(imageView4)
            make.left.equalTo(contentLabel1.snp.left)
        }
        
        allowBtn.snp.makeConstraints { (make) in
            make.top.equalTo(contentLabel4.snp.bottom).offset(AdaptiveUtils.screenWidth < 350 ? (20/667*AdaptiveUtils.screenHeight):(32/667*AdaptiveUtils.screenHeight))
            make.height.equalTo(46/667*AdaptiveUtils.screenHeight)
            make.right.equalTo(self.backView.snp.right).offset(-32/375*AdaptiveUtils.screenWidth)
            make.left.equalTo(self.backView.snp.left).offset(32/375*AdaptiveUtils.screenWidth)
        }
     }
    func tapToDismissView() {
        self.removeFromSuperview()
    }
    
    func createContentLabel(_ str:String) -> UILabel {
        let contentLabel = UILabel()
        contentLabel.font = HSFont.baseRegularFont(18)
        contentLabel.textColor = UIColor.guideNotifColor()
        contentLabel.numberOfLines = 1
        contentLabel.textAlignment = NSTextAlignment.left
        contentLabel.text = str
        return contentLabel
    }
    
    func createImageView() -> UIView {
        let imageView = UIView()
        imageView.backgroundColor = UIColor.guideNotifColor()
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        return imageView
    }
    
    func closeGuideView() {
        self.removeFromSuperview()
    }
    func allowBtnCLick() {
        if self.delegate != nil {
            delegate?.guideNotificationViewTurnOnBtnClick(self)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
