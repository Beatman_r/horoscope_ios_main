//
//  CommentStatusBar.swift
//  Horoscope
//
//  Created by Beatman on 17/2/15.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

protocol CommentStatusBarDelegate : class {
    func didClickComment()
    func didClickLike()
    func didClickDislike()
    func didClickShare()
}

//            HSHelpCenter.sharedInstance.textTool.getFormattedNumber(100)

class CommentStatusBar: UIView {
    
    weak var delegate : CommentStatusBarDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.addCBSubviews()
        self.addCBConstraint()
        self.addTargets()
    }
    
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    
    let likeButton : UIButton = {
        let likeButton = UIButton()
        likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
        return likeButton
    }()
    
    let likeCountLabel : UILabel = {
        let likeCountLabel = UILabel()
        likeCountLabel.text = "1024"
        likeCountLabel.textAlignment = NSTextAlignment.left
        likeCountLabel.font = HSFont.baseRegularFont(15)
        likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
        return likeCountLabel
    }()
    
    let unLikeButton : UIButton = {
        let unLikeButton = UIButton()
        unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
        return unLikeButton
    }()
    
    let unLikeCountLabel : UILabel = {
        let unLikeCountLabel = UILabel()
        unLikeCountLabel.text = "1024"
        unLikeCountLabel.textAlignment = NSTextAlignment.left
        unLikeCountLabel.font = HSFont.baseRegularFont(15)
        unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
        return unLikeCountLabel
    }()
    
    let commentButton : UIButton = {
        let commentButton = UIButton()
        commentButton.setImage(UIImage(named: "comments_"), for: UIControlState())
        return commentButton
    }()
    
    let commentCountLabel : UILabel = {
        let commentCountLabel = UILabel()
        commentCountLabel.text = "1024"
        commentCountLabel.textAlignment = .left
        commentCountLabel.font = HSFont.baseRegularFont(15)
        commentCountLabel.textColor = UIColor.luckyPurpleTitleColor()
        return commentCountLabel
    }()
    
    let shareButton : UIButton = {
        let shareButton = UIButton()
        shareButton.setImage(UIImage(named: "CB_share_"), for: UIControlState())
        return shareButton
    }()
    
//    let shareCountLabel : UILabel = {
//        let shareCountLabel = UILabel()
//        shareCountLabel.text = "1024"
//        shareCountLabel.textAlignment = .Right
//        shareCountLabel.font = HSFont.baseLightFont(15)
//        shareCountLabel.textColor = UIColor.luckyPurpleTitleColor()
//        return shareCountLabel
//    }()
    
    let divideView : UIView = {
        let divideView = UIView()
        divideView.backgroundColor = UIColor.divideColor()
        return divideView
    }()
    
    func addCBSubviews() {
        self.addSubview(likeButton)
        self.addSubview(likeCountLabel)
        self.addSubview(unLikeButton)
        self.addSubview(unLikeCountLabel)
        self.addSubview(commentButton)
        self.addSubview(shareButton)
        self.addSubview(divideView)
        self.addSubview(commentCountLabel)
//        self.addSubview(shareCountLabel)
    }
    
    func addTargets() {
        self.likeButton.addTarget(self, action: #selector(likeAction), for: .touchUpInside)
        self.unLikeButton.addTarget(self, action: #selector(unlikeAction), for: .touchUpInside)
        self.commentButton.addTarget(self, action: #selector(commentAction), for: .touchUpInside)
        self.shareButton.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
    }
    func likeAction() {
        self.delegate?.didClickLike()
    }
    
    func unlikeAction() {
        self.delegate?.didClickDislike()
    }
    
    func commentAction() {
        self.delegate?.didClickComment()
    }
    
    func shareAction() {
        self.delegate?.didClickShare()
    }
    let iconHeight = 18
    let countLabelHeight = 17
    func addCBConstraint() {
        self.likeButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(SCREEN_WIDTH * 20 / 375)
            make.left.equalTo(self.snp.left).offset(SCREEN_WIDTH * 12 / 375)
            make.width.equalTo(iconHeight)
            make.height.equalTo(iconHeight)
        }
        self.likeCountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(likeButton.snp.right).offset(SCREEN_WIDTH * 12 / 375)
            make.bottom.equalTo(likeButton.snp.bottom)
            make.height.equalTo(countLabelHeight)
        }
        self.unLikeButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.likeButton.snp.top).offset(SCREEN_WIDTH * 4 / 375)
            make.left.equalTo(self.likeButton.snp.right).offset(SCREEN_WIDTH * 65/375)
            make.width.equalTo(iconHeight)
            make.height.equalTo(iconHeight)
        }
        self.unLikeCountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.likeCountLabel.snp.top)
            make.left.equalTo(self.unLikeButton.snp.right).offset(SCREEN_WIDTH * 12 / 375)
            make.height.equalTo(countLabelHeight)
        }
//        self.shareCountLabel.snp.makeConstraints { (make) in
//            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
//            make.top.equalTo(self.commentCountLabel.snp.top)
//        }
        self.shareButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
            make.top.equalTo(self.likeButton.snp.top)
            make.width.equalTo(iconHeight)
            make.height.equalTo(iconHeight)
        }
        self.commentCountLabel.snp.makeConstraints { (make) in
            make.right.equalTo(self.shareButton.snp.right).offset(-SCREEN_WIDTH * 65/375)
            make.bottom.equalTo(likeCountLabel.snp.bottom)
            make.height.equalTo(countLabelHeight)
        }
        self.commentButton.snp.makeConstraints { (make) in
            make.right.equalTo(self.commentCountLabel.snp.left).offset(-SCREEN_WIDTH * 12 / 375)
            make.top.equalTo(self.likeButton.snp.top)
            make.width.equalTo(iconHeight)
            make.height.equalTo(iconHeight)
        }
        self.divideView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(screenWidth)
            make.height.equalTo(1)
            make.top.equalTo(self.likeButton.snp.bottom).offset(SCREEN_WIDTH * 20 / 375)
        }
    }
    
    class func commentToolBarHeight() -> CGFloat{
        return SCREEN_HEIGHT * 59/667
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
