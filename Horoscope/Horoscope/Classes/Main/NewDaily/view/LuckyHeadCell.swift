//
//  LuckyHeadCell.swift
//  Horoscope
//
//  Created by Wang on 17/1/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import pop
import FBSDKShareKit
import IDMPhotoBrowser
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LuckyHeadCell: BaseCardCell,CommentStatusBarDelegate,CommentPreLookViewDelegate,CommentPreLookViewBelowDelegate,FBSDKSharingDelegate {
 
   
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    var horoName = ""

    fileprivate var timer:Timer?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        self.createUI()
        self.addConstrains()
        self.commentStatusBar.delegate = self
        self.commentPreLookView.delegate = self
        self.commentPreLookView.belowDelegate = self
        self.contentView.backgroundColor = UIColor.clear
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(goSeePost))
        self.commentPreLookView.addGestureRecognizer(tap)
    }
    
    func goSeePost() {
        if let vc1 = self.rootVC {
            let vc = FortureListViewcontroller.create(vc1.horoName ?? "")
            vc.pageType = 1
            vc.currentIndex = 1
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    weak var rootVC : HomeTimeLineViewController?

    let zodiacImage: UIImageView = {
        let backImage = UIImageView()
        return backImage
        }()
    
    let luckyNumIma:UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    let numberOfDayLab:UILabel = {
        let lab = UILabel()
        lab.text = NSLocalizedString("Lucky Number", comment: "")
        lab.textAlignment = .left
        lab.font=HSFont.baseLightFont(17)
        lab.textColor =  UIColor.luckyPurpleTitleColor()
        return lab
    }()
    
    let luckyColorLab:UILabel = {
        let lab = UILabel()
        lab.text = NSLocalizedString("Lucky color", comment: "")
        lab.textAlignment = .left
        lab.font=HSFont.baseLightFont(17)
        lab.textColor =  UIColor.luckyPurpleTitleColor()
        return lab
    }()
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.text = NSLocalizedString("Today's Horoscope", comment: "") 
        lab.textAlignment = .left
        lab.font=HSFont.baseRegularFont(20)
        lab.textColor =  UIColor.luckyPurpleTitleColor()
       
        return lab
    }()
    
    let rssTileLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.rssSourceBlueColor()
        lab.text = NSLocalizedString("Written by Tarot.com's Rick Levine", comment: "")
        lab.font = HSFont.baseRegularFont(15)
        return lab
    }()
    
    let rssIma:UIImageView = {
        let ima = UIImageView()
        ima.image = UIImage(named:"topLogo.jpg")
        return ima
    }()
    
    let hotTagLab:UILabel = {
        let lab = UILabel()
        lab.text = NSLocalizedString("Top", comment: "")
        lab.font = HSFont.baseRegularFont(18)
        lab.textColor=UIColor.white
        lab.backgroundColor = UIColor.commonPinkColor()
        lab.textAlignment = .center
        return lab 
    }()
    
    let luckWordsLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace("", space: 1.25)
        lab.textAlignment = .left
        lab.lineBreakMode = NSLineBreakMode.byTruncatingTail
        lab.font=HSFont.baseLightRegularFont(18)
        lab.textColor =  UIColor.luckyPurpleTitleColor()
        return lab
    }()
    
    let luckyColorView:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "color_")
        return view
    }()
    
    let leftHillbackView:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "transparent1_")
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let rightHillbackView:UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "transparent2_")
        return view
    }()
    
    let commentStatusBar : CommentStatusBar = {
        let commentStatusBar = CommentStatusBar(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return commentStatusBar
    }()
    
    let commentPreLookView : CommentPreLookView = {
        let commentPreLookView = CommentPreLookView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return commentPreLookView
    }()
    
//    let bottomDivideView : UIView = {
//        let bottomDivideView = UIView()
//        bottomDivideView.backgroundColor = UIColor.basePurpleBackgroundColor()
//        return bottomDivideView
//    }()
    
    func createUI() {
        self.cornerBackView?.backgroundColor = UIColor.clear
        self.cornerBackView?.addSubview(zodiacImage)
        self.cornerBackView?.addSubview(rightHillbackView)
        self.cornerBackView?.addSubview(leftHillbackView)
        self.cornerBackView?.addSubview(luckyNumIma)
        self.cornerBackView?.addSubview(numberOfDayLab)
        self.cornerBackView?.addSubview(luckyColorLab)
        self.cornerBackView?.addSubview(luckWordsLab)
        self.cornerBackView?.addSubview(titleLab)
        self.cornerBackView?.addSubview(rssTileLab)
        self.cornerBackView?.addSubview(luckyColorView)
        self.cornerBackView?.addSubview(commentStatusBar)
        self.cornerBackView?.addSubview(commentPreLookView)
        self.cornerBackView?.addSubview(hotTagLab)
        self.cornerBackView?.addSubview(rssIma)
//        self.cornerBackView?.addSubview(bottomDivideView)
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(horoDidSelect))
        zodiacImage.isUserInteractionEnabled = true
        zodiacImage.addGestureRecognizer(gesture1)
       
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(todayHoroscopeDidSelect))
        luckWordsLab.isUserInteractionEnabled = true
        luckWordsLab.addGestureRecognizer(gesture2)
        
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(luckyNumberSelect))
        luckyNumIma.isUserInteractionEnabled = true
        luckyNumIma.addGestureRecognizer(gesture3)
        
        let gesture4 = UITapGestureRecognizer(target: self, action: #selector(luckyColorSelect))
        luckyColorLab.isUserInteractionEnabled = true
        luckyColorLab.addGestureRecognizer(gesture4)
        
        luckyColorView.isUserInteractionEnabled = true
        luckyColorView.addGestureRecognizer(gesture4)
        
        let rssImatap = UITapGestureRecognizer()
        rssImatap.addTarget(self, action: #selector(imaViewRssURL))
        self.rssIma.addGestureRecognizer(rssImatap)
        self.rssIma.isUserInteractionEnabled = true
        
        let rssAuthortap = UITapGestureRecognizer()
        rssAuthortap.addTarget(self, action: #selector(authorRssTap))
        self.rssTileLab.addGestureRecognizer(rssAuthortap)
        self.rssTileLab.isUserInteractionEnabled = true
        self.rssTileLab.addGestureRecognizer(rssAuthortap)
    }
    
    func imaViewRssURL()  {
         UIApplication.shared.openURL(URL(string: "https://www.tarot.com/?code=dailyinnovations&utm_medium=Referral&utm_source=DailyInnovations&utm_campaign=daily_app&utm_content=logo")!)
    }
    
    func authorRssTap()  {
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/bios/rick-levine/?code=dailyinnovations&utm_medium=Referral&utm_source=DailyInnovations&utm_campaign=daily_app&utm_content=rick-levine-bio")!)
    }
    
    func luckyNumberSelect() {
         AnaliticsManager.sendEvent(AnaliticsManager.main_card_click, data: ["category":"\(AnaliticsManager.lucky_number)"])
    }
    
    func luckyColorSelect() {
         AnaliticsManager.sendEvent(AnaliticsManager.main_card_click, data: ["category":"\(AnaliticsManager.lucky_color)"])
    }
    
    func horoDidSelect(){
        AnaliticsManager.sendEvent(AnaliticsManager.main_card_click, data: ["category":"\(AnaliticsManager.character_dailog)"])
        if let vc1 = self.rootVC {
            let vc = CharacterListViewController.create(vc1.horoName ?? "")
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func todayHoroscopeDidSelect()  {
        AnaliticsManager.sendEvent(AnaliticsManager.main_card_click, data: ["category":"\(AnaliticsManager.today_horoscope)"])
        if let vc1 = self.rootVC {
            let vc = FortureListViewcontroller.create(vc1.horoName ?? "")
            vc.pageType = 1
            vc.currentIndex = 1
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static let cardHeight = 120
    func addConstrains() {
 
        self.contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 10, 0))
        }
 
        self.zodiacImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top)
            make.left.equalTo(self.snp.left).offset(SCREEN_WIDTH * 5 / 375)
            make.right.equalTo(self.snp.right).offset(-SCREEN_WIDTH * 5 / 375)
            make.bottom.equalTo(self.luckyNumIma.snp.top).offset(-SCREEN_HEIGHT * 5 / 667)
        }
        
        self.numberOfDayLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left).offset(SCREEN_WIDTH * 12 / 375)
            make.top.equalTo(self.snp.top).offset(SCREEN_HEIGHT * 518/667)
        }
        
        self.luckyNumIma.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.numberOfDayLab.snp.centerX)
            make.height.equalTo(72/667*screenHeight)
            make.width.equalTo(72/667*screenHeight)
            make.bottom.equalTo(numberOfDayLab.snp.top).offset(-SCREEN_HEIGHT * 30 / 667)
        }
        
        self.luckyColorLab.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.numberOfDayLab.snp.centerY)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-SCREEN_WIDTH * 12 / 375)
        }
        
        self.luckyColorView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.luckyColorLab.snp.top).offset(-SCREEN_HEIGHT * 8 / 667)
            make.centerX.equalTo(self.luckyColorLab.snp.centerX)
            make.height.equalTo(7/375*screenWidth)
            make.width.equalTo(64/667*screenHeight)
        }
        
        self.rssIma.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.hotTagLab.snp.centerY)
            make.right.equalTo(luckyColorLab.snp.right)
        }
        
        self.hotTagLab.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.titleLab.snp.top).offset(-SCREEN_HEIGHT * 20 / 667)
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.top.equalTo(numberOfDayLab.snp.bottom).offset(SCREEN_HEIGHT * 20 / 667)
            make.width.equalTo(SCREEN_WIDTH * 48 / 375)
            make.height.equalTo(SCREEN_HEIGHT * 20 / 667)
        }
        
        self.titleLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.hotTagLab.snp.bottom).offset(SCREEN_HEIGHT * 5 / 667)
            make.left.equalTo(self.numberOfDayLab.snp.left)
            make.right.equalTo(self.luckyColorLab.snp.right)
        }
        
        self.rssTileLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLab.snp.bottom).offset(SCREEN_HEIGHT * 6 / 667)
            make.left.equalTo(self.numberOfDayLab.snp.left)
            make.right.equalTo(self.luckyColorLab.snp.right)
        }
        
        self.luckWordsLab.snp.makeConstraints {(make) in
            make.top.equalTo(self.rssTileLab.snp.bottom)
            make.left.equalTo(self.numberOfDayLab.snp.left)
            make.right.equalTo(self.luckyColorLab.snp.right)
            make.height.equalTo(SCREEN_HEIGHT * 60 / 667)
        }
        
        self.leftHillbackView.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right)
            make.top.equalTo(self.luckyNumIma.snp.top).offset(SCREEN_HEIGHT * 22 / 667)
            make.bottom.equalTo(self.cornerBackView!.snp.bottom)
        }
        
        self.rightHillbackView.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right)
            make.top.equalTo(self.leftHillbackView.snp.top).offset(SCREEN_HEIGHT * 25 / 667)
            make.bottom.equalTo(self.cornerBackView!.snp.bottom)
        }
        
        let commentToolBarHeight = CommentStatusBar.commentToolBarHeight()
        self.commentStatusBar.snp.makeConstraints { (make) in
            make.top.equalTo(self.luckWordsLab.snp.bottom)
            make.height.equalTo(SCREEN_HEIGHT * commentToolBarHeight/667)
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right)
        }
        self.commentPreLookView.snp.makeConstraints { (make) in
            make.top.equalTo(self.commentStatusBar.snp.bottom).offset(SCREEN_HEIGHT * 20 / 667)
            make.left.equalTo(self.commentStatusBar.snp.left)
            make.right.equalTo(self.commentStatusBar.snp.right)
            make.height.equalTo(2000)
        }
//        self.bottomDivideView.snp.makeConstraints { (make) in
//            make.top.equalTo(self.snp.bottom)
//            make.height.equalTo(10)
//            make.width.equalTo(screenWidth)
//            make.centerX.equalTo(self.snp.centerX)
//        }
    }
    
    var model:LuckyModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        if model != nil {
            self.luckyNumIma.image = UIImage(named: "number_\(model?.luckNumber ?? "")")
            LXSWLog("number_\(model!.luckNumber ?? "")")
            let colorStr = (model!.luckColorValue! as NSString).substring(from: 2)
            
            self.luckyColorView.tintColor = UIColor(hexString: colorStr, withAlpha: 1)
            LXSWLog(colorStr)
//            self.zodiacImage.image = UIImage(named: "back\(model!.zodiacName!)")
//            LXSWLog("back\(model!.zodiacName!)")
            //
            //         self.luckyNumLab.text =  "\(model?.luckNumber ?? "")"
            //         self.titleLab.text = "Today Horoscope"
            self.luckWordsLab.attributedText=HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: model?.content ?? "" , font: HSFont.baseLightRegularFont(18))
            self.commentStatusBar.likeCountLabel.text = "\(model?.likeCount ?? 0)"
            if self.commentStatusBar.likeCountLabel.text == "0"{
                self.commentStatusBar.likeCountLabel.isHidden = true
            }else {
                self.commentStatusBar.likeCountLabel.isHidden = false
            }
            if model?.isLiked == true {
                self.commentStatusBar.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                self.commentStatusBar.likeCountLabel.textColor = UIColor.commonPinkColor()
            }else {
                self.commentStatusBar.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                self.commentStatusBar.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
            }
            
            self.commentStatusBar.unLikeCountLabel.text = "\(model?.dislikeCount ?? 0)"
            if self.commentStatusBar.unLikeCountLabel.text == "0"{
                self.commentStatusBar.unLikeCountLabel.isHidden = true
            }else {
                self.commentStatusBar.unLikeCountLabel.isHidden = false
            }
            if model?.isDisliked == true {
                self.commentStatusBar.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                self.commentStatusBar.unLikeCountLabel.textColor = UIColor.commonPinkColor()
            }
            else {
                self.commentStatusBar.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                self.commentStatusBar.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
            }
            
//            self.commentStatusBar.shareCountLabel.text = "\(model?.shareCount ?? 0)"
//            if self.commentStatusBar.shareCountLabel.text == "0"{
//                self.commentStatusBar.shareCountLabel.hidden = true
//            }else {
//                self.commentStatusBar.shareCountLabel.hidden = false
//            }
            
            self.commentStatusBar.commentCountLabel.text = "\(model?.commentCount ?? 0)"
            if self.commentStatusBar.commentCountLabel.text == "0"{
                self.commentStatusBar.commentCountLabel.isHidden = true
            }else {
                self.commentStatusBar.commentCountLabel.isHidden = false
            }
//            self.commentStatusBar.shareCountLabel.text = "\(model?.shareCount ?? 0)"
//            if self.commentStatusBar.shareCountLabel.text == "0"{
//                self.commentStatusBar.shareCountLabel.hidden = true
//            }else {
//                self.commentStatusBar.shareCountLabel.hidden = false
//            }
            
            let count = model?.commentList?.count ?? 0
            if count > 0 {
                for sub in 0..<count{
                    if sub == 0 {
                        self.commentPreLookView.nameLabel.text = model?.commentList?[sub].userInfo?.name ?? ""
                        self.commentPreLookView.signLabel.text = model?.commentList?[sub].userInfo?.horoscopeName ?? ""
                        let headerUrl = URL(string: model?.commentList?[sub].userInfo?.avatar ?? "")
                        self.commentPreLookView.userAva.sd_setImage(with: headerUrl, placeholderImage: UIImage(named: "defaultImg1.jpg"))
                        self.commentPreLookView.createTime.text = model?.commentList?[sub].createTimeHuman ?? ""
                        
                        let topComment = model?.commentList?[sub].content ?? ""
                        self.commentPreLookView.contentLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: topComment, font: HSFont.baseRegularFont(15))
                        self.commentPreLookView.likeCountLabel.text = "\(model?.commentList?[sub].likeCount ?? 0)"
//                        self.commentPreLookView.likeCountLabel.hidden = self.commentPreLookView.likeCountLabel.text == "0" ? true : false
                        if model?.commentList?[sub].isLiked == true {
                            self.commentPreLookView.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                            self.commentPreLookView.likeCountLabel.textColor = UIColor.commonPinkColor()
                        }else {
                            self.commentPreLookView.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                            self.commentPreLookView.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                        }
                        self.commentPreLookView.unLikeCountLabel.text = "\(model?.commentList?[sub].dislikeCount ?? 0)"
//                        self.commentPreLookView.unLikeCountLabel.hidden = self.commentPreLookView.unLikeCountLabel.text == "0" ? true : false
                        if model?.commentList?[sub].isDisliked == true {
                            self.commentPreLookView.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                            self.commentPreLookView.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                        }else {
                            self.commentPreLookView.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                            self.commentPreLookView.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                        }
                        if model?.commentList?[sub].imageList?.count>0 {
                            self.commentPreLookView.viewImageButton.isHidden = false
                            self.commentPreLookView.viewImgLabel.isHidden = false
                            self.commentPreLookView.viewImageButton.snp.updateConstraints({ (make) in
                                make.height.equalTo(SCREEN_WIDTH * 20/375)
                            })
                        }else {
                            self.commentPreLookView.viewImageButton.isHidden = true
                            self.commentPreLookView.viewImgLabel.isHidden = true
                            self.commentPreLookView.viewImageButton.snp.updateConstraints({ (make) in
                                make.height.equalTo(0)
                            })
                        }
                    }else if sub == 1 {
                        self.commentPreLookView.nameLabelBelow.text = model?.commentList?[sub].userInfo?.name ?? ""
                        self.commentPreLookView.signLabelBelow.text = model?.commentList?[sub].userInfo?.horoscopeName ?? ""
                        let headerUrl = URL(string: model?.commentList?[sub].userInfo?.avatar ?? "")
                        self.commentPreLookView.userAvaBelow.sd_setImage(with: headerUrl, placeholderImage: UIImage(named: "defaultImg1.jpg"))
                        self.commentPreLookView.createTimeBelow.text = model?.commentList?[sub].createTimeHuman ?? ""
                        
                        let topComment = model?.commentList?[sub].content ?? ""
                        self.commentPreLookView.contentLabelBelow.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: topComment, font: HSFont.baseRegularFont(15))
                        self.commentPreLookView.likeCountLabelBelow.text = "\(model?.commentList?[sub].likeCount ?? 0)"
//                        self.commentPreLookView.likeCountLabelBelow.hidden = self.commentPreLookView.likeCountLabelBelow.text == "0" ? true : false
                        if model?.commentList?[sub].isLiked == true {
                            self.commentPreLookView.likeButtonBelow.setImage(UIImage(named: "praise2_"), for: UIControlState())
                            self.commentPreLookView.likeCountLabelBelow.textColor = UIColor.commonPinkColor()
                        }else {
                            self.commentPreLookView.likeButtonBelow.setImage(UIImage(named: "praise_"), for: UIControlState())
                            self.commentPreLookView.likeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                        }
                        self.commentPreLookView.unLikeCountLabelBelow.text = "\(model?.commentList?[sub].dislikeCount ?? 0)"
//                        self.commentPreLookView.unLikeCountLabelBelow.hidden = self.commentPreLookView.unLikeCountLabelBelow.text == "0" ? true : false
                        if model?.commentList?[sub].isDisliked == true {
                            self.commentPreLookView.unLikeButtonBelow.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                            self.commentPreLookView.unLikeCountLabelBelow.textColor = UIColor.commonPinkColor()
                        }else {
                            self.commentPreLookView.unLikeButtonBelow.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                            self.commentPreLookView.unLikeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                        }
                        if model?.commentList?[sub].imageList?.count > 0 {
                            self.commentPreLookView.viewImageButtonBelow.isHidden = false
                            self.commentPreLookView.viewImgLabelBelow.isHidden = false
                            self.commentPreLookView.viewImageButtonBelow.snp.updateConstraints({ (make) in
                                make.height.equalTo(SCREEN_WIDTH * 20/375)
                            })
                        }else {
                            self.commentPreLookView.viewImageButtonBelow.isHidden = true
                            self.commentPreLookView.viewImgLabelBelow.isHidden = true
                            self.commentPreLookView.viewImageButtonBelow.snp.updateConstraints({ (make) in
                                make.height.equalTo(0)
                            })
                        }
                        let commentCount = self.model?.commentCount ?? 0
                        if commentCount > 2 {
                            self.commentPreLookView.viewAllCommentLabel.isHidden = false
                        }else {
                            self.commentPreLookView.viewAllCommentLabel.isHidden = true
                        }
                    }
                }
            }
        }
    }
    
    func didClickComment() {
        
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let name = self.rootVC?.horoName ?? ""
            let fortuneListVC:FortureListViewcontroller = FortureListViewcontroller.create(name)
            fortuneListVC.pageType = 1
            fortuneListVC.currentIndex = 1
            self.rootVC?.navigationController?.pushViewController(fortuneListVC, animated: true)
//            let postVC = PostViewController()
//            postVC.isFirstClassReply = true
//            postVC.isForcastReply = true
//            postVC.horoscopeName = self.horoName
//            postVC.forecastName = "today"
//            let postId = self.model?.postId ?? ""
//            let model = MyPostModel(postId: postId)
//            let readModel = ReadPostModel.init(model: model)
//            postVC.postModel = readModel
//            self.rootVC?.navigationController?.pushViewController(postVC, animated: true)
        }
    }
    
    func didClickDislike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.isDisliked == false {
                let newCount = (self.model?.dislikeCount ?? 0) + 1
                self.model?.dislikeCount = newCount
                self.model?.isDisliked = true
                self.commentStatusBar.unLikeCountLabel.isHidden = false
                self.renderCell()
                self.commentStatusBar.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                self.commentStatusBar.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: true, id: self.model?.postId ?? "")
            }else if self.model?.isDisliked == true{
                var newCount = (self.model?.dislikeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
                    self.commentStatusBar.unLikeCountLabel.isHidden = true
                }
                self.model?.dislikeCount = newCount
                self.model?.isDisliked = false
                self.renderCell()
                self.commentStatusBar.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                self.commentStatusBar.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: false, id: self.model?.postId ?? "")
            }
        }
    }
    
    func didClickShare() {
        let content = FBSDKShareLinkContent.init()
        content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_FORECAST)
        FBSDKShareDialog.show(from: self.rootVC, with: content, delegate: self)
        HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "share", value: true, id: self.model?.postId ?? "")
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        let newCount = (self.model?.shareCount ?? 0) + 1
        self.model?.shareCount = newCount
        self.renderCell()
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        return
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!){
        print(error)
    }
    
    func didClickLike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.isLiked == false {
                let newCount = (self.model?.likeCount ?? 0) + 1
                self.model?.likeCount = newCount
                self.model?.isLiked = true
                self.commentStatusBar.likeCountLabel.isHidden = false
                self.renderCell()
                self.commentStatusBar.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                self.commentStatusBar.likeCountLabel.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: true, id: self.model?.postId ?? "")
            }else if self.model?.isLiked == true{
                var newCount = (self.model?.likeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
                    self.commentStatusBar.likeCountLabel.isHidden = true
                }
                self.model?.likeCount = newCount
                self.model?.isLiked = false
                self.renderCell()
                self.commentStatusBar.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                self.commentStatusBar.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: false, id: self.model?.postId ?? "")
            }
        }
    }
    
 
    func preLookDidClickLike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.commentList?.first?.isLiked == false {
                let newCount = (self.model?.commentList?.first?.likeCount ?? 0) + 1
                self.model?.commentList?.first?.likeCount = newCount
                self.model?.commentList?.first?.isLiked = true
//                self.commentPreLookView.likeCountLabel.hidden = false
                self.renderCell()
                self.commentPreLookView.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                self.commentPreLookView.likeCountLabel.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: self.model?.commentList?.first?.commentId ?? "")
            }else if self.model?.commentList?.first?.isLiked == true{
                var newCount = (self.model?.commentList?.first?.likeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
//                    self.commentPreLookView.likeCountLabel.hidden = true
                }
                self.model?.commentList?.first?.likeCount = newCount
                self.model?.commentList?.first?.isLiked = false
                self.renderCell()
                self.commentPreLookView.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                self.commentPreLookView.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: self.model?.commentList?.first?.commentId ?? "")
            }
        }
    }
    func preLookDidClickDislike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.commentList?.first?.isDisliked == false {
                let newCount = (self.model?.commentList?.first?.dislikeCount ?? 0) + 1
                self.model?.commentList?.first?.dislikeCount = newCount
                self.model?.commentList?.first?.isDisliked = true
//                self.commentPreLookView.unLikeCountLabel.hidden = false
                self.renderCell()
                self.commentPreLookView.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                self.commentPreLookView.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: self.model?.commentList?.first?.commentId ?? "")
            }else if self.model?.commentList?.first?.isDisliked == true{
                var newCount = (self.model?.commentList?.first?.dislikeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
//                    self.commentPreLookView.unLikeCountLabel.hidden = true
                }
                self.model?.commentList?.first?.dislikeCount = newCount
                self.model?.commentList?.first?.isDisliked = false
                self.renderCell()
                self.commentPreLookView.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                self.commentPreLookView.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: self.model?.commentList?.first?.commentId ?? "")
            }
        }
    }
    func preLookBelowDidClickLike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.commentList?[1].isLiked == false {
                let newCount = (self.model?.commentList?[1].likeCount ?? 0) + 1
                self.model?.commentList?[1].likeCount = newCount
                self.model?.commentList?[1].isLiked = true
//                self.commentPreLookView.likeCountLabelBelow.hidden = false
                self.renderCell()
                self.commentPreLookView.likeButtonBelow.setImage(UIImage(named: "praise2_"), for: UIControlState())
                self.commentPreLookView.likeCountLabelBelow.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: self.model?.commentList?[1].commentId ?? "")
            }else if self.model?.commentList?[1].isLiked == true{
                var newCount = (self.model?.commentList?[1].likeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
//                    self.commentPreLookView.likeCountLabelBelow.hidden = true
                }
                self.model?.commentList?[1].likeCount = newCount
                self.model?.commentList?[1].isLiked = false
                self.renderCell()
                self.commentPreLookView.likeButtonBelow.setImage(UIImage(named: "praise_"), for: UIControlState())
                self.commentPreLookView.likeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: self.model?.commentList?[1].commentId ?? "")
            }
        }
    }
    func preLookBelowDidClickDislike() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.commentList?[1].isDisliked == false {
                let newCount = (self.model?.commentList?[1].dislikeCount ?? 0) + 1
                self.model?.commentList?[1].dislikeCount = newCount
                self.model?.commentList?[1].isDisliked = true
//                self.commentPreLookView.unLikeCountLabelBelow.hidden = false
                self.renderCell()
                self.commentPreLookView.unLikeButtonBelow.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                self.commentPreLookView.unLikeCountLabelBelow.textColor = UIColor.commonPinkColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: self.model?.commentList?[1].commentId ?? "")
            }else if self.model?.commentList?[1].isDisliked == true{
                var newCount = (self.model?.commentList?[1].dislikeCount ?? 0) - 1
                if newCount <= 0 {
                    newCount = 0
//                    self.commentPreLookView.unLikeCountLabelBelow.hidden = true
                }
                self.model?.commentList?[1].dislikeCount = newCount
                self.model?.commentList?[1].isDisliked = false
                self.renderCell()
                self.commentPreLookView.unLikeButtonBelow.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                self.commentPreLookView.unLikeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: self.model?.commentList?[1].commentId ?? "")
            }
        }
    }
    
    func prelookDidClickViewImg() {
        let imaName = self.model?.commentList?.first?.imageList?.first ?? ""
        let imgUrl = URL(string: imaName)
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.rootVC?.present(brower!, animated: true, completion: nil)
    }
    
    func prelookBelowDidClickViewImg() {
        let imaName = self.model?.commentList?[1].imageList?.first ?? ""
        let imgUrl = URL(string: imaName)
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.rootVC?.present(brower!, animated: true, completion: nil)
    }
    
    func seeOriginalTapAction() {
        if let vc1 = self.rootVC {
            let vc = FortureListViewcontroller.create(vc1.horoName ?? "")
            vc.pageType = 1
            vc.currentIndex = 1
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static let cellId="LuckyHeadCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> LuckyHeadCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! LuckyHeadCell
        return cell
    }
    
    class func calculateHeight(_ content:String) ->CGFloat{
        return SCREEN_WIDTH == 320 ? SCREEN_HEIGHT + SCREEN_HEIGHT * 125/667 : SCREEN_HEIGHT + SCREEN_HEIGHT * 105/667
    }
    
    deinit {
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AppDelegate.StartApp"), object: nil)
    }
    
    
}
