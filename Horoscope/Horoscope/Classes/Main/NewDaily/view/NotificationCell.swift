//
//  NotificationCell.swift
//  Horoscope
//
//  Created by xiao  on 17/1/18.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

protocol NotificationCellDelegate:class {
    func notificationCellDidClick()
}
class NotificationCell: BaseCardCell {
    weak var rootVc:UIViewController?
    weak var delegate:NotificationCellDelegate?
    func createUI() {
        self.contentView.backgroundColor = UIColor.baseCardBackgroundColor()
        self.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.cornerBackView?.backgroundColor = UIColor.clear
        self.cornerBackView?.addSubview(bgImg)
        self.cornerBackView?.addSubview(titleLab)
    }
    
    let bgImg:UIImageView = {
        let view = UIImageView()
        view.backgroundColor=UIColor.clear
        return view
    }()
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.textAlignment = .left
        lab.font=HSFont.baseRegularFont(20)
        lab.textColor = UIColor.purpleTitleColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    var model:CardCampaignModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell(){
        if self.model != nil {
            if let authorImaStr = model?.figure {
                bgImg.sd_setImage(with: URL(string:authorImaStr), placeholderImage: UIImage(named:"defaultImg1.jpg"))
            }
            self.titleLab.text = self.model?.title
        }
    }
    
    func addConstrains() {
        self.contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 10, 0))
        }
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.contentView.snp.right).offset(-BaseCardCell.cellPadding)
            make.top.equalTo(self.contentView.snp.top).offset(BaseCardCell.cellPadding)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        bgImg.snp.makeConstraints { (make) in
            make.top.equalTo(self.cornerBackView!.snp.top)
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right)
            make.height.equalTo(BaseHomeCell.imgHeight)
        }
        
        titleLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.bgImg.snp.bottom).offset(12)
            make.left.equalTo(bgImg.snp.left)
            make.right.equalTo(bgImg.snp.right)
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(cellDidSelect))
        self.addGestureRecognizer(gesture)
        self.createUI()
        self.addConstrains()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //FIXME:noti
    func cellDidSelect(){
        if self.delegate != nil {
            delegate?.notificationCellDidClick()
        }
    }
    
    //MARK:Dequeuse
    static let cellId="notificationCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> NotificationCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! NotificationCell
        return cell
    }

    static let height = (UIScreen.main.bounds.size.width-BaseCardCell.cellPadding*2)/258 * 145 + 85
    static let imgHeight = (UIScreen.main.bounds.size.width-BaseCardCell.cellPadding*2)/16 * 9

    class func calculateHeight(_ title:String?) ->CGFloat{
        let titleHeight=HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: title ?? "", fontSize: 20, font: HSFont.baseRegularFont(20), width: UIScreen.main.bounds.size.width-20, multyplyLineSpace: 1)
        return (titleHeight + imgHeight + BaseCardCell.cellPadding + 22 + 10)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
