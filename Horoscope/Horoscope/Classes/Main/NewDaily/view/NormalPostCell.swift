//
//  NormalPostCell.swift
//  Horoscope
//
//  Created by Wang on 17/2/20.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit
import IDMPhotoBrowser
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@objc protocol NormalPostCellDelegate:class {
    func praiseOneNormalTopic(_ name:String,action:String,value:Bool,id:String)
    func tradeOneNormalTopic(_ name:String,action:String,value:Bool,id:String)
    @objc optional func shouldChangeCellHeight(_ rowNum:Int)
 }

class NormalPostCell: BaseCardCell {
    weak var delegate:NormalPostCellDelegate?
    weak var rootVc:UIViewController?
    var shouldShowAllTag : Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
     // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.createUI()
        self.addConstrains()
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(goUserPostList))
        self.userAvaIma.addGestureRecognizer(tap)
        self.userAvaIma.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:BtnClick
    func shareBtnClick() {
        let shareImgUrl = self.readModel?.imageList?.first as? String ?? ""
        let content = FBSDKShareLinkContent.init()
        if self.figureIma.image != nil {
            content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_WITH_IMG + shareImgUrl)
        }else{
            content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_WITHOUT_IMG)
        }
        FBSDKShareDialog.show(from: self.rootVc, with: content, delegate: nil)
    }
    
    func praiseBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.readModel?.iliked == false{
                let newCount = (readModel?.likeCount ?? 0) + 1
                self.readModel?.likeCount = newCount
                self.readModel?.iliked = true
                self.renderCell()
                self.delegate?.praiseOneNormalTopic("post", action: "like", value: true, id: readModel?.postId ?? "" )
            }
            else  if self.readModel?.iliked == true {
                var newCount = (readModel?.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.readModel?.likeCount = newCount
                self.readModel?.iliked = false
                self.renderCell()
                self.delegate?.praiseOneNormalTopic("post", action: "like", value: false, id: readModel?.postId ?? "" )
            }
        }
    }
    
    func treadBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if  self.readModel?.iDisliked == false{
                self.readModel?.iDisliked = true
                let newCount = (readModel?.dislikeCount ?? 0) + 1
                self.readModel?.dislikeCount = newCount
                self.renderCell()
                self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: true, id: readModel?.postId ?? "" )
            }
            else if  self.readModel?.iDisliked == true{
                self.readModel?.iDisliked = false
                var newCount = (readModel?.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.readModel?.dislikeCount = newCount
                self.renderCell()
                self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: false, id: readModel?.postId ?? "" )
            }
        }
    }
    func commentBtnClick()  {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = PostViewController()
            vc.isFirstClassReply = true
            // vc.postModel = MyPostModel.ini
            vc.replyModel = MyPostModel.init(model: readModel)
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func goUserPostList() {
        if self.readModel?.user?.userId ?? "" == "" || (self.readModel?.user?.userId ?? "").characters.count == 0  {
            return
        }
        let vc = ReadOtherUserPostViewController()
        vc.userId = self.readModel?.user?.userId ?? ""
        vc.userName = self.readModel?.user?.name ?? ""
        self.rootVc?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func createUI() {
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.cornerBackView?.addSubview(userNameLabe)
        self.cornerBackView?.addSubview(userAvaIma)
        self.cornerBackView?.addSubview(horoLab)
        self.cornerBackView?.addSubview(timeLab)
        self.cornerBackView?.addSubview(contentLab)
        self.cornerBackView?.addSubview(figureIma)
        self.cornerBackView?.addSubview(praiseBtn)
        self.cornerBackView?.addSubview(praiseLab)
        self.cornerBackView?.addSubview(treadBtn)
        self.cornerBackView?.addSubview(treadNumLab)
        self.cornerBackView?.addSubview(commentBtn)
        self.cornerBackView?.addSubview(shareBtn)
        self.cornerBackView?.addSubview(tagListView)
        self.cornerBackView?.addSubview(contentLine)
        self.cornerBackView?.addSubview(commentNumLab)
        self.cornerBackView?.addSubview(tempTagView)
        tempTagView.addSubview(self.firstTagLabel)
        tempTagView.addSubview(self.seeAllTagLabel)
        
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(viewImage))
        self.figureIma.addGestureRecognizer(tap)
        self.figureIma.isUserInteractionEnabled = true
        
        let seeAllTagTap = UITapGestureRecognizer()
        seeAllTagTap.addTarget(self, action: #selector(seeAllTagOnClick))
        self.seeAllTagLabel.addGestureRecognizer(seeAllTagTap)
        self.seeAllTagLabel.isUserInteractionEnabled = true
        
        shareBtn.addTarget(self, action: #selector(shareBtnClick), for: .touchUpInside)
        commentBtn.addTarget(self, action: #selector(commentBtnClick), for: .touchUpInside)
        praiseBtn.addTarget(self, action: #selector(praiseBtnClick), for: .touchUpInside)
        treadBtn.addTarget(self, action: #selector(treadBtnClick), for: .touchUpInside)
    }
    
    func viewImage() {
        let imaName = self.readModel?.imageList?.first as? String
        let imgUrl = URL(string: imaName ?? "")
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr, animatedFrom: self.figureIma)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.rootVc?.present(brower!, animated: true, completion: nil)
//        let bigImgView = ViewBigImage(frame: UIScreen.mainScreen().bounds, image: self.figureIma.image!)
//        self.rootVc?.view.addSubview(bigImgView)
    }
    
     func addConstrains() {
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-10)
        }
        
        userAvaIma.snp.makeConstraints { (make) in
            make.left.equalTo(cornerBackView!.snp.left).offset(12)
            make.top.equalTo(cornerBackView!.snp.top).offset(20)
            make.width.height.equalTo(44)
        }
        
        userNameLabe.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.right).offset(15)
            make.top.equalTo(userAvaIma.snp.top)
            make.width.equalTo(SCREEN_WIDTH/2)
        }
        
        horoLab.snp.makeConstraints { (make) in
            make.left.equalTo(userNameLabe.snp.left)
            make.bottom.equalTo(userAvaIma.snp.bottom)
        }
        
        timeLab.snp.makeConstraints { (make) in
            make.right.equalTo(contentLab.snp.right)
            make.bottom.equalTo(self.userNameLabe.snp.bottom)
        }
        
        contentLine.snp.makeConstraints { (make) in
            make.left.equalTo(cornerBackView!.snp.left)
            make.top.equalTo(userAvaIma.snp.bottom).offset(20)
            make.height.equalTo(1)
            make.right.equalTo(cornerBackView!.snp.right)
        }
        
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.top.equalTo(contentLine.snp.bottom).offset(10)
        }
        
        figureIma.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.left)
            make.right.equalTo(self.contentLab.snp.right)
            make.top.equalTo(contentLab.snp.bottom).offset(20)
            let figureHeight = (SCREEN_WIDTH - 24)
            make.height.equalTo(figureHeight)
        }
        
        praiseBtn.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.left).offset(1)
            make.bottom.equalTo(cornerBackView!.snp.bottom).offset(-20)
            make.height.equalTo(18)
            make.width.equalTo(18)
        }
        
        praiseLab.snp.makeConstraints { (make) in
            make.left.equalTo(praiseBtn.snp.right).offset(10)
            make.bottom.equalTo(praiseBtn.snp.bottom)
        }
        
        treadBtn.snp.makeConstraints { (make) in
            make.left.equalTo(praiseLab.snp.right).offset(30)
            make.bottom.equalTo(praiseLab.snp.bottom).offset(3)
            make.height.equalTo(18)
            make.width.equalTo(18)
        }
        
        treadNumLab.snp.makeConstraints { (make) in
            make.left.equalTo(treadBtn.snp.right).offset(10)
            make.bottom.equalTo(praiseLab.snp.bottom)
        }
        
        commentNumLab.snp.makeConstraints { (make) in
            make.left.equalTo(commentBtn.snp.right).offset(10)
            make.bottom.equalTo(commentBtn.snp.bottom)
        }
        
        shareBtn.snp.makeConstraints { (make) in
            make.right.equalTo(contentLab.snp.right)
            make.bottom.equalTo(praiseBtn.snp.bottom)
            make.width.height.equalTo(18)
        }
        
        commentBtn.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.centerX).offset(50)
            make.bottom.equalTo(shareBtn.snp.bottom)
            make.width.height.equalTo(18)
        }
        
        tagListView.snp.makeConstraints { (make) in
            make.left.equalTo(self.userAvaIma.snp.left).offset(-2)
            make.right.equalTo(self.contentLab.snp.right)
            make.top.equalTo(self.figureIma.snp.bottom).offset(10)
            make.height.equalTo(0)
        }
        
        tempTagView.snp.makeConstraints { (make) in
            make.top.equalTo(figureIma.snp.bottom).offset(10)
            make.left.equalTo(self.userAvaIma.snp.left)
            make.right.equalTo(self.contentLab.snp.right)
            make.height.equalTo(20)
        }
        firstTagLabel.snp.makeConstraints { (make) in
            make.top.equalTo(tempTagView.snp.top)
            make.left.equalTo(tempTagView.snp.left)
        }
        seeAllTagLabel.snp.makeConstraints { (make) in
            make.top.equalTo(firstTagLabel.snp.top)
            make.left.equalTo(firstTagLabel.snp.right).offset(5)
        }
    }
 
    var readModel:ReadPostModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        for arr in tagListView.subviews {
            arr.removeFromSuperview()
        }
        if readModel?.iliked == true{
            praiseBtn.setImage(UIImage(named:"praise2_"), for: UIControlState())
        }else{
            praiseBtn.setImage(UIImage(named:"praise_"), for: UIControlState())
        }
        
        if readModel?.iDisliked == true{
            treadBtn.setImage(UIImage(named:"dislike_pink_"), for: UIControlState())
        }else{
            treadBtn.setImage(UIImage(named:"Step-on_"), for: UIControlState())
        }
        
        self.userAvaIma.sd_setImage(with: URL(string: "\(readModel?.user?.avatar ?? "")"), placeholderImage: UIImage(named:"icon_author"))
        self.userNameLabe.text = readModel?.user?.name
        self.contentLab.attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.5, textColor: UIColor.purpleContentColor(), targetStr: readModel?.content ?? "", font: HSFont.baseRegularFont(18))
        self.horoLab.text = "\(readModel?.user?.horoscopeName?.capitalized ?? "") "
        self.timeLab.text =  HSHelpCenter.sharedInstance.dateTool.calculateTimeInterval(Double(readModel?.createTime ?? 0))
        
        if readModel?.likeCount != 0 {
        self.praiseLab.isHidden = false
        self.praiseLab.text = "\(self.readModel?.likeCount ?? 0)"
        }else {
        self.praiseLab.isHidden = true
        }
        if readModel?.dislikeCount != 0{
        self.treadNumLab.isHidden = false
        self.treadNumLab.text="\(HSHelpCenter.sharedInstance.textTool.getFormattedNumber(readModel?.dislikeCount ?? 0))"
        }else {
        self.treadNumLab.isHidden = true
        self.treadNumLab.text="\(HSHelpCenter.sharedInstance.textTool.getFormattedNumber(readModel?.dislikeCount ?? 0))"
        }
        if readModel?.commentCount != 0{
            self.commentNumLab.isHidden = false
            self.commentNumLab.text="\(HSHelpCenter.sharedInstance.textTool.getFormattedNumber(readModel?.commentCount ?? 0))"
        }else {
            self.commentNumLab.isHidden = true
            self.commentNumLab.text="\(HSHelpCenter.sharedInstance.textTool.getFormattedNumber(readModel?.commentCount ?? 0))"
        }
        
        if self.readModel?.imageList?.count != 0{
            let imaName = self.readModel?.imageList?.first as? String
            self.figureIma.sd_setImage(with: URL(string:"\(imaName ?? "")"), placeholderImage: UIImage(named:"defaultImg1.jpg"))
            figureIma.snp.remakeConstraints{ (make) in
                make.left.equalTo(userAvaIma.snp.left)
                make.right.equalTo(contentLab.snp.right)
                make.top.equalTo(contentLab.snp.bottom).offset(20)
                let figureHeight = (UIScreen.main.bounds.size.width - 24)
                make.height.equalTo(figureHeight)
            }
             figureIma.isHidden = false
        }else{
            figureIma.isHidden = true
            figureIma.snp.makeConstraints { (make) in
                make.left.equalTo(userAvaIma.snp.left)
                make.right.equalTo(contentLab.snp.right)
                make.top.equalTo(contentLab.snp.bottom)
                make.height.equalTo(0)
            }
        }
        self.seeAllTagLabel.isHidden = self.readModel?.tagList?.count > 1 ? false : true
        self.firstTagLabel.isHidden = false
        let firstTagName : String = ((self.readModel?.tagList?.first)! as? String ?? "") 
        if self.readModel?.tagList?.count > 1 {
            self.firstTagLabel.text = "#\(firstTagName.capitalized)..."
        }else {
            self.firstTagLabel.text = "#\(firstTagName.capitalized)"
        }
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(singleTagOnClick))
        self.firstTagLabel.addGestureRecognizer(tap)
        self.firstTagLabel.isUserInteractionEnabled = true
        
        if self.readModel?.shouldShowAllTag == true {
            self.seeAllTagLabel.isHidden = true
            self.firstTagLabel.isHidden = true
            createTaglistView()
            self.readModel?.shouldShowAllTag = false
        }
    }
    
    func singleTagOnClick() {
        let tagName : String = ((self.readModel?.tagList?.first)! as? String ?? "") 
        let vc = ReadTagListViewController()
        vc.tagName = tagName
        self.rootVc?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func seeAllTagOnClick() {
        self.seeAllTagLabel.isHidden = true
        self.firstTagLabel.isHidden = true
        self.delegate?.shouldChangeCellHeight?(self.readModel?.cellRowNumber ?? 0)
    }
    
    func createTaglistView() {
//        for arr in tagListView.subviews {
//            arr.removeFromSuperview()
//        }
        var x:CGFloat = 0
        var y:CGFloat = 0
        let labHeight:CGFloat = 18
        let arr = readModel?.tagList
        
        for index in 0..<(arr ?? []).count {
            let btn = UIButton()
            let eachTag = arr![index]
            let tagWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth("#\(eachTag.capitalized)", font: 15, height: labHeight)
            if (x+tagWidth+10)>(SCREEN_WIDTH-24){
                x = 0
                y += 30
            }else{
            }
            btn.setTitle("#\(eachTag.capitalized)", for: UIControlState())
            btn.titleLabel?.font = HSFont.baseRegularFont(15)
            btn.setTitleColor(UIColor.rssSourceBlueColor(), for: UIControlState())
            btn.titleLabel?.textAlignment = .left
            btn.frame = CGRect(x: x, y: y, width: tagWidth, height: 20)
            btn.tag = index
            btn.addTarget(self, action: #selector(goSeeTagList(_:)), for: .touchUpInside)
            x = x+tagWidth+10
            self.cornerBackView?.addSubview(tagListView)
            tagListView.addSubview(btn)
            tagListView.snp.updateConstraints { (make) in
                make.height.equalTo(y+20)
                make.top.equalTo(figureIma.snp.bottom).offset(20)
                make.left.equalTo(self.userAvaIma.snp.left).offset(-2)
                make.right.equalTo(self.contentLab.snp.right)
            }
        }
//        self.layoutIfNeeded()
    }
    
    func goSeeTagList(_ sender:UIButton){
        let tagName = self.readModel?.tagList![sender.tag] as! String
        let vc = ReadTagListViewController()
        vc.tagName = tagName
        self.rootVc?.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    let tagListView:UIView = {
        let v = UIView()
        return v
    }()
    
    let userAvaIma:UIImageView = {
        let ima = UIImageView()
        ima.layer.masksToBounds=true
        ima.layer.cornerRadius=22
        return ima
    }()
    
    let contentLine:UIView = {
        let line = UIView()
        line.backgroundColor=UIColor.divideColor()
        return line
    }()
    
    let userNameLabe:UILabel = {
        let lab = UILabel()
        lab.font=HSFont.baseRegularFont(18)
        lab.textColor = UIColor.purpleContentColor()
        lab.textAlignment = .left
        return lab
    }()
    
    let horoLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.pubTimeColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let commentNumLab:UILabel = {
        let lab = UILabel()
        lab.textColor=UIColor.purpleContentColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let timeLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.pubTimeColor()
        lab.font=HSFont.baseRegularFont(15)
        lab.textAlignment = .right
        return lab
    }()
    
    let contentLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.font=HSFont.baseRegularFont(18)
        lab.textColor = UIColor.purpleContentColor()
        return lab
    }()
    
    let figureIma:UIImageView = {
        let ima = UIImageView()
        ima.contentMode = UIViewContentMode.scaleAspectFit
        return ima
    }()
    
    let treadBtn:UIButton = {
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named:"Step-on_"), for: UIControlState())
        return button
    }()
    
    let praiseBtn:UIButton = {
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named:"praise_"), for: UIControlState())
        return button
    }()
    
    let treadNumLab:UILabel = {
        let lab = UILabel()
        lab.textColor=UIColor.purpleContentColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let praiseLab:UILabel = {
        let lab = UILabel()
        lab.textColor=UIColor.purpleContentColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let commentBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"comments_"), for: UIControlState())
        return btn
    }()
    
    let shareBtn : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"CB_share_"), for: UIControlState())
        return btn
    }()
    
    let tempTagView : UIView = {
        let tempTagView = UIView()
        return tempTagView
    }()
    
    let firstTagLabel : UILabel = {
        let firstTagLabel = UILabel()
        firstTagLabel.font = HSFont.baseRegularFont(15)
        firstTagLabel.textAlignment = .left
        firstTagLabel.textColor = UIColor.commonPinkColor()
        return firstTagLabel
    }()
    
    let seeAllTagLabel : UILabel = {
        let seeAllTagLabel = UILabel()
        seeAllTagLabel.font = HSFont.baseRegularFont(15)
        seeAllTagLabel.textAlignment = .left
        seeAllTagLabel.textColor = UIColor.pubTimeColor()
        seeAllTagLabel.text = "More"
        return seeAllTagLabel
    }()
    
    //MARK:Dequeuse
    static let cellId="NormalPostCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> NormalPostCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! NormalPostCell
        return cell
    }
    
    class func calculateheight(_ model:ReadPostModel?) ->CGFloat{
        let wenben = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: model?.content ?? "", fontSize: 18, font: HSFont.baseRegularFont(18), width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.5)
        
        var h = 20+44+20+10+wenben
        
        h += 20
        let figureHeight = (UIScreen.main.bounds.size.width - 24)
        if (model?.imageList?.count) == 0{
            h -= 20
        }else {
            h += figureHeight
        }
        h += 20
//        h += calculateTagListHeight(model?.tagList ?? [])
        h += 30
        h += 18
        h += 20
        h += 10
        return  h
    }
    
    class func calculateTagListHeight(_ tagList:[AnyObject]) ->CGFloat {
        var x:CGFloat = 0
        var y:CGFloat = 0
        let labHeight:CGFloat = 18
        for index in 0..<tagList.count {
            let eachTag = tagList[index]
            let tagWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth("#\(eachTag.capitalized)", font: 15, height: labHeight)
            //if longer than screenWidth
            if (x+tagWidth+10)>(UIScreen.main.bounds.size.width-24){
                x = 0
                y += 30
            }else{
            }
            x = x+tagWidth+10
        }
        return y+20
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
