//
//  VideoDisplayHeader.swift
//  Horoscope
//
//  Created by Beatman on 17/2/15.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class VideoDisplayHeader: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addHeaderSubviews()
        self.addHeaderConstraint()
    }
    
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    
    let avatarIV : UIImageView = {
        let avatarIV = UIImageView()
        avatarIV.backgroundColor = UIColor.flatNavyBlue
        return avatarIV
    }()
    
    let zodiacLabel : UILabel = {
        let zodiacLabel = UILabel()
        zodiacLabel.text = "MySign"
        zodiacLabel.textAlignment = NSTextAlignment.left
        zodiacLabel.font = HSFont.baseRegularFont(20)
        zodiacLabel.textColor = UIColor.luckyPurpleTitleColor()
        return zodiacLabel
    }()
    
    let nameLabel : UILabel = {
        let nameLabel = UILabel()
        nameLabel.text = "MyName"
        nameLabel.textAlignment = NSTextAlignment.left
        nameLabel.font = HSFont.baseRegularFont(20)
        nameLabel.textColor = UIColor.luckyPurpleTitleColor()
        return nameLabel
    }()
    
    let createTime : UILabel = {
        let createTime = UILabel()
        createTime.textColor = UIColor.gray
        createTime.textAlignment = NSTextAlignment.left
        createTime.text = "6 hours ago"
        createTime.font = HSFont.baseLightFont(16)
        return createTime
    }()
    
    func addHeaderSubviews() {
        self.addSubview(avatarIV)
        self.addSubview(zodiacLabel)
        self.addSubview(nameLabel)
        self.addSubview(createTime)
    }
    
    func addHeaderConstraint() {
        self.avatarIV.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(self.screenHeight * 84/667)
            make.left.equalTo(self.snp.left).offset(self.screenWidth * 25/375)
            make.width.equalTo(self.screenWidth * 50/375)
            make.height.equalTo(self.screenWidth * 50/375)
        }
        self.nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.avatarIV.snp.top)
            make.left.equalTo(self.avatarIV.snp.right).offset(self.screenWidth * 10/375)
            make.height.equalTo(self.screenHeight * 22/667)
        }
        self.zodiacLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.nameLabel.snp.bottom).offset(self.screenHeight * 6/667)
            make.left.equalTo(self.nameLabel.snp.left)
            make.height.equalTo(self.screenHeight * 50/667)
        }
        self.createTime.snp.makeConstraints { (make) in
            make.top.equalTo(self.zodiacLabel.snp.top).offset(2)
            make.left.equalTo(self.zodiacLabel.snp.right).offset(2)
            make.right.equalTo(self.snp.right).offset(-10)
            make.height.equalTo(18)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
