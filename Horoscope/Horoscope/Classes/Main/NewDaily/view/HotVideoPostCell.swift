//
//  HotVideoPostCell.swift
//  Horoscope
//
//  Created by Wang on 17/3/2.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit
class HotVideoPostCell: BaseCardCell {
    weak var rootVc:UIViewController?
    weak var delegate:NormalPostCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createUI()
        self.addConstrains()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createUI() {
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
//        self.cornerBackView?.addSubview(hotTagLab)
        self.cornerBackView?.addSubview(userNameLabe)
        self.cornerBackView?.addSubview(userAvaIma)
        self.cornerBackView?.addSubview(horoLab)
        self.cornerBackView?.addSubview(timeLab)
        self.cornerBackView?.addSubview(praiseBtn)
        self.cornerBackView?.addSubview(praiseLab)
        self.cornerBackView?.addSubview(treadBtn)
        self.cornerBackView?.addSubview(treadNumLab)
        self.cornerBackView?.addSubview(commentBtn)
        self.cornerBackView?.addSubview(shareBtn)
        self.cornerBackView?.addSubview(contentLine)
        self.cornerBackView?.addSubview(thumbNailView)
        self.cornerBackView?.addSubview(durationLabel)
        self.cornerBackView?.addSubview(titleLabel)
        self.cornerBackView?.addSubview(viewsCountLab)
        self.cornerBackView?.addSubview(fromOrg)
        self.cornerBackView?.addSubview(videoIcon)
        self.cornerBackView?.addSubview(commentNumLab)
        
        shareBtn.addTarget(self, action: #selector(shareBtnClick), for: .touchUpInside)
        commentBtn.addTarget(self, action: #selector(commentBtnClick), for: .touchUpInside)
        praiseBtn.addTarget(self, action: #selector(praiseBtnClick), for: .touchUpInside)
        treadBtn.addTarget(self, action: #selector(treadBtnClick), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(goUserPostList))
        self.userAvaIma.addGestureRecognizer(tap)
        self.userAvaIma.isUserInteractionEnabled = true
    }
    
    func goUserPostList() {
        let vc = ReadMasterPostViewController.create(self.model?.user?.userId ?? "")
        vc.userName = self.model?.user?.name ?? ""
        self.rootVc?.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:BtnClick
    func shareBtnClick() {
        let content = FBSDKShareLinkContent.init()
        content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_WITHOUT_IMG)
        FBSDKShareDialog.show(from: self.rootVc, with: content, delegate: nil)
    }
    
    func praiseBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if self.model?.iliked == false{
                let newCount = (model?.likeCount ?? 0) + 1
                self.model?.likeCount = newCount
                self.model?.iliked = true
                self.rencderCell()
                self.delegate?.praiseOneNormalTopic("post", action: "like", value: true, id: model?.postId ?? "" )
            }
            else  if self.model?.iliked == true {
                var newCount = (model?.likeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.model?.likeCount = newCount
                self.model?.iliked = false
                self.rencderCell()
                self.delegate?.praiseOneNormalTopic("post", action: "like", value: false, id: model?.postId ?? "" )
            }
        }
    }
    
    func treadBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            if  self.model?.iDisliked == false{
                self.model?.iDisliked = true
                let newCount = (model?.dislikeCount ?? 0) + 1
                self.model?.dislikeCount = newCount
                self.rencderCell()
                self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: true, id: model?.postId ?? "" )
            }
            else if  self.model?.iDisliked == true{
                self.model?.iDisliked = false
                var newCount = (model?.dislikeCount ?? 0) - 1
                if newCount <= 0{
                    newCount = 0
                }
                self.model?.dislikeCount = newCount
                self.rencderCell()
                self.delegate?.tradeOneNormalTopic("post", action: "dislike", value: false, id: model?.postId ?? "" )
            }
        }
    }
    
    func commentBtnClick()  {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = PostViewController()
            vc.isFirstClassReply = true
            vc.replyModel = MyPostModel.init(model: model)
            self.rootVc?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func addConstrains() {
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom).offset(-10)
        }
        
//        self.hotTagLab.snp.makeConstraints { (make) in
//            make.left.equalTo(self.cornerBackView!.snp.left)
//            make.top.equalTo(cornerBackView!.snp.top)
//            make.width.equalTo(48)
//            make.height.equalTo(20)
//        }
        
        userAvaIma.snp.makeConstraints { (make) in
            make.left.equalTo(cornerBackView!.snp.left).offset(12)
            make.top.equalTo(cornerBackView!.snp.top).offset(20)
            make.width.height.equalTo(44)
        }
        
        userNameLabe.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.right).offset(15)
            make.top.equalTo(userAvaIma.snp.top)
        }
        
        horoLab.snp.makeConstraints { (make) in
            make.left.equalTo(userNameLabe.snp.left)
            make.bottom.equalTo(userAvaIma.snp.bottom)
        }
        
        timeLab.snp.makeConstraints { (make) in
            
            make.right.equalTo(contentLine.snp.right)
            make.centerY.equalTo(userNameLabe.snp.centerY)
        }
        
        contentLine.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.left)
            make.top.equalTo(userAvaIma.snp.bottom).offset(20)
            make.height.equalTo(1)
            make.right.equalTo(cornerBackView!.snp.right).offset(-12)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentLine.snp.bottom).offset(15)
            make.left.equalTo(self.userAvaIma.snp.left)
            make.right.equalTo(self.contentLine.snp.right)
        }
        
        thumbNailView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.left.equalTo(self.userAvaIma)
            make.width.equalTo(UIScreen.main.bounds.size.width-24)
            let figureHeight = (UIScreen.main.bounds.size.width-24)*5/7
            make.height.equalTo(figureHeight)
        }
      
         videoIcon.snp.makeConstraints { (make) in
            make.left.equalTo(self.userAvaIma.snp.left)
            make.top.equalTo(self.thumbNailView.snp.bottom).offset(20)
            make.width.equalTo(15)
            make.height.equalTo(12)
        }
        
        fromOrg.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.videoIcon.snp.bottom)
            make.left.equalTo(self.videoIcon.snp.right).offset(6)
        }
        
        viewsCountLab.snp.makeConstraints { (make) in
            make.right.equalTo(self.contentLine.snp.right)
            make.bottom.equalTo(self.videoIcon.snp.bottom)
        }
        
        praiseBtn.snp.makeConstraints { (make) in
            make.left.equalTo(userAvaIma.snp.left)
            make.bottom.equalTo(cornerBackView!.snp.bottom).offset(-30)
            make.height.equalTo(18)
            make.width.equalTo(18)
        }
        
        praiseLab.snp.makeConstraints { (make) in
            make.left.equalTo(praiseBtn.snp.right).offset(10)
            make.bottom.equalTo(praiseBtn.snp.bottom)
        }
        
        treadBtn.snp.makeConstraints { (make) in
            make.left.equalTo(praiseLab.snp.right).offset(30)
            make.bottom.equalTo(praiseLab.snp.bottom).offset(3)
            make.height.equalTo(18)
            make.width.equalTo(18)
        }
        
        treadNumLab.snp.makeConstraints { (make) in
            make.left.equalTo(treadBtn.snp.right).offset(10)
            make.bottom.equalTo(praiseBtn.snp.bottom)
        }
        
        shareBtn.snp.makeConstraints { (make) in
            make.right.equalTo(contentLine.snp.right)
            make.bottom.equalTo(praiseBtn.snp.bottom)
            make.width.height.equalTo(18)
        }
        
        commentBtn.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.centerX).offset(50)
            make.bottom.equalTo(praiseBtn.snp.bottom)
            make.width.equalTo(18)
            make.height.equalTo(18)
        }
        
        commentNumLab.snp.makeConstraints { (make) in
            make.left.equalTo(commentBtn.snp.right).offset(10)
            make.bottom.equalTo(praiseBtn.snp.bottom)
        }
        
        durationLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.thumbNailView.snp.bottom).offset(-5)
            make.right.equalTo(self.thumbNailView.snp.right).offset(-6)
        }
    }
    
    var model:ReadPostModel? {
        didSet{
            rencderCell()
        }
    }
    
    func rencderCell() {
        if model?.iliked == true{
            praiseBtn.setImage(UIImage(named:"praise2_"), for: UIControlState())
        }else{
            praiseBtn.setImage(UIImage(named:"praise_"), for: UIControlState())
        }
        if model?.iDisliked == true{
            treadBtn.setImage(UIImage(named:"dislike_pink_"), for: UIControlState())
        }else{
            treadBtn.setImage(UIImage(named:"Step-on_"), for: UIControlState())
        }
        if model?.likeCount != 0 {
            self.praiseLab.isHidden = false
            self.praiseLab.text = "\(self.model?.likeCount ?? 0)"
        }else {
            self.praiseLab.isHidden = true
        }
        
        if model?.dislikeCount != 0{
            self.treadNumLab.isHidden = false
            self.treadNumLab.text="\(model?.dislikeCount ?? 0)"
        }else {
            self.treadNumLab.isHidden = true
        }
        
        if model?.commentCount != 0{
            self.commentNumLab.isHidden = false
            self.commentNumLab.text="\(model?.commentCount ?? 0)"
        }else {
            self.commentNumLab.isHidden = true
            self.commentNumLab.text="\(model?.commentCount ?? 0)"
        }
        
        self.userAvaIma.sd_setImage(with: URL(string: "\(model?.user?.avatar ?? "")"), placeholderImage: UIImage(named:"icon_author"))
        self.userNameLabe.text = model?.user?.name
        self.horoLab.text = "\(model?.user?.horoscopeName?.capitalized ?? "") "
        self.timeLab.text =  HSHelpCenter.sharedInstance.dateTool.calculateTimeInterval(Double(model?.createTime ?? 0))
        
        durationLabel.text = " \(model?.video?.richMediaDuration ?? "") "
        
        titleLabel.text = model?.title
        let viewsCountWidth = HSHelpCenter.sharedInstance.textTool.calculateStrWidth(String(describing: model?.viewCount) + " " + NSLocalizedString("views", comment: ""), font: 15, height: 17)
        self.viewsCountLab.snp.updateConstraints({ (make) in
            make.width.equalTo(viewsCountWidth)
        })
        
        if model?.viewCount == 1 || model?.viewCount == 0{
            viewsCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((model?.viewCount) ?? 0) + " " + NSLocalizedString("view", comment: "")
        }else{
            viewsCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((model?.viewCount) ?? 0) + " " + NSLocalizedString("views", comment: "")
        }
        if let imgUrl = model?.video?.thumbnail{
            thumbNailView.sd_setImage(with: URL(string:imgUrl), placeholderImage: UIImage(named:"defaultImg1.jpg"), options: .continueInBackground) {
                (image,error,type,url) in
                if image != nil {
                    self.thumbNailView.image=image
                }
            }
        }
    }
    
//    let hotTagLab:UILabel = {
//        let lab = UILabel()
//        lab.text = NSLocalizedString("TOP", comment: "")
//        lab.font = HSFont.baseRegularFont(18)
//        lab.textColor=UIColor.whiteColor()
//        lab.backgroundColor = UIColor.commonPinkColor()
//        lab.textAlignment = .Center
//        return lab
//    }()
    
    let userAvaIma:UIImageView = {
        let ima = UIImageView()
        ima.layer.masksToBounds=true
        ima.layer.cornerRadius=22
        return ima
    }()
    
    let contentLine:UIView = {
        let line = UIView()
        line.backgroundColor=UIColor.divideColor()
        return line
    }()
    
    let userNameLabe:UILabel = {
        let lab = UILabel()
        lab.font=HSFont.baseRegularFont(18)
        lab.textColor = UIColor.purpleContentColor()
        lab.textAlignment = .right
        return lab
    }()
    
    let horoLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.pubTimeColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let timeLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.pubTimeColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let treadBtn:UIButton = {
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named:"Step-on_"), for: UIControlState())
        return button
    }()
    
    let praiseBtn:UIButton = {
        let button = UIButton(type:.custom)
        button.setImage(UIImage(named:"praise_"), for: UIControlState())
        return button
    }()
    
    let treadNumLab:UILabel = {
        let lab = UILabel()
        lab.textColor=UIColor.purpleContentColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let praiseLab:UILabel = {
        let lab = UILabel()
        lab.textColor=UIColor.purpleContentColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let commentBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"comments_"), for: UIControlState())
        return btn
    }()
    
    let shareBtn : UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"CB_share_"), for: UIControlState())
        return btn
    }()
    
    //MARK:VideoPartUI
    let durationLabel:UILabel = {
        let lab = UILabel()
        lab.font=HSFont.baseRegularFont(11)
        lab.textColor=UIColor.durationTimeColor()
        lab.textAlignment = .center
        lab.layer.masksToBounds = true
        lab.layer.cornerRadius = 6
        lab.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.4)
        return lab
    }()
    
    let thumbNailView:UIImageView = {
        let ima=UIImageView()
        return ima
    }()
    
    let commentNumLab:UILabel = {
        let lab = UILabel()
        lab.textColor=UIColor.purpleContentColor()
        lab.font=HSFont.baseRegularFont(15)
        return lab
    }()
    
    let titleLabel:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.text = " "
        lab.font = HSFont.baseRegularFont(18)
        lab.textColor = UIColor.pubTimeColor()
        return lab
    }()
    
    let viewsCountLab:UILabel = {
        let lab = UILabel()
        lab.textColor = UIColor.purpleTitleColor()
        lab.font = HSFont.baseLightFont(15)
        if UIScreen.main.bounds.size.width >= 375 {
            lab.font =  HSFont.baseLightFont(15)
        } else{
            lab.font =  HSFont.baseLightFont(13)
        }
        lab.textAlignment = NSTextAlignment.right
        return lab
    }()
    
    let fromOrg:UILabel = {
        let lab = UILabel()
        lab.text = NSLocalizedString("Youtube", comment: "")
        if UIScreen.main.bounds.size.width >= 375{
            lab.font = HSFont.baseLightFont(15)
        }else{
            lab.font =  HSFont.baseLightFont(13)
        }
        lab.textColor = UIColor.purpleTitleColor()
        return lab
    }()
    
    let videoIcon:UIImageView = {
        let ima = UIImageView()
        ima.image = UIImage(named:"ic_youtube_" )
        return ima
    }()
    
    //MARK:Dequeuse
    static let cellId="HotVideoPostCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> HotVideoPostCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! HotVideoPostCell
        return cell
    }
    
    class func calculateHeight(_ model:ReadPostModel?) ->CGFloat{
        let titleHeight = HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: model?.title ?? "", fontSize: 18, font: HSFont.baseRegularFont(18), width: UIScreen.main.bounds.size.width-24, multyplyLineSpace: 1.5)
        let h:CGFloat = 20+20.0+44.0+20.0+1.0+20.0+titleHeight+20.0+(SCREEN_WIDTH-24)*5/7+20.0+15.0+30.0+10.0
        return  h
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
