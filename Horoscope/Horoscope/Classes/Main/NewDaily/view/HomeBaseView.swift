//
//  HomeBaseView.swift
//  Horoscope
//
//  Created by Beatman on 17/1/24.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class HomeBaseView: UIView {

    var dataView : HomeTimeLineViewController?
    var animateView : BaseZodiacAnimateView?
    
    func refreashWith(_ frame:CGRect,dataView:HomeTimeLineViewController,animateView:BaseZodiacAnimateView) {
        self.frame = frame
        self.animateView = animateView
        self.dataView = dataView
        self.addSubview(animateView)
        self.addSubview(dataView.view)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.dataView = HomeTimeLineViewController()
        self.animateView = BaseZodiacAnimateView()
        self.addSubview(animateView!)
        self.addSubview(dataView!.view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
