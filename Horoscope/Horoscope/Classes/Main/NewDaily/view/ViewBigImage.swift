//
//  ViewBigImage.swift
//  Horoscope
//
//  Created by Beatman on 17/2/23.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ViewBigImage: UIView {

    init(frame:CGRect,image:UIImage) {
        super.init(frame: frame)
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(removeAction))
        self.addGestureRecognizer(tap)
        self.backgroundColor = UIColor.black
        self.create()
        self.imageView.image = image
    }
    
    init(frame:CGRect,imageUrl:URL) {
        super.init(frame: frame)
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(removeAction))
        self.addGestureRecognizer(tap)
        self.backgroundColor = UIColor.black
        self.create()
        self.imageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "defaultImg1.jpg"))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView : UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        return imgView
    }()
    
    func removeAction() {
        self.removeFromSuperview()
    }
    
    func create() {
        self.addSubview(imageView)
        self.imageView.snp.makeConstraints { (make) in
            make.center.equalTo(self.snp.center)
            make.width.height.equalTo(self.snp.width).offset(-24)
        }
    }

}
