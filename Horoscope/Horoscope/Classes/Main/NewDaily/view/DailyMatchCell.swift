//
//  DailyMatchCell.swift
//  Horoscope
//
//  Created by Beatman on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class DailyMatchCell: BaseCardCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var titleLabel : UILabel!
    var leftImageView : UIImageView!
    var rightImageView : UIImageView!
    var centerAndImg : UIImageView!
    var descripLabel : UILabel!
    var collectionView : UICollectionView!
    var divideView : UIView?
    var bottomDivideView : UIView?
    
    var leftFlag = false
    var rightFlag = false
    var leftImgButton : UIButton!
    weak var rootVC : HomeTimeLineViewController?
    var currentZodiacName : String?
    
    var zodiacName:String?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name(rawValue: "DidEndScroll"), object: nil)
        self.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: 1)
        self.createUI()
        self.addSubviews()
        self.addConstraints()
    }
    
    func reloadData() {
        
        self.currentZodiacName = self.rootVC?.horoName ?? ""
        self.leftImageView.image = UIImage(named: "ic_\(currentZodiacName ?? "")_match")
        
        let zodiacArr :[String] = ["aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn","aquarius","pisces"]
        for sub in 0..<zodiacArr.count {
            let index = IndexPath(item: sub, section: 0)
            let selectCell = self.collectionView.cellForItem(at: index) as! DailyMatchCollectViewCell
            if zodiacArr[sub] == currentZodiacName {
                selectCell.avaImg.image = UIImage(named: ZodiacModel.getZodiaImage(sub)+"selected")
            }else {
                selectCell.avaImg.image = UIImage(named: ZodiacModel.getZodiaImage(sub))
            }
        }
    }
    
    func leftImageOnClick() {
        self.leftImageView.image = UIImage(named: "ic_heart_blank")
        let zodiacArr :[String] = ["aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn","aquarius","pisces"]
        self.leftFlag = false
        for sub in 0..<zodiacArr.count {
            let index = IndexPath(item: sub, section: 0)
            let selectCell = self.collectionView.cellForItem(at: index) as! DailyMatchCollectViewCell
            selectCell.avaImg.image = UIImage(named: ZodiacModel.getZodiaImage(sub))
        }
    }
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    func createUI() {
        self.cornerBackView?.layer.masksToBounds = false
        self.cornerBackView?.layer.cornerRadius = 0
        self.titleLabel = UILabel()
        self.titleLabel.textColor = UIColor.init(red: 181/255, green: 176/255, blue: 253/255, alpha: 1)
        self.titleLabel.text = NSLocalizedString("Daily Match", comment: "")
        self.titleLabel.font = HSFont.baseRegularFont(20)
        
        self.divideView = UIView()
        self.divideView?.backgroundColor = UIColor.divideColor()
        
        self.bottomDivideView = UIView()
        self.bottomDivideView?.backgroundColor = UIColor.baseDetailBackgroundColor()
        
        self.cornerBackView?.backgroundColor = UIColor.clear
        self.leftImageView = UIImageView()
        self.leftImageView.isUserInteractionEnabled = true
        self.leftImageView.image = UIImage(named: "ic_heart_blank")
        self.leftImgButton = UIButton()
        self.leftImgButton.addTarget(self, action: #selector(leftImageOnClick), for: .touchUpInside)
        self.leftImgButton.backgroundColor = UIColor.clear
        
        self.rightImageView = UIImageView()
        self.rightImageView.image = UIImage(named: "ic_heart_blank")
        
        self.centerAndImg = UIImageView()
        self.centerAndImg.image = UIImage(named: "ic_and_")
        
        self.descripLabel = UILabel()
        self.descripLabel.textColor = UIColor.init(red: 181/255, green: 176/255, blue: 253/255, alpha: 1)
        self.descripLabel.text = NSLocalizedString("Click the signs below to start matching", comment: "")
        self.descripLabel.textAlignment = NSTextAlignment.center
        let flowLayout = UICollectionViewFlowLayout()
        let itemMargin : CGFloat = 10
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        let collectionWidth = self.screenWidth*self.frame.width/375
        flowLayout.itemSize = CGSize(width: (collectionWidth - self.frame.width * itemMargin/375 * 7)/6, height: (collectionWidth - self.frame.width * itemMargin/375 * 7)/6)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(DailyMatchCollectViewCell.self, forCellWithReuseIdentifier: "cell")
        self.collectionView.backgroundColor = UIColor.clear
        
    }
    
    func addSubviews() {
        self.cornerBackView?.addSubview(self.titleLabel)
        self.cornerBackView?.addSubview(self.divideView!)
        self.cornerBackView?.addSubview(self.leftImageView)
        self.leftImageView.addSubview(self.leftImgButton)
        self.cornerBackView?.addSubview(self.rightImageView)
        self.cornerBackView?.addSubview(self.centerAndImg)
        self.cornerBackView?.addSubview(self.descripLabel)
        self.cornerBackView?.addSubview(self.collectionView)
        self.cornerBackView?.addSubview(self.bottomDivideView!)
    }
    
    func addConstraints() {
        self.cornerBackView?.snp.remakeConstraints({ (make) in
            make.left.equalTo(self.contentView.snp.left).offset(BaseCardCell.cellPadding)
            make.right.equalTo(self.contentView.snp.right).offset(-BaseCardCell.cellPadding)
            make.top.equalTo(self.contentView.snp.top).offset(BaseCardCell.cellPadding)
            make.bottom.equalTo(self.contentView.snp.bottom)
        })
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo((self.cornerBackView?.snp.top)!).offset(8)
            make.left.equalTo((self.cornerBackView?.snp.left)!)
            make.width.equalTo((self.cornerBackView?.snp.width)!)
        }
        
        self.divideView?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.titleLabel?.snp.bottom)!).offset(20)
            make.centerX.equalTo(self.snp.centerX)
            make.height.equalTo(1)
            make.width.equalTo(self.snp.width)
        })
        
        self.descripLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.divideView!.snp.bottom).offset(self.screenHeight * 20/667)
            make.width.equalTo((self.cornerBackView?.snp.width)!)
        }
        
        self.leftImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.descripLabel.snp.bottom).offset(30)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(5)
            make.width.equalTo(self.screenHeight * 130/667)
            make.height.equalTo(self.screenHeight * 130/667)
        }
        
        self.leftImgButton.snp.makeConstraints { (make) in
            make.edges.equalTo(self.leftImageView.snp.edges)
        }
        
        self.rightImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.leftImageView.snp.top)
            make.left.equalTo(self.centerAndImg.snp.right).offset(15)
            make.width.equalTo(self.leftImageView.snp.width)
            make.height.equalTo(self.leftImageView.snp.width)
        }
        
        self.centerAndImg.snp.makeConstraints { (make) in
            make.centerX.equalTo((self.cornerBackView?.snp.centerX)!)
            make.centerY.equalTo(self.leftImageView.snp.centerY).offset(15)
            make.width.equalTo(50)
            make.height.equalTo(50)
        }
        
        self.collectionView.snp.makeConstraints { (make) in
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(5)
            make.top.equalTo(self.leftImageView.snp.bottom).offset(30)
            make.bottom.equalTo((self.cornerBackView?.snp.bottom)!).offset(-5)
            make.right.equalTo((self.cornerBackView?.snp.right)!).offset(-5)
        }
        
        self.bottomDivideView?.snp.makeConstraints({ (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(self.snp.bottom)
            make.height.equalTo(10)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DailyMatchCollectViewCell
        self.leftName = self.rootVC?.horoName ?? ""
        cell.avaImg.image = UIImage(named: ZodiacModel.getZodiaImage(indexPath.item))
        cell.itemName = ZodiacModel.getZodiacName(indexPath.item)
        if cell.itemName == self.leftName.lowercased() {
            cell.avaImg.image = UIImage(named: "ic_\(ZodiacModel.getZodiacName(indexPath.item))_selected")
        }
        return cell
    }
    
    var leftName : String = ""
    var rightName : String = ""
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectItem = self.collectionView.cellForItem(at: indexPath) as? DailyMatchCollectViewCell
        if self.leftFlag == false && self.rightFlag == false {
            selectItem!.avaImg.image = UIImage(named: ZodiacModel.getZodiaImage(indexPath.item)+"selected")
            self.leftImageView.image = UIImage(named: ZodiacModel.getZodiaImage(indexPath.item)+"match")
            self.leftName = ZodiacModel.getZodiacName(indexPath.item)
            self.leftFlag = true
        }else if self.leftFlag == true && self.rightFlag == false{
            selectItem!.avaImg.image = UIImage(named: ZodiacModel.getZodiaImage(indexPath.item)+"selected")
            self.rightImageView.image = UIImage(named: ZodiacModel.getZodiaImage(indexPath.item)+"match")
            self.rightName = ZodiacModel.getZodiacName(indexPath.item)
            self.collectionView.isUserInteractionEnabled = false
            let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                let matchDetailVC = MatchDetailController()
                matchDetailVC.contentString = MatchData.getMatchInfomation(self.leftName, zodiacNameRight: self.rightName)
                matchDetailVC.leftZodiacName = self.leftName
                matchDetailVC.rightZodiacName = self.rightName
                self.rootVC?.navigationController?.pushViewController(matchDetailVC, animated: true)
                self.rightImageView.image = UIImage(named: "ic_heart_blank")
                selectItem?.avaImg.image = UIImage(named: "ic_"+ZodiacModel.getZodiacName(indexPath.item)+"_")
                self.collectionView.isUserInteractionEnabled = true
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func cellHeight() ->CGFloat{
        return UIScreen.main.bounds.width == 320 ? SCREEN_HEIGHT * 450 / 667 : SCREEN_HEIGHT * 430 / 667
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
