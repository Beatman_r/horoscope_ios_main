//
//  DailyCharacterCell.swift
//  Horoscope
//
//  Created by Beatman on 17/1/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import FBSDKShareKit
import IDMPhotoBrowser
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class DailyFortuneCell: BaseCardCell,UIScrollViewDelegate,CommentStatusBarDelegate,CommentPreLookViewDelegate,CommentPreLookViewBelowDelegate,FBSDKSharingDelegate {
    
    let screenHeight = UIScreen.main.bounds.height
    let screenWidth = UIScreen.main.bounds.width
    
    var isReply : Bool = false
    var titleLabel : UILabel?
    var scroller : UIScrollView?
    weak var rootVC : HomeTimeLineViewController?
    
    var dateLabel : UILabel?
    var contentLabel : UILabel?
    var divideView : UIView?
    var contentFrame : UIImageView?
    var bottomDivideView : UIView?
    var forecastFrame : UIImageView?
    var commentStatusBar : CommentStatusBar?
    var commentPreLookBar : CommentPreLookView?
    
    var prelookBarHeightTop : CGFloat = 40
    var prelookBarHeightBelow : CGFloat = 40
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: 1)
        self.cornerBackView?.backgroundColor = UIColor.clear
        self.createUI()
        self.createScroller()
        self.addSubviews()
        self.addConstraints()
    }
    
//    let hotTagLab:UILabel = {
//        let lab = UILabel()
//        lab.text = " " + NSLocalizedString("Top", comment: "") + " "
//        lab.font = HSFont.baseRegularFont(18)
//        lab.textColor = UIColor.whiteColor()
//        lab.backgroundColor = UIColor.commonPinkColor()
//        lab.layer.borderColor = UIColor.commonPinkColor().CGColor
//        lab.layer.borderWidth = 1
//        return lab
//    }()
    
    func createUI() {
        self.cornerBackView?.layer.masksToBounds = false
        self.cornerBackView?.layer.cornerRadius = 0
        self.titleLabel = UILabel()
        self.titleLabel?.text = NSLocalizedString("Forecast", comment: "")
        self.titleLabel?.font = HSFont.baseRegularFont(20)
        self.titleLabel?.textColor = UIColor.init(red: 181/255, green: 176/255, blue: 253/255, alpha: 1)
        
        self.divideView = UIView()
        self.divideView?.alpha = 0.4
        self.divideView?.backgroundColor = UIColor.divideColor()
        
        self.scroller = UIScrollView()
        self.scroller!.contentSize = CGSize(width: 4*self.screenWidth/3*2, height: 25)
        self.scroller!.delegate = self
        self.scroller!.backgroundColor = UIColor.clear
        self.scroller?.showsHorizontalScrollIndicator = false
        
        self.forecastFrame = UIImageView()
        self.forecastFrame?.image = UIImage(named: "forcastFrame")
        
        self.bottomDivideView = UIView()
        self.bottomDivideView?.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.commentStatusBar = CommentStatusBar(frame: CGRect(x: 0, y: 0, width: 50, height: 100))
        self.commentStatusBar?.delegate = self
        self.commentPreLookBar = CommentPreLookView(frame: CGRect(x: 0, y: 0, width: 50, height: 100))
        self.commentPreLookBar?.delegate = self
        self.commentPreLookBar?.belowDelegate = self
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(seeOriginalTapAction))
        self.commentPreLookBar?.addGestureRecognizer(tap)
    }
    
    var dateLabelList : [UILabel] = []
    var contentLabelList : [UILabel] = []
    func createScroller() {
        let titleList = [NSLocalizedString("Tomorrow", comment: ""),
                         NSLocalizedString("Weekly", comment: ""),
                         NSLocalizedString("Monthly", comment: ""),
                         NSLocalizedString("Yearly", comment: "")]
        for count in 0..<4 {
            let view = UIView()
            view.backgroundColor = UIColor.clear
            dateLabel = UILabel()
            dateLabel!.textAlignment = NSTextAlignment.center
            dateLabel!.text = titleList[count]
            dateLabel!.textColor = UIColor.luckyPurpleTitleColor()
            dateLabel!.font = HSFont.baseLightRegularFont(18)
            dateLabelList.append(dateLabel!)
            contentLabel = UILabel()
            contentLabel!.numberOfLines = 6
            contentLabel!.font = HSFont.baseLightRegularFont(18)
            contentLabel!.textColor = UIColor.luckyPurpleTitleColor()
            contentLabelList.append(contentLabel!)
            
            self.contentFrame = UIImageView()
            self.contentFrame?.image = UIImage(named: "forcast_frame")
            
            let button = UIButton()
            button.tag = count
            button.addTarget(self, action: #selector(buttonOnClick(_:)), for: .touchUpInside)
            view.addSubview(dateLabel!)
            view.addSubview(contentLabel!)
            view.addSubview(self.contentFrame!)
            view.addSubview(button)
            self.scroller!.addSubview(view)
            
            view.snp.makeConstraints({ (make) in
                make.top.equalTo(SCREEN_WIDTH * 5 / 375)
                make.left.equalTo(self.screenWidth/3*2 * CGFloat(count))
                make.width.equalTo(self.screenWidth/1.8)
                make.height.equalTo(SCREEN_WIDTH * 200 / 375)
            })
            dateLabel!.snp.makeConstraints { (make) in
                make.top.equalTo(view.snp.top).offset(10)
                make.width.equalTo(view.snp.width)
                make.left.equalTo(view.snp.left)
            }
            contentLabel!.snp.makeConstraints { (make) in
                make.top.equalTo(dateLabel!.snp.bottom)
                make.left.equalTo(view.snp.left).offset(5)
                make.right.equalTo(view.snp.right).offset(-5)
                make.height.equalTo(self.screenWidth * 160/375)
            }
            contentFrame?.snp.makeConstraints({ (make) in
                make.centerX.equalTo((self.contentLabel?.snp.centerX)!)
                make.width.equalTo((self.contentLabel?.snp.width)!).offset(10)
                make.top.equalTo((self.contentLabel?.snp.top)!).offset(-self.screenWidth * 30/375)
                make.bottom.equalTo((self.contentLabel?.snp.bottom)!).offset(3)
            })
            button.snp.makeConstraints { (make) in
                make.edges.equalTo(contentLabel!.snp.edges)
            }
        }
    }
    
    func buttonOnClick(_ sender:UIButton) {
        let name = self.rootVC?.horoName ?? ""
        let fortuneListVC:FortureListViewcontroller = FortureListViewcontroller.create(name)
        fortuneListVC.pageType = 1
        fortuneListVC.currentIndex = Int32(sender.tag)+2
        let dic = [0:"tomorrow",1:"weekly",2:"monthly",3:"yearly"]
        if let str = dic[sender.tag]{
            AnaliticsManager.sendEvent(AnaliticsManager.main_card_forcast_click, data: ["date":str ])
        }
        self.rootVC?.navigationController?.pushViewController(fortuneListVC, animated: true)
    }
    
    func addSubviews() {
        self.cornerBackView?.addSubview(self.titleLabel!)
        self.cornerBackView?.addSubview(self.scroller!)
        self.cornerBackView?.addSubview(self.forecastFrame!)
        self.cornerBackView?.addSubview(self.divideView!)
        self.cornerBackView?.addSubview(self.bottomDivideView!)
        self.cornerBackView?.addSubview(self.commentStatusBar!)
        self.cornerBackView?.addSubview(self.commentPreLookBar!)
//        self.cornerBackView?.addSubview(hotTagLab)
    }
    
    func addConstraints() {
        
        self.cornerBackView?.snp.remakeConstraints({ (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        })
        
//        self.hotTagLab.snp.makeConstraints { (make) in
//            make.top.equalTo(self.cornerBackView!.snp.top)
//            make.left.equalTo(self.cornerBackView!.snp.left)
//        }
        
        self.titleLabel!.snp.makeConstraints { (make) in
            make.top.equalTo(self.cornerBackView!.snp.top).offset(20)
            make.left.equalTo((self.cornerBackView?.snp.left)!).offset(BaseCardCell.cellPadding)
        }
        
        self.divideView?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.titleLabel?.snp.bottom)!).offset(10)
            make.centerX.equalTo(self.snp.centerX)
            make.height.equalTo(1)
            make.width.equalTo(self.snp.width)
        })
        
        self.scroller!.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLabel!.snp.bottom).offset(20)
            make.left.equalTo((self.titleLabel?.snp.left)!)
            make.right.equalTo((self.cornerBackView?.snp.right)!).offset(-BaseCardCell.cellPadding)
            make.height.equalTo(SCREEN_HEIGHT * 200/667)
        }
        
        self.forecastFrame?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom)
            make.right.equalTo(self.snp.right)
            make.width.equalTo(SCREEN_WIDTH * 150/375)
        })
        
        self.bottomDivideView?.snp.makeConstraints({ (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(self.snp.bottom)
            make.height.equalTo(10)
        })
        let commentToolBarHeight = CommentStatusBar.commentToolBarHeight()
        self.commentStatusBar?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.scroller?.snp.bottom)!).offset(SCREEN_HEIGHT * 10/667)
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right) 
            make.height.equalTo(SCREEN_HEIGHT * commentToolBarHeight/667)
        })
        
        self.commentPreLookBar?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.commentStatusBar?.snp.bottom)!).offset(20)
            make.left.equalTo((self.cornerBackView?.snp.left)!)
            make.right.equalTo((self.cornerBackView?.snp.right)!)
            make.height.equalTo(self.snp.height)
        })
    }
    
    class func cellHeight() ->CGFloat {
        return SCREEN_WIDTH == 320 ? SCREEN_HEIGHT * 355 / 667 : SCREEN_HEIGHT * 335 / 667
    }
    
    var dataModel:ForecastPart? {
        didSet{
            renderCell()
        }
    }
    var currentLikeCount = 0
    var currentDislikeCount = 0
    var currentCommentCount = 0
    var currentShareCount = 0
    var tomorrowPostId = ""
    
    var currentPrelookLikeCount = 0
    var commentTopId = ""
    var currentPrelookLikeCountBelow = 0
    var currentPrelookDislikeCount = 0
    var commentBelowId = ""
    var currentPrelookDislikeCountBelow = 0
    
    func renderCell() {
        var count = 0
        for mo in (dataModel?.list) ?? []{
            self.dateLabelList[count].text = mo.period?.capitalized ?? ""
            // self.contentLabelList[count].text = mo.content ?? ""
            let font = HSFont.baseLightRegularFont(18)
            self.contentLabelList[count].attributedText = HSHelpCenter.sharedInstance.textTool.setTextlabelAttributedString(lineSpace: 1.25, textColor: UIColor.luckyPurpleTitleColor(), targetStr: mo.content ?? "", font: font)
            if mo.period == NSLocalizedString("tomorrow", comment: ""){
                self.commentStatusBar?.likeCountLabel.text = "\(mo.likeCount)"
                self.commentStatusBar?.likeCountLabel.isHidden = self.commentStatusBar?.likeCountLabel.text == "0" ? true : false
                if mo.isLiked == true {
                    self.commentStatusBar?.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                    self.commentStatusBar?.likeCountLabel.textColor = UIColor.commonPinkColor()
                }else {
                    self.commentStatusBar?.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                    self.commentStatusBar?.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                }
                currentLikeCount = mo.likeCount
                
                self.commentStatusBar?.unLikeCountLabel.text = "\(mo.dislikeCount )"
                self.commentStatusBar?.unLikeCountLabel.isHidden = self.commentStatusBar?.unLikeCountLabel.text == "0" ? true : false
                if mo.isDisliked == true {
                    self.commentStatusBar?.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                    self.commentStatusBar?.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                }else {
                    self.commentStatusBar?.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                    self.commentStatusBar?.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                }
                currentDislikeCount = mo.dislikeCount
                
                self.commentStatusBar?.commentCountLabel.text = "\(mo.commentCount)"
                self.commentStatusBar?.commentCountLabel.isHidden = self.commentStatusBar?.commentCountLabel.text == "0" ? true : false
                currentCommentCount = mo.commentCount
//                self.commentStatusBar?.shareCountLabel.text = String(mo.shareCount ?? 0)
                //                self.commentStatusBar?.shareCountLabel.text = "\(mo.shareCount ?? 0)"
                //                self.commentStatusBar?.shareCountLabel.hidden = self.commentStatusBar?.shareCountLabel.text == "0" ? true : false
                //                currentShareCount = mo.shareCount ?? 0
                
                tomorrowPostId = mo.postId ?? ""
                let commentList = mo.commentList ?? []
                let count = commentList.count
                if count > 0 {
                    for sub in 0..<count{
                        if sub == 0 {
                            self.commentPreLookBar?.likeCountLabel.text = "\(commentList[sub].likeCount ?? 0)"
//                            self.commentPreLookBar?.likeCountLabel.hidden = commentList[sub].likeCount ?? 0 == 0 ? true : false
                            self.currentPrelookLikeCount = commentList[sub].likeCount ?? 0
                            let avaStr = commentList[sub].userInfo?.avatar ?? ""
                            let avaUrl = URL(string: avaStr)
                            self.commentPreLookBar?.userAva.sd_setImage(with: avaUrl, placeholderImage: UIImage(named: "defaultImg1.jpg"))
                            if commentList[sub].isLiked == true {
                                self.commentPreLookBar?.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                                self.commentPreLookBar?.likeCountLabel.textColor = UIColor.commonPinkColor()
                            }else {
                                self.commentPreLookBar?.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                                self.commentPreLookBar?.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                            }
                            
                            self.commentPreLookBar?.unLikeCountLabel.text = "\(commentList[sub].dislikeCount ?? 0)"
//                            self.commentPreLookBar?.unLikeCountLabel.hidden = commentList[sub].dislikeCount ?? 0 == 0 ? true : false
                            self.currentPrelookDislikeCount = commentList[sub].dislikeCount ?? 0
                            if commentList[sub].isDisliked == true {
                                self.commentPreLookBar?.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                                self.commentPreLookBar?.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                            }else {
                                self.commentPreLookBar?.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                                self.commentPreLookBar?.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                            }
                            let contentStr = commentList[sub].content ?? ""
                            self.commentPreLookBar?.contentLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(contentStr, space: 1.25)
                            self.commentPreLookBar?.nameLabel.text = commentList[sub].userInfo?.name ?? ""
                            self.commentPreLookBar?.signLabel.text = commentList[sub].userInfo?.horoscopeName ?? ""
                            self.commentPreLookBar?.createTime.text = commentList[sub].createTimeHuman ?? ""
                            self.commentTopId = commentList[sub].commentId ?? ""
                            if commentList[sub].imageList?.count > 0 {
                                self.commentPreLookBar?.viewImageButton.isHidden = false
                                self.commentPreLookBar?.viewImgLabel.isHidden = false
                                self.commentPreLookBar?.viewImageButton.snp.updateConstraints({ (make) in
                                    make.height.equalTo(SCREEN_HEIGHT * 20 / 667)
                                })
                            }else {
                                self.commentPreLookBar?.viewImageButton.isHidden = true
                                self.commentPreLookBar?.viewImgLabel.isHidden = true
                                self.commentPreLookBar?.viewImageButton.snp.updateConstraints({ (make) in
                                    make.height.equalTo(0)
                                })
                            }
                        }else if sub == 1 {
                            self.commentPreLookBar?.likeCountLabelBelow.text = "\(commentList[sub].likeCount ?? 0)"
                            self.commentPreLookBar?.likeCountLabelBelow.isHidden = commentList[sub].likeCount ?? 0 == 0 ? true : false
                            self.currentPrelookLikeCountBelow = commentList[sub].likeCount ?? 0
                            let avaStr = commentList[sub].userInfo?.avatar ?? ""
                            let avaUrl = URL(string: avaStr)
                            self.commentPreLookBar?.userAvaBelow.sd_setImage(with: avaUrl, placeholderImage: UIImage(named: "defaultImg1.jpg"))
                            if commentList[sub].isLiked == true {
                                self.commentPreLookBar?.likeButtonBelow.setImage(UIImage(named: "praise2_"), for: UIControlState())
                                self.commentPreLookBar?.likeCountLabelBelow.textColor = UIColor.commonPinkColor()
                            }else {
                                self.commentPreLookBar?.likeButtonBelow.setImage(UIImage(named: "praise_"), for: UIControlState())
                                self.commentPreLookBar?.likeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                            }
                            self.commentPreLookBar?.unLikeCountLabelBelow.text = "\(commentList[sub].dislikeCount ?? 0)"
                            self.commentPreLookBar?.unLikeCountLabelBelow.isHidden = commentList[sub].dislikeCount ?? 0 == 0 ? true : false
                            self.currentPrelookDislikeCountBelow = commentList[sub].dislikeCount ?? 0
                            if commentList[sub].isDisliked == true {
                                self.commentPreLookBar?.unLikeButtonBelow.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                                self.commentPreLookBar?.unLikeCountLabelBelow.textColor = UIColor.commonPinkColor()
                            }else {
                                self.commentPreLookBar?.unLikeButtonBelow.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                                self.commentPreLookBar?.unLikeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                            }
                            let contentStr = commentList[sub].content ?? ""
                            self.commentPreLookBar?.contentLabelBelow.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(contentStr, space: 1.25)
                            self.commentPreLookBar?.nameLabelBelow.text = commentList[sub].userInfo?.name ?? ""
                            self.commentPreLookBar?.signLabelBelow.text = commentList[sub].userInfo?.horoscopeName ?? ""
                            self.commentPreLookBar?.createTimeBelow.text = commentList[sub].createTimeHuman ?? ""
                            self.commentBelowId = commentList[sub].commentId ?? ""
                            if commentList[sub].imageList?.count > 0 {
                                self.commentPreLookBar?.viewImageButtonBelow.isHidden = false
                                self.commentPreLookBar?.viewImgLabelBelow.isHidden = false
                                self.commentPreLookBar?.viewImageButtonBelow.snp.updateConstraints({ (make) in
                                    make.height.equalTo(SCREEN_HEIGHT * 20 / 667)
                                })
                            }else {
                                self.commentPreLookBar?.viewImageButtonBelow.isHidden = true
                                self.commentPreLookBar?.viewImgLabelBelow.isHidden = true
                                self.commentPreLookBar?.viewImageButtonBelow.snp.updateConstraints({ (make) in
                                    make.height.equalTo(0)
                                })
                            }
                        }
                    }
                }
            }
            count += 1
        }
    }
    
    func didClickComment() {
        
        let name = self.rootVC?.horoName ?? ""
        let fortuneListVC:FortureListViewcontroller = FortureListViewcontroller.create(name)
        fortuneListVC.pageType = 1
        fortuneListVC.currentIndex = 2
        self.rootVC?.navigationController?.pushViewController(fortuneListVC, animated: true)
        
        //        let vc = PostViewController()
        //        vc.isFirstClassReply = true
        //        vc.isForcastReply = true
        //        vc.horoscopeName = CURRENT_MAIN_SIGN
        //        vc.forecastName = "tomorrow"
        //        let replyModel = MyPostModel.init(jsonData: nil, num: 0)
        //        vc.replyModel = replyModel
        //        replyModel.postId = self.tomorrowPostId
        //        self.rootVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func didClickLike() {
        for model in self.dataModel?.list ?? [] {
            if model.period == NSLocalizedString("tomorrow", comment: "") {
                let userStatus = AccountManager.sharedInstance.getLogStatus()
                if userStatus == .null || userStatus == .anonymous{
                    let vc = HsLoginViewController()
                    self.rootVC?.navigationController?.pushViewController(vc, animated: true)
                }else {
                    if model.isLiked == false {
                        let newCount = (model.likeCount) + 1
                        model.likeCount = newCount
                        model.isLiked = true
                        self.commentStatusBar?.likeCountLabel.isHidden = false
                        self.renderCell()
                        self.commentStatusBar?.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                        self.commentStatusBar?.likeCountLabel.textColor = UIColor.commonPinkColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: true, id: model.postId ?? "")
                    }else if model.isLiked == true {
                        var newCount = (model.likeCount) - 1
                        if newCount <= 0 {
                            newCount = 0
                            self.commentStatusBar?.likeCountLabel.isHidden = true
                        }
                        model.likeCount = newCount
                        model.isLiked = false
                        self.renderCell()
                        self.commentStatusBar?.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                        self.commentStatusBar?.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "like", value: false, id: model.postId ?? "")
                    }
                }
            }
        }
    }
    
    func didClickDislike() {
        for model in self.dataModel?.list ?? [] {
            if model.period == NSLocalizedString("tomorrow", comment: "") {
                let userStatus = AccountManager.sharedInstance.getLogStatus()
                if userStatus == .null || userStatus == .anonymous{
                    let vc = HsLoginViewController()
                    self.rootVC?.navigationController?.pushViewController(vc, animated: true)
                }else {
                    if model.isDisliked == false {
                        let newCount = (model.dislikeCount) + 1
                        model.dislikeCount = newCount
                        model.isDisliked = true
                        self.commentStatusBar?.unLikeCountLabel.isHidden = false
                        self.renderCell()
                        self.commentStatusBar?.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                        self.commentStatusBar?.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: true, id: model.postId ?? "")
                    }else if model.isDisliked == true {
                        var newCount = (model.dislikeCount) - 1
                        if newCount <= 0 {
                            newCount = 0
                            self.commentStatusBar?.unLikeCountLabel.isHidden = true
                        }
                        model.dislikeCount = newCount
                        model.isDisliked = false
                        self.renderCell()
                        self.commentStatusBar?.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                        self.commentStatusBar?.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "dislike", value: false, id: model.postId ?? "")
                    }
                }
            }
        }
    }
    
    func didClickShare() {
        let content = FBSDKShareLinkContent.init()
        content.contentURL = URL(string: BASIC_FBSHARE_URL + FBSHARE_FORECAST)
        FBSDKShareDialog.show(from: self.rootVC, with: content, delegate: self)
        for mo in dataModel?.list ?? [] {
            if mo.period == NSLocalizedString("tomorrow", comment: "") {
                let postId = mo.postId ?? ""
                HoroscopeDAO.sharedInstance.likeOrDislikeAction("forecast", action: "share", value: true, id: postId)
            }
        }
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        return
    }
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print(error)
    }
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
//        let newCountStr = self.commentStatusBar?.shareCountLabel.text ?? ""
//        self.commentStatusBar?.shareCountLabel.text = "\(Int(newCountStr)! + 1)"
    }
    
    func preLookDidClickDislike() {
        for model in self.dataModel?.list ?? [] {
            if model.period == NSLocalizedString("tomorrow", comment: "") {
                let userStatus = AccountManager.sharedInstance.getLogStatus()
                if userStatus == .null || userStatus == .anonymous{
                    let vc = HsLoginViewController()
                    self.rootVC?.navigationController?.pushViewController(vc, animated: true)
                }else {
                    let topCommentModel = model.commentList?.first ?? MainComment()
                    if topCommentModel.isDisliked == false {
                        let newCount = (topCommentModel.dislikeCount ?? 0) + 1
                        topCommentModel.dislikeCount = newCount
                        topCommentModel.isDisliked = true
                        self.renderCell()
                        self.commentPreLookBar?.unLikeButton.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                        self.commentPreLookBar?.unLikeCountLabel.textColor = UIColor.commonPinkColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: topCommentModel.commentId ?? "")
                    }else if topCommentModel.isDisliked == true {
                        var newCount = (topCommentModel.dislikeCount ?? 0) - 1
                        if newCount <= 0 {
                            newCount = 0
                        }
                        topCommentModel.dislikeCount = newCount
                        topCommentModel.isDisliked = false
                        self.renderCell()
                        self.commentPreLookBar?.unLikeButton.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                        self.commentPreLookBar?.unLikeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: topCommentModel.commentId ?? "")
                    }
                }
            }
        }
    }
    
    func preLookDidClickLike() {
        for model in self.dataModel?.list ?? [] {
            if model.period == NSLocalizedString("tomorrow", comment: "") {
                let userStatus = AccountManager.sharedInstance.getLogStatus()
                if userStatus == .null || userStatus == .anonymous{
                    let vc = HsLoginViewController()
                    self.rootVC?.navigationController?.pushViewController(vc, animated: true)
                }else {
                    let topCommentModel = model.commentList?.first ?? MainComment()
                    if topCommentModel.isLiked == false {
                        let newCount = (topCommentModel.likeCount ?? 0) + 1
                        topCommentModel.likeCount = newCount
                        topCommentModel.isLiked = true
                        self.renderCell()
                        self.commentPreLookBar?.likeButton.setImage(UIImage(named: "praise2_"), for: UIControlState())
                        self.commentPreLookBar?.likeCountLabel.textColor = UIColor.commonPinkColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: topCommentModel.commentId ?? "")
                    }else if topCommentModel.isLiked == true {
                        var newCount = (topCommentModel.likeCount ?? 0) - 1
                        if newCount <= 0 {
                            newCount = 0
                        }
                        topCommentModel.likeCount = newCount
                        topCommentModel.isLiked = false
                        self.renderCell()
                        self.commentPreLookBar?.likeButton.setImage(UIImage(named: "praise_"), for: UIControlState())
                        self.commentPreLookBar?.likeCountLabel.textColor = UIColor.luckyPurpleTitleColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: topCommentModel.commentId ?? "")
                    }
                }
            }
        }
    }
    
    func preLookBelowDidClickLike(){
        for model in self.dataModel?.list ?? [] {
            if model.period == NSLocalizedString("tomorrow", comment: "") {
                let userStatus = AccountManager.sharedInstance.getLogStatus()
                if userStatus == .null || userStatus == .anonymous{
                    let vc = HsLoginViewController()
                    self.rootVC?.navigationController?.pushViewController(vc, animated: true)
                }else {
                    let topCommentModel = model.commentList?[1]
                    if topCommentModel!.isLiked == false {
                        let newCount = (topCommentModel!.likeCount ?? 0) + 1
                        topCommentModel!.likeCount = newCount
                        topCommentModel!.isLiked = true
                        self.commentPreLookBar?.likeCountLabelBelow.isHidden = false
                        self.renderCell()
                        self.commentPreLookBar?.likeButtonBelow.setImage(UIImage(named: "praise2_"), for: UIControlState())
                        self.commentPreLookBar?.likeCountLabelBelow.textColor = UIColor.commonPinkColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: true, id: topCommentModel!.commentId ?? "")
                    }else if topCommentModel!.isLiked == true {
                        var newCount = (topCommentModel!.likeCount ?? 0) - 1
                        if newCount <= 0 {
                            newCount = 0
                            self.commentPreLookBar?.likeCountLabelBelow.isHidden = true
                        }
                        topCommentModel!.likeCount = newCount
                        topCommentModel!.isLiked = false
                        self.renderCell()
                        self.commentPreLookBar?.likeButtonBelow.setImage(UIImage(named: "praise_"), for: UIControlState())
                        self.commentPreLookBar?.likeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "like", value: false, id: topCommentModel!.commentId ?? "")
                    }
                }
            }
        }
    }
    
    func preLookBelowDidClickDislike(){
        for model in self.dataModel?.list ?? [] {
            
            if model.period == NSLocalizedString("tomorrow", comment: "") {
                let userStatus = AccountManager.sharedInstance.getLogStatus()
                if userStatus == .null || userStatus == .anonymous{
                    let vc = HsLoginViewController()
                    self.rootVC?.navigationController?.pushViewController(vc, animated: true)
                }else {
                    let topCommentModel = model.commentList?[1] ?? MainComment()
                    if topCommentModel.isDisliked == false {
                        let newCount = (topCommentModel.dislikeCount ?? 0) + 1
                        topCommentModel.dislikeCount = newCount
                        topCommentModel.isDisliked = true
                        self.commentPreLookBar?.unLikeCountLabelBelow.isHidden = false
                        self.renderCell()
                        self.commentPreLookBar?.unLikeButtonBelow.setImage(UIImage(named: "dislike_pink_"), for: UIControlState())
                        self.commentPreLookBar?.unLikeCountLabelBelow.textColor = UIColor.commonPinkColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: true, id: topCommentModel.commentId ?? "")
                    }else if topCommentModel.isDisliked == true {
                        var newCount = (topCommentModel.dislikeCount ?? 0) - 1
                        if newCount <= 0 {
                            newCount = 0
                            self.commentPreLookBar?.unLikeCountLabelBelow.isHidden = true
                        }
                        topCommentModel.dislikeCount = newCount
                        topCommentModel.isDisliked = false
                        self.renderCell()
                        self.commentPreLookBar?.unLikeButtonBelow.setImage(UIImage(named: "Step-on_"), for: UIControlState())
                        self.commentPreLookBar?.unLikeCountLabelBelow.textColor = UIColor.luckyPurpleTitleColor()
                        HoroscopeDAO.sharedInstance.likeOrDislikeAction("comment", action: "dislike", value: false, id: topCommentModel.commentId ?? "")
                    }
                }
            }
        }
    }
    
    func prelookDidClickViewImg() {
        let imaName = self.dataModel?.list![0].commentList?.first?.imageList?.first ?? ""
        let imgUrl = URL(string: imaName)
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.rootVC?.present(brower!, animated: true, completion: nil)
    }
    
    func prelookBelowDidClickViewImg() {
        let imaName = self.dataModel?.list![0].commentList?[1].imageList?.first ?? ""
        let imgUrl = URL(string: imaName)
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.rootVC?.present(brower!, animated: true, completion: nil)
    }
    
    func seeOriginalTapAction() {
        let name = self.rootVC?.horoName ?? ""
        let fortuneListVC:FortureListViewcontroller = FortureListViewcontroller.create(name)
        fortuneListVC.pageType = 1
        fortuneListVC.currentIndex = 2
        let dic = [0:"tomorrow",1:"weekly",2:"monthly",3:"yearly"]
        if let str = dic[0]{
            AnaliticsManager.sendEvent(AnaliticsManager.main_card_forcast_click, data: ["date":str])
        }
        self.rootVC?.navigationController?.pushViewController(fortuneListVC, animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

