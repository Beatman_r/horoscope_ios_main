//
//  SelectZodiacCell.swift
//  Horoscope
//
//  Created by Beatman on 17/1/13.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class SelectZodiacCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    var itemMargin : CGFloat = 30
    var collectionView : UICollectionView?
    var topTitleLabel : UILabel?
    var topLabel : UILabel?
    var selectNowButton : UIButton?
    var centerLabel : UILabel?
    weak var rootVC : HomeTimeLineViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let noti = NotificationCenter.default
        noti.addObserver(self, selector: #selector(changeColor(_:)), name: NSNotification.Name(rawValue: noti_didChangeColor), object: nil)
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.createTopView()
        self.selectionStyle = .none
        self.createCollectionView()
    }
    
    func createTopView() {
        self.topTitleLabel = UILabel()
        topTitleLabel?.text = NSLocalizedString("Select your horoscope", comment: "")
        topTitleLabel?.font = self.screenWidth == 320 ? HSFont.baseRegularFont(20) : HSFont.baseRegularFont(22)
        topTitleLabel?.numberOfLines = 0
        topTitleLabel?.textAlignment = NSTextAlignment.left
        topTitleLabel?.textColor = UIColor.luckyPurpleContentColor()
        
        self.topLabel = UILabel()
        topLabel?.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(NSLocalizedString("\"Before you miss out on\n Your daily forecast\n Your daily lucky number\n Your daily lucky color\n ..................\"", comment: ""), space: 1.25)
        topLabel?.font = self.screenWidth == 320 ? HSFont.baseRegularFont(17) : HSFont.baseRegularFont(19)
        topLabel?.numberOfLines = 5
        topLabel?.textAlignment = NSTextAlignment.left
        topLabel?.textColor = UIColor.luckyPurpleContentColor()
        
        self.selectNowButton = UIButton()
        selectNowButton?.setTitle(NSLocalizedString("Select Now", comment: ""), for: UIControlState())
        self.selectNowButton?.layer.cornerRadius = self.screenWidth * 25/375
        self.selectNowButton?.layer.borderWidth = 1
        self.selectNowButton?.layer.borderColor = UIColor.commonPinkColor().cgColor
        self.selectNowButton?.layer.masksToBounds = true
        self.selectNowButton?.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        self.selectNowButton?.titleLabel?.textAlignment = NSTextAlignment.center
        self.selectNowButton?.titleLabel?.font = HSFont.baseRegularFont(22)
        self.selectNowButton?.addTarget(self, action: #selector(selectButtonOnClick), for: .touchUpInside)
        
        self.contentView.addSubview(self.topTitleLabel!)
        self.contentView.addSubview(self.topLabel!)
        self.contentView.addSubview(self.selectNowButton!)
        
        self.contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 10, 0))
        }
        self.topTitleLabel?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(self.contentView.snp.centerX)
            make.centerY.equalTo(self.contentView.snp.top).offset(self.screenHeight * 80/667)
            make.width.equalTo(self.screenWidth * 225/375)
        })
        self.topLabel?.snp.makeConstraints({ (make) in
            make.centerX.equalTo((self.topTitleLabel?.snp.centerX)!)
            make.top.equalTo((self.topTitleLabel?.snp.bottom)!).offset(1.25)
            make.width.equalTo(self.screenWidth * 225/375)
        })
        self.selectNowButton?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.topLabel?.snp.bottom)!).offset(10)
            make.left.equalTo(self.contentView.snp.centerX).offset(-self.screenWidth * 80/375)
            make.right.equalTo(self.contentView.snp.centerX).offset(self.screenWidth * 80/375)
            make.height.equalTo(self.screenWidth * 50/375)
        })
    }
    
    func selectButtonOnClick() {
        AnaliticsManager.sendEvent(AnaliticsManager.main_select_now_click)
        let vc = AddHoroscopeViewController()
        self.rootVC?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func createCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: (self.screenWidth - 3 * itemMargin) / 4, height: (self.screenWidth - 3 * itemMargin) / 4)
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        self.collectionView?.isScrollEnabled = false
        self.collectionView?.backgroundColor = UIColor.clear
        self.collectionView?.register(DailyMatchCollectViewCell.self, forCellWithReuseIdentifier: "cell")
        self.contentView.addSubview(self.collectionView!)
        
        self.centerLabel = UILabel()
        self.centerLabel?.textAlignment = NSTextAlignment.left
        self.centerLabel?.textColor = UIColor.luckyPurpleTitleColor()
        self.centerLabel?.text = NSLocalizedString("Today's Horoscope", comment: "")
        self.centerLabel?.font = HSFont.baseMediumFont(22)
        self.contentView.addSubview(self.centerLabel!)
        
        self.collectionView?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.centerLabel?.snp.bottom)!).offset(10)
            make.left.equalTo(self.contentView.snp.left).offset(10)
            make.right.equalTo(self.contentView.snp.right).offset(-10)
            make.height.equalTo((self.screenWidth - 3 * itemMargin) / 4 * 3 + 20)
        })
        
        self.centerLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo((self.selectNowButton?.snp.bottom)!).offset(self.screenHeight * 35/667)
            make.left.equalTo(self.contentView.snp.left).offset(10)
        })
    }
    func changeColor(_ noti:Notification) {
        let offY = (noti.userInfo?["offsetY"] as AnyObject).floatValue
        let kColor = offY!/400
        self.backgroundColor = UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha: CGFloat(kColor))
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DailyMatchCollectViewCell
        cell.avaImg.image = UIImage(named: ZodiacModel.getZodiaImage(indexPath.item))
        cell.itemName = ZodiacModel.getZodiacName(indexPath.item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     // UIApplication.sharedApplication().openURL(NSURL(string:UIApplicationOpenSettingsURLString)!)
        let name = ZodiacModel.getZodiacName(indexPath.item)
        AnaliticsManager.sendEvent(AnaliticsManager.main_card_today_click, data: ["signs":"\(name)"])
        let fortuneListVC:FortureListViewcontroller = FortureListViewcontroller.create(name)
        fortuneListVC.pageType = 1
        fortuneListVC.currentIndex = 1
        self.rootVC?.navigationController?.pushViewController(fortuneListVC, animated: true)
    }
    
    class func cellHeight() -> CGFloat {
        return UIScreen.main.bounds.height * 649/667
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static let cellId="SelectZodiacCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> SelectZodiacCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! SelectZodiacCell
        return cell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
