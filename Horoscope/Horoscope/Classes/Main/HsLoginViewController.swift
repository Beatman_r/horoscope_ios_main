//
//  HsLoginViewController.swift
//  Horoscope
//
//  Created by Wang on 16/12/19.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class HsLoginViewController: UIViewController,HsHUD,GIDSignInUIDelegate,ChooseHoroDelegate {
    
    weak var mevc:UIViewController?
    override func viewDidLoad(){
        super.viewDidLoad()
        AnaliticsManager.sendEvent(AnaliticsManager.log_in_show)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        horScan = AdaptiveUtils.screenWidth/375
        self.view.backgroundColor = UIColor.white
        self.view.layer.contents  = UIImage(named: "meBgImg.jpg")?.cgImage
        self.addLoginViews()
        self.addConstraint()
        self.setAuthData()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: createLeftItemButton())
    }
    
    func createLeftItemButton() ->UIButton{
        let button = UIButton()
        button.setImage(UIImage(named: "meback_"), for: UIControlState())
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        return button
    }
    
    @objc func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func googleLog() {
        if HSHelpCenter.sharedInstance.appAndDeviceTool.deviceNetConnectionAvailiable()==true{
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().signIn()
//            self.showProccessHUD()
        }else{
            self.toast("Sorry,network lost,please try again")
        }
    }
    
    func setAuthData() {
        let user = AccountManager.sharedInstance.getUserInfo()
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus != .anonymous && userStatus != .null{
            if let imgUrl = user?.avatar{
                self.topBgImageView.sd_setImage(with: URL(string:imgUrl), placeholderImage: UIImage(named:"ic_avatar_"))
            }
            self.FBLoginButton.isHidden=true
            self.googleLoginButton.isHidden=true
            self.SignOutBtn.isHidden = false
            self.titleLab.attributedText =  HSHelpCenter.sharedInstance.textTool.setTextLineSpace("\(user?.userName ?? "")\n\(user?.horoname?.capitalized ?? "")", space: 1.5)
            self.titleLab.textAlignment = .center
            self.titleLab.isHidden = false
            self.horoIma.isHidden = true
           }else{
            self.topBgImageView.image = UIImage(named:"ic_avatar_")
            self.FBLoginButton.isHidden=false
            self.googleLoginButton.isHidden=false
            self.SignOutBtn.isHidden = true
            self.titleLab.isHidden = true
            self.horoIma.isHidden = false
            self.titleLab.text=NSLocalizedString("Horoscope", comment: "")
        }
    }
    
    func createSignOutBtn() {
        
    }

    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
//        self.hideHUD()
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        self.hideHUD()
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        AnaliticsManager.sendEvent(AnaliticsManager.log_in_succeed)
        viewController.dismiss(animated: true, completion: nil)
        self.showProccessHUD()
    }
    
    @objc func login() {
        if HSHelpCenter.sharedInstance.appAndDeviceTool.deviceNetConnectionAvailiable()==true{
            AccountManager.sharedInstance.loginWithType(.faceBookUser,
                                                        viewcontroller: self,
                                                        data: nil,
                                                        logInSuccess: {
                                                            self.hideHUD()
                                                            let vc = ChooseHoroViewController()
                                                            vc.delegate=self
                                                            AnaliticsManager.sendEvent(AnaliticsManager.log_in_succeed)
                                                            self.present(vc, animated: true, completion: {
                                                                
                                                            })
                                                        
            }) {
                self.hideHUD()
                self.showErrorHUD()
            }
        }else{
            self.toast("Sorry,network lost,please try again")
        }
    }
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.textAlignment = .center
        lab.numberOfLines = 0
        lab.text = NSLocalizedString("Horoscope", comment: "")
        lab.textColor = UIColor.commonPinkColor()
        lab.font = HSFont.baseRegularFont(20)
        return lab
    }()
    
    var topBgImageView : UIImageView = {
        var imageView = UIImageView()
        imageView.layer.masksToBounds=true
        imageView.layer.cornerRadius=49
        imageView.image = UIImage(named: "ic_avatar_")
        return imageView
    }()
    
    var horoIma : UIImageView = {
        var imageView = UIImageView()
         imageView.image = UIImage(named: "horoscope_")
        return imageView
    }()
    
    var FBLoginButton : UIButton = {
        var googleButton = UIButton()
        googleButton.setTitle(NSLocalizedString("Login with Facebook", comment: ""), for: UIControlState())
        googleButton.titleLabel?.font = HSFont.baseRegularFont(18)
        googleButton.setTitleColor(UIColor.FBLoginButtonColor(), for: UIControlState())
        googleButton.layer.borderColor = UIColor.FBLoginButtonColor().cgColor
        googleButton.layer.borderWidth = 1
        googleButton.layer.masksToBounds = true
        googleButton.layer.cornerRadius=22
        return googleButton
    }()
    
    var SignOutBtn : UIButton = {
        var googleButton = UIButton()
        googleButton.setTitle(NSLocalizedString("Sign Out", comment: ""), for: UIControlState())
        googleButton.titleLabel?.font = HSFont.baseRegularFont(18)
        googleButton.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        googleButton.layer.borderWidth = 1
        googleButton.layer.borderColor = UIColor.commonPinkColor().cgColor
        googleButton.layer.masksToBounds = true
        googleButton.layer.cornerRadius=22
        return googleButton
    }()
    
    var googleLoginButton : UIButton = {
        var googleButton = UIButton()
        googleButton.setTitle(NSLocalizedString("Login with Google", comment: ""), for: UIControlState())
        googleButton.titleLabel?.font = HSFont.baseRegularFont(18)
        googleButton.setTitleColor(UIColor.GoogleLoginButtonColor(), for: UIControlState())
        googleButton.layer.borderColor = UIColor.GoogleLoginButtonColor().cgColor
        googleButton.layer.borderWidth = 1
        googleButton.layer.masksToBounds = true
        googleButton.layer.cornerRadius=22
        return googleButton
    }()
    
    func addLoginViews() {
        self.view.addSubview(topBgImageView)
        self.view.addSubview(SignOutBtn)
        self.view.addSubview(googleLoginButton)
        self.view.addSubview(FBLoginButton)
        self.view.addSubview(titleLab)
        self.view.addSubview(horoIma)
        SignOutBtn.addTarget(self, action: #selector(logOut), for: .touchUpInside)
        FBLoginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        googleLoginButton.addTarget(self, action: #selector(googleLog), for: .touchUpInside)
     }
    
    @objc func logOut() {
        self.showProccessHUD()
        AccountManager.sharedInstance.logout({
            self.hideHUD()
            self.toast(NSLocalizedString("You have Sign Out", comment: ""))
            self.setAuthData()
            }) {
            self.showErrorHUD()
        }
    }
   
    var horScan:CGFloat? = 1
    func addConstraint() {
        self.FBLoginButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(self.view.snp.bottom).offset(-200)
            make.width.equalTo(274*horScan!)
            make.height.equalTo(45)
        }
        self.googleLoginButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(self.FBLoginButton.snp.bottom).offset(40)
            make.width.equalTo(274*horScan!)
            make.height.equalTo(45)
        }
        
        self.SignOutBtn.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(self.FBLoginButton.snp.bottom).offset(40)
            make.width.equalTo(274*horScan!)
            make.height.equalTo(45)
        }
        
        self.topBgImageView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(40+64)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.width.equalTo(98)
        }
        
        self.horoIma.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(topBgImageView.snp.bottom).offset(28)
            make.width.equalTo(146)
            make.height.equalTo(24)
        }
        
        self.titleLab.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(topBgImageView.snp.bottom).offset(28)
        }
    }
    
    func chooseCompleteToRefresh() {
         self.setAuthData()
    }
   
    func jumpToHome() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
     /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
