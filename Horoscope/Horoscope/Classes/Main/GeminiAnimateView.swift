//
//  GeminiAnimateView.swift
//  Horoscope
//
//  Created by Beatman on 17/1/20.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class GeminiAnimateView: BaseZodiacAnimateView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createBasicUI()
        self.addToView()
        self.addViewConstraint()
    }
        
    let timeList = [1,2,3]
    func createBasicUI() {
        self.bgColorView.image = UIImage(named: "img_gemini")

        self.starTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(subLightUpStar), userInfo: nil, repeats: true)
        self.shootingTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(subFireShootingStar), userInfo: nil, repeats: true)
    }
    
    override func startAnimation() {
        self.starTimer?.fireDate = Date.init(timeIntervalSinceNow: 0)
        self.shootingTimer?.fireDate = Date.init(timeIntervalSinceNow: 0)
    }
    
    override func stopAnimate() {
        self.starTimer?.invalidate()
        self.shootingTimer?.invalidate()
    }
    
    @objc func subFireShootingStar() {
        self.fireShootingStar(self.shootingStarView)
    }
    
    @objc func subLightUpStar() {
        self.lightUpStar(self.frontStarView, backView: self.backStarView)
    }
    
    func addToView() {
        self.addSubview(bgColorView)
        self.addSubview(shootingStarView)
    }
    
    func addViewConstraint() {
        self.bgColorView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.snp.edges)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}
