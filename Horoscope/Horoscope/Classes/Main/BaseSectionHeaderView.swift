//
//  BaseSectionHeaderView.swift
//  Horoscope
//
//  Created by Wang on 16/12/8.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class BaseSectionHeaderView: UIView {

    func createUI() {
        self.addSubview(iconIma)
        self.addSubview(leftTitleLab)
        self.addSubview(seeAllBtn)
    }
    
    func addConstrains() {
         iconIma.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left).offset(24)
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom).offset(-8)
        }
    }
    
    let iconIma:UIImageView = {
        let ima = UIImageView()
        ima.backgroundColor = UIColor.black
        ima.layer.masksToBounds = true
        ima.layer.cornerRadius = 20
        return ima
    }()
    
    let leftTitleLab:UILabel = {
        let lab = UILabel()
        lab.textAlignment = .left
        lab.text = "head"
        lab.font = HSFont.baseTitleFont()
        lab.textColor = UIColor.hsTextColor(1)
        return lab
    }()
    
    let seeAllBtn:UIButton = {
        let button = UIButton(type:.custom)
        button.setTitle("See All", for: UIControlState())
        return button
    }()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
