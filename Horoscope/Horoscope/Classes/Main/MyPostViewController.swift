//
//  MyPostViewController.swift
//  Horoscope
//
//  Created by Wang on 17/2/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class MyPostViewController: BaseViewController,NormalPostCellDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        self.title=NSLocalizedString("My posts", comment: "")
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        initViewModel()
        
       // Do any additional setup after loading the view.
    }
    
    var viewModel:MyPostListViewmodel?
    func initViewModel(){
        viewModel = MyPostListViewmodel()
        viewModel?.loadTopicList({ 
            self.failedRetryView?.loadSuccess()
            if self.viewModel?.count == 0 {
                let blankLabel = UILabel()
                blankLabel.text = "No post yet! Go and roast something now!"
                blankLabel.textColor = UIColor.luckyPurpleTitleColor()
                blankLabel.font = HSFont.baseRegularFont(20)
                blankLabel.numberOfLines = 0
                blankLabel.textAlignment = .center
                self.view.addSubview(blankLabel)
                self.view.bringSubview(toFront: blankLabel)
                blankLabel.snp.makeConstraints({ (make) in
                    make.center.equalTo(self.view)
                    make.width.equalTo(self.view).offset(-24)
                })
            }
            self.baseTableView?.reloadData()
            }, failed: { 
            self.failedRetryView?.loadFailed()
         })
    }

    
    
    //RetryViewDelegate
    override func retryToLoadData() {
        viewModel?.loadTopicList({
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            }, failed: {
            self.failedRetryView?.loadFailed()
       })
    }
    
    override func registerCell() {
        self.baseTableView?.register(BaseTopicCell.self, forCellReuseIdentifier: "BaseTopicCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    //MARK:praiseOrtreadePost
    func praiseOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOneNormalTopic(name, action: action, value: value, id: id)
    }
    
    func tradeOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOrPraiseAction(name, action: action, value: value, id: id)
    }
    
    func tradeOrPraiseAction(_ name: String, action: String, value: Bool, id: String) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.viewModel?.dao.likeOrDislikeAction(name, action: action, value: value, id: id)
        }
    }
    
    //MARK:tableviewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return viewModel?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BaseTopicCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
        let model = viewModel?.getModel(indexPath.row)
        cell.model = model
        cell.rootVc=self
        cell.delegate = self
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = viewModel?.getModel(indexPath.row)
        let vc = ReadPostViewController()
        vc.postModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = viewModel?.getModel(indexPath.row)
        return BaseTopicCell.calculateHeight(model)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
