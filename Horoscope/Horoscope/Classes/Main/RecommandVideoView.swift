//
//  RecommandVideoView.swift
//  Horoscope
//
//  Created by Wang on 16/12/13.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class RecommandVideoView: BaseOperationView {
    weak var rootVc:UIViewController?
    static let height = (UIScreen.main.bounds.size.width)/375 * 210
    class func create(_ model:CampaignModel,viewController:UIViewController?,index:Int) ->RecommandVideoView{
        let view = RecommandVideoView()
        view.editUI()
        view.rootVc=viewController
        view.addConstrains()
        view.model=model
        view.tapAction()
        view.position=index
        return view
    }
    var position:Int = 0
    var model:CampaignModel?{
        didSet{
            renderView()
        }
    }
    
    func tapAction() {
        let tap=UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(self.onSelectedCurrentView))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled=true
    }
    
    var durationLab:UILabel?
    func addDurationLab() {
        durationLab=UILabel()
        durationLab?.textColor=UIColor.white
        durationLab?.textAlignment = .right
        durationLab?.font=HSFont.baseContentFont()
        self.addSubview(durationLab!)
        durationLab?.text="5:56"
        durationLab?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(self.img.snp.bottom).offset(-5)
            make.right.equalTo(self.img.snp.right).offset(-8)
        })
   }
    
    @objc func onSelectedCurrentView() {
        AnaliticsManager.sendEvent(AnaliticsManager.video_banner_click, data: ["position":"\(self.position)"])
      }
    
    func renderView() {
        if self.model != nil {
           if let authorImaStr = model?.figure {
           img.sd_setImage(with: URL(string:authorImaStr), placeholderImage: UIImage(named:"defaultImg1.jpg"))
           }
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
