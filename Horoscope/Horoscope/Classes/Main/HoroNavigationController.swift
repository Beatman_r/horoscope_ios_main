//
//  MainNavigationController.swift
//  Horoscope
//
//  Created by Beatman on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class HoroNavigationController: UINavigationController {
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        // rewrite backButton and hide tabbar
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true;
            let backBtn = UIButton(type:.custom)
            backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
            backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
            backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
        }
        super.pushViewController(viewController, animated: true)
    }
    
    @objc func popView() {
        self.popViewController(animated: true)
    }
    
    override var shouldAutorotate : Bool {
        return self.topViewController!.shouldAutorotate
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return self.topViewController!.supportedInterfaceOrientations
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return self.topViewController!.preferredStatusBarStyle
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension HoroNavigationController: UIGestureRecognizerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactivePopGestureRecognizer?.delegate = self;
        //refactor: comment should write in english
        // 设置导航栏样式
        //refactor: wrong name, rename navBar to apperance
        self.automaticallyAdjustsScrollViewInsets = false
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        let navBarApperance = UINavigationBar.appearance()
        navBarApperance.barStyle = .blackTranslucent
        navBarApperance.barTintColor = UIColor.baseNavigationBarColor()
        navBarApperance.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: HSFont.baseRegularFont(20)]
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return self.childViewControllers.count > 1
    }
}
