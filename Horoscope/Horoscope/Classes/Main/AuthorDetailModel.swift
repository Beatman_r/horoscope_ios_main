//
//  AuthorDetailModel.swift
//  Horoscope
//
//  Created by Wang on 16/12/10.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class AuthorDetailModel: NSObject {
    var authorName:String?
    var title : String?
    var fromUrl:String?
    var viewCount:Int = 0
    var avatar:String?
}
