//
//  MyPostModel.swift
//  Horoscope
//
//  Created by Beatman on 17/2/16.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyPostModel: NSObject {
    
    var dislikeCount : Int?
    var likeCount : Int?
    var commentCount : Int?
    var anonymous : Bool?
    var postId : String?
    var createTime : Int64?
    var tagList : [AnyObject]?
    var viewCount : Int?
    var content : String?
    var createTimeHuman : String?
    var shareCount : Int?
    var imageList : [AnyObject]?
    var myVideo : NSNull?
    var user:User?
    var total : Int?
    var size : Int?
    var iliked:Bool? = false
    var iDisliked:Bool? = false
    
    var commentList:[MainComment]?
    var video : VideoPostModel?
    
    init(postId : String){
        self.postId = postId
    }
    
    init(model:ReadPostModel?) {
        self.dislikeCount = model?.dislikeCount
        self.likeCount = model?.likeCount
        self.commentCount = model?.commentCount
        self.anonymous = model?.anonymous
        self.postId = model?.postId
        self.createTime = model?.createTime
        self.tagList = model?.tagList
        self.viewCount = model?.viewCount
        self.content = model?.content
        self.createTimeHuman = model?.createTimeHuman
        self.shareCount = model?.shareCount
        self.imageList = model?.imageList
        self.user = model?.user
        self.iliked = model?.iliked
        self.iDisliked = model?.iDisliked
    }
    
    init(jsonData : JSON, num : Int) {
        super.init()
        self.dislikeCount = jsonData["postList"][num]["dislikeCount"].intValue 
        self.likeCount = jsonData["postList"][num]["likeCount"].intValue 
        self.commentCount = jsonData["postList"][num]["commentCount"].intValue 
        self.myVideo = jsonData["postList"][num]["video"].object as? NSNull
        self.anonymous = jsonData["postList"][num]["anonymous"].boolValue 
        self.postId = jsonData["postList"][num]["postId"].stringValue 
        self.createTime = jsonData["postList"][num]["createTime"].int64Value
        self.tagList = jsonData["postList"][num]["tagList"].arrayObject as [AnyObject]?
        self.viewCount = jsonData["postList"][num]["viewCount"].intValue 
        self.content = jsonData["postList"][num]["content"].stringValue 
        self.shareCount = jsonData["postList"][num]["shareCount"].intValue 
        self.createTimeHuman = jsonData["postList"][num]["createTimeHuman"].stringValue 
        self.imageList = jsonData["postList"][num]["imageList"].arrayObject as [AnyObject]?
        self.total = jsonData["postList"][num]["total"].intValue 
        self.size = jsonData["postList"][num]["size"].intValue 
        self.iliked = jsonData["postList"][num]["isLiked"].boolValue
        self.iDisliked = jsonData["postList"][num]["isDisliked"].boolValue
        if let  userJson = jsonData["postList"][num]["userInfo"].dictionaryObject{
            let user = User()
            user.initWitchDic(userJson as [String : AnyObject])
            self.user = user
        }
    }
    
    init(jsonData : JSON){
        super.init()
        self.dislikeCount = jsonData["dislikeCount"].intValue 
        self.likeCount = jsonData["likeCount"].intValue 
        self.commentCount = jsonData["commentCount"].intValue 
        self.anonymous = jsonData["anonymous"].boolValue 
        self.postId = jsonData["postId"].stringValue 
        self.createTime = jsonData["createTime"].int64Value
        self.tagList = jsonData["tagList"].arrayValue as [AnyObject]
        self.viewCount = jsonData["viewCount"].intValue 
        self.content = jsonData["content"].stringValue 
        self.shareCount = jsonData["shareCount"].intValue 
        self.createTimeHuman = jsonData["createTimeHuman"].stringValue
        self.imageList = jsonData["imageList"].arrayValue as [AnyObject]
        if let  userJson = jsonData["userInfo"].dictionaryObject{
            let user = User()
            user.initWitchDic(userJson as [String : AnyObject])
            self.user = user
        }
        if let videoData = jsonData["postList"][0]["video"].dictionaryObject{
            let video = VideoPostModel(dic: videoData as [String : AnyObject])
            self.video = video
        }
        self.iliked = jsonData["iliked"].boolValue
        self.iDisliked = jsonData["isDisliked"].boolValue
    }
}
