//
//  RateUsFullView.swift
//  Horoscope
//
//  Created by Beatman on 16/12/20.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

class RateUsFullView: RateUsView {
    
    let backView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(hexString: "000000", withAlpha: 0.6)
        self.createRateUsSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func getRateUsLimitShowCount() -> Int {
        
        return 3
    }
    
    func createRateUsLabel(_ content:String) -> UILabel {
        let label = UILabel()
        label.text = content
        label.textColor = UIColor.clear
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }
    
    @objc func dismissRateView() {
        self.removeFromSuperview()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let currentPoint = touch?.location(in: self)
        if backView.frame.contains(currentPoint!)  == false {
            self.dismissRateView()
        }
    }
    
    func createRateUsSubviews() {
        let bgView = UIView()
        bgView.backgroundColor = UIColor.clear
        bgView.frame = UIScreen.main.bounds
        
        backView.backgroundColor = UIColor.clear
        backView.layer.cornerRadius = 12
        backView.layer.masksToBounds = true
        
        let topImg = UIImageView()
        topImg.image = UIImage(named: "rateUsFrame.jpg")
        topImg.isUserInteractionEnabled = true
        let starView = createStarView()
        
        let closeButton = UIButton()
        closeButton.addTarget(self, action: #selector(dismissRateView), for: .touchUpInside)
        closeButton.setImage(UIImage(named: "rateUsClose"), for: UIControlState())
        self.addSubview(closeButton)
        
        self.addSubview(bgView)
        bgView.addSubview(backView)
        backView.addSubview(topImg)
        backView.addSubview(starView)
        
        backView.snp.makeConstraints { (make) in
            make.left.equalTo(bgView.snp.left).offset(32)
            make.right.equalTo(bgView.snp.right).offset(-32)
            make.top.equalTo(bgView.snp.top).offset(screenWidth * 157/375)
            make.bottom.equalTo(bgView.snp.bottom).offset(-screenWidth * 108/375)
        }
        
        topImg.snp.makeConstraints { (make) in
            make.top.equalTo(backView.snp.top)
            make.left.equalTo(backView.snp.left)
            make.right.equalTo(backView.snp.right)
            make.bottom.equalTo(backView.snp.bottom)
        }
        
        closeButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.snp.top).offset(31)
            make.right.equalTo(self.snp.right).offset(-22)
            make.height.equalTo(50)
            make.width.equalTo(50)
        }
        
        starView.snp.makeConstraints { (make) in
            make.left.equalTo(backView.snp.left)
            make.right.equalTo(backView.snp.right)
            make.bottom.equalTo(backView.snp.bottom).offset(-screenWidth * 33/375)
            make.top.equalTo(starView.snp.bottom).offset(-screenWidth*0.1).offset(-screenWidth * 33/375)
        }
    }
    
    func createStarView() ->UIView{
        let starView = UIView()
        
        for index in 0...4 {
            let bgButton:UIButton = {
                let button = UIButton(type: .custom)
                button.tag = index + 10
                button.frame = CGRect(x: screenWidth*0.14+CGFloat(index)*screenWidth*0.12, y: 0, width: screenWidth*0.08, height: screenWidth*0.08)
                button.addTarget(self, action: #selector(rateButtonOnClick(_:)), for: .touchUpInside)
                button.setImage(UIImage(named:"ic_rate_star_empty_small")?.withRenderingMode(.alwaysOriginal), for: UIControlState())
                return button
            } ()
            starView.addSubview(bgButton)
            let fButton:UIButton = {
                let button = UIButton(type: .custom)
                button.alpha = 0
                button.tag = index + 10
                button.addTarget(self, action: #selector(rateButtonOnClick(_:)), for: .touchUpInside)
                button.frame = CGRect(x: screenWidth*0.14+CGFloat(index)*screenWidth*0.12, y: 0, width: screenWidth*0.08, height: screenWidth*0.08)
                button.setImage(UIImage(named:"card_star")?.withRenderingMode(.alwaysOriginal), for: UIControlState())
                return button
            } ()
            self.buttons.append(fButton)
            self.bgButtons.append(bgButton)
            starView.addSubview(fButton)
        }
        self.createTimer()
        return starView
    }
    
    
    override func rateButtonOnClick(_ sender:UIButton) {
        if sender.tag-10 == 4 {
            let urlString = "itms-apps://itunes.apple.com/app/id1184938845"
            let url = URL(string: urlString)
            UIApplication.shared.openURL(url!)
            let notiCenter = NotificationCenter.default
            notiCenter.post(name: Notification.Name(rawValue: "rateButtonOnClick"), object: nil)
            self.removeFromSuperview()
           
        }else{
            let notiCenter = NotificationCenter.default
            notiCenter.post(name: Notification.Name(rawValue: "rateButtonOnClick"), object: nil)
            let rateDetailVC = RateDetailViewController()
            rateDetailVC.starRank = sender.tag+200-10
            self.rootvc?.navigationController?.pushViewController(rateDetailVC, animated: true)
            self.removeFromSuperview()
            
        }
        self.ratedOrCanceled()
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
