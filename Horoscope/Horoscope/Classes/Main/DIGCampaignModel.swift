//
//  DIGCampaignModel.swift
//  Horoscope
//
//  Created by Beatman on 2017/5/2.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

enum DIGCampaignCategory:Int {
    case love
    case tarot
    case number
}

class DIGCampaignModel: BaseCardListModel {
    var title:String?
    var cate:DIGCampaignCategory?
    var figure:String?
    var tarotImgName : String?
    var number : String?
    init(dic:[String:AnyObject]) {
        super.init()
        self.title            = dic["title"] as? String
        self.figure          = dic["figure"] as? String
    }
}
