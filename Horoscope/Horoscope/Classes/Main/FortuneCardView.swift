//
//  FortuneCardView.swift
//  Horoscope
//
//  Created by Wang on 16/12/7.
//  Copyright © 2016年 meevii. All rights reserved.


import UIKit

class FortuneCardView: BaseOperationView {
 
    weak var rootVc:UIViewController?
    class func create(_ imgName:String,title:String,viewController:UIViewController?) ->FortuneCardView{
        let view = FortuneCardView()
        view.editUI()
//        view.titleLab.text = title
        view.addConstrains()
        view.img.image=UIImage(named: imgName)
        view.rootVc = viewController
        view.addPushAction()
        return view
    }
    
    func addPushAction(){
        let tap=UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(self.onSelectedCurrentCell))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled=true
    }
    
    @objc func onSelectedCurrentCell() {
        AnaliticsManager.sendEvent(AnaliticsManager.forecast_banner_click, data: ["sign":"cookie"])
        self.rootVc?.navigationController?.pushViewController(CookieViewController(), animated: true)
    }

    
}
