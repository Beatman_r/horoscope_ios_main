//
//  AuthorDetailCell.swift
//  Horoscope
//
//  Created by Wang on 16/12/9.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}




class AuthorDetailCell: BaseCardCell {
    let screenWidth = UIScreen.main.bounds.width
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addUI()
        self.addConstrains()
        
        let openUrltap = UITapGestureRecognizer()
        openUrltap.addTarget(self, action: #selector(self.openUrl))
        fromUrlLab.addGestureRecognizer(openUrltap)
        fromUrlLab.isUserInteractionEnabled = true
        self.cornerBackView?.backgroundColor = UIColor.baseCardBackgroundColor()
        self.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.contentView.backgroundColor = UIColor.baseDetailBackgroundColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var detailModel:BreadModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        if detailModel != nil{
            if let authorImaStr = detailModel?.authorAvatar {
               self.authorImg.sd_setImage(with: URL(string: authorImaStr), placeholderImage: UIImage(named:"icon_author"))
            }
            titleLab.text=detailModel?.title
            authorNameLab.text=NSLocalizedString("By", comment: "")+" "+(detailModel?.author ?? "")
            if detailModel?.viewCount <= 1{
                viewCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((detailModel?.viewCount) ?? 0) + " " + NSLocalizedString("view", comment: "")
            }else{
                viewCountLab.text=HSHelpCenter.sharedInstance.textTool.getFormattedNumber((detailModel?.viewCount) ?? 0)+" "+NSLocalizedString("views", comment: "")
            }
            let urlS:NSString=(detailModel?.authorWebsite as NSString? ?? "")
            let urlSafter=urlS.addingPercentEscapes(using: String.Encoding.utf8.rawValue)
            let url = URL(string:urlSafter ?? "")
            let urlHost = (url?.host) ?? ""
            let headString = NSLocalizedString("From", comment: "")+":"+" "
            fromUrlLab.text=NSLocalizedString("From", comment: "")+":"+" "+urlHost
            
            let AttributedStr1: NSMutableAttributedString = NSMutableAttributedString(string: headString + urlHost)
            AttributedStr1.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue,
                                        range: NSMakeRange(headString.count, (urlHost.count)))
            fromUrlLab.attributedText = AttributedStr1
        }
    }
    
    func addConstrains() {
        self.cornerBackView?.snp.remakeConstraints { (make) in
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
            make.top.equalTo(self.contentView.snp.top)
            make.bottom.equalTo(self.contentView.snp.bottom)
        }
        
        self.backImg.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left)
            make.right.equalTo(self.cornerBackView!.snp.right)
            make.top.equalTo(self.cornerBackView!.snp.top)
            make.bottom.equalTo(self.cornerBackView!.snp.bottom)
        }
        
        self.titleLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!.snp.left).offset(12)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-12)
            make.top.equalTo(self.cornerBackView!.snp.top).offset(11)
        }
        
        self.authorBackImg.snp.makeConstraints { (make) in
            make.left.equalTo(self.titleLab.snp.left)
            make.top.equalTo(self.titleLab.snp.bottom).offset(12)
            make.height.equalTo(48)
            make.width.equalTo(48)
        }
        
        self.authorImg.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.authorBackImg)
            make.centerY.equalTo(self.authorBackImg)
            make.height.equalTo(45)
            make.width.equalTo(45)
        }
        
        self.authorNameLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.authorBackImg.snp.top)
            make.left.equalTo(self.authorBackImg.snp.right).offset(11)
        }
        
        self.viewCountLab.snp.makeConstraints { (make) in
            make.left.leading.equalTo(self.authorNameLab)
            make.top.equalTo(self.authorNameLab.snp.bottom).offset(5)
        }
        
        self.fromUrlLab.snp.makeConstraints { (make) in
            make.left.leading.equalTo(self.authorNameLab)
            make.top.equalTo(self.viewCountLab.snp.bottom).offset(5)
        }
        
//        let maskLayer = CAShapeLayer()
//        maskLayer.path = UIBezierPath(roundedRect: (authorImg.bounds), byRoundingCorners: [.AllCorners],
//                                      cornerRadii: CGSizeMake(22, 22)).CGPath
//        maskLayer.frame = (authorImg.bounds)
//        authorImg.layer.mask = maskLayer
    }
    
    func addUI() {
        self.cornerBackView?.addSubview(backImg)
        self.cornerBackView?.addSubview(authorBackImg)
        self.cornerBackView?.addSubview(authorImg)
        self.cornerBackView?.addSubview(authorNameLab)
        self.cornerBackView?.addSubview(titleLab)
        self.cornerBackView?.addSubview(viewCountLab)
        self.cornerBackView?.addSubview(fromUrlLab)
     }
    
    @objc func openUrl() {
        let url = URL(string: (detailModel?.originalUrl) ?? "")
        if let sourecUrl = url{
            if UIApplication.shared.canOpenURL(sourecUrl){
                UIApplication.shared.openURL(sourecUrl)
            }
        }
    }
    
    let backImg:UIImageView = {
        let view = UIImageView()
        view.backgroundColor=UIColor.clear
        return view
    }()
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 0
        lab.textAlignment = .left
        
        lab.font=HSFont.baseRegularFont(20)
        lab.textColor = UIColor.luckyPurpleTitleColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    let authorNameLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 1
        lab.textAlignment = .left
        lab.font=HSFont.baseMediumFont(18)
        lab.textColor = UIColor.purpleContentColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    let viewCountLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 1
        lab.textAlignment = .left
        lab.font=HSFont.baseMediumFont(15)
        lab.textColor = UIColor.purpleContentColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    let fromUrlLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 1
        lab.textAlignment = .left
        lab.font=HSFont.baseMediumFont(15)
        lab.textColor = UIColor.purpleContentColor()
        lab.backgroundColor=UIColor.clear
        return lab
    }()
    
    let authorBackImg:UIImageView = {
        let view = UIImageView()
        view.image=UIImage(named:"authorCover")
        view.backgroundColor=UIColor.white
        view.layer.cornerRadius = 24
        view.layer.masksToBounds = true
        return view
    }()
    
    let authorImg:UIImageView = {
        let view = UIImageView()
        view.layer.masksToBounds=true
        view.layer.cornerRadius=22
        return view
    }()
    
    class func calculateHeight(_ title:String)->CGFloat{
        var titleHeight=HSHelpCenter.sharedInstance.textTool.calculateStringHeight(paramString: title, fontSize: 20, font: HSFont.baseMediumFont(20), width: UIScreen.main.bounds.size.width-30, multyplyLineSpace: 1)
        titleHeight += (AdaptiveUtils.screenWidth < 350 ? 105 : 110 )
        return titleHeight
    }
    
    static let cellId="AuthorDetailCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath,model:BreadModel?) -> AuthorDetailCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! AuthorDetailCell
        cell.detailModel=model
        return cell
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
