//
//  DeviveUserInfo.swift
//  bibleverse
//
//  Created by Wang on 16/8/2.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class UserInfo: NSObject, NSCoding {
    
    var userName: String!
    var gid: String!
    var uid: String!
    var token: String?
    var avatar: String?
    var email: String?
    var source: String?
    var sourceId: String?
    var horoname:String?
    var birthday:String?
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.userName ?? "", forKey: "userName")
        aCoder.encode(self.gid, forKey: "gid")
        aCoder.encode(self.uid, forKey: "uid")
        aCoder.encode(self.token ?? "", forKey: "token")
        aCoder.encode(self.avatar ?? "", forKey: "avatar")
        aCoder.encode(self.horoname ?? "", forKey: "horoname")
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        self.userName = (aDecoder.decodeObject(forKey: "userName") as? String) ?? ""
        self.gid = aDecoder.decodeObject(forKey: "gid") as? String ?? ""
        self.uid = aDecoder.decodeObject(forKey: "uid") as? String ?? ""
        self.token = (aDecoder.decodeObject(forKey: "token") as? String) ?? ""
        self.avatar = (aDecoder.decodeObject(forKey: "avatar") as? String) ?? ""
        self.horoname = (aDecoder.decodeObject(forKey: "horoname") as? String) ?? ""
    }
    
    override init() {
        
    }
    
    class func createLocalInfo() -> UserInfo {
        let info = UserInfo()
        info.uid = "dev:" + HSHelpCenter.sharedInstance.appAndDeviceTool.deviceUUID()
        info.userName = ""
        info.avatar = ""
        info.gid = self.getGid(info.uid)
        info.token = ""
        return info
    }
    
    class func create(_ sourceDis:[String:AnyObject],name:String,avatar:String) -> UserInfo {
        let firebaseAuth = sourceDis["firebaseAuth"] as! String
        let gid = sourceDis["gid"] as! String
        let uid = sourceDis["uid"] as! String
        let info = UserInfo()
        info.userName = name
        info.gid = gid
        info.uid = uid
        info.token = firebaseAuth
        info.avatar = avatar
        return info
    }
    
    class func getGid(_ uid:String) -> String {
        let str = uid.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(uid.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        CC_MD5(str!, strLen, result)
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        result.deallocate(capacity: digestLen)
        let strss = String(hash)
        let index = strss.characters.index(strss.startIndex, offsetBy: 3)
        let noteStr = strss.substring(to: index)
        return noteStr
    }
    
    class func getMd5(_ paramStr:String)->String{
        let str = paramStr.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(paramStr.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        CC_MD5(str!,strLen, result)
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        result.deallocate(capacity: digestLen)
        let md5Str = String(hash)
        return md5Str
    }
    
}
