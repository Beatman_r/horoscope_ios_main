//
//  MeViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/19.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit



class MeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,HsHUD{
    
    var verScan:CGFloat? = 1
    
    override func viewDidLoad() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: createLeftItemButton())
        self.view.layer.contents = UIImage(named: "meBgImg.jpg")?.cgImage
        createUI()
        verScan = AdaptiveUtils.screenHeight/667
        self.title = NSLocalizedString("Me", comment: "")
        addHeaderView()
        refreshAuthData()
        registerCell()
    }
 
    func createLeftItemButton() ->UIButton{
        let button = UIButton()
        button.setImage(UIImage(named: "meback_"), for: UIControlState())
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        return button
    }
    
    @objc func goBack() {
        if self.isKind(of: MeViewController.self){
            if (HoroTabBarViewController.gloableInstance?.selectedViewController as? UINavigationController) != nil {
                self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
                self.navigationController?.navigationBar.shadowImage = nil
            }
            _ = self.navigationController?.popViewController(animated: true)
        }else if self.isKind(of: MeViewController.self){
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            if let vc = self.navigationController?.viewControllers[1]{
                _ = self.navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
    
    var RowNum=0
    var tableview:UITableView?
    func createUI() {
        tableview = UITableView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), style: .plain)
        tableview?.delegate = self
        tableview?.separatorStyle = .none
        tableview?.dataSource = self
        tableview?.backgroundColor = UIColor.clear
        tableview?.separatorStyle = .none
        let footView=UIView()
        footView.frame = CGRect(x: 0, y: 0, width: 0, height: 10)
        footView.backgroundColor=UIColor.clear
        tableview?.tableFooterView=footView
        self.view.addSubview(tableview!)
   }
    
    func refreshAuthData(){
        let status = AccountManager.sharedInstance.getLogStatus()
        if status == .faceBookUser || status == .googleUser {
            self.RowNum = 5
            self.tableview?.reloadData()
        }else{
            self.RowNum = 5
            self.tableview?.reloadData()
        }
       self.userHeaderView?.userSetting()
    }
    
    func registerCell(){
        self.tableview?.register(MeRowCell.self, forCellReuseIdentifier: "MeRowCell")
    }
 
   var userHeaderView:MeHeaderView?
    func addHeaderView(){
        userHeaderView=MeHeaderView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 240*verScan!))
        userHeaderView?.rootVc=self
        self.tableview?.tableHeaderView=userHeaderView
    }
  
    //MARK:tableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     var rowNum = 0
        if indexPath.row == rowNum{
            let cell = MeRowCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell(NSLocalizedString("Add Horoscope", comment: ""))
            return cell
        }
        rowNum += 1
        if indexPath.row == rowNum{
            let cell = MeRowCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell(NSLocalizedString("My Notifications", comment: ""))
            return cell
        }
        rowNum += 1
        if indexPath.row == rowNum{
            let cell = MeRowCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell(NSLocalizedString("My Post", comment: ""))
            return cell
        }
        rowNum += 1
        if indexPath.row == rowNum{
            let cell = MeRowCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell(NSLocalizedString("My Comments", comment: ""))
            return cell
        }
        rowNum += 1
        if indexPath.row == rowNum{
            let cell = MeRowCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.renderCell(NSLocalizedString("Setting", comment: ""))
            cell.line.isHidden = true
            return cell
        }
       
        return UITableViewCell()
    }
    //MARK:tableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.RowNum
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69*verScan!
    }
    
    
    let categoryArr = ["add_horoscope","my_notification","my_posts","my_comments"]
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row <= 3{
            AnaliticsManager.sendEvent(AnaliticsManager.me_click, data: ["category":"\(categoryArr[indexPath.row])"])
        }
        var rowNum = 0
        if indexPath.row == rowNum{
            let vc = HorocopeListViewController()
            AnaliticsManager.sendEvent(AnaliticsManager.drawer_item_click, data: ["category":"add_horoscope"])
            self.navigationController?.pushViewController(vc, animated: true)
        }
        rowNum+=1
        if indexPath.row == rowNum{
            let userStatus = AccountManager.sharedInstance.getLogStatus()
            if userStatus == .null || userStatus == .anonymous{
                let vc = HsLoginViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = MyNotificationsViewController()
                AnaliticsManager.sendEvent(AnaliticsManager.drawer_item_click, data: ["category":"my_notification"])
                self.navigationController?.pushViewController(vc, animated: true)
            }
         }
        rowNum+=1
        if indexPath.row == rowNum{
            let userStatus = AccountManager.sharedInstance.getLogStatus()
            if userStatus == .null || userStatus == .anonymous{
                let vc = HsLoginViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = MyPostViewController()
                AnaliticsManager.sendEvent(AnaliticsManager.drawer_item_click, data: ["category":"my_post"])
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        rowNum += 1
        if indexPath.row == rowNum{
            let userStatus = AccountManager.sharedInstance.getLogStatus()
            if userStatus == .null || userStatus == .anonymous{
                let vc = HsLoginViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let commentVC = MyCommentsViewController()
                AnaliticsManager.sendEvent(AnaliticsManager.drawer_item_click, data: ["category":"my_comments"])
                self.navigationController?.pushViewController(commentVC, animated: true)
            }
        }
        if indexPath.row == 4{
            let settingVc = SettingViewController()
            self.navigationController?.pushViewController(settingVc, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.refreshAuthData()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
