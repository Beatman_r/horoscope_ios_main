//
//  NotificationViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/13.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

protocol NotificationGuideDelegate:class {
    func readyToPop()
 }


class NotificationViewController: UIViewController {
    weak var delegate:NotificationGuideDelegate?
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
         GuideAuthManager.sharedInstance.sendGuideViewShowAnaliticEvent()
        self.view.backgroundColor = UIColor.white
        self.addSubviews()
        self.addConstraint()
        self.view.layoutIfNeeded()
        self.setText()
        let notiCenter = NotificationCenter.default
        notiCenter.addObserver(self, selector: #selector(dismissVC), name: NSNotification.Name(rawValue: "RegisteredNotification"), object: nil)
    }
    
    let topBgImg : UIImageView = {
        let bgImageView = UIImageView()
        bgImageView.image = UIImage(named: "guideSplash.jpg")
        return bgImageView
    }()
    
    let titleLabel : UILabel = {
        let lab = UILabel()
        lab.font = HSFont.baseTitleFont()
        lab.font = UIFont.systemFont(ofSize: 20)
        lab.textColor = UIColor.white
        lab.textAlignment = NSTextAlignment.center
        lab.backgroundColor = UIColor.clear
        return lab
    }()
    
    let detailLabel : UILabel = {
        let lab = UILabel()
        lab.font = HSFont.baseContentFont()
        lab.font = UIFont.systemFont(ofSize: 16)
        lab.textColor = UIColor.white
        lab.textAlignment = NSTextAlignment.center
        lab.backgroundColor = UIColor.clear
        lab.numberOfLines = 0
        return lab
    }()
    
    let allowButton : UIButton = {
        let button = UIButton()
        button.setTitle("Allow", for: UIControlState())
        //MARK: addTarget
         return button
    }()
    
    let noThanksButton : UIButton = {
        let button = UIButton()
        button.setTitle("Maybe Later", for: UIControlState())
        button.setTitleColor(UIColor.flatSkyBlue, for: .normal)
        button.backgroundColor = UIColor.white
        
        return button
    }()
    
    let closeButton : UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "noti_close_"), for: UIControlState())
        return button
    }()
    
    func addSubviews () {
        self.view.addSubview(topBgImg)
        self.view.addSubview(titleLabel)
        self.view.addSubview(detailLabel)
        self.view.addSubview(allowButton)
        self.view.addSubview(noThanksButton)
        self.view.addSubview(closeButton)
        allowButton.addTarget(self, action: #selector(allowToRegister), for: .touchUpInside)
        noThanksButton.addTarget(self, action: #selector(dismissNotificationView), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(dismissNotificationView), for: .touchUpInside)
        closeButton.isHidden = true
    }
    
    func addConstraint() {
        let buttonHeight = 56
        let padding = screenWidth*0.048
        self.topBgImg.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left)
            make.right.equalTo(self.view.snp.right)
            make.top.equalTo(self.view.snp.top)
            make.bottom.equalTo(self.view.snp.bottom)//.offset(UIScreen.mainScreen().bounds.height*0.5 + CGFloat(padding*2))
        }
        
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(52)
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
        }
        
        self.detailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left).offset(24)
            make.right.equalTo(self.view.snp.right).offset(-24)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(12)
        }
        
        let testType = GuideAuthManager.sharedInstance.getGuideAuthType()
        if  testType == TestAuthPushType.allowType{
            self.allowButton.snp.makeConstraints { (make) in
                make.left.equalTo(noThanksButton.snp.right)
                make.right.equalTo(self.view.snp.right)
                make.bottom.equalTo(self.view.snp.bottom)
                make.height.equalTo(buttonHeight)
            }
            self.updateViewConstraints()
            allowButton.setTitle("Continue", for: UIControlState())
            allowButton.setTitleColor(UIColor.flatSkyBlue, for: .normal)
            allowButton.backgroundColor = UIColor.white
            }else if testType == TestAuthPushType.allow_laterType{
            self.noThanksButton.snp.makeConstraints { (make) in
                make.left.equalTo(self.view.snp.left)
                make.width.equalTo(UIScreen.main.bounds.size.width/2)
                make.bottom.equalTo(self.view.snp.bottom)
                make.height.equalTo(buttonHeight)
            }
            
            self.allowButton.snp.makeConstraints { (make) in
                make.left.equalTo(noThanksButton.snp.right)
                make.right.equalTo(self.view.snp.right)
                make.bottom.equalTo(self.view.snp.bottom)
                make.height.equalTo(buttonHeight)
            }
            self.updateViewConstraints()
            allowButton.setTitle("Allow", for: UIControlState())
            allowButton.setTitleColor(UIColor.white, for: UIControlState())
            allowButton.backgroundColor = UIColor.flatSkyBlue
        }
        
        self.closeButton.snp.makeConstraints { (make) in
            make.left.equalTo(topBgImg.snp.left).offset(padding/2)
            make.right.equalTo(topBgImg.snp.left).offset(4*padding)
            make.top.equalTo(topBgImg.snp.top).offset(padding/2)
            make.bottom.equalTo(topBgImg.snp.top).offset(4*padding)
        }
    }
    
    func setText() {
        let textArr = GuideAuthManager.sharedInstance.getGuideTextArr()
         titleLabel.text = textArr.first
         detailLabel.text = textArr.last
     }
    
    
    @objc func allowToRegister()  {
        self.delegate?.readyToPop()
        GuideAuthManager.sharedInstance.sendConfirmEvent()
        NotificationLimitManager.sharedInstance.registerLocalNotification()
        let notiCenter = NotificationCenter.default
        notiCenter.post(name: Notification.Name(rawValue: "AllowedToRegister"), object: nil)
    }
    
    @objc func dismissVC(){
         self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissNotificationView() {
        NotificationLimitManager.sharedInstance.refuseNotification()
        self.delegate?.readyToPop()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
