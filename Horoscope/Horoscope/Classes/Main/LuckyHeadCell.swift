//
//  LuckyHeadCell.swift
//  Horoscope
//
//  Created by Wang on 17/1/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import pop
import FBSDKShareKit
import IDMPhotoBrowser
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LuckyHeadCell: BaseCardCell {
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    var horoName = ""

    fileprivate var timer:Timer?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        self.createUI()
        self.addConstrains()
        self.contentView.backgroundColor = UIColor.clear
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(goSeePost))
    }
    
    @objc func goSeePost() {
        if let vc1 = self.rootVC {
            let vc = FortureListViewcontroller.create(vc1.horoName ?? "")
            vc.pageType = 1
            vc.currentIndex = 1
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    weak var rootVC : HomeTimeLineViewController?
    
    let titleLab:UILabel = {
        let lab = UILabel()
        lab.numberOfLines = 1
        lab.text = NSLocalizedString("Daily Horoscope", comment: "")
        lab.textAlignment = .left
        if #available(iOS 8.2, *) {
            lab.font = UIFont.systemFont(ofSize: 20.0, weight: UIFont.Weight.medium)
        } else {
            lab.font = UIFont.systemFont(ofSize: 20.0)
        }
        lab.textColor =  UIColor.white
       
        return lab
    }()
    
    let rssIma:UIImageView = {
        let ima = UIImageView()
        ima.image = UIImage(named:"topLogo.jpg")
        return ima
    }()
    
    let luckWordsLab:UILabel = {
        let lab = UILabel()
        lab.textAlignment = .left
        lab.lineBreakMode = NSLineBreakMode.byTruncatingTail
        lab.numberOfLines = 4
        if #available(iOS 8.2, *) {
            lab.font = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.regular)
        } else {
            lab.font = UIFont.systemFont(ofSize: 14.0)
        }
        lab.textColor =  UIColor.white
        
        return lab
    }()
    
    func createUI() {
        self.cornerBackView?.backgroundColor = UIColor.clear
        self.cornerBackView?.addSubview(luckWordsLab)
        self.cornerBackView?.addSubview(titleLab)
        self.cornerBackView?.addSubview(rssIma)
       
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(todayHoroscopeDidSelect))
        luckWordsLab.isUserInteractionEnabled = true
        luckWordsLab.addGestureRecognizer(gesture2)
        
        let rssImatap = UITapGestureRecognizer()
        rssImatap.addTarget(self, action: #selector(imaViewRssURL))
        self.rssIma.addGestureRecognizer(rssImatap)
        self.rssIma.isUserInteractionEnabled = true
    }
    
    @objc func imaViewRssURL()  {
         UIApplication.shared.openURL(URL(string: "https://www.tarot.com/?code=dailyinnovations&utm_medium=Referral&utm_source=DailyInnovations&utm_campaign=daily_app&utm_content=logo")!)
    }
    
    func authorRssTap()  {
        UIApplication.shared.openURL(URL(string: "https://www.tarot.com/bios/rick-levine/?code=dailyinnovations&utm_medium=Referral&utm_source=DailyInnovations&utm_campaign=daily_app&utm_content=rick-levine-bio")!)
    }
    
    
    func horoDidSelect(){
        AnaliticsManager.sendEvent(AnaliticsManager.main_card_click, data: ["category":"\(AnaliticsManager.character_dailog)"])
        if let vc1 = self.rootVC {
            let vc = CharacterListViewController.create(vc1.horoName ?? "")
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func todayHoroscopeDidSelect()  {
        AnaliticsManager.sendEvent(AnaliticsManager.main_card_click, data: ["category":"\(AnaliticsManager.today_horoscope)"])
        if let vc1 = self.rootVC {
            let vc = FortureListViewcontroller.create(vc1.horoName ?? "")
            vc.pageType = 1
            vc.currentIndex = 1
            self.rootVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static let cardHeight = 120
    
    func addConstrains() {
 
        self.contentView.snp.makeConstraints { (make) in
            make.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        self.titleLab.snp.makeConstraints { (make) in
            make.left.equalTo(self.cornerBackView!).offset(16)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-16)
            make.height.equalTo(28)
        }
        self.luckWordsLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLab.snp.bottom).offset(5)
            make.left.equalTo(self.titleLab)
            make.right.equalTo(self.cornerBackView!.snp.right).offset(-27)
        }
        self.rssIma.snp.makeConstraints { (make) in
            make.top.equalTo(self.luckWordsLab.snp.bottom).offset(5)
            make.left.equalTo(self.titleLab)
            make.height.equalTo(10.0)
            make.width.equalTo(self.rssIma.snp.height).multipliedBy((self.rssIma.image?.size.width)! / (self.rssIma.image?.size.height)!)
            make.bottom.equalToSuperview().offset(-5.0)
        }
    }
    
    var model:LuckyModel?{
        didSet{
            renderCell()
        }
    }
    
    func renderCell() {
        if model != nil {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 4.0
            paragraphStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail;
            paragraphStyle.alignment = .left
            let dict: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : self.luckWordsLab.font, NSAttributedStringKey.paragraphStyle : paragraphStyle];
            let attribuedString = NSAttributedString(string: model?.content ?? "", attributes: dict)
            self.luckWordsLab.attributedText = attribuedString
        }
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static let cellId="LuckyHeadCell"
    class func dequeueReusableCell(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> LuckyHeadCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! LuckyHeadCell
        return cell
    }
    
    class func calculateHeight(_ content:String) ->CGFloat{
        return 345
    }
    
    deinit {
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AppDelegate.StartApp"), object: nil)
    }
}
