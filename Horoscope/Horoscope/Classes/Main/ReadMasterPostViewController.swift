//
//  ReadMasterPostViewController.swift
//  Horoscope
//
//  Created by Wang on 17/2/28.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit


class ReadMasterPostViewController:BaseViewController,NormalPostCellDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        self.setupRefreshHeaderView()
        self.setupRefreshFooterView()
        self.title=NSLocalizedString("\(self.userName ?? "")", comment: "")
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        viewmodel = MasterViewmodel.init(userId: self.userId ?? "")
        requestData()
        // Do any additional setup after loading the view.
    }
    
    var userName:String?
    
    
    func requestData() {
        viewmodel?.loadAndRefreshPostList({
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_header.endRefreshing()
            }, failed: {
            self.failedRetryView?.loadFailed()
            self.baseTableView?.mj_header.endRefreshing()
        })
    }
    
    //RetryViewDelegate
    override func retryToLoadData() {
        viewmodel?.loadAndRefreshPostList({
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_header.endRefreshing()
            }, failed: {
            self.failedRetryView?.loadFailed()
            self.baseTableView?.mj_header.endRefreshing()
        })
    }
    
    override func refreshData() {
        viewmodel?.loadAndRefreshPostList({
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_header.endRefreshing()
            }, failed: {
                self.failedRetryView?.loadFailed()
                self.baseTableView?.mj_header.endRefreshing()
        })
    }
    
    override func loadMoreList() {
       viewmodel?.loadMorePostList({ (more) in
        if more == false{
            self.baseTableView?.mj_footer.endRefreshingWithNoMoreData()
            self.perform(#selector(self.delayReload),
                with: nil,
                afterDelay: 0.5)
            self.baseTableView?.reloadData()
        }else{
            self.baseTableView?.mj_footer.endRefreshing()
            self.baseTableView?.reloadData()
        }
        }, failed: { () in
            self.baseTableView?.mj_footer.endRefreshing()
            self.baseTableView?.reloadData()
        })
    }
    
    @objc func delayReload()  {
        self.baseTableView?.mj_footer.endRefreshing()
    }
    
    override func registerCell() {
        self.baseTableView?.register(VideoPostCell.self, forCellReuseIdentifier: "VideoPostCell")
        self.baseTableView?.register(NormalPostCell.self, forCellReuseIdentifier: "NormalPostCell")
    }
    
    //MARK:tableViewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  viewmodel?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewmodel?.getModel(indexPath.row)
        if model?.type == .video{
            let cell = VideoPostCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.model = model
            cell.rootVc = self
            cell.delegate = self
            return cell
        }
        
        if model?.type == .normal{
            let cell = NormalPostCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.readModel = model
            cell.rootVc=self
            cell.delegate = self
           return cell
        }
       return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = viewmodel?.getModel(indexPath.row)
        if model?.type == .video{
            let vc = VideoPlayViewController.create(model?.video?.richmedia, postId: model?.postId ?? "")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if model?.type == .normal{
            let vc = ReadPostViewController()
            vc.postModel = MyPostModel.init(model: model!)
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = viewmodel?.getModel(indexPath.row)
        if model?.type == .video{
            return VideoPostCell.calculateHeight(model)
        }
        if model?.type == .normal{
            return BaseTopicCell.calculateheight(model)
        }
        return 0
    }
    
    //MARK:praiseOrtreadePost
    func praiseOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOneNormalTopic(name, action: action, value: value, id: id)
    }
    
    func tradeOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOrPraiseAction(name, action: action, value: value, id: id)
    }
    
    func tradeOrPraiseAction(_ name: String, action: String, value: Bool, id: String) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            HoroscopeDAO.sharedInstance.likeOrDislikeAction(name, action: action, value: value, id: id)
        }
    }
    
    
    var viewmodel:MasterViewmodel?
    var userId:String?
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func create(_ uid:String) ->ReadMasterPostViewController{
        let vc = ReadMasterPostViewController()
        vc.userId = uid
        return vc
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
