//
//  TarotDefineController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class TarotDefineController: TarotBaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    var model : TarotCardModel!
    var tableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createTableView()
    }
    
    func createTableView() {
        self.tableView = UITableView(frame: CGRect(x: 0, y: 64, width: screenWidth, height: screenHeight-64), style: .plain)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.addSubview(tableView)
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorStyle = .none
        self.tableView.register(TarotTitleCell.self, forCellReuseIdentifier: "topCell")
        self.tableView.register(TarotDetailCell.self, forCellReuseIdentifier: "detailCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "topCell", for: indexPath) as! TarotTitleCell
            cell.cardImg.image = UIImage(named: self.model.imageName)
            cell.selectionStyle = .none
            cell.cardNameDetailLabel.text = self.model.cardName
            cell.keyWordDetailLabel.text = self.model.keyWord
            cell.astrologyDetailLabel.text = self.model.astrological
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! TarotDetailCell
            cell.selectionStyle = .none
            cell.titleLabel.text = NSLocalizedString("Explanation", comment: "")
            cell.detailLabel.text = model.content
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! TarotDetailCell
            cell.selectionStyle = .none
            cell.titleLabel.text = NSLocalizedString("Tarot's Meaning", comment: "")
            cell.detailLabel.text = model.detail
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return self.screenHeight * 190/667
        case 1:
            return HSHelpCenter().textTool.calculateStringHeight(paramString: self.model.content, fontSize: self.screenHeight == 480 ? 13 : 15, font: HSFont.baseRegularFont(self.screenHeight == 480 ? 13 : 15), width: self.tableView.frame.size.width-30, multyplyLineSpace: 1)+60
        case 2:
            return HSHelpCenter().textTool.calculateStringHeight(paramString: self.model.detail, fontSize: self.screenHeight == 480 ? 13 : 15, font: HSFont.baseRegularFont(self.screenHeight == 480 ? 13 : 15), width: self.tableView.frame.size.width-30, multyplyLineSpace: 1)+60
        default:
            return 0
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    deinit {
        LXSWLog("--------TarotDefineController")
//        self.resultContainer.removeFromSuperview()
    }

}
