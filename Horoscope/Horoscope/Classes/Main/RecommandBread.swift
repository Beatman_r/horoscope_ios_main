//
//  RecommandBread.swift
//  Horoscope
//
//  Created by Wang on 16/12/27.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class RecommandBread: NSObject {
     init(id:String) {
        super.init()
        self.breadId = id
     }
    
     var list:[BreadModel] = []
     fileprivate var breadId:String?
    
     var count:Int?{
        return list.count
     }
    
     func loadAlsoLikeData(_ success:@escaping ()-> (), failure:@escaping (_ errors:NSError) -> ()) {
         DataManager.sharedInstance.breadInfoDao.getRecommandBreadList(self.breadId ?? "", success: { (list) in
           self.list = list
           success()
        }) { (error) in
          failure(error)
        }
    }

}
