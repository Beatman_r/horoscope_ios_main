//
//  BaseHomeModel.swift
//  Horoscope
//
//  Created by Wang on 17/1/12.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

enum cardType:Int {
    case campaign
    case ad
    case dailyCard1
    case dailyCard2
    case forecast
    case todayHoroscope
    case bread
    case match
    case rateUs
    case dig
    case emptyHoroscope
    case survey
}

class BaseCardListModel: NSObject {
    var position:Int?
    var category:cardType?
    var figureImg:String?
    
    var surveyId : String?
    var surveyTitle : String?
    var surveyUrl : String?
    var surveyImage : String?
    
}
