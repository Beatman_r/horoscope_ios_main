//
//  TarotEnterController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit


class TarotEnterController: TarotBaseViewController {
    
    var cardFloatingTimer : Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        AnaliticsManager.sendEvent(AnaliticsManager.tarot_show)
        
        createTarotCardBack()
        
        cardFloatingTimer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(cardFloating), userInfo: nil, repeats: true)
        
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.cardFloatingTimer?.fire()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.loveImg.frame.origin.y == self.screenHeight{
            self.topicImgAnimate()
        }
    }
    
    let tarotCardBack = UIImageView()
    let setNameImg = UIImageView()
    let introLabel = UILabel()
    let cardBackLight = UIImageView()
    
    func createTarotCardBack() {
        let tap=UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(cardBackOnClick))
        self.tarotCardBack.addGestureRecognizer(tap)
        
        self.cardBackLight.image = UIImage(named: "tarot_card_triple_dark_")
        self.cardBackLight.isUserInteractionEnabled = true
        self.tarotCardBack.image = UIImage(named: "tarot_card_triple_light_")
        self.tarotCardBack.isUserInteractionEnabled=true
        
        self.introLabel.attributedText = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(NSLocalizedString("Relax,keep focus,eliminate all distractions to you...\nAnd now, tap on the deck to start!", comment: ""),space: 1.2)
        
        self.introLabel.textAlignment = NSTextAlignment.center
        self.introLabel.textColor = UIColor.init(red: 129/255, green: 67/255, blue: 107/255, alpha: 1)
        self.introLabel.numberOfLines = 0
        self.introLabel.font = HSFont.baseContentFont()
        self.introLabel.font = UIFont.systemFont(ofSize: 14)
        self.setNameImg.image = UIImage(named: "rider_waite_tarot")
        
        self.view.addSubview(self.cardBackLight)
        self.view.addSubview(self.tarotCardBack)
        self.view.addSubview(self.setNameImg)
        self.view.addSubview(self.introLabel)
        
        self.cardBackLight.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.centerY.equalTo(self.view.snp.centerY).offset((-self.screenHeight * 30/736))
            make.height.equalTo((UIScreen.main.bounds.height-64)*0.67)
            make.width.equalTo(UIScreen.main.bounds.width*0.87)
        }
        
        self.tarotCardBack.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view.snp.centerX)
            make.centerY.equalTo(self.view.snp.centerY).offset((-self.screenHeight * 30/736))
            make.height.equalTo((UIScreen.main.bounds.height-64)*0.67)
            make.width.equalTo(UIScreen.main.bounds.width*0.87)
        }
        
        self.introLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom).offset(-self.screenHeight * 30/736)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.equalTo(self.screenHeight * 50/self.screenHeight)
            make.width.equalTo(self.view.snp.width).offset(-40)
        }
        
        self.setNameImg.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.introLabel.snp.top).offset(-self.screenHeight * 25/736)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.equalTo(self.screenHeight * 50/self.screenHeight)
            make.width.equalTo(self.view.snp.width).offset(-40)
        }
    }
    
    @objc func cardFloating() {
        UIView.animate(withDuration: 1, delay: 0, options: .allowUserInteraction, animations: {
            self.cardBackLight.alpha = 0.3
            self.cardBackLight.snp.updateConstraints { (make) in
                make.centerX.equalTo(self.view.snp.centerX)
                make.centerY.equalTo(self.view.snp.centerY).offset((-self.screenHeight * 30/736))
                make.height.equalTo((UIScreen.main.bounds.height-64)*0.57)
                make.width.equalTo(UIScreen.main.bounds.width*0.77)
            }
            self.tarotCardBack.snp.updateConstraints { (make) in
                make.centerX.equalTo(self.view.snp.centerX)
                make.centerY.equalTo(self.view.snp.centerY).offset((-self.screenHeight * 30/736))
                make.height.equalTo((UIScreen.main.bounds.height-64)*0.57)
                make.width.equalTo(UIScreen.main.bounds.width*0.77)
            }
            self.view.layoutIfNeeded()
        }) { (true) in
            UIView.animate(withDuration: 1, delay: 0, options: .allowUserInteraction, animations: { 
                self.cardBackLight.alpha = 1
                self.cardBackLight.snp.updateConstraints { (make) in
                    make.centerX.equalTo(self.view.snp.centerX)
                    make.centerY.equalTo(self.view.snp.centerY).offset((-self.screenHeight * 30/736))
                    make.height.equalTo((UIScreen.main.bounds.height-64)*0.77)
                    make.width.equalTo(UIScreen.main.bounds.width*0.97)
                }
                self.tarotCardBack.snp.updateConstraints { (make) in
                    make.centerX.equalTo(self.view.snp.centerX)
                    make.centerY.equalTo(self.view.snp.centerY).offset((-self.screenHeight * 30/736))
                    make.height.equalTo((UIScreen.main.bounds.height-64)*0.77)
                    make.width.equalTo(UIScreen.main.bounds.width*0.97)
                }
                self.view.layoutIfNeeded()
                }, completion: { (true) in
                    
            })
        }
    }
    
    var loveImg = UIImageView()
    var careerImg = UIImageView()
    var healthImg = UIImageView()
    
    func createTopicUI() {
        
        loveImg = self.createTopBgImg("button_love_")
        careerImg = self.createTopBgImg("button_career_")
        healthImg = self.createTopBgImg("button_health_")
        
        loveImg.frame = CGRect(x: 0, y: screenHeight, width: screenWidth, height: self.screenHeight * 200/736)
        careerImg.frame = CGRect(x: 0, y: screenHeight, width: screenWidth, height: self.screenHeight * 200/736)
        healthImg.frame = CGRect(x: 0, y: screenHeight, width: screenWidth, height: self.screenHeight * 200/736)
        
        let loveLabel = self.createTopicLabel(NSLocalizedString("Love", comment: ""))
        let careerLabel = self.createTopicLabel(NSLocalizedString("Career", comment: ""))
        let healthLabel = self.createTopicLabel(NSLocalizedString("Health", comment: ""))
        
        loveLabel.frame = loveImg.frame
        careerLabel.frame = careerImg.frame
        healthLabel.frame = healthImg.frame
        
        loveImg.addSubview(loveLabel)
        careerImg.addSubview(careerLabel)
        healthImg.addSubview(healthLabel)
        
        self.view.addSubview(loveImg)
        self.view.addSubview(careerImg)
        self.view.addSubview(healthImg)
    }
    
    var loveButton : UIButton?
    var careerButton : UIButton?
    var healthButton : UIButton?
    
    func topicImgAnimate() {
        
        UIView.animate(withDuration: 0.3, animations: {
            let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                UIView.animate(withDuration: 0.3, animations: {
                    self.careerImg.frame = CGRect(x: 0, y: self.screenHeight*0.5-(self.screenHeight*130/736), width: self.screenWidth, height: self.screenHeight * 200/736)
                })
            })
            let healthDelayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: healthDelayTime, execute: {
                UIView.animate(withDuration: 0.3, animations: {
                    self.healthImg.frame = CGRect(x: 0, y: self.screenHeight*0.5+(self.screenHeight*2/736), width: self.screenWidth, height: self.screenHeight * 200/736)
                })
            })
            self.loveImg.frame = CGRect(x: 0, y: self.screenHeight*0.5-(self.screenHeight*262/736), width: self.screenWidth, height: self.screenHeight * 200/736)
            
        }, completion: { (true) in
            
            UIView.animate(withDuration: 0.3, animations: {
                let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.careerImg.frame = CGRect(x: 0, y: self.screenHeight*0.5-(self.screenHeight*100/736), width: self.screenWidth, height: self.screenHeight * 200/736)
                    })
                })
                let helathDelayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: helathDelayTime, execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.healthImg.frame = CGRect(x: 0, y: self.screenHeight*0.5+(self.screenHeight*32/736), width: self.screenWidth, height: self.screenHeight * 200/736)
                    })
                })
                self.loveImg.frame = CGRect(x: 0, y: self.screenHeight*0.5-(self.screenHeight*232/736), width: self.screenWidth, height: self.screenHeight * 200/736)
            })
            
            self.loveButton = UIButton()
            self.loveButton?.addTarget(self, action: #selector(self.loveButtonOnClick), for: .touchUpInside)
            self.loveImg.addSubview(self.loveButton!)
            self.loveButton?.snp.makeConstraints({ (make) in
                make.center.equalTo(self.loveImg.snp.center)
                make.width.equalTo(self.loveImg.snp.width).offset(-80)
                make.height.equalTo(self.loveImg.snp.height).offset(-80)
            })
            self.careerButton = UIButton()
            self.careerButton?.addTarget(self, action: #selector(self.careerButtonOnClick), for: .touchUpInside)
            self.careerImg.addSubview(self.careerButton!)
            self.careerButton?.snp.makeConstraints({ (make) in
                make.center.equalTo(self.careerImg.snp.center)
                make.width.equalTo(self.careerImg.snp.width).offset(-80)
                make.height.equalTo(self.careerImg.snp.height).offset(-80)
            })
            self.healthButton = UIButton()
            self.healthButton?.addTarget(self, action: #selector(self.healthButtonOnClick), for: .touchUpInside)
            self.healthImg.addSubview(self.healthButton!)
            self.healthButton?.snp.makeConstraints({ (make) in
                make.center.equalTo(self.healthImg.snp.center)
                make.width.equalTo(self.healthImg.snp.width).offset(-80)
                make.height.equalTo(self.healthImg.snp.height).offset(-80)
            })
            self.view.layoutIfNeeded()
        }) 
    }
    
    func sinkTopicButtons(_ topic:Int) {
        UIView.animate(withDuration: 0.3, animations: {
            
            let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                UIView.animate(withDuration: 0.3, animations: {
                    self.careerImg.frame = CGRect(x: 0, y: self.screenHeight*0.5-(self.screenHeight*130/736), width: self.screenWidth, height: self.screenHeight * 200/736)
                })
            })
            let healthDelayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: healthDelayTime, execute: {
                UIView.animate(withDuration: 0.3, animations: {
                    self.loveImg.frame = CGRect(x: 0, y: self.screenHeight*0.5-(self.screenHeight*262/736), width: self.screenWidth, height: self.screenHeight * 200/736)
                })
            })
            self.healthImg.frame = CGRect(x: 0, y: self.screenHeight*0.5+(self.screenHeight*2/736), width: self.screenWidth, height: self.screenHeight * 200/736)
            
            
        }, completion: { (true) in
            
            UIView.animate(withDuration: 0.3, animations: { 
                let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.careerImg.frame = CGRect(x: 0, y: self.screenHeight, width: self.screenWidth, height: self.screenHeight * 200/736)
                    })
                })
                let helathDelayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: helathDelayTime, execute: {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.loveImg.frame = CGRect(x: 0, y: self.screenHeight, width: self.screenWidth, height: self.screenHeight * 200/736)
                    })
                })
                self.healthImg.frame = CGRect(x: 0, y: self.screenHeight, width: self.screenWidth, height: self.screenHeight * 200/736)
                }, completion: { (true) in
                    self.loveButton!.removeFromSuperview()
                    self.careerButton!.removeFromSuperview()
                    self.healthButton!.removeFromSuperview()
                    self.view.layoutIfNeeded()
                    let vc = TarotIntroController()
                    vc.topic = topic
                    self.navigationController?.pushViewController(vc, animated: true)
            })
        }) 
    }
    
    @objc func loveButtonOnClick() {
        
        AnaliticsManager.sendEvent(AnaliticsManager.tarot_select_category, data: ["name":"love"])
        self.sinkTopicButtons(1)
    }
    
    @objc func careerButtonOnClick() {
        
        AnaliticsManager.sendEvent(AnaliticsManager.tarot_select_category, data: ["name":"career"])
        self.sinkTopicButtons(0)
    }
    
    @objc func healthButtonOnClick() {
        
        AnaliticsManager.sendEvent(AnaliticsManager.tarot_select_category, data: ["name":"health"])
        self.sinkTopicButtons(2)
    }
    
    func createTopicLabel(_ title:String) -> UILabel {
        let lab = UILabel()
        lab.text = title
        lab.textAlignment = NSTextAlignment.center
        lab.font = HSFont.baseMediumFont(24)
        lab.textColor = UIColor.white
        return lab
    }
    
    func createTopBgImg(_ imgName:String) -> UIImageView {
        let imgView = UIImageView()
        imgView.image = UIImage(named: imgName)
        imgView.isUserInteractionEnabled = true
        return imgView
    }
    
    @objc func cardBackOnClick() {
        self.cardFloatingTimer?.invalidate()
        self.cardBackLight.layer.removeAllAnimations()
        self.tarotCardBack.layer.removeAllAnimations()
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.cardBackLight.snp.updateConstraints { (make) in
                make.height.equalTo((UIScreen.main.bounds.height-64)*0.67)
                make.width.equalTo(UIScreen.main.bounds.width*0.87)
            }
            self.tarotCardBack.snp.updateConstraints { (make) in
                make.height.equalTo((UIScreen.main.bounds.height-64)*0.67)
                make.width.equalTo(UIScreen.main.bounds.width*0.87)
            }
            self.view.layoutIfNeeded()
        }, completion: { (true) in
            UIView.animate(withDuration: 1, animations: {
                self.cardBackLight.snp.updateConstraints { (make) in
                    make.height.equalTo((UIScreen.main.bounds.height-64)*1.07)
                    make.width.equalTo(UIScreen.main.bounds.width*1.22)
                }
                self.tarotCardBack.snp.updateConstraints { (make) in
                    make.height.equalTo((UIScreen.main.bounds.height-64)*1.07)
                    make.width.equalTo(UIScreen.main.bounds.width*1.22)
                }
                self.introLabel.snp.remakeConstraints({ (make) in
                    make.bottom.equalTo(self.view.snp.bottom).offset(self.screenWidth * 200/414)
                    make.left.equalTo(self.view.snp.left).offset(20)
                    make.right.equalTo(self.view.snp.right).offset(-20)
                    make.top.equalTo(self.introLabel.snp.bottom).offset(-50)
                })
                self.cardBackLight.alpha = 0
                self.tarotCardBack.alpha = 0
                self.introLabel.alpha = 0
                self.setNameImg.alpha = 0
                self.view.layoutIfNeeded()
            }, completion: { (true) in
                self.cardBackLight.removeFromSuperview()
                self.tarotCardBack.removeFromSuperview()
                self.introLabel.removeFromSuperview()
                self.setNameImg.removeFromSuperview()
                self.createTopicUI()
                self.topicImgAnimate()
            }) 
        }) 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
