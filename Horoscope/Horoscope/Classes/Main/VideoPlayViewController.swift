//
//  VideoPlayViewController.swift
//  Horoscope
//
//  Created by Wang on 16/12/9.
//  Copyright © 2016年 meevii. All rights reserved.
//


import UIKit
import youtube_ios_player_helper
import PKHUD
import MJRefresh
import SwiftyJSON

class VideoPlayViewController: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource,
    YTPlayerViewDelegate,
VideoLoadingViewDelegate,PostViewControllerDelegate,CommentFootBtnDelegate{
    var playView:YTPlayerView?
    var id:String?
    var viewModel:videoPlayViewModel?
    //    var recommandViewModel:RecommandBread?
    @objc var postId:String? = ""
    var commentList = [MainComment]()
    var hotComentList = [MainComment]()
    var postModel:ReadPostModel?
    let commentFootBtn = CommentFootBtn.init(frame: CGRect.zero)
    var nameButton:UIButton?
    var timeLabel:UILabel?
    var headImageView:UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        AnaliticsManager.sendEvent(AnaliticsManager.video_show)
        //     self.navigationItem.title = self.model?.title
        self.addBackBtn()
        editBasicUI()
        createPlayView()
        createTableView()
        initViewModel()
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeRotate(_:)), name: NSNotification.Name.UIApplicationDidChangeStatusBarFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFoldComents(_:)), name: NSNotification.Name(rawValue: MoreCommentBtnClick), object: nil)
        NotificationCenter.default.addObserver(self
            , selector: #selector(self.CommentViewClickToShowCommentReplyView(_:)), name: NSNotification.Name(rawValue: CommentViewClickToShowReplyView), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.replyBtnClick(_:)), name: NSNotification.Name(rawValue: ReplyBtnClickToShowReplyController), object: nil)
        NotificationCenter.default.addObserver(self
            , selector: #selector(self.reportBtnClickToShowReportController), name: NSNotification.Name(rawValue: ReportBtnClickToShowReportController), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.commentsMoreThanSeventyFloor), name: NSNotification.Name(rawValue: CommentsMoreThanSeventyFloor), object: nil)
        
        self.createCommentView()
        self.setupRefreshFooterView()
        self.getComments()
        self.addRightNavReportBtn()
        NotificationCenter.default.addObserver(self, selector: #selector(self.likeBtnClick(_:)), name: NSNotification.Name(rawValue: LikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.disLikeBtnClick(_:)), name: NSNotification.Name(rawValue: DisLikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userClickToUserTopic(_:)), name: NSNotification.Name(rawValue: UserClickToUserTopic), object: nil)
    }
    
    @objc func userClickToUserTopic(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! BaseComment)
        let userVC  = ReadOtherUserPostViewController()
        userVC.userId = mod.userInfo?.userId ?? ""
        userVC.userName = mod.userInfo?.name ?? ""
        self.navigationController?.pushViewController(userVC, animated: true)
    }
    
    @objc func likeBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! ParentComment)
        for main in self.commentList {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
        
        for main in self.hotComentList {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
    }
    }
    
    @objc func disLikeBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! ParentComment)
        for main in self.commentList {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
        
        for main in self.hotComentList {
            if main.commentId == mod.commentId {
                changeModelLikeOrDisLikeState(main, mod: mod)
            }
        }
    }
  }
    
    func changeModelLikeOrDisLikeState(_ model:BaseComment,mod:ParentComment){
        model.likeCount = mod.likeCount
        model.dislikeCount = mod.dislikeCount
        model.isDisliked = mod.isDisliked
        model.isLiked = mod.isLiked
        
    }
        
    func addRightNavReportBtn() {
        let reportBtn = UIButton(type:.custom)
        reportBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        reportBtn.setImage(UIImage(named: "report_"), for: UIControlState())
        reportBtn.isUserInteractionEnabled = true
        reportBtn.addTarget(self, action: #selector(self.rightNavReportBtnClick), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: reportBtn)
    }
    
    @objc func rightNavReportBtnClick() {
        ReportViewController.addacrion(BaseComment(),postId: self.postId ?? "", isPostReport: true,vc:self)
    }
    
    @objc func commentsMoreThanSeventyFloor() {
        HUD.flash(.label("Too more comments"),delay: 1)
    }
    func createCommentView() {
        let backview = UIView()
        self.view.addSubview(backview)
        backview.backgroundColor = UIColor.commentFootBtnBGColor()
        backview.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.height.equalTo(44)
        }
        backview.addSubview(commentFootBtn)
        commentFootBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(backview)
            make.left.equalTo(backview).offset(20)
            make.right.equalTo(backview).offset(-20)
            make.top.equalTo(backview)
        }
        commentFootBtn.delegate = self
        commentFootBtn.isHidden = false
    }
    func getComments() {
        HoroscopeDAO.sharedInstance.getTopic(self.postId ?? "" , success: { (post) in
            self.postModel = post
            self.commentList = post.commentList ?? [MainComment]()
            self.hotComentList = post.hotCommentList ?? [MainComment]()
            self.tableView?.reloadData()
        }) { (failure) in
            
        }
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    
    @objc func popView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func initViewModel() {
        self.playYoutubeVideo()
        self.tableView?.reloadData()
    }
    
    func playYoutubeVideo(){
        let dic = ["playsinline":1]
        self.playView?.load(withVideoId: self.richMedia ?? "", playerVars: dic)
    }
    
    func editBasicUI() {
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
    }
    
    var tableView:UITableView?
    func createTableView() {
        //        CGRectMake(0, UIScreen.mainScreen().bounds.size.width*9/16+64,UIScreen.mainScreen().bounds.size.width ,UIScreen.mainScreen().bounds.size.height-UIScreen.mainScreen().bounds.size.width*9/16)
        tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        tableView?.delegate=self
        tableView?.dataSource=self
        tableView?.separatorStyle = .none
        tableView?.backgroundColor=UIColor.clear
        self.view.addSubview(tableView!)
        self.tableView!.snp.makeConstraints{ (make) in
            make.top.equalTo((self.playView!.snp.bottom))
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view.snp.bottom)
        }
        
        let footView = UIView()
        footView.frame = CGRect(x: 0, y: 0, width: 0, height: 8)
        footView.backgroundColor=UIColor.clear
        self.tableView?.tableFooterView = footView
        registerCell()
    }
    
    func registerCell() {
        self.tableView?.register(VideoToolCell.self, forCellReuseIdentifier: "VideoToolCell")
        self.tableView?.register(VideoCell.self, forCellReuseIdentifier: "videoListCell")
        self.tableView?.register(RecommandTitleCell.self, forCellReuseIdentifier: "RecommandTitleCell")
        self.tableView?.register(CommentCellTableViewCell.self, forCellReuseIdentifier: "cell")

    }
    
    var loadsView:VideoLoadingView?
    func createPlayView() {
        
        self.playView=YTPlayerView.init()
        self.playView?.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width*9/16+64)
        self.playView?.backgroundColor=UIColor.black
        loadsView = VideoLoadingView.create()
        loadsView?.delegate=self
        loadsView?.loading()
        self.loadsView?.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width*9/16+64)
        self.view.addSubview(playView!)
        playView?.delegate=self
        self.view.addSubview(loadsView!)
        

        
    }
    
    @objc func changeRotate(_ noti:Notification) {
        let interface = UIApplication.shared.statusBarOrientation
        if interface == UIInterfaceOrientation.portrait{
            self.navigationController?.navigationBar.isHidden = false
            self.playView?.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width*9/16+64)
        }else{
            self.navigationController?.navigationBar.isHidden = true
            self.playView?.frame = self.view.frame
        }
    }
    
    //MARK:loadingViewDelegate
    func retry() {
        self.playYoutubeVideo()
    }
    
    //MARK:playViewDelegate
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        self.loadsView?.endLoading()
        self.playView?.playVideo()
        self.showPageTime = Date().timeIntervalSince1970
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        if state == .ended{
            portraitScreen()
        }
    }
    
    func playerView(_ playerView: YTPlayerView, didChangeTo quality:  YTPlaybackQuality) {
        
    }
    
    func portraitScreen() {
        if isFullScreen{
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            UIApplication.shared.setStatusBarHidden(false, with: UIStatusBarAnimation.fade)
            UIApplication.shared.setStatusBarOrientation(UIInterfaceOrientation.portrait, animated: false)
        }
        else{
            //  self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    var isFullScreen:Bool {
        get {
            return UIApplication.shared.statusBarOrientation.isLandscape
        }
    }
    
    
    func playerView(_ playerView: YTPlayerView, receivedError error: YTPlayerError) {
        self.loadsView?.loadError()
    }
    
    var showPageTime:TimeInterval = 0
    fileprivate func getScreenStayTime() -> String {
        let nowTime:TimeInterval = Date().timeIntervalSince1970
        let stayTime:TimeInterval = nowTime - self.showPageTime
        var stayString:String = "15s"
        if stayTime >= 15 && stayTime < 20 {
            stayString = "15-20s"
        } else if stayTime >= 20 && stayTime < 30 {
            stayString = "20-30s"
        } else if stayTime >= 30 && stayTime < 60 {
            stayString = "30s-1min"
        } else if stayTime >= 60 && stayTime < 120 {
            stayString = "1-2min"
        }
        else if stayTime >= 120 && stayTime < 300 {
            stayString = "2-5min"
        }else {
            stayString = ">5min"
        }
        return stayString
    }
    
    //MARK:ViewAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
         self.navigationController?.navigationBar.shadowImage = nil
        let deletgate = UIApplication.shared.delegate as! AppDelegate
        deletgate.allowRotate = 1
        
        //
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let deletgate = UIApplication.shared.delegate as! AppDelegate
        deletgate.allowRotate = 0
        AnaliticsManager.sendEvent(AnaliticsManager.video_screen_stay_time, data: ["video":getScreenStayTime()])
    }
    
    //MARK:tableviewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            return 3
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }else if section == 1{
                return 30
            }
            else if section == 2{
                return 30
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }else if section == 1{
                return 30
            }
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                let title = self.postModel?.title ?? ""
                    return VideoToolCell.calculateHeight(title)
            }
            else if indexPath.section == 1{
                let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row],isMyNotification:false)
                return  container.frame.size.height
            }
            else if indexPath.section == 2{
                let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                return  container.frame.size.height
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                let title = self.postModel?.title ?? ""
                return VideoToolCell.calculateHeight(title)
            }
            else if indexPath.section == 1{
                if self.hotComentList.count > 0 {
                    let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                    return  container.frame.size.height
                }else if self.commentList.count > 0 {
                    let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                    return  container.frame.size.height
                }
            }
        }
        let title = self.viewModel?.detail?.title
        return VideoToolCell.calculateHeight(title ?? "")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if section == 0 {
                return 1
            }
            else if section == 1{
                return self.hotComentList.count
            }else if section == 2{
                return self.commentList.count
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 0 {
                return 1
            }
            if section == 1{
                if self.commentList.count > 0 {
                    return self.commentList.count
                }else if self.hotComentList.count > 0 {
                    return self.hotComentList.count
                }
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            if section == 1 {
                return createHeaderView("   Top Comments")
            }else if section == 2{
                return createHeaderView("   New Comments")
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 1 && self.commentList.count > 0{
                return createHeaderView("   New Comments")
            }else if section == 1 && self.hotComentList.count > 0{
                return createHeaderView("   Top Comments")
            }
        }
        return nil
    }
    
    func createHeaderView(_ str:String)-> UIView {
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 32)
        label.textColor = UIColor.purpleContentColor()
        label.font = UIFont.systemFont(ofSize: 21)
        label.text = str
        label.backgroundColor = UIColor.basePurpleBackgroundColor()
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        headerView.addSubview(label)
        headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 32)
        
        let divideLine = UIView()
        divideLine.backgroundColor = UIColor.divideColor()
        divideLine.frame = CGRect(x: 0, y: headerView.frame.maxY, width: SCREEN_WIDTH, height: 1)
        headerView.addSubview(divideLine)
        return headerView
    }
    
    func createHeaderLable(_ str:String) -> UILabel {
        let label = UILabel()
        label.frame = CGRect(x: 12, y: 0, width: AdaptiveUtils.screenWidth - 24, height: 30)
        label.backgroundColor = UIColor.basePurpleBackgroundColor()
        label.textColor = UIColor.purpleContentColor()
        label.font = UIFont.systemFont(ofSize: 21)
        label.text = str
        return label
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                let cell = VideoToolCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath,model:self.postModel)
                cell.rootVc = self
                return cell
            }else if indexPath.section == 1{
                let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                cell.contentView.addSubview(container)
                return cell
            }else if indexPath.section == 2{
                let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                cell.contentView.addSubview(container)
                return cell
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                let cell = VideoToolCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath,model:self.postModel)
                cell.rootVc = self
                return cell
            }else if indexPath.section == 1{
                if self.commentList.count > 0 {
                    let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                    let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                    cell.contentView.addSubview(container)
                    return cell
                }else if self.hotComentList.count > 0 {
                    let cell = CommentCellTableViewCell(style: .default, reuseIdentifier: "cell")
                    let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                    cell.contentView.addSubview(container)
                    return cell
                }
            }
        }
        let cell = VideoToolCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath,model:self.postModel)
        cell.rootVc = self
        return cell
    }
    
     func setupRefreshFooterView() {
        let footer = MJRefreshBackNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreList))
        footer?.setTitle(NSLocalizedString("Pull up to load", comment: ""), for: .idle)
        footer?.setTitle(NSLocalizedString("Release to refresh", comment: ""), for: .pulling)
        footer?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        self.tableView?.mj_footer = footer
    }
    
    
     @objc func loadMoreList() {
        let offset  = String(self.commentList.count)
        HoroscopeDAO.sharedInstance.getMoreComment(self.postId ?? "", size: "20", offset: offset, sort: "new", success: { (commentList) in
            if commentList.count == 0 {
                HUD.flash(.label("No More Comments"),delay: 1)
                self.tableView?.mj_footer.endRefreshing()
            }else{
                self.commentList  = self.commentList + commentList
                self.tableView?.reloadData()
                self.tableView?.mj_footer.endRefreshing()
            }
        }) { (failure) in
            self.tableView?.mj_footer.endRefreshing()
        }
    }
    
    @objc func getFoldComents(_ noti:Notification) {
        
        guard let mainComment = noti.userInfo?["mainCommet"] else {
            return
        }
        guard let foldIndex = (mainComment as! MainComment ).parentCommentWarp?["index"] else {
            return
        }
        
        guard let negative_index = (mainComment as! MainComment ).parentCommentWarp?["negative_index"] else {
            return
        }
        
        guard let commentId = (mainComment as! MainComment ).commentId else {
            return
        }
        
        HoroscopeDAO.sharedInstance.getFoldCommentList(commentId, index: (foldIndex as! Int), negative_index: (negative_index as! Int), success: { (foldCommentList) in
            
            var needInsertCmt : MainComment?
            //            var indexpath : NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            for  index  in 0..<self.hotComentList.count {
                let mainCmt = self.hotComentList[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
                    
                    //                    indexpath = NSIndexPath(forRow: index, inSection: 1)
                }
            }
            
            for  index  in 0..<self.commentList.count {
                let mainCmt = self.commentList[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
                    //needfix
                    //                    indexpath = NSIndexPath(forRow: index, inSection: 1)
                }
            }
            
            needInsertCmt?.parentCommentWarp = nil
            
            guard let  needInsertCmtSubCmtCount = needInsertCmt?.parentCommentList.count else {
                return
            }
            for index in 0..<needInsertCmtSubCmtCount {
                if index == (foldIndex as! Int ) {
                    for index in 0..<foldCommentList.count {
                        needInsertCmt?.parentCommentList.insert(foldCommentList[index], at: index+2)
                    }
                }
            }
            self.tableView?.reloadData()
        }) { (failure) in
            LXSWLog(failure)
        }
    }
    
    // MARK: CommentFootBtn delegate
    func commentFootBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        let replyVC = PostViewController()
        replyVC.isFirstClassReply = true
        let replyModel = MyPostModel.init(jsonData: JSON.null, num: 0)
        replyVC.replyModel = replyModel
        replyModel.postId = self.postId
        replyVC.delegate = self
        self.navigationController?.pushViewController(replyVC, animated: true)
    }
    }
    //MARK:- CommentViewCellDelegate
    @objc func CommentViewClickToShowCommentReplyView(_ noti: Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let replyView = ReplyCommentView(frame: self.view.bounds, model: model as! BaseComment)
        self.view.addSubview(replyView)
    }
    
    
    @objc func replyBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let replyVC = PostViewController()
        replyVC.isReply = true
        replyVC.model = model as? BaseComment
        let replyModel = MyPostModel.init(jsonData: JSON.null, num: 0)
        replyVC.replyModel = replyModel
        replyModel.postId = (model as AnyObject).postId
        replyVC.delegate = self
        self.navigationController?.pushViewController(replyVC, animated: true)
    }
    }
    
    @objc func reportBtnClickToShowReportController(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod = model as! BaseComment
        ReportViewController.addacrion(mod, postId: mod.postId ?? "", isPostReport: false,vc:self)
    }
    }
    
    //MARK:- PostViewControllerDelegate
    func postCommentFailure(_ error: NSError) {
         HUD.flash(.label("delivery failure"),delay: 1)
    }
    
    func postCommentSuccess(_ comment: MainComment) {
        self.commentList.insert(comment, at: 0)
        self.tableView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:create
    var richMedia:String?
    class func create(_ media:String?,postId:String) ->VideoPlayViewController{
        let vc = VideoPlayViewController()
        vc.postId=postId
        vc.richMedia = media
        return vc
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
