//
//  MyCommentsViewController.swift
//  Horoscope
//
//  Created by Wang on 17/2/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}




class MyCommentsViewController: BaseViewController {
    var dataArr:[Bool] = []
    var dataList:[MyCommentModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        self.loadData()
        self.title=NSLocalizedString("My Comments", comment: "")
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        // Do any additional setup after loading the view.
    }
    
    override func loadData() {
        HoroscopeDAO().getUserCommentList(0, success: { (modelList, hasImaList) in
            self.dataList = modelList
            if self.dataList.count == 0 {
                let blankLabel = UILabel()
                blankLabel.text = "No comment yet! Look around and make some comments!"
                blankLabel.textColor = UIColor.luckyPurpleTitleColor()
                blankLabel.font = HSFont.baseRegularFont(20)
                blankLabel.textAlignment = .center
                blankLabel.numberOfLines = 0
                self.view.addSubview(blankLabel)
                self.view.bringSubview(toFront: blankLabel)
                blankLabel.snp.makeConstraints({ (make) in
                    make.center.equalTo(self.view)
                    make.width.equalTo(self.view).offset(-24)
                })
            }
            self.failedRetryView?.loadSuccess()
            for sub in 0..<self.dataList.count {
                if self.dataList[sub].imageList?.count > 0 {
                    self.dataArr.append(true)
                }else {
                    self.dataArr.append(false)
                }
            }
            self.baseTableView?.reloadData()
            }) { (error) in
                self.failedRetryView?.loadFailed()
                print(error)
        }
    }
    
    override func retryToLoadData() {
        self.loadData()
    }
    
    override func registerCell() {
         self.baseTableView?.register(BaseCommentCell.self, forCellReuseIdentifier: "BaseCommentCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:tableviewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BaseCommentCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
//        let bo = dataArr[indexPath.row]
        let model=self.dataList[indexPath.row]
        cell.rootVc = self
        cell.model = model
        return cell
    }
    let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    func addActivity() {
        activity.frame = CGRect(x: SCREEN_WIDTH/2-100, y: SCREEN_HEIGHT/2-100, width: 200, height: 200)
        activity.color = UIColor.white
        self.baseTableView?.addSubview(activity)
        self.baseTableView?.bringSubview(toFront: activity)
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.activity.startAnimating()
        let model=self.dataList[indexPath.row]
        
        let vc = ReadPostViewController()
        let postID = model.postId ?? ""
        HoroscopeDAO.sharedInstance.getTopic(postID, success: { (post) in
            vc.postModel = MyPostModel.init(model: post)
            self.activity.stopAnimating()
            self.navigationController?.pushViewController(vc, animated: true)
            }) { (failure) in
         }
     }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let bo = dataList[indexPath.row]
        return  BaseCommentCell.calculateHeight(bo)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
