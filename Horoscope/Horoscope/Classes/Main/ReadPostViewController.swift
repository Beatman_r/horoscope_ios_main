//
//  ReadPostViewController.swift
//  Horoscope
//
//  Created by Beatman on 17/2/17.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD
import MJRefresh
import IDMPhotoBrowser
import SwiftyJSON

class ReadPostViewController: BaseViewController,CommentFootBtnDelegate,PostViewControllerDelegate,NormalPostCellDelegate{
    var postModel : MyPostModel?
    var readPost:ReadPostModel?
    var commentList = [MainComment]()
    var hotComentList = [MainComment]()
    let commentFootBtn = CommentFootBtn.init(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseTableView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT-36)
        AnaliticsManager.sendEvent(AnaliticsManager.thread_view)
        self.loadCommentData()
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        self.createCommentView()
        self.setupRefreshFooterView()
        self.addNoti()
        self.addRightNavReportBtn()
        NotificationCenter.default.addObserver(self, selector: #selector(self.userClickToUserTopic(_:)), name: NSNotification.Name(rawValue: UserClickToUserTopic), object: nil)
    }
    
    @objc func userClickToUserTopic(_ noti:Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let mod =  (model as! BaseComment)
        let userVC  = ReadOtherUserPostViewController()
        userVC.userId = mod.userInfo?.userId ?? ""
        userVC.userName = mod.userInfo?.name ?? ""
        self.navigationController?.pushViewController(userVC, animated: true)
    }
    
    func addRightNavReportBtn() {
        let reportBtn = UIButton(type:.custom)
        reportBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        reportBtn.setImage(UIImage(named: "report_"), for: UIControlState())
        reportBtn.isUserInteractionEnabled = true
        reportBtn.addTarget(self, action: #selector(self.rightNavReportBtnClick), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: reportBtn)
    }
    
    @objc func rightNavReportBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            ReportViewController.addacrion(BaseComment(),postId: self.postModel?.postId ?? "", isPostReport: true,vc:self)
        }
    }
    
    func addNoti() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFoldComents(_:)), name: NSNotification.Name(rawValue: MoreCommentBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.CommentViewClickToShowCommentReplyView(_:)), name: NSNotification.Name(rawValue: CommentViewClickToShowReplyView), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.replyBtnClick(_:)), name: NSNotification.Name(rawValue: ReplyBtnClickToShowReplyController), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reportBtnClickToShowReportController(_:)), name: NSNotification.Name(rawValue: ReportBtnClickToShowReportController), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.commentsMoreThanSeventyFloor), name: NSNotification.Name(rawValue: CommentsMoreThanSeventyFloor), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.likeBtnClick(_:)), name: NSNotification.Name(rawValue: LikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.disLikeBtnClick(_:)), name: NSNotification.Name(rawValue: DisLikeBtnClick), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(imageClick(_:)), name: NSNotification.Name(rawValue: "deviationClickBigImage"), object: nil)
    }
    
    @objc func imageClick(_ noti:Notification){
        let imaName = noti.userInfo?["url"] ?? ""
        let imgUrl = URL(string: (imaName) as! String)
        let urlArr : [AnyObject] = [imgUrl as AnyObject? ?? "" as AnyObject]
        let brower = IDMPhotoBrowser.init(photoURLs: urlArr)
        brower?.forceHideStatusBar = true
        brower?.usePopAnimation = true
        self.present(brower!, animated: true, completion: nil)
    }
    
    @objc func likeBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            guard let model = noti.userInfo?["model"] else {
                return
            }
            let mod =  (model as! ParentComment)
            for main in self.commentList {
                if main.commentId == mod.commentId {
                    changeModelLikeOrDisLikeState(main, mod: mod)
                }
            }
            
            for main in self.hotComentList {
                if main.commentId == mod.commentId {
                    changeModelLikeOrDisLikeState(main, mod: mod)
                }
            }
        }
    }
    @objc func disLikeBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            guard let model = noti.userInfo?["model"] else {
                return
            }
            let mod =  (model as! ParentComment)
            for main in self.commentList {
                if main.commentId == mod.commentId {
                    changeModelLikeOrDisLikeState(main, mod: mod)
                }
            }
            
            for main in self.hotComentList {
                if main.commentId == mod.commentId {
                    changeModelLikeOrDisLikeState(main, mod: mod)
                }
            }
        }
    }
    
    func changeModelLikeOrDisLikeState(_ model:BaseComment,mod:ParentComment){
        model.likeCount = mod.likeCount
        model.dislikeCount = mod.dislikeCount
        model.isDisliked = mod.isDisliked
        model.isLiked = mod.isLiked
    }
    
    
    override func setupRefreshFooterView() {
        let footer = MJRefreshBackNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreList))
        footer?.setTitle(NSLocalizedString("Pull up to load", comment: ""), for: .idle)
        footer?.setTitle(NSLocalizedString("Release to refresh", comment: ""), for: .pulling)
        footer?.setTitle(NSLocalizedString("Loading...", comment: ""), for: .refreshing)
        self.baseTableView?.mj_footer = footer
    }
    
    
    override func loadMoreList() {
        let offset  = String(self.commentList.count)
        dao.getMoreComment(self.postModel?.postId ?? "", size: "20", offset: offset, sort: "new", success: { (commentList) in
            if commentList.count == 0 {
                HUD.flash(.label("No More Comments"),delay: 1)
                self.baseTableView?.mj_footer.endRefreshing()
            }else{
                self.commentList  = self.commentList + commentList
                self.baseTableView?.reloadData()
                self.baseTableView?.mj_footer.endRefreshing()
            }
        }) { (failure) in
            self.baseTableView?.mj_footer.endRefreshing()
        }
    }
    
    func loadCommentData() {
        let postId = self.postModel?.postId ?? ""
        dao.getTopic(postId, success: { (post) in
            self.readPost = post
            self.commentList = post.commentList ?? [MainComment]()
            self.hotComentList = post.hotCommentList ?? [MainComment]()
            self.baseTableView?.reloadData()
        }) { (failure) in
            print("fail")
        }
    }
    func createCommentView() {
        let backview = UIView()
        self.view.addSubview(backview)
        backview.backgroundColor = UIColor.commentFootBtnBGColor()
        backview.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.height.equalTo(44)
        }
        backview.addSubview(commentFootBtn)
        commentFootBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(backview)
            make.left.equalTo(backview).offset(20)
            make.right.equalTo(backview).offset(-20)
            make.top.equalTo(backview)
        }
        commentFootBtn.delegate = self
        commentFootBtn.isHidden = false
    }
    
    
    override func registerCell() {
        self.baseTableView?.register(BaseTopicCell.self, forCellReuseIdentifier: "BaseTopicCell")
        self.baseTableView?.register(ReadPostCommentCell.self, forCellReuseIdentifier: "cell")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            return 3
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            return 2
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if section == 0 {
                return 1
            }else if section == 1{
                return self.hotComentList.count
            }else if section == 2{
                return self.commentList.count
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 0 {
                return 1
            }else if section == 1{
                if self.commentList.count > 0 {
                    return self.commentList.count
                }else if self.hotComentList.count > 0 {
                    return self.hotComentList.count
                }
            }
        }else{
            return 1
        }
        return 0
    }
    
    //MARK: TableViewDelegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                let cell = BaseTopicCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                cell.delegate = self
                cell.model = postModel
                cell.rootVc = self
                
                return cell
            }else if indexPath.section == 1{
                let cell = ReadPostCommentCell(style: .default, reuseIdentifier: "cell")
                let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                cell.contentView.addSubview(container)
                return cell
                
            }else if indexPath.section == 2{
                let cell = ReadPostCommentCell(style: .default, reuseIdentifier: "cell")
                let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                cell.contentView.addSubview(container)
                return cell
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                let cell = BaseTopicCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
                cell.model = postModel
                cell.delegate = self
                cell.rootVc = self
                return cell
            }else if indexPath.section == 1{
                if self.commentList.count > 0 {
                    let cell = ReadPostCommentCell(style: .default, reuseIdentifier: "cell")
                    let container = CommentLayoutContainer(model: self.commentList[indexPath.row],isMyNotification:false)
                    cell.contentView.addSubview(container)
                    return cell
                }else if self.hotComentList.count > 0 {
                    let cell = ReadPostCommentCell(style: .default, reuseIdentifier: "cell")
                    let container = CommentLayoutContainer(model: self.hotComentList[indexPath.row],isMyNotification:false)
                    cell.contentView.addSubview(container)
                    return cell
                }
            }
        }else{
            let cell = BaseTopicCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
            cell.model = postModel
            cell.delegate = self
            cell.rootVc = self
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.commentList.count > 0 && self.hotComentList.count > 0{
            if indexPath.section == 0 {
                return BaseTopicCell.calculateHeight(postModel)
            }else if indexPath.section == 1{
                let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                return container.frame.size.height
                
            }else if indexPath.section == 2{
                let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                return  container.frame.size.height
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if indexPath.section == 0 {
                return BaseTopicCell.calculateHeight(postModel)
            }else if indexPath.section == 1{
                if self.hotComentList.count > 0 {
                    let container = CommentLayoutContainer.init(model: self.hotComentList[indexPath.row ],isMyNotification:false)
                    return  container.frame.size.height
                }else if self.commentList.count > 0 {
                    let container = CommentLayoutContainer.init(model: self.commentList[indexPath.row ],isMyNotification:false)
                    return  container.frame.size.height
                }
            }
        }else{
            return BaseTopicCell.calculateHeight(postModel)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            if section == 1 {
                return createHeaderView("   Top Comments")
            }else if section == 2{
                return createHeaderView("   New Comments")
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 1 && self.commentList.count > 0{
                return createHeaderView("   New Comments")
            }else if section == 1 && self.hotComentList.count > 0{
                return createHeaderView("   Top Comments")
            }
        }
        return nil
    }
    
    func createHeaderView(_ str:String)-> UIView {
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 32)
        label.textColor = UIColor.purpleContentColor()
        label.font = UIFont.systemFont(ofSize: 18)
        label.text = str
        label.backgroundColor = UIColor.basePurpleBackgroundColor()
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        headerView.addSubview(label)
        headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 32)
        
        let divideLine = UIView()
        divideLine.backgroundColor = UIColor.divideColor()
        divideLine.frame = CGRect(x: 0, y: headerView.frame.maxY, width: SCREEN_WIDTH, height: 1)
        headerView.addSubview(divideLine)
        return headerView
    }
    
    func createHeaderLable(_ str:String) -> UILabel {
        let label = UILabel()
        label.frame = CGRect(x: 20, y: 0, width: SCREEN_WIDTH - 130, height: 32)
        label.textColor = UIColor.purpleContentColor()
        label.font = UIFont.systemFont(ofSize: 21)
        label.text = str
        label.backgroundColor = UIColor.basePurpleBackgroundColor()
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.commentList.count > 0 && self.hotComentList.count > 0 {
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }else if section == 1{
                return 30
            }
            else if section == 2{
                return 30
            }
        }else if self.commentList.count > 0 || self.hotComentList.count > 0{
            if section == 0 {
                return CGFloat.leastNormalMagnitude
            }else if section == 1{
                return 30
            }
        }
        return CGFloat.leastNormalMagnitude
    }
    
    //MARK- Comments
    @objc func getFoldComents(_ noti:Notification) {
        
        guard let mainComment = noti.userInfo?["mainCommet"] else {
            return
        }
        guard let foldIndex = (mainComment as! MainComment ).parentCommentWarp?["index"] else {
            return
        }
        
        guard let negative_index = (mainComment as! MainComment ).parentCommentWarp?["negative_index"] else {
            return
        }
        
        guard let commentId = (mainComment as! MainComment ).commentId else {
            return
        }
        
        dao.getFoldCommentList(commentId, index: (foldIndex as! Int), negative_index: (negative_index as! Int), success: { (foldCommentList) in
            
            var needInsertCmt : MainComment?
            //            var indexpath : NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
            for  index  in 0..<self.hotComentList.count {
                let mainCmt = self.hotComentList[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
                    
                    //                    indexpath = NSIndexPath(forRow: index, inSection: 1)
                }
            }
            
            for  index  in 0..<self.commentList.count {
                let mainCmt = self.commentList[index]
                let foldMainCmtId = (mainComment as! MainComment).commentId
                if mainCmt.commentId == foldMainCmtId  {
                    needInsertCmt = mainCmt
                    //needfix
                    //                    indexpath = NSIndexPath(forRow: index, inSection: 1)
                }
            }
            
            needInsertCmt?.parentCommentWarp = nil
            
            guard let  needInsertCmtSubCmtCount = needInsertCmt?.parentCommentList.count else {
                return
            }
            for index in 0..<needInsertCmtSubCmtCount {
                if index == (foldIndex as! Int ) {
                    for index in 0..<foldCommentList.count {
                        needInsertCmt?.parentCommentList.insert(foldCommentList[index], at: index+2)
                    }
                }
            }
            self.baseTableView?.reloadData()
        }) { (failure) in
            LXSWLog(failure)
        }
    }
    
    //MARK:praise tread
    //MARK:praiseOrtreadePost
    func praiseOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOneNormalTopic(name, action: action, value: value, id: id)
    }
    
    func tradeOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOrPraiseAction(name, action: action, value: value, id: id)
    }
    
    fileprivate var dao = HoroscopeDAO()
    func tradeOrPraiseAction(_ name: String, action: String, value: Bool, id: String) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            dao.likeOrDislikeAction(name, action: action, value: value, id: id)
        }
    }
    
    // MARK: CommentFootBtn delegate
    func commentFootBtnClick() {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let replyVC = PostViewController()
            replyVC.isFirstClassReply = true
            let replyModel = MyPostModel.init(jsonData: JSON.null, num: 0)
            replyVC.replyModel = replyModel
            replyVC.delegate = self
            replyModel.postId = self.postModel?.postId
            self.navigationController?.pushViewController(replyVC, animated: true)
        }
    }
    //MARK:- CommentViewCellDelegate
    @objc func CommentViewClickToShowCommentReplyView(_ noti: Notification) {
        guard let model = noti.userInfo?["model"] else {
            return
        }
        let replyView = ReplyCommentView(frame: self.view.bounds, model: model as! BaseComment)
        self.view.addSubview(replyView)
    }
    
    @objc func replyBtnClick(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            guard let model = noti.userInfo?["model"] else {
                return
            }
            let replyVC = PostViewController()
            replyVC.isReply = true
            replyVC.model = model as? BaseComment
            let replyModel = MyPostModel.init(jsonData: JSON.null, num: 0)
            replyVC.replyModel = replyModel
            replyModel.postId = (model as AnyObject).postId
            replyVC.delegate = self
            self.navigationController?.pushViewController(replyVC, animated: true)
        }
    }
    @objc func reportBtnClickToShowReportController(_ noti:Notification) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            guard let model = noti.userInfo?["model"] else {
                return
            }
            let mod = model as! BaseComment
            ReportViewController.addacrion(mod, postId: mod.postId ?? "", isPostReport: false,vc:self)
        }
    }
    @objc func commentsMoreThanSeventyFloor() {
        HUD.flash(.label("Too more comments"),delay: 1)
    }
    
    //MARK:- PostViewControllerDelegate
    func postCommentFailure(_ error: NSError) {
        HUD.flash(.label("delivery failure"),delay: 1)
    }
    
    func postCommentSuccess(_ comment: MainComment) {
        self.commentList.insert(comment, at: 0)
        self.baseTableView?.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
