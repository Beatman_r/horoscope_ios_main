//
//  ReadTagListViewController.swift
//  Horoscope
//
//  Created by Beatman on 17/2/21.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD

class ReadTagListViewController: BaseViewController,NormalPostCellDelegate,TagListViewModelDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addRetryView()
        self.title=NSLocalizedString("\(self.tagName.capitalized)", comment: "")
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.view.backgroundColor=UIColor.baseDetailBackgroundColor()
        initViewModel()
        self.setupRefreshHeaderView()
        self.setupRefreshFooterView()
        // Do any additional setup after loading the view.
    }
    
    var tagName : String = ""
    
    var viewModel:TagListViewModel?
    func initViewModel() {
        viewModel = TagListViewModel()
        viewModel?.delegate = self
        viewModel?.loadTagList(tagName, offset: 0, success: { 
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_header.endRefreshing()
            }, failed: { 
                self.failedRetryView?.loadFailed()
                self.baseTableView?.mj_header.endRefreshing()
        })
    }
    
    override func refreshData() {
        let offset = 0
        viewModel?.loadTagList(tagName, offset: offset, success: {
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_header.endRefreshing()
            }, failed: {
                self.failedRetryView?.loadFailed()
                self.baseTableView?.mj_header.endRefreshing()
        })
    }
    
    override func loadMoreList() {
        let offset = self.viewModel?.count ?? 0
        viewModel?.loadTagList(tagName, offset: offset, success: {
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            self.baseTableView?.mj_footer.endRefreshing()
            }, failed: {
                self.failedRetryView?.loadFailed()
                self.baseTableView?.mj_footer.endRefreshing()
        })
    }
    
    func shitHappends() {
        HUD.flash(.label("No More Topics"),delay: 1)
    }
    
    //RetryViewDelegate
    override func retryToLoadData() {
        viewModel?.loadTagList(tagName, offset: 0, success: {
            self.failedRetryView?.loadSuccess()
            self.baseTableView?.reloadData()
            }, failed: {
                self.failedRetryView?.loadFailed()
        })
    }
    
    override func registerCell() {
        self.baseTableView?.register(BaseTopicCell.self, forCellReuseIdentifier: "BaseTopicCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:tableviewDelegate
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return viewModel?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel?.getModel(indexPath.row)
        let cell = BaseTopicCell.dequeueReusableCell(tableView, cellForRowAtIndexPath: indexPath)
        
        cell.model = model
        cell.rootVc=self
        cell.delegate=self
       
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = viewModel?.getModel(indexPath.row)
        let vc = ReadPostViewController()
        vc.postModel = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = viewModel?.getModel(indexPath.row)
        return BaseTopicCell.calculateHeight(model)
    }

    
    //MARK:praiseOrtreadePost
    func praiseOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOneNormalTopic(name, action: action, value: value, id: id)
    }
    
    func tradeOneNormalTopic(_ name: String, action: String, value: Bool, id: String) {
        self.tradeOrPraiseAction(name, action: action, value: value, id: id)
    }
    
    func tradeOrPraiseAction(_ name: String, action: String, value: Bool, id: String) {
        let userStatus = AccountManager.sharedInstance.getLogStatus()
        if userStatus == .null || userStatus == .anonymous{
            let vc = HsLoginViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.viewModel?.dao.likeOrDislikeAction(name, action: action, value: value, id: id)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
