//
//  RateDetailViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/3.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import PKHUD
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class RateDetailViewController: UIViewController,UITextFieldDelegate {
    
    var rateView = UIView()
    var submitBtn = UIButton()
    var greenLine = UIView()
    var suggestLabel = UILabel()
    var starBtnsView = UIView()
    var suggestTextField = UITextField()
    var rankLab = UILabel()
    var starRank : Int?
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var starsList : [UIButton] = []
    let rankWordList = ["Strongly dislike","Dislike","Neither","Like","Strongly like"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.baseDetailBackgroundColor()
        self.title = "Rate Us"
        setupStarBtnsView()
        setupRankLabel()
        setupSuggestUI()
        self.addBackBtn()
        self.suggestTextField.delegate = self
        self.suggestTextField.textColor = UIColor.luckyPurpleTitleColor()
        submitBtn.addTarget(self, action: #selector(submitReview), for:.touchUpInside)
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    @objc func popView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func submitReview() {
        if suggestTextField.text?.isEmpty == false {
            HoroscopeDAO().postUserFeedback(suggestTextField.text!, rateDate: HSHelpCenter.sharedInstance.dateTool.getDateString(Date(), dateFormater: "yyyy-MM-dd HH:mm:ss") as NSString, starRate: self.rankLab.text!)
            let rateVC = RateUsView()
            rateVC.dismissCloseButton()
            
            HUD.flash(.success, delay: 1.0) { (true) in
                _ = self.navigationController?.popViewController(animated: true)
            }
            UserDefaults.standard.set(true, forKey: "hasRate")
            UserDefaults.standard.synchronize()
        }
    }
    
    func setupStarBtnsView() {
        
        for count in 0...4 {
            let starBtn = UIButton(frame: CGRect(x: screenWidth*0.14*CGFloat(count), y: 0, width: screenWidth*0.07, height: screenWidth*0.07))
            starBtn.setImage(UIImage(named: "ic_rate_star_empty_small"), for: UIControlState())
            starBtn.tag = count+200
            if starBtn.tag<=starRank {
                starBtn.setImage(UIImage(named: "card_star"), for: UIControlState())
            }
            starBtn.addTarget(self, action: #selector(changeRankLabel), for: .touchUpInside)
            starsList.append(starBtn)
            starBtnsView.addSubview(starBtn)
        }
        
        self.view.addSubview(starBtnsView)
        starBtnsView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.bottom).offset(-screenHeight/2-50)
            make.left.equalTo(self.view.snp.left).offset(screenWidth*0.015+screenWidth*0.18)
            make.right.equalTo(self.view.snp.right).offset(-screenWidth*0.015-screenWidth*0.18)
            make.bottom.equalTo(self.view.snp.bottom).offset(-screenHeight/2)
        }
    }
    
    func setupRankLabel() {
        self.rankLab.font = HSFont.baseMediumFont(22)
        self.rankLab.textColor = UIColor.luckyPurpleTitleColor()
        self.rankLab.textAlignment = NSTextAlignment.center
        self.view.addSubview(rankLab)
        
        self.rankLab.snp.makeConstraints { (make) in
            make.top.equalTo(self.starBtnsView.snp.bottom)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(200)
            make.height.equalTo(30)
        }
        for count in 0...starRank!-200 {
            rankLab.text = NSLocalizedString("\(rankWordList[count])", comment: "")
        }
    }
    
    func setupSuggestUI() {
        let placeHolderColor = UIColor(hexString: "ffffff", withAlpha: 0.1)
        let string = NSLocalizedString("Let us know what you think", comment: "")
        self.suggestTextField.attributedPlaceholder = NSAttributedString(string: string, attributes: [NSAttributedStringKey.foregroundColor : placeHolderColor as Any])
        self.suggestTextField.textAlignment = NSTextAlignment.center
        self.suggestTextField.returnKeyType = .done
        
        self.suggestLabel.text = NSLocalizedString("Your feedback is highly appreciated and will help us improve", comment: "")
        self.suggestLabel.font = HSFont.baseRegularFont(18)
        self.suggestLabel.numberOfLines = 0
        self.suggestLabel.textAlignment = NSTextAlignment.center
        self.suggestLabel.textColor = UIColor.luckyPurpleContentColor()
        
        self.greenLine.backgroundColor = UIColor(hexString: "ffffff", withAlpha: 0.1)
        
        self.submitBtn.layer.masksToBounds = true
        self.submitBtn.layer.cornerRadius = self.screenHeight * 25/667
        self.submitBtn.layer.borderColor = UIColor.commonPinkColor().cgColor
        self.submitBtn.layer.borderWidth = 1
        self.submitBtn.setTitle(NSLocalizedString("Submit", comment: ""), for: UIControlState())
        self.submitBtn.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        self.submitBtn.backgroundColor = UIColor.clear
        
        self.view.addSubview(suggestTextField)
        self.view.addSubview(suggestLabel)
        self.view.addSubview(greenLine)
        self.view.addSubview(submitBtn)
        
        self.submitBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom).offset(-self.screenHeight * 40/667)
            make.top.equalTo(self.view.snp.bottom).offset(-self.screenHeight * 90/667)
            make.left.equalTo(self.view.snp.left).offset(30)
            make.right.equalTo(self.view.snp.right).offset(-30)
        }
        self.suggestLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.submitBtn.snp.top).offset(-10)
            make.top.equalTo(self.submitBtn.snp.top).offset(-80)
            make.left.equalTo(self.view.snp.left).offset(30)
            make.right.equalTo(self.view.snp.right).offset(-30)
        }
        
        self.greenLine.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.suggestLabel.snp.top).offset(-31)
            make.top.equalTo(self.suggestLabel.snp.top).offset(-32)
            make.left.equalTo(self.view.snp.left).offset(26)
            make.right.equalTo(self.view.snp.right).offset(-26)
        }
        
        self.suggestTextField.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.greenLine.snp.top)
            make.top.equalTo(self.greenLine.snp.top).offset(-30)
            make.left.equalTo(self.view.snp.left).offset(26)
            make.right.equalTo(self.view.snp.right).offset(-26)
        }
    }
    
    func selectStar(_ sender:UIButton) {
        
        for index  in 200...204 {
            
            let button:UIButton=starBtnsView.viewWithTag(index) as!UIButton
            
            if button.tag<=sender.tag{
                button.setImage(UIImage(named: "card_star"), for: UIControlState())
            }
            else{
                button.setImage(UIImage(named: "ic_rate_star_empty_small"), for: UIControlState())
            }
        }
    }
    
    @objc func changeRankLabel(_ sender:UIButton) {
        selectStar(sender)
        
        self.rankLab.text = rankWordList[sender.tag-200]
    }
    
    //MARK: TextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        animatedTextField(true)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        animatedTextField(false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        suggestTextField.endEditing(true)
        return true
    }
    
    func animatedTextField(_ up:Bool)    {
        
        let movementDistance=80
        
        let movement: Int = (up ? -movementDistance : movementDistance)
        
        UIView.beginAnimations("Animation", context: nil)
        
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(0.2)
        
        self.view.frame=self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        
        UIView.commitAnimations()
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
