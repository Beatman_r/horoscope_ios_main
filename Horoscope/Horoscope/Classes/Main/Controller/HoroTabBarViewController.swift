//
//  MainTabBarViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/11/23.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class HoroTabBarViewController: UITabBarController,UITabBarControllerDelegate {
    var canShowAd:Bool = true
    static var gloableInstance: HoroTabBarViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.value(forKey: "userBirthday") == nil{
            UserDefaults.standard.setValue("", forKey: "userBirthday")
        }
        HoroTabBarViewController.gloableInstance = self
        if NotificationLimitManager.sharedInstance.firstStartUpApp() == true{
            showStartAnimate()
            addChildVC()
           }else{
            loadAd()
            showStartAnimate()
            addChildVC()
        }
        self.delegate=self
        self.automaticallyAdjustsScrollViewInsets = false
        //todo:先放到这个位置，因为之前老的代码再navgationBar时候会有UI问题，注意：这里由于TabBarViewController一直会存在，所以这里不移除通知
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadIntertitialAd), name: NSNotification.Name(rawValue: HomeIntertitialAdShowNoti), object: nil)

        if NotificationLimitManager.sharedInstance.isOldUpdateVersion() == true {
            NotificationLimitManager.sharedInstance.registerLocalNotification()
        }
    }
    

    
    let bgImg : UIImageView = {
        let bgImg = UIImageView()
        let bgImgPath = Bundle.main.path(forResource: "splash_animate_background", ofType: "jpg")
        bgImg.image = UIImage(contentsOfFile: bgImgPath ?? "")
        return bgImg
    }()
    
    let airesCharacter : UIImageView = {
        let airesCharater = UIImageView()
        let airesPath = Bundle.main.path(forResource: "splash_animate_aries", ofType: "png")
        airesCharater.image = UIImage(contentsOfFile: airesPath ?? "")
        airesCharater.alpha = 0
        return airesCharater
    }()
    
    let leoCharacter : UIImageView = {
        let leoCharacter = UIImageView()
        let leoPath = Bundle.main.path(forResource: "splash_animate_leo", ofType: "png")
        leoCharacter.image = UIImage(contentsOfFile: leoPath ?? "")
        leoCharacter.alpha = 0
        return leoCharacter
    }()
    
    let scorpioCharacter : UIImageView = {
        let scorpioCharacter = UIImageView()
        let scorpioPath = Bundle.main.path(forResource: "splash_animate_scorpio", ofType: "png")
        scorpioCharacter.image = UIImage(contentsOfFile: scorpioPath ?? "")
        scorpioCharacter.alpha = 0
        return scorpioCharacter
    }()
    
    let taurusCharacter : UIImageView = {
        let taurusCharacter = UIImageView()
        let taurusPath = Bundle.main.path(forResource: "splash_animate_taurus", ofType: "png")
        taurusCharacter.image = UIImage(contentsOfFile: taurusPath ?? "")
        taurusCharacter.alpha = 0
        return taurusCharacter
    }()
    
    let splashCloud : UIImageView = {
        let splashCloud = UIImageView()
        let cloudPath = Bundle.main.path(forResource: "splash_animate_cloud", ofType: "png")
        splashCloud.image = UIImage(contentsOfFile: cloudPath ?? "")
        splashCloud.alpha = 0
        return splashCloud
    }()
    
    let groundfront : UIImageView = {
        let groundfront = UIImageView()
        let groundFrontPath = Bundle.main.path(forResource: "splash_animate_ground2", ofType: "png")
        groundfront.image = UIImage(contentsOfFile: groundFrontPath ?? "")
        groundfront.alpha = 0
        return groundfront
    }()
    
    let groundBack : UIImageView = {
        let groundBack = UIImageView()
        let groundBackPath = Bundle.main.path(forResource: "splash_animate_ground1", ofType: "png")
        groundBack.image = UIImage(contentsOfFile: groundBackPath ?? "")
        return groundBack
    }()
    
    let splashHoroscope : UIImageView = {
        let splashHoroscope = UIImageView()
        let horoscopePath = Bundle.main.path(forResource: "splash_animate_horoscope", ofType: "png")
        splashHoroscope.image = UIImage(contentsOfFile: horoscopePath ?? "")
        splashHoroscope.alpha = 0
        return splashHoroscope
    }()
    
    let splashMoon : UIImageView = {
        let splashMoon = UIImageView()
        let splashMoonPath = Bundle.main.path(forResource: "splash_animate_moon", ofType: "png")
        splashMoon.image = UIImage(contentsOfFile: splashMoonPath ?? "")
        splashMoon.alpha = 0
        return splashMoon
    }()
    
    let splashStar : UIImageView = {
        let splashStar = UIImageView()
        let splashStarPath = Bundle.main.path(forResource: "splash_animate_star", ofType: "png")
        splashStar.image = UIImage(contentsOfFile: splashStarPath ?? "")
        splashStar.alpha = 0
        return splashStar
    }()

    func loadAd() {
        IntertitialAdManager.shareInstance.loadSplash { (isSuccess) in
            IntertitialAdManager.shareInstance.show(.SplashAdV2, fromVC: self, complete: { (isSuccess) in
                self.hideStartAnimate()
            })
        }
    }
    
    @objc func  loadIntertitialAd(){
        IntertitialAdManager.shareInstance.loadAdConfigData(adPlaceMentKey: .HomeInterstitial_002) { (isSuccess) in
            IntertitialAdManager.shareInstance.showIntertitialAd(fromVC: self, complete: { (isSuccess) in
            })
        }
  
        
    }
    func addSplashAnimation() {
        self.view.addSubview(bgImg)
        self.view.addSubview(airesCharacter)
        self.view.addSubview(splashCloud)
        self.view.addSubview(groundfront)
        self.view.addSubview(groundBack)
        self.view.addSubview(splashHoroscope)
        self.view.addSubview(leoCharacter)
        self.view.addSubview(splashMoon)
        self.view.addSubview(scorpioCharacter)
        self.view.addSubview(splashStar)
        self.view.addSubview(taurusCharacter)
        
        self.bgImg.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.airesCharacter.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.splashCloud.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.groundfront.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.groundBack.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.splashHoroscope.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.view)
            make.centerY.equalTo(self.view).offset(50)
            make.width.equalTo(self.view)
            make.height.equalTo(self.view)
        }
        self.leoCharacter.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.splashMoon.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.scorpioCharacter.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.splashStar.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        self.taurusCharacter.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
    }
    
    func startSplashAnimate() {
//        UIView.animateWithDuration(1) {
//            self.groundfront.alpha = 1
//        }
        UIView.animate(withDuration: 1, animations: {
            self.groundfront.alpha = 1
            }, completion: { (true) in
                UIView.animate(withDuration: 1.2, animations: {
                    self.splashMoon.alpha = 1
                    self.scorpioCharacter.alpha = 1
                    self.leoCharacter.alpha = 1
                    self.airesCharacter.alpha = 1
                    self.taurusCharacter.alpha = 1
                    }, completion: { (true) in
                        
                        UIView.animate(withDuration: 1.5, animations: {
                            self.splashHoroscope.alpha = 1
                            self.splashStar.alpha = 1
                            self.splashHoroscope.snp.updateConstraints { (make) in
                                make.centerX.equalTo(self.view)
                                make.centerY.equalTo(self.view)
                                make.width.equalTo(self.view)
                                make.height.equalTo(self.view)
                            }
                           // self.view.layoutIfNeeded()
                            }, completion: { (true) in
                                if NotificationLimitManager.sharedInstance.firstStartUpApp() == true{
                                    self.hideStartAnimate()
                                }
                        })
                })
        }) 
    }
    
    func showStartAnimate() {
        self.addSplashAnimation()
        self.startSplashAnimate()
    }
    
    func hideStartAnimate() {
        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            UIView.animate(withDuration: 0.5, animations: {
                self.bgImg.alpha = 0
                self.airesCharacter.alpha = 0
                self.splashCloud.alpha = 0
                self.groundfront.alpha = 0
                self.groundBack.alpha = 0
                self.splashHoroscope.alpha = 0
                self.leoCharacter.alpha = 0
                self.splashMoon.alpha = 0
                self.scorpioCharacter.alpha = 0
                self.splashStar.alpha = 0
                self.taurusCharacter.alpha = 0
            }, completion: { (true) in
                self.bgImg.removeFromSuperview()
                self.airesCharacter.removeFromSuperview()
                self.splashCloud.removeFromSuperview()
                self.groundfront.removeFromSuperview()
                self.groundBack.removeFromSuperview()
                self.splashHoroscope.removeFromSuperview()
                self.leoCharacter.removeFromSuperview()
                self.splashMoon.removeFromSuperview()
                self.scorpioCharacter.removeFromSuperview()
                self.splashStar.removeFromSuperview()
                self.taurusCharacter.removeFromSuperview()
            }) 
        }
    }
    
    var adView :UIImageView?
    func addChildVC() {
//        self.addChildViewController("DailyViewController", title: "Forecast", imageName: "hs_daily_")
   //     self.addChildViewController("VideoViewController", title: "Video", imageName: "hs_video_")
    //    self.addChildViewController("MeViewController", title: "Me", imageName: "ic_characteristics_")
//
//        self.addChildViewController("MatchViewController", title: "Match", imageName: "hs_match_")
       self.addChildViewController("MainDailyScrollController", title: NSLocalizedString("", comment: ""), imageName: "hs_fortune_")
     }
    
    //refactor: 多此一举...., 将class的创建, 改为通过"类名"动态创建, 放弃了类型检查, 没有得到便利(因为类名也是hard code在代码里的), 反而把可能的错误从编译期推到了运行期.
    func addChildViewController(_ ChildViewControllerName: String,title : String , imageName : String) {
        let nameSpace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String
        let cls : AnyClass = NSClassFromString(nameSpace + "." + ChildViewControllerName)!
        let vccls = cls as! UIViewController.Type
        let vc = vccls.init()
        vc.tabBarItem.title = title
        vc.tabBarItem.image = UIImage(named: imageName + "grey_")
        vc.tabBarItem.selectedImage = UIImage(named: imageName)
        tabBar.tintColor = UIColor.tabbarTintColor()
        tabBar.barTintColor = UIColor.white
        let nav = HoroNavigationController()
        nav.addChildViewController(vc)
        vc.title = title
        addChildViewController(nav)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var shouldAutorotate :Bool {
        return self.selectedViewController?.shouldAutorotate ?? false
    }
    
    override var supportedInterfaceOrientations:UIInterfaceOrientationMask {
        return self.selectedViewController?.supportedInterfaceOrientations ?? .all
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
