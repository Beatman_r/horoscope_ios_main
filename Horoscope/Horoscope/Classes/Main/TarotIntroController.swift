//
//  TarotIntroController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit

class TarotIntroController: TarotBaseViewController {

    var topic : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createIntroSubviews()
    }
    
    func createIntroCardBack() ->UIImageView{
        let cardBackImg = UIImageView()
        cardBackImg.image = UIImage(named: "tarot_card_main")
        return cardBackImg
    }
    
    let topLeftBackLight = UIImageView()
    let topRightBackLight = UIImageView()
    let bottomBackLight = UIImageView()
    
    func createIntroSubviews() {
        
        let backLightPadding = 0
        
        let topLeftCardBack = createIntroCardBack()
        let topRightCardBack = createIntroCardBack()
        let bottomCardBack = createIntroCardBack()
        
        topLeftBackLight.image = UIImage(named: "tarot_card_back_light")
        topRightBackLight.image = UIImage(named: "tarot_card_back_light")
        bottomBackLight.image = UIImage(named: "tarot_card_back_light")
    
        let string = NSLocalizedString("This is the very common three-card-spread, it is good for a quick answer to most questions and involves a reading of your past, present and future regarding a particular situation in your life.", comment: "")
        let text = HSHelpCenter.sharedInstance.textTool.setTextLineSpace(string, space: 1.2)
        let introduceLabel = UILabel()
        introduceLabel.attributedText = text
        introduceLabel.font = HSFont.baseRegularFont(self.screenHeight == 480 ? 12 : 18)
        introduceLabel.textColor = UIColor.hsTarotContentColor()
        introduceLabel.numberOfLines = 0
        introduceLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        introduceLabel.textAlignment = NSTextAlignment.center
        
        
        let goButon = UIButton()
        let buttonImg = UIImageView(image: UIImage(named: "tarot_button_"))
        let buttonTitleLabel = UILabel()
        buttonTitleLabel.textColor = UIColor.white
        buttonTitleLabel.textAlignment = NSTextAlignment.center
        let font = UIFont.init(name: "Charter Bd BT", size: 24)
        buttonTitleLabel.text = NSLocalizedString("GO", comment: "")
        buttonTitleLabel.font = font
        goButon.addSubview(buttonImg)
        goButon.addSubview(buttonTitleLabel)
        goButon.addTarget(self, action: #selector(goButtonOnClick), for: .touchUpInside)
        
        self.view.addSubview(topLeftBackLight)
        self.view.addSubview(topRightBackLight)
        self.view.addSubview(bottomBackLight)
        self.view.addSubview(topLeftCardBack)
        self.view.addSubview(topRightCardBack)
        self.view.addSubview(bottomCardBack)
        self.view.addSubview(introduceLabel)
        self.view.addSubview(goButon)
        
        topLeftCardBack.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(screenHeight * 80/667)
            make.right.equalTo(self.view.snp.centerX).offset(-(screenWidth * 50/375))
            make.width.equalTo(self.screenWidth * 81/375)
            make.height.equalTo(self.screenWidth * 81/375 * 1.6)
        }
        topLeftBackLight.snp.makeConstraints { (make) in
            make.top.equalTo(topLeftCardBack.snp.top).offset(-backLightPadding)
            make.bottom.equalTo(topLeftCardBack.snp.bottom).offset(backLightPadding)
            make.left.equalTo(topLeftCardBack.snp.left).offset(-backLightPadding)
            make.right.equalTo(topLeftCardBack.snp.right).offset(backLightPadding)
        }
        
        topRightCardBack.snp.makeConstraints { (make) in
            make.top.equalTo(topLeftCardBack.snp.top)
            make.left.equalTo(self.view.snp.centerX).offset(screenWidth * 50/375)
            make.width.equalTo(self.screenWidth * 81/375)
            make.height.equalTo(self.screenWidth * 81/375 * 1.6)
        }
        topRightBackLight.snp.makeConstraints { (make) in
            make.top.equalTo(topRightCardBack.snp.top).offset(-backLightPadding)
            make.bottom.equalTo(topRightCardBack.snp.bottom).offset(backLightPadding)
            make.left.equalTo(topRightCardBack.snp.left).offset(-backLightPadding)
            make.right.equalTo(topRightCardBack.snp.right).offset(backLightPadding)
        }
        
        bottomCardBack.snp.makeConstraints { (make) in
            make.top.equalTo(topLeftCardBack.snp.bottom).offset(screenHeight * 30/667)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.screenWidth * 81/375)
            make.height.equalTo(self.screenWidth * 81/375 * 1.6)
        }
        bottomBackLight.snp.makeConstraints { (make) in
            make.top.equalTo(bottomCardBack.snp.top).offset(-backLightPadding)
            make.bottom.equalTo(bottomCardBack.snp.bottom).offset(backLightPadding)
            make.left.equalTo(bottomCardBack.snp.left).offset(-backLightPadding)
            make.right.equalTo(bottomCardBack.snp.right).offset(backLightPadding)
        }
        
        introduceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(bottomCardBack.snp.bottom).offset(screenHeight * 36/667)
            make.left.equalTo(self.view.snp.left).offset(20)
            make.right.equalTo(self.view.snp.right).offset(-20)
        }
        
        goButon.snp.makeConstraints { (make) in
            make.top.equalTo(introduceLabel.snp.bottom).offset(self.screenWidth == 320 ? screenHeight * 15/667 : screenHeight * 36/667)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(introduceLabel).offset(-20)
            make.height.equalTo(screenHeight * 65/667)
        }
        
        buttonImg.snp.makeConstraints { (make) in
            make.edges.equalTo(goButon)
        }
        buttonTitleLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(goButon)
        }
    }
    
    @objc func goButtonOnClick() {
        let vc = TarotPrepareController()
        vc.topic = self.topic
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
