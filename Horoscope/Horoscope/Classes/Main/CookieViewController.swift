//
//  CookieViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/12/7.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SnapKit
import AVFoundation
import pop

class CookieViewController: UIViewController {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    let buttonUnclickString = NSLocalizedString("Shake or Click", comment: "")
    let buttonClickedString = NSLocalizedString("Open another one?", comment: "")
    
    let backgroundImageView = UIImageView()
    let cookieImageView = UIImageView()
    var labelImgView = UIImageView()
    var fortuneLabel = UILabel()
    let backgroundView = UIView()
    let fortuneDetailLabel = UILabel()
    let cookieTitleLabel = UILabel()
    let snackPieceView = UIImageView()
    let piecesImgView = UIImageView()
    let crackButton = UIButton(type: .custom)
    let shakeImageView = UIImageView()
    let shakeFrontImage = UIImageView()
    let bottomBackView = UIView()
    
    var fakeView : UIView?
    
    let shareButton = UIButton()
    //  let shareTitleLabel = UILabel()
    //  let shareImageView = UIImageView()
    
    let shakeTipsImageView = UIImageView(image: UIImage(named: "img_shake"))
    let shakeTipsLabel = UILabel()

    let buttonUnclickImg = UIImage(named: "button_pink")
    let buttonClickedImg = UIImage(named: "button_blue")
    let buttonImageView = UIImageView()
    let buttonTitleLabel = UILabel()
    
    var player: AVAudioPlayer?
    var crackCount : Int = 0
    
    fileprivate let fortuneDetail : String = NSLocalizedString("Need a little bit of sage advice or a quick pick-me-up? Get the wisdom of your fortune cookie without the calories!", comment: "")
    let adContainerView  = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AnaliticsManager.sendEvent(AnaliticsManager.cookie_show)
        self.navigationItem.title = NSLocalizedString("Fortune Cookie", comment: "")
        self.view.backgroundColor = UIColor.white
        let backImgView = UIImageView(frame: UIScreen.main.bounds)
        backImgView.image = UIImage(named: "background.jpg")
        self.view.addSubview(backImgView)
        
        UIApplication.shared.applicationSupportsShakeToEdit = true
        self.setupBackgroundView()
        let timer = Timer.scheduledTimer(timeInterval: 1.4, target: self, selector: #selector(shakeAnimation), userInfo: nil, repeats: true)
        timer.fire()
        if UserDefaults.standard.bool(forKey: "hasShaked") {
            shakeImageView.removeFromSuperview()
        }
        self.addBackBtn()
        self.adContainerView.layer.cornerRadius = 5
        self.adContainerView.layer.masksToBounds = true
        self.adContainerView.isHidden = true

        self.bottomBackView.backgroundColor = UIColor.white
        self.bottomBackView.layer.cornerRadius = 5
        self.bottomBackView.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    @objc func popView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func readyToPop() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: motionTracker
    
    func initPlayer(_ wavName:String) {
        let voicePath = Bundle.main.path(forResource: wavName, ofType: "wav")
        let voiceData = try? Data(contentsOf: URL(fileURLWithPath: voicePath!))
        self.player = try? AVAudioPlayer(data: voiceData!)
    }
    
    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
        self.initPlayer("shakingVoice")
        self.player?.play()
        self.shakeImageView.isHidden = true
        UserDefaults.standard.set(true, forKey: "hasShaked")
        UserDefaults.standard.synchronize()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        self.player?.pause()
        if self.firstClick == true{
            AnaliticsManager.sendEvent(AnaliticsManager.cookie_open, data: ["action":"shake"])
        }else{
            AnaliticsManager.sendEvent(AnaliticsManager.cookie_another_open, data: ["action":"shake"])
        }
        self.crackTheCookie()
    }
    
    func createShareButton() {
        let shareButton : UIButton = UIButton()
        shareButton.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
        shareButton.setImage(UIImage(named: "ic_share_"), for: UIControlState())
        shareButton.addTarget(self, action: #selector(shareButtonOnClick), for: .touchUpInside)
        let rightItem = UIBarButtonItem(customView: shareButton)
        self.navigationItem.rightBarButtonItem = rightItem
    }
    
    func setupBackgroundView() {
        backgroundView.backgroundColor = UIColor.white
        backgroundView.layer.cornerRadius = 8
        backgroundView.layer.masksToBounds = true
        backgroundView.addSubview(backgroundImageView)
        
        bottomBackView.backgroundColor = UIColor.white
        bottomBackView.layer.cornerRadius = 8
        bottomBackView.layer.masksToBounds = true
        
        shakeTipsLabel.font = HSFont.baseRegularFont(20.0)
        shakeTipsLabel.textColor = UIColor(hexString: "F95CD1")
        shakeTipsLabel.text = NSLocalizedString("Shake Shake!", comment: "")

        self.fakeView = UIView()
        self.fakeView?.backgroundColor = UIColor.clear
        backgroundView.addSubview(self.fakeView!)
        
        cookieTitleLabel.text = NSLocalizedString("Fortune Cookie", comment: "")
        cookieTitleLabel.font = HSFont.baseTitleFont()
        cookieTitleLabel.textColor = UIColor.hsTextColor(1)
        self.fakeView?.addSubview(cookieTitleLabel)
        
        fortuneDetailLabel.text = fortuneDetail
        fortuneDetailLabel.numberOfLines = 0
        fortuneDetailLabel.textColor = UIColor.hsTextColor(1)
        fortuneDetailLabel.font = HSFont.baseContentFont()
        if screenWidth == 320 {
            fortuneDetailLabel.font = UIFont.systemFont(ofSize: 14)
        }
        snackPieceView.image = UIImage(named: "cookie_7")
        self.fakeView?.addSubview(snackPieceView)
        
        labelImgView.image = UIImage(named: "cookie_card")
        labelImgView.backgroundColor = UIColor.clear
        self.fakeView?.addSubview(labelImgView)
        
        fortuneLabel.text = ""
        if screenWidth == 320 {
            fortuneLabel.font = UIFont.systemFont(ofSize: 12)
        }else{
            fortuneLabel.font = UIFont.systemFont(ofSize: 16)
        }
        fortuneLabel.numberOfLines = 3
        fortuneLabel.textColor = UIColor.hsTextColor(1)
        fortuneLabel.textAlignment = NSTextAlignment.center
        _ = fortuneLabel.alignmentRectInsets
        fortuneLabel.sizeToFit()
        labelImgView.addSubview(fortuneLabel)
        
        cookieImageView.image = UIImage(named: "cookie_1.jpg")
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(clickImg))
        cookieImageView.addGestureRecognizer(tap)
        cookieImageView.isUserInteractionEnabled = true
        self.fakeView?.addSubview(cookieImageView)
        self.fakeView?.addSubview(fortuneDetailLabel)
        
        piecesImgView.image = UIImage(named: "paperPieces")
        piecesImgView.isHidden = true
        self.fakeView?.addSubview(piecesImgView)
        
        crackButton.layer.cornerRadius = 8
        crackButton.layer.masksToBounds = true
        buttonTitleLabel.text = buttonUnclickString
        buttonTitleLabel.textAlignment = NSTextAlignment.center
        buttonTitleLabel.font = HSFont.baseTitleFont()
        buttonTitleLabel.textColor = UIColor.white
        buttonImageView.image = buttonUnclickImg
        crackButton.addSubview(buttonImageView)
        crackButton.addSubview(buttonTitleLabel)
        crackButton.addTarget(self, action: #selector(clickBtn), for: .touchUpInside)
        crackButton.layer.cornerRadius = crackButton.bounds.height/2
        crackButton.layer.masksToBounds = true
        
        shareButton.bounds.size = CGSize(width: 30, height: 30)
        shareButton.setImage(UIImage(named: "ic_share_"), for: UIControlState())
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: shareButton)
        shareButton.isHidden = true
        shareButton.addTarget(self, action: #selector(shareButtonOnClick), for: .touchUpInside)
        
        shakeImageView.image = UIImage(named: "ic_shake_bg")
        self.backgroundView.addSubview(shakeImageView)
        
        shakeFrontImage.image = UIImage(named: "ic_shake_front")
        self.shakeImageView.addSubview(shakeFrontImage)
        
        
        self.view.addSubview(crackButton)
        self.view.addSubview(backgroundView)
        self.view.addSubview(bottomBackView)

        //MARK: Make Constraint
        backgroundView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(screenWidth*0.202)
            make.left.equalTo(self.view.snp.left).offset(screenWidth*0.024)
            make.right.equalTo(self.view.snp.right).offset(-screenWidth*0.024)
            make.bottom.equalTo(self.view.snp.top).offset(screenHeight*0.662)
        }
        
        bottomBackView.snp.makeConstraints { (make) in
            make.left.equalTo(self.backgroundView)
            make.right.equalTo(self.backgroundView)
            make.height.equalTo((screenWidth * 100.0/375))
            make.top.equalTo(self.backgroundView.snp.bottom).offset(-10)
        }
        
        let bottomBackContainer = UIView()
        bottomBackContainer.backgroundColor = UIColor.clear
        bottomBackContainer.addSubview(shakeTipsImageView)
        bottomBackContainer.addSubview(shakeTipsLabel)
        
        shakeTipsImageView.snp.makeConstraints { (make) in
            make.leading.top.bottom.equalToSuperview()
        }
        
        shakeTipsLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(shakeTipsImageView.snp.trailing).offset(20.0)
            make.centerY.equalTo(shakeTipsImageView)
            make.trailing.equalToSuperview()
        }
        
        bottomBackView.addSubview(bottomBackContainer)
        bottomBackContainer.snp.makeConstraints { (make) in
            make.centerX.bottom.equalToSuperview()
        }

        backgroundImageView.snp.makeConstraints { (make) in
            make.top.equalTo(backgroundView.snp.top)
            make.bottom.equalTo(backgroundView.snp.bottom)
            make.left.equalTo(backgroundView.snp.left)
            make.right.equalTo(backgroundView.snp.right)
        }
        
        self.fakeView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(backgroundView.snp.edges)
        })
        
        cookieTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(backgroundView.snp.top).offset(screenWidth*0.036)
            make.left.equalTo(backgroundView.snp.left).offset(screenWidth*0.048)
            make.right.equalTo(backgroundView.snp.right).offset(-screenWidth*0.048)
            make.bottom.equalTo(backgroundView.snp.top).offset(screenWidth*0.145)
        }
        
        fortuneDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(cookieTitleLabel.snp.bottom)
            make.left.equalTo(self.fakeView!.snp.left).offset(screenWidth*0.048)
            make.right.equalTo(self.fakeView!.snp.right).offset(-screenWidth*0.048)
            make.bottom.equalTo(self.fortuneDetailLabel.snp.top).offset(screenWidth*0.193)
        }
        
        if screenWidth == 320 {
            snackPieceView.snp.makeConstraints { (make) in
                make.top.equalTo(fakeView!.snp.top).offset(screenWidth*0.32)
                make.bottom.equalTo(self.fakeView!.snp.bottom).offset(-screenWidth*0.109)
                make.left.equalTo(self.fakeView!.snp.left).offset(-screenWidth*0.0375)
                make.right.equalTo(self.fakeView!.snp.right).offset(-screenWidth*0.030)
            }
        }else{
            snackPieceView.snp.makeConstraints { (make) in
                make.top.equalTo(fakeView!.snp.top).offset(screenWidth*0.32)
                make.bottom.equalTo(self.fakeView!.snp.bottom).offset(-screenWidth*0.109)
                make.left.equalTo(self.fakeView!.snp.left).offset(-screenWidth*0.0375)
                make.right.equalTo(self.fakeView!.snp.right).offset(-screenWidth*0.030)
            }
        }
        
        labelImgView.snp.makeConstraints { (make) in
            make.center.equalTo(cookieImageView.snp.center)
            make.width.equalTo(cookieImageView.snp.width).offset(-screenWidth*0.072)
            make.height.equalTo(cookieImageView.snp.height).offset(0)
        }
        
        fortuneLabel.snp.makeConstraints { (make) in
            make.left.equalTo(labelImgView).offset(screenWidth*0.12)
            make.right.equalTo(labelImgView).offset(-screenWidth*0.048)
            make.top.equalTo(labelImgView).offset(screenWidth*0.12)
            make.bottom.equalTo(labelImgView).offset(-screenWidth*0.12)
        }
        //MARK: CookieImageView Constraint
        if screenWidth == 320 {
            cookieImageView.snp.makeConstraints({ (make) in
                make.top.equalTo(fakeView!.snp.top).offset(screenWidth*0.3)
                make.left.equalTo(fakeView!.snp.left).offset(screenWidth*0.12)
                make.right.equalTo(fakeView!.snp.right).offset(screenWidth*0.193)
                make.bottom.equalTo(fakeView!.snp.bottom).offset(0)
            })
        }else{
            cookieImageView.snp.makeConstraints { (make) in
                make.top.equalTo(fakeView!.snp.top).offset(screenWidth*0.3)
                make.left.equalTo(fakeView!.snp.left).offset(screenWidth*0.12)
                make.right.equalTo(fakeView!.snp.right).offset(screenWidth*0.193)
                make.bottom.equalTo(fakeView!.snp.bottom).offset(0)
            }
        }
        
        
        piecesImgView.snp.makeConstraints({ (make) in
            make.top.equalTo(self.cookieImageView.snp.top)
            make.left.equalTo(self.cookieImageView.snp.left).offset(self.screenWidth*0.05)
            make.bottom.equalTo(self.cookieImageView.snp.bottom)
            make.right.equalTo(self.cookieImageView.snp.right).offset(self.screenWidth*0.05)
        })
        
        shakeImageView.snp.makeConstraints { (make) in
            
            make.centerY.equalTo(backgroundView.snp.centerY).offset(screenWidth*0.185)
            make.centerX.equalTo(backgroundView.snp.centerX)
            make.width.equalTo(screenWidth*0.2415)
            make.height.equalTo(screenWidth*0.2415)
        }
        
        shakeFrontImage.snp.makeConstraints { (make) in
            make.top.equalTo(shakeImageView.snp.top)
            make.bottom.equalTo(shakeImageView.snp.bottom)
            make.left.equalTo(shakeImageView.snp.left)
            make.right.equalTo(shakeImageView.snp.right)
        }
        
        //add addContainer
        self.view.addSubview(self.adContainerView)
        self.adContainerView.layer.cornerRadius = 5
        self.adContainerView.layer.masksToBounds = true
        self.adContainerView.backgroundColor = UIColor.clear
        self.adContainerView.snp.makeConstraints { (make) in
            make.left.equalTo(self.view.snp.left).offset(10)
            make.right.equalTo(self.view.snp.right).offset(-10)
            make.height.equalTo((screenWidth*245/375))
            make.top.equalTo(self.backgroundView.snp.bottom).offset(10)
        }
    }
    
    //MARK: shakePOPAnimation
    @objc func shakeAnimation() {
        let shakeSpringAnimation = POPSpringAnimation(propertyNamed: kPOPLayerRotation)
        shakeSpringAnimation?.velocity = NSNumber(value: 20.0 as Float)
        shakeSpringAnimation?.springBounciness = 20.0
        shakeFrontImage.layer.pop_add(shakeSpringAnimation, forKey: "shakeSpringAnimaion")
    }
    
    //MARK: ShareFunc
    @objc func shareButtonOnClick(){
        AnaliticsManager.sendEvent(AnaliticsManager.cookie_share_click)
        let shareImg = UIImage.screenCapture(with: backgroundView, capture: CGRect(x: 0, y: screenHeight*0.4, width: backgroundView.bounds.width,  height: backgroundView.bounds.height))
        let activityViewController = UIActivityViewController(activityItems: [shareImg!],applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    var firstClick:Bool=true
    @objc func clickImg(){
        if self.firstClick == true{
            AnaliticsManager.sendEvent(AnaliticsManager.cookie_open, data: ["action":"click_screen"])
        }else{
            AnaliticsManager.sendEvent(AnaliticsManager.cookie_another_open, data: ["action":"click_screen"])
        }
        crackTheCookie()
    }
    
    @objc func clickBtn() {
        if self.firstClick == true{
            AnaliticsManager.sendEvent(AnaliticsManager.cookie_open, data: ["action":"click_button"])
        }else{
            AnaliticsManager.sendEvent(AnaliticsManager.cookie_another_open, data: ["action":"click_another_button"])
        }
        crackTheCookie()
    }
    
    func crackTheCookie() {
        UIApplication.shared.applicationSupportsShakeToEdit = false
        self.player?.pause()
        self.shakeImageView.isHidden = true
        self.backgroundView.bringSubview(toFront: self.backgroundImageView)
        self.piecesImgView.isHidden = true
        self.fortuneDetailLabel.textColor = UIColor.clear
        let randomNum = arc4random()%49
        self.fortuneLabel.text = cookieMessageList[Int(randomNum)]
        UIView.animate(withDuration: 1, animations: {
            if self.screenWidth == 320{
                self.cookieImageView.snp.remakeConstraints { (make) in
                    make.top.equalTo(self.cookieTitleLabel.snp.bottom).offset(self.screenWidth*0.10)
                    make.bottom.equalTo(self.fakeView!.snp.bottom).offset(-self.screenWidth*0.1)
                    make.left.equalTo(self.fakeView!.snp.left)
                    make.right.equalTo(self.fakeView!.snp.right).offset(-self.screenWidth*0.05)
                }
            }else{
                self.cookieImageView.snp.remakeConstraints { (make) in
                    make.top.equalTo(self.cookieTitleLabel.snp.bottom).offset(self.screenWidth*0.10)
                    make.bottom.equalTo(self.fakeView!.snp.bottom).offset(-self.screenWidth*0.1)
                    make.left.equalTo(self.fakeView!.snp.left)
                    make.right.equalTo(self.fakeView!.snp.right).offset(-self.screenWidth*0.05)
                }
            }
            self.cookieImageView.superview!.layoutIfNeeded()
        }, completion: { (true) in
            self.labelImgView.snp.remakeConstraints { (make) in
                make.centerY.equalTo(self.cookieImageView.snp.centerY)
                make.centerX.equalTo(self.cookieImageView.snp.centerX).offset(-self.screenWidth*0.307)
                make.width.equalTo(self.cookieImageView.snp.width).offset(-self.screenWidth*0.072)
                make.height.equalTo(self.cookieImageView.snp.height).offset(self.screenWidth*0.2415)
            }
            self.fortuneLabel.snp.remakeConstraints { (make) in
                make.left.equalTo(self.labelImgView).offset(self.screenWidth*0.267)
                make.right.equalTo(self.labelImgView).offset(-self.screenWidth*0.096)
                make.top.equalTo(self.labelImgView).offset(self.screenWidth*0.096)
                make.bottom.equalTo(self.labelImgView).offset(-self.screenWidth*0.144)
            }
            self.cookieImageView.image = UIImage(named: "cookie_5")
            var cookieAnimationImgArr : [UIImage] = []
            for count in 1...4 {
                let imgName : String = "cookie_\(count).jpg"
                let keyImage = UIImage(named: imgName)!
                cookieAnimationImgArr.append(keyImage)
            }
            self.cookieImageView.animationImages = cookieAnimationImgArr
            self.cookieImageView.animationRepeatCount = 1
            self.cookieImageView.animationDuration = 0.5
            self.cookieImageView.startAnimating()
            self.backgroundView.layoutIfNeeded()
            
            let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                
                UIView.animate(withDuration: 0.9, animations: {
                    self.labelImgView.snp.remakeConstraints { (make) in
                        // Label's X track
                        make.centerX.equalTo(self.cookieImageView.snp.centerX).offset(self.screenWidth*0.066)
                        make.centerY.equalTo(self.cookieImageView.snp.centerY).offset(0)
                        make.width.equalTo(self.cookieImageView.snp.width).offset(-self.screenWidth*0.07)
                        make.height.equalTo(self.cookieImageView.snp.height).offset(self.screenWidth*0.2415)
                    }
                    self.cookieImageView.superview?.layoutIfNeeded()
                    }, completion: { (true) in
                        self.shareButton.isHidden = false
                        self.initPlayer("shakedVoice")
                        self.player?.play()
                        
                        if (self.view.subviews.last?.isKind(of: RateUsFullView.self))! {
                            let vRateUs = self.view.subviews.last!
                            self.view.bringSubview(toFront: vRateUs)
                        }
                        self.buttonTitleLabel.text = self.buttonClickedString
                        self.shareButton.isHidden = false
                        self.piecesImgView.isHidden = false
                        
                        var pieceAnimationImgArr : [UIImage] = []
                        for count in 1...4{
                            let imgName : String = "paperPiece\(count)"
                            let keyImage = UIImage(named: imgName)!
                            pieceAnimationImgArr.append(keyImage)
                        }
                        //show ad
                        self.piecesImgView.animationImages = pieceAnimationImgArr
                        self.piecesImgView.animationRepeatCount = 1
                        self.piecesImgView.animationDuration = 0.2
                        self.piecesImgView.startAnimating()
                        
                        self.crackCount += 1
                        if self.crackCount == 2 && UserDefaults.standard.bool(forKey: "hasRate") == false && UserDefaults.standard.integer(forKey: "rateUsShowCount")<=RateUsFullView.getRateUsLimitShowCount() {
                            let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                                if !(self.view.subviews.last?.isKind(of: RateUsFullView.self))! {
                                    let rateUsView = RateUsFullView.init(frame: UIScreen.main.bounds)
                                    rateUsView.rootvc = self
                                    self.view.addSubview(rateUsView)
                                }
                                var showCount = UserDefaults.standard.integer(forKey: "rateUsShowCount")
                                showCount += 1
                                UserDefaults.standard.set(showCount, forKey: "rateUsShowCount")
                                if self.crackCount == 2 {
                                    self.crackCount = 0
                                }
                            })
                        }
                })
                UIApplication.shared.applicationSupportsShakeToEdit = true
                
            }
        }) 
        firstClick=false
    }
    
    lazy var cookieMessageList : [String] = {
        return ["It's your own business to create the peacefulness.","A friend indeed will not ask only for your money.","Your principles spell a big SUCCESS.","Change does hurt, but leads to a better destination.","Have the good luck be with you forever.","A chance is a new path to success.","Learn from your mistakes and you'll become stronger","Never let go something is good in your life!","Your shirt will make you lucky today.","You cannot make life love you until you love the life.","One good turn deserves another.","The person you desire feels exactly the same about you.","Accepting adversity is a source to gain strength.","Your dream is going to come true.","You will never fail if you never give up.","Just believe in yourself and you will succeed.","Nothing can ever separate us from Christ’s love.","You must try before deeply regret from not trying.","Your happiness is at your own hands.","The greatest risk in your life is not taking one.","Love can last for long if you want it to.","Adversity is the essence to success.","You will get rid of those serious trouble.","Try something new and you'll find how great you are!","Wealth awaits you...","let’s not merely say, let's show the truth by actions.","Someone special is coming for you.","Give, and you will receive.","Isn’t life more than food and more than clothing?","You are the best no matter what others say.","Jealousy closes all your doors!","Never fear the fear, go defeat it!","The man on the top of the mountain never fall.","All good turns you did to people, you did for yourself.","Fortune always comes to the brave men.","Sometimes you just need to stay low key.","Keep yourself going no matter how hard it will be.","Live righteously, and you will be given everything you need.","Stop expecting. Start acting.","Be happy. Your life is never belong to others.","Delight yourself in helping other people.","Everything exists for a reason.","You are the dealer of your own table.","Hate will never be defeated by hate.","If you feel happy, you are on the way to success.","Enrich others and you will become rich as well.","Your life is as sweet as a candy.","Be graceful is the essence of happiness.","Stay honest to those who would do the same for you."]
    }()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
