
//  DatePartToolProtocol.swift
//  Horoscope
//
//  Created by Wang on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.


import UIKit

protocol DatePartToolProtocol {

    /*
     Get paramDateString
     for example  20161123
     */
    func getDateString(_ paramDate:Date,dateFormater:String)->String

    /*
     get formated DateString
     For example Nov 20,2016
     */
    func getFormatedDateString(_ dateString:String) -> String

    /*
     calculate Time Interval
     Useable Place : update time
     */
    func calculateTimeInterval(_ sinterval:Double) -> String
 }

class DateTransactManager:DatePartToolProtocol{

    fileprivate var dateFormatter = DateFormatter()

    func getDateString(_ paramDate:Date,dateFormater:String) ->String{
        dateFormatter.dateFormat = dateFormater
        let currentDateString = dateFormatter.string(from: paramDate) as String
        return currentDateString
    }

    fileprivate let monthsArr = [NSLocalizedString("Jan", comment: ""),
                             NSLocalizedString("Feb", comment: ""),
                             NSLocalizedString("Mar", comment: ""),
                             NSLocalizedString("Apr", comment: ""),
                             NSLocalizedString("May", comment: ""),
                             NSLocalizedString("Jun", comment: ""),
                             NSLocalizedString("Jul", comment: ""),
                             NSLocalizedString("Aug", comment: ""),
                             NSLocalizedString("Sep", comment: ""),
                             NSLocalizedString("Oct", comment: ""),
                             NSLocalizedString("Nov", comment: ""),
                             NSLocalizedString("Dec", comment: "")]

    fileprivate let weekDayArr=[NSLocalizedString("Sun.",comment:""),
                            NSLocalizedString("Mon.",comment:""),
                            NSLocalizedString("Tues.",comment:""),
                            NSLocalizedString("Wed.",comment:""),
                            NSLocalizedString("Thur.",comment:""),
                            NSLocalizedString("Fri.",comment:""),
                            NSLocalizedString("Sat.",comment:""),
                            NSLocalizedString("Sun.",comment:"")]

    func getFormatedDateString(_ dateString:String) ->String {
        var timeStr = ""
        let index   = dateString.characters.index(dateString .endIndex, offsetBy: -4)
        let year    = dateString .substring(to: index)
        let index2 = dateString.characters.index(dateString .startIndex, offsetBy: 4)
        let mothdayStr = dateString.substring(from: index2)
        let index3 = mothdayStr.characters.index(mothdayStr.endIndex, offsetBy: -2)
        let devotionDateStr = mothdayStr.substring(to: index3)
        let index4 = mothdayStr.characters.index(mothdayStr.startIndex, offsetBy: 2)
        let monthStr = monthsArr[Int(devotionDateStr)! - 1]
        let dayStr = (mothdayStr.substring(from: index4))
        timeStr = "\(monthStr) \(dayStr) , \(year)"
        return timeStr
    }

    func calculateTimeInterval(_ sinterval:Double) -> String {
        var baseInterVal:Double = 0.0
        if  sinterval > 9999999999 {
            baseInterVal = sinterval / 1000
        }else{
            baseInterVal = sinterval
        }
        var timeString:String    = ""
        let createDate:Date    = Date(timeIntervalSince1970:baseInterVal)
        let late:TimeInterval  = createDate.timeIntervalSince1970
        let nowDa:Date         = Date()
        let now:TimeInterval  = nowDa.timeIntervalSince1970
        let cha:Double           =  now - late
        var distanceT :Double    = 0.0
        //1分钟内
        if (cha/60.0 < 1.0) {
            let VodStr = NSLocalizedString("Just now", comment: "Just now")
            timeString = VodStr
            return timeString
        }
        // 1min ~ 59 min
        if (cha/3600.0 < 1.0 ) {
            let VodStr1 = NSLocalizedString("mins ago", comment: "")
            distanceT = cha/60.0
            timeString = String(format:"%.f",distanceT)
            let s: Array = timeString.components(separatedBy: ".")
            timeString = "\(s[0]) \(VodStr1)"
            return timeString
        }
        // 1h ~ 24h
        if  (cha/3600.0>1.0 && (cha/86400.0<1.0)) {
            let hours = NSLocalizedString("hours ago", comment: "")
            distanceT = cha/3600.0
            timeString = String(format:"%.f",distanceT)
            let s: Array = timeString.components(separatedBy: ".")
            timeString = "\(s[0]) \(hours)"
            return timeString
        }
        //24h ~ 1 week
        if(cha/86400.0>1.0 && (cha/604800.0<1.0)) {
            distanceT = cha/86400.0
            timeString = getWeekdayFromDateInterval(baseInterVal)
            //"Sun at 23:09"
            return timeString
        }
        // After one week
        if  (cha/604800.0 > 1.0)  {
            // "May 4 at 23:09"
            dateFormatter.dateFormat = "yyyy/MM/dd/HH/mm"
            let p = dateFormatter.string(from: createDate)
            timeString=self.getStrAfter1Week(p)
            return timeString
        }
        return timeString
    }

    func getWeekdayFromDateInterval(_ interval:Double) ->String {
        let createDate=Date(timeIntervalSince1970:interval)
        dateFormatter.dateFormat="yyyy/MM/dd/HH/mm"
        let dateStr = dateFormatter.string(from: createDate)
        let days=interval/86400
        let index=Int((days-3).truncatingRemainder(dividingBy: 7))
        let weekdayStr = weekDayArr[index]
        let arr = dateStr.components(separatedBy: "/")
        let hour  = arr[3]
        let min   = arr[4]
        var daaaa:String=""
        daaaa="\(weekdayStr) at \(hour):\(min) "
        return daaaa
    }

    func getStrAfter1Week(_ dateStr:String)->String{
        let arr = dateStr.components(separatedBy: "/")
        let month = Int(arr[1])! - 1
        _   = arr[3]
        _   = arr[4]
        let year  = arr[0]
        let dayStr=arr[2]
        let monthStr=monthsArr[month]
        var daaaa:String=""
        if  year=="2016"{
            daaaa="\(dayStr) \(monthStr)"
        } else{
            daaaa="\(dayStr) \(monthStr) \(year)"
        }
        return  daaaa
    }
 }

