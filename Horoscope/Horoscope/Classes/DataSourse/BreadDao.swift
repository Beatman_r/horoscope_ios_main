//
//  BreadDao.swift
//  Horoscope
//
//  Created by Wang on 16/12/12.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit


protocol BreadInfoProtocol {
    /**
     get bread detail
     */
    func getAllTypeBreadList(_ success:@escaping (_ topList:[BreadModel],_ newList:[BreadModel])->(),failure:@escaping (_ error:NSError)->())
    func getBreadDataWithType(_ page:Int,size:Int,requestType:BreadRequestWay,success:@escaping (_ dataList:[BreadModel])->(),failure:@escaping (_ error:NSError)->())
    func viewBread(_ breadId:String,success:@escaping ()->(),failure:@escaping (_ error:NSError)->())
    func getBread(_ id:String?,success:@escaping (_ model:BreadModel?)->(),failure:@escaping (_ error:NSError)->())
    func getRecommandBreadList(_ id:String?,success:@escaping (_ list:[BreadModel])->(),failure:@escaping (_ error:NSError)->())
}

enum BreadRequestWay:String{
    case top = "top"
    case new = "new"
}

class BreadDao: BaseDao,BreadInfoProtocol {
    func getAllTypeBreadList(_ success:@escaping (_ topList:[BreadModel],_ newList:[BreadModel])->(),failure:@escaping (_ error:NSError)->()){
        self.GET(BaseUrl.allTypeBreadListUrl, parameters: nil, success: { (jsonData) in
            print(jsonData as Any)
            
            var topArr = [BreadModel]()
            if let json = jsonData?.array![0] {
                for item in json["breadList"].array ?? Array() {
                    if let dic = item.dictionaryObject {
                        let model=BreadModel(dic:dic as [String : AnyObject])
                        model.sectionId=json["sectionId"].string
                        topArr.append(model)
                    }
                }
                
            }
            var newArr = [BreadModel]()
            if let json = jsonData?.array![1] {
                for item in json["breadList"].array ?? Array() {
                    if let dic = item.dictionaryObject {
                        let model=BreadModel(dic:dic as [String : AnyObject])
                        model.sectionId=json["sectionId"].string
                        newArr.append(model)
                    }
                }
            }
            success(topArr,newArr)
            
        }) { (error) in
            failure(error)
            print(error)
        }
    }
    
    func getBreadDataWithType(_ page:Int,size:Int,requestType:BreadRequestWay,success:@escaping (_ dataList:[BreadModel])->(),failure:@escaping (_ error:NSError)->()) {
        self.GET(BaseUrl.allBreadListUrl, parameters: ["page":page as AnyObject,"size":size as AnyObject,"sectionId":requestType.rawValue as AnyObject], success: { (jsonData) in
            print(jsonData as Any)
            var dataArray = [BreadModel]()
            if let json = jsonData {
                for item in json["breadList"].array ?? Array() {
                    if let dic = item.dictionaryObject {
                        let model=BreadModel(dic:dic as [String : AnyObject])
                        dataArray.append(model)
                    }
                }
            }
            success(dataArray)
        }) { (error) in
            failure(error)
        }
    }
    
    func viewBread(_ breadId:String,success:@escaping ()->(),failure:@escaping (_ error:NSError)->()) {
        self.POST(BaseUrl.viewBreadUrl, parameters: ["breadId":breadId as AnyObject], success: { (jsonData) in
            success()
        }) { (error) in
            failure(error)
        }
    }
    
    func getBread(_ id:String?,success:@escaping (_ model:BreadModel?)->(),failure:@escaping (_ error:NSError)->()){
        self.GET(BaseUrl.breadDetail, parameters: ["breadId":id as AnyObject? ?? "" as AnyObject], success: { (jsonData) in
            var model: BreadModel?
            if let dic = jsonData?.dictionaryObject {
                model = BreadModel.init(dic: dic as [String : AnyObject])
            }
            success(model)
            }, failure: { (error) in
            failure(error)
        })
    }
    
    func getRecommandBreadList(_ id:String?,success:@escaping (_ list:[BreadModel])->(),failure:@escaping (_ error:NSError)->()){
        self.GET(BaseUrl.recommandListUrl, parameters: ["breadId":id as AnyObject? ?? "" as AnyObject], success: { (jsonData) in
            var dataArray = [BreadModel]()
            if let json = jsonData {
                for item in json["breadList"].array ?? Array() {
                    if let dic = item.dictionaryObject {
                        let model=BreadModel(dic:dic as [String : AnyObject])
                        dataArray.append(model)
                    }
                }
            }
            success(dataArray)
            }) { (error) in
            failure(error)
        }
    }
    
    
}
