//
//  BaseUrl.swift
//  Horoscope
//
//  Created by Beatman on 16/11/19.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class BaseUrl: NSObject {
    
    class func getHost() -> String {
//        #if DEBUG
//            return "http://horoscopeapitest.idailybread.com"
//        #else
            return "http://dailyhoroscopeapi.idailybread.com"
//        #endif
    }
    
    
    class func getFeedbackHost() -> String{
        #if DEBUG
            return "http://apitest.idailybread.com/"
        #else
            return "http://api.idailybread.com/"
        #endif
    }
    
    class func getDeviceLogUrl() -> String {
        let urlStr = "login/device"
        return urlStr
    }
    
    class func getFBLogUrl() -> String {
        let urlStr = "login/facebook"
        return urlStr
    }
    
    static let sectionListUrl = "/horoscope/bread/sectionList"
    static let allBreadListUrl = "/horoscope/bread/list"
    static let allTypeBreadListUrl = "/horoscope/bread/sectionList"
    static let viewBreadUrl  = "/horoscope/bread/view"
    static let campainUrl = "/horoscope/campaignItems"
    static let breadDetail="/horoscope/bread"
    static let recommandListUrl="/horoscope/bread/recommend"
    static let userLogin = "/user/login"
    static let userInfo = "/user/info"
    static let unreadNotificationCount="/user/unreadNotificationCount"
    
    //Comments
    static let breadCommentUrl = "bibleverse/bread/comment_v2"
    
    //TimeLine
    static let HoroscopeTimeline = "/horoscope/timeline"
    static let HoroscopeTimelineV2 = "/horoscope/timeline2"
    static let HoroscopeTimeLineV3 = "/horoscope/timeline3"
    static let HoroscopeTimeLineV4 = "/horoscope/timeline4"
}


