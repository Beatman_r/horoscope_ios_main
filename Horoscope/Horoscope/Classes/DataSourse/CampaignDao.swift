//
//  CampaignDao.swift
//  Horoscope
//
//  Created by Wang on 16/12/14.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

protocol CampaignProtocol {
    func getCampainList(_ position:String,success:@escaping (_ list:[CampaignModel])->(),failure:(_ error:NSError)->())
}

class CampaignDao: BaseDao,CampaignProtocol {
    func getCampainList(_ position:String,success:@escaping (_ list:[CampaignModel])->(),failure:(_ error:NSError)->()){
        self.GET(BaseUrl.campainUrl, parameters: ["positionId":position as AnyObject], success: { (jsonData) in
            print(jsonData as Any)
            var dataArr = [CampaignModel]()
            if let json = jsonData{
                for item in json.array ?? Array() {
                    if let dic = item.dictionaryObject {
                        dataArr.append(CampaignModel(dic:dic as [String : AnyObject]))
                    }
                }
            }
            success(dataArr)
        }) { (error) in
            print(error)
        }
    }
}
