//
//  TimelineDao.swift
//  Horoscope
//
//  Created by Wang on 17/1/10.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol TimelineProtocol {
    func loadTimelineDataV2(_ offSet:Int,date:Date,horoscopeKey:String,success:@escaping (_ postList:[ReadPostModel],_ campainList:[BaseCardListModel],_ adList:[basePostModel],_ count:Int) ->(),failure:@escaping (_ error:NSError) -> ())
}



class TimelineDao: BaseDao,TimelineProtocol {
    
    func loadTimelineDataV2(_ offSet:Int,date:Date,horoscopeKey:String,success:@escaping (_ postList:[ReadPostModel],_ campainList:[BaseCardListModel],_ adList:[basePostModel],_ count:Int) ->(),failure:@escaping (_ error:NSError) -> ()) {
        let dateString = HSHelpCenter.sharedInstance.dateTool.getDateString(date, dateFormater: "yyyyMMdd")
        var horo:String = horoscopeKey
        if  horoscopeKey.isEmpty == true{
            horo = "zodiac"
        }
        self.GET(BaseUrl.HoroscopeTimeLineV4, parameters: ["offset":offSet as AnyObject,"dateString":dateString as AnyObject,"horoscopeName":horo as AnyObject], success: { (jsonData) in
            print(jsonData as Any)
            var totalCount:Int = 0
            var postList=[ReadPostModel]()
            for item in jsonData!["postList"].array ?? Array(){
                
                totalCount += 1
                let model = ReadPostModel.init(jsonData: item)
                if model.video?.thumbnail?.isEmpty==false{
                    model.type = .video
                }else{
                    model.type = .normal
                }
                postList.append(model)
            }
            let horoscopeToday:LuckyModel? = LuckyModel()
            var cardList = [BaseCardListModel]()
            let forecastModel = ForecastPart()
            
            var adList = [basePostModel]()
            for adCardItem in jsonData!["adList"].array ?? Array() {
                if let dic = adCardItem.dictionaryObject {
                    totalCount += 1
                       let model = basePostModel()
                        let categoryNum = (dic["insertPosition"] as AnyObject).integerValue
                        model.position=Int(categoryNum!)
                        model.type = .ad
                        adList.append(model)
               }
            }
            
            for cardItem in jsonData!["cardList"].array ?? Array() {
                totalCount += 1
                if let dic = cardItem.dictionaryObject {
                    let categoryNum = (dic["category"] as AnyObject).integerValue
                    if categoryNum == 20 {
                        if let data = dic["data"]{
                            horoscopeToday?.create(data as! [String : AnyObject])
                            let position = (dic["pos"] as AnyObject).integerValue
                            horoscopeToday?.position = position
                            horoscopeToday?.category = .todayHoroscope
                            horoscopeToday?.zodiacName=horoscopeKey
                            
                            horoscopeToday?.commentList = self.getTopicWithCommentList(cardItem["data"])
                            cardList.append(horoscopeToday!)
                        }
                    }
                    if categoryNum == 30 {
                        if  let dataDic = dic["data"] as? [String:AnyObject]{
                            let tomorrowModel = ForecastModel()
                            tomorrowModel.period = NSLocalizedString("tomorrow", comment: "")
                            if let tomorrowDic = dataDic["tomorrow"] as? [String:AnyObject] {
                                tomorrowModel.parse(tomorrowDic)
                                
                                for eachComment in cardItem["data"]["tomorrow"]["commentList"].array ?? []{
                                    let eachCommentModel = MainComment()
                                    eachCommentModel.parse(eachComment)
                                    tomorrowModel.commentList?.append(eachCommentModel)
                                }
                               
                                let firstHoroname = ZodiacRecordManager.getSelectedZodiac().first?.lowercased()
                                if firstHoroname == horo{
                                    EveningNotification.saveTomorrowNotiContent(tomorrowModel.content ?? "")
                                }
                            }
                            
                            let weeklyModel = ForecastModel()
                            weeklyModel.period = NSLocalizedString("weekly", comment: "")
                            if let weeklyDic = dataDic["weekly"] as? [String:AnyObject] {
                                weeklyModel.parse(weeklyDic)
                                weeklyModel.content = weeklyDic["content"] as? String
                            }
                            
                            let monthlyModel = ForecastModel()
                            monthlyModel.period = NSLocalizedString("monthly", comment: "monthly")
                            if let monthlyDic = dataDic["monthly"] as? [String:AnyObject] {
                                monthlyModel.parse(monthlyDic)
                                monthlyModel.content = monthlyDic["content"] as? String
                            }
                            
                            let yearlyModel = ForecastModel()
                            yearlyModel.period = NSLocalizedString("yearly", comment: "")
                            if let yearlyDic = dataDic["yearly"] as? [String:AnyObject] {
                                yearlyModel.parse(yearlyDic)
                                yearlyModel.content = yearlyDic["content"] as? String
                            }
                            forecastModel.list? = [tomorrowModel,weeklyModel,monthlyModel,yearlyModel]
                            let position = (dic["pos"] as AnyObject).integerValue
                            forecastModel.position = position
                            forecastModel.category = .forecast
                            cardList.append(forecastModel)
                        }
                    }
                    if categoryNum == 40{
                        if let campaignData = dic["data"] as? [String : AnyObject] {
                            let campaign = CardCampaignModel.init(dic: campaignData)
                            let realCate = campaignData["category"]?.integerValue
                            if realCate == 20{
                                campaign.cate = .cardCookie
                            }else if realCate == 30{
                                campaign.cate = .cardTarot
                            } else if realCate == 40 {
                                campaign.cate = .cardNotification
                            }
                            campaign.category = .campaign
                            if campaign.cate == .cardCookie  || campaign.cate == .cardTarot || campaign.cate == .cardNotification{
                                cardList.append(campaign)
                            }
                        }
                    }
                    if categoryNum == 50{
                        let matchModel = BaseCardListModel()
                        matchModel.category = .match
                        if cardList.count >= 2 {
                            cardList.insert(matchModel, at: 2)
                        }
                        else if cardList.count >= 1 {
                            cardList.insert(matchModel, at: 1)
                        }
                        else {
                            cardList.append(matchModel)
                        }
                    }
                    if categoryNum == 80{
                        if let campaignData = dic["data"] as? [String : AnyObject]{
                            let model = AdModel()
                            let placement = campaignData["placement"] as? String
                            if let place = placement{
                                if place == "dailyCard1"{
                                    model.category = .dailyCard1
                                }
                                if placement == "dailyCard2"{
                                    model.category = .dailyCard2
                                }
                                cardList.append(model)
                            }
                        }
                    }
                    
                    if categoryNum == 90 {
                        if let campaignData = dic["data"] as? [String : AnyObject]{
                            let placement = campaignData["figure"] as? String
                            let RateUsModel = BaseCardListModel()
                            RateUsModel.category = .rateUs
                            RateUsModel.figureImg = placement
                            cardList.append(RateUsModel)
                        }
                    }
                    
                    if categoryNum == 100{
                        let RateUsModel = BaseCardListModel()
                        RateUsModel.category = .emptyHoroscope
                        cardList.append(RateUsModel)
                    }
                    if categoryNum == 120{
                        if let campaignData = dic["data"] as? [String : AnyObject] {
                            let campaign = SurveyCellModel.init(dic: campaignData)
                            campaign.category = .survey
                            cardList.append(campaign)
                        }
                    }
                    if categoryNum == 180 {
//                        if let campaignData = dic["data"] as? [String : AnyObject] {
//                            let campaign = DIGCampaignModel.init(dic: campaignData)
//                            let realCate = campaignData["category"]?.integerValue
//                            if realCate == 150{
//                                campaign.cate = .love
//                            }else if realCate == 160{
//                                campaign.cate = .tarot
//                            } else if realCate == 170 {
//                                campaign.cate = .number
//                            }
//                            campaign.category = .dig
//                            if campaign.cate == .love  || campaign.cate == .tarot || campaign.cate == .number{
//                                cardList.append(campaign)
//                            }
//                        }
//
//                        let digTimelineCell = BaseCardListModel()
//                        digTimelineCell.category = .dig
//                        cardList.append(digTimelineCell)
                    }
                }
            }
            success(postList,cardList,adList,totalCount)
        }) { (error) in
            print("V3DataError\(error)")
            failure(error)
        }
    }
    
    func getTopicWithCommentList(_ commentjson:JSON?)->[MainComment]{
        guard let commentListArrayJson = commentjson!["commentList"].array else {
            return []
        }
        
        var commentList:[MainComment] = [MainComment]()
        for json  in commentListArrayJson {
            let maincomment = MainComment()
            maincomment.parse(json)
            commentList.append(maincomment)
        }
        commentList.sort(by: { (main1, main2) -> Bool in
            return  main1.createTime > main2.createTime
        })
        return  commentList
    }
    
}
