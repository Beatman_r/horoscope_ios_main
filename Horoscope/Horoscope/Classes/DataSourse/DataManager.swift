//
//  DataManager.swift
//  Horoscope
//
//  Created by Wang on 16/12/12.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    static let sharedInstance = DataManager()
    let breadInfoDao:BreadInfoProtocol = BreadDao()
    let campaingInfo:CampaignProtocol=CampaignDao()
    let horoInfo : HoroProtocol=HoroDao()
    let homeInfo:TimelineProtocol=TimelineDao()
    let adConfigDao:AdConfigProtocol = AdConfigDao()
}
