//
//  Characteristcs.swift
//  Horoscope
//
//  Created by Beatman on 16/11/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class Characteristics: NSObject {
    
    class func getCharacteristics(_ subScript : Int) -> CharacteristicsModel{
        let path = Bundle.main.path(forResource: ZodiacModel.getZodiacName(subScript), ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""))
        let json = JSON(data: jsonData!)
        let resultModel = CharacteristicsModel(jsonData: json)
        return resultModel
    }
    
    class func getCharacteristicsWithName(_ name : String) -> CharacteristicsModel{
        let path = Bundle.main.path(forResource: name, ofType: "json")
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""))
        let json = JSON(data: jsonData!)
        let resultModel = CharacteristicsModel(jsonData: json)
        return resultModel
    }
    
}
