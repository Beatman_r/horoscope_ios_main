//
//  MatchData.swift
//  Horoscope
//
//  Created by Beatman on 16/11/22.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class MatchData: NSObject {
    
    class func getMatchInfomation(_ zodiacNameLeft:String,zodiacNameRight:String) ->String {
        let path = Bundle.main.path(forResource: zodiacNameRight, ofType: "json", inDirectory: "horoscopeData/match/"+zodiacNameLeft)
        let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""))
        let json = JSON(data: jsonData!)
        return json["content"].stringValue
    }
    
}
