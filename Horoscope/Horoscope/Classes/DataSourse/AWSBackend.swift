//
//  AWSBackend.swift
//  bibleverse
//
//  Created by qi on 8/8/16.
//  Copyright © 2016 qi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
            debugPrint(self)
        #endif
        return self
    }
}

protocol AWSBackend {
    func GET(_ url: String,
             parameters: [String: AnyObject]?,
             success: @escaping (_ jsonData: JSON?) -> Void,
             failure: @escaping (_ error: NSError) -> Void)
}

extension AWSBackend {
    
    private var host: String {
        get {
            return BaseUrl.getHost()
        }
    }
    
    private var feedbackHost : String {
        get {
            return BaseUrl.getFeedbackHost()
        }
    }
    
    private var baseParameters: [String : AnyObject] {
        get {
            #if DEBUG
                return ["app":"com.daily.horoscope.free.test" as AnyObject,"version": HSHelpCenter.sharedInstance.appAndDeviceTool.appVersionCode() as AnyObject, "apiVersion": 1 as AnyObject, "versionNum": HSHelpCenter.sharedInstance.appAndDeviceTool.appBuildNum() as AnyObject]
            #else
                return ["app":"com.daily.horoscope.free" as AnyObject,"version": HSHelpCenter.sharedInstance.appAndDeviceTool.appVersionCode() as AnyObject, "apiVersion": 1 as AnyObject, "versionNum":HSHelpCenter.sharedInstance.appAndDeviceTool.appBuildNum() as AnyObject]
            #endif
        }
    }
    
    func GETWithoutHost(_ url: String, parameters: [String: AnyObject]?, success: @escaping (_ jsonData: JSON?) -> Void,
                        failure: @escaping (_ error: NSError) -> Void) {
        
        let manager = SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        var paras = self.baseParameters
        for (k, v) in (parameters ?? [String: AnyObject]()) {
            paras[k] = v
        }
        _ = manager.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: ["s":"s"])
            
            //        manager.request(.GET, url, parameters: parameters)
            .debugLog()
            .validate(statusCode: [200])
            .response { data in
                if let error = data.error {
                    failure(error as NSError)
                    return
                }
                guard let jsonData = data.data else {
                    failure(NSError(domain: "AWSBackend GET", code: 1, userInfo: nil))
                    return
                }
                let json = JSON(data: jsonData)
                if let statusCode = json["status"]["code"].int {
                    if statusCode == 0 {
                        success(json["data"])
                    } else {
                        if let message = json["status"]["message"].string{
                            failure(NSError(domain: message , code: 1, userInfo: nil))
                            return
                        }
                        failure(NSError(domain: "AWSBackend GET", code: 1, userInfo: nil))
                        return
                    }
                }
        }
    }
    
    func GET(_ url: String, parameters: [String: AnyObject]?, success: @escaping (_ jsonData: JSON?) -> Void,
             failure: @escaping (_ error: NSError) -> Void) {
        let manager = SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        var paras = self.baseParameters
        for (k, v) in (parameters ?? [String: AnyObject]()) {
            paras[k] = v
        }
        if let param = parameters{
            let ccc = HTTPCookieStorage.shared.cookies(for: NSURL(string: self.host+url)! as URL)
            var cookie : String = ""
            if let firstCookie = ccc?.first {
                if let dic = firstCookie.properties {
                    if let cook = HTTPCookie.init(properties: dic) {
                        let correctCookie = "Session=\(cook.value );expires=\(cook.expiresDate ?? Date());\(cook.path)"
                        cookie = correctCookie
                    }
                }
            }
            let user = AccountManager.sharedInstance.getUserInfo()
            let localZone = NSTimeZone.local.identifier
            let country = NSLocale.current.identifier
            let today = HSHelpCenter.sharedInstance.dateTool.getDateString(Date(),dateFormater: "yyyyMMdd")
            print(localZone)
            #if DEBUG
                let hsHeader = ["uid":"\(user?.uid ?? "")",
                    "version-num":HSHelpCenter.sharedInstance.appAndDeviceTool.appBuildNum(),
                    "app":"com.daily.horoscope.free.test",
                    "version":HSHelpCenter.sharedInstance.appAndDeviceTool.appVersionCode(),
                    "cookie":"\(cookie)",
                    "platform":"ios",
                    "timezone":localZone,
                    "country":country,
                    "today":today]
            #else
                let hsHeader = ["uid":"\(user?.uid ?? "")",
                    "version-num":HSHelpCenter.sharedInstance.appAndDeviceTool.appBuildNum(),
                    "app":"com.daily.horoscope.free",
                    "version":HSHelpCenter.sharedInstance.appAndDeviceTool.appVersionCode(),
                    "cookie":"\(cookie)",
                    "platform":"ios",
                    "timezone":localZone,
                    "country":country,
                    "today":today]
            #endif
            
            manager.request(self.host + url, method: .get, parameters: param, encoding: URLEncoding.default, headers: hsHeader)
                .debugLog()
                .validate(statusCode: [200])
                .response { data in
                    if let error = data.error {
                        failure(error as NSError)
                        return
                    }
                    guard let jsonData = data.data else {
                        failure( NSError(domain: "AWSBackend GET, no Data", code: 1, userInfo: nil))
                        return
                    }
                    let json = JSON(data: jsonData)
                    
                    guard let statusCode = json["status"]["code"].int else {
                        failure(NSError(domain: "AWSBackend get failure", code: 1, userInfo: nil))
                        return
                    }
                    if statusCode == 0 {
                        success(json["data"])
                        return
                    }else if let message = json["status"]["message"].string {
                        failure(NSError(domain:message , code: 1, userInfo: nil))
                        return
                    }else {
                        failure(NSError(domain: "AWSBackend get no statusMessage", code: 1, userInfo: nil))
                        return
                    }
            }
        }
    }
    
    func POSTWithoutHost(_ url: String, parameters: [String: AnyObject]?, success: @escaping (_ jsonData: JSON?) -> Void,
                         failure: @escaping (_ error: NSError) -> Void) {
        
        var paras = self.baseParameters
        for (k, v) in (parameters ?? [String: AnyObject]()) {
            paras[k] = v
        }
        let manager = SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        
        _ = manager.request(self.host+url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: ["s":"s"])
            .validate(statusCode: [200]).response{ (data) in
                if let error = data.error {
                    failure(error as NSError)
                    return
                }
                guard let jsonData = data.data else {
                    failure(NSError(domain: "AWSBackend GET", code: 1, userInfo: nil))
                    return
                }
                let json = JSON(data: jsonData)
                if let statusCode = json["status"]["code"].int {
                    if statusCode == 0 {
                        success(json["data"])
                    } else {
                        if let message = json["status"]["message"].string{
                            failure(NSError(domain: message , code: 1, userInfo: nil))
                            return
                        }
                        failure(NSError(domain: "AWSBackend GET", code: 1, userInfo: nil))
                        return
                    }
                }
        }
    }
    
    func POST(_ url: String, parameters: [String: AnyObject]?, success: @escaping (_ jsonData: JSON?) -> Void,
              failure: @escaping (_ error: NSError) -> Void) {
        var paras = self.baseParameters
        for (k, v) in (parameters ?? [String: AnyObject]()) {
            paras[k] = v
        }
        let manager = SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        
        let ccc = HTTPCookieStorage.shared.cookies(for: URL(string: self.host+url)!)
        var cookie : String = ""
        if let firstCookie = ccc?.first {
            if let dic = firstCookie.properties {
                if let cook = HTTPCookie.init(properties: dic) {
                    let correctCookie = "Session=\(cook.value );expires=\(cook.expiresDate ?? Date());\(cook.path)"
                    cookie = correctCookie
                }
            }
        }
        print(cookie)
        let user = AccountManager.sharedInstance.getUserInfo()
        let localZone = NSTimeZone.local.identifier
        let country = NSLocale.current.identifier
        let today = HSHelpCenter.sharedInstance.dateTool.getDateString(Date(),dateFormater: "yyyyMMdd")
        #if DEBUG
            let hsHeader = ["uid":"\(user?.uid ?? "")",
                "version-num":HSHelpCenter.sharedInstance.appAndDeviceTool.appBuildNum(),
                "app":"com.daily.horoscope.free.test",
                "version":HSHelpCenter.sharedInstance.appAndDeviceTool.appVersionCode(),
                "cookie":"\(cookie)",
                "platform":"ios",
                "timezone":localZone,
                "country":country,
                "today":today]
        #else
            let hsHeader = ["uid":"\(user?.uid ?? "")",
                "version-num":HSHelpCenter.sharedInstance.appAndDeviceTool.appBuildNum(),
                "app":"com.daily.horoscope.free",
                "version":HSHelpCenter.sharedInstance.appAndDeviceTool.appVersionCode(),
                "cookie":"\(cookie)",
                "platform":"ios",
                "timezone":localZone,
                "country":country,
                "today":today]
        #endif
        _ = manager.request(self.host+url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: hsHeader)
            .validate(statusCode: [200])
            .response { data in
                print(data)
                if let error = data.error {
                    failure(error as NSError)
                    print("### post failed")
                    return
                }
                guard let jsonData = data.data else {
                    failure(NSError(domain: "AWSBackend GET", code: 1, userInfo: nil))
                    return
                }
                let json = JSON(data: jsonData)
                if let statusCode = json["status"]["code"].int {
                    if statusCode == 0 {
                        
                        success(json["data"])
                    } else {
                        if let message = json["status"]["message"].string{
                            failure(NSError(domain:url+message, code: 1, userInfo: nil))
                            return
                        }
                        failure(NSError(domain: "AWSBackend GET", code: 1, userInfo: nil))
                        return
                    }
                }
        }
    }
}
