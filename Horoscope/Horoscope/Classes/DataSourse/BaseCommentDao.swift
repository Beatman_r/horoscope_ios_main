//
//  BaseCommentDao.swift
//  Horoscope
//
//  Created by Wang on 17/1/4.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

protocol CommentProtocol {
//    func loadCommentListWithBreadId(breadId:String,success:(commentList:[BaseCommentModel])->(),failure:(error:NSError)->())
    
    func updateCommentWithBreadId(_ breadId:String,success:()->(),failure:@escaping (_ error:NSError)->())
    func likeOneComment(_ commentId:String,success:()->(),failure:@escaping (_ error:NSError)->())
}

class BaseCommentDao: BaseDao,CommentProtocol {
    
//    func loadCommentListWithBreadId(breadId:String,success:(commentList:[BaseCommentModel])->(),failure:(error:NSError)->()){
//    self.GET(BaseUrl.breadCommentUrl, parameters: ["":""], success: { (jsonData) in
//        
//        }) { (error) in
//           failure(error: error)
//        }
//     }
    
    func updateCommentWithBreadId(_ breadId:String,success:()->(),failure:@escaping (_ error:NSError)->()){
        self.GET(BaseUrl.breadCommentUrl, parameters: ["":"" as AnyObject], success: { (jsonData) in
            
        }) { (error) in
            failure(error)
        }
    }
    
    func likeOneComment(_ commentId:String,success:()->(),failure:@escaping (_ error:NSError)->()){
        self.GET(BaseUrl.breadCommentUrl, parameters: ["":"" as AnyObject], success: { (jsonData) in
            
        }) { (error) in
            failure(error)
        }
    }
    
}
