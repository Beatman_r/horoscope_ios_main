//
//  HoroDao.swift
//  Horoscope
//
//  Created by Wang on 16/12/21.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

protocol HoroProtocol {
    func getZodiacnameByDate(_ dateString:String)->[String]
}

class HoroDao: BaseDao, HoroProtocol {
    
    func getZodiacnameByDate(_ dateString:String)->[String]{
        let arr = [120,219,321,420,521,621,723,823,923,1023,1122,1222]
        let nameArr = ["aquarius","pisces","aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn"]
        let zodiacImgAvaDic = ["aries":"aries_.png",
                               "taurus":"taurus_.png",
                               "gemini":"gemini_.png",
                               "cancer":"cancer_.png",
                               "leo":"leo_.png",
                               "virgo":"virgo_.png",
                               "libra":"libra_.png",
                               "scorpio":"scorpio_.png",
                               "sagittarius":"sagittarius_.png",
                               "capricorn":"capricorn_.png",
                               "aquarius":"aquarius_.png",
                               "pisces":"pisces_.png"]

        let dateValue = Int(dateString)
        //HSHelpCenter.sharedInstance.dateTool.getDateString(date, dateFormater: "MMdd"))
        var currentIndex = 0
        for index in 0..<arr.count{
            let currentValue = arr[index]
            if index < arr.count-1{
                let nextValue = arr[index+1]
                if dateValue >= currentValue && dateValue < nextValue{
                    currentIndex = index
                    break
                }
            }
            if index==0{
                if dateValue <= arr[index]{
                    currentIndex = nameArr.count-1
                    break
                }
            }
            if index == arr.count-1{
                if dateValue >= arr[index]{
                    currentIndex = index
                    break
                }
            }
        }
        let Str = nameArr[currentIndex]
        let picString = zodiacImgAvaDic["\(Str)"] ?? ""
        return  [Str,picString ]
    }
    
    
}
