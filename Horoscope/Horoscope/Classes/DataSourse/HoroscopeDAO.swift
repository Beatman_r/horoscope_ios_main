//
//  HoroscopeDAO.swift
//  Horoscope
//
//  Created by Beatman on 16/11/21.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}
public extension UIDevice {
    var modelName : String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1":                               return "iPhone 7 (CDMA)"
        case "iPhone9,3":                               return "iPhone 7 (GSM)"
        case "iPhone9,2":                               return "iPhone 7 Plus (CDMA)"
        case "iPhone9,4":                               return "iPhone 7 Plus (GSM)"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

class HoroscopeDAO:NSObject, AWSBackend {
    static let sharedInstance = HoroscopeDAO()
    fileprivate let dateString:String = HSHelpCenter.sharedInstance.dateTool.getDateString(Date(),dateFormater: "yyyyMMdd")
    
    fileprivate let weeklyHoroscopeUrl  = "/horoscope/weekly"
    fileprivate let monthlyHoroscopeUrl = "/horoscope/monthly"
    fileprivate let yearlyHoroscopeUrl  = "/horoscope/yearly"
    fileprivate let dailyHoroscopeUrl   = "/horoscope/daily"
    fileprivate let userFeedbackUrl = "/horoscope/feedback"
    fileprivate let getTopicUrl = "/post/get"
    fileprivate let getFoldCommentList = "/comment/getParent"
    fileprivate let getUserPostList = "/user/postList"
    fileprivate let getUserCommentList = "/user/commentList"
    fileprivate let getMoreCommentList = "/comment/list"
    fileprivate let getMyNotificatinoList = "/user/notificationList"
    fileprivate let getTagListUrl = "/post/list"
    fileprivate let getMyForruneDaily = "/forecast/daily"
    fileprivate let getMyForruneWeekly = "/forecast/weekly"
    fileprivate let getMyForruneMonthly = "/forecast/monthly"
    fileprivate let getMyForruneYearly = "/forecast/yearly"
    fileprivate let emojiAndTagList = "/misc/extraPublishInfo"
    fileprivate let reportUrl = "/misc/report"
    fileprivate let likeOrDislike = "/misc/count"
    fileprivate let lovescope = "/lovescope/combinedLovescope"
    fileprivate let tarotscope = "/dailytarot/combinedDailyTarot"
    fileprivate let numberScope = "/numberscope/dailyNumberscope"
    fileprivate let match = "/match/matchInfo"
    
    func getDailyHoroscope(_ horoscopeName:String, dateOffset:Int,success:@escaping (_ model:ZodiacModel)->(),failure:@escaping (_ error:NSError)->()){
        self.GET(self.dailyHoroscopeUrl, parameters: ["currentDateString" : dateString as AnyObject,"offset" : dateOffset as AnyObject,"horoscopeName":horoscopeName as AnyObject], success: { (jsonData) in
            let resultModel = ZodiacModel(jsonData: jsonData!)
            success(resultModel)
        }) { (error) in
            failure(error)
        }
    }
    
    func getHoroscopeOfDate(_ horoscopeName:String, dateOffset:Int,date:String,success:@escaping (_ model:ZodiacModel)->(),failure:@escaping (_ error:NSError)->()){
        self.GET(self.dailyHoroscopeUrl, parameters: ["currentDateString" : date as AnyObject,"offset" : dateOffset as AnyObject,"horoscopeName":horoscopeName as AnyObject], success: { (jsonData) in
            let resultModel = ZodiacModel(jsonData: jsonData!)
            success(resultModel)
        }) { (error) in
            failure(error)
        }
    }
    
    func getWeeklyHoroscope(_ horoscopeName:String, dateOffset:Int,success:@escaping (_ model:ZodiacModel)->(),failure:@escaping (_ error:NSError)->()) {
        
        self.GET(self.weeklyHoroscopeUrl, parameters: ["currentDateString" : dateString as AnyObject,"horoscopeName" : horoscopeName as AnyObject, "offset" : dateOffset as AnyObject], success: { (jsonData) in
            let resultModel = ZodiacModel(jsonData: jsonData!)
            success(resultModel)
        }) { (error) in
            failure(error)
        }
    }
    
    func getMonthlyHoroscopeUrl(_ horoscopeName:String,success:@escaping (_ model:ZodiacModel)->(),failure:@escaping (_ error:NSError)->()) {
        
        self.GET(self.monthlyHoroscopeUrl, parameters: ["currentDateString" : dateString as AnyObject,"horoscopeName" : horoscopeName as AnyObject], success: { (jsonData) in
            let resultModel = ZodiacModel(jsonData:jsonData!)
            success(resultModel)
        }) { (error) in
            failure(error)
        }
    }
    
    func getYearlyHoroscopeUrl(_ horoscopeName:String,success:@escaping (_ model:ZodiacModel)->(),failure:@escaping (_ error:NSError)->()) {
        self.GET(self.yearlyHoroscopeUrl, parameters: ["currentDateString" : (dateString as AnyObject),"horoscopeName" : horoscopeName as AnyObject], success: { (jsonData) in
            let resultModel = ZodiacModel(jsonData: jsonData!)
            success(resultModel)
        }) { (error) in
            failure(error)
        }
    }
    
    func postUserFeedback(_ userFeedback:String,rateDate:NSString,starRate:String) {
        //平台 机型 版本号
        let platForm = "iOS"
        let modelName = UIDevice.current.modelName
        let infoDictionary = Bundle.main.infoDictionary
        let majorVersion = infoDictionary?["CFBundleShortVersionString"]
        print("平台: \(platForm),机型: \(modelName),版本号: \(majorVersion ?? "")")
        self.POSTWithoutHost(self.userFeedbackUrl, parameters: ["userFeedback":userFeedback as AnyObject,"rateDate":rateDate,"starRate":starRate as AnyObject,"platform":platForm as AnyObject,"modelName":modelName as AnyObject,"appVersion":majorVersion as AnyObject], success: { (jsonData) in
        }) { (error) in
            print(error)
        }
    }
    
    class func uploadImageDAO(_ uploadImage:UIImage,success:@escaping (_ url:String)->(),failure:@escaping (_ error:NSError)->()) {
        let data = UIImageJPEGRepresentation(uploadImage, 0.85)
        let uploadUrl = "http://horoscopeapitest.idailybread.com/post/uploadImage"
        let request = NSMutableURLRequest(url: URL(string: uploadUrl)!)
        request.httpMethod = "POST"
        
        let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
        let contentType = "multipart/form-data;boundary="+boundary
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        let body = NSMutableData()
        body.append(NSString(format: "--%@\r\n", boundary).data(using: String.Encoding.utf8.rawValue)!);
        body.append(NSString(format: "Content-Disposition:form-data;name=\"userid\"\r\n\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append("\("123")\r\n".data(using: String.Encoding.utf8)!)
        body.append(NSString(format:"--\(boundary)\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
        body.append(NSString(format:"Content-Disposition:form-data;name=\"file\";filename=\"dd.jpg\"\r\n").data(using: String.Encoding.utf8.rawValue)!)
        body.append("Content-Type:image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(data!)
        body.append(NSString(format:"\r\n--\(boundary)--\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
        
        request.httpBody = body as Data
        let que = OperationQueue()
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: que) { (response, data, error) in
            if error != nil {
                failure(error! as NSError)
                print(error as Any)
            }else {
                let jsonData = JSON(data: data!)
                let url = jsonData["data"]["url"].stringValue 
                success(url)
            }
        }
    }
    
    func checkStatusCode(_ jsonData:JSON) {
        let statusCode = jsonData["status"]["code"].int
        if statusCode == 4000 {
            AccountManager.sharedInstance.setLogStatus(.null)
            let postTool = UIApplication.shared.delegate as! AppDelegate
            postTool.rootViewController.present(HsLoginViewController(), animated: true, completion: nil)
        }
    }
    
    func getEmojiAndTagList(_ success:@escaping (_ tagList:[String],_ emojiList:[String])->(),failure:@escaping (_ error:NSError)->()) {
        self.GET(emojiAndTagList, parameters: [:], success: { (jsonData) in
            let tagList = jsonData!["tagList"].arrayObject ?? []
            var outputTagList : [String] = []
            for tagName in tagList {
                let eachTag = tagName as! String
                outputTagList.append(eachTag)
            }
            let imgList = jsonData!["imageList"].arrayObject ?? []
            let count = imgList.count
            var outputImageList : [String] = []
            for sub in 0..<count{
                let eachImgStr = imgList[sub] as! String
                outputImageList.append(eachImgStr)
            }
            success(outputTagList, outputImageList)
            }) { (error) in
                failure(error)
                print(error)
        }
    }
    
    func postTopic(_ content:String,imageList:[String],tagList:[String],anonymous:Bool,success:@escaping (_ model:ReadPostModel)->(),failure:@escaping (_ error:NSError)->()) {
        self.POST("/post/publish", parameters: ["content":content as AnyObject,"imageList": imageList as AnyObject,"tagList":tagList as AnyObject,"anonymous":anonymous as AnyObject], success: { (jsonData) in
            self.checkStatusCode(jsonData ?? "")
            let model = ReadPostModel(jsonData: jsonData ?? "")
            success(model)
        }) { (error) in
            print(error)
            failure(error)
        }
    }
    
    func getTopic(_ postId:String,success:@escaping (_ post:ReadPostModel)->(),failure:@escaping (_ failure:NSError)->()){
        self.GET(self.getTopicUrl, parameters: ["postId":postId as AnyObject], success: { (jsonData) in
            
            guard let jsonData = jsonData else{
                return
            }
            let post = ReadPostModel.init(jsonData: jsonData)
            
            guard let commentjson = jsonData["comment"].dictionary else {
                return
            }
            
            guard let commentListArrayJson = commentjson["commentList"]?.array else {
                return
            }
            
            guard let commentStructureJson = commentjson["commentStructure"]?.array else {
                return
            }
            
            guard let topCommentCount = jsonData["topCommentCount"].int else {
                return
            }
            
            var parentCommentList:[ParentComment] = [ParentComment]()
            for json in commentListArrayJson {
                let parentComment = ParentComment()
                parentComment.parse(json)
                parentCommentList.append(parentComment)
            }
            
            parentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var hotComentList:[MainComment] = [MainComment]()
            for index in 0..<topCommentCount{
                let mainComment = MainComment()
                mainComment.parse(commentStructureJson[index])
                hotComentList.append(mainComment)
            }
            
            var mainComentList:[MainComment] = [MainComment]()
            for index in topCommentCount..<commentStructureJson.count{
                let mainComment = MainComment()
                mainComment.parse(commentStructureJson[index])
                mainComentList.append(mainComment)
            }
            
            for hot in hotComentList{
                for parent in parentCommentList{
                    if hot.commentId == parent.commentId{
                        self.change(hot, parentComment: parent)
                    }else if (hot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        hot.parentCommentList.append(parent)
                    }
                }
            }
            
            for main in mainComentList{
                for parent in parentCommentList{
                    if main.commentId == parent.commentId{
                        self.change(main, parentComment: parent)
                    }else if (main.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        main.parentCommentList.append(parent)
                    }
                }
            }
            mainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            post.commentList = mainComentList
            post.hotCommentList = hotComentList
            success(post)
        }) { (error) in
            print("SeeOriginalError\(error)")
            failure(error)
        }
    }
    
    func change (_ mainComment:MainComment,parentComment:ParentComment)  {
        mainComment.likeCount = parentComment.likeCount
        mainComment.dislikeCount = parentComment.dislikeCount
        mainComment.createTime = parentComment.createTime
        mainComment.content = parentComment.content
        mainComment.imageList = parentComment.imageList
        mainComment.userInfo = parentComment.userInfo
        mainComment.pos = parentComment.pos
        mainComment.createTimeHuman = parentComment.createTimeHuman
        mainComment.postId = parentComment.postId
        mainComment.anonymous = parentComment.anonymous
        mainComment.isDisliked = parentComment.isDisliked
        mainComment.isLiked = parentComment.isLiked
        
    }
    
    func postContent(_ content:String,postId:String,imageList:[String],parent:String,anonymous:Bool,success:@escaping (_ comment:MainComment)->(),failure:@escaping (_ error:NSError)->()){
//        var jsonImageString = [String]()
//        if imageList.count == 0 {
//            jsonImageString = []
//        }else{
//            let jsonImageData = try! NSJSONSerialization.dataWithJSONObject(imageList, options: .PrettyPrinted)
//             jsonImageString = [(String(data: jsonImageData, encoding: NSUTF8StringEncoding) ?? "")]
//        }
        self.POST("/comment/publish", parameters: ["content":content as AnyObject,"postId":postId as AnyObject,"imageList":imageList as AnyObject,"parent":parent as AnyObject,"anonymous":anonymous as AnyObject], success: { (jsonData) in
            self.checkStatusCode(jsonData ?? "")
            guard let jsonData = jsonData else{
                return
            }
            
            guard let commentListArrayJson = jsonData["commentList"].array else {
                return
            }
            
            guard let commentStructureJson = jsonData["commentStructure"].array?.first else {
                return
            }
            
             let code = jsonData["status"]["code"].int
            if code  == 2001{
                NotificationCenter.default.post(name: Notification.Name(rawValue: CommentsMoreThanSeventyFloor), object: nil)
                return
            }
            
            
            var parentCommentList:[ParentComment] = [ParentComment]()
            
            for json  in commentListArrayJson {
                let parentComment = ParentComment()
                parentComment.parse(json)
                parentCommentList.append(parentComment)
            }
            
            let mainComent:MainComment = MainComment()
            mainComent.parse(commentStructureJson)
            
            for item in parentCommentList{
                if item.commentId == mainComent.commentId{
                self.change(mainComent, parentComment: item)
                }else{
                    mainComent.parentCommentList.append(item)
                }
            }
            mainComent.parentCommentList.sort(by: { (parent1 , parent2) -> Bool in
                 return parent1.createTime < parent2.createTime
            })
            success(mainComent)
        }) { (error) in
            print(error)
            failure(error)
        }
    }
    
    func postForcastContent(_ content:String,postId:String,imageList:[String],parent:String,anonymous:Bool,horoscopeName:String,forecastName:String,success:@escaping (_ comment:MainComment)->(),failure:@escaping (_ error:NSError)->()){
        self.POST("/comment/publish", parameters: ["content":content as AnyObject,"postId":postId as AnyObject,"imageList":imageList as AnyObject,"parent":parent as AnyObject,"anonymous":anonymous as AnyObject,"horoscopeName":horoscopeName as AnyObject,"forecastName":forecastName as AnyObject], success: { (jsonData) in
            self.checkStatusCode(jsonData ?? "")
            guard let jsonData = jsonData else{
                return
            }
            
            guard let commentListArrayJson = jsonData["commentList"].array else {
                return
            }
            
            guard let commentStructureJson = jsonData["commentStructure"].array?.first else {
                return
            }
            
            let code = jsonData["status"]["code"].int
            if code  == 2001{
                NotificationCenter.default.post(name: Notification.Name(rawValue: CommentsMoreThanSeventyFloor), object: nil)
                return
            }
            
            
            var parentCommentList:[ParentComment] = [ParentComment]()
            
            for json  in commentListArrayJson {
                let parentComment = ParentComment()
                parentComment.parse(json)
                parentCommentList.append(parentComment)
            }
            
            let mainComent:MainComment = MainComment()
            mainComent.parse(commentStructureJson)
            
            for item in parentCommentList{
                if item.commentId == mainComent.commentId{
                    self.change(mainComent, parentComment: item)
                }else{
                    mainComent.parentCommentList.append(item)
                }
            }
            mainComent.parentCommentList.sort(by: { (parent1 , parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            success(mainComent)
        }) { (error) in
            print(error)
            failure(error)
        }
    }
    
    
    
    func getFoldCommentList(_ commentId:String,index:Int,negative_index:Int,success:@escaping (_ foldCommentList:[ParentComment])->(),failure:@escaping (_ failure:NSError)->()){
        self.GET(self.getFoldCommentList, parameters: ["commentId":commentId as AnyObject,"index":index as AnyObject,"negative_index":negative_index as AnyObject], success: { (jsonData) in
            
            guard let jsonData = jsonData?.array else{
                return
            }
            var commentList = [ParentComment]()
            for item in jsonData{
                let parentComment = ParentComment()
                parentComment.parse(item)
                commentList.append(parentComment)
            }
            success(commentList)
        }) { (error) in
            failure(error)
        }
    }
    
    func getUserPostList(_ offset:Int,success:@escaping (_ modelList:[MyPostModel])->(),failure:@escaping (_ error:NSError)->()) {
        self.GET(self.getUserPostList, parameters: ["offset":offset as AnyObject], success: { (jsonData) in
            let totalCount = jsonData!["total"].intValue
            var modelList : [MyPostModel] = []
            for sub in 0..<totalCount {
                let model = MyPostModel(jsonData: jsonData ?? "", num: sub)
                modelList.append(model)
            }
            success(modelList)
        }) { (error) in
            print(error)
            failure(error)
        }
    }
    
    func getUserCommentList(_ offset:Int,success:@escaping (_ modelList:[MyCommentModel],_ hasImaList:[Bool])->(),failure:@escaping (_ error:NSError)->()){
        self.GET(self.getUserCommentList, parameters: ["offset":offset as AnyObject], success: { (jsonData) in
            print(jsonData as Any)
            let size = jsonData!["commentList"].arrayObject?.count ?? 0
            var modelList : [MyCommentModel] = []
            let hasImaList : [Bool] = []
            for sub in 0..<size {
                let model = MyCommentModel(jsonData: jsonData ?? "", num: sub)
//                if model.imageList?.count > 0 {
//                    hasImaList.append(true)
//                }else {
//                    hasImaList.append(false)
//                }
                modelList.append(model)
            }
            modelList.sort(by: { (model1, model2) -> Bool in
                return model1.createTime > model2.createTime
            })
            success(modelList, hasImaList)
        }) { (error) in
            print(error)
            failure(error)
        }
    }
    
    func getMoreComment(_ postId:String,size:String,offset:String,sort:String,success:@escaping (_ commentList:[MainComment])->(),failure:@escaping (_ failure:NSError)->()) {
        self.GET(self.getMoreCommentList, parameters: ["postId":postId as AnyObject,"size":size as AnyObject,"offset":offset as AnyObject,"sort":sort as AnyObject], success: { (jsonData) in
            
            guard let jsonData = jsonData else{
                return
            }

            guard let commentListArrayJson = jsonData["commentList"].array else {
                return
            }
            
            guard let commentStructureJson = jsonData["commentStructure"].array else {
                return
            }
            
            var parentCommentList:[ParentComment] = [ParentComment]()
            for json  in commentListArrayJson {
                let parentComment = ParentComment()
                parentComment.parse(json)
                parentCommentList.append(parentComment)
            }
            parentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var mainComentList:[MainComment] = [MainComment]()
            for json in commentStructureJson{
                let mainComment = MainComment()
                mainComment.parse(json)
                mainComentList.append(mainComment)
            }
            
            for main in mainComentList{
                for parent in parentCommentList{
                    if main.commentId == parent.commentId{
                        self.change(main, parentComment: parent)
                    }else if (main.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        main.parentCommentList.append(parent)
                    }
                }
            }
            mainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
                success(mainComentList)
        }) { (error) in
            failure(error)
        }
    }
    
    func getMyNotificationList(_ offset:String,success:@escaping (_ commentList:[MainComment])->(),failure:@escaping (_ failure:NSError)->()) {
        self.GET(self.getMyNotificatinoList, parameters: ["offset":offset as AnyObject], success: { (jsonData) in
            
            guard let jsonData = jsonData else{
                return
            }
            print(jsonData)
            guard let commentListArrayJson = jsonData["commentList"].array else {
                return
            }
            
            guard let commentStructureJson = jsonData["commentStructure"].array else {
                return
            }
            
            var parentCommentList:[ParentComment] = [ParentComment]()
            for json  in commentListArrayJson {
                let parentComment = ParentComment()
                parentComment.parse(json)
                parentCommentList.append(parentComment)
            }
            parentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var mainComentList:[MainComment] = [MainComment]()
            var sub = 0
            for json in commentStructureJson{
                let mainComment = MainComment()
                mainComment.parse(json)
//                mainComment.horoscopeName = parentCommentList[sub].horoscopeName
//                mainComment.forecastName = parentCommentList[sub].forecastName
                mainComentList.append(mainComment)
                sub += 1
            }
            
            for main in mainComentList{
                for parent in parentCommentList{
                    if main.commentId == parent.commentId{
                        self.change(main, parentComment: parent)
                    }else if (main.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        main.parentCommentList.append(parent)
                    }
                }
            }
            mainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            success(mainComentList)
        }) { (error) in
            failure(error)
        }
    }
    
    func getPostListWithTag(_ tag:String,offset:Int,success:@escaping (_ postList:[MyPostModel])->(),failure:(_ failure:NSError)->()) {
        self.GET(self.getTagListUrl, parameters: ["tag":tag as AnyObject,"offset":offset as AnyObject], success: { (jsonData) in
            let postList = jsonData!["postList"] 
            let size = postList.count 
            var outputPostList : [MyPostModel] = []
            for sub in 0..<size{
                let eachPost = MyPostModel(jsonData: jsonData!, num: sub)
                outputPostList.append(eachPost)
            }
            success(outputPostList)
            }) { (error) in
                print(error)
        }
    }
    
    func getOtherUserPostList(_ userId:String,offset:Int,success:@escaping (_ postList:[MyPostModel])->(),failure:(_ failure:NSError)->()) {
        self.GET(self.getTagListUrl, parameters: ["userId":userId as AnyObject,"offset":offset as AnyObject], success: { (jsonData) in
            let postList = jsonData!["postList"].array ?? []
            let size = postList.count 
            var outputPostList : [MyPostModel] = []
            for sub in 0..<size{
                let eachPost = MyPostModel(jsonData: jsonData!, num: sub)
                outputPostList.append(eachPost)
            }
            success(outputPostList)
        }) { (error) in
            print(error)
        }
    }
    
    func getMasterPostList(_ userId:String,offset:Int,success:@escaping (_ postList:[ReadPostModel])->(),failure:(_ failure:NSError)->()) {
        self.GET(self.getTagListUrl, parameters: ["userId":userId as AnyObject,"offset":offset as AnyObject], success: { (jsonData) in
            var totalCount:Int = 0
            var postList=[ReadPostModel]()
            for item in jsonData!["postList"].array ?? Array(){
                totalCount += 1
                let model = ReadPostModel.init(jsonData: item)
                if model.video?.thumbnail?.isEmpty==false{
                   model.type = .video
                }else{
                    model.type = .normal
                }
                postList.append(model)
            }
             success(postList)
         }) { (error) in
             print(error)
        }
    }
    
   func getMyTodayFortune(_ currentDateString:String,horoscopeName:String,success:@escaping (_ post:ReadPostModel)->(),failure:(_ error:NSError)->()) {
        self.getMyFortuneComments(self.getMyForruneDaily, currentDateString: currentDateString, horoscopeName: horoscopeName, offset:"0", forecastName: "today", success: { (post) in
            success(post)
            }) { (failure) in
               
        }
    }
    
    func getMyYestodayFortune(_ currentDateString:String,horoscopeName:String,success:@escaping (_ post:ReadPostModel)->(),failure:(_ error:NSError)->()) {
        self.getMyFortuneComments(self.getMyForruneDaily, currentDateString:currentDateString, horoscopeName: horoscopeName, offset: "-1", forecastName: "yesterday", success: { (post) in
            success(post)
        }) { (failure) in
            
        }
    }
    
    func getMyTomorrowFortune(_ currentDateString:String,horoscopeName:String,success:@escaping (_ post:ReadPostModel)->(),failure:(_ error:NSError)->()) {
        self.getMyFortuneComments(self.getMyForruneDaily, currentDateString:currentDateString, horoscopeName: horoscopeName, offset: "1", forecastName: "tomorrow", success: { (post) in
            success(post)
        }) { (failure) in
            
        }
    }
    
    func getMyWeeklyFortune(_ currentDateString:String,horoscopeName:String,success:@escaping (_ post:ReadPostModel)->(),failure:(_ error:NSError)->()) {
        self.getMyFortuneComments(self.getMyForruneWeekly,currentDateString: currentDateString, horoscopeName: horoscopeName, offset: "0", forecastName: "weekly", success: { (post) in
            success(post)
        }) { (failure) in
            
        }
    }
    
    func getMyMonthlyFortune(_ currentDateString:String,horoscopeName:String,success:@escaping (_ post:ReadPostModel)->(),failure:(_ error:NSError)->()) {
        self.getMyFortuneComments(self.getMyForruneMonthly,currentDateString: currentDateString, horoscopeName: horoscopeName, offset: "0", forecastName: "monthly", success: { (post) in
            success(post)
        }) { (failure) in
            
        }
    }
    
    func getMyYearlyFortune(_ currentDateString:String,horoscopeName:String,success:@escaping (_ post:ReadPostModel)->(),failure:(_ error:NSError)->()) {
        self.getMyFortuneComments(self.getMyForruneYearly,currentDateString: currentDateString, horoscopeName: horoscopeName, offset: "0", forecastName: "yearly", success: { (post) in
            success(post)
        }) { (failure) in
            
        }
    }
    
    fileprivate  func getMyFortuneComments(_ url:String,currentDateString:String,horoscopeName:String,offset:String,forecastName:String,success:@escaping (_ post:ReadPostModel)->(),failure:@escaping (_ failure:NSError)->()) {
        self.GET(url, parameters: ["currentDateString":currentDateString as AnyObject,"horoscopeName":horoscopeName as AnyObject,"offset":offset as AnyObject,"forecastName":forecastName as AnyObject], success: { (jsonData) in
            guard let jsonData = jsonData else{
                return
            }
            
            let post = ReadPostModel.init(jsonData: jsonData)
    
            guard let commentListArrayJson = jsonData["commentList"].array else {
                return
            }
            
            guard let commentStructureJson = jsonData["commentStructure"].array else {
                return
            }
            
            guard let topCommentCount = jsonData["topCommentCount"].int else {
                return
            }
            
            var parentCommentList:[ParentComment] = [ParentComment]()
            for json  in commentListArrayJson {
                let parentComment = ParentComment()
                parentComment.parse(json)
                parentCommentList.append(parentComment)
            }
            parentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })

            var hotComentList:[MainComment] = [MainComment]()
            for index in 0..<topCommentCount{
                let mainComment = MainComment()
                mainComment.parse(commentStructureJson[index])
                hotComentList.append(mainComment)
            }
            
            var mainComentList:[MainComment] = [MainComment]()
            for index in topCommentCount..<commentStructureJson.count{
                let mainComment = MainComment()
                mainComment.parse(commentStructureJson[index])
                mainComentList.append(mainComment)
            }
            
            for hot in hotComentList{
                for parent in parentCommentList{
                    if hot.commentId == parent.commentId{
                        self.change(hot, parentComment: parent)
                    }else if (hot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        hot.parentCommentList.append(parent)
                    }
                }
            }
            
            for main in mainComentList{
                for parent in parentCommentList{
                    if main.commentId == parent.commentId{
                        self.change(main, parentComment: parent)
                    }else if (main.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        main.parentCommentList.append(parent)
                    }
                }
            }
            mainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            post.commentList = mainComentList
            post.hotCommentList = hotComentList
            success(post)

        }) { (error) in
            failure(error)
            print(error)
        }
    }
    

    func reportComment(_ commentId:String,postId:String,reason:String){
    self.POST(self.reportUrl, parameters: ["commentId":commentId as AnyObject,"postId":postId as AnyObject,"reason":reason as AnyObject], success: { (jsonData) in
        self.checkStatusCode(jsonData ?? "")
        }) { (error) in
            
        }
    }

    func reportPost(_ postId:String,reason:String){
        self.POST(self.reportUrl, parameters: ["postId":postId as AnyObject,"reason":reason as AnyObject], success: { (jsonData) in
            self.checkStatusCode(jsonData ?? "")
        }) { (error) in
    
        }
    }
    
    func likeOrDislikeAction(_ entry:String,action:String,value:Bool,id:String) {
        self.POST(likeOrDislike, parameters: ["entry":entry as AnyObject,"action":action as AnyObject,"value":value as AnyObject,"id":id as AnyObject], success: { (jsonData) in
            self.checkStatusCode(jsonData ?? "")
            }) { (error) in
            print(error)
         }
    }
    
    func getUnreadNumber(_ success:@escaping (_ num:Int)->()) {
        self.GET(BaseUrl.unreadNotificationCount, parameters: ["":"" as AnyObject], success: { (jsonData) in
            
            guard let jsonData = jsonData else{
                return
            }
            let unreadNum = jsonData["unreadCount"].intValue
            success(unreadNum)
            }) { (error) in
             print("\(error)")
        }
    }
    
    //MARK: DIG
    
//    func getLovescope(horoscopeName : String,success:@escaping (_ model:LovescopeModel)->(),failure:@escaping (_ error:NSError)->()){
//        self.GET(self.lovescope, parameters: ["currentDateString":self.dateString as AnyObject,"horoscopeName":horoscopeName as AnyObject], success: { (jsonData) in
//            guard let jsonData = jsonData else {
//                return
//            }
//            let model = LovescopeModel(jsonData: jsonData)
//            success(model)
//        }, failure: { (error) in
//            print(error)
//        })
//    }
    
    
    func getLovescope(horoscopeName : String,success:@escaping (_ model:LovescopeModel)->(),failure:@escaping (_ error:NSError)->()) {
        self.GET(self.lovescope, parameters: ["currentDateString":self.dateString as AnyObject,"horoscopeName":horoscopeName as AnyObject], success: { (jsonData) in
            guard let jsonData = jsonData else {
                return
            }
            
            let model = LovescopeModel(jsonData: jsonData)
            
            guard let todayLoveCommentList = jsonData["today"]["commentList"].array else {
                return
            }
            guard let tomorrowLoveCommentList = jsonData["tomorrow"]["commentList"].array else {
                return
            }
            guard let yesterdayLoveCommentList = jsonData["yesterday"]["commentList"].array else {
                return
            }
            
            guard let todayLoveCommentStructure = jsonData["today"]["commentStructure"].array else {
                return
            }
            guard let tomorrowLoveCommentStructure = jsonData["tomorrow"]["commentStructure"].array else {
                return
            }
            guard let yesterdayLoveCommentStructure = jsonData["yesterday"]["commentStructure"].array else {
                return
            }
    
            let todayTopCommentCount = jsonData["today"]["topCommentCount"].int ?? 0
            
            let tomorrowTopCommentCount = jsonData["tomorrow"]["topCommentCount"].int ?? 0
            
            let yesterdayTopCommentCount = jsonData["yesterday"]["topCommentCount"].int ?? 0
            
            var todayParentCommentList:[ParentComment] = [ParentComment]()
            for json in todayLoveCommentList {
                let parentComment = ParentComment()
                parentComment.parse(json)
                todayParentCommentList.append(parentComment)
            }
            todayParentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var tomorrowParentCommentList:[ParentComment] = [ParentComment]()
            for json in tomorrowLoveCommentList {
                let parentComment = ParentComment()
                parentComment.parse(json)
                tomorrowParentCommentList.append(parentComment)
            }
            tomorrowParentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var yesterdayParentCommentList:[ParentComment] = [ParentComment]()
            for json in yesterdayLoveCommentList {
                let parentComment = ParentComment()
                parentComment.parse(json)
                yesterdayParentCommentList.append(parentComment)
            }
            yesterdayParentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var todayHotComentList:[MainComment] = [MainComment]()
            for index in 0..<todayTopCommentCount{
                let mainComment = MainComment()
                mainComment.parse(todayLoveCommentStructure[index])
                todayHotComentList.append(mainComment)
            }
            var tomorrowHotComentList:[MainComment] = [MainComment]()
            for index in 0..<tomorrowTopCommentCount{
                let mainComment = MainComment()
                mainComment.parse(tomorrowLoveCommentStructure[index])
                tomorrowHotComentList.append(mainComment)
            }
            var yesterdayHotComentList:[MainComment] = [MainComment]()
            for index in 0..<yesterdayTopCommentCount{
                let mainComment = MainComment()
                mainComment.parse(yesterdayLoveCommentStructure[index])
                yesterdayHotComentList.append(mainComment)
            }
            
            var todayMainComentList:[MainComment] = [MainComment]()
            for index in todayTopCommentCount..<todayLoveCommentStructure.count{
                let mainComment = MainComment()
                mainComment.parse(todayLoveCommentStructure[index])
                todayMainComentList.append(mainComment)
            }
            var tomorrowMainComentList:[MainComment] = [MainComment]()
            for index in tomorrowTopCommentCount..<tomorrowLoveCommentStructure.count{
                let mainComment = MainComment()
                mainComment.parse(tomorrowLoveCommentStructure[index])
                tomorrowMainComentList.append(mainComment)
            }
            var yesterdayMainComentList:[MainComment] = [MainComment]()
            for index in yesterdayTopCommentCount..<yesterdayLoveCommentStructure.count{
                let mainComment = MainComment()
                mainComment.parse(yesterdayLoveCommentStructure[index])
                yesterdayMainComentList.append(mainComment)
            }
            
            for todayHot in todayHotComentList{
                for parent in todayParentCommentList{
                    if todayHot.commentId == parent.commentId{
                        self.change(todayHot, parentComment: parent)
                    }else if (todayHot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        todayHot.parentCommentList.append(parent)
                    }
                }
            }
            for tomorrowHot in tomorrowHotComentList{
                for parent in tomorrowParentCommentList{
                    if tomorrowHot.commentId == parent.commentId{
                        self.change(tomorrowHot, parentComment: parent)
                    }else if (tomorrowHot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        tomorrowHot.parentCommentList.append(parent)
                    }
                }
            }
            for yesterdayHot in yesterdayHotComentList{
                for parent in yesterdayParentCommentList{
                    if yesterdayHot.commentId == parent.commentId{
                        self.change(yesterdayHot, parentComment: parent)
                    }else if (yesterdayHot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        yesterdayHot.parentCommentList.append(parent)
                    }
                }
            }
            
            for todayMain in todayMainComentList{
                for parent in todayParentCommentList{
                    if todayMain.commentId == parent.commentId{
                        self.change(todayMain, parentComment: parent)
                    }else if (todayMain.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        todayMain.parentCommentList.append(parent)
                    }
                }
            }
            todayMainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            for tomorrowMain in tomorrowMainComentList{
                for parent in tomorrowParentCommentList{
                    if tomorrowMain.commentId == parent.commentId{
                        self.change(tomorrowMain, parentComment: parent)
                    }else if (tomorrowMain.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        tomorrowMain.parentCommentList.append(parent)
                    }
                }
            }
            tomorrowMainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            for yesterdayMain in yesterdayMainComentList{
                for parent in yesterdayParentCommentList{
                    if yesterdayMain.commentId == parent.commentId{
                        self.change(yesterdayMain, parentComment: parent)
                    }else if (yesterdayMain.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        yesterdayMain.parentCommentList.append(parent)
                    }
                }
            }
            yesterdayMainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            
            model.todayLoveModel?.commentList = todayMainComentList
            model.todayLoveModel?.hotCommentList = todayHotComentList
            
            model.tomorrowLoveModel?.commentList = tomorrowMainComentList
            model.tomorrowLoveModel?.hotCommentList = tomorrowHotComentList
            
            model.yesterdayLoveModel?.commentList = yesterdayMainComentList
            model.yesterdayLoveModel?.hotCommentList = yesterdayHotComentList
            
            success(model)
        }) { (error) in
            print(self.dateString,horoscopeName)
            print(error)
        }
    }
    
    func getTarotscope(success:@escaping (_ model:TarotscopeModel)->(),failure:@escaping (_ error:NSError)->()){
        self.GET(self.tarotscope, parameters: ["currentDateString":self.dateString as AnyObject], success: { (jsonData) in
            guard let jsonData = jsonData else {
                return
            }
            print(jsonData)
            let model = TarotscopeModel(jsonData: jsonData)
            
            guard let todayLoveCommentList = jsonData["today"]["commentList"].array else {
                return
            }
            guard let tomorrowLoveCommentList = jsonData["tomorrow"]["commentList"].array else {
                return
            }
            guard let yesterdayLoveCommentList = jsonData["yesterday"]["commentList"].array else {
                return
            }
            
            guard let todayLoveCommentStructure = jsonData["today"]["commentStructure"].array else {
                return
            }
            guard let tomorrowLoveCommentStructure = jsonData["tomorrow"]["commentStructure"].array else {
                return
            }
            guard let yesterdayLoveCommentStructure = jsonData["yesterday"]["commentStructure"].array else {
                return
            }
            
            let todayTopCommentCount = jsonData["today"]["topCommentCount"].int ?? 0
            
            let tomorrowTopCommentCount = jsonData["tomorrow"]["topCommentCount"].int ?? 0
            
            let yesterdayTopCommentCount = jsonData["yesterday"]["topCommentCount"].int ?? 0
            
            var todayParentCommentList:[ParentComment] = [ParentComment]()
            for json in todayLoveCommentList {
                let parentComment = ParentComment()
                parentComment.parse(json)
                todayParentCommentList.append(parentComment)
            }
            todayParentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var tomorrowParentCommentList:[ParentComment] = [ParentComment]()
            for json in tomorrowLoveCommentList {
                let parentComment = ParentComment()
                parentComment.parse(json)
                tomorrowParentCommentList.append(parentComment)
            }
            tomorrowParentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var yesterdayParentCommentList:[ParentComment] = [ParentComment]()
            for json in yesterdayLoveCommentList {
                let parentComment = ParentComment()
                parentComment.parse(json)
                yesterdayParentCommentList.append(parentComment)
            }
            yesterdayParentCommentList.sort(by: { (parent1, parent2) -> Bool in
                return parent1.createTime < parent2.createTime
            })
            
            var todayHotComentList:[MainComment] = [MainComment]()
            for index in 0..<todayTopCommentCount{
                let mainComment = MainComment()
                mainComment.parse(todayLoveCommentStructure[index])
                todayHotComentList.append(mainComment)
            }
            var tomorrowHotComentList:[MainComment] = [MainComment]()
            for index in 0..<tomorrowTopCommentCount{
                let mainComment = MainComment()
                mainComment.parse(tomorrowLoveCommentStructure[index])
                tomorrowHotComentList.append(mainComment)
            }
            var yesterdayHotComentList:[MainComment] = [MainComment]()
            for index in 0..<yesterdayTopCommentCount{
                let mainComment = MainComment()
                mainComment.parse(yesterdayLoveCommentStructure[index])
                yesterdayHotComentList.append(mainComment)
            }
            
            var todayMainComentList:[MainComment] = [MainComment]()
            for index in todayTopCommentCount..<todayLoveCommentStructure.count{
                let mainComment = MainComment()
                mainComment.parse(todayLoveCommentStructure[index])
                todayMainComentList.append(mainComment)
            }
            var tomorrowMainComentList:[MainComment] = [MainComment]()
            for index in tomorrowTopCommentCount..<tomorrowLoveCommentStructure.count{
                let mainComment = MainComment()
                mainComment.parse(tomorrowLoveCommentStructure[index])
                tomorrowMainComentList.append(mainComment)
            }
            var yesterdayMainComentList:[MainComment] = [MainComment]()
            for index in yesterdayTopCommentCount..<yesterdayLoveCommentStructure.count{
                let mainComment = MainComment()
                mainComment.parse(yesterdayLoveCommentStructure[index])
                yesterdayMainComentList.append(mainComment)
            }
            
            for todayHot in todayHotComentList{
                for parent in todayParentCommentList{
                    if todayHot.commentId == parent.commentId{
                        self.change(todayHot, parentComment: parent)
                    }else if (todayHot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        todayHot.parentCommentList.append(parent)
                    }
                }
            }
            for tomorrowHot in tomorrowHotComentList{
                for parent in tomorrowParentCommentList{
                    if tomorrowHot.commentId == parent.commentId{
                        self.change(tomorrowHot, parentComment: parent)
                    }else if (tomorrowHot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        tomorrowHot.parentCommentList.append(parent)
                    }
                }
            }
            for yesterdayHot in yesterdayHotComentList{
                for parent in yesterdayParentCommentList{
                    if yesterdayHot.commentId == parent.commentId{
                        self.change(yesterdayHot, parentComment: parent)
                    }else if (yesterdayHot.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        yesterdayHot.parentCommentList.append(parent)
                    }
                }
            }
            
            for todayMain in todayMainComentList{
                for parent in todayParentCommentList{
                    if todayMain.commentId == parent.commentId{
                        self.change(todayMain, parentComment: parent)
                    }else if (todayMain.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        todayMain.parentCommentList.append(parent)
                    }
                }
            }
            todayMainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            for tomorrowMain in tomorrowMainComentList{
                for parent in tomorrowParentCommentList{
                    if tomorrowMain.commentId == parent.commentId{
                        self.change(tomorrowMain, parentComment: parent)
                    }else if (tomorrowMain.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        tomorrowMain.parentCommentList.append(parent)
                    }
                }
            }
            tomorrowMainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            for yesterdayMain in yesterdayMainComentList{
                for parent in yesterdayParentCommentList{
                    if yesterdayMain.commentId == parent.commentId{
                        self.change(yesterdayMain, parentComment: parent)
                    }else if (yesterdayMain.parentCommentIdList ?? []).contains(parent.commentId ?? ""){
                        yesterdayMain.parentCommentList.append(parent)
                    }
                }
            }
            yesterdayMainComentList.sort(by: { (main1, main2) -> Bool in
                return  main1.createTime > main2.createTime
            })
            
            model.todayTarotModel?.commentList = todayMainComentList
            model.todayTarotModel?.hotCommentList = todayHotComentList
            
            model.tomorrowTarotModel?.commentList = tomorrowMainComentList
            model.tomorrowTarotModel?.hotCommentList = tomorrowHotComentList
            
            model.yesterdayTarotModel?.commentList = yesterdayMainComentList
            model.yesterdayTarotModel?.hotCommentList = yesterdayHotComentList
            
            success(model)
        }) { (error) in
            print(error)
        }
    }
    
    func getNumberScope(birthday:String,success:@escaping (_ model:NumberScopeModel)->(),failure:@escaping (_ error:NSError)->()) {
        self.GET(self.numberScope, parameters: ["birthday":birthday as AnyObject], success: { (jsonData) in
            guard let jsonData = jsonData else {
                return
            }
            let model = NumberScopeModel.init(jsonData: jsonData)
            success(model)
        }) { (error) in
            print(error)
        }
    }
    
    func getMatchData(horoscopeA:String,horoscopeB:String,success:@escaping (_ content:String)->(),failure:@escaping (_ error:NSError)->()) {
        self.GET(self.match, parameters: ["horoscopeA":horoscopeA as AnyObject,"horoscopeB":horoscopeB as AnyObject], success: { (jsonData) in
            guard let jsonData = jsonData else {
                return
            }
            let content = jsonData["content"].stringValue
            success(content)
        }) { (error) in
            print(error)
        }
    }
    
}
    
