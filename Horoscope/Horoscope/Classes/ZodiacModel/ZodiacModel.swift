//
//  ZodiacModel.swift
//  Horoscope
//
//  Created by Beatman on 16/11/18.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import SwiftyJSON

class ZodiacModel: NSObject {
    var horoscopeName : String?
    var updateTime : String?
    var content : String?
    var createTime : String?
    var dateString : String?
    var startDateString : String?
    var monthString : String?
    var yearString : String?
    
    init(jsonData : JSON){
        super.init()
        self.horoscopeName = jsonData["horoscopeName"].stringValue
        self.updateTime = jsonData["updateTime"].stringValue
        self.content = jsonData["content"].stringValue
        self.createTime = jsonData["createTime"].stringValue 
        self.dateString = jsonData["dateString"].stringValue 
        self.startDateString = jsonData["startDateString"].stringValue 
        self.monthString = jsonData["monthString"].stringValue
        self.yearString = jsonData["yearString"].stringValue 
    }
    class  func getZodiacName(_ subScript:Int) -> String {
        let zodiacArr :[String] = ["aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn","aquarius","pisces"]
        return zodiacArr[subScript]
    }
    
    class func getIndexWithName(_ name:String) -> Int{
        let zodiacArr :[String] = ["aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn","aquarius","pisces"]
        var targetIndex:Int?
        for index in 0..<zodiacArr.count {
            let eachName = zodiacArr[index]
            if eachName == name.lowercased(){
                targetIndex = index
            }
        }
         return targetIndex ?? 0
    }
    
    class func getZodiacNameForDisplay(_ subScript:Int) -> String {
        let zodiacArr :[String] = ["Aries","Taurus","Gemini","Cancer","Leo","Virgo","Libra","Scorpio","Sagittarius","Capricorn","Aquarius","Pisces"]
        return zodiacArr[subScript]
    }
    
    class func getHoroscopeBackImgNameWithName(_ name:String) -> String{
        let zodiacImgNameDic = ["aries":"daily_aries.jpg","taurus":"daily_taurus.jpg","gemini":"daily_gemini.jpg","cancer":"daily_cancer.jpg","leo":"daily_leo.jpg","virgo":"daily_virgo.jpg","libra":"daily_libra.jpg","scorpio":"daily_scorpio.jpg","sagittarius":"daily_sagittarius.jpg","capricorn":"daily_capricorn.jpg","aquarius":"daily_aquarius.jpg","pisces":"daily_pisces.jpg","":""]
        return   zodiacImgNameDic[name] ?? ""
    }
    
    class func getHoroscopeSquareImgNameWithName(_ name:String) ->String{
        let zodiacSquareImgDic = ["aries":"ic_aries_match.png","taurus":"ic_taurus_match.png","gemini":"ic_gemini_match.png","cancer":"ic_cancer_match.png","leo":"ic_leo_match.png","virgo":"ic_virgo_match.png","libra":"ic_libra_match.png","scorpio":"ic_scorpio_match.png","sagittarius":"ic_sagittarius_match.png","capricorn":"ic_capricorn_match.png","aquarius":"ic_aquarius_match.png","pisces":"ic_pisces_match.png","":""]
        return zodiacSquareImgDic[name ] ?? ""
    }
    
    class func getZodiacPeriod(_ subScript:Int) -> String{
        let zodiacPeriodArr : [String] = ["3/21-4/19","4/20-5/20","5/21-6/20","6/21-7/22","7/23-8/22","8/23-9/22","9/23-10/22","10/23-11/21","11/22-12/21","12/22-1/19","1/20-2/18","2/19-3/20"]
        return zodiacPeriodArr[subScript]
    }
    
    class func getZodiaImage(_ subScript:Int) ->String{
        let zodiacImageArr : [String] = ["ic_aries_","ic_taurus_","ic_gemini_","ic_cancer_","ic_leo_","ic_virgo_","ic_libra_","ic_scorpio_","ic_sagittarius_","ic_capricorn_","ic_aquarius_","ic_pisces_"]
        return zodiacImageArr[subScript]
    }
    
    class func getCharacterTopImage(_ name:String) -> String{
        let zodiacImgNameDic = ["aries":"aries_charaTop.jpg","taurus":"taurus_charaTop.jpg","gemini":"gemini_charaTop.jpg","cancer":"cancer_charaTop.jpg","leo":"leo_charaTop.jpg","virgo":"virgo_charaTop.jpg","libra":"libra_charaTop.jpg","scorpio":"scorpio_charaTop.jpg","sagittarius":"sagittarius_charaTop.jpg","capricorn":"capricorn_charaTop.jpg","aquarius":"aquarius_charaTop.jpg","pisces":"pisces_charaTop.jpg","":""]
        return   zodiacImgNameDic[name] ?? ""
    }
    
    class func getZodicPeriodWithName(_ name:String) ->String{
     let zodiacDic = ["aries":"3/21-4/19","taurus":"4/20-5/20","gemini":"5/21-6/20","cancer":"6/21-7/22","leo":"7/23-8/22","virgo":"8/23-9/22","libra":"9/23-10/22","scorpio":"10/23-11/21","sagittarius":"11/22-12/21","capricorn":"12/22-1/19","aquarius":"1/20-2/18","pisces":"2/19-3/20"]
        var periodString:String = ""
        for each in zodiacDic{
            if each.0 == name{
                periodString = each.1
            }
         }
       return periodString
    }
    

    class func getZodicMonthWithName(_ name:String) ->Int{
        let zodiacMonthDic =   ["aries":"March","taurus":"April","gemini":"May","cancer":"June","leo":"July","virgo":"August","libra":"September","scorpio":"October","sagittarius":"November","capricorn":"December","aquarius":"January","pisces":"February"]
        let monthArr = ["January", "February","March","April","May","June","July","August","September","October","November","December"]
    
        let currentMonth = zodiacMonthDic[name] ?? ""
        
        for index in 0..<monthArr.count {
            let month = monthArr[index]
            if month.lowercased() == currentMonth.lowercased() {
               return index
            }
        }
        return 0
    }
}


class CharacteristicsModel: NSObject {
    var avaImg : String!
    var charaTitle : String!
    var element : String!
    var quality : String!
    var color : String!
    var rulingPlanet : String!
    var rulingHouse : String!
    var tarotCard : String!
    var descriptions: String!
    var positive : String!
    var negative : String!
    
    init(jsonData : JSON) {
        super.init()
        self.avaImg = jsonData["avaImg"].stringValue 
        self.charaTitle = jsonData["charaTitle"].stringValue 
        self.element = jsonData["element"].stringValue 
        self.quality = jsonData["quality"].stringValue 
        self.color = jsonData["color"].stringValue 
        self.rulingPlanet = jsonData["rulingPlanet"].stringValue 
        self.rulingHouse = jsonData["rulingHouse"].stringValue 
        self.tarotCard = jsonData["tarotCard"].stringValue 
        self.descriptions = jsonData["description"].stringValue 
        self.positive = jsonData["positive"].stringValue 
        self.negative = jsonData["negative"].stringValue 
    }
}

