//
//  OCUtils.m
//  bibleverse
//
//  Created by 麦子 on 2016/11/17.
//  Copyright © 2016年 qi. All rights reserved.
//

#import "OCUtils.h"

@implementation OCUtils

+ (NSStringDrawingOptions)stringDrawingOptions {
    return NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
}

@end
