//
//  CircleScrollView.h
//  CircleScrollDemo3
//
//  Created by MacBook on 15/2/28.
//  Copyright (c) 2015年 I'm very lazy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleScrollView : UIView
/**
 *  image数组
 */
@property(nonatomic, strong) NSMutableArray  *images;
/**
 *  pageControlTint的颜色
 */
@property(nonatomic, strong) UIColor  *pageIndicatorTintColor;

/**
 *  pageControlCurrentPage的颜色
 */
@property(nonatomic, strong) UIColor  *currentPageIndicatorTintColor;


@end
