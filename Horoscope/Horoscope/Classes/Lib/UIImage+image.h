//
//  UIImage+image.h
//  03-图片裁剪(圆环)
//
//  Created by LW on 16/5/29.
//  Copyright © 2016年 Mr Li. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (image)

//利用图形上下文设置图片圆角边框
+ (nullable UIImage *)imageClipWithImage:(nullable UIImage *)image borderWidth:(CGFloat)borderWidth borderColor:(nullable UIColor *)color;

// 单纯获取屏幕截图
+ (nonnull UIImage *)captureImageFromView:(nullable UIView *)targetView captureRect:(CGRect)rect;

// 截屏保存到系统相册
+ (nullable UIImage *)screenCaptureWithView:(nullable UIView *)targetView captureRect:(CGRect)rect;

// 截屏保存到指定路径中
+ (void)screenCaptureWithView:(nullable UIView *)targetView captureRect:(CGRect)rect savePath:(nonnull NSString *)path;
@end
