//
//  UIImage+image.m
//  03-图片裁剪(圆环)
//
//  Created by LW on 16/5/29.
//  Copyright © 2016年 Mr Li. All rights reserved.
//

#import "UIImage+image.h"

@implementation UIImage (image)

+(UIImage *)imageClipWithImage:(UIImage *)image borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color{
    
    // 图片裁剪，把正方形图片重新生成一张圆形图片
    CGFloat imageW = image.size.width;
    CGFloat imageH = image.size.height;
    // 设置圆环的宽度
    CGFloat border = borderWidth;
    
    //设置大圆形的宽度、高度
    CGFloat ovalW = imageW + 2*border;
    CGFloat ovalH = imageH + 2*border;
    
    // 1.开启上下文
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(ovalW, ovalH), NO, 0);
    // 2.画大圆
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, ovalW, ovalH)];
    [color set];
    [path fill];
    // 3.设置裁剪区域
    UIBezierPath *clipPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(border, border, imageW, imageH)];
    [clipPath addClip];
    // 4.绘制图片
    [image drawAtPoint:CGPointMake(border, border)];
    // 5.获取图片
    UIImage *clipImage = UIGraphicsGetImageFromCurrentImageContext();
    // 6.关不上下文
    UIGraphicsEndImageContext();
    return clipImage;
}

// 获取屏幕截图
+ (UIImage *)captureImageFromView:(UIView *)targetView captureRect:(CGRect)rect{
   
    // 开启位图上下文
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
  // 渲染目标view图层上的控件到，当前上下文
  //  [targetView.layer renderInContext:UIGraphicsGetCurrentContext()];
    [targetView drawViewHierarchyInRect:rect afterScreenUpdates:YES];
    // 从当前上下文中获取image
    UIImage *showImage = UIGraphicsGetImageFromCurrentImageContext();
    // 关闭位图上下文
    UIGraphicsEndImageContext();
  //  UIImageWriteToSavedPhotosAlbum(showImage, self,nil, NULL);
    return showImage;
}

 //截屏保存到系统相册
+ (UIImage *)screenCaptureWithView:(UIView *)targetView captureRect:(CGRect)rect{
    
    // 开启位图上下文
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    // 渲染目标view图层上的控件到，当前上下文
    [targetView.layer renderInContext:UIGraphicsGetCurrentContext()];
    // 从当前上下文中获取image
    UIImage *showImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 关闭位图上下文
    UIGraphicsEndImageContext();
    // 将图片保存到系统相册
    UIImageWriteToSavedPhotosAlbum(showImage, self,nil, NULL);
    return showImage;
 }
//+ (UIImage *)screenCaptureWithView:(UIView *)targetView captureRect:(CGRect)rect{
//    // 开启位图上下文
//    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
//  
//    // 渲染目标view图层上的控件到，当前上下文
//    [targetView.layer renderInContext:UIGraphicsGetCurrentContext()];
//    // [targetView drawViewHierarchyInRect:rect afterScreenUpdates:YES];
//    // 从当前上下文中获取image
//    __block UIImage *showImage;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        showImage = UIGraphicsGetImageFromCurrentImageContext();
//    });
//    
//    // 关闭位图上下文
//    UIGraphicsEndImageContext();
//    return showImage;
//}

// 截屏保存到指定路径中
+ (void)screenCaptureWithView:(UIView *)targetView captureRect:(CGRect)rect savePath:(nonnull NSString *)path{
    
    // 开启位图上下文
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    // 渲染目标view图层上的控件到，当前上下文
    [targetView.layer renderInContext:UIGraphicsGetCurrentContext()];
    // 从当前上下文中获取image
    UIImage *showImage = UIGraphicsGetImageFromCurrentImageContext();
    // 关闭位图上下文
    UIGraphicsEndImageContext();
    // 保存到指定路径中
    //图片转化成二进制码
    NSData* imageData = UIImagePNGRepresentation(showImage);
    
    //或者
    //compressionQuality :压缩图片质量 1表示最高质量
    // NSData* imageData = UIImageJPEGRepresentation(showImage, 1);
    [imageData writeToFile:path atomically:YES];
    
}

@end
