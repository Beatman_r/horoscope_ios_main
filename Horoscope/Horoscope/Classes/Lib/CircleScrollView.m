//
//  CircleScrollView.m
//  CircleScrollDemo3
//
//  Created by MacBook on 15/2/28.
//  Copyright (c) 2015年 I'm very lazy. All rights reserved.
//

#import "CircleScrollView.h"

#define Identifier @"circle"

#define ImagesItemCount self.images.count*5000

#define ImagesDefaultRow (NSUInteger)ImagesItemCount * 0.5
@interface CircleScrollView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong) UICollectionView  *collectionView;

/**
 *  流水布局
 */
@property(nonatomic, strong) UICollectionViewFlowLayout  *layout;

@property(nonatomic, strong) UIPageControl  *pageControl;
/**
 *  定时器
 */
@property(nonatomic, strong) NSTimer  *timer;

@end
@implementation CircleScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initCollectView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
/**
 *  初始化collewctionView
 */
- (void)initCollectView
{
    //流水布局
    self.layout =[[UICollectionViewFlowLayout alloc] init];
     self.layout.itemSize = self.bounds.size;
    self.layout.minimumLineSpacing = 0;
    //设置水平滚动
    self.layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:self.layout];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor=[UIColor clearColor];
    //注册collectionViewCell
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:Identifier];
    [self addSubview:self.collectionView];
    
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.userInteractionEnabled = NO;
    [self addSubview:self.pageControl];
    
    [self addTimer];
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    //默认滚动中间位置
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:ImagesDefaultRow inSection:0];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    
    CGFloat pageControlH = 20;
    CGFloat pageControlW = self.images.count * 20;
    CGFloat pageControlY = self.bounds.size.height - pageControlH;
    self.pageControl.frame = CGRectMake(self.center.x - pageControlW*0.5, pageControlY, pageControlW, 20);
    self.pageControl.numberOfPages = self.images.count;
    
    self.pageControl.pageIndicatorTintColor = self.pageIndicatorTintColor ==nil ?[UIColor purpleColor] : self.pageIndicatorTintColor;
    
    self.pageControl.currentPageIndicatorTintColor = self.currentPageIndicatorTintColor == nil ? [UIColor orangeColor] : self.currentPageIndicatorTintColor;
    
}

#pragma mark - collectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return ImagesItemCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:Identifier forIndexPath:indexPath];
    UIView *imageView = [[UIView alloc] initWithFrame:self.bounds];
    NSInteger s = self.images.count;
    [imageView addSubview: self.images[indexPath.item%s]];
    [cell addSubview:imageView];
    return cell;
}

- (void)addTimer
{
     self.timer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
}
- (void)removeTimer
{
    [self.timer invalidate];
     self.timer = nil;
}
- (void)nextImage
{
    NSIndexPath *visiablePath = [[self.collectionView indexPathsForVisibleItems] firstObject];
    NSInteger visiableItem = visiablePath.item;
    if (visiableItem%self.images.count == 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:ImagesDefaultRow inSection:0];
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        visiableItem = ImagesDefaultRow;
    }
    NSUInteger nextItem = visiableItem + 1;;
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:nextItem inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *visiablePath = [[collectionView indexPathsForVisibleItems] firstObject];
    self.pageControl.currentPage = visiablePath.item % self.images.count;
}
/**
 *  滑动时移除定时器
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self removeTimer];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self addTimer];
}


@end
