//
//  HUD.swift
//  bb
//
//  Created by qi on 16/1/27.
//  Copyright © 2016年 qi. All rights reserved.
//
import PKHUD
import Foundation
protocol HsHUD {
    func showErrorHUD()
    func showSuccessHUD()
    func showProccessHUD()
    func hideHUD()
    func toast(_ title:String)
}

extension HsHUD {
    func showErrorHUD(){
        PKHUD.sharedHUD.contentView = PKHUDErrorView()
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: 0.3)
    }
    
    func showSuccessHUD(){
        PKHUD.sharedHUD.contentView = PKHUDSuccessView()
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: 0.3)
    }
    
    func showProccessHUD(){
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    func hideHUD(){
        PKHUD.sharedHUD.hide(animated: false, completion: nil)
    }
    
    func toast(_ title:String){
        let alertView = OLGhostAlertView(title: title, message: nil, timeout: 1.5, dismissible: true)
        alertView?.style=OLGhostAlertViewStyle.dark
        alertView?.position=OLGhostAlertViewPosition.center
        alertView?.show()
    }
}


func g_toast(_ title:String){
    let alertView = OLGhostAlertView(title: title, message: nil, timeout: 1.5, dismissible: true)
    alertView?.style=OLGhostAlertViewStyle.dark
    alertView?.position=OLGhostAlertViewPosition.center
    alertView?.show()
}
