//
//  BaseHelperTool.swift
//  Horoscope
//
//  Created by Beatman on 17/2/21.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

public var CURRENT_MAIN_SIGN : String? = ""
public var SCREEN_HEIGHT : CGFloat = UIScreen.main.bounds.height
public var SCREEN_WIDTH : CGFloat = UIScreen.main.bounds.width
public var LOCAL_TAG_LIST : String = "localTagList"
public var LOCAL_EMOJI_LIST : String = "localEmojiList"
public let BASIC_FBSHARE_URL = "http://horoscopeweb.dailyinnovation.biz/share"
public let FBSHARE_POST = "/post?postId="
public let FBSHARE_FORECAST = "/forecast?postId="
public let FBSHARE_LOVE = "/dailyLove?postId="
public let FBSHARE_TAROT = "/dailyTarot?postId="
public let CODE_4000 = "code_4000"

public let baseBigFontScale : CGFloat = 15
public let baseSmallFontScale : CGFloat = 13

class BaseHelperTool: NSObject {
    class func getCurrentLanguage() -> String {
        let defs = UserDefaults.standard
        let languages = defs.object(forKey: "AppleLanguages")
        let perferredLanguage = (languages as AnyObject).object(at: 0)
        return perferredLanguage as! String
    }
}
