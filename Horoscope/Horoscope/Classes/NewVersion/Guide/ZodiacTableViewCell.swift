//
//  ZodiacTableViewCell.swift
//  Horoscope
//
//  Created by xiao  on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ZodiacTableViewCell: UITableViewCell {

    let screenWidth = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()
    let screenHeight = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenHeight()
    var zodiacName:UILabel?
    var zodiacTime:UILabel?
    var zodiacImage:UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.zodiacName = UILabel()
        self.zodiacName?.textColor = UIColor.zodicNameLabelColor()
        self.zodiacTime = UILabel()
        self.zodiacTime?.textColor = UIColor.zodicNameLabelColor()
        self.zodiacTime?.textAlignment = NSTextAlignment.right
        self.zodiacImage = UIImageView()
        let backImage  = UIImageView()
        backImage.backgroundColor = UIColor.baseCardBackgroundColor()
        let divide = UIView()
        divide.backgroundColor = UIColor.divideColor()
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        self.contentView.addSubview(backImage)
        self.contentView.addSubview(self.zodiacName!)
        self.contentView.addSubview(self.zodiacTime!)
        self.contentView.addSubview(self.zodiacImage!)
        self.contentView.addSubview(divide)
        backImage.snp.makeConstraints { (make) in
            make.edges.equalTo(self.contentView.snp.edges)
        }
        
        self.zodiacImage?.snp.makeConstraints({ (make) in
            make.left.equalTo(self.contentView.snp.left).offset(25)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.width.equalTo(screenWidth*0.147)
            make.height.equalTo(screenWidth*0.147)
        })

        self.zodiacName?.snp.makeConstraints({ (make) in
            make.left.equalTo(self.zodiacImage!.snp.right).offset(25)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.width.equalTo(screenWidth*0.267)
            make.height.equalTo(40)
        })
        self.zodiacTime?.snp.makeConstraints({ (make) in
            make.right.equalTo(self.contentView.snp.right).offset(-25)
            make.centerY.equalTo(self.contentView.snp.centerY)
            make.width.equalTo(screenWidth*0.4)
            make.height.equalTo(40)
        })
        
        divide.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.bottom.equalTo(self.contentView.snp.bottom)
            make.left.equalTo(self.contentView.snp.left)
            make.right.equalTo(self.contentView.snp.right)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
