//
//  GuideHoroscopeViewController.swift
//  Horoscope
//
//  Created by xiao  on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit
import PKHUD

protocol GuideHoroscopeViewControllerDelegate:class {
      func goOnBtnClick()
}

class GuideHoroscopeViewController: BaseOptionViewController {
    
 
    weak var delegate:GuideHoroscopeViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTopChooseDescripe()
        self.addBackBtn()
        AnaliticsManager.sendEvent(AnaliticsManager.guide_show)
        self.goOnBtn?.addTarget(self, action: #selector(leadToNotification), for: .touchUpInside)
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    func addTopChooseDescripe(){
        //顶部选择星座描述
        let topChooselabel = UILabel()
        self.view.addSubview(topChooselabel)
        topChooselabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(70)
            make.width.equalTo(304)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        topChooselabel.numberOfLines = 2;
        topChooselabel.text = NSLocalizedString("Choose one or more horoscope that you want to see", comment: "")
        topChooselabel.textColor = .white
        topChooselabel.backgroundColor = UIColor.clear
        topChooselabel.font =  UIFont(name: "PingFangHK-Medium", size: 20)
        topChooselabel.textAlignment = NSTextAlignment.center
    }
    
    @objc func popView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func leadToNotification() {
        let chooseCellData = NSKeyedArchiver.archivedData(withRootObject: self.optionCellSelectedArray)
        UserDefaults.standard.set(chooseCellData, forKey: ChooseZodiacArray)
        UserDefaults.standard.synchronize()
        let selectNameArray = self.getSelectedZodiac(self.optionCellSelectedArray)
        NotificationCenter.default.post(name: Notification.Name(rawValue: ZodiacDidChange), object: self, userInfo: ["selectArray":selectNameArray])
        self.dismiss(animated: true, completion: nil)
        if selectNameArray.count == 0{
            AnaliticsManager.sendEvent(AnaliticsManager.guide_continue_click, data: ["signs":"empty"])
        } else{
            var paramStr = ""
            for index in 0 ..< selectNameArray.count{
                let eachStr = selectNameArray[index]
                if index == selectNameArray.count-1{
                   paramStr += "\(eachStr.lowercased())"
                }else{
                   paramStr += "\(eachStr.lowercased()),"
                }
            }
            AnaliticsManager.sendEvent(AnaliticsManager.guide_continue_click, data: ["signs":"\(paramStr)"])
         }
        if self.delegate != nil {
            self.delegate?.goOnBtnClick()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
}
