//
//  HorocopeListViewController.swift
//  Horoscope
//
//  Created by xiao  on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class HorocopeListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,AddHoroscopeViewControllerDelegate{
    
    var listTable = UITableView()
    var zodiacArray:[ZodiacCellModel] = [ZodiacCellModel]()
    var selectedZodiacArray:[ZodiacCellModel] = [ZodiacCellModel]()
    let ZodiacCell = "ZodiacTableViewCell"
    var optionCellSelectedArray:[IndexPath] = [IndexPath]()
    var lastOptionCellArray:[IndexPath] = [IndexPath]()
    override func viewDidLoad() {
        super.viewDidLoad()
        AnaliticsManager.sendEvent(AnaliticsManager.horoscopelist_detail, data: ["a1_page_horoscopelistdetail_show" : "true"])

        self.addBackBtn()
        setUpUI()
        setUpRightNav()
        registerCell()
        getData()
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    
    @objc func popView() {
        if self.lastOptionCellArray == self.optionCellSelectedArray {
            _ = self.navigationController?.popViewController(animated: true)
        }else{
        
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func saveChoosedDatas() -> Void {
        let chooseCellData = NSKeyedArchiver.archivedData(withRootObject: self.optionCellSelectedArray)
        UserDefaults.standard.set(chooseCellData, forKey: ChooseZodiacArray)
        UserDefaults.standard.synchronize()
        let selectNameArray = ZodiacRecordManager.getSelectedZodiacFromselectArray(self.optionCellSelectedArray)
        NotificationCenter.default.post(name: Notification.Name(rawValue: ZodiacDidChange), object: self, userInfo: ["selectArray":selectNameArray])
    }
    
    func getData()  {
        let chooseArrayData = UserDefaults.standard.value(forKey: ChooseZodiacArray) as? Data
        if chooseArrayData != nil {
            let chooseArray = NSKeyedUnarchiver.unarchiveObject(with: chooseArrayData!) as! [IndexPath]
            self.optionCellSelectedArray = chooseArray
            self.lastOptionCellArray = chooseArray
        }
        
        zodiacArray = BaseHoroCollectionViewCell.horoscopeCellModel()
        for indexPath  in self.optionCellSelectedArray {
            let selectedZodiacCell = zodiacArray[indexPath.row]
            self.selectedZodiacArray.append(selectedZodiacCell)
        }
    }
    func registerCell()  {
        self.listTable.register(ZodiacTableViewCell.self, forCellReuseIdentifier: ZodiacCell)
    }
    func setUpRightNav() {
        let editBtn = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editBtnClick))
        self.navigationItem.rightBarButtonItem = editBtn
    }
    
    @objc func editBtnClick()  {
        let chooseCellData = NSKeyedArchiver.archivedData(withRootObject: self.optionCellSelectedArray)
        UserDefaults.standard.set(chooseCellData, forKey: ChooseZodiacArray)
        UserDefaults.standard.synchronize()
        let vc  = AddHoroscopeViewController()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func setUpUI() {
        self.navigationItem.title = "Horoscope List"
        let bgImgView:UIImageView = UIImageView(frame: UIScreen.main.bounds)
        bgImgView.image = UIImage(named:"balckground.jpg")
        self.view.backgroundColor = UIColor.clear
        self.listTable.backgroundColor = UIColor.clear
        self.listTable.frame = CGRect(x: 0, y: 64, width: HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth(), height: HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenHeight() - 64)
        self.listTable.separatorStyle = .none
        self.listTable.delegate = self
        self.listTable.dataSource = self
        self.view.addSubview(bgImgView)
        self.view.addSubview(self.listTable )
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.selectedZodiacArray.count) 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let zodiacCell = tableView.dequeueReusableCell(withIdentifier: ZodiacCell) as! ZodiacTableViewCell
        let zodiacItem = self.selectedZodiacArray[indexPath.row]
        zodiacCell.zodiacName?.text = NSLocalizedString("\(zodiacItem.zodiacName)", comment: "")
        zodiacCell.zodiacTime?.text = zodiacItem.periodString
        zodiacCell.zodiacImage?.image = UIImage(named: zodiacItem.imageName)
        return zodiacCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let mainVc: MainDailyScrollController = self.navigationController?.viewControllers.first as! MainDailyScrollController
        mainVc.scroll(indexPath.row, animated: true)
        self.navigationController?.popToRootViewController(animated: false)
        if indexPath.row < self.selectedZodiacArray.count {
            let zodiacItem = self.selectedZodiacArray[indexPath.row]
            AnaliticsManager.sendEvent(AnaliticsManager.horoscopelist_detail, data: ["a2_button_horoscopelistdetail_click" : zodiacItem.zodiacName])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height*0.119 
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            if self.optionCellSelectedArray.count > 0 {
                self.optionCellSelectedArray.remove(at: indexPath.row)
            }
            self.selectedZodiacArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
            self.saveChoosedDatas()
        }
 
    }
    //MARK:-AddHoroscopeViewControllerDelegate
    func selectCellHaveChanged(_ selectedArray: [IndexPath]) {
        self.optionCellSelectedArray = selectedArray
        self.selectedZodiacArray.removeAll()
        for indexPath  in self.optionCellSelectedArray {
            let selectedZodiacCell = zodiacArray[indexPath.row]
            self.selectedZodiacArray.append(selectedZodiacCell)
        }
        self.listTable.reloadData()
    }
}
