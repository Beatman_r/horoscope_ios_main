//
//  ZodiacCollectionViewCell.swift
//  Horoscope
//
//  Created by xiao  on 17/1/7.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class ZodiacCollectionViewCell: UICollectionViewCell {
    
    var numberImage:UIImageView?
    var zodiacLabel:UILabel?
    var zoidcPeriodLabel:UILabel?
//    var selectedImage:UIImageView?
    var zodiacImage:UIImageView?
    var backView:UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backView = UIView()
        self.backView?.backgroundColor = UIColor.clear
        self.contentView.addSubview(self.backView!)
        self.backView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.contentView.snp.edges)
        })
        self.zodiacImage = UIImageView()
        self.backView!.addSubview(zodiacImage!)
        self.zodiacImage?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.backView!.snp.top)
            make.left.equalTo(self.backView!.snp.left).offset(5)
            make.right.equalTo(self.backView!.snp.right).offset(-5)
            make.height.equalTo(self.contentView.snp.width).offset(-10)
        })
        self.numberImage = UIImageView()
        self.backView?.addSubview(self.numberImage!)
        self.numberImage?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.backView!.snp.top)
            make.left.equalTo(self.backView!.snp.left).offset(5)
            make.right.equalTo(self.backView!.snp.right).offset(-5)
            make.height.equalTo(self.contentView.snp.width).offset(-10)
        })
        self.numberImage?.isHidden = true
        self.zodiacLabel = UILabel()
        self.zodiacLabel?.backgroundColor = UIColor.clear
        self.zodiacLabel?.textColor = UIColor.zodicNameLabelColor()
        self.zodiacLabel?.font = HSFont.baseLightFont(16)
        self.zodiacLabel?.textAlignment = NSTextAlignment.center
        self.backView!.addSubview(self.zodiacLabel!)
        
        self.zoidcPeriodLabel = UILabel()
        self.zoidcPeriodLabel?.textAlignment = NSTextAlignment.center
        self.zoidcPeriodLabel?.textColor = UIColor.zodicNameLabelColor()
        self.zoidcPeriodLabel?.font = HSFont.baseLightFont(10)
        self.backView!.addSubview(self.zoidcPeriodLabel!)
        
        self.zodiacLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.zodiacImage!.snp.bottom).offset(1)
            make.left.equalTo(self.backView!.snp.left).offset(-15)
            make.right.equalTo(self.backView!.snp.right).offset(15)
        })
        self.zoidcPeriodLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(self.zodiacLabel!.snp.bottom).offset(2)
            make.centerX.equalTo(self.backView!.snp.centerX)
        })
        self.backView!.addSubview(self.numberImage!)
//        self.selectedImage = UIImageView()
//        self.selectedImage?.image = UIImage(named: "ic_chooseHoro")
//        self.selectedImage?.hidden = true
//        self.backView!.addSubview(self.selectedImage!)
//        self.selectedImage?.snp.makeConstraints({ (make) in
//            make.right.equalTo(self.backView!.snp.right).offset(2)
//            make.bottom.equalTo(self.backView!.snp.bottom).offset(-39)
//            make.height.equalTo(24)
//            make.width.equalTo(24)
//        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
