//
//  AddHoroscopeViewController.swift
//  Horoscope
//
//  Created by xiao  on 17/1/9.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

protocol AddHoroscopeViewControllerDelegate:class {
    func selectCellHaveChanged(_ selectedArray:[IndexPath])
}
let ZodiacDidChange = "ZodiacDidChange"
let ChooseZodiacArray = "chooseZodiacArray"
class AddHoroscopeViewController: BaseOptionViewController{
    weak var delegate:AddHoroscopeViewControllerDelegate?
    let btnClick = "goOnBtnClick"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Edit", comment: "")
        self.view.bringSubview(toFront: self.goOnBtn!)
        self.tabBarController?.tabBar.isHidden = true
        self.goOnBtn?.isUserInteractionEnabled = true
        self.goOnBtn?.addTarget(self, action: #selector(goOnBtnClick), for: .touchUpInside)
        getData()
        self.addTopChooseDescripe()
        self.addBackBtn()
        self.optionCollectionView?.reloadData()
    }
    
    func addBackBtn() {
        let backBtn = UIButton(type:.custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        backBtn.setImage(UIImage(named: "ic_1"), for: UIControlState())
        backBtn.addTarget(self, action: #selector(popView), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backBtn)
    }
    func addTopChooseDescripe(){
        //顶部选择星座描述
        let topChooselabel = UILabel()
        var offsetHeight:CGFloat = 10
        var descHeight:CGFloat = 80
        var fontSize :CGFloat = 15
        let screenWidth =  UIScreen.main.bounds.size.width
        
        if screenWidth == 320{
           offsetHeight = -25
           descHeight = 70
        }
        else if screenWidth == 375{
            offsetHeight = 0
            fontSize = 20
        }
        else if screenWidth >= 414{
             fontSize = 20
             offsetHeight = 10
             descHeight = 90
        }
        
        self.view.addSubview(topChooselabel)
        topChooselabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.top).offset(descHeight)
            make.width.equalTo(304)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        self.optionCollectionView?.frame = CGRect(x: 0, y: 140+offsetHeight,width: width , height: height - 64 - 140-offsetHeight)
        topChooselabel.numberOfLines = 2;
        topChooselabel.text = NSLocalizedString("Choose one or more horoscope that you want to see", comment: "")
        topChooselabel.textColor = .white
        topChooselabel.backgroundColor = UIColor.clear
        topChooselabel.font =  UIFont(name: "PingFangHK-Medium", size: fontSize)
        topChooselabel.textAlignment = NSTextAlignment.center
    }
    @objc func popView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func getData()  {
        let chooseArrayData = UserDefaults.standard.value(forKey: ChooseZodiacArray) as? Data
        if chooseArrayData != nil {
            let chooseArray = NSKeyedUnarchiver.unarchiveObject(with: chooseArrayData!) as! [IndexPath]
            self.optionCellSelectedArray = chooseArray
            self.optionCollectionView?.reloadData()
        }
    }
    
    @objc func goOnBtnClick()  {
        let chooseCellData = NSKeyedArchiver.archivedData(withRootObject: self.optionCellSelectedArray)
        UserDefaults.standard.set(chooseCellData, forKey: ChooseZodiacArray)
        UserDefaults.standard.synchronize()
 //        self.dismissViewControllerAnimated(true, completion: nil)
 //        if self.delegate != nil {
            self.delegate?.selectCellHaveChanged(self.optionCellSelectedArray as [IndexPath])
  //      }
        let selectNameArray = self.getSelectedZodiac(self.optionCellSelectedArray)
        NotificationCenter.default.post(name: Notification.Name(rawValue: ZodiacDidChange), object: self, userInfo: ["selectArray":selectNameArray])
        _ = self.navigationController?.popViewController(animated: true)
    }
    
   override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let optionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionCell", for: indexPath) as! ZodiacCollectionViewCell
        for index in 0..<self.optionCellSelectedArray.count {
            let indexP = self.optionCellSelectedArray[index]
            if indexP as IndexPath == indexPath {
                optionCell.numberImage?.image = UIImage(named: self.numberArray[index])
                optionCell.numberImage?.isHidden = false
                optionCell.zodiacImage?.isHidden = true
                optionCell.zodiacLabel?.textColor = UIColor.commonPinkColor()
                optionCell.zodiacLabel?.textColor = UIColor.commonPinkColor()
                
            }
        }
        optionCell.zodiacLabel?.text = NSLocalizedString("\(self.zodiacArray[indexPath.row].zodiacName)", comment: "")
        optionCell.zodiacImage?.image = UIImage(named: self.zodiacArray[indexPath.row].imageName)
        optionCell.zoidcPeriodLabel?.text = NSLocalizedString("\(self.zodiacArray[indexPath.row].periodString)", comment: "星座日期")
        return optionCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override  func viewWillAppear(animated: Bool) {
//         super.viewWillAppear(animated)
//   }
//    

    
    
}
