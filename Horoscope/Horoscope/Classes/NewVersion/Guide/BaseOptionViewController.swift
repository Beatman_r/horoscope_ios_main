//
//  BaseOptionViewController.swift
//  Horoscope
//
//  Created by xiao  on 17/1/6.
//  Copyright © 2017年 meevii. All rights reserved.
//

import UIKit

class BaseOptionViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    let width = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenWidth()
    let height  = HSHelpCenter.sharedInstance.appAndDeviceTool.deviceScreenHeight()
    var optionCollectionView:UICollectionView?
    @objc dynamic var optionCellSelectedArray:[IndexPath] = [IndexPath](){
        didSet{
            if optionCellSelectedArray.count>0 {
                self.goOnBtn?.backgroundColor = UIColor.commonPinkColor()
                self.goOnBtn?.isUserInteractionEnabled = true
                self.goOnBtn?.setTitleColor(.white, for: .normal)
                self.goOnBtn?.layer.borderWidth = 0
            }
            if optionCellSelectedArray.count == 0 {
                self.goOnBtn?.backgroundColor = .clear
                self.goOnBtn?.isUserInteractionEnabled = false
                self.goOnBtn?.setTitleColor(UIColor(hexString: "#F95CD1", withAlpha: 0.6), for: .normal)
                self.goOnBtn?.layer.borderColor = UIColor(hexString: "#AC45A2", withAlpha: 0.6)?.cgColor
                self.goOnBtn?.layer.borderWidth = 1
            }
        }}

    let numberArray = ["ic_one_","ic_two_","ic_three_","ic_four_","ic_five_","ic_six_","ic_seven_","ic_eight_","ic_nine_","ic_ten_","ic_eleven_","ic_twelve_"]
    var goOnBtn:UIButton?
    var zodiacArray:[ZodiacCellModel] = [ZodiacCellModel]()
    var backgroundView:UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        registerCell()
        zodiacArray = BaseHoroCollectionViewCell.horoscopeCellModel()
    }
    
    func registerCell() {
        self.optionCollectionView?.register(ZodiacCollectionViewCell.self, forCellWithReuseIdentifier: "optionCell")
    }
    
    func setUpUI() {
        let wPending:CGFloat = width*0.111
//        let hPending:CGFloat = width*0.028
        let insetPending:CGFloat = width*0.128
        let collecitonLayout = UICollectionViewFlowLayout()
        let cellW = (width-2*wPending - 2*insetPending)/3
        let cellH = cellW+height*0.042
        
        
        collecitonLayout.itemSize = CGSize(width: cellW, height: cellH)
        self.optionCollectionView = UICollectionView(frame: CGRect(x: 0, y: 130,width: width , height: height - 64 - 130), collectionViewLayout: collecitonLayout)
        self.optionCollectionView?.delegate = self
        self.optionCollectionView?.dataSource = self
        self.optionCollectionView?.backgroundColor = UIColor.clear
        let bgImgView:UIImageView = UIImageView(frame: UIScreen.main.bounds)
        bgImgView.image = UIImage(named: "balckground.jpg")
        self.view.addSubview(bgImgView)
        self.view.addSubview(self.optionCollectionView!)
        self.initOkButtonStyle()
    }
    
    func initOkButtonStyle(){
        self.goOnBtn = UIButton(type: .custom)
        goOnBtn?.titleLabel?.font = HSFont.baseRegularFont(24)
        goOnBtn?.setTitleColor(UIColor(hexString: "#F95CD1", withAlpha: 0.6), for: .normal)
        self.goOnBtn?.isUserInteractionEnabled = false
        self.goOnBtn?.setTitle(NSLocalizedString("OK", comment: ""), for: UIControlState())
        self.view.addSubview(goOnBtn!)
        
        var okButtonWidth:CGFloat = 270
        var okButtonHight:CGFloat = height*0.08
        if ScreenW == 320{
            okButtonWidth = 240
            okButtonHight = height*0.07
        }
        else if ScreenW >= 414{
            okButtonWidth = 300
        }
        
        goOnBtn!.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp.bottom).offset(-13)
            make.width.equalTo(okButtonWidth)
            make.centerX.equalTo(self.view.snp.centerX)
            make.height.equalTo(okButtonHight)
        }
        goOnBtn?.backgroundColor = .clear
        goOnBtn?.layer.cornerRadius = okButtonHight/2
        goOnBtn?.layer.masksToBounds = true
        goOnBtn?.layer.borderColor = UIColor(hexString: "#AC45A2", withAlpha: 0.6)?.cgColor
        goOnBtn?.layer.borderWidth = 1
        self.view.bringSubview(toFront: self.goOnBtn!)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return zodiacArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if optionCellSelectedArray.contains(indexPath) {
            let selectCell = collectionView.cellForItem(at: indexPath) as? ZodiacCollectionViewCell
            selectCell?.numberImage?.isHidden = true
            selectCell?.zodiacImage?.isHidden = false
            selectCell?.zodiacLabel?.textColor = UIColor.zodicNameLabelColor()
            optionCellSelectedArray = optionCellSelectedArray.filter({ (index) -> Bool in
                index != indexPath
            })
        }else{
            let selectCell = collectionView.cellForItem(at: indexPath) as? ZodiacCollectionViewCell
                selectCell?.numberImage?.isHidden = false
                selectCell?.zodiacImage?.isHidden = true
            self.optionCellSelectedArray.append(indexPath)
        }
        if self.optionCellSelectedArray.count > 0  {
            for  index  in  0..<self.optionCellSelectedArray.count{
                let indexPathOfSelectItem = self.optionCellSelectedArray[index]
                let selectCell = collectionView.cellForItem(at: indexPathOfSelectItem) as? ZodiacCollectionViewCell
                selectCell?.numberImage?.image = UIImage(named: numberArray[index])
                selectCell?.numberImage?.isHidden = false
                selectCell?.zodiacLabel?.textColor = UIColor.commonPinkColor()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return width*0.056
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return width*0.028
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let optionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionCell", for: indexPath) as! ZodiacCollectionViewCell
        optionCell.zodiacLabel?.text = NSLocalizedString("\(self.zodiacArray[indexPath.row].zodiacName)", comment: "")
        optionCell.zodiacImage?.image = UIImage(named: self.zodiacArray[indexPath.row].imageName)
        optionCell.zoidcPeriodLabel?.text = NSLocalizedString("\(self.zodiacArray[indexPath.row].periodString)", comment: "星座日期")

        return optionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsetsMake(10, 0.128*width, 10, 0.128*width)
    }
    
    func getSelectedZodiac(_ selectArray:[IndexPath]) -> [String]{
        let zodiacArray = BaseHoroCollectionViewCell.horoscopeCellModel()
        var selectedZodiacArray = [String]()
        for indexPath in selectArray {
            let selectedZodiacName = zodiacArray[indexPath.row].zodiacName
            selectedZodiacArray.append(selectedZodiacName)
        }
        return selectedZodiacArray
    }

}
