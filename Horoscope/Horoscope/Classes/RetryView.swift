//
//  RetryView.swift
//  Horoscope
//
//  Created by Wang on 16/12/13.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

protocol RetryViewDelegate:class {
    func retryToLoadData()
}

class RetryView: UIView,UITableViewDelegate {
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        self.backgroundColor=UIColor.init(hexString: "1A1A1A", withAlpha: 0.6)
        self.addViews()
        self.addConstraint()
        self.startLoading()
    }
    
    weak var delegate:RetryViewDelegate?
    let retryLabel:UILabel = {
        let lab = UILabel()
        lab.numberOfLines=0
        lab.text = NSLocalizedString("The data was lost. Click to retry.", comment: "")
        lab.font = HSFont.baseLightFont(18)
        lab.textColor = UIColor.retryWordColor()
        lab.textAlignment = .center
        return lab
    }()
    
    let retryButton:UIButton = {
        let lab = UIButton()
        lab.setTitle("Retry", for: UIControlState())
        lab.layer.masksToBounds = true
        lab.layer.cornerRadius = 18
        lab.backgroundColor = UIColor.clear
        lab.layer.borderColor = UIColor.commonPinkColor().cgColor
        lab.setTitleColor(UIColor.commonPinkColor(), for: UIControlState())
        lab.layer.borderWidth = 1
        return lab
    }()
    
    let retryIcon : UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(named: "ic_spaceship_")
        return imgView
    }()
    
    let oopsLabel:UILabel = {
        let lab = UILabel()
        lab.text = NSLocalizedString("Oops!", comment: "")
        lab.font = HSFont.baseLightFont(18)
        lab.textColor = UIColor.retryWordColor()
        lab.textAlignment = .center
        return lab
    }()
    
    let indiacator:UIActivityIndicatorView = {
        let indi = UIActivityIndicatorView(activityIndicatorStyle: .white)
        indi.color = UIColor.white
        indi.startAnimating()
        return indi
    }()
    
    func addViews() {
        self.addSubview(self.retryButton)
        self.addSubview(self.oopsLabel)
        self.addSubview(self.retryLabel)
        self.addSubview(self.indiacator)
        self.addSubview(self.retryIcon)
        self.retryButton.addTarget(self, action: #selector(reloading), for: .touchUpInside)
    }
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    func addConstraint(){
        self.retryIcon.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top).offset(100)
            make.height.equalTo(self.screenWidth * 140/375)
            make.width.equalTo(self.screenWidth * 140/375)
        }
        self.indiacator.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top).offset(250)
            make.height.equalTo(50)
            make.width.equalTo(50)
        }
        self.oopsLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.retryIcon.snp.bottom).offset(10)
        }
        self.retryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.oopsLabel.snp.bottom)
            make.left.equalTo(self.snp.left).offset(60)
            make.right.equalTo(self.snp.right).offset(-60)
        }
        self.retryButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.retryLabel.snp.bottom).offset(16)
            make.height.equalTo(36)
            make.width.equalTo(120)
        }
    }
    
    //MARK:ActionsAndStatus
    func startLoading(){
        self.indiacator.startAnimating()
        self.oopsLabel.isHidden=true
        self.retryLabel.isHidden=true
        self.retryButton.isHidden=true
        self.retryIcon.isHidden = true
    }
    
    func loadFailed() {
        self.indiacator.stopAnimating()
        self.oopsLabel.isHidden=false
        self.retryLabel.isHidden=false
        self.retryButton.isHidden=false
        self.retryIcon.isHidden=false
    }
    
    @objc func reloading() {
        self.indiacator.startAnimating()
        self.retryButton.isHidden = true
        self.oopsLabel.isHidden = true
        self.retryLabel.isHidden = true
        self.retryIcon.isHidden = true
        self.delegate?.retryToLoadData()
    }
    
    func loadSuccess(){
        self.indiacator.stopAnimating()
        self.retryButton.isHidden = true
        self.oopsLabel.isHidden = true
        self.retryLabel.isHidden = true
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
