//
//  DataTypeUtil.swift
//  bibleverse
//
//  Created by haoxudong on 2016/12/13.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class DataTypeUtil: NSObject {

    class func isNull(_ value: String?) -> String {
        if value == nil {
            return ""
        } else {
            return value!
        }
    }
    
    class func isNull(_ num: Int?) -> Int {
        if num == nil {
            return 0
        } else {
            return num!
        }
    }
    
    class func isNull(_ b: Bool?) -> Bool {
        if b == nil {
            return false
        } else {
            return b!
        }
    }

}
