//
//  TextPartToolProtocol.swift
//  Horoscope
//
//  Created by Wang on 16/11/24.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

protocol TextPartToolProtocol {
    
    func removeBibleAriTag(_ text:String)->String
    
    func getFormattedNumber(_ paramCount:Int) -> String
    
    func calculateStrWidth(_ str:String?, font:CGFloat, height:CGFloat) ->CGFloat
    
    func calculateStringHeight(paramString text:String,
                                           fontSize:CGFloat,
                                           font:UIFont,
                                           width:CGFloat,
                                           multyplyLineSpace:CGFloat) -> CGFloat
    
    func setTextlabelAttributedString(lineSpace multiplyLineSpace:CGFloat,
                                                textColor:UIColor,
                                                targetStr:String,
                                                font:UIFont?)->NSMutableAttributedString
    
    func setTextLineSpace(_ content:String, space:CGFloat) -> NSMutableAttributedString
    
    func calculateLabelHeight(_ str:String?, font:UIFont, width:CGFloat) ->CGFloat
}

class TextTransactManager: TextPartToolProtocol {
    
    func removeBibleAriTag(_ text:String)->String{
        var str=text
        do {
            str=str.replacingOccurrences(of: "</bible>", with: "")
            let pattern = "<bible ari='.*'>"
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            str = regex.stringByReplacingMatches(in: str, options: [], range: NSRange(0..<str.count), withTemplate: "")
        }
        catch {
            return text
        }
        return str
    }
    
    func getFormattedNumber(_ paramCount:Int) -> String  {
        var countStr:String = ""
        let s = Double(paramCount)/1000
        if paramCount < 1000{
            countStr=String(paramCount)
        }else  if paramCount<10000 && paramCount >= 1000 {
            let index = String(paramCount).index(String(paramCount).startIndex, offsetBy: 1)
            let kNum = String(String(paramCount)[..<index]);
            let otherStr = String(String(paramCount)[index...]);
            countStr="\(kNum),\(otherStr)"
        }
        else if paramCount >= 10000 && paramCount<100000  {
            countStr=String(format: "%.1f", s)+" k"
        }
        else if paramCount >= 100000{
            countStr="99.9 k"
        }
        return countStr
    }
    
    
    func calculateStrWidth(_ str:String?, font:CGFloat, height:CGFloat) ->CGFloat {
        guard let string:String = str else {
            return 0
        }
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: font)]
        let option : NSStringDrawingOptions = OCUtils.stringDrawingOptions()
        let rect:CGRect = string.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: height), options: option, attributes: attributes, context: nil)
        let width = rect.size.width+1
        return width
    }
    
    func setTextlabelAttributedString(lineSpace multiplyLineSpace:CGFloat,
                                                textColor:UIColor,
                                                targetStr:String,
                                                font:UIFont?)->NSMutableAttributedString{
        
        let AttributedStr1: NSMutableAttributedString = NSMutableAttributedString(string: targetStr)
        let paragraphStyle1 = NSMutableParagraphStyle()
        paragraphStyle1.lineBreakMode = NSLineBreakMode.byTruncatingTail
        
        AttributedStr1.addAttribute(NSAttributedStringKey.font, value: font ??  UIFont.systemFont(ofSize: 18) , range: NSMakeRange(0, targetStr.unicodeScalars.count))
        paragraphStyle1.lineHeightMultiple = multiplyLineSpace
        AttributedStr1.addAttribute(NSAttributedStringKey.foregroundColor, value: textColor,
                                    range: NSMakeRange(0,targetStr.unicodeScalars.count))
        AttributedStr1.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle1,
                                    range: NSMakeRange(0, (targetStr.unicodeScalars.count)))
        AttributedStr1.yy_setLineBreakMode(NSLineBreakMode.byWordWrapping, range: NSMakeRange(0, (targetStr.unicodeScalars.count)))
        return AttributedStr1
    }
    
    
    func calculateStringHeight(paramString text:String,
                                           fontSize:CGFloat,
                                           font:UIFont,
                                           width:CGFloat,
                                           multyplyLineSpace:CGFloat) -> CGFloat{
        
        let string:String = text
        let attributedString = NSMutableAttributedString.init(string: string)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = multyplyLineSpace
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.unicodeScalars.count))
        attributedString.addAttribute(NSAttributedStringKey.font, value: font, range: NSMakeRange(0, text.unicodeScalars.count))
        let option : NSStringDrawingOptions = OCUtils.stringDrawingOptions()
        let rect:CGRect = attributedString.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: option, context: nil)
        let height = rect.size.height+1
        return height
    }
    
    func setTextLineSpace(_ content:String, space:CGFloat) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString.init(string: content)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = space
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, content.unicodeScalars.count))
        return attributedString
    }
    
    func calculateLabelHeight(_ str:String?, font:UIFont, width:CGFloat) ->CGFloat {
        guard let string:String = str else {
            return 0
        }
        let attributes = [NSAttributedStringKey.font: font]
        let option : NSStringDrawingOptions = OCUtils.stringDrawingOptions()
        let rect:CGRect = string.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: option, attributes: attributes, context: nil)
        let height = rect.size.height+1
        return height
    }
}
