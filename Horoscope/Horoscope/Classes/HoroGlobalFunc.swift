//
//  HoroGlobalFunc.swift
//  Horoscope
//
//  Created by xiao  on 16/12/1.
//  Copyright © 2016年 meevii. All rights reserved.
//

import Foundation

func LXSWLog<T>(_ message:T, file:String=#file, lineNumber:Int=#line) {
    #if DEBUG
        let fileName = (file as NSString).lastPathComponent
        print("[\(fileName):line:\(lineNumber)]- \(message)")
    #endif
}
