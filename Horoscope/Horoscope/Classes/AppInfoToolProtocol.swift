//
//  tokenfile.swift
//  bb
//
//  Created by ryan on 16/5/4.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol AppInfoToolProtocol {
    func   appVersionCode() -> String
    func   appBuildNum()    -> String
    func   deviceLanuageCode()    -> String
    func   deviceCountryCode()    -> String
    func   deviceUUID()     -> String
    func   deviceScreenWidth()  ->CGFloat
    func   deviceScreenHeight() ->CGFloat
    func   deviceNetConnectionAvailiable() -> Bool
}

class AppAndDeviceBaseInfoManager:AppInfoToolProtocol{
    
    func appVersionCode() -> String {
        let dic = Bundle.main.infoDictionary
        let versionCode = dic!["CFBundleShortVersionString"]
        return String(describing: versionCode!)
    }
    
    func appBuildNum() -> String {
        let dic = Bundle.main.infoDictionary
        let versionCode = dic!["CFBundleVersion"]
        return String(describing: versionCode!)
    }
    
    func deviceLanuageCode() -> String {
        let local = Locale.current.identifier.components(separatedBy: "_").first
        var urlStr: String = ""
        if local == "pt" {
            urlStr = "pt"
        } else if local == "es" {
            urlStr = "es"
        }  else if local == "ko" {
            urlStr = "ko"
        }  else {
            urlStr = "en"
        }
        return urlStr
    }
    
    func deviceUUID() -> String {
        let idString = UIDevice.current.identifierForVendor?.uuidString
        return idString!
    }
    
    func deviceCountryCode() -> String {
        let s = Locale.current.identifier.components(separatedBy: "_")
        return s[1]
    }
    
    func deviceScreenWidth() ->CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    func deviceScreenHeight() ->CGFloat{
        return UIScreen.main.bounds.size.height
    }
    
    // MARK;netConnection
    func deviceNetConnectionAvailiable() -> Bool {
        var isConnected = false
        
        let reach = NetworkReachabilityManager()        // 没网
        if reach?.networkReachabilityStatus == .notReachable {
            isConnected = false
        } else if reach?.networkReachabilityStatus == .unknown {
            isConnected = false
        } else {
            isConnected = true
        }
        return isConnected
    }
}




