//
//  HSColor.swift
//  Horoscope
//
//  Created by Wang on 16/11/26.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit
import ChameleonFramework

/*
 应用种使用的颜色
 */
extension UIColor {
    class func bbColors()->[UIColor]{
        return [UIColor]()
    }
    
    class func FBADButtonColor()->UIColor{
        return UIColor(red: 129/255, green:183/255, blue: 61/255, alpha: 1)
    }
    
    class func FBLoginButtonColor()->UIColor{
        return UIColor(red: 87/255, green:128/255, blue: 196/255, alpha: 1)
    }
    class func GoogleLoginButtonColor()->UIColor{
        return UIColor(red: 217/255, green:72/255, blue: 61/255, alpha: 1)
    }
    
    class func cardBackColor()->UIColor {
        return UIColor(hexString: "f5f6f7", withAlpha: 0.6)!
    }
    
    class func purpleTitleColor() ->UIColor{
        return UIColor(hexString: "c4beff", withAlpha: 1)!
    }
    
    class func purpleContentColor() ->UIColor{
        return UIColor(hexString: "b5aeff", withAlpha: 1)!
    }
    
    class func commentFootBtnBorderColor() -> UIColor {
        return UIColor(hexString: "b5aeff", withAlpha: 0.3)!
    }
    
    class func purpleContentColorWithAlpha() ->UIColor{
        return UIColor(hexString: "b5aeff", withAlpha: 0.5)!
    }
    
    class func luckyPurpleContentColor() ->UIColor{
        return UIColor(hexString: "746dc1", withAlpha: 1)!
    }
    
    class func retryWordColor() -> UIColor {
        return UIColor(hexString: "#dddddd", withAlpha: 1)!
    }
    
    class func luckyPlaceHolderColor() ->UIColor{
        return UIColor(hexString: "746dc1", withAlpha: 0.5)!
    }
    
    class func luckyPurpleTitleColor() ->UIColor{
        return UIColor(hexString: "b5aeff", withAlpha: 1)!
    }
    
    class func luckyHeaderButtonColor() ->UIColor {
        return UIColor(hexString: "be5af7", withAlpha: 1)!
    }
    
    class func luckyPurpleReplyColor() ->UIColor{
        return UIColor(hexString: "7e7aa6", withAlpha: 0.6)!
    }
    
    class func basePurpleBackgroundColor() ->UIColor {
        return UIColor.init(red: 43/255, green: 20/255, blue: 57/255, alpha:1)
    }
    
    class func basePurpleBackgroundColorDark() ->UIColor {
        return UIColor.init(red: 21/255, green: 9/255, blue: 29/255, alpha:1)
    }
    
    class func commentFootBtnBGColor() -> UIColor {
        return UIColor(hexString: "#251132")!
    }
    
    class func tabbarTintColor()->UIColor{
        return UIColor(hexString: "ffa200", withAlpha: 1)!
    }
    class func windowColor()->UIColor{
        return UIColor(hexString: "3e1b53",withAlpha: 1)!
    }
    
    class func hsViewcountsColor() ->UIColor{
        return UIColor(hexString: "ffa200",withAlpha: 1)!
    }
    class func replyViewLabelColor() ->UIColor{
        return UIColor(hexString: "3e1b53",withAlpha: 1)!
    }
    class func lightPurpleColor() ->UIColor{
        return UIColor(hexString:"746dc1",withAlpha: 1)!
    }
    class func durationTimeColor() ->UIColor{
        return UIColor(hexString:"ffffff",withAlpha: 1)!
    }
    class func viewsLabelColor() ->UIColor{
        return UIColor(hexString:"666666",withAlpha: 1)!
    }
    class func divideLineColor() ->UIColor{
        return UIColor(hexString:"ffffff",withAlpha: 0.1)!
    }
    
    class func hsChooseHoroBtnColor() ->UIColor{
        return UIColor(hexString:"ffa200",withAlpha: 1)!
    }
    
    class func hsTarotContentColor() ->UIColor{
        return UIColor.init(red: 255/255, green: 169/255, blue: 225/255, alpha: 1)
    }
    class func adOfTarotBackColor() ->UIColor{
        return UIColor.init(red: 95/255, green: 9/255, blue: 123/255, alpha: 1)
    }
    
    class func hsYouTubuttonColor()->UIColor{
        return UIColor(red: 197/255, green:17/255, blue: 9/255, alpha: 1)
    }
    class func markBtnGreenColor()->UIColor{
        return UIColor(hexString: "4eae50")!
    }
    class func tarotAdButtonColor()->UIColor {
        return UIColor(hexString: "ff68ca")!
    }
    class func cookieButtonColor()->UIColor {
        return UIColor(hexString: "9c00ff")!
    }
    class func cookieTitleColor()->UIColor {
        return UIColor(hexString: "100d32")!
    }
    class func planBackColor()->UIColor{
        return UIColor(red: 244/255, green:244/255, blue: 244/255, alpha: 1)
    }
    class func zodicTitleColor()->UIColor{
        return UIColor(hexString: "899cfa", withAlpha: 1)!
    }
    class func zodicNameLabelColor()->UIColor{
        return UIColor(hexString: "7284f2",withAlpha: 1)!
    }
    class func createTimeColor()->UIColor{
        return UIColor(hexString: "7e7aa6",withAlpha: 1)!
    }
    class func replyBtnBarBackgroundColor() -> UIColor {
        return UIColor.init(red: 20/255, green: 15/255, blue: 26/255, alpha: 1)
    }
    class func baseCardBackgroundColor() ->UIColor {
        return UIColor(hexString: "000000",withAlpha: 0.3)!
    }
    class func baseDetailBackgroundColor() ->UIColor{
        return UIColor.init(red: 62/255, green: 27/255, blue: 83/255, alpha: 1)
    }
    class func baseNavigationBarColor() ->UIColor{
        return UIColor(hexString: "#1c0e27")!
    }
    class func divideColor() ->UIColor{
        return UIColor(hexString: "ffffff",withAlpha: 0.1)!
    }
    class func guideNotifColor() ->UIColor{
        return UIColor(hexString: "272262",withAlpha: 1)!
    }
    
    class func disableCancelGrey() -> UIColor{
        return UIColor(hexString: "aaa",withAlpha: 1)!
    }
    
    class func commonPinkColor() ->UIColor{
        return UIColor(hexString: "#f95cd1",withAlpha: 1)!
    }
    
    class func unlikeColor() ->UIColor{
        return UIColor(red: 74/255, green: 94/255, blue: 231/255, alpha: 1)
    }
    
    class func bbVerseGrayColor() -> UIColor {
        return UIColor(hexString: "333333")!
    }
    
    class func bbgrayLineColor()->UIColor{
        //       return UIColor(hexString: "d2d2d2")
        return UIColor(hexString: "eaeaea")!
    }
    
    class func bbVerseBtnColor() ->UIColor {
        return UIColor(red: 74/255, green: 94/255, blue: 231/255, alpha: 1)
    }
    
    class func commentTimeColor() ->UIColor{
        return UIColor(hexString: "7e7aa6",withAlpha: 1)!
    }
    class func commentFloorColor() ->UIColor{
        return UIColor(hexString: "7e7aa6",withAlpha: 1)!
    }
    class func commentFloorBackgroundColor() ->UIColor{
        return UIColor(red: 60/255, green: 40/255, blue: 79/255, alpha: 1)
    }
    class func commentViewImage() ->UIColor{
        return UIColor(hexString: "566cf2",withAlpha: 1)!
    }
    class func commentDivide() ->UIColor{
        return UIColor(hexString: "b5aeff",withAlpha: 0.5)!
    }
    
    class func meSpreadLineColor() -> UIColor{
        return UIColor(hexString: "#c3cde8",withAlpha: 1)!
    }
    
    class func pubTimeColor() ->UIColor{
        return  UIColor(hexString: "#7e7aa6",withAlpha: 1)!
    }
    
    class func spreadLineColor() ->UIColor{
        return  UIColor(hexString: "#ffffff",withAlpha: 0.1)!
    }
    
    class func rssSourceBlueColor() -> UIColor{
        return  UIColor(hexString: "#be5af7",withAlpha: 1)!
    }
    
    class func notiRed()-> UIColor{
        return UIColor(hexString: "#e5004f",withAlpha: 1)!
    }
    
    class func hsTextColor(_ colorNum:Int)->UIColor? {
        if colorNum<1 || colorNum>4 {
            return nil
        }
        else {
            let fontColorList = [
                "333333",
                "999999",
                ]
            return UIColor(hexString:fontColorList[colorNum-1])
        }
    }
}
