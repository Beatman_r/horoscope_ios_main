//
//  DetailViewController.swift
//  Horoscope
//
//  Created by Beatman on 16/11/21.
//  Copyright © 2016年 meevii. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    
    var leftName : String!
    
    var rightName : String!
    
    var tag = 2
    
    var contentToShow : String = ""
    
    var zodiacModel : ZodiacModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshLabel()
    }
    
    func refreshLabel() {
        if tag == 1{
            topLabel.text = contentToShow
            centerLabel.text = leftName.uppercaseString
            bottomLabel.text = rightName.uppercaseString
        }else{
            self.topLabel.text = zodiacModel.content
            self.centerLabel.text = zodiacModel.horoscopeName?.uppercaseString
            self.bottomLabel.text = zodiacModel.dateString
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
