//
//  OCUtils.h
//  bibleverse
//
//  Created by 麦子 on 2016/11/17.
//  Copyright © 2016年 qi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OCUtils : NSObject

+ (NSStringDrawingOptions)stringDrawingOptions; 

@end
