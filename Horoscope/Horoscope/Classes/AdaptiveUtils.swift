//
//  AdaptiveUtils.swift
//  bibleverse
//
//  Created by 麦子 on 2016/11/23.
//  Copyright © 2016年 qi. All rights reserved.
//

import UIKit

class AdaptiveUtils: NSObject {
    
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let widthScale:CGFloat = screenHeight/667.0   // default is iphone6
    static let heightScale:CGFloat = screenWidth/375.0   // default is iphone6
    static let fontScale:CGFloat = screenWidth/375.0   // default is iphone6
    
    class func getX(_ x:CGFloat) -> CGFloat {
        return x * widthScale
    }
    
    class func getY(_ y:CGFloat) -> CGFloat {
        return y * heightScale
    }
    
    class func getF(_ font:CGFloat) -> CGFloat {
        return font * fontScale
    }
    
    class func getW(_ w:CGFloat) -> CGFloat {
        return w * widthScale
    }
    
    class func getH(_ h:CGFloat) -> CGFloat {
        return h * heightScale
    }
    
    class func BBRectMake(_ x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> CGRect {
        let rect:CGRect = CGRect(x: getX(x), y: getY(y), width: getW(width), height: getH(height))
        return rect
    }
    
    class func BBSizeMake(_ width: CGFloat, height: CGFloat) -> CGSize {
        let size:CGSize = CGSize(width: getW(width), height: getH(height))
        return size
    }
}
